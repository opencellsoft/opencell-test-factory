package customKeywords

import com.github.javafaker.Faker
import com.kms.katalon.core.annotation.Keyword

class DataGenerator {
	static Faker faker = new Faker()

	@Keyword
	static String generateRandomName() {
		return faker.name().fullName()
	}

	@Keyword
	static String generateRandomProductName() {
		return faker.commerce.productName()
	}

	@Keyword
	static String generateRandomEmail() {
		return faker.internet().emailAddress()
	}

	@Keyword
	static String generateRandomPhoneNumber() {
		return faker.phoneNumber().cellPhone()
	}

	@Keyword
	static String generateRandomCity() {
		return faker.address().city()
	}
}
