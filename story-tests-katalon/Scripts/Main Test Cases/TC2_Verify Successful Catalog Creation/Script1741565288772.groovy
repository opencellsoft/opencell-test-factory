import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Common Test Cases/CreateProductSimpleOneshot'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Cases/CreateOffer'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Common Test Cases/LoginOC'), [('Username') : 'opencell.admin', ('Password') : 'opencell.admin'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Catalog/Offer/Page_- Opencell/a_Catalog_AppButton AppButtonActive css-18f_dc5b2e'))

WebUI.click(findTestObject('Object Repository/Catalog/Offer/Page_Commercial offers - Catalog - Opencell/div_Name_ag-header-cell-resize'))

WebUI.setText(findTestObject('Object Repository/Catalog/Offer/Page_Commercial offers - Catalog - Opencell/input__searchBar'), 'OF-25000041')

WebUI.click(findTestObject('Object Repository/Catalog/Offer/Page_Commercial offers - Catalog - Opencell/p_Fantastic Rubber Clock'))

WebUI.click(findTestObject('Object Repository/Catalog/Offer/Page_Fantastic Rubber Clock OF-25000041 - C_8e0d10/button_Add product'))

WebUI.click(findTestObject('Object Repository/Catalog/Offer/Page_Fantastic Rubber Clock OF-25000041 - C_8e0d10/div_'))

WebUI.setText(findTestObject('Object Repository/Catalog/Offer/Page_Fantastic Rubber Clock OF-25000041 - C_8e0d10/input__searchBar'), 
    'XGL')

WebUI.click(findTestObject('Object Repository/Catalog/Offer/Page_Fantastic Rubber Clock OF-25000041 - C_8e0d10/p_Small Cotton Knife'))

WebUI.click(findTestObject('Object Repository/Catalog/Offer/Page_Fantastic Rubber Clock OF-25000041 - C_8e0d10/button_Add product_1'))

WebUI.click(findTestObject('Object Repository/Catalog/Offer/Page_OF-25000041 OF-25000041 - Commercial o_afe21b/button_Publish'))

WebUI.click(findTestObject('Object Repository/Catalog/Offer/Page_OF-25000041 OF-25000041 - Commercial o_afe21b/button_Actions'))

WebUI.click(findTestObject('Object Repository/Catalog/Offer/Page_OF-25000041 OF-25000041 - Commercial o_afe21b/div_Action executed successfuly'))

WebUI.closeBrowser()

