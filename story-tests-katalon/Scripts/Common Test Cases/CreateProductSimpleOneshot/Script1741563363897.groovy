import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.testobject.ConditionType
import groovy.json.JsonSlurper
import com.github.javafaker.Faker
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import internal.GlobalVariable

// Générer des données aléatoires
Faker faker = new Faker()
def productCode = faker.bothify("???#####", true)


def variables = [
	'chargeCode': faker.bothify("???#####", true),
	'productCode': faker.bothify("???#####", true),
	'label': faker.commerce().productName()
	
]

ResponseObject response = WS.sendRequest(findTestObject('Object Repository/API/CreateProductSimpleOneshot', variables))

// Vérifier le statut HTTP de la réponse
WS.verifyResponseStatusCode(response, 200)

// Afficher la réponse dans la console
println "Response Body: " + response.getResponseBodyContent()

// Convertir la réponse JSON en objet
def jsonResponse = new JsonSlurper().parseText(response.getResponseBodyContent())

// Vérifier une valeur spécifique dans la réponse
WS.verifyEqual(jsonResponse.status, "SUCCESS")

GlobalVariable.productCode = jsonResponse.entityCode