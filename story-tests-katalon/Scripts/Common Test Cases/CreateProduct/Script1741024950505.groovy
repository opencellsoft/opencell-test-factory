import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.G_SiteURL)

WebUI.callTestCase(findTestCase('Common Test Cases/LoginOC'), [('Username') : 'opencell.admin', ('Password') : 'opencell.admin'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Catalog/Page_- Opencell/a_Catalog_AppButton AppButtonActive css-18f_dc5b2e'))

WebUI.click(findTestObject('Object Repository/Catalog/Page_Commercial offers - Catalog - Opencell/path'))

WebUI.click(findTestObject('Object Repository/Catalog/Page_Commercial offers - Catalog - Opencell/span_Products'))

WebUI.click(findTestObject('Object Repository/Catalog/Page_Products - Catalog - Opencell/path'))

productName = CustomKeywords.'customKeywords.DataGenerator.generateRandomProductName'()

WebUI.setText(findTestObject('Object Repository/Catalog/Page_New - products - Catalog - Opencell/input__label'), 
    productName)

WebUI.click(findTestObject('Object Repository/Catalog/Page_New - products - Catalog - Opencell/button_Save'))

WebUI.verifyElementText(findTestObject('Object Repository/Catalog/Page_TESTproduct - Products - Catalog - Opencell/div_Element created'), 
    'Element created')

WebUI.closeBrowser()

