<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path</name>
   <tag></tag>
   <elementGuidId>fa404642-3676-48ab-b8be-35d3c27aca22</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.css-wzf3e6 > svg.MuiSvgIcon-root.MuiSvgIcon-fontSizeMedium.css-vubbuv > path</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>[data-testid=&quot;BookIcon&quot;] path</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
      <webElementGuid>13413853-e8d8-484e-9d17-6035200504cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M18 2H6c-1.1 0-2 .9-2 2v16c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2M6 4h5v8l-2.5-1.5L6 12z</value>
      <webElementGuid>a57b82f7-bd09-4c6c-a5a1-7fe90fe20297</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;layout css-j2ivl7-LayoutWithoutTheme-root&quot;]/div[@class=&quot;css-rr5ovl-LayoutWithoutTheme-appFrame-appFrame&quot;]/main[@class=&quot;css-cz9pya-LayoutWithoutTheme-contentWithSidebar&quot;]/div[@class=&quot;MuiDrawer-root MuiDrawer-docked css-izhvmf-Sidebar-wrapper-Sidebar-wrapperClosed&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-elevation MuiPaper-elevation0 MuiDrawer-paper MuiDrawer-paperAnchorLeft MuiDrawer-paperAnchorDockedLeft css-1ylinub-Sidebar-drawerPaper&quot;]/div[@class=&quot;CustomMenu css-i0zwen-CustomMenu-menuWrapper css-1lnodjf-CustomMenu-menuOpen&quot;]/div[@class=&quot;css-16ny2p0&quot;]/div[@class=&quot;css-11uhf22&quot;]/span[1]/span[@class=&quot;AppButton AppButtonActive css-rj038m&quot;]/div[@class=&quot;css-wzf3e6&quot;]/svg[@class=&quot;MuiSvgIcon-root MuiSvgIcon-fontSizeMedium css-vubbuv&quot;]/path[1]</value>
      <webElementGuid>5b2d5608-6b9c-40a6-961f-33ffeacf3564</webElementGuid>
   </webElementProperties>
</WebElementEntity>
