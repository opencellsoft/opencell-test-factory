<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Products</name>
   <tag></tag>
   <elementGuidId>be09a484-afe6-44de-99de-5e1f6eebb744</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div/main/div/div/div/div[2]/div[2]/a[2]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:role=menuitem[name=&quot;Products&quot;i]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ea955995-2080-47e6-95c5-62be70ad323b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>menuLabel</value>
      <webElementGuid>b99b24e9-e7d2-4e99-8375-d57b6328a31e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Products</value>
      <webElementGuid>db1df36e-2d5f-468a-a0a4-f720c3c05b72</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;layout css-j2ivl7-LayoutWithoutTheme-root&quot;]/div[@class=&quot;css-rr5ovl-LayoutWithoutTheme-appFrame-appFrame&quot;]/main[@class=&quot;css-cz9pya-LayoutWithoutTheme-contentWithSidebar&quot;]/div[@class=&quot;MuiDrawer-root MuiDrawer-docked css-9r0ewe-Sidebar-wrapper&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-elevation MuiPaper-elevation0 MuiDrawer-paper MuiDrawer-paperAnchorLeft MuiDrawer-paperAnchorDockedLeft css-1ylinub-Sidebar-drawerPaper&quot;]/div[@class=&quot;CustomMenu MenuOpen css-i0zwen-CustomMenu-menuWrapper css-1lnodjf-CustomMenu-menuOpen css-1lnodjf-CustomMenu-menuOpen&quot;]/div[@class=&quot;css-uhicyb-CustomMenu-relativeWrapper&quot;]/div[@class=&quot;MenuItemsContainer css-pfr6r8-CustomMenu-scrollWrapper&quot;]/a[@class=&quot;MuiButtonBase-root MuiMenuItem-root MuiMenuItem-gutters MuiMenuItem-root MuiMenuItem-gutters css-1aitmf1&quot;]/span[@class=&quot;menuLabel&quot;]</value>
      <webElementGuid>06238e73-6cc1-48db-b88e-5a054bbb3888</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/main/div/div/div/div[2]/div[2]/a[2]/span</value>
      <webElementGuid>3e94bf7f-59cc-4dc4-8d33-43250dd7f608</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Commercial offers'])[2]/following::span[2]</value>
      <webElementGuid>16e82510-803c-4891-a695-450dce147773</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Commercial offers'])[1]/following::span[18]</value>
      <webElementGuid>0196f4dd-d40a-4cac-a655-db188b8a9e13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Price lists'])[1]/preceding::span[4]</value>
      <webElementGuid>58844039-4aa4-4e5f-ac4d-ea81b4289295</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Discount plans'])[1]/preceding::span[6]</value>
      <webElementGuid>ea951485-f92e-4362-94a7-e10ad8ca96d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Products']/parent::*</value>
      <webElementGuid>141fa247-285b-40db-9b35-c259d21b0cf7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[2]/span</value>
      <webElementGuid>07b51864-7ccb-4f97-b3fb-cfdef2a1650b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Products' or . = 'Products')]</value>
      <webElementGuid>5ac3c0ef-0b8c-4ba7-acb2-044398f3f5a7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
