<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Action executed successfuly</name>
   <tag></tag>
   <elementGuidId>2c800274-7406-4811-b626-50189b8e1196</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.MuiAlert-message.css-1xsto0d</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[2]/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:text=&quot;Action executed successfuly&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>546480f3-97cf-4661-9d3c-b6a727867d89</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiAlert-message css-1xsto0d</value>
      <webElementGuid>48a68e74-9713-4878-babe-ba4cbd416529</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Action executed successfuly</value>
      <webElementGuid>7dfe9951-2506-43f8-8f0c-5ceb2fe7579c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;MuiSnackbar-root MuiSnackbar-anchorOriginTopRight css-3sz7wh&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-elevation MuiPaper-rounded MuiPaper-elevation0 MuiAlert-root MuiAlert-colorInfo MuiAlert-standardInfo MuiAlert-standard css-rdbnqa&quot;]/div[@class=&quot;MuiAlert-message css-1xsto0d&quot;]</value>
      <webElementGuid>c92d2e1b-1989-49d9-a137-aaef4beb8c75</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div/div[2]</value>
      <webElementGuid>214a6576-da39-4c89-ac26-6905010e4cda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Created by opencell.admin'])[1]/following::div[8]</value>
      <webElementGuid>54d2f701-1165-4fd3-b58e-1f6c5b338643</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Activity'])[1]/following::div[14]</value>
      <webElementGuid>c8edd946-08df-4969-9e1e-aa168ff23fbb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Opencell, your favorite billing platform'])[1]/preceding::div[2]</value>
      <webElementGuid>d34f4459-7da6-487b-a928-023265113949</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Action executed successfuly']/parent::*</value>
      <webElementGuid>4610c8a0-5426-4f6f-b4f6-f859b4104bed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]</value>
      <webElementGuid>f4bfa93d-fd8c-4b05-b1a9-e7a7f2a607ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Action executed successfuly' or . = 'Action executed successfuly')]</value>
      <webElementGuid>7d08925f-1841-42d1-980e-c7142c4bad6d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
