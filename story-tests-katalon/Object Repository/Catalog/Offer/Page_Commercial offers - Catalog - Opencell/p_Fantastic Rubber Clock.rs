<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Fantastic Rubber Clock</name>
   <tag></tag>
   <elementGuidId>2516c28b-afef-4856-a100-3d7651d22bb2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.MuiTypography-root.MuiTypography-body1.css-1es736p > p</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='cell-name-1200']/span/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:label=&quot;Fantastic Rubber Clock&quot;i >> internal:text=&quot;Fantastic Rubber Clock&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>e79a07f5-3c63-4fdf-a3fa-4916acb64cd1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Fantastic Rubber Clock</value>
      <webElementGuid>b20d69e4-d980-4052-b766-d80ba4ce9993</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cell-name-1200&quot;)/span[@class=&quot;MuiTypography-root MuiTypography-body1 css-1es736p&quot;]/p[1]</value>
      <webElementGuid>5f73ea06-d93b-4553-af12-7045c30efecc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='cell-name-1200']/span/p</value>
      <webElementGuid>926699a6-228d-4efb-a614-52e52df10713</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[1]/following::p[1]</value>
      <webElementGuid>bcdd3551-ae0f-4868-bbe3-f8d1e1ae1e95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Modified'])[1]/following::p[1]</value>
      <webElementGuid>46b4a2cb-70a6-4c28-943d-742c6e9eb7ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Draft'])[1]/preceding::p[5]</value>
      <webElementGuid>666eeb79-449f-416a-89a6-5a7b05f70658</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Page Size:'])[1]/preceding::p[5]</value>
      <webElementGuid>55cac7f6-4314-4278-9e24-b8eeaa12ba5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Fantastic Rubber Clock']/parent::*</value>
      <webElementGuid>9c7a33b7-8e5d-454c-80e4-e2537f0f9256</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/p</value>
      <webElementGuid>f806cbcb-9d78-47af-8eea-e46981eeb6be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Fantastic Rubber Clock' or . = 'Fantastic Rubber Clock')]</value>
      <webElementGuid>b7f51e88-14fe-499a-b299-14241057dd4c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
