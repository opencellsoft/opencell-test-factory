<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Small Cotton Knife</name>
   <tag></tag>
   <elementGuidId>af10714f-994e-4150-b726-c797befe1670</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#cell-description-1241 > span.MuiTypography-root.MuiTypography-body1.css-1es736p > p</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='cell-description-1241']/span/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:label=&quot;Add product&quot;i >> internal:text=&quot;Small Cotton Knife&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>11ae9a8f-c6e7-445a-bccf-6e4f5dc1ef00</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Small Cotton Knife</value>
      <webElementGuid>ac1ac764-ff6f-46d9-8e34-4849495807a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cell-description-1241&quot;)/span[@class=&quot;MuiTypography-root MuiTypography-body1 css-1es736p&quot;]/p[1]</value>
      <webElementGuid>255b525b-5457-4a8f-846e-eab4aef515ff</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//span[@id='cell-description-1241']/span/p</value>
      <webElementGuid>bf85cc75-8013-4e38-9592-db15e09c091c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Family'])[2]/following::p[1]</value>
      <webElementGuid>e74e4344-118e-4dad-914a-bb942c25f622</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Code'])[2]/following::p[1]</value>
      <webElementGuid>7bb5b4e7-2dda-4dbf-a116-9084f2ad12ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='to'])[2]/preceding::p[3]</value>
      <webElementGuid>439802f8-017a-4498-b2e8-8e0adc5c4317</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='of'])[3]/preceding::p[3]</value>
      <webElementGuid>9f2e8805-bb8e-4ad4-ba1d-093f02ef8385</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Small Cotton Knife']/parent::*</value>
      <webElementGuid>6081a7ef-59c6-4fa8-8244-d21f9574cdfc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/span/p</value>
      <webElementGuid>78c766fe-b042-4bc3-9855-013e3f960dba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Small Cotton Knife' or . = 'Small Cotton Knife')]</value>
      <webElementGuid>61b6fc57-0778-467d-bc62-09345abce6da</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
