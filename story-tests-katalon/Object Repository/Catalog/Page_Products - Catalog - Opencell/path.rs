<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path</name>
   <tag></tag>
   <elementGuidId>c2d8c66b-b5bc-4b85-9055-506664d0796d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.MuiButtonBase-root.MuiButton-root.MuiButton-text.MuiButton-textSecondary.MuiButton-sizeSmall.MuiButton-textSizeSmall.MuiButton-colorSecondary.MuiButton-disableElevation.MuiButton-root.MuiButton-text.MuiButton-textSecondary.MuiButton-sizeSmall.MuiButton-textSizeSmall.MuiButton-colorSecondary.MuiButton-disableElevation.css-1j44f4v > svg.MuiSvgIcon-root.MuiSvgIcon-fontSizeMedium.css-z72lng-CustomActionItem-icon > path</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;main-content&quot;)/div[@class=&quot;css-1yp6wxb-Layoutusd1-wrapper&quot;]/div[@class=&quot;css-1apg2cy-Layoutusd1-flex&quot;]/div[@class=&quot;css-t3won0-Layoutusd1-main&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-elevation MuiPaper-rounded MuiPaper-elevation1 MuiCard-root css-tshp2j&quot;]/div[@id=&quot;CPQ/products&quot;]/div[@class=&quot;css-1t8jh3g-ListView-mainWrapper&quot;]/div[2]/div[@class=&quot;MuiToolbar-root MuiToolbar-gutters MuiToolbar-regular css-1f5chrt&quot;]/div[@class=&quot;MuiToolbar-root MuiToolbar-gutters MuiToolbar-regular CardActions list-actions css-1irxnda&quot;]/div[2]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-sizeSmall MuiButton-textSizeSmall MuiButton-colorSecondary MuiButton-disableElevation MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-sizeSmall MuiButton-textSizeSmall MuiButton-colorSecondary MuiButton-disableElevation css-1j44f4v&quot;]/svg[@class=&quot;MuiSvgIcon-root MuiSvgIcon-fontSizeMedium css-z72lng-CustomActionItem-icon&quot;]/path[1]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:testid=[testId=&quot;CustomActions-CPQ/products-CustomActionItem-0&quot;s]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
      <webElementGuid>86a45cdf-405b-4257-9470-e2b9e8cf0607</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6z</value>
      <webElementGuid>95681d42-2911-447d-96a9-8fae95edcaa1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content&quot;)/div[@class=&quot;css-1yp6wxb-Layoutusd1-wrapper&quot;]/div[@class=&quot;css-1apg2cy-Layoutusd1-flex&quot;]/div[@class=&quot;css-t3won0-Layoutusd1-main&quot;]/div[@class=&quot;MuiPaper-root MuiPaper-elevation MuiPaper-rounded MuiPaper-elevation1 MuiCard-root css-tshp2j&quot;]/div[@id=&quot;CPQ/products&quot;]/div[@class=&quot;css-1t8jh3g-ListView-mainWrapper&quot;]/div[2]/div[@class=&quot;MuiToolbar-root MuiToolbar-gutters MuiToolbar-regular css-1f5chrt&quot;]/div[@class=&quot;MuiToolbar-root MuiToolbar-gutters MuiToolbar-regular CardActions list-actions css-1irxnda&quot;]/div[2]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-sizeSmall MuiButton-textSizeSmall MuiButton-colorSecondary MuiButton-disableElevation MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-sizeSmall MuiButton-textSizeSmall MuiButton-colorSecondary MuiButton-disableElevation css-1j44f4v&quot;]/svg[@class=&quot;MuiSvgIcon-root MuiSvgIcon-fontSizeMedium css-z72lng-CustomActionItem-icon&quot;]/path[1]</value>
      <webElementGuid>0a37e5f1-6fc8-48ae-bd5e-0ab81dc4cd41</webElementGuid>
   </webElementProperties>
</WebElementEntity>
