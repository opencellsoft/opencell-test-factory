<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Catalog_AppButton AppButtonActive css-18f_dc5b2e</name>
   <tag></tag>
   <elementGuidId>5fbd55cf-6fd3-4223-a35d-cee60b97e46d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@href = '/opencell/frontend/DEMO/portal/CPQ']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-content']/div/div/div/div/div/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.AppButton.AppButtonActive.css-18f28jg-MenuItem-wrapper.css-1ihjezu-MenuItem-active</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>div >> internal:has-text=/^Catalog$/ >> internal:role=link</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>ca097476-1f26-40a6-908c-b1a692acee83</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>AppButton AppButtonActive css-18f28jg-MenuItem-wrapper css-1ihjezu-MenuItem-active</value>
      <webElementGuid>01d68e6c-8dbd-494b-b308-5388aeb7d186</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/opencell/frontend/DEMO/portal/CPQ</value>
      <webElementGuid>8fbf766a-7ba3-4d82-9e36-d7f976e40258</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content&quot;)/div[@class=&quot;home-page&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;menu&quot;]/div[@class=&quot;menu-hold&quot;]/div[@class=&quot;menu-item&quot;]/a[@class=&quot;AppButton AppButtonActive css-18f28jg-MenuItem-wrapper css-1ihjezu-MenuItem-active&quot;]</value>
      <webElementGuid>b885596f-4037-4aa4-aec2-c1c5220dc479</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-content']/div/div/div/div/div/a</value>
      <webElementGuid>494ddc32-5ccc-4f11-a5db-a4deb9684396</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Catalog'])[1]//a[1]</value>
      <webElementGuid>60518015-8df5-4b2b-b48d-c30794aaa81f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Customer Care'])[1]/preceding::a[1]</value>
      <webElementGuid>b3f059dd-14b8-444c-ae2a-482d947381ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Finance'])[1]/preceding::a[2]</value>
      <webElementGuid>aa355c3f-1f7b-47c8-8ca6-0b38293c8125</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/opencell/frontend/DEMO/portal/CPQ')]</value>
      <webElementGuid>2cf4252a-f44e-48fa-b8af-6050d476f832</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a</value>
      <webElementGuid>05e22adc-2dbe-4bca-8bae-0f368c8915ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/opencell/frontend/DEMO/portal/CPQ']</value>
      <webElementGuid>35902b20-206d-4c3b-a9be-45631e693b19</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
