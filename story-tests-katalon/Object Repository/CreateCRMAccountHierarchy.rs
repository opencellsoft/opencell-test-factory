<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>CreateCRMAccountHierarchy</name>
   <tag></tag>
   <elementGuidId>63ccba4a-9fa8-4d46-9120-4fb225a0eaab</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>true</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;crmAccountType\&quot;: \&quot;C_UA\&quot;,\n    \&quot;crmParentCode\&quot;: \&quot;${crmParentCode}\&quot;,\n    \&quot;code\&quot;: \&quot;${code}\&quot;,\n    \&quot;seller\&quot;: \&quot;MAIN_SELLER\&quot;,\n    \&quot;description\&quot;: \&quot;${description}\&quot;,\n    \&quot;language\&quot;: \&quot;FRA\&quot;,\n    \&quot;currency\&quot;: \&quot;EUR\&quot;,\n    \&quot;billingCycle\&quot;: \&quot;CYC_INV_MT_1\&quot;,\n    \&quot;country\&quot;: \&quot;FR\&quot;,\n    \&quot;customerCategory\&quot;: \&quot;CLIENT\&quot;,\n    \&quot;taxCategoryCode\&quot;: \&quot;REGULAR\&quot;,\n    \&quot;creditCategory\&quot;: \&quot;NEWCUSTOMER\&quot;,\n    \&quot;jobTitle\&quot;: \&quot;${jobTitle}\&quot;,\n    \&quot;isCompany\&quot;: true,\n    \&quot;electronicBilling\&quot;: \&quot;true\&quot;,\n    \&quot;email\&quot;: \&quot;${email}\&quot;,\n    \&quot;ccedEmails\&quot;: \&quot;${ccedEmails}\&quot;,\n    \&quot;mailingType\&quot;: \&quot;MANUAL\&quot;,\n    \&quot;emailTemplate\&quot;: \&quot;FR-Invoice\&quot;,\n    \&quot;legalEntityType\&quot;: {\n        \&quot;code\&quot;: \&quot;Company\&quot;\n    },\n    \&quot;name\&quot;: {\n        \&quot;title\&quot;: \&quot;MR\&quot;,\n        \&quot;firstName\&quot;: \&quot;${firstName}\&quot;,\n        \&quot;lastName\&quot;: \&quot;${lastName}\&quot;\n    },\n    \&quot;address\&quot;: {\n        \&quot;address1\&quot;: \&quot;${address1}\&quot;,\n        \&quot;address2\&quot;: \&quot;\&quot;,\n        \&quot;address3\&quot;: \&quot;\&quot;,\n        \&quot;zipCode\&quot;: \&quot;${zipCode}\&quot;,\n        \&quot;city\&quot;: \&quot;${city}\&quot;,\n        \&quot;country\&quot;: \&quot;FR\&quot;\n    },\n    \&quot;contactInformation\&quot;: {\n        \&quot;phone\&quot;: \&quot;${phone}\&quot;,\n        \&quot;mobile\&quot;: \&quot;${mobile}\&quot;\n    },\n    \&quot;registrationNumbers\&quot;: [\n        {\n            \&quot;isoIcdCode\&quot;: \&quot;0002\&quot;,\n            \&quot;registrationNo\&quot;: \&quot;362 521 879 00034\&quot;\n        }\n    ]\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>daf7e960-e167-4e48-9d03-912cbc10fa55</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Basic b3BlbmNlbGwuc3VwZXJhZG1pbjpvcGVuY2VsbC5zdXBlcmFkbWlu</value>
      <webElementGuid>4aa9eef9-c79b-4fa7-8705-f050b00685a4</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>10.1.0</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <path></path>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.apiBaseUrl}/account/accountHierarchy/createCRMAccountHierarchy</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>5f13eca8-4965-4427-91a1-8c598622baa4</id>
      <masked>false</masked>
      <name>crmParentCode</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>99691b35-0027-4712-bbe3-a23d4e136af0</id>
      <masked>false</masked>
      <name>code</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>ad89ccf0-5966-4e2a-8240-05934ed5aefc</id>
      <masked>false</masked>
      <name>description</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>0b9fa11d-ec17-4141-bea2-a40ea48e5c38</id>
      <masked>false</masked>
      <name>jobTitle</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>aad4d065-0ab6-4d37-bb50-4e59c17847f3</id>
      <masked>false</masked>
      <name>email</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>41609d35-ac8b-4559-b2a9-635821cc9254</id>
      <masked>false</masked>
      <name>ccedEmails</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>81a0cce3-d7e7-4656-aa5c-24fa7c0cc6f5</id>
      <masked>false</masked>
      <name>firstName</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>467299dd-24a2-46ed-b007-fdb7662de4dc</id>
      <masked>false</masked>
      <name>lastName</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>5b0cad40-e9e2-4b21-b627-8d59dce07427</id>
      <masked>false</masked>
      <name>address1</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>13afbcc2-ab5e-4284-a7ff-e94bd96e5021</id>
      <masked>false</masked>
      <name>zipCode</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>534032a9-a85b-43d6-9962-93f7e36d6ff6</id>
      <masked>false</masked>
      <name>city</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>0addeebf-aad1-4b66-b827-c41203fa8a14</id>
      <masked>false</masked>
      <name>phone</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>803326d4-fc73-44f8-8813-84a402b3d19a</id>
      <masked>false</masked>
      <name>mobile</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
