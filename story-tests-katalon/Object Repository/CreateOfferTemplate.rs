<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>CreateOfferTemplate</name>
   <tag></tag>
   <elementGuidId>6c04c22d-a405-477c-9a48-5ad5ded4ca2f</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>true</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\&quot;name\&quot;:\&quot;${offerName}\&quot;,\&quot;attributes\&quot;:[],\&quot;offerAttributes\&quot;:[],\&quot;commercialRuleCodes\&quot;:[],\&quot;mediaCodes\&quot;:[],\&quot;offerProducts\&quot;:[],\&quot;tagCodes\&quot;:[],\&quot;newValidFrom\&quot;:null,\&quot;newValidTo\&quot;:null,\&quot;customFields\&quot;:{},\&quot;seller\&quot;:[],\&quot;channel\&quot;:[],\&quot;allowedDiscountPlans\&quot;:[],\&quot;allowedOfferChange\&quot;:[],\&quot;isOfferChangeRestricted\&quot;:false,\&quot;autoEndOfEngagement\&quot;:false,\&quot;renewalRule\&quot;:{\&quot;initialTermType\&quot;:\&quot;RECURRING\&quot;,\&quot;initialyActiveForUnit\&quot;:\&quot;MONTH\&quot;,\&quot;autoRenew\&quot;:true,\&quot;extendAgreementPeriodToSubscribedTillDate\&quot;:true,\&quot;endOfTermAction\&quot;:\&quot;SUSPEND\&quot;,\&quot;renewalTermType\&quot;:\&quot;RECURRING\&quot;},\&quot;offerTemplateCategory\&quot;:[]}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Accept</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>eb4c5b39-7b77-40c9-812d-72b9454c8952</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Basic b3BlbmNlbGwuYWRtaW46b3BlbmNlbGwuYWRtaW4=</value>
      <webElementGuid>43254aeb-3ff9-420d-9832-1445858b0a8d</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>daf7e960-e167-4e48-9d03-912cbc10fa55</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>10.1.0</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <path></path>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.apiBaseUrl}/catalog/offerTemplate</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>b6fc86aa-e110-40c7-a017-7344c466a732</id>
      <masked>false</masked>
      <name>offerName</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
