package com.opencellsoft.tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.catalog.tags.TagsListPage;
import com.opencellsoft.pages.catalog.tags.TagsPage;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.offers.ListOfOffersPage;
import com.opencellsoft.utility.Constant;

public class TagsTests extends TestBase {
	TagsListPage tagsListPage;
	TagsPage tagsPage;
	public void createTag(String tag_code,String tag_name,String tag_category,String tag_seller,String tag_parent) {

		tagsListPage = new TagsListPage();
		tagsListPage.clickOnCreateButton();
		waitPageLoaded();
		tagsPage = new TagsPage();
		tagsPage.setCode(tag_code);
		tagsPage.setName(tag_name);
		tagsPage.selectCategory(tag_category);
		tagsPage.selectSeller(tag_seller);
		tagsPage.selectParent(tag_parent);
		tagsPage.clickOnSaveButton();
	}
	
	
	public void updateTag(String tag_name, String tag_category, String tag_seller, String tag_parent) {
		tagsPage = new TagsPage();
		if(tag_name != null) {tagsPage.setName(tag_name);}
		if(tag_category != null) {tagsPage.selectCategory(tag_category);}
		if(tag_seller != null) {tagsPage.selectSeller(tag_seller);}
		if(tag_parent != null) {tagsPage.selectParent(tag_parent);}
		tagsPage.clickOnSaveButton();
	}
	
	public void delete() {
		tagsPage = new TagsPage();
		tagsPage.clickOnDeleteButton();
		tagsPage.clickOnConfirmButton();
	}
	
	public void goBack() {
		tagsPage = new TagsPage();
		tagsPage.clickOnGoBackButton();
	}
	
	public void searchTagsByName(String value) {
		tagsListPage = new TagsListPage();
		tagsListPage.setSearchInput(value);
	}
	
	public int tagsTotal() {
		tagsListPage = new TagsListPage();
		return tagsListPage.tagsList().size();
	}
	
	public void goToTagsDetails (String email) {
		tagsListPage = new TagsListPage();
		tagsListPage.goToTagsDetails(email);
	}
	
	public void activateFilterBy(String value1, String value2) {
		tagsListPage = new TagsListPage();
		tagsListPage.clickOnFilterButton();
		try {
			tagsListPage.activateFilterBy(value1);
		}catch (Exception e) {
			tagsListPage.activateFilterBy(value2);
		}
	}
	
	public void filterByTagsName(String value) {
		tagsListPage = new TagsListPage();
		tagsListPage.setFilterByTagsName(value);
	}
	
	public void sortTagsListBy(String value) {
		tagsListPage = new TagsListPage();
		tagsListPage.sortBy(value);
	}
	
	public void closeFilterBy(String value) {
		tagsListPage = new TagsListPage();
		tagsListPage.clickOnRemoveFilterBy(value);
	}
	
	///////////////////////////////////////////////////////////	
	
	@Test(priority=1)
	public void createTag() {

		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCatalogButton();
		
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnTagsButton();
		
		TagsListPage listOfTagsPage = PageFactory.initElements(driver, TagsListPage.class);
		listOfTagsPage.clickOnCreateButton();

		TagsPage newTagPage = PageFactory.initElements(driver, TagsPage.class);
		newTagPage.setCode(Constant.tagCode);
		newTagPage.setName(Constant.tagName);
		newTagPage.clickOnSaveButton();
	}
	
	@Parameters("capability")
	@Test(priority=2)
	public void searchForTagsWithDefaultFilter(String capability) {
	
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCatalogButton();
		
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnTagsButton();
		
		TagsListPage listOfTagsPage = PageFactory.initElements(driver, TagsListPage.class);
		listOfTagsPage.setSearchInput(capability);
	}
	
	@Parameters("tagCode")
	@Test(priority=3)
	public void searchForTagsWithAdvancedFilters(String tagCode) {
		
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCatalogButton();
		
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnTagsButton();
		
		TagsListPage listOfTagsPage = PageFactory.initElements(driver, TagsListPage.class);
		listOfTagsPage.setSearchInput(tagCode);
	}
}
