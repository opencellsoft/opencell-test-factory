package com.opencellsoft.tests;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.finance.reports.TrialBalancePage;

public class TrialBalanceTests extends TestBase {

	TrialBalancePage trialBalancePage;
	
	public void filterByPeriod(String value) {
		trialBalancePage = new TrialBalancePage();
		trialBalancePage.clickOnFilterByPeriodInput();
		trialBalancePage.clickOnPeriod(value);
	}
	
	public void export(String value) {
		trialBalancePage = new TrialBalancePage();
		trialBalancePage.clickOnDownloadSelectionButton();
		trialBalancePage.clickOnExportFormat(value);
	}
	
	public Boolean alertMsg() {
		return trialBalancePage.isAlertMsgDisplayed();
	}
}
