package com.opencellsoft.tests;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.charges.ChargePage;
import com.opencellsoft.pages.charges.ListOfChargesPage;
import com.opencellsoft.pages.charges.NewChargePage;
import com.opencellsoft.pages.charges.NewPriceVersionpage;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.offers.ListOfOffersPage;
import com.opencellsoft.pages.priceversions.NewPriceVersionPage;
import com.opencellsoft.pages.priceversions.PriceVersionPage;
import com.opencellsoft.pages.products.ListOfProductsPage;
import com.opencellsoft.pages.products.ProductPage;
import com.opencellsoft.utility.Constant;

public class ChargesTests extends TestBase {
	@Test(priority = 12)
	public void publishProductVersion() throws InterruptedException {
		//		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		//		homePage.clickOnCatalogButton();
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnProductsButton();
		ListOfProductsPage listOfProductsPage = PageFactory.initElements(driver, ListOfProductsPage.class);
		listOfProductsPage.clickOnFilterButton();
		listOfProductsPage.clickOnFilterByProductCodeFilter();
		listOfProductsPage.setProductCode(Constant.productCode);
		Thread.sleep(2000);
		listOfProductsPage.selectVisibleFoundProduct(Constant.productCode);
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		//		productPage.clickOnGeneralTab();
		//		productPage.clickOnProductVersionEndDateInput();
		//		productPage.setProductVersionEndDate();
		productPage.clickOnSaveButton();
		Assert.assertTrue(productPage.getTextMessage().toLowerCase().contains("element updated"));
		productPage.clickOnPublishProductVersionButton();
		//		Assert.assertEquals(productPage.getTextMessage(), "Element updated");
		//		Assert.assertEquals(productPage.getProductCodeInputText(), Constant.productCode);

	}
	@Test(priority = 13)
	public void productStatusFromDraftToActivated() throws InterruptedException {
		//		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		//		homePage.clickOnCatalogButton();
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnProductsButton();
		ListOfProductsPage listOfProductsPage = PageFactory.initElements(driver, ListOfProductsPage.class);
		listOfProductsPage.clickOnFilterButton();
		listOfProductsPage.clickOnFilterByProductCodeFilter();
		listOfProductsPage.setProductCode(Constant.productCode);
		listOfProductsPage.selectVisibleFoundProduct(Constant.productCode);
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.activateProduct();
		//		productPage.clickOnGeneralTab();
		//		Assert.assertEquals(productPage.getStatusText(), Constant.productCode);
		//		Assert.assertEquals(productPage.getStatusText(), Constant.productCode);
		Assert.assertEquals(productPage.getTextMessage(), "Action executed successfuly");
	}

	@Parameters({ "tarifTypeOneShot"})
	@Test(priority = 3, groups = { "smoke tests", "administration tests" })
	public void addNewOneshotCharge(String tarifTypeOneShot) throws Exception {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		//		productPage.clickOnChargeProduct();
		productPage.clickOnPricingButton();
		productPage.clickOnCreatePricingButton();
		NewChargePage newChargePage = PageFactory.initElements(driver, NewChargePage.class);
		waitPageLoaded();
		newChargePage.setChargeCode(Constant.oneShotChargeCode);
		newChargePage.setChargeDescription(Constant.oneShotChargeDescription);
		newChargePage.selectChargeType("One shot");
		newChargePage.selectOneShotTarifType(tarifTypeOneShot);
		waitPageLoaded();
		newChargePage.clickOnSaveChargeButton();
//		Thread.sleep(5000);
		ChargePage chargePage = PageFactory.initElements(driver, ChargePage.class);
		Assert.assertEquals(newChargePage.getTextMessage(), "Element created");
		Assert.assertEquals(chargePage.getChargeCodeInputValue(), Constant.oneShotChargeCode);
		Assert.assertEquals(chargePage.getChargeDescriptionInputValue(), Constant.oneShotChargeDescription);
	}

	@Test(priority = 4, groups = { "administration tests" })
	public void goBackToProductPage() {
		ChargePage chargePage = PageFactory.initElements(driver, ChargePage.class);
		chargePage.clickOnGoBackButton();
		//		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
	}

	@Parameters({ "priceVersionUnitPrice" })
	@Test(priority = 4, groups = { "smoke tests" })
	public void createNewPricePlanVersion(String priceVersionUnitPrice) throws InterruptedException {
		ChargePage chargePage = PageFactory.initElements(driver, ChargePage.class);
		chargePage.clickOnCreatePriceVersionButton();
		NewPriceVersionpage newPriceVersionPage = PageFactory.initElements(driver, NewPriceVersionpage.class);
		waitPageLoaded();
		newPriceVersionPage.setPriceVersionCode(Constant.pricePlanVersionCode);
		waitPageLoaded();
		newPriceVersionPage.setPriceVersionStartDate();
		waitPageLoaded();
		newPriceVersionPage.setPriceVersionUnitPrice(priceVersionUnitPrice);
		newPriceVersionPage.clickOnSavePriceVersionButton();
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		waitPageLoaded();
		Assert.assertEquals(priceVersionPage.getTextMessage(), "Element created");
		Assert.assertEquals(priceVersionPage.getPriceVersionLabelValue(), Constant.pricePlanVersionCode);
	}

	@Test(priority = 5, groups = { "smoke tests" })
	public void publishPricePlanVersion() throws InterruptedException {
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		priceVersionPage.clickOnPublishPriceVersionButton();
		Assert.assertEquals(priceVersionPage.getTextMessage(), "Action executed successfuly");
	}

	@Test(priority = 6, groups = { "smoke tests" })
	public void activateOneshotCharge() throws InterruptedException {
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		Thread.sleep(3000);
		priceVersionPage.clickOnGoBackButton();
		ChargePage chargePage = PageFactory.initElements(driver, ChargePage.class);
		Thread.sleep(2000);
		chargePage.clickOnActivateChargeButton();
		Assert.assertEquals(priceVersionPage.getTextMessage(), "Action executed successfuly");
		Thread.sleep(1000);
		chargePage.clickOnGoBackButton();
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnSaveButton();
	}

	@Test(priority = 3, groups = { "administrationTests" })
	public void saveProductPage() throws InterruptedException {
		ChargePage chargePage = PageFactory.initElements(driver, ChargePage.class);
		chargePage.clickOnGoBackButton();
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnSaveButton();
		Assert.assertTrue(productPage.getTextMessage().toLowerCase().contains("element updated"));
		productPage.clickOnChargeProduct();
		Thread.sleep(1000);
		productPage.clickOnCharge();
	}

	@Parameters({ "repetitionCalendarRecurringWithPriceGrid" })
	@Test(priority = 7, groups = { "smoke tests" })
	public void addNewRecurringChargePriceGrid(String repetitionCalendarRecurringWithPriceGrid)
			throws InterruptedException {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnPricingButton();
		Thread.sleep(1000);
		productPage.clickOnCreatePricingButton();
		NewChargePage newChargePage = PageFactory.initElements(driver, NewChargePage.class);
		Thread.sleep(1000);
		newChargePage.setChargeCode(Constant.recurringChargeCode);
		newChargePage.setChargeDescription(Constant.chargeDescriptionRecurring);
		newChargePage.selectChargeType("Recurring");
		newChargePage.selectRepetitionCalendar(repetitionCalendarRecurringWithPriceGrid);
		newChargePage.activateSubscriptionProrata();
		newChargePage.activateTerminationProrata();
		newChargePage.activateApplyInAdvance();
		newChargePage.clickOnSaveChargeButton();
//		Thread.sleep(4000);
		ChargePage chargePage = PageFactory.initElements(driver, ChargePage.class);
		Assert.assertEquals(chargePage.getTextMessage(), "Element created");
		Assert.assertEquals(chargePage.getChargeCodeInputValue(), Constant.recurringChargeCode);
		Assert.assertEquals(chargePage.getChargeDescriptionInputValue(), Constant.chargeDescriptionRecurring);
	}

	@Test(priority = 8, groups = { "smoke tests" })
	public void createNewPricePlanMatrixVersion() throws InterruptedException {
		ChargePage chargePage = PageFactory.initElements(driver, ChargePage.class);
		Thread.sleep(4000);
		chargePage.clickOnCreatePriceVersionButton();
		Thread.sleep(2000);
		NewPriceVersionPage newPriceVersionPage = PageFactory.initElements(driver, NewPriceVersionPage.class);
		newPriceVersionPage.setPriceVersionLabel(Constant.pricePlanMatrixVersionCode);
		newPriceVersionPage.clickOnPriceGridButton();
		newPriceVersionPage.selectCalendar();
		Thread.sleep(2000);
		newPriceVersionPage.clickOnSaveMatrixPriceVersionButton();
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		Assert.assertEquals(priceVersionPage.getTextMessage(), "Element created");
		Assert.assertEquals(priceVersionPage.getMatrixPriceVersionLabelValue(), Constant.pricePlanMatrixVersionCode);
	}

	@Parameters({ "unitPrice1", "unitPrice2" })
	@Test(priority = 9, groups = { "smoke tests" })
	public void addGridDimensionstoPriceGrid(String unitPrice1, String unitPrice2) throws InterruptedException {
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		Thread.sleep(5000);
		priceVersionPage.clickOnAddAttributeButton();
		Thread.sleep(1000);
		priceVersionPage.clickOnAttributeInput();
		Thread.sleep(1000);
		priceVersionPage.setAttributeCode(Constant.ListOfTextValuesAttributeCode);
		priceVersionPage.clickOnAttribute(Constant.ListOfTextValuesAttributeCode);
		priceVersionPage.clickOnSaveAttributeGridButton();
		Thread.sleep(1000);
		priceVersionPage.selectFirstAttributeProperty();
		Thread.sleep(1000);
		//priceVersionPage.setFirstDescription(Constant.gridLineDescription1);
		priceVersionPage.setFirstUnitPrice(unitPrice1);
		Thread.sleep(2000);
		priceVersionPage.selectSecondAttributeProperty();
		Thread.sleep(2000);
		priceVersionPage.setSecondDescription(Constant.gridLineDescription2);
		priceVersionPage.setSecondUnitPrice(unitPrice2);
		Thread.sleep(2000);
		priceVersionPage.clickOnSaveMatrixPriceVersionButton();
		//Assert.assertEquals(priceVersionPage.getTextMessage(), "Element updated");
	}

	@Parameters({ "unitPrice1", "unitPrice2" })
	@Test(priority = 9, groups = { "smoke tests" })
	public void addGridDimensionsToPriceMatrix(String unitPrice1, String unitPrice2) throws InterruptedException {
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		priceVersionPage.clickOnAddAttributeButton();
		priceVersionPage.clickOnAttributeInput();
		priceVersionPage.setAttributeCode(Constant.ListOfTextValuesAttributeCode);
		priceVersionPage.clickOnAttribute(Constant.ListOfTextValuesAttributeCode);
		priceVersionPage.clickOnSaveAttributeGridButton();
		priceVersionPage.selectFirstAttributeProperty();
		priceVersionPage.setFirstDescription(Constant.gridLineDescription1);
		priceVersionPage.setFirstUnitPrice(unitPrice1);
		priceVersionPage.selectSecondAttributeProperty();
		priceVersionPage.setSecondDescription(Constant.gridLineDescription2);
		priceVersionPage.setSecondUnitPrice(unitPrice2);
		priceVersionPage.clickOnSaveMatrixPriceVersionButton();
		Assert.assertEquals(priceVersionPage.getTextMessage(), "Element updated");
	}

	@Parameters({ "buisnessAttributeCode" })
	@Test(priority = 10, groups = { "smoke tests" })
	public void addSubscriptionDateBusinessAttributeToPriceGrid(String buisnessAttributeCode) throws InterruptedException {
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		Thread.sleep(1000);
		priceVersionPage.clickOnAddBuisnessAttributeButton();
		Thread.sleep(1000);
		priceVersionPage.clickOnAttributeInput();
		priceVersionPage.clickOnBuisnessToggle();
		priceVersionPage.setAttributeCode(buisnessAttributeCode);
		priceVersionPage.clickOnAttribute(buisnessAttributeCode);
		priceVersionPage.clickOnSaveAttributeGridButton();
		Thread.sleep(100);
		priceVersionPage.setSubscriptionDateFirstDimension();
		priceVersionPage.setSubscriptionDateSecondDimension();
		priceVersionPage.clickOnSaveMatrixPriceVersionButton();	
	}
	@Parameters({ "buisnessAttributeCode","unitPrice1", "unitPrice2"})
	@Test(priority = 10, groups = { "smoke tests" })
	public void  createPriceGridWithTwoDimensions(String buisnessAttributeCode,String unitPrice1, String unitPrice2) throws InterruptedException {

		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		Thread.sleep(5000);
		priceVersionPage.clickOnAddAttributeButton();
		Thread.sleep(1000);
		priceVersionPage.clickOnAttributeInput();
		priceVersionPage.setAttributeCode(Constant.ListOfTextValuesAttributeCode);
		priceVersionPage.clickOnAttribute(Constant.ListOfTextValuesAttributeCode);
		priceVersionPage.clickOnSaveAttributeGridButton();
		Thread.sleep(1000);
		priceVersionPage.selectFirstAttributeProperty();
		Thread.sleep(1000);
		priceVersionPage.setFirstDescription(Constant.gridLineDescription1);
		priceVersionPage.setFirstUnitPrice(unitPrice1);
		priceVersionPage.selectSecondAttributeProperty();
		Thread.sleep(2000);
		priceVersionPage.setSecondDescription(Constant.gridLineDescription2);
		priceVersionPage.setSecondUnitPrice(unitPrice2);
		priceVersionPage.clickOnSavePriceGridButton();
		Assert.assertEquals(priceVersionPage.getTextMessage(), "Element updated");
		Thread.sleep(1000);
		priceVersionPage.clickOnAddBuisnessAttributeButton();
		Thread.sleep(1000);
		priceVersionPage.clickOnAttributeInput();
		priceVersionPage.clickOnBuisnessToggle();
		priceVersionPage.setAttributeCode(buisnessAttributeCode);
		priceVersionPage.clickOnAttribute(buisnessAttributeCode);
		priceVersionPage.clickOnSaveAttributeGridButton();
		Thread.sleep(100);
		priceVersionPage.setSubscriptionDateFirstDimension();
		Thread.sleep(100);
		priceVersionPage.setSubscriptionDateSecondDimension();
		priceVersionPage.clickOnSavePriceGridButton();
		Assert.assertEquals(priceVersionPage.getTextMessage(), "Element updated");

	}

	@Test(priority = 10, groups = { "smoke tests" }, dependsOnMethods = "addGridDimensionstoPriceGrid")
	public void publishPricePlanMatrixVersion() throws InterruptedException {
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		Thread.sleep(5000);
		priceVersionPage.clickOnPublishPriceVersionButton();
		Thread.sleep(5000);
		priceVersionPage.clickOnGoBackButton();
	}
	@Test(priority = 10)
	public void publishPricePlanMatrixVersionFromChargesList() throws InterruptedException {
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		Thread.sleep(5000);
		priceVersionPage.clickOnPublishPriceVersionButton();
		Thread.sleep(5000);
		priceVersionPage.clickOnGoBackButton();
	}

	@Test(priority = 11)
	public void activateRecurringChargeWithPriceGridFromChargesList() throws InterruptedException {
		NewChargePage newChargePage = PageFactory.initElements(driver, NewChargePage.class);
		newChargePage.clickOnActivateButton();
		Assert.assertEquals(newChargePage.getTextMessage(), "Action executed successfuly");
		ChargePage chargePage = PageFactory.initElements(driver, ChargePage.class);
		chargePage.clickOnGoBackButton();
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnSaveButton();
	}
	@Test(priority = 11, groups = { "smoke tests" })
	public void activateRecurringChargeWithPriceGrid() throws InterruptedException {
		NewChargePage newChargePage = PageFactory.initElements(driver, NewChargePage.class);
		waitPageLoaded();
		Thread.sleep(2000);
		newChargePage.clickOnActivateButton();
		Assert.assertEquals(newChargePage.getTextMessage(), "Action executed successfuly");
		ChargePage chargePage = PageFactory.initElements(driver, ChargePage.class);
		chargePage.clickOnGoBackButton();
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnSaveButton();
	}

	@Parameters({ "repetitionCalendar" })
	@Test(priority = 7, groups = { "administration tests" })
	public void addNewRecurringCharge(String repetitionCalendar) throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnChargesTab();
		ListOfChargesPage listChargesPage = PageFactory.initElements(driver, ListOfChargesPage.class);
		listChargesPage.clickOnCreateChargeButton();
		NewChargePage newChargePage = PageFactory.initElements(driver, NewChargePage.class);
		newChargePage.setChargeCode(Constant.recurringChargeCode);
		newChargePage.setChargeDescription(Constant.chargeDescriptionRecurring);
		newChargePage.selectRecurringChargeType();
		newChargePage.selectRepetitionCalendar(repetitionCalendar);
		newChargePage.activateSubscriptionProrata();
		newChargePage.activateTerminationProrata();
		newChargePage.activateApplyInAdvance();
		newChargePage.clickOnSaveChargeButton();
		//		ChargePage chargePage = PageFactory.initElements(driver, ChargePage.class);
	}

	/***  create Usage Charge from product page ***/
	public void createUsageCharge(String usageChargeCode, String chargeDescriptionUsage, String param1, String param2, String version) {	
		// Go to Pricing Tab
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnPricingButton();
		waitPageLoaded();

		//Go to add new charge page
		if(version.equals("16.X") || version.equals("17.X")) {
			productPage.clickOnCreatePricingButton1();
		}else {
			productPage.clickOnCreatePricingButton2();
		}
		
		NewChargePage newChargePage = PageFactory.initElements(driver, NewChargePage.class);
		waitPageLoaded();
		// Add new charge
		newChargePage.clearAndSetChargeCode(usageChargeCode);
		waitPageLoaded();
		newChargePage.setChargeDescription(chargeDescriptionUsage);
		newChargePage.selectUsageChargeType();
		waitPageLoaded();
		newChargePage.setParam1(param1);
		newChargePage.setParam2(param2);
		newChargePage.clearAndSetChargeCode(usageChargeCode);
		newChargePage.clickOnSaveChargeButton();
		waitPageLoaded();
	}
	/***  Update Usage Charge from product page ***/
	public void updateUsageChargeType() {	

		NewChargePage newChargePage = PageFactory.initElements(driver, NewChargePage.class);
		waitPageLoaded();	
		newChargePage.selectUsageChargeType();
		waitPageLoaded();
		newChargePage.clickOnSaveChargeButton();
		waitPageLoaded();
	}	
	/***  create One shot Charge  ***/
	public void createOneshotCharge(String code,String description, String tarifTypeOneShot, String version) {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnPricingButton();
		waitPageLoaded();

		//Go to add new charge page
		if(version.equals("17.X") || version.equals("16.X")) {
			productPage.clickOnCreatePricingButton1();
		}else {
			productPage.clickOnCreatePricingButton2();
		}
		NewChargePage newChargePage = PageFactory.initElements(driver, NewChargePage.class);

		// Add new charge
		newChargePage.setChargeCode(code);
		newChargePage.setChargeDescription(description);
		newChargePage.selectChargeType("One shot");
		waitPageLoaded();
		newChargePage.selectOneShotTarifType(tarifTypeOneShot);
		newChargePage.clickOnSaveChargeButton();
	}

	/***  create Recurring Charge  ***/
	public void createRecurringCharge(String code,String description, String repetitionCalendar, String version) throws InterruptedException {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnPricingButton();
		waitPageLoaded();

		//Go to add new charge page
		if(version.equals("17.X") || version.equals("16.X")) {
			productPage.clickOnCreatePricingButton1();
		}else {
			productPage.clickOnCreatePricingButton2();
		}
		NewChargePage newChargePage = PageFactory.initElements(driver, NewChargePage.class);
		waitPageLoaded();

		// Add new charge
		newChargePage.setChargeCode(code);
		newChargePage.setChargeDescription(description);
		newChargePage.selectChargeType("Recurring");
		newChargePage.selectRepetitionCalendar(repetitionCalendar);
		//newChargePage.activateSubscriptionProrata();
		//newChargePage.activateTerminationProrata();
		newChargePage.activateApplyInAdvance();
		newChargePage.clickOnSaveChargeButton();
	}
	/***  search For Charge  ***/
	public void searchForCharge(String chargeDescription) {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCatalogButton();
		waitPageLoaded();
		homePage.clickOnChargesTab();
		ListOfChargesPage listOfChargesPage = PageFactory.initElements(driver, ListOfChargesPage.class);
		waitPageLoaded();
		listOfChargesPage.setSearchBarText(chargeDescription);
		waitPageLoaded();
		listOfChargesPage.clickOnChargeFound(chargeDescription);
		waitPageLoaded();

	}

	/***  create New Price Version  ***/
	public void createNewPriceVersion(String pricePlanVersionCode, String unitPrice){
		ChargePage chargePage = PageFactory.initElements(driver, ChargePage.class);
		NewPriceVersionpage newPriceVersionPage = PageFactory.initElements(driver, NewPriceVersionpage.class);

		for(int i = 0; i< 3; i++) {
			chargePage.clickOnCreatePriceVersionButton();
			waitPageLoaded();
			newPriceVersionPage.setPriceVersionCode(pricePlanVersionCode);
			waitPageLoaded();
			newPriceVersionPage.setPriceVersionUnitPrice(unitPrice);
			waitPageLoaded();
			newPriceVersionPage.clickOnSavePriceVersionButton();
			waitPageLoaded();
			if(newPriceVersionPage.errorMessageIsDsplayed() == true) {
				newPriceVersionPage.clickOnGoBackButton();
				waitPageLoaded();
			}else {
				break;
			}
		}
	}

	/***  create New Price Version  ***/
	public void createNewPriceVersionInPresent(String pricePlanVersionCode, String unitPrice){
		ChargePage chargePage = PageFactory.initElements(driver, ChargePage.class);
		chargePage.clickOnCreatePriceVersionButton();
		NewPriceVersionpage newPriceVersionPage = PageFactory.initElements(driver, NewPriceVersionpage.class);
		waitPageLoaded();
		newPriceVersionPage.setPriceVersionCode(pricePlanVersionCode);
		newPriceVersionPage.setPriceVersionDateInPresent();
		newPriceVersionPage.setPriceVersionUnitPrice(unitPrice);
		waitPageLoaded();
		newPriceVersionPage.clickOnSavePriceVersionButton();
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);	
		waitPageLoaded();
		//Assert New Price Version is created
		Assert.assertEquals(priceVersionPage.getPriceVersionLabelValue(), pricePlanVersionCode);
	}
	/***  create New Price Version in futur ***/
	public void createNewPriceVersionInFutur(String pricePlanVersionCode, String unitPrice){
		ChargePage chargePage = PageFactory.initElements(driver, ChargePage.class);
		chargePage.clickOnCreatePriceVersionButton();
		NewPriceVersionpage newPriceVersionPage = PageFactory.initElements(driver, NewPriceVersionpage.class);
		waitPageLoaded();
		newPriceVersionPage.setPriceVersionCode(pricePlanVersionCode);
		newPriceVersionPage.setPriceVersionDateInFutur();
		newPriceVersionPage.setPriceVersionUnitPrice(unitPrice);
		waitPageLoaded();
		newPriceVersionPage.clickOnSavePriceVersionButton();
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);	
		waitPageLoaded();
		//Assert New Price Version is created
		Assert.assertEquals(priceVersionPage.getPriceVersionLabelValue(), pricePlanVersionCode);
	}
	/***  publish Price Version  ***/
	public void publishPriceVersion(){
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		priceVersionPage.clickOnPublishPriceVersionButton();
		waitPageLoaded();
		priceVersionPage.clickOnGoBackButton();
		waitPageLoaded();
	}

	/***  activate Usage Charge  ***/
	public void activateCharge(){
		ChargePage chargePage = PageFactory.initElements(driver, ChargePage.class);
		chargePage.clickOnActivateChargeButton();
		waitPageLoaded();
		chargePage.clickOnGoBackButton();
		waitPageLoaded();
	}

	/***  pick Charge To Product  ***/
	public void pickChargeToProduct(String chargeDescription, String version){
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		ChargePage chargePage = PageFactory.initElements(driver, ChargePage.class);
		// Check if charge is picked
		try {
			productPage.clickOnPricingButton();
		}catch (Exception e) {
			chargePage.clickOnGoBackButton();
			waitPageLoaded();
			productPage.clickOnPricingButton();
		}
		waitPageLoaded();

		// pick charge 
		if(version.equals("16.X") || version.equals("17.X")) {productPage.clickOnPickButton1();}
		else {productPage.clickOnPickButton2();}
		
		waitPageLoaded();
		productPage.setChargeDescription(chargeDescription);
		waitPageLoaded();
		productPage.selectChargeToPick(chargeDescription);
		waitPageLoaded();
			try {
				productPage.clickOnSaveButton();
			}catch (Exception e) {
				// TODO: handle exception
			}
	}

	/***  create New Price Version Price Grid Method  ***/
	public void createNewPriceVersionPriceGrid(String pricePlanVersionCode) throws InterruptedException{
		ChargePage chargePage = PageFactory.initElements(driver, ChargePage.class);
		NewPriceVersionpage newPriceVersionPage = PageFactory.initElements(driver, NewPriceVersionpage.class);

		for(int i = 0; i< 3; i++) {
			chargePage.clickOnCreatePriceVersionButton();
			waitPageLoaded();
			newPriceVersionPage.clickOnPriceGridButton();
			waitPageLoaded();
			newPriceVersionPage.setPriceVersionCode(pricePlanVersionCode);
			waitPageLoaded();
			newPriceVersionPage.clickOnSavePriceVersionButton();
			waitPageLoaded();
			if(newPriceVersionPage.errorMessageIsDsplayed() == true) {
				newPriceVersionPage.clickOnGoBackButton();
				waitPageLoaded();
			}else {
				break;
			}
		}
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);	

		//Assert New Price Version is created
		Assert.assertEquals(priceVersionPage.getPriceVersionLabelValue(), pricePlanVersionCode);
	}

	/***   Add a unit price to the price version   ***/
	public void addUnitPriceToPriceVersion(String UnitPrice) throws InterruptedException{
		Thread.sleep(5000);
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		priceVersionPage.setUnitPrice(UnitPrice);
		priceVersionPage.clickOnSave();
		Thread.sleep(5000);
	}

	public void addUnitPriceToPriceVersionWithAttribute(boolean isBusiness,String attributeId,String attributeName,String attributeValue, String unitPriceValue) throws InterruptedException {
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		priceVersionPage.clickOnAddAttributeButton();
		waitPageLoaded();
		priceVersionPage.clickOnAttributeInput();
		waitPageLoaded();
		priceVersionPage.clickOnBuisnessToggle();
		waitPageLoaded();
		priceVersionPage.setAttributeCode(attributeName);
		waitPageLoaded();
		priceVersionPage.clickOnAttribute(attributeName);
		waitPageLoaded();
		priceVersionPage.clickOnSaveAttributeGridButton();
		waitPageLoaded();
		priceVersionPage.setAttributValue(attributeId, attributeValue);
		priceVersionPage.setUnitPrice1( unitPriceValue);
		priceVersionPage.clickOnSavePriceGridButton();
		waitPageLoaded();
	}
	/***   Add a unit price to the price version   ***/
	public void addUnitPrice1(String UnitPrice) throws InterruptedException{
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		priceVersionPage.setUnitPrice1(UnitPrice);
		priceVersionPage.clickOnSavePriceGridButton();
		waitPageLoaded();
	}	
	/***   Add a unit price to the price version   ***/
	public void addUnitPrice2(String UnitPrice) throws InterruptedException{
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		priceVersionPage.setUnitPrice2(UnitPrice);
		waitPageLoaded();
	}

	public void savePrice() throws InterruptedException{
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		priceVersionPage.clickOnSavePriceGridButton();
		waitPageLoaded();
	}
	
	public void goBack() {
		ChargePage chargePage = PageFactory.initElements(driver, ChargePage.class);
		chargePage.clickOnGoBackButton();
		waitPageLoaded();
	}

	public void addAttribute(String attrCode, String addButtonNumber, String version){
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		try {
			priceVersionPage.clickOnAddNewAttributeButton(addButtonNumber);
		}catch (Exception e) {
			waitPageLoaded();
			priceVersionPage.clickOnAddNewAttributeButton(addButtonNumber);
		}
		waitPageLoaded();
		try {
			priceVersionPage.scrollIntoViewAttributeInput();
			priceVersionPage.clickOnAttributeInput();
		}catch (Exception e) {
			priceVersionPage.clickOnAddNewAttributeButton(addButtonNumber);
			priceVersionPage.clickOnAttributeInput();
		}
		waitPageLoaded();
		priceVersionPage.setAttributeCode(attrCode);
		waitPageLoaded();
		priceVersionPage.clickOnSelectedAttribute(attrCode);
		waitPageLoaded();
		if(version.equals("17.X")) {
			priceVersionPage.clickOnSaveAttributeGridButton2();
		} else {
			priceVersionPage.clickOnSaveAttributeGridButton();
		}
		
		waitPageLoaded();
		priceVersionPage.clickOnSavePriceGridButton();
		waitPageLoaded();
	}

	public void addNewValueToPriceVersion(String Value,String attributeName,String versionLigne) {
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		priceVersionPage.setAttributeValue(Value, attributeName, versionLigne);
	}
	
	public void save() {
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		priceVersionPage.clickOnSave();
	}
}