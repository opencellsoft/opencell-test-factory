package com.opencellsoft.tests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.operation.billingrun.BillingRunDetailsPage;
import com.opencellsoft.pages.operation.billingrun.BillingRunListPage;
import com.opencellsoft.pages.operation.billingrun.BillingrunPage;
import com.opencellsoft.pages.operation.billingrun.NewCycleRunPage;
import com.opencellsoft.pages.operation.billingrun.NewExeptionalRunPage;

public class BillingrunTests extends TestBase {

	BillingRunListPage billingRunListPage;
	BillingRunDetailsPage billingRunDetailsPage;

	@Parameters({ "billingCycle" })
	@Test(priority = 1)
	public void createNewCycleRun(String billingCycle) throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnOperationButton();
		homePage.clickOnBillibRunButton();
		BillingrunPage BillingrunPage = PageFactory.initElements(driver, BillingrunPage.class);
		BillingrunPage.clickOnAddBillingrunButton();
		BillingrunPage.clickOnCyclerunButton();
		NewCycleRunPage newCycleRunPage = PageFactory.initElements(driver, NewCycleRunPage.class);
		newCycleRunPage.setBillingCycleInput();
		newCycleRunPage.clickOnFilterButton();
		newCycleRunPage.clickOnCodeButton();
		newCycleRunPage.setCodeInput();
		newCycleRunPage.clickOnBillingCycleCode(billingCycle);
		newCycleRunPage.setProcessTypeInput();
		newCycleRunPage.selectProcessTypeOption();
		newCycleRunPage.setLastTransDateInput();
		newCycleRunPage.clickOnLastTransDateButton();
		newCycleRunPage.setInvoiceDateInput();
		newCycleRunPage.clickOnInvoiceDateButton();
		newCycleRunPage.setReferenceDateInput();
		newCycleRunPage.selectReferenceDateOption();
		newCycleRunPage.setActionOnrejectInput();
		newCycleRunPage.selectActionOnrejectOption();
		newCycleRunPage.setActionOnSuspectInput();
		newCycleRunPage.selectActionOnSuspectOption();
		newCycleRunPage.clickOncreateButton();
		newCycleRunPage.clickOnGoBackButton();
		Assert.assertEquals(newCycleRunPage.getMessageBox(), "Element created");
	}

	@Test(priority = 2)
	public void createNewExeptionalRun() throws InterruptedException {
		BillingrunPage BillingrunPage = PageFactory.initElements(driver, BillingrunPage.class);
		BillingrunPage.clickOnAddBillingrunButton();
		BillingrunPage.clickOnExeptionalrunButton();
		NewExeptionalRunPage newExeptionalRunPage = PageFactory.initElements(driver, NewExeptionalRunPage.class);
		newExeptionalRunPage.setProcessTypeInput();
		newExeptionalRunPage.selectProcessTypeOption();
		newExeptionalRunPage.setInvoiceDateInput();
		newExeptionalRunPage.clickOnInvoiceDateButton();
		newExeptionalRunPage.setActionOnrejectInput();
		newExeptionalRunPage.selectActionOnrejectOption();
		newExeptionalRunPage.setActionOnSuspectInput();
		newExeptionalRunPage.selectActionOnSuspectOption();
		//				newExeptionalRunPage.setFilterFields();
		newExeptionalRunPage.clickOnCreateButton();
		Assert.assertEquals(newExeptionalRunPage.getMessageBox(), "Element created");
		newExeptionalRunPage.clickOnCancelButton();

	}

	public void createCycleRun(String billingCycle,String processType,String lastTransactionDate, Boolean IncrementalInvoiceLines,String actionOnReject,String actionOnSuspect,Boolean PIROnBillingRunCreation,Boolean PIRWhenLaunchingInvoiceLinesJob,String applicationEl) {
		billingRunListPage = new BillingRunListPage();
		try {
			billingRunListPage.clickOnCreateNewBillingRunButton();
		}catch (Exception e) {
			waitPageLoaded();
			billingRunListPage.clickOnCreateNewBillingRunButton();
		}
		
		billingRunListPage.selectCycleRun();

		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.selectBillingCycle(billingCycle);
		billingRunDetailsPage.selectProcessType(processType);
		if(lastTransactionDate != null) {billingRunDetailsPage.setLastTransactionDate();}
		if(IncrementalInvoiceLines != null) {billingRunDetailsPage.setIncrementalInvoiceLines(IncrementalInvoiceLines);}
		if(actionOnReject != null) {billingRunDetailsPage.selectActionOnReject(actionOnReject);}
		if(actionOnSuspect != null) {billingRunDetailsPage.selectActionOnSuspect(actionOnSuspect);}
		if(PIROnBillingRunCreation != null) {billingRunDetailsPage.setPIROnBillingRunCreation(PIROnBillingRunCreation);}
		if(PIRWhenLaunchingInvoiceLinesJob != null) {billingRunDetailsPage.setPIRWhenLaunchingInvoiceLinesJob(PIRWhenLaunchingInvoiceLinesJob);}
		if(applicationEl != null) {billingRunDetailsPage.setApplicationEl(applicationEl);}
		billingRunDetailsPage.clickOnCreateButton();
	}

	
	
	
	public void createExceptionalRunByPortal(String processType,String invoiceDate,Boolean SkipInvoiceValidation,
			Boolean recomputeDatesAtValidation,Boolean IncrementalInvoiceLines,
			String rejectAutoAction,String suspectAutoAction,Boolean preReportAutoOnCreate,Boolean preReportAutoOnInvoiceLinesJob, String dateAggregation, String invoiceType,List<List<String>> filterFields) throws InterruptedException  {

		billingRunListPage = new BillingRunListPage();
		billingRunListPage.clickOnCreateNewBillingRunButton();
		billingRunListPage.selectExceptionalRun();

		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.selectProcessType(processType);
		if(invoiceDate != null) {billingRunDetailsPage.setInvoiceDate(invoiceDate);}
		if(SkipInvoiceValidation != null) {billingRunDetailsPage.setSkipInvoiceValidation(SkipInvoiceValidation);}
		if(recomputeDatesAtValidation != null) {billingRunDetailsPage.setRecomputeDatesAtValidation(recomputeDatesAtValidation);}
		if(IncrementalInvoiceLines != null) {billingRunDetailsPage.setIncrementalInvoiceLines(IncrementalInvoiceLines);}
		if(rejectAutoAction != null) {billingRunDetailsPage.selectActionOnReject(rejectAutoAction);}
		if(suspectAutoAction != null) {billingRunDetailsPage.selectActionOnSuspect(suspectAutoAction);}
		if(invoiceType != null) {billingRunDetailsPage.selectInvoiceType(invoiceType);}

		for (List<String> field : filterFields) {
			if(field.get(0) == "billingAcountCode") {
				billingRunDetailsPage.clickOnBillingAccountDraggable();
				billingRunDetailsPage.dragAndDropBillingAccountCode();
				billingRunDetailsPage.setOperator(field.get(1), "1");
				billingRunDetailsPage.setValue(field.get(2), "1");
			}

			if(field.get(0) == "usageDate") {
				billingRunDetailsPage.dragAndDropUsageDate();
				billingRunDetailsPage.setOperator(field.get(1), "2");
				billingRunDetailsPage.setValue(field.get(2), "2");
			}

		}
		billingRunDetailsPage.clickOnUpdateFilter();
		billingRunDetailsPage.clickOnCreateButton();

	}

	public void searchForBillingRun(String id, String billingCycleDescription, String status, String runType, String splitlevel) {
		billingRunListPage = new BillingRunListPage();
		if( id != null ) {billingRunListPage.searchById(id);}
		if( billingCycleDescription != null ) {billingRunListPage.searchByBillingCycle(billingCycleDescription);}
		if( status != null ) {billingRunListPage.searchByBillingStatus(status);}
		if( runType != null ) {billingRunListPage.searchByBillingRunType(runType);}
		if( splitlevel != null ) {billingRunListPage.searchByBillingRunSplitlevel(splitlevel);}
		waitPageLoaded();
	}

	public void goToBillingRunDetails()
	{
		billingRunListPage = new BillingRunListPage();
		billingRunListPage.goToBillingRunDetails();
	}

	public void cancelBillingRun() {
		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.clickOnCancelButton();
		try {
			billingRunDetailsPage.clickOnYesButton();
		}catch (Exception e) {
			// TODO: handle exception
		}
		billingRunDetailsPage.clickOnConfirmButton();
	}

	public void cancelBillingRunCancelRTs() {
		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.clickOnCancelButton();
		try {
			billingRunDetailsPage.clickOnYesButton();
		}catch (Exception e) {
			// TODO: handle exception
		}
		billingRunDetailsPage.CheckCancelRTs();
		billingRunDetailsPage.clickOnConfirmButton();
	}

	public void processBillingRun() {
		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.clickOnProcessButton();
//		billingRunDetailsPage.clickOnRefreshButton();
		try {billingRunDetailsPage.clickOnYesButton();
		}catch (Exception e) {}
	}	

	public void importNewRatedItemsAndKeepInvoiceLinesOpen() {
		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.clickOnProcessButton();
		billingRunDetailsPage.clickOnImportNewRatedItemsAndKeepInvoiceLinesOpen();
		try {billingRunDetailsPage.clickOnYesButton();}catch (Exception e) {}
		billingRunDetailsPage.clickOnRefreshButton();
	}

	public void closeInvoiceLinesAndGenerateInvoices() {
		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.clickOnProcessButton();
		billingRunDetailsPage.clickOnCloseInvoiceLinesAndGenerateInvoices();
		try {billingRunDetailsPage.clickOnYesButton();}catch (Exception e) {}
		billingRunDetailsPage.clickOnRefreshButton();
	}	

	public void disableBillingRun() {
		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.clickOnActionsButton();
		billingRunDetailsPage.clickOnDisableBillingRunButton();
		billingRunDetailsPage.clickOnConfirmButton();
	}

	public void enableBillingRun() {
		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.clickOnEnableBillingRunButton();
		billingRunDetailsPage.clickOnConfirmButton();
	}

	public String getBillingRunStatus() {
		billingRunDetailsPage = new BillingRunDetailsPage();
		return billingRunDetailsPage.billingRunStatus();
	}

	public String getInvoiceStatus() {
		billingRunDetailsPage = new BillingRunDetailsPage();
		return billingRunDetailsPage.invoiceStatus();
	}	

	public String getInvoiceType() {
		billingRunDetailsPage = new BillingRunDetailsPage();
		return billingRunDetailsPage.invoiceType();
	}

	public String getInvoiceAmountWoT() {
		billingRunDetailsPage = new BillingRunDetailsPage();
		return billingRunDetailsPage.invoiceAmountWoT();
	}	

	public String getInvoiceNumber() {
		billingRunDetailsPage = new BillingRunDetailsPage();
		return billingRunDetailsPage.invoiceNumber();
	}

	public List<String> getInvoiceGeneratedDetails(int i, String version) {
		billingRunDetailsPage = new BillingRunDetailsPage();
		return billingRunDetailsPage.getInvoiceGeneratedDetails(i, version);
	}
	
	public void goToInvoicePage(int i) {
		billingRunDetailsPage = new BillingRunDetailsPage();
		try {
			clickOn(driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[1]//td[" + i + "]//span//span")));
		}catch (Exception e) {
			clickOn(driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[1]//td[" + i + "]//span//span")));
		}
		
	}

	public String getInvoiceDetails(String index) {
		billingRunDetailsPage = new BillingRunDetailsPage();
		return billingRunDetailsPage.invoiceDetail(index);
	}

	public void goToCustomerPage(String index) {
		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.clickOnCustomerLink(index);
	}	

	public void goToDetails(String index) {
		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.clickOnlink(index);
	}	

	public List<String> getBillingRunDetailsFromList() {
		billingRunListPage = new BillingRunListPage();
		return billingRunListPage.getBillingRunDetails();
	}

	public void refreshPage() {
		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.clickOnRefreshButton();
	}

	public void sortListBy(String value){
		clickOn(driver.findElement(By.xpath("//table[@listid='operation/billing-run']//span[text() = '" + value + "']")));
	}

	public void openRepport(){
		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.clickOnStatisticsButton();
	}
	
	public void generateRepport(){
		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.clickOnGenerateRepportButton();
	}
	
	public void regenerateRepport(){
		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.clickOnRegenerateRepportButton();
	}

	public String checkRepport(String tbl, String tr, String td) {
		WebElement element = driver.findElement(By.xpath("(//form[@class = 'EditBillingRun']//table)[" + tbl + "]//tr[" + tr + "]//td[" + td + "]"));
		return element.getText();
		//		for (int i =0;i<2;i++) {
		//			String val1 =driver.findElements(By.tagName("td")).get(i).getText();
		//			Assert.assertTrue(val1.equalsIgnoreCase(value1));
		//		}

	}
	public boolean paginationIsDisplayed() {
		billingRunListPage = new BillingRunListPage();
		return billingRunListPage.paginationIsDisplayed();
	}
	
	public boolean displayfrom1to10RowInThePage() {
		billingRunListPage = new BillingRunListPage();
		return billingRunListPage.displayfrom1to10RowInThePage();
	}
	public boolean displayfrom1to20RowInThePage() {
		billingRunListPage = new BillingRunListPage();
		return billingRunListPage.displayfrom1to20RowInThePage();
	}
	public boolean displayfrom1to25RowInThePage() {
		billingRunListPage = new BillingRunListPage();
		return billingRunListPage.displayfrom1to25RowInThePage();
	}
	public boolean displayfrom1to50RowInThePage() {
		billingRunListPage = new BillingRunListPage();
		return billingRunListPage.displayfrom1to50RowInThePage();
	}
	public boolean displayfrom21to40RowInThePage() {
		billingRunListPage = new BillingRunListPage();
		return billingRunListPage.displayfrom21to40RowInThePage();
	}
	public boolean displayfrom26to50RowInThePage() {
		billingRunListPage = new BillingRunListPage();
		return billingRunListPage.displayfrom26to50RowInThePage();
	}
	public int thereIs10RowDisplayed() {
		billingRunListPage = new BillingRunListPage();
		return billingRunListPage.thereIs10RowDisplayed();
	}
	public int thereIs20RowDisplayed() {
		billingRunListPage = new BillingRunListPage();
		return billingRunListPage.thereIs20RowDisplayed();
	}
	public int thereIs25RowDisplayed() {
		billingRunListPage = new BillingRunListPage();
		return billingRunListPage.thereIs25RowDisplayed();
	}
	public int thereIs50RowDisplayed() {
		billingRunListPage = new BillingRunListPage();
		return billingRunListPage.thereIs50RowDisplayed();
	}
	public void goTotheNextPage() {
		billingRunListPage = new BillingRunListPage();
		billingRunListPage.clickOnNextbutton();
	}
	
	public void goToThePreviousPage() {
		billingRunListPage = new BillingRunListPage();
		billingRunListPage.clickOnPreviousbutton();
	}
	
	public void changeRowsPerPageTo50() {
		billingRunListPage = new BillingRunListPage();
		billingRunListPage.clickOnPaginationList();
		billingRunListPage.select50row();
	}
	public void changeRowsPerPageTo10() {
		billingRunListPage = new BillingRunListPage();
		billingRunListPage.clickOnPaginationList();
		billingRunListPage.select10row();
	}
	
	public void filterByBillingAccount() {
		billingRunListPage = new BillingRunListPage();
		billingRunListPage.clickOnFilterBySplitLevel();
		billingRunListPage.selectSplitByBillingAccount();
	}
	
	public void filterBySubscription() {
		billingRunListPage = new BillingRunListPage();
		billingRunListPage.clickOnFilterBySplitLevel();
		billingRunListPage.selectSplitBySubscription();
	}
	
	public void filterByOrder() {
		billingRunListPage = new BillingRunListPage();
		billingRunListPage.clickOnFilterBySplitLevel();
		billingRunListPage.selectSplitByOrder();
	}
	
	public int elementSize(String splitLevel) {
		return driver.findElements(By.xpath("//table[@id = 'operation/billing-run']//td//span[text() = '" + splitLevel + "']")).size();
	}
	
	public int elementMarkSize(String splitLevel) {
		return driver.findElements(By.xpath("//table[@id = 'operation/billing-run']//td//span//mark[text() = '" + splitLevel + "']")).size();
	}
	public void filterByCycle() {
		billingRunListPage = new BillingRunListPage();
		billingRunListPage.clickOnFilterByType();
		billingRunListPage.selectFilterByCycle();
	}
	
	public void filterByExceptional() {
		billingRunListPage = new BillingRunListPage();
		billingRunListPage.clickOnFilterByType();
		billingRunListPage.selectFilterByExceptional();
	}

	public void filterByAllRunTypes() {
		billingRunListPage = new BillingRunListPage();
		billingRunListPage.clickOnFilterByType();
		billingRunListPage.selectAllRunTypes();
	}
	
	public void sortByStatus() {
		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.clickOnSortByStatus();
	}
	
	public void sortByCustomer() {
		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.clickOnSortByCustomer();
	}
	
	public void searchForInvoice(String number, String customer, String seller, String status, String amountMin, String amountMax, String version) {
		billingRunDetailsPage = new BillingRunDetailsPage();
		if( number != null ) {billingRunDetailsPage.searchByInvoiceNumber(number);}
		if( customer != null ) {
			if( version.equals("14.1.X") ) {
				billingRunDetailsPage.searchByInvoiceCustomer141X(customer);}
			else {
				billingRunDetailsPage.searchByInvoiceCustomer(customer);
			}
		}
		if( seller != null ) {billingRunDetailsPage.searchByInvoiceSeller(seller);}
		if( status != null ) {billingRunDetailsPage.searchByInvoiceStatus(status);}
		if( amountMin != null && !version.equals("14.1.X") ) {
			billingRunDetailsPage.addFilterByAmountWOTax(); 
			billingRunDetailsPage.searchByInvoiceAmountMin(amountMin);
			}
		if( amountMax != null && !version.equals("14.1.X") ) {
			if(amountMin == null) {billingRunDetailsPage.addFilterByAmountWOTax();}
			billingRunDetailsPage.searchByInvoiceAmountMax(amountMax);
			}
		
		waitPageLoaded();
	}
	
	public int filteredInvoicesNumber() {
		return driver.findElements(By.xpath("//table[@listid='operation/invoices']//tbody//*[contains(@pathname, '/operation/billing-run/')]")).size();
	}
	
	public void validateFilteredInvoices() {
		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.clickonValidateInvoicesButton();
		billingRunDetailsPage.clickOnFilteredOption();
		billingRunDetailsPage.clickOnConfirmButton();
	}

	public void validateSelectedInvoices() {
		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.clickonValidateInvoicesButton();
		billingRunDetailsPage.clickOnSelectedOption();
		billingRunDetailsPage.clickOnConfirmButton();
	}
	
	public void selectinvoice(int i) {
		clickOn(driver.findElement(By.xpath("(//table[@listid='operation/invoices']//tbody//input[@type = 'checkbox'])[" + i + "]")));
	}
	
	public void clearFilter() {
		billingRunDetailsPage = new BillingRunDetailsPage();
		billingRunDetailsPage.clearStatus();
	}
}
