package com.opencellsoft.tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.subscriptions.SubscriptionsListPage;

public class SubscriptionsTests extends TestBase {
	
	@Parameters({ "selectSubscription" })
	@Test(priority = 1)
	public void checkSubscriptionDetails(String selectSubscription) throws InterruptedException {
		HomePage homepage = PageFactory.initElements(driver, HomePage.class);
		homepage.clickOnSubscriptionsButton();
		SubscriptionsListPage subscriptionsListPage = PageFactory.initElements(driver, SubscriptionsListPage.class);
		subscriptionsListPage.searchForSubscription(selectSubscription);
		subscriptionsListPage.clicOnSubscription();
	}
}
