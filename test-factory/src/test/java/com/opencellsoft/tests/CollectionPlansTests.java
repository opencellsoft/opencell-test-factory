package com.opencellsoft.tests;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.customercare.collectionplans.CollectionPlansListPage;

public class CollectionPlansTests extends TestBase  {

	CollectionPlansListPage collectionPlansListPage;
	
	public void filterCollectionPlansByStatus(String value) {
		collectionPlansListPage = new CollectionPlansListPage();
		collectionPlansListPage.selectStatus(value);
	}
	
	public void exportFormat(String value) {
		collectionPlansListPage = new CollectionPlansListPage();
		collectionPlansListPage.exportFormat(value);
	}
	
}
