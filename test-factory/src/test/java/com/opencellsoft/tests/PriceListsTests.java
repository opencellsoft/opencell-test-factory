package com.opencellsoft.tests;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.catalog.pricelists.PriceLinePage;
import com.opencellsoft.pages.catalog.pricelists.PricelistsListPage;
import com.opencellsoft.pages.catalog.pricelists.PricelistsPage;
import com.opencellsoft.pages.customercare.dunning.TemplatesListPage;

public class PriceListsTests extends TestBase {

	PricelistsListPage pricelistsListPage;
	PricelistsPage pricelistsPage;
	PriceLinePage pricelinePage ;
	public void createPriceList(String priceList_name,String priceList_description){
		pricelistsListPage= new PricelistsListPage();
		pricelistsListPage.clickOnCreateButton();
		
		pricelistsPage = new PricelistsPage();
		pricelistsPage.setName(priceList_name);
		pricelistsPage.setDescription(priceList_description);
		pricelistsPage.setValidFrom("7");
		pricelistsPage.setValidTo("22");
		pricelistsPage.setApplicationStartDate("8");
		pricelistsPage.setApplicationEndDate("20");
		pricelistsPage.clickOnSaveButton();
	}
	
	public void updatePriceList(String priceList_name,String priceList_description){
		
		pricelistsPage = new PricelistsPage();
		pricelistsPage.setName(priceList_name);
		pricelistsPage.setDescription(priceList_description);
		pricelistsPage.clickOnSaveButton();
	}
	
	public void addApplicationRulesCountry(String country) {
		pricelistsPage = new PricelistsPage();
		pricelistsPage.setCountry(country);
		pricelistsPage.clickOnSaveButton();
	}
	
	public void createPriceLine(String code,String description, String chargeName, String price) {
		pricelistsPage = new PricelistsPage();
		pricelistsPage.clickOnCreatePriceLineButton();
		
		pricelinePage = new PriceLinePage();
//		pricelinePage.setPricelineCode(code);
		pricelinePage.setPricelineDescription(description);
		pricelinePage.setPricelineCharge(chargeName);
		pricelinePage.setPriceLinePrice(price);
		pricelinePage.clickOnSaveButton();
	}
	
	public void activatePriceList() {
		pricelistsPage = new PricelistsPage();
		pricelistsPage.clickOnActionsButton();
		pricelistsPage.clickOnActivateButton();
	}
	
	public void duplicatePriceList() {
		pricelistsPage = new PricelistsPage();
		pricelistsPage.clickOnActionsButton();
		pricelistsPage.clickOnDuplicateButton();
	}
	
	public void archivePriceList() {
		pricelistsPage = new PricelistsPage();
		pricelistsPage.clickOnActionsButton();
		pricelistsPage.clickOnArchiveButton();
	}
	public void closePriceList() {
		pricelistsPage = new PricelistsPage();
		pricelistsPage.clickOnActionsButton();
		pricelistsPage.clickOnCloseButton();
		pricelistsPage.clickOnConfirmButton();
	}
	public void goBack() {
		pricelistsPage = new PricelistsPage();
		pricelistsPage.clickOnGoBackButton();
	}
	
	public void searchPriceListsByName(String value) {
		pricelistsListPage= new PricelistsListPage();
		pricelistsListPage.setSearchInput(value);
	}
	
	public int PriceListsTotal() {
		pricelistsListPage= new PricelistsListPage();
		return pricelistsListPage.PriceListsList().size();
	}
	
	public int PriceListsTotalOfStatus(String value) {
		pricelistsListPage= new PricelistsListPage();
		return pricelistsListPage.PriceListsInStatus(value).size();
	}
	
	public void goToPriceListDetails (String email) {
		pricelistsListPage= new PricelistsListPage();
		pricelistsListPage.goToPriceListDetails(email);
	}
	
	public void activateFilterBy(String value) {
		pricelistsListPage= new PricelistsListPage();
		pricelistsListPage.clickOnFilterButton();
		pricelistsListPage.activateFilterBy(value);
	}
	
	public void filterPriceListsByStatus(String value) {
		pricelistsListPage= new PricelistsListPage();
		pricelistsListPage.filterByStatus(value);
	}
	
	public void closeFilterBy(String value) {
		pricelistsListPage= new PricelistsListPage();
		pricelistsListPage.clickOnRemoveFilterBy(value);
	}
	
	public void sortPriceListsListBy(String value) {
		pricelistsListPage= new PricelistsListPage();
		pricelistsListPage.sortBy(value);
	}
}
