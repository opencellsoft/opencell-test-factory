package com.opencellsoft.tests;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.admin.AdminHomePage;
import com.opencellsoft.pages.admin.NewScriptPage;
import com.opencellsoft.pages.admin.ScriptsListPage;

public class ScriptTests extends TestBase {

	/***  go To Script List Page  ***/
	public void goToScriptListPage(){
		
		AdminHomePage adminHomePage = new AdminHomePage();
		adminHomePage.MoveToAdministrationMenu();
		adminHomePage.MoveToScriptMenu();
		adminHomePage.clickOnScriptsMenu();
		waitPageLoaded();
	}

	/***  add ValoCap Script  ***/
	public void addValoCapScript(){
		ScriptsListPage scriptsListPage = new ScriptsListPage();
		scriptsListPage.clickOnNewButton();
		waitPageLoaded();
		NewScriptPage newScriptPage = new NewScriptPage();
		newScriptPage.setValoCapScriptSource();
		waitPageLoaded();
		newScriptPage.clickOnValidateCompileButton();
		waitPageLoaded();
		newScriptPage.clickOnSaveButton();
		waitPageLoaded();
	}
	
	/***  add ValoCap Script  ***/
	public void addEligibilityUtilsScript(){
		ScriptsListPage scriptsListPage = new ScriptsListPage();
		scriptsListPage.clickOnNewButton();
		waitPageLoaded();
		NewScriptPage newScriptPage = new NewScriptPage();
		newScriptPage.setEligibilityUtilsScriptSource();
		waitPageLoaded();
		newScriptPage.clickOnValidateCompileButton();
		waitPageLoaded();
		newScriptPage.clickOnSaveButton();
		waitPageLoaded();
	}
	
}
