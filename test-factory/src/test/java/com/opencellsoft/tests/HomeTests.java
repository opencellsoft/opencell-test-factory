package com.opencellsoft.tests;

import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.HomePage;

public class HomeTests extends TestBase {
	HomePage homePage;
	
	/***  Go to Catalog page  ***/
	public void goToPortal() {
		homePage = PageFactory.initElements(driver, HomePage.class);
		try {
			homePage.clickOnCatalogButtonMenu2();
		}catch (Exception e) {}
	}
	
	/***  Go to offers page  ***/
	public void goToOffersPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCatalogButtonMenu22();
	}
	
	/***  Go to offers page  ***/
	public void goToProductsPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCatalogButtonMenu22();
		homePage.clickOnProductsButton();
	}
	
	
	/***  Go to Wallet operations page  ***/
	public void goToWalletOperationsPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnOperationButton();
		homePage.clickOnRatingButton();
		homePage.clickOnWalletOperations();
	}
	
	/***  Go to Framework Agreement page  ***/
	public void goToFrameworkAgreementPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnFinanceButton();
		homePage.clickOnCustomerCareButton0();
		homePage.clickOnFrameworkAgreementButton();
	}
	
	/***  go Custom Tables List Page  ***/
	public void goToCustomTablesListPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCustomerCareButton();
	}
	
	/***  go To Subscriptions List Page  ***/
	public void goToSubscriptionsListPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCustomerCareButton();
		homePage.clickOnSubscriptionsButton();
	}
	
	/***  go To Subscriptions List Page  ***/
	public void goToOrdersListPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCustomerCareButton();
		homePage.clickOnOrdersButton();
	}
	
	/***  go To Accounting Period Manager Page  ***/
	public void goToAccountingPeriodManagerPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnFinanceButton();
		homePage.clickOnAccountingPeriodButton();
		homePage.clickOnAccountingPeriodManagerButton();
	}
	
	/***  go To Accounting Period Report Page  ***/
	public void goToAccountingPeriodReportPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnFinanceButton();
		homePage.clickOnAccountingPeriodButton();
		homePage.clickOnAccountingPeriodReportButton();
	}
	
	/***  Go to accounting entities  ***/
	public void goToAccountingEntriesPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnFinanceButton();
		homePage.clickOnReportsButton();
		homePage.clickOnAccountingEntriesButton();
	}

	/***  go To Aged Trial Balance Page  ***/
	public void goToAgedTrialBalancePage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnFinanceButton();
		homePage.clickOnReportsButton();
		homePage.clickOnAgedTrialBalanceButton();
	}
	
	/***  go To Billing Cycle Page  ***/
	public void goToBillingCyclePage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnOperationButton();
		homePage.clickOnBillingRulesButton();
		try {
			homePage.clickOnBillingCyclesButton();
		}catch (Exception e) {
			homePage.clickOnBillingRulesButton();
			homePage.clickOnBillingCyclesButton();
		}
		
	}
	
	/***  go To Billing Cycle Page  ***/
	public void goToBillingRunPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnOperationButton();
	}
	
	/***  go To Rated Items Page  ***/
	public void goToRatedItemsPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnOperationButton();
		homePage.clickOnRatingButton();
		homePage.clickOnRatedItemsButton();
	}
	
	/***  go To Rated Items Page  ***/
	public void goToInvoiceValidationPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnOperationButton();
		homePage.clickOnBillingRulesButton();
		homePage.clickOnInvoiceValidationButton();
	}
	
	/***  Go to EDRs Page  ***/
	public void goToEDRsPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnOperationButton();
		homePage.clickOnMediationButton();
		homePage.clickOnEdrsButton();
	}
	
	/***  Go to EDRs setting  ***/
	public void goToEDRsSettingsPage(String version){
		homePage = PageFactory.initElements(driver, HomePage.class);
		if(version.equals("16.X") || version.equals("17.X")) {
			homePage.clickOnGeneralSettingsMainMenu();
			homePage.clickonApplicationSettings();
			homePage.clickOnEDR();
		}
		else {
			// Go to Edr Settings page
			homePage.clickOnGeneralSettingsMainMenu();
			homePage.clickOnOperationButton();
			homePage.clickOnMediationButton();
			homePage.clickOnEdrSettings();
		}
	}
	
	/***  Go to contacts Page  ***/
	public void goToContactsPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCustomerCareButton();
		homePage.clickOnContactsButton();
	}
	
	/***  Go to collection plans Page  ***/
	public void goToCollectionPlansPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCustomerCareButton();
		homePage.clickOnCollectionPlansButton();
	}
	
	/***  Go to template Page  ***/
	public void goToTemplatePage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCustomerCareButton();
		homePage.clickOnDunningsButton();
		homePage.clickOnTemplatesButton();
	}

	/***  Go to actions Page  ***/
	public void goToActionPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCustomerCareButton();
		homePage.clickOnDunningsButton();
		homePage.clickOnActionsButton();
	}
	
	/***  Go to levels Page  ***/
	public void goToLevelPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCustomerCareButton();
		homePage.clickOnDunningsButton();
		homePage.clickOnLevelsButton();
	}

	/***  Go to policies Page  ***/
	public void goToPolicyPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCustomerCareButton();
		homePage.clickOnDunningsButton();
		homePage.clickOnPoliciesButton();
	}
	
	/***  Go to Dunning Setting Page  ***/
	public void goToDunningSettingPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCustomerCareButton();
		homePage.clickOnDunningsButton();
		homePage.clickOnDunningSettingButton();
	}
	
	/***  Go to Price Lists Page  ***/
	public void goToPriceListsPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCatalogButtonMenu22();
		homePage.clickOnPriceListsButton();
	}
	
	/***  Go to Discount Plans Page  ***/
	public void goToDiscountPlansPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCatalogButtonMenu22();
		homePage.clickOnDiscountPlansButton();
	}
	
	/***  Go to Export Price Versions Page  ***/
	public void goToExportPriceVersionsPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCatalogButtonMenu22();
		homePage.clickOnCatalogManagerButton();
		homePage.clickOnPriceVersionsButton();
		homePage.clickOnExportButton();
		waitPageLoaded();
	}	
	
	/***  Go to Import Price Versions Page  ***/
	public void goToImportPriceVersionsPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCatalogButtonMenu22();
		homePage.clickOnCatalogManagerButton();
		homePage.clickOnPriceVersionsButton();
		homePage.clickOnImportButton();
	}
	
	/***  go To Business Attributes Page  ***/
	public void goToBusinessAttributesPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCatalogButtonMenu22();
		homePage.clickOnCatalogManagerButton();
		homePage.clickOnBusinessAttributesButton();
	}
	
	/***  Go To Attribute Groups Page  ***/
	public void goToAttributeGroupsPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCatalogButtonMenu22();
		homePage.clickOnCatalogManagerButton();
		homePage.clickOnAttributeGroupsButton();
	}
	
	/***  go To Media Page  ***/
	public void goToMediaPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCatalogButtonMenu22();
		homePage.clickOnCatalogManagerButton();
		homePage.clickOnMediaButton();
	}
	
	/***  go To Product Families Page  ***/
	public void goToProductFamiliesPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCatalogButtonMenu22();
		homePage.clickOnCatalogManagerButton();
		homePage.clickOnProductFamiliesButton();
	}
	
	/***  go To Product Families Page  ***/
	public void goToTagsPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCatalogButtonMenu22();
		homePage.clickOnCatalogManagerButton();
		homePage.clickOnTagsButton();
	}
	
	/***  go To Product Families Page  ***/
	public void goToTagCategoriesPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCatalogButtonMenu22();
		homePage.clickOnCatalogManagerButton();
		homePage.clickOnTagCategoriesButton();
	}
	
	/***  go To Report Extracts Page  ***/
	public void goToReportExtractsPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnOperationButton();
		homePage.clickOnReportExtractsButton();
		homePage.clickOnReportExtractsButton();
	}
	
	/***  go To Report Extracts History Page  ***/
	public void goToReportExtractsHistoryPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnOperationButton();
		homePage.clickOnReportExtractsButton();
		homePage.clickOnReportExtractsHistoryButton();
	}
	
	/***  go To Query Builder Page  ***/
	public void goToQueryBuilderPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnOperationButton();
		homePage.clickOnQueryToolsButton();
		homePage.clickOnQueryBuilderButton();
	}
	
	/***  go To Query Results Page  ***/
	public void goToQueryResultsPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnOperationButton();
		homePage.clickOnQueryToolsButton();
		homePage.clickOnQueryResultsButton();
	}
	
	/***  go To Manual Matching Page  ***/
	public void goToManualMatchingPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnFinanceButton();
		homePage.clickOnManualMatchingButton();
	}
	
	/***  go To Trial Balance Page  ***/
	public void goToTrialBalancePage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnFinanceButton();
		homePage.clickOnReportsButton();
		homePage.clickOnTrialBalanceButton();
	}
	
	/***  go To Trial Balance Page  ***/
	public void goToGeneralLedgerPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnFinanceButton();
		homePage.clickOnReportsButton();
		homePage.clickOnGeneralLedgerButton();
	}
	
	/***  go To Trial Balance Page  ***/
	public void goToSecurityDepositPage(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnFinanceButton();
		homePage.clickOnReportsButton();
		homePage.clickOnSecurityDepositButton();
	}
	
	public void logout () {
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnProfileButton();
		homePage.clickOnLogoutButton();
	}
	
	
}
