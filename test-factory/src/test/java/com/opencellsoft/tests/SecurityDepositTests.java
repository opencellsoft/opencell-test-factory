package com.opencellsoft.tests;

import org.testng.Assert;
import org.openqa.selenium.support.PageFactory;
import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.customercare.balance.BalancePage;
import com.opencellsoft.pages.customercare.securitydeposits.ListOfSecurityDepositPage;
import com.opencellsoft.pages.customercare.securitydeposits.NewSecurityDepositPage;
import com.opencellsoft.pages.customercare.securitydeposits.SecurityDepositPage;
import com.opencellsoft.pages.customers.CustomerInvoiceTabPage;
import com.opencellsoft.pages.finance.reports.SecurityDepositReportPage;
import com.opencellsoft.pages.invoices.InvoicePage;
import com.opencellsoft.pages.invoices.NewInvoiceLinePage;
import com.opencellsoft.pages.invoices.NewInvoicePage;

public class SecurityDepositTests extends TestBase {
	
	ListOfSecurityDepositPage listOfSecurityDepositPage;
	NewSecurityDepositPage newSecurityDepositPage;
	SecurityDepositPage securityDepositPage;
	InvoicePage invoicePage;
	NewInvoicePage newInvoicePage;
	CustomerInvoiceTabPage customerInvoiceTabPage;
	NewInvoiceLinePage newInvoiceLinePage;
	BalancePage balancePage;
	HomePage homePage;
	SecurityDepositReportPage securityDepositReportPage;
	String SDName;
	String invoiceNumber;
	
	/***   create Security Deposit    ***/
	public void createSecurityDeposit(String templateName,String securityDepositName,String securityDepositDescription,String expectedBalance) {
		listOfSecurityDepositPage = new ListOfSecurityDepositPage();
		listOfSecurityDepositPage.clickOnNewSecurityDepositButton();
		newSecurityDepositPage = new NewSecurityDepositPage();
		waitPageLoaded();
		newSecurityDepositPage.selectTemplateName();
		newSecurityDepositPage.setSecurityDepositDescription(securityDepositDescription);
		newSecurityDepositPage.setExpectedBalance(expectedBalance);
		newSecurityDepositPage.clickOnSaveButton();
		securityDepositPage = new SecurityDepositPage();
		waitPageLoaded();
		SDName = getSDName();
		System.out.println(SDName);
	}

	/***   Instantiate Security Deposit    ***/
	public void InstantiateSecurityDeposit() {
		securityDepositPage = new SecurityDepositPage();
		securityDepositPage.clickOnInstantiateButton();
		waitPageLoaded();
		Assert.assertTrue(securityDepositPage.SecurityDepositStatusValidated());
	}

	/***   credit Security Deposit    ***/
	public void creditSecurityDeposit(String amount, String reference) {
		securityDepositPage = new SecurityDepositPage();
		securityDepositPage.clickOnCreditButton();
		waitPageLoaded();
		securityDepositPage.setCheckAmount(amount);
		securityDepositPage.setCheckReference(reference);
		waitPageLoaded();
		securityDepositPage.clickOnConfirmButton();
		waitPageLoaded();
	}
	
	/***   refund Security Deposit    ***/
	public void refundSecurityDeposit(String reason) {
		securityDepositPage = new SecurityDepositPage();
		securityDepositPage.clickOnRefundButton();
		waitPageLoaded();
		securityDepositPage.setRefundReason(reason);
		waitPageLoaded();
		securityDepositPage.clickOnConfirmButton();
		waitPageLoaded();
	}
	
	/***   cancel Security Deposit   ***/
	public void cancelSecurityDeposit(String reason) {
		securityDepositPage = new SecurityDepositPage();
		securityDepositPage.clickOnCancelButton();
		waitPageLoaded();
		securityDepositPage.setCancelReason(reason);
		securityDepositPage.clickOnConfirmButton();
		waitPageLoaded();
	}
	
	/***   search For Security Deposit from  sdTab   ***/
	public void searchForSecurityDeposit() {
		listOfSecurityDepositPage.selectSecurityDeposit(SDName);
		waitPageLoaded();
	}
	
	/***   check SD Status : Locked    ***/
	public void checkSDStatusLocked() {
		securityDepositPage = new SecurityDepositPage();
		Assert.assertTrue(securityDepositPage.SecurityDepositStatusLocked());
	}
	
	/***   check SD Status : Refunded   ***/
	public void checkSDStatusRefunded () {
		securityDepositPage = new SecurityDepositPage();
		Assert.assertTrue(securityDepositPage.SecurityDepositStatusRefunded());
	}
	
	/***   check SD Status = Canceled   ***/
	public void checkSDStatusCanceled() {
		securityDepositPage = new SecurityDepositPage();
		Assert.assertTrue(securityDepositPage.SecurityDepositStatusCanceled());
	}

	/***   check SD Status = Unlocked ***/
	public void checkSDStatusUnlocked() {
		securityDepositPage = new SecurityDepositPage();
		Assert.assertTrue(securityDepositPage.SecurityDepositStatusUnlocked());
	}
	
	/***   check SD Status = Hold ***/
	public void checkSDStatusHold() {
		securityDepositPage = new SecurityDepositPage();
		Assert.assertTrue(securityDepositPage.SecurityDepositStatusHold());
	}
	
	/***   check SD Status = Unlocked ***/
	public void checkSDBalanceEqual(String balance) {
		securityDepositPage = new SecurityDepositPage();
		Assert.assertEquals(balance,securityDepositPage.getCurrentBalance());
	}
	
	
	/***   check Generated Invoice Type : SD    ***/
	public void checkGeneratedInvoiceTypeSD() {
		Assert.assertTrue(securityDepositPage.invoiceTypeSDIsGenerated());
		securityDepositPage.clickOnInvoiceLink();
		invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		waitPageLoaded();
		Assert.assertEquals(invoicePage.getInvoiceType(), "Security Deposit");
		Assert.assertEquals(invoicePage.getInvoiceStatus(), "VALIDATED");
		Assert.assertTrue(invoicePage.paymentStatusIsPending());
		invoicePage.clickOnGoBackButton();
		waitPageLoaded();
	}
	
	/***   check Generated Invoice Type : Adjustement   ***/
	public void checkGeneratedInvoiceTypeAdjustement () {
		Assert.assertTrue(securityDepositPage.invoiceTypeAdjustementIsGenerated());
		securityDepositPage.clickOnAdjustementLink();
		invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		waitPageLoaded();
		Assert.assertEquals(invoicePage.getInvoiceType(), "Adjustement");
		Assert.assertEquals(invoicePage.getInvoiceStatus(), "VALIDATED");
		Assert.assertTrue(invoicePage.paymentStatusIsRefunded());
		invoicePage.clickOnGoBackButton();
		waitPageLoaded();
	}

	/***   check Invoice Status : Paid    ***/
	public void checkInvoiceStatusPaid() {
		securityDepositPage.clickOnInvoiceLink();
		invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		waitPageLoaded();
		Assert.assertEquals(invoicePage.getInvoiceStatus(), "VALIDATED");
		Assert.assertTrue(invoicePage.paymentStatusIsPaid());
		invoicePage.clickOnPaymentTab();
		waitPageLoaded();
		Assert.assertTrue(invoicePage.paymentStatusIsMatched());
		invoicePage.clickOnGoBackButton();
		waitPageLoaded();
	}
	
	/***   check Invoice Status : PartPaid    ***/
	public void checkInvoiceStatusPartPaid() {
		securityDepositPage.clickOnInvoiceLink();
		invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		waitPageLoaded();
		Assert.assertEquals(invoicePage.getInvoiceStatus(), "VALIDATED");
		Assert.assertTrue(invoicePage.paymentStatusIsPartPaid());
		invoicePage.clickOnPaymentTab();
		waitPageLoaded();
		Assert.assertTrue(invoicePage.paymentStatusIsMatched());
		invoicePage.clickOnGoBackButton();
		waitPageLoaded();
	}
	
	/***   check Invoice Status = Abondoned   ***/
	public void checkInvoiceStatusAbondoned() {
		securityDepositPage.clickOnInvoiceLink();
		invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		waitPageLoaded();
		Assert.assertEquals(invoicePage.getInvoiceStatus(), "ABONDONED");
		invoicePage.clickOnGoBackButton();
		waitPageLoaded();
	}

	/***   get security Deposit Name   ***/
	public String getSDName() {
		return securityDepositPage.getSDName();
	}
	/***   validate Invoice   ***/

	/***   pay Invoice With SD   ***/
	public void payInvoiceWithSD(String SDName) {
		balancePage = PageFactory.initElements(driver, BalancePage.class); 
		balancePage.selectLastInvoice();
		waitPageLoaded();
		balancePage.clickOnPayWithSecurityDepositButton();
		waitPageLoaded();
		balancePage.clickOnSecurityDepositsList();
		waitPageLoaded();
		balancePage.selectSecurityDeposit(SDName);
		waitPageLoaded();
		balancePage.clickOnConfirmButton();
		waitPageLoaded();
	}
	
	public void GoToSDReport() {
		homePage = PageFactory.initElements(driver, HomePage.class); 
		homePage.clickOnFinanceButton();
		waitPageLoaded();
		homePage.clickOnReportsButton();
		waitPageLoaded();
		homePage.clickOnSecurityDepositButton();
		waitPageLoaded();
	}
	
	public void searchByCustomerInSDReport(String value) {
		securityDepositReportPage = new  SecurityDepositReportPage();
		securityDepositReportPage.setCustomerAccountInput(value);
	}
	
	public void searchBySecurityDepositName(String value) {
		securityDepositReportPage = new  SecurityDepositReportPage();
		securityDepositReportPage.setSecurityDepositNameInput(value);
	}
	
	public void activateFilterBy(String value) {
		securityDepositReportPage = new SecurityDepositReportPage();
		securityDepositReportPage.clickOnFilterButton();
		securityDepositReportPage.activateFilterBy(value);
	}
	
	public void filterByCurrency(String value) {
		securityDepositReportPage = new  SecurityDepositReportPage();
		securityDepositReportPage.clickOnFilterByCurrencyInput();
		securityDepositReportPage.clickOnCurrency(value);
	}
	
	public void deleteFilterByCurrency(String value) {
		securityDepositReportPage = new  SecurityDepositReportPage();
		securityDepositReportPage.clickOnDeleteFilterByCurrency(value);
	}
	
	// return the number of lines found
	public int elementsFoundSize(String value) {
		securityDepositReportPage = new SecurityDepositReportPage();
		return securityDepositReportPage.elementsFoundSize(value);
	}
}
