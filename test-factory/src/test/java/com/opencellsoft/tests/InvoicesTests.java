package com.opencellsoft.tests;

import java.awt.AWTException;
import java.util.Locale;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;
import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.customercare.subscriptions.ListOfSubscriptionsPage;
import com.opencellsoft.pages.customers.CustomerInvoiceTabPage;
import com.opencellsoft.pages.customers.CustomerPage;
import com.opencellsoft.pages.invoices.InvoiceLinePage;
import com.opencellsoft.pages.invoices.InvoicePage;
import com.opencellsoft.pages.invoices.InvoicesList;
import com.opencellsoft.pages.invoices.NewInvoiceLinePage;
import com.opencellsoft.pages.invoices.NewInvoicePage;
import com.opencellsoft.utility.Constant;

public class InvoicesTests extends TestBase {
	FakeValuesService fakeValuesService = new FakeValuesService(new Locale("en-GB"), new RandomService());
	InvoicePage invoicePage;
	CustomerInvoiceTabPage customerInvoiceTabPage;
	NewInvoicePage newInvoicePage;
	NewInvoiceLinePage newInvoiceLinePage;
	
	@Parameters({ "priceVersionUnitPrice" })
	@Test(priority = 1, groups = { "smokeTests" })
	public void generateInvoice(String priceVersionUnitPrice) throws InterruptedException {
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		customerPage.clickOnRatedItemsButton();
		waitPageLoaded();
		customerPage.clickOnGenerateInvoiceButton();
		InvoicePage invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		waitPageLoaded();
		Assert.assertEquals(invoicePage.getTextMessage(), "Action executed successfuly");
	}

	@Test(priority = 41)
	public void generateInvoiceFromInvoiceListScreen() throws InterruptedException {
//		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
//		homePage.clickOnCustomerCareButton();
//		homePage.clickOnInvoicesButton();
		InvoicePage invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		invoicePage.clickOnGoBackButton();
		InvoicesList invoicesList = PageFactory.initElements(driver, InvoicesList.class);
		Thread.sleep(1000);
		invoicesList.clickOnCreateInvoiceButton();
		Thread.sleep(1000);
		invoicesList.selectGenerateInvoiceOption();
		Thread.sleep(1000);
		invoicesList.clickOnTargetCustomersInput();
		Thread.sleep(1000);
		invoicesList.selectCustomerByVisibleCode(Constant.customerCode);
		Thread.sleep(1000);
		invoicesList.clickOnConfirmButton();
//		Assert.assertEquals(invoicePage.getTextMessage(), "Action executed successfuly");
	}

	@Test(priority = 2, groups = {
			"smokeTests" }, dependsOnMethods = "com.opencellsoft.tests.OrdersTests.generateInvoice")
	public void validateGeneratedInvoice() throws InterruptedException {
		InvoicePage invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		Thread.sleep(4000);
		invoicePage.validateInvoice();
		//Assert.assertEquals(invoicePage.getTextMessage(), "Action executed successfuly");

	}

	@Test(priority = 38, groups = {
	"smoke tests" }, dependsOnMethods = "com.opencellsoft.tests.OrdersTests.viewSubscriptionWithInvoicingPlan")
	//@Test(priority = 38, groups = {"smoke tests" })
	public void createNewCommercialInvoice() throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnInvoicesButton();
		InvoicesList invoicesList = PageFactory.initElements(driver, InvoicesList.class);
		invoicesList.clickOnCreateInvoiceButton();
		Thread.sleep(2000);
		invoicesList.selectCreateInvoiceOption();
		NewInvoicePage newInvoicePage = PageFactory.initElements(driver, NewInvoicePage.class);
		newInvoicePage.clickOnCustomerDropdown();
		Thread.sleep(3000);
		newInvoicePage.clickAddFilterBtn();
		Thread.sleep(3000);
		newInvoicePage.selectCodeFilter();
		newInvoicePage.searchForCustomer(Constant.customerCode);
		Thread.sleep(4000);
		newInvoicePage.clickOnCustomer(Constant.customerCode);
		newInvoicePage.clickOnInvoiceTypeDropdown();
		newInvoicePage.selectCommercialInvoiceType();
		newInvoicePage.clickOnSaveCommercialInvoiceButton();
		Assert.assertTrue(newInvoicePage.getTextMessage().toLowerCase().contains("element created"));
	}

	@Test(priority = 38, groups = {"smoke tests" })
	public void createNewInvoiceFromCustomerView() throws InterruptedException {
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		customerPage.clickOnInvoicesTab();
		customerPage.clickOnNewInvoiceButton();
		InvoicePage invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		Assert.assertEquals(invoicePage.getTextMessage(), "Action executed successfuly");
	}
	
	@Parameters({ "articleCode", "invoiceQuantity", "invoicePrice" })
	@Test(priority = 39, groups = { "smoke tests" }, dependsOnMethods = "createNewCommercialInvoice")
	//@Test(priority = 39, groups = { "smoke tests" })
	public void addInvoiceLines(String articleCode, String invoiceQuantity, String invoicePrice) throws Exception {
		InvoicePage invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		invoicePage.clickOnCreateInvoiceLine();
		waitPageLoaded();
		NewInvoiceLinePage newInvoiceLinePage = PageFactory.initElements(driver, NewInvoiceLinePage.class);
		newInvoiceLinePage.setArticle(articleCode);
		newInvoiceLinePage.setInvoiceLineDescription(Constant.invoiceLineDescription);
		newInvoiceLinePage.setInvoiceLineQuantity(invoiceQuantity);
		newInvoiceLinePage.setInvoiceLinePrice(invoicePrice);
		newInvoiceLinePage.clickOnSaveInvoiceLineButton();
		waitPageLoaded();
		InvoiceLinePage invoiceLinePage = PageFactory.initElements(driver, InvoiceLinePage.class);
		Assert.assertEquals(invoiceLinePage.getTextMessage(), "Element created");
	}

	@Test(priority = 38, groups = { "smoke tests" })
	public void createAdjustementInvoice() throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnInvoicesButton();
		InvoicesList invoicesList = PageFactory.initElements(driver, InvoicesList.class);
		invoicesList.clickOnCreateInvoiceButton();
		invoicesList.selectCreateInvoiceOption();
		NewInvoicePage newInvoicePage = PageFactory.initElements(driver, NewInvoicePage.class);
		newInvoicePage.clickOnCustomerDropdown();
		newInvoicePage.clickAddFilterBtn();
		newInvoicePage.selectCodeFilter();
		newInvoicePage.searchForCustomer(Constant.customerCode);
		waitPageLoaded();
		newInvoicePage.clickOnCustomer(Constant.customerCode);
		newInvoicePage.clickOnInvoiceTypeDropdown();
		newInvoicePage.selectInvoiceType("ADJ - Adjustement");
		;
		newInvoicePage.clickOnSaveCommercialInvoiceButton();
		Assert.assertEquals(newInvoicePage.getTextMessage(), "Element created");
	}

	@Test(priority = 38, groups = { "smoke tests" })
	public void createDraftTypeInvoice() throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
//		homePage.clickOnCustomerCareButton();
		homePage.clickOnInvoicesButton();
		InvoicesList invoicesList = PageFactory.initElements(driver, InvoicesList.class);
		invoicesList.clickOnCreateInvoiceButton();
		invoicesList.selectCreateInvoiceOption();
		NewInvoicePage newInvoicePage = PageFactory.initElements(driver, NewInvoicePage.class);
		newInvoicePage.clickOnCustomerDropdown();
		waitPageLoaded();
		newInvoicePage.clickAddFilterBtn();
		newInvoicePage.selectCodeFilter();
		newInvoicePage.searchForCustomer(Constant.customerCode);
		waitPageLoaded();
		newInvoicePage.clickOnCustomer(Constant.customerCode);
		newInvoicePage.clickOnInvoiceTypeDropdown();
		newInvoicePage.selectInvoiceType("DRAFT - Draft invoice");
		;
		newInvoicePage.clickOnSaveCommercialInvoiceButton();
		Assert.assertEquals(newInvoicePage.getTextMessage(), "Element created");
	}

	@Parameters({"articleCode", "invoiceQuantity", "invoicePrice" })
	@Test(priority = 39)
	public void addInvoiceLine(String articleCode, String invoiceQuantity, String invoicePrice) throws Exception {
		InvoicePage invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		invoicePage.clickOnCreateInvoiceLine();
		NewInvoiceLinePage newInvoiceLinePage = PageFactory.initElements(driver, NewInvoiceLinePage.class);
		newInvoiceLinePage.setArticle(articleCode);
		newInvoiceLinePage.setInvoiceLineDescription(Constant.invoiceLineDescription);
		newInvoiceLinePage.setInvoiceLineQuantity(invoiceQuantity);
		newInvoiceLinePage.setInvoiceLinePrice(invoicePrice);
		newInvoiceLinePage.clickOnSaveInvoiceLineButton();
		InvoiceLinePage invoiceLinePage = PageFactory.initElements(driver, InvoiceLinePage.class);
		Assert.assertEquals(invoiceLinePage.getTextMessage(), "Element created");
	}

	@Parameters({ "articleCode", "invoiceQuantity", "invoicePrice" })
	@Test(priority = 39, groups = { "smoke tests" })
	public void addInvoiceLinesWithDiscount(String articleCode, String invoiceQuantity, String invoicePrice)
			throws Exception {
		InvoicePage invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		Thread.sleep(2000);
		invoicePage.clickOnCreateInvoiceLine();
		NewInvoiceLinePage newInvoiceLinePage = PageFactory.initElements(driver, NewInvoiceLinePage.class);
		newInvoiceLinePage.setArticle(articleCode);
		newInvoiceLinePage.setInvoiceLineDescription(Constant.invoiceLineDescription);
		newInvoiceLinePage.setInvoiceLineQuantity(invoiceQuantity);
		newInvoiceLinePage.setInvoiceLinePrice(invoicePrice);
		newInvoiceLinePage.clickOnDiscountPlanInput();
		newInvoiceLinePage.clickOnFilterButton();
		newInvoiceLinePage.selectFilterByCodeOption();
		newInvoiceLinePage.setDiscountPlanCodeFilter(Constant.discountPlanCode);
		Thread.sleep(1000);
		newInvoiceLinePage.selectFoundDiscountPlan();
		newInvoiceLinePage.clickOnSaveInvoiceLineButton();
		// Assert.assertTrue(newInvoiceLinePage.getTextMessage().toLowerCase().contains("element
		// created"));
	}

	@Test(priority = 40, groups = { "smoke tests" }, dependsOnMethods = "addInvoiceLines")
	public void validateInvoice() throws InterruptedException {
		InvoiceLinePage invoiceLinePage = PageFactory.initElements(driver, InvoiceLinePage.class);
		invoiceLinePage.clickOnGoBackButton();
		InvoicePage invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		Thread.sleep(4000);
		invoicePage.validateInvoice();
		invoicePage.clickOnUpdateAndValidateButton();
		Assert.assertEquals(invoicePage.getTextMessage(), "Action executed successfuly");		
	}

	@Test(priority = 40)
	public void validateDraftInvoice() throws InterruptedException {
		InvoiceLinePage invoiceLinePage = PageFactory.initElements(driver, InvoiceLinePage.class);
		Thread.sleep(2000);
		invoiceLinePage.clickOnGoBackButton();
		InvoicePage invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		invoicePage.validateInvoice();
		Assert.assertEquals(invoicePage.getTextMessage(), "Action executed successfuly");
	}

	@Test(priority = 40, groups = { "smoke tests" })
	public void validateInvoiceWithDiscount() throws InterruptedException {
		InvoiceLinePage invoiceLinePage = PageFactory.initElements(driver, InvoiceLinePage.class);
		Thread.sleep(2000);
		invoiceLinePage.clickOnGoBackButton();
		InvoicePage invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		invoicePage.validateInvoice();
		Assert.assertTrue(invoicePage.getTextMessage().toLowerCase().contains("action executed successfuly"));
	}

	@Test(priority = 41, groups = { "smoke tests" }, dependsOnMethods = "validateInvoice")
	public void viewPDFInvoice() throws InterruptedException {
		InvoicePage invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		waitPageLoaded();
		invoicePage.clickOnActionsButton();
		waitPageLoaded();
		invoicePage.clickOnViewPDFButton();
		waitPageLoaded();
		Assert.assertEquals(invoicePage.getTextMessage(), "Action executed successfuly");
	}

	@Test
	public void createInvoiceFromCustomerView() {
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		customerPage.clickOnInvoicesButton();
		customerPage.clickOnAddInvoiceButton();
	}

	@Test(priority = 6, groups = { "perf tests" })
	public void checkInvoicesList() { //Check the Subscriptions list page
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCustomerCareButton();
		homePage.clickOnInvoicesButton();
		InvoicesList invoicesList = PageFactory.initElements(driver, InvoicesList.class);
		invoicesList.checkBreadCrumb("Customer Care");
		invoicesList.checkBreadCrumb("Invoices");
	}

	@Test(priority = 7, groups = { "perf tests" }) 
	public void checkPaginationInvoicesList() { 
		InvoicesList invoicesList = PageFactory.initElements(driver, InvoicesList.class);
		invoicesList.checkPagination();
	}	

	@Parameters({ "customerCode" })
	@Test(priority = 8, groups = { "perf tests" }) 
	public void checkInvoiceDetail(String customerCode) { 
		InvoicesList invoicesList = PageFactory.initElements(driver, InvoicesList.class);
		invoicesList.clickOnAddFilterButton();
		invoicesList.clickOnCustomerFilter();
		invoicesList.searchForCustomer(customerCode);
		invoicesList.clickOnCustomer(customerCode);
	}	
	
	/***   create New Commercial Invoice   ***/
	public void createCommercialInvoice() throws AWTException {
		customerInvoiceTabPage = PageFactory.initElements(driver, CustomerInvoiceTabPage.class);
		customerInvoiceTabPage.clickOnNewInvoiceButton();
		waitPageLoaded();
		newInvoicePage = PageFactory.initElements(driver, NewInvoicePage.class); 
		newInvoicePage.setCommercialInvoiceType();
		newInvoicePage.setSeller();
		newInvoicePage.clickOnSaveButton();
	}
	
	/***   create Invoice line   ***/
	public void createInvoiceLine(String articleCode,String invoiceQuantity,String invoicePrice, String taxCode) throws AWTException {
		invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		invoicePage.clickOnCreateInvoiceLine();
		newInvoiceLinePage = PageFactory.initElements(driver, NewInvoiceLinePage.class); 
		waitPageLoaded();
		newInvoiceLinePage.createNewInvoiceLineMethod(articleCode, invoiceQuantity, invoicePrice, taxCode);
		waitPageLoaded();
		invoicePage.clickOnSaveButton();
		waitPageLoaded();
	}
	
	/***   put the invoice in the past   ***/
	public void putTheInvoiceInThePast(int m) throws AWTException {
		invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		invoicePage.setInvoiceDateInPast(m);
		invoicePage.setDueDateInPast(m);
		waitPageLoaded();
		invoicePage.clickOnSaveButton();
		waitPageLoaded();
	}
	
	/***   put the invoice in the futur   ***/
	public void putTheInvoiceInThefutur(int m) throws AWTException {
		invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		invoicePage.setInvoiceDateInFutur(m);
		invoicePage.setDueDateInFutur(m);
		waitPageLoaded();
		invoicePage.clickOnSaveButton();
		waitPageLoaded();
	}
	
	public String validateTheInvoice() {
		String invN;
		invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		invoicePage.validateInvoice();
		if(invoicePage.validatingInvoiceIsDisplayed() == true) {
			invoicePage.clickOnUpdateAndValidateButton();
		}
		waitPageLoaded();
		invN = invoicePage.getInvoiceNumber();
		System.out.println("invN ===> " + invN);
		if(invN == "Draft" || invN.contains("-")) {
			invoicePage.validateInvoice();
			if(invoicePage.validatingInvoiceIsDisplayed() == true) {
				invoicePage.clickOnUpdateAndValidateButton();
				waitPageLoaded();
			}
		}
		invN = invoicePage.getInvoiceNumber();
		invoicePage.clickOnGoBackButton();
		waitPageLoaded();
		return invN;
	}
	
	/***   get Invoice Number   ***/
	public String getInvoiceNumber() {
		invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		return invoicePage.getInvoiceNumber();
	}
	
	/***   get Invoice Type   ***/
	public String getInvoiceType() {
		invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		return invoicePage.getInvoiceType();
	}
	
	/***   get Invoice Status   ***/
	public String getInvoiceStatus() {
		invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		return invoicePage.getInvoiceStatus();
	}
	
	/***   get Invoice Status   ***/
	public String getRejectionByRule() {
		invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		return invoicePage.getRejectedByRule();
	}
	
	public void goBack() {
		invoicePage.clickOnGoBackButton();
		waitPageLoaded();
	}
	
	public void navigateBack() {
		driver.navigate().back();
	}

}