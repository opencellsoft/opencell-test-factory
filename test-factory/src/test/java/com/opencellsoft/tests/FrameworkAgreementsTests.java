package com.opencellsoft.tests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.customercare.contracts.ContractPage;
import com.opencellsoft.pages.customercare.contracts.ListOfContractsPage;
import com.opencellsoft.pages.customercare.contracts.NewContractLinePage;
import com.opencellsoft.pages.customercare.contracts.NewContractPage;

public class FrameworkAgreementsTests extends TestBase {


	/***  create New framework Agreement in the past  ***/
	public void createNewframeworkAgreementInPast(String code,String description, String level,String entity ){
		ListOfContractsPage listOfContractsPage =  PageFactory.initElements(driver, ListOfContractsPage.class);
		listOfContractsPage.clickOnCreateNewContractButton();
		NewContractPage newContractPage = PageFactory.initElements(driver, NewContractPage.class);
		newContractPage.setContractCode(code);
		newContractPage.setContractDescription(description);
		newContractPage.selectStartDateInPast();
		newContractPage.selectEndDateInPast();
		newContractPage.selectContractAccountLevel();
		newContractPage.setAttachedEntity(entity);
		newContractPage.selectContractDate();
		newContractPage.clickOnSaveContractButton();
		waitPageLoaded();
	}
	
	/***  create New framework Agreement in the present  ***/
	public void createNewframeworkAgreementInPresent(String code,String description, String level,String entity ){
		ListOfContractsPage listOfContractsPage =  PageFactory.initElements(driver, ListOfContractsPage.class);
		listOfContractsPage.clickOnCreateNewContractButton();
		NewContractPage newContractPage = PageFactory.initElements(driver, NewContractPage.class);
		newContractPage.setContractCode(code);
		newContractPage.setContractDescription(description);
		newContractPage.selectStartDateInPresent();
		newContractPage.selectEndDateInPresent();
		newContractPage.selectContractAccountLevel();
		newContractPage.setAttachedEntity(entity);
		newContractPage.selectContractDate();
		newContractPage.clickOnSaveContractButton();
		waitPageLoaded();
	}	
	/***  create New framework Agreement in the futur  ***/
	public void createNewframeworkAgreementInFutur(String code,String description, String level,String entity ){
		ListOfContractsPage listOfContractsPage =  PageFactory.initElements(driver, ListOfContractsPage.class);
		listOfContractsPage.clickOnCreateNewContractButton();
		NewContractPage newContractPage = PageFactory.initElements(driver, NewContractPage.class);
		newContractPage.setContractCode(code);
		newContractPage.setContractDescription(description);
		newContractPage.selectStartDateInFutur();
		newContractPage.selectEndDateInFutur();
		newContractPage.selectContractAccountLevel();
		newContractPage.setAttachedEntity(entity);
		newContractPage.selectContractDate();
		newContractPage.clickOnSaveContractButton();
		waitPageLoaded();
	}	
	/***  Add Contract Line To framework Agreement : Global price  ***/
	public void AddContractLineGlobalPrice(String code, String description,String offer, String product, String charge, String type, String price ) {
		ContractPage contractPage =  PageFactory.initElements(driver, ContractPage.class);
		contractPage.clickOnCreateContractLineButton();
		waitPageLoaded();
		NewContractLinePage newContractLinePage = PageFactory.initElements(driver, NewContractLinePage.class);
		newContractLinePage.setContractLineCode(code);
		newContractLinePage.setContractLineDescription(description);
		newContractLinePage.setOffer(offer);
		newContractLinePage.setProduct(product);
		newContractLinePage.setCharge(charge);
		newContractLinePage.selectContractLineType(type);
		newContractLinePage.setContractPrice(price);
		newContractLinePage.clickOnSaveButton();
		waitPageLoaded();
		newContractLinePage.clickOnGoBackButton();
		waitPageLoaded();
	}
	
	/***  Add Contract Line To framework Agreement : Global discount (%)  ***/
	public void AddContractLineGlobalDiscount(String code, String description,String offer, String product, String charge, String type, String rate, boolean separate ) {
		ContractPage contractPage =  PageFactory.initElements(driver, ContractPage.class);
		contractPage.clickOnCreateContractLineButton();
		waitPageLoaded();
		NewContractLinePage newContractLinePage = PageFactory.initElements(driver, NewContractLinePage.class);
		newContractLinePage.setContractLineCode(code);
		newContractLinePage.setContractLineDescription(description);
		newContractLinePage.setOffer(offer);
		newContractLinePage.setProduct(product);
		newContractLinePage.setCharge(charge);
		newContractLinePage.selectContractLineType(type);
		newContractLinePage.setDiscoutRate(rate);
		newContractLinePage.setSeparateDiscountLine(separate);
		newContractLinePage.clickOnSaveButton();
		waitPageLoaded();
		newContractLinePage.clickOnGoBackButton();
		waitPageLoaded();
	}
	
	/***  Add Contract Line To framework Agreement : Custom Contract Price   ***/
	public void AddContractLineCustomContractPriceWithCreatePriceVersion(String code, String description,String offer, String product, String charge, String type, String rate ) throws InterruptedException {
		ContractPage contractPage =  PageFactory.initElements(driver, ContractPage.class);
		contractPage.clickOnCreateContractLineButton();
		waitPageLoaded();
		NewContractLinePage newContractLinePage = PageFactory.initElements(driver, NewContractLinePage.class);
		newContractLinePage.setContractLineCode(code);
		newContractLinePage.setContractLineDescription(description);
		newContractLinePage.setOffer(offer);
		newContractLinePage.setProduct(product);
		newContractLinePage.setCharge(charge);
		newContractLinePage.selectContractLineType(type);
		newContractLinePage.clickOnCreatePriceVersionButton();
		waitPageLoaded();
		newContractLinePage.clickOnPriceVersionLine();
		waitPageLoaded();
		ChargesTests chargesTests = new ChargesTests();
		try {
			chargesTests.addUnitPriceToPriceVersion(rate);
		}catch (Exception e) {
			newContractLinePage.clickOnPriceVersionLine();
			waitPageLoaded();
			chargesTests.addUnitPriceToPriceVersion(rate);
		}
		
		chargesTests.publishPriceVersion();
		newContractLinePage.clickOnSaveButton();
		waitPageLoaded();
		newContractLinePage.goToContract();
		waitPageLoaded();
		
	}
	
	/***  Add Contract Line To framework Agreement : Custom Contract Price  ***/
	public void AddContractLineCustomContractPriceWithDuplicateCharge(String code, String description,String offer, String product, String charge, String type, String rate ) throws InterruptedException {
		ContractPage contractPage =  PageFactory.initElements(driver, ContractPage.class);
		contractPage.clickOnCreateContractLineButton();
		waitPageLoaded();
		NewContractLinePage newContractLinePage = PageFactory.initElements(driver, NewContractLinePage.class);
		newContractLinePage.setContractLineCode(code);
		newContractLinePage.setContractLineDescription(description);
		newContractLinePage.setOffer(offer);
		newContractLinePage.setProduct(product);
		newContractLinePage.setCharge(charge);
		newContractLinePage.selectContractLineType(type);
		newContractLinePage.clickOnpriceVersionToDuplicateInput();
		waitPageLoaded();
		newContractLinePage.clickOnPriceVersionLine();
		waitPageLoaded();
		newContractLinePage.clickOnDuplicateButton();
		waitPageLoaded();
		newContractLinePage.clickOnPriceVersionLine();
		waitPageLoaded();
		ChargesTests chargesTests = new ChargesTests();
		chargesTests.addUnitPriceToPriceVersion(rate);
		chargesTests.publishPriceVersion();
		newContractLinePage.clickOnSaveButton();
		waitPageLoaded();
		newContractLinePage.goToContract();
		waitPageLoaded();
		
	}
	
	/***  Add Contract Line To framework Agreement : Custom contract discount (%)  
	 * @throws InterruptedException ***/
	public void AddContractLineCustomContractDiscountWithCreatePriceVersion(String code, String description,String offer, String product, String charge, String type, String rate, boolean separate ) throws InterruptedException {
		ContractPage contractPage =  PageFactory.initElements(driver, ContractPage.class);
		NewContractLinePage newContractLinePage = PageFactory.initElements(driver, NewContractLinePage.class);
		try {
			contractPage.clickOnCreateContractLineButton();
		}catch (Exception e) {
			newContractLinePage.goToContract();
			waitPageLoaded();
			contractPage.clickOnCreateContractLineButton();
		}
		waitPageLoaded();
		
		newContractLinePage.setContractLineCode(code);
		newContractLinePage.setContractLineDescription(description);
		newContractLinePage.setOffer(offer);
		newContractLinePage.setProduct(product);
		newContractLinePage.setCharge(charge);
		newContractLinePage.selectContractLineType(type);
		newContractLinePage.setSeparateDiscountLine(separate);
		newContractLinePage.clickOnCreatePriceVersionButton();
		waitPageLoaded();
		newContractLinePage.clickOnPriceVersionLine();
		waitPageLoaded();
		ChargesTests chargesTests = new ChargesTests();
		chargesTests.addUnitPriceToPriceVersion(rate);
		chargesTests.publishPriceVersion();
		waitPageLoaded();
		newContractLinePage.clickOnSaveButton();
		waitPageLoaded();
		newContractLinePage.goToContract();
		waitPageLoaded();
	}
	
	/***  Add Contract Line To framework Agreement : Custom contract discount (%)  ***/
	public void AddContractLineCustomContractDiscountWithDuplicateCharge(String code, String description,String offer, String product, String charge, String type, String rate, boolean separate ) throws InterruptedException {
		ContractPage contractPage =  PageFactory.initElements(driver, ContractPage.class);
		contractPage.clickOnCreateContractLineButton();
		waitPageLoaded();
		NewContractLinePage newContractLinePage = PageFactory.initElements(driver, NewContractLinePage.class);
		newContractLinePage.setContractLineCode(code);
		newContractLinePage.setContractLineDescription(description);
		newContractLinePage.setOffer(offer);
		newContractLinePage.setProduct(product);
		newContractLinePage.setCharge(charge);
		newContractLinePage.selectContractLineType(type);
		newContractLinePage.setSeparateDiscountLine(separate);
		waitPageLoaded();
		newContractLinePage.clickOnpriceVersionToDuplicateInput();
		waitPageLoaded();
		newContractLinePage.clickOnPriceVersionLine();
		waitPageLoaded();
		newContractLinePage.clickOnDuplicateButton();
		waitPageLoaded();
		newContractLinePage.clickOnSaveButton();
		waitPageLoaded();
		newContractLinePage.clickOnPriceVersionLine();
		waitPageLoaded();
		ChargesTests chargesTests = new ChargesTests();
		chargesTests.addUnitPriceToPriceVersion(rate);
		chargesTests.publishPriceVersion();
		newContractLinePage.clickOnSaveButton();
		waitPageLoaded();
		newContractLinePage.goToContract();
		waitPageLoaded();

	}
	/***  Add Contract Line To framework Agreement : Custom contract discount (%)  ***/
	public void AddContractLineCustomContractDiscountWithDuplicateCharge0(String code, String description,String offer, String product, String charge, String type, String rate, boolean separate ) throws InterruptedException {
		ContractPage contractPage =  PageFactory.initElements(driver, ContractPage.class);
		contractPage.clickOnCreateContractLineButton();
		waitPageLoaded();
		NewContractLinePage newContractLinePage = PageFactory.initElements(driver, NewContractLinePage.class);
		newContractLinePage.setContractLineCode(code);
		newContractLinePage.setContractLineDescription(description);
		newContractLinePage.setOffer(offer);
		newContractLinePage.setProduct(product);
		newContractLinePage.setCharge(charge);
		newContractLinePage.selectContractLineType(type);
		newContractLinePage.setSeparateDiscountLine(separate);
		waitPageLoaded();
		newContractLinePage.clickOnpriceVersionToDuplicateInput();
		waitPageLoaded();
		newContractLinePage.clickOnPriceVersionLine();
		waitPageLoaded();
		newContractLinePage.clickOnDuplicateButton();
		waitPageLoaded();
		newContractLinePage.clickOnPriceVersionLine();
		waitPageLoaded();
		ChargesTests chargesTests = new ChargesTests();
		//chargesTests.addUnitPrice1(rate);
		//chargesTests.addUnitPrice2(rate);
		chargesTests.publishPriceVersion();
		newContractLinePage.clickOnSaveButton();
		waitPageLoaded();
		newContractLinePage.goToContract();
		waitPageLoaded();

	}	
	public void activateContract() {
		ContractPage contractPage =  PageFactory.initElements(driver, ContractPage.class);
		try {
			clickOn(driver.findElement(By.xpath("//span[contains(text(), 'Actions')]")));
			waitPageLoaded();
			contractPage.clickOnAcivateContract();
		}catch (Exception e) {
			contractPage.clickOnAcivateContract();
		}
		
		waitPageLoaded();
		if(contractPage.contractStatusIsActivated()!= 1) {
			try {
				clickOn(driver.findElement(By.xpath("//span[contains(text(), 'Actions')]")));
				waitPageLoaded();
				contractPage.clickOnAcivateContract();
			}catch (Exception e) {
				contractPage.clickOnAcivateContract();
			}
			waitPageLoaded();
		}
		assertEquals(contractPage.contractStatusIsActivated(), 1);
	}
	
	public void searchContractByCode(String code) {
		ListOfContractsPage listOfContractsPage =  PageFactory.initElements(driver, ListOfContractsPage.class);
		listOfContractsPage.clickOnFilterButton();
		waitPageLoaded();
		listOfContractsPage.clickOnFilterByCodeButton();
		waitPageLoaded();
		listOfContractsPage.setCode(code);
		waitPageLoaded();
		listOfContractsPage.goToContractDetails();
		waitPageLoaded();
	}
	
	public void closeContract() {
		ContractPage contractPage =  PageFactory.initElements(driver, ContractPage.class);
		try {
			contractPage.clickOnActionButton();
		}catch (Exception e) {
			ListOfContractsPage listOfContractsPage =  PageFactory.initElements(driver, ListOfContractsPage.class);
			listOfContractsPage.goToContractDetails();
			contractPage.clickOnActionButton();
		}
		try {
			contractPage.clickOnCloseContract();
		}catch (Exception e) {
			contractPage.clickOnActionButton();
			contractPage.clickOnCloseContract();
		}
		
		assertTrue(contractPage.contractStatusIsClosed());
	}
	
	public void addBillingRule(String criteriaEL, String invoicedBillingAccountEL) {
		
	}
}
