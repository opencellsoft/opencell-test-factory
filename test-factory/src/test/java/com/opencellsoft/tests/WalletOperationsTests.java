package com.opencellsoft.tests;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.operations.walletoperations.WalletOperationsPage;

public class WalletOperationsTests extends TestBase {
	WalletOperationsPage walletOperationsPage;
	/***   search by Subscription code ***/
	public void searchBySubscriptionCode(String sub){
		// search by Subscription code
		walletOperationsPage = new WalletOperationsPage();
		walletOperationsPage.clickOnFilterButton();
		waitPageLoaded();
		try {
			walletOperationsPage.selectFilterBySubscription();
		}catch (Exception ex) {
			walletOperationsPage.selectFirstChoice();
		}	
		
		try {
			/*     14.1.X & 15.1.X   */
			walletOperationsPage.clickOnFilterBySubscription();
			waitPageLoaded();
			walletOperationsPage.setSubscriptionCodeInput(sub);
			waitPageLoaded();
			walletOperationsPage.selectSubscription();
			waitPageLoaded();
		}catch (Exception e) {
			/*     15.0.X   */		
			walletOperationsPage.setSubscriptionInput(sub);
			waitPageLoaded();
		}
	}

	// return the number of lines found
	public int elementsFoundSize(String value) {
		walletOperationsPage = new WalletOperationsPage();
		return walletOperationsPage.elementsFoundSize(value);
	}

}
