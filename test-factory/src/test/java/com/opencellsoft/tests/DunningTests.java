package com.opencellsoft.tests;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.customercare.collectionplans.CollectionPlansListPage;
import com.opencellsoft.pages.customercare.contacts.ContactPage;
import com.opencellsoft.pages.customercare.contacts.ContactsListPage;
import com.opencellsoft.pages.customercare.dunning.ActionPage;
import com.opencellsoft.pages.customercare.dunning.ActionsListPage;
import com.opencellsoft.pages.customercare.dunning.DunningSettingsPage;
import com.opencellsoft.pages.customercare.dunning.LevelPage;
import com.opencellsoft.pages.customercare.dunning.LevelsListPage;
import com.opencellsoft.pages.customercare.dunning.PoliciesListPage;
import com.opencellsoft.pages.customercare.dunning.PolicyPage;
import com.opencellsoft.pages.customercare.dunning.TemplatePage;
import com.opencellsoft.pages.customercare.dunning.TemplatesListPage;

public class DunningTests extends TestBase  {
	
	//////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////   Templates tests  ///////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////
	TemplatesListPage templatesListPage;
	TemplatePage templatePage;
	
	public void createTemplate(String template_name,String template_language, String template_channel, String template_object, String template_body) {
		templatesListPage= new TemplatesListPage();
		templatesListPage.clickOnCreateButton();
		
		templatePage = new TemplatePage();
		templatePage.setName(template_name);
		templatePage.setLanguage(template_language);
		templatePage.setChannel(template_channel);
		templatePage.setObject(template_object);
		templatePage.setBody(template_body);
		templatePage.clickOnSaveButton();
	}
	
	public void updateTemplate(String template_name,String template_language,Boolean template_active, String template_channel, String template_object, String template_body) {
		templatePage = new TemplatePage();
		if(template_name != null) {templatePage.setName(template_name);}
		if(template_language != null) {templatePage.setLanguage(template_language);}
		if(template_active != null) {templatePage.setActive(template_active);}
		if(template_channel != null) {templatePage.setChannel(template_channel);}
		if(template_object != null) {templatePage.setObject(template_object);}
		if(template_body != null) {templatePage.setBody(template_body);}
		templatePage.clickOnSaveButton();
	}
	
	public void delete() {
		templatePage = new TemplatePage();
		templatePage.clickOnDeleteButton();
		templatePage.clickOnConfirmButton();
	}
	
	public void goBack() {
		templatePage = new TemplatePage();
		templatePage.clickOnGoBackButton();
	}
	
	public void searchTemplateByName(String value) {
		templatesListPage= new TemplatesListPage();
		templatesListPage.setSearchInput(value);
	}
	
	public int templateTotal() {
		templatesListPage= new TemplatesListPage();
		return templatesListPage.templatesList().size();
	}
	
	public void goToTemplateDetails (String email) {
		templatesListPage= new TemplatesListPage();
		templatesListPage.goToTemplateDetails(email);
	}
	public void activateFilterBy(String value) {
		templatesListPage= new TemplatesListPage();
		templatesListPage.clickOnFilterButton();
		templatesListPage.activateFilterBy(value);
	}
	
	public void filterTemplateByLanguage(String value) {
		templatesListPage= new TemplatesListPage();
		templatesListPage.clickOnLanguageInput();
		templatesListPage.filterBy(value);
	}
	
	public void filterTemplateByChannel(String value) {
		templatesListPage= new TemplatesListPage();
		templatesListPage.clickOnChannelInput();
		templatesListPage.filterBy(value);
	}
	
	public void sortTemplatesListBy(String value) {
		templatesListPage = new TemplatesListPage();
		templatesListPage.sortBy(value);
	}
	
	public void closeFilterBy(String value) {
		templatesListPage= new TemplatesListPage();
		templatesListPage.clickOnRemoveFilterBy(value);
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////   Actions tests   ////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////
	
	ActionsListPage actionsListPage;
	ActionPage actionPage;
	
	public void createAction(String action_name,String action_description, String action_type) {
		actionsListPage= new ActionsListPage();
		actionsListPage.clickOnCreateButton();
		
		actionPage = new ActionPage();
		actionPage.setName(action_name);
		actionPage.setDescription(action_description);
		actionPage.setType(action_type);
		actionPage.clickOnSaveButton();
	}
	
	public void updateAction(String action_name,String action_description, String action_type) {
		actionPage = new ActionPage();
		actionPage.clickOnUpdateButton();
		if(action_name != null) {actionPage.setName(action_name);}
		if(action_description != null) {actionPage.setDescription(action_description);}
		if(action_type != null) {actionPage.setType(action_type);}
		actionPage.clickOnSaveButton();
	}
	
	public void searchActionByName(String value) {
		actionsListPage= new ActionsListPage();
		actionsListPage.setSearchInput(value);
	}
	
	public int actionsTotal() {
		actionsListPage= new ActionsListPage();
		return actionsListPage.actionsList().size();
	}
	
	public void goToActionDetails (String email) {
		actionsListPage= new ActionsListPage();
		actionsListPage.goToActionDetails(email);
	}
	
	public void sortActionsListBy(String value) {
		actionsListPage= new ActionsListPage();
		actionsListPage.sortBy(value);
	}
	//////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////   levels tests   ////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////
	
	LevelsListPage levelsListPage;
	LevelPage levelPage;
	
	public void createLevel(String level_name,String level_description,String level_daysOverdue,Boolean level_endOfDunning, String action_name) {
		levelsListPage= new LevelsListPage();
		levelsListPage.clickOnCreateButton();
		
		levelPage = new LevelPage();
		levelPage.setName(level_name);
		levelPage.setDescription(level_description);
		levelPage.setDaysOverdue(level_daysOverdue);
		if(level_endOfDunning != null) {levelPage.setEndOfDunning(level_endOfDunning);}
		if(action_name != null) {pickActionToLevel(action_name);}
		levelPage.clickOnSaveButton();
	}
	
	public void updateLevel(String level_name,String level_description,String level_daysOverdue,Boolean level_active) {
		levelPage = new LevelPage();
		if(level_name != null) {levelPage.setName(level_name);}
		if(level_description != null) {levelPage.setDescription(level_description);}
		if(level_daysOverdue != null) {levelPage.setDaysOverdue(level_daysOverdue);}
		if(level_active != null) {levelPage.setActive(level_active);}
		levelPage.clickOnSaveButton();
	}
	
	public void searchLevelByName(String value) {
		levelsListPage= new LevelsListPage();
		levelsListPage.setSearchInput(value);
	}
	
	public int levelsTotal() {
		levelsListPage= new LevelsListPage();
		return levelsListPage.levelsList().size();
	}
	
	public int noResultsFound() {
		levelsListPage= new LevelsListPage();
		return levelsListPage.noResultsFound().size();
	}
	
	
	public void goToLevelDetails (String email) {
		levelsListPage= new LevelsListPage();
		levelsListPage.goToLevelDetails(email);
	}
	
	public void sortLevelsListBy(String value) {
		levelsListPage= new LevelsListPage();
		levelsListPage.sortBy(value);
	}
	
	public void pickActionToLevel(String action_name) {
		levelPage = new LevelPage();
		levelPage.clickOnPickButton();
		levelPage.setSearchByActionName(action_name);
		levelPage.clickOnFirstResult();
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////   Policies tests   ///////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////
	PoliciesListPage policiesListPage;
	PolicyPage policyPage;
	
	public void createPolicy(String policy_name,String policy_description,String level_name) {
		policiesListPage= new PoliciesListPage();
		policiesListPage.clickOnCreateButton();
		
		policyPage = new PolicyPage();
		policyPage.setName(policy_name);
		policyPage.setDescription(policy_description);
		policyPage.pickLevelToPolicy(level_name);
		policyPage.clickOnSaveButton();
	}
	
	public void updatePolicy(String policy_name,String policy_description,Boolean policy_active) {
		policyPage = new PolicyPage();
		policyPage.clickOnUpdateButton();
		if(policy_name != null) {policyPage.setName(policy_name);}
		if(policy_description != null) {policyPage.setDescription(policy_description);}
		if(policy_active != null) {policyPage.setActive(policy_active);}

		policyPage.clickOnSaveButton();
	}
	
	public void ApplyPolicyto(String inEditRule,String operator,String editValue) {
		policyPage = new PolicyPage();
		policyPage.clickOnUpdateButton();
		policyPage.clickOnAddLineButton();
		policyPage.setInEditRule(inEditRule);
		if(operator != null) {policyPage.setOperator(operator);}
		policyPage.setEditValue(editValue);
		
		policyPage.clickOnSaveButton();
		
	}
	public void searchPolicyByName(String value) {
		policiesListPage= new PoliciesListPage();
		policiesListPage.setSearchInput(value);
	}
	
	public int policyTotal() {
		policiesListPage= new PoliciesListPage();
		return policiesListPage.policiesList().size();
	}
	
	public void goToPolicyDetails (String email) {
		policiesListPage= new PoliciesListPage();
		policiesListPage.goToPolicyDetails(email);
	}
	
	public void filterPolicyByActive(Boolean value) {
		policiesListPage= new PoliciesListPage();
		policiesListPage.setActive(value);
	}
	
	public void filterPolicyByDescription(String value) {
		policiesListPage= new PoliciesListPage();
		policiesListPage.setDescriptionInput(value);
	}
	
	public void sortPoliciesListBy(String value) {
		policiesListPage= new PoliciesListPage();
		policiesListPage.sortBy(value);
	}
	//////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////   Dunning Settings  //////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////	
	
	DunningSettingsPage dunningSettingsPage;
	public void UpdateMaximumNumberOfDunningLevels(String value) {
		dunningSettingsPage= new DunningSettingsPage();
		dunningSettingsPage.setMaxDunningLevelsInput(value);
		dunningSettingsPage.clickOnSave1Button();
	}
	
	public String MaximumNumberOfDunningLevelsValue() {
		dunningSettingsPage= new DunningSettingsPage();
		return dunningSettingsPage.getSetMaxDunningLevelsInput();
	}
	public void AddPauseReason(String pausReason,String pauseDescription, String line) {
		dunningSettingsPage= new DunningSettingsPage();
		dunningSettingsPage.setPausReasonInput(pausReason, line);
		dunningSettingsPage.setPauseDescriptionInput(pauseDescription, line);
		dunningSettingsPage.clickOnSave2Button();
	}
	
	public void openStopReasontab(){
		dunningSettingsPage= new DunningSettingsPage();
		dunningSettingsPage.clickOnStopReasonButton();
	}
	
	public void AddStopReason(String stopReason,String stopDescription, String line) {
		dunningSettingsPage= new DunningSettingsPage();
		
		dunningSettingsPage.setStopReasonInput(stopReason, line);
		dunningSettingsPage.setStopDescriptionInput(stopDescription, line);
		dunningSettingsPage.clickOnSave2Button();
	}
	
	public Boolean searchInPauseReasonTable(String pauseReason){
		dunningSettingsPage= new DunningSettingsPage();
		return dunningSettingsPage.searchInPauseReasonTable(pauseReason);
	}
	
	public Boolean searchInStopReasonTable(String stopReason){
		dunningSettingsPage= new DunningSettingsPage();
		return dunningSettingsPage.searchInStopReasonTable(stopReason);
	}
	
}
