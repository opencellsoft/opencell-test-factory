package com.opencellsoft.tests;

import java.awt.AWTException;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.catalog.discountplans.DiscountLinePage;
import com.opencellsoft.pages.catalog.discountplans.DiscountPlanPage;
import com.opencellsoft.pages.catalog.discountplans.DiscountPlansListPage;
import com.opencellsoft.pages.customercare.dunning.TemplatePage;
import com.opencellsoft.pages.customercare.dunning.TemplatesListPage;

public class DiscountPlansTests extends TestBase {

	DiscountPlanPage discountPlanPage;
	DiscountPlansListPage discountPlansListPage;
	DiscountLinePage discountLinePage;
	
	/***  Create a new Discount Plan  ***/
	public void createDiscountPlan(String code,String label, String type) throws InterruptedException, AWTException {
		discountPlansListPage = new DiscountPlansListPage();
		discountPlansListPage.clickOnCreateDiscountPlanButton();

		discountPlanPage = new DiscountPlanPage();
		discountPlanPage.setCode(code);
		discountPlanPage.setLabel(label);
		discountPlanPage.setType(type);
		discountPlanPage.clickOnSaveButton();
	}

	/***  update Discount Plan  ***/
	public void updateDiscountPlan(String label,String type) throws InterruptedException, AWTException {
		discountPlanPage = new DiscountPlanPage();
		if(label != null) {discountPlanPage.setLabel(label);}
		if(type != null) {discountPlanPage.setType(type);}
		discountPlanPage.clickOnSaveButton();
	}
	
	public void activateDiscountPlan() {
		discountPlanPage = new DiscountPlanPage();
		discountPlanPage.clickOnActivateButton();
	}
	
	public void goBack() {
		discountPlanPage = new DiscountPlanPage();
		discountPlanPage.clickOnGoBackButton();
	}

	public void searchDiscountPlan(String value) {
		discountPlansListPage = new DiscountPlansListPage();
		discountPlansListPage.setSearchInput(value);
	}
	
	public int discountPlansTotal() {
		discountPlansListPage = new DiscountPlansListPage();
		return discountPlansListPage.discountPlansList().size();
	}
	
	public void sortDiscountPlansListBy(String value) {
		discountPlansListPage = new DiscountPlansListPage();
		discountPlansListPage.sortBy(value);
	}
	
	public void goToDiscountPlanDetails (String code) {
		discountPlansListPage = new DiscountPlansListPage();
		discountPlansListPage.goToDiscountPlanDetails(code);
	}
	
	/***  Create a new Discount Plan  ***/
	public void createDiscountLine(String code,String label, String type) throws InterruptedException, AWTException {
		discountPlanPage = new DiscountPlanPage();
		discountPlanPage.clickOnCreateDiscountLineButton();
		
		discountLinePage = new DiscountLinePage();
		discountLinePage.setLabel(label);
		discountLinePage.setDiscountValue(type);
		discountLinePage.clickOnSaveButton();
	}
	
	public void activateFilterBy(String value) {
		discountPlansListPage = new DiscountPlansListPage();
		discountPlansListPage.clickOnFilterButton();
		discountPlansListPage.activateFilterBy(value);
	}
	
	public void filterDiscountPlanByType(String value) {
		discountPlansListPage = new DiscountPlansListPage();
		discountPlansListPage.clickOnTypeInput();
		discountPlansListPage.filterBy(value);
	}
	public void closeFilterBy(String value) {
		discountPlansListPage = new DiscountPlansListPage();
		discountPlansListPage.clickOnRemoveFilterBy(value);
	}
	
	public void delete() {
		discountLinePage = new DiscountLinePage();
		discountLinePage.clickOnDeleteButton();
		discountLinePage.clickOnConfirmButton();
	}
}
