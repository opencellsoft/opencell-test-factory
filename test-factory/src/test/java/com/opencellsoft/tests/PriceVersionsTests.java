package com.opencellsoft.tests;

import org.openqa.selenium.By;
import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.catalog.catalogmanager.priceversions.PriceVersionsPage;

public class PriceVersionsTests extends TestBase  {

	PriceVersionsPage priceVersionsPage;
	
	public void filterByStatus(String value, String value2) {
		priceVersionsPage = new PriceVersionsPage();
		priceVersionsPage.selectStatus(value, value2);
	}
	
	public int getPriceversionsTotalWithStatus(String value, String value2){
		return driver.findElements(By.xpath("(//table)[1]//*[text() ='" + value + "' or text() = '" + value2 + "']")).size();
	}
	
	public void exportPriceVersions() {
		priceVersionsPage = new PriceVersionsPage();
		priceVersionsPage.clickOnSelectAllCheckBox();
		priceVersionsPage.clickOnAddButton();
		priceVersionsPage.clickOnExportButton();
	}
	
	public void importPriceVersions() {
		priceVersionsPage = new PriceVersionsPage();
		priceVersionsPage.clickOnUploadFileButton();
//		priceVersionsPage.setStartDate(8);
		priceVersionsPage.clickOnValidateButton();
	}
	
	public String getExecutionMessage() {
		priceVersionsPage = new PriceVersionsPage();
		return priceVersionsPage.getExecutionMessage();
	}
}
