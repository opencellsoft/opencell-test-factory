package com.opencellsoft.tests.customercare;

import java.awt.AWTException;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.customercare.subscriptions.ListOfSubscriptionsPage;
import com.opencellsoft.pages.customercare.subscriptions.NewSubscriptionPage;
import com.opencellsoft.pages.customercare.subscriptions.SubscriptionPage;
import com.opencellsoft.pages.customers.CustomerPage;
import com.opencellsoft.pages.customers.ListOfCustomersPage;
import com.opencellsoft.pages.subscriptions.AccessPointsPage;
import com.opencellsoft.pages.subscriptions.SubscriptionsListPage;
import com.opencellsoft.utility.Constant;

public class SubscriptionsTests extends TestBase {

	@Parameters({ "selectSubscription" })
	@Test(priority = 1)
	public void checkSubscriptionDetails(String selectSubscription) throws InterruptedException {
		HomePage homepage = PageFactory.initElements(driver, HomePage.class);
		homepage.clickOnSubscriptionsButton();
		ListOfSubscriptionsPage subscriptionsListPage = PageFactory.initElements(driver, ListOfSubscriptionsPage.class);
		subscriptionsListPage.searchForSubscription(selectSubscription);
		subscriptionsListPage.clicOnSubscription();
	}

	@Parameters({ "billingCycle","quantity"})
	@Test(priority = 1)
	public void  createSubscriptionFromCustomerView(String billingCycle, String quantity) throws InterruptedException, AWTException {
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		waitPageLoaded();
		customerPage.clickOnSubscriptionTab();
		waitPageLoaded();
		customerPage.clickOnCreateSubscriptionButton();		
		NewSubscriptionPage newSubscriptionPage = PageFactory.initElements(driver, NewSubscriptionPage.class);
		waitPageLoaded();
		newSubscriptionPage.clickOnConsumerInput();
		newSubscriptionPage.selectConsumer();
		newSubscriptionPage.clickOnBillingCycleInput();
		waitPageLoaded();
		newSubscriptionPage.selectBillingCycleOption(billingCycle);
		//newSubscriptionPage.selectSalesPerson("opencell.admin");
		newSubscriptionPage.setSubscriptionCode(Constant.subscriptionCode);
		newSubscriptionPage.clickOnOfferInput();
		newSubscriptionPage.setOffername(Constant.offerDescription);
		newSubscriptionPage.selectOfferByName(Constant.offerDescription);
		//newSubscriptionPage.clickOnAddFilterButton();
		//newSubscriptionPage.selectFilterOption();
		//newSubscriptionPage.setOfferCodefilter(Constant.offerCode);
		waitPageLoaded();
		//newSubscriptionPage.clickOnSortButton();
		newSubscriptionPage.selectOffer(Constant.offerCode);
		newSubscriptionPage.clickOnProductDiv();
		//newSubscriptionPage.setQuantity(quantity);	
		waitPageLoaded();
		//		newSubscriptionPage.setAttribute();	
		newSubscriptionPage.clickOnSaveSubscriptionButton(); 
		SubscriptionPage subscriptionPage = PageFactory.initElements(driver, SubscriptionPage.class);
		waitPageLoaded();
		Assert.assertEquals(subscriptionPage.getTextMessage(), "Element created");
		//newSubscriptionPage.clickOnGotItButton();
	}

	@Parameters({"quantity" })
	@Test(priority =2)
	public void addProductToSubscription(String quantity) throws AWTException, InterruptedException {
		SubscriptionPage subscriptionPage = PageFactory.initElements(driver, SubscriptionPage.class);		
		subscriptionPage.clickOnAddProductButton();
		waitPageLoaded();
		subscriptionPage.clickOnProduct(Constant.productCode);
	}

	@Parameters({"quantity" })
	@Test(priority =3)
	public void modifySubscriptionQuantity(String quantity) throws AWTException, InterruptedException {
		SubscriptionPage subscriptionPage = PageFactory.initElements(driver, SubscriptionPage.class);		
		subscriptionPage.clickOnSelectedProductDiv();
		waitPageLoaded();
		subscriptionPage.setQuantity(quantity);
		subscriptionPage.clickOnProductCode(Constant.productCode);	
		subscriptionPage.clickOnSaveSubscriptionButton(); 
		waitPageLoaded();
		Assert.assertEquals(subscriptionPage.getTextMessage(), "Element updated");
	}

	@Test(priority =4)
	public void modifySubscriptionProductAttributes() throws AWTException  {
		SubscriptionPage subscriptionPage = PageFactory.initElements(driver, SubscriptionPage.class);		
		subscriptionPage.clickOnSelectedProductDiv();
		waitPageLoaded();
		subscriptionPage.clickOnProductName(Constant.productDescription);	
		waitPageLoaded();
		subscriptionPage.clickOnAttributesTab();
		subscriptionPage.setAttribute();	
	}

	@Test(priority=5)
	public void activateProductInstance() throws InterruptedException, AWTException {
		SubscriptionPage subscriptionPage = PageFactory.initElements(driver, SubscriptionPage.class);
		subscriptionPage.clickOnActivateProductInstanceButton();
		subscriptionPage.clickOnConfirmActivationProduct();
		waitPageLoaded();
		subscriptionPage.clickOnGoBackButton();
		waitPageLoaded();
		subscriptionPage.clickOnGoBackButton();
	}

	@Parameters({"quantity" })
	@Test(priority =10)
	public void editSubscriptionProductAttributes(String quantity) throws AWTException, InterruptedException {
		SubscriptionPage subscriptionPage = PageFactory.initElements(driver, SubscriptionPage.class);		
		subscriptionPage.clickOnSelectedProductDiv();
		waitPageLoaded();
		subscriptionPage.setQuantity(quantity);
		subscriptionPage.setAttribute();	
		waitPageLoaded();
		subscriptionPage.clickOnSaveSubscriptionButton();	
		waitPageLoaded();
		Assert.assertEquals(subscriptionPage.getTextMessage(), "Element updated");
	}


	@Test(priority=15)
	public void activateProductInstanceFromSubscriptionPage() throws InterruptedException, AWTException {
		SubscriptionPage subscriptionPage = PageFactory.initElements(driver, SubscriptionPage.class);
		//subscriptionPage.clickOnSelectedProductDiv();	
		//subscriptionPage.clickOnActivateProductButton();
		subscriptionPage.clickOnConfirmActivationProduct();
		Thread.sleep(2000);
		Assert.assertEquals(subscriptionPage.getTextMessage(), "Success");
		Thread.sleep(1000);
		subscriptionPage.clickOnGoBackButton();
	}

	@Test(priority =15)
	public void activateAttributeInstances() throws AWTException, InterruptedException {
		SubscriptionPage subscriptionPage = PageFactory.initElements(driver, SubscriptionPage.class);
		//subscriptionPage.clickOnSelectedProductDiv();	
		subscriptionPage.clickOnActivateAttributeInstancesButton();	
	}	

	@Test(priority = 6, groups = { "perf tests" })
	public void checkSubscriptionsList() { //Check the Subscriptions list page
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCustomerCareButton();
		homePage.clickOnSubscriptionsButton();
		ListOfSubscriptionsPage subscriptionsListPage = PageFactory.initElements(driver, ListOfSubscriptionsPage.class);
		subscriptionsListPage.checkBreadCrumb("Customer Care");
		subscriptionsListPage.checkBreadCrumb("subscriptions");
	}

	@Test(priority = 7, groups = { "perf tests" }) 
	public void checkPaginationSubscriptionsList() { 
		ListOfSubscriptionsPage subscriptionsListPage = PageFactory.initElements(driver, ListOfSubscriptionsPage.class);
		subscriptionsListPage.checkPagination();
	}	

	@Parameters({ "customerCode" })
	@Test(priority = 8, groups = { "perf tests" }) 
	public void checkSubscriptionDetail(String customerCode) { 
		ListOfSubscriptionsPage subscriptionsListPage = PageFactory.initElements(driver, ListOfSubscriptionsPage.class);
		//subscriptionsListPage.setSearchBarText(customerCode);
		subscriptionsListPage.clicOnSubscription();
		//SubscriptionPage subscriptionPage = PageFactory.initElements(driver, SubscriptionPage.class);	
	}

	/***  Create a new subscription from subscription tab in customer details page  ***/
	public void create_subscription_FromCustomerPage ( String seller, String subCode, String OfferCode) throws AWTException, InterruptedException {
		// Go to subscription tab
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		try {
			customerPage.clickOnSubscriptionTab2();
		}catch (Exception e) {
			ListOfCustomersPage listOfCustomersPage = PageFactory.initElements(driver, ListOfCustomersPage.class);
			listOfCustomersPage.clickCustomer(null);
			customerPage.clickOnSubscriptionTab2();
		}
		

		// create a new subscription
		customerPage.clickOnCreateSubscription();		
		waitPageLoaded();
		NewSubscriptionPage newSubscriptionPage = PageFactory.initElements(driver, NewSubscriptionPage.class);
		newSubscriptionPage.clickOnConsumerInput();
		newSubscriptionPage.selectConsumer();
		newSubscriptionPage.setSubscriptionCode(subCode);
		newSubscriptionPage.setSalesPerson();
		newSubscriptionPage.selectSeller(seller);
		waitPageLoaded();
		newSubscriptionPage.clickOnOfferInput();
		waitPageLoaded();
		newSubscriptionPage.clickOnAddFilterButton();
		waitPageLoaded();
		try {
			newSubscriptionPage.selectFilterOption();
		}catch (Exception e) {
			newSubscriptionPage.clickOnAddFilterButton();
			waitPageLoaded();
			newSubscriptionPage.selectFilterOption();
		}
		
		waitPageLoaded();
		newSubscriptionPage.setOfferCodefilter(OfferCode);
		waitPageLoaded();
		newSubscriptionPage.selectOffer(OfferCode);
		waitPageLoaded();
		newSubscriptionPage.clickOnSaveSubscriptionButton(); 
		waitPageLoaded();
		activateRenewAfterInitialTerm();
	}
	
	/***  Create a new subscription from subscriptions list  ***/
	public void create_subscription_FromSubscriptionsListPage ( String seller, String customerCode,String subCode, String OfferCode) throws AWTException, InterruptedException {
		
		SubscriptionsListPage subscriptionsListPage = new SubscriptionsListPage();
		// create a new subscription
		subscriptionsListPage.clickAddNewSubscription();	
		
		NewSubscriptionPage newSubscriptionPage = PageFactory.initElements(driver, NewSubscriptionPage.class);
		newSubscriptionPage.setCustomer(customerCode);
		newSubscriptionPage.clickOnConsumerInput();
		newSubscriptionPage.selectConsumer();
		newSubscriptionPage.setSalesPerson();
		newSubscriptionPage.selectSeller(seller);
		newSubscriptionPage.setOffer(OfferCode);
		newSubscriptionPage.setSubscriptionCode(subCode);
		newSubscriptionPage.clickOnSaveButton(); 
		activateRenewAfterInitialTerm();
	}
	
	/***  Activate  Renew After Initial Term***/
	public void activateRenewAfterInitialTerm() {
		NewSubscriptionPage newSubscriptionPage = PageFactory.initElements(driver, NewSubscriptionPage.class);
		newSubscriptionPage.clickOnRenewalConditionsTab();
		newSubscriptionPage.clickOnRenewAfterInitialTerm();
		newSubscriptionPage.clickOnSaveSubscriptionButton(); 
		waitPageLoaded();
	}

	/***  Create Access point  ***/
	public void createAccessPointMethod(String AP) {
		// go to access point tab
		SubscriptionPage subscriptionPage = PageFactory.initElements(driver, SubscriptionPage.class);
		subscriptionPage.clickOnAccessPointTab();

		// create an access point
		subscriptionPage.clickOnCreateAPButton();

		AccessPointsPage accessPointsPage = new AccessPointsPage();
		accessPointsPage.setAccessPointCode(AP);
		accessPointsPage.clickOnSaveButton();

		// go back to subscription page
		accessPointsPage.clickOnGoBackLink();
	}

	public void goBack() {
		AccessPointsPage accessPointsPage = new AccessPointsPage();
		accessPointsPage.clickOnGoBackButton();
	}

	/***  Activate the product instance for the subscription  ***/
	public void activateProductInstanceMethod() throws InterruptedException, AWTException {	
		SubscriptionPage subscriptionPage = PageFactory.initElements(driver, SubscriptionPage.class);
		subscriptionPage.clickOnGotItButton();
		try {
			subscriptionPage.clickOnSelectedProductDiv();
		}catch (Exception e) {
			try {
				subscriptionPage.clickOnAddProductButton();
				subscriptionPage.clickOnProduct();
				subscriptionPage.clickOnSelectedProductDiv();		
			}catch (Exception ex) {

				subscriptionPage.clickOnGoBackButton();
				subscriptionPage.clickOnSelectedProductDiv();
			}

		}
		subscriptionPage.clickOnActivateProductButton();
		subscriptionPage.clickOnConfirmActivationProduct();
		subscriptionPage.clickOnGotItButton();
		subscriptionPage.goToCustomer();
	}
	
	/***  Activate the product instance with attributes   ***/
	public void activateProductInstanceWithAttribute1(String integerAttribut, String numericAttribut, String booleanAttribut, String textAttribut, String version) throws InterruptedException, AWTException {	
		SubscriptionPage subscriptionPage = PageFactory.initElements(driver, SubscriptionPage.class);
		try {
			subscriptionPage.clickOnSelectedProductDiv();
		}catch (Exception e) {
			try {
				subscriptionPage.clickOnAddProductButton();
				subscriptionPage.clickOnProduct(null);
				subscriptionPage.clickOnSelectedProductDiv();		
			}catch (Exception ex) {

				subscriptionPage.clickOnGoBackButton();
				subscriptionPage.clickOnSelectedProductDiv();
			}
		}
		subscriptionPage.clickOnGotItButton();
		if(version.equals("14.1.X")) {
			subscriptionPage.setAttributes141X(integerAttribut, numericAttribut, booleanAttribut, textAttribut, version);
		}else if(version.equals("15.X") || version.equals("15.0.X")) {
			subscriptionPage.setAttributes150X(integerAttribut, numericAttribut, booleanAttribut, textAttribut, version);
		}else {
			subscriptionPage.setAttributes16X(integerAttribut, numericAttribut, booleanAttribut, textAttribut, version);
		}
		subscriptionPage.clickOnSaveSubscriptionButton();
		waitPageLoaded();
		subscriptionPage.clickOnSelectedProductDiv();
		waitPageLoaded();
		subscriptionPage.clickOnActivateProductButton();
		waitPageLoaded();
		subscriptionPage.clickOnConfirmActivationProduct();
		waitPageLoaded();
		subscriptionPage.clickOnGotItButton();
		subscriptionPage.goToCustomer();
		waitPageLoaded();
	}
	
	
	/***  Activate the product instance with attributes   ***/
	public void activateProductInstanceWithAttribute2(String booleanattr, String dateattr, String elattr, String emailattr, String informationattr, String integerattr, String listofnumericvaluesattr, String listoftextvaluesattr, String ml_numericvaluesattr, String ml_textvaluesattr, String numericattr, String phoneattribut, String textattr, String version) throws InterruptedException, AWTException {	
		SubscriptionPage subscriptionPage = PageFactory.initElements(driver, SubscriptionPage.class);
		try {
			subscriptionPage.clickOnSelectedProductDiv();
		}catch (Exception e) {
			try {
				subscriptionPage.clickOnAddProductButton();
				subscriptionPage.clickOnProduct(null);
				subscriptionPage.clickOnSelectedProductDiv();		
			}catch (Exception ex) {

				subscriptionPage.clickOnGoBackButton();
				subscriptionPage.clickOnSelectedProductDiv();
			}
		}
		
		subscriptionPage.clickOnGotItButton();
		
		if(version.equals("14.1.X")) {
			subscriptionPage.setAttributes141X2(booleanattr, dateattr, elattr, emailattr, informationattr, integerattr, listofnumericvaluesattr, listoftextvaluesattr, ml_numericvaluesattr, ml_textvaluesattr, numericattr, phoneattribut, textattr, version);
		}else if(version.equals("15.X") || version.equals("15.0.X")) {
			subscriptionPage.setAttributes150X2(booleanattr, dateattr, elattr, emailattr, informationattr, integerattr, listofnumericvaluesattr, listoftextvaluesattr, ml_numericvaluesattr, ml_textvaluesattr, numericattr, phoneattribut, textattr, version);
		}else {
			subscriptionPage.setAttributes16X2( booleanattr, dateattr, elattr, emailattr, informationattr, integerattr, listofnumericvaluesattr, listoftextvaluesattr, ml_numericvaluesattr, ml_textvaluesattr, numericattr, phoneattribut, textattr, version);
		}
		subscriptionPage.clickOnSaveSubscriptionButton();
		waitPageLoaded();
		subscriptionPage.clickOnSelectedProductDiv();
		waitPageLoaded();
		subscriptionPage.clickOnActivateProductButton();
		waitPageLoaded();
		subscriptionPage.clickOnConfirmActivationProduct();
		waitPageLoaded();
		subscriptionPage.clickOnGotItButton();
		subscriptionPage.clickOnGoBack();
		waitPageLoaded();
	}


}
