package com.opencellsoft.tests.customercare;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.customercare.contracts.ContractPage;
import com.opencellsoft.pages.customercare.contracts.ListOfContractsPage;
import com.opencellsoft.pages.customercare.contracts.NewContractLinePage;
import com.opencellsoft.pages.customercare.contracts.NewContractPage;
import com.opencellsoft.tests.CustomersTests;
import com.opencellsoft.utility.Constant;

public class ContractsTests extends TestBase{

	@Test(priority = 1)
	public void createNewContract() throws InterruptedException {
	HomePage homePage= PageFactory.initElements(driver, HomePage.class);
	homePage.clickOnContractsButton();
	ListOfContractsPage listOfContractsPage= PageFactory.initElements(driver, ListOfContractsPage.class);
	listOfContractsPage.clickOnCreateNewContractButton();
	NewContractPage newContractPage= PageFactory.initElements(driver, NewContractPage.class);
	newContractPage.setContractCode(Constant.contractCode);
	newContractPage.setContractDescription(Constant.contractDescription);
	newContractPage.clickOnContractStartDateInput();
	newContractPage.clickOnContractStartDateButton();
	newContractPage.clickOnContractEndDateInput();
	newContractPage.clickOnContractEndDateButton();
	newContractPage.clickOnContractAccountLevelInput();
	newContractPage.selectContractAccountLevelOption();
	newContractPage.clickOnContractAttachedEntityInput();
	newContractPage.clickOnFilterButton();
	newContractPage.clickOnFilterByCodeButton();
	newContractPage.setCodeFilter(Constant.customerCode);
	newContractPage.selectAttachedEntity(Constant.customerCode);
	newContractPage.clickOnContractDateInput();
	newContractPage.clickOnContractDateButton();
	newContractPage.clickOnSaveContractButton();
	Assert.assertEquals(newContractPage.getTextCreateContractMessage(), "Element created");
}
	
@Parameters({"discountRate"})
	@Test(priority = 2)
		public void createNewDiscountContractLine(String discountRate) throws InterruptedException {
			ContractPage contractPage= PageFactory.initElements(driver, ContractPage.class);
			contractPage.clickOnCreateContractLineButton();
			NewContractLinePage newContractLinePage= PageFactory.initElements(driver, NewContractLinePage.class);
			newContractLinePage.setContractLineCode(Constant.DiscountContractLineCode);
			newContractLinePage.setContractLineDescription(Constant.contractLineDescription);
			newContractLinePage.setDiscoutRate(discountRate);
			newContractLinePage.clickOnSaveButton();
			Assert.assertEquals(newContractLinePage.getTextMessage(), "Element created");
			newContractLinePage.clickOnGoBackButton();
		}
@Parameters({"contractPrice"})
@Test(priority=3)
public void createNewCustomPriceContractLine(String contractPrice) {
	ContractPage contractPage= PageFactory.initElements(driver, ContractPage.class);
	contractPage.clickOnCreateContractLineButton();
	NewContractLinePage newContractLinePage= PageFactory.initElements(driver, NewContractLinePage.class);
	newContractLinePage.setContractLineCode(Constant.ContractLinePriceGridCode);
	newContractLinePage.setContractLineDescription(Constant.contractLineDescription);
	newContractLinePage.clickOnChargeInput();
	newContractLinePage.clickOnChargeFilterButton();
	newContractLinePage.clickOnFilterByChargeNameButton();
	newContractLinePage.setChargeNameFilter(Constant.recurringChargeCode);
	newContractLinePage.selectCharge(Constant.recurringChargeCode);
	newContractLinePage.selectCustomContractPrice();	
	newContractLinePage.setContractPrice(contractPrice);	
	newContractLinePage.clickOnSaveButton();
	Assert.assertEquals(newContractLinePage.getTextMessage(), "Element created");
	newContractLinePage.clickOnGoBackButton();
}

	@Test(priority = 3)
		public void createNewCustomContractLinePriceGrid() throws InterruptedException {
		ContractPage contractPage= PageFactory.initElements(driver, ContractPage.class);
		contractPage.clickOnCreateContractLineButton();
		NewContractLinePage newContractLinePage= PageFactory.initElements(driver, NewContractLinePage.class);
		newContractLinePage.setContractLineCode(Constant.ContractLinePriceGridCode);
		newContractLinePage.setContractLineDescription(Constant.contractLineDescription);
		newContractLinePage.clickOnChargeInput();
		newContractLinePage.clickOnChargeFilterButton();
		Thread.sleep(1000);
		newContractLinePage.clickOnFilterByChargeNameButton();
		newContractLinePage.setChargeNameFilter(Constant.recurringChargeCode);
		newContractLinePage.selectCharge(Constant.recurringChargeCode);
		newContractLinePage.selectCustomContractPrice();
		newContractLinePage.clickOnPriceGridtoggle();
		newContractLinePage.clickOnDuplicateCharge();
		newContractLinePage.clickOnChargeFilterButton();
		newContractLinePage.clickOnFilterByChargeNameButton();
		newContractLinePage.setChargeNameFilter(Constant.recurringChargeCode);
		newContractLinePage.selectCharge(Constant.recurringChargeCode);
		newContractLinePage.clickOnDuplicatePriePlan();
		newContractLinePage.selectVisiblePricePlan(Constant.recurringChargeCode);
		newContractLinePage.selectPriceVersionToDuplicate();
		newContractLinePage.clickOnDuplicateButton();
		Assert.assertEquals(newContractLinePage.getTextMessage(), "Action executed successfuly");
		newContractLinePage.clickOnGoBackButton();
		}
		
	@Test(priority = 4)
	public void acivateContract() {
		ContractPage contractPage= PageFactory.initElements(driver, ContractPage.class);
		contractPage.clickOnAcivateContract();
		Assert.assertEquals(contractPage.getTextMessage(), "Action executed successfuly");	
		CustomersTests customerTest =  new CustomersTests();
		customerTest.searchCustomer(Constant.customerCode);
//		newContractLinePage.clickOnSaveButton();
		}
	

}
