package com.opencellsoft.tests.customercare;

import java.awt.AWTException;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.administration.billing.invoicingplans.InvoicingPlanLinesPage;
import com.opencellsoft.pages.administration.billing.invoicingplans.ListOfInvoicingPlansPage;
import com.opencellsoft.pages.administration.billing.invoicingplans.NewInvoicingPlanPage;
import com.opencellsoft.pages.charges.ChargePage;
import com.opencellsoft.pages.charges.NewChargePage;
import com.opencellsoft.pages.charges.NewPriceVersionpage;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.offers.ListOfOffersPage;
import com.opencellsoft.pages.offers.NewOfferPage;
import com.opencellsoft.pages.offers.OfferPage;
import com.opencellsoft.pages.orders.NewOrderPage;
import com.opencellsoft.pages.orders.OrderOfferPage;
import com.opencellsoft.pages.orders.OrderPage;
import com.opencellsoft.pages.orders.OrdersListPage;
import com.opencellsoft.pages.priceversions.PriceVersionPage;
import com.opencellsoft.pages.products.NewProductPage;
import com.opencellsoft.pages.products.ProductPage;
import com.opencellsoft.utility.Constant;

public class InvoicingPlansTests extends TestBase {

	@Test(priority = 1, groups = { "smoke tests" })
	public void createInvoicingPlan() throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		waitPageLoaded();
		homePage.clickOnCustomerCareButton();
		homePage.clickOnInvoicingPlansButton();
		ListOfInvoicingPlansPage listOfInvoicingPlansPage = PageFactory.initElements(driver,
				ListOfInvoicingPlansPage.class);
		waitPageLoaded();
		listOfInvoicingPlansPage.clickOnCreateInvoicePlanButton();
		NewInvoicingPlanPage newInvoicingPlanPage = PageFactory.initElements(driver, NewInvoicingPlanPage.class);
		newInvoicingPlanPage.setInvoicingName(Constant.invoicingPlanName);
		newInvoicingPlanPage.setInvoicingDescription(Constant.invoicingPlanDescription);
		newInvoicingPlanPage.clickSaveInvoicingPlanButton();
		Assert.assertEquals(newInvoicingPlanPage.getTextMessage(), "Element created");
		Assert.assertEquals(newInvoicingPlanPage.getInvoicingPlanCodeInputText(), Constant.invoicingPlanName);
		Assert.assertEquals(newInvoicingPlanPage.getInvoicingPlanDescriptionInputText(),
				Constant.invoicingPlanDescription);
	}

	@Parameters({ "advancementRate", "downPayementRate", "advancementRate2", "downPayementRate2", "advancementRate3",
			"downPayementRate3" })
	@Test(priority = 2, groups = { "smoke tests" })
	public void createInvoicingPlanLines(String advancementRate, String downPayementRate, String advancementRate2,
			String downPayementRate2, String advancementRate3, String downPayementRate3) throws InterruptedException {
		InvoicingPlanLinesPage invoicingPlanLinesPage = PageFactory.initElements(driver, InvoicingPlanLinesPage.class);
		waitPageLoaded();
		invoicingPlanLinesPage.clickOnCreateInvoicePlanLinesButton();
		invoicingPlanLinesPage.setCodeInvoicePlanLines(Constant.invoicingPlanLine1Code);
		invoicingPlanLinesPage.setDescriptionInvoicePlanLines(Constant.invoicingPlanLine1Code);
		invoicingPlanLinesPage.setInvoicingAdvancementRate(advancementRate);
		invoicingPlanLinesPage.setInvoicingDownPayementRate(downPayementRate);
		invoicingPlanLinesPage.clickOnSaveInvoicingPlanLinesButton();
		waitPageLoaded();
		Assert.assertEquals(invoicingPlanLinesPage.getTextMessage(), "Element created");
		invoicingPlanLinesPage.clickOnGoBackInvoicingPlanLinesButton();
		waitPageLoaded();
		invoicingPlanLinesPage.clickOnCreateInvoicePlanLinesButton();
		invoicingPlanLinesPage.setCodeInvoicePlanLines(Constant.invoicingPlanLine2Code);
		invoicingPlanLinesPage.setDescriptionInvoicePlanLines(Constant.invoicingPlanLine2Code);
		invoicingPlanLinesPage.setInvoicingAdvancementRate(advancementRate2);
		invoicingPlanLinesPage.setInvoicingDownPayementRate(downPayementRate2);
		invoicingPlanLinesPage.clickOnSaveInvoicingPlanLinesButton();
		Assert.assertEquals(invoicingPlanLinesPage.getTextMessage(), "Element created");
		invoicingPlanLinesPage.clickOnGoBackInvoicingPlanLinesButton();
		waitPageLoaded();
		invoicingPlanLinesPage.clickOnCreateInvoicePlanLinesButton();
		invoicingPlanLinesPage.setCodeInvoicePlanLines(Constant.invoicingPlanLine3Code);
		invoicingPlanLinesPage.setDescriptionInvoicePlanLines(Constant.invoicingPlanLine3Code);
		invoicingPlanLinesPage.setInvoicingAdvancementRate(advancementRate3);
		invoicingPlanLinesPage.setInvoicingDownPayementRate(downPayementRate3);
		invoicingPlanLinesPage.clickOnSaveInvoicingPlanLinesButton();
		Assert.assertEquals(invoicingPlanLinesPage.getTextMessage(), "Element created");
		invoicingPlanLinesPage.clickOnGoBackInvoicingPlanLinesButton();
		NewInvoicingPlanPage newInvoicingPlanPage = PageFactory.initElements(driver, NewInvoicingPlanPage.class);
		waitPageLoaded();
		newInvoicingPlanPage.clickSaveInvoicingPlanButton();
		Assert.assertEquals(newInvoicingPlanPage.getTextMessage(), "Element updated");
	}

	@Test(priority = 3)
	public void createOffer() {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		waitPageLoaded();
		homePage.clickOnCatalogButton();
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		waitPageLoaded();
		offerListPage.clickOnCreateOfferButton();
		NewOfferPage newOfferPage = PageFactory.initElements(driver, NewOfferPage.class);
		newOfferPage.setOfferCode(Constant.offerCode);
		newOfferPage.setOfferDescription(Constant.offerDescription);
		newOfferPage.setValidFromDate();
		newOfferPage.clickOnSubmitButton();
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		waitPageLoaded();
		Assert.assertEquals(offerPage.getTextMessage(), "Element created");
	}

	// Create a new product
	@Test(priority = 4)
	public void createProduct() throws InterruptedException, AWTException {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		waitPageLoaded();
		offerPage.clickOnCreateProductButton();
		NewProductPage newProductPage = PageFactory.initElements(driver, NewProductPage.class);
		waitPageLoaded();
		newProductPage.setProductCode(Constant.productCode2);
		newProductPage.setProductLabel(Constant.productDescription2);
		newProductPage.clickOnSaveProductButton();
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		waitPageLoaded();
		Assert.assertEquals(productPage.getTextMessage(), "Element created");
	}

	@Parameters({ "tarifTypeOneShot"})
	@Test(priority = 5)
	public void createOneshotCharge(String tarifTypeOneShot) {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnPricingButton();
		productPage.clickOnCreatePricingButton();
		NewChargePage newChargePage = PageFactory.initElements(driver, NewChargePage.class);
		waitPageLoaded();
		newChargePage.setChargeCode(Constant.oneShotChargeCode);
		newChargePage.setChargeDescription(Constant.oneShotChargeDescription);
		newChargePage.selectChargeType("One shot");
		newChargePage.selectOneShotTarifType(tarifTypeOneShot);
		newChargePage.clickOnSaveChargeButton();
		waitPageLoaded();
		Assert.assertEquals(newChargePage.getTextMessage(), "Element created");
	}
	
	@Parameters({ "priceVersionUnitPrice" })
	@Test(priority = 6)
	public void createPriceVersion(String priceVersionUnitPrice) {
		ChargePage chargePage = PageFactory.initElements(driver, ChargePage.class);
		waitPageLoaded();
		chargePage.clickOnCreatePriceVersionButton();
		NewPriceVersionpage newPriceVersionPage = PageFactory.initElements(driver, NewPriceVersionpage.class);
		newPriceVersionPage.setPriceVersionCode(Constant.pricePlanVersionCode);
		waitPageLoaded();
		newPriceVersionPage.setPriceVersionStartDate();
		newPriceVersionPage.setPriceVersionUnitPrice(priceVersionUnitPrice);
		newPriceVersionPage.clickOnSavePriceVersionButton();
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		Assert.assertEquals(priceVersionPage.getTextMessage(), "Element created");
	}

	
	@Test(priority = 7)
	public void publishPriceVersion() {
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		priceVersionPage.clickOnPublishPriceVersionButton();
		Assert.assertEquals(priceVersionPage.getTextMessage(), "Element created");
	}

	@Test(priority = 8)
	public void activateOneshotCharge() {
		PriceVersionPage priceVersionPage = PageFactory.initElements(driver, PriceVersionPage.class);
		waitPageLoaded();
		priceVersionPage.clickOnGoBackButton();
		ChargePage chargePage = PageFactory.initElements(driver, ChargePage.class);
		waitPageLoaded();
		chargePage.clickOnActivateChargeButton();
		Assert.assertEquals(priceVersionPage.getTextMessage(), "Action executed successfuly");
		waitPageLoaded();
		chargePage.clickOnGoBackButton();
	}
	
	@Test(priority = 10)
	public void publishProductVersion() throws InterruptedException {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		waitPageLoaded();
		productPage.clickOnPublishProductVersionButton();
		waitPageLoaded();
		Assert.assertEquals(productPage.getTextMessage(), "Action executed successfuly");
	}
	
	@Test(priority = 11)
	public void activateProduct() {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		waitPageLoaded();
		productPage.clickOnActivateProductButton();
		waitPageLoaded();
		Assert.assertEquals(productPage.getTextMessage(), "Action executed successfuly");
	}

	@Test(priority = 12)
	public void saveOffer() {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		waitPageLoaded();
		productPage.clickOnGoBackButton();
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		waitPageLoaded();
		Assert.assertEquals(offerPage.getOfferCode(), Constant.offerCode);
		offerPage.clickOnSaveOfferButton();
		waitPageLoaded();
		Assert.assertEquals(offerPage.getTextMessage(), "Element updated");
	}
	

	@Test(priority = 13)
	public void activateOffer() throws InterruptedException {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		waitPageLoaded();
		offerPage.clickOnActivateOfferButton();
		waitPageLoaded();
		Assert.assertEquals(offerPage.getTextMessage(), "Action executed successfuly");
	}

	@Test(priority = 14)
	public void createOrderWithInvoicingPlan() {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCustomerCareButton();
		homePage.clickOnOrdersButton();
		OrdersListPage ordersListPage = PageFactory.initElements(driver, OrdersListPage.class);
		ordersListPage.clickOnCreateOrderButton();
		waitPageLoaded();
		NewOrderPage newOrderrPage = PageFactory.initElements(driver, NewOrderPage.class);
		newOrderrPage.clickOnCustomerList();
		waitPageLoaded();
		newOrderrPage.clickOnAddFilterButton();
		waitPageLoaded();
		newOrderrPage.selectCodeFilterOption();
		waitPageLoaded();
		newOrderrPage.setCustomerCode("MAL3243");
		waitPageLoaded();
		newOrderrPage.clickOnFoundCustomer("MAL3243");
		newOrderrPage.clickOnInvoicingPlansList();
		newOrderrPage.setInvoicingPlanCode(Constant.invoicingPlanName);
		waitPageLoaded();
		newOrderrPage.selectInvoicingPlan(Constant.invoicingPlanName);
		newOrderrPage.clickOnSaveOrderButton();
		Assert.assertEquals(newOrderrPage.getTextMessage(), "Element created");
	}

	@Parameters({ "quantity" })
	@Test(priority = 16)
	public void createOrderLine(String quantity) throws AWTException, InterruptedException {
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		waitPageLoaded();
		orderPage.clickOnAddOrderLineButton();
		orderPage.clickOnCreateOrderLineButton();
		OrderOfferPage orderOfferPage = PageFactory.initElements(driver, OrderOfferPage.class);
		waitPageLoaded();
		orderOfferPage.clickOnOfferInput();
		orderOfferPage.clickOnAddFilterButton();
		orderOfferPage.selectCodeFilterOption();
		orderOfferPage.setOfferCode(Constant.offerCode);
		waitPageLoaded();
		orderOfferPage.clickOnFoundOffer(Constant.offerCode);
		orderOfferPage.clickOnProductTab();
		orderOfferPage.setProductQuantity(quantity);
		orderOfferPage.clickSaveButton();
		Assert.assertEquals(orderOfferPage.getTextMessage(), "Element created");
		orderOfferPage.clickGoBack();
	}

	@Test(priority = 17)
	public void finalizeOrder() {
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		orderPage.clickFinalize();
		Assert.assertEquals(orderPage.getTextMessage(), "Action executed successfuly");
	}

	@Test(priority = 18)
	public void updateProgressToNextLevel() {
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		orderPage.clickUpdateProgressToNextLevel1();
		waitPageLoaded();
		Assert.assertEquals(orderPage.getTextMessage(), "Action executed successfuly");
	}

	@Test(priority = 18)
	public void checkInvoicesLinkedToOrder() {

	}

}
