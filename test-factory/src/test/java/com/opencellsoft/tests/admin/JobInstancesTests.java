package com.opencellsoft.tests.admin;

import java.util.ArrayList;

import org.openqa.selenium.JavascriptExecutor;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.admin.AdminHomePage;
import com.opencellsoft.pages.admin.JobInstancesPage;
import com.opencellsoft.pages.admin.NewTriggeredEDRPage;
import com.opencellsoft.pages.admin.TriggeredEdrPage;
import com.opencellsoft.pages.admin.TriggeredEdrUpdatePage;
import com.opencellsoft.pages.admin.UsageChargeDetailPage;
import com.opencellsoft.pages.admin.UsageChargesListPage;

public class JobInstancesTests extends TestBase {

	ArrayList<String> tabs;
	AdminHomePage adminHomePage;
	JobInstancesPage jobsPage;
	;
	
	public void openNewTab(String baseURI) {
		// Open new Tab
				((JavascriptExecutor)driver).executeScript("window.open()");
				tabs = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tabs.get(1));
				driver.get( baseURI);
				waitPageLoaded();
	}
	
	public void closeTab() {
			driver.close();
			tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(0));
			waitPageLoaded();
	}
	
	/***  open Job Instances Page  ***/
	public void openJobInstancesPage(){
		// Go to Triggered EDR Page list
		adminHomePage = new AdminHomePage();
		adminHomePage.MoveToAdministrationMenu();
		waitPageLoaded();
		try {
			adminHomePage.MoveToJobsMenu();
		}catch (Exception e) {
			adminHomePage.MoveToAdministrationMenu();
			adminHomePage.MoveToJobsMenu();
		}
		
		waitPageLoaded();
		adminHomePage.clickOnJobInstancesMenu();
	}

	/***  search Triggered EDR  ***/
	public void searchForJob( String jobCode){	
		// Search the Triggered EDR
		jobsPage = new JobInstancesPage();
		jobsPage.setSearchingCode(jobCode);
		jobsPage.clickOnSearchButton();
		jobsPage.clickOnSearchResult(jobCode);
	}
	
	/***  update param3EL  ***/
	public void updateNextJob( String value){
		// update Triggered EDR : param3EL
		jobsPage = new JobInstancesPage();
		jobsPage.SetNextJob(value);
		jobsPage.clickOnSaveButton();
	}

}
