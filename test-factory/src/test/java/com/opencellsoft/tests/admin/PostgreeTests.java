package com.opencellsoft.tests.admin;

import static org.testng.Assert.assertEquals;

import java.time.Duration;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.opencellsoft.base.TestBase;

public class PostgreeTests extends TestBase {

	ArrayList<String> tabs;
	
	public void openPostgree(String dbURI) {
		// Open new Tab
		((JavascriptExecutor)driver).executeScript("window.open()");
		tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		waitPageLoaded();
		driver.get(dbURI);
		waitPageLoaded();
	}
	
	public void login() {
		WebElement usernameInput = driver.findElement(By.xpath("//input[@id='username']"));
		usernameInput.sendKeys("rajae.test");
		waitPageLoaded();
		WebElement passwordInput = driver.findElement(By.xpath("//input[@id='password']"));
		passwordInput.sendKeys("Rajae.test123!");
		waitPageLoaded();
		WebElement submitButton = driver.findElement(By.xpath("//input[@type='submit']"));
		clickOn(submitButton);
	}
	
	public void goToRequeteSql() {
		WebElement goButton = driver.findElement(By.xpath("//input[@type = 'submit' and @value = 'GO !']"));
		clickOn(goButton);
	}
	
	public void executeRequeteSql(String query) {
		WebElement requeteSQL = null;
		WebElement spanInput = null;
		WebElement executeButton = null;
		
		requeteSQL = driver.findElement(By.xpath("//div[@id = 'menu']//p//a[contains(text(), 'SQL')]"));
		clickOn(requeteSQL);
		spanInput = driver.findElement(By.xpath("//form[@id = 'form']//pre//span"));
		spanInput.sendKeys(query);
		waitPageLoaded();
		waitPageLoaded();
		executeButton = driver.findElement(By.xpath("(//form[@id = 'form']//input[@type = 'submit'])[1]"));
		clickOn(executeButton);
		try {
			goToRequeteSql() ;
		}catch (Exception e) {
			waitPageLoaded();
		}
	}
	public void checkResult(String location, int expectedResult) {
		int actualResult = driver.findElements(By.xpath(location)).size();
		System.out.println(actualResult);
		assertEquals(actualResult,expectedResult);
	}
	
	public void closeTab() {
		driver.close();
		tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(0));
		waitPageLoaded();
	}

}
