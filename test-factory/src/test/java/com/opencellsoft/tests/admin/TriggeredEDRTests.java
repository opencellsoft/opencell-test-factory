package com.opencellsoft.tests.admin;

import java.util.ArrayList;
import org.openqa.selenium.JavascriptExecutor;
import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.admin.AdminHomePage;
import com.opencellsoft.pages.admin.NewTriggeredEDRPage;
import com.opencellsoft.pages.admin.TriggeredEdrPage;
import com.opencellsoft.pages.admin.TriggeredEdrUpdatePage;
import com.opencellsoft.pages.admin.UsageChargeDetailPage;
import com.opencellsoft.pages.admin.UsageChargesListPage;

public class TriggeredEDRTests extends TestBase {

	ArrayList<String> tabs;
	AdminHomePage adminHomePage;
	NewTriggeredEDRPage newtriggeredEdrPage;
	TriggeredEdrPage triggeredEdrPage;
	TriggeredEdrUpdatePage triggeredEdrUpdatePage;
	UsageChargesListPage usageChargesListPage;
	UsageChargeDetailPage usageChargeDetailPage;
	
	public void openNewTab(String baseURI) {
		// Open new Tab
				((JavascriptExecutor)driver).executeScript("window.open()");
				tabs = new ArrayList<String>(driver.getWindowHandles());
				driver.switchTo().window(tabs.get(1));
				driver.get( baseURI);
				waitPageLoaded();
	}
	
	public void closeTab() {
			driver.close();
			tabs = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(tabs.get(0));
			waitPageLoaded();
	}	
	
	/***  create Triggered EDR  ***/
	public void createTriggeredEDR(String baseURI, String ConditionEL, String QuantityEL , String param2EL, String param3EL, String triggerEdrCode, String triggerEdrDescription ) {

		// Go to Triggered EDR Page list
		openTriggeredEDRListPage();
		
		// Create a new Triggered EDR
		TriggeredEdrPage triggeredEdrPage = new TriggeredEdrPage();
		triggeredEdrPage.clickOnNewButton();
		waitPageLoaded();
		
		newtriggeredEdrPage = new NewTriggeredEDRPage();
		newtriggeredEdrPage.SetCode(triggerEdrCode);
		newtriggeredEdrPage.SetDescription(triggerEdrDescription);
		newtriggeredEdrPage.SetConditionEL(ConditionEL);
		newtriggeredEdrPage.SetQuantityEL(QuantityEL);
		newtriggeredEdrPage.Setparam2EL(param2EL);
		newtriggeredEdrPage.Setparam3EL(param3EL);
		newtriggeredEdrPage.clickOnSaveButton();
	}
	
	/***  update Triggered EDR  ***/
	public void updateTriggeredEDRparam3EL( String triggerEdrCode, String param3EL){
		// Go to Triggered EDR Page list
		openTriggeredEDRListPage();
		
		// Search the Triggered EDR
		searchTriggeredEDR(triggerEdrCode);
		
		// update Triggered EDR
		updateparam3EL(param3EL);
	}
	
	/***  open TriggeredEDR List Page  ***/
	public void openTriggeredEDRListPage(){
		// Go to Triggered EDR Page list
		adminHomePage = new AdminHomePage();
		
		try {
			adminHomePage.MoveToCatalogMenu();
			waitPageLoaded();
			adminHomePage.clickOnTriggerEdrMenu();
		}catch (Exception e) {
			driver.navigate().refresh();
			waitPageLoaded();
			adminHomePage.MoveToCatalogMenu();
			waitPageLoaded();
			adminHomePage.clickOnTriggerEdrMenu();
		}
		
		waitPageLoaded();
	}
	
	/***  search Triggered EDR  ***/
	public void searchTriggeredEDR( String triggerEdrCode){	
		// Search the Triggered EDR
		triggeredEdrPage = new TriggeredEdrPage();
		triggeredEdrPage.setSearchingCode(triggerEdrCode);
		triggeredEdrPage.clickOnSearchButton();
		waitPageLoaded();
		triggeredEdrPage.clickOnSearchResult(triggerEdrCode);
		waitPageLoaded();
	}
	
	/***  update param3EL  ***/
	public void updateparam3EL( String param3EL){
		
		// update Triggered EDR : param3EL
		triggeredEdrUpdatePage =  new TriggeredEdrUpdatePage();
		triggeredEdrUpdatePage.Setparam3EL(param3EL);
		triggeredEdrUpdatePage.clickOnSaveButton();
		waitPageLoaded();
	}
	
	public void updateQuantity( String quantity){
		
		// update Triggered EDR : param3EL
		triggeredEdrUpdatePage =  new TriggeredEdrUpdatePage();
		triggeredEdrUpdatePage.setQuantity(quantity);
		triggeredEdrUpdatePage.clickOnSaveButton();
		waitPageLoaded();
	}
	
	
	/***  update Triggered EDR  ***/
	public void updateConditionELInTriggeredEDR( String triggerEdrCode, String conditionEL){
		// Go to Triggered EDR Page list
		openTriggeredEDRListPage();
		
		// Search the Triggered EDR
		searchTriggeredEDR(triggerEdrCode);
		
		// update Triggered EDR : ConditionEL
		updateConditionEL( conditionEL);
	}
	
	/***  update param3EL  ***/
	public void updateConditionEL( String conditionEL){
		// update Triggered EDR : ConditionEL
		triggeredEdrUpdatePage =  new TriggeredEdrUpdatePage();
		triggeredEdrUpdatePage.SetConditionEL(conditionEL);
		triggeredEdrUpdatePage.clickOnSaveButton();
		waitPageLoaded();
	}	
	
	/***  add Triggered EDR To Charge  ***/
	public void addTriggeredEDRToCharge(String usageChargeCode,String triggerEdrCode , boolean TriggerNextCharge) {
		// Go to Usage Charges List Page 
		adminHomePage = new AdminHomePage();
		adminHomePage.MoveToCatalogMenu();
		adminHomePage.MoveToServiceManagementMenu();
		adminHomePage.MoveToChargesMenu();
		adminHomePage.clickOnUsageMenu();

		// Search the Usage charge
		usageChargesListPage = new UsageChargesListPage();
		usageChargesListPage.SetChargeCode(usageChargeCode);
		usageChargesListPage.clickOnsearchButton();

		// Go to Usage charge detail
		usageChargesListPage.clickOnChargeCodeLink(usageChargeCode);
		usageChargeDetailPage = new UsageChargeDetailPage();

		// Check TriggerNextCharge CheckBox
		if(TriggerNextCharge == true) {
			usageChargeDetailPage.checkTriggerNextCharge();
		}
		
		// Add Triggered EDR To this Charge
		usageChargeDetailPage.clickOnTriggeredEdr(triggerEdrCode);
		waitPageLoaded();
		usageChargeDetailPage.clickOnAddButton();
		waitPageLoaded();
		usageChargeDetailPage.selectSubCategory();
		usageChargeDetailPage.clickOnSaveButton();
		usageChargeDetailPage.clickOnYesButton();

	}
}
