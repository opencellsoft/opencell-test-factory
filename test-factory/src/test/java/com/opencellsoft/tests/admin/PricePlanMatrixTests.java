package com.opencellsoft.tests.admin;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.admin.AdminHomePage;
import com.opencellsoft.pages.admin.PricePlanMatrixDetailPage;
import com.opencellsoft.pages.admin.PricePlanMatrixesListPage;

public class PricePlanMatrixTests extends TestBase {

	// Go to price plan list
	public void goToPricePlanList() {
		AdminHomePage adminHomePage = new AdminHomePage();
		adminHomePage.MoveToCatalogMenu();
		waitPageLoaded();
		try {
			adminHomePage.MoveToPricePlans();
		}catch (Exception e) {
			adminHomePage.MoveToCatalogMenu();
			waitPageLoaded();
			adminHomePage.MoveToPricePlans();
		}
		waitPageLoaded();
		
		try {
			
			adminHomePage.clickOnPricePlans();
		}catch (Exception e) {
			adminHomePage.MoveToPricePlans();
			waitPageLoaded();
			adminHomePage.clickOnPricePlans();
		}
		waitPageLoaded();
	}

	// search for pricePlanMatrixe by charge
	public void searchForPricePlanMatrixeByCharge(String code) {
		PricePlanMatrixesListPage pricePlanMatrixesListPage = new PricePlanMatrixesListPage();
		pricePlanMatrixesListPage.setChargeCodeInput(code);
		waitPageLoaded();
		pricePlanMatrixesListPage.clickOnSearchButton();
		waitPageLoaded();
		pricePlanMatrixesListPage.clickOnPricePlanLine(code);
		waitPageLoaded();
	}

	// Add rating script to price plan
	public void AddScriptToPricePlan(String script, String amount) {
		PricePlanMatrixDetailPage pricePlanMatrixDetailPage = new PricePlanMatrixDetailPage();
		pricePlanMatrixDetailPage.clickOnScriptSelectIdButton();
		waitPageLoaded();
		pricePlanMatrixDetailPage.setsearchPricePlanMatScript(script);
		waitPageLoaded();
		pricePlanMatrixDetailPage.clickOnSearchPricePlanMatScriptButton();
		waitPageLoaded();
		pricePlanMatrixDetailPage.selectFirstResult();
		waitPageLoaded();
		pricePlanMatrixDetailPage.setAmountWithoutTax(amount);
		pricePlanMatrixDetailPage.clickOnSaveButton();
		waitPageLoaded();
	}


}
