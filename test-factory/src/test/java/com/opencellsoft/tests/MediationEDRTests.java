package com.opencellsoft.tests;

import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.operations.mediation.EDRSettingsPage;
import com.opencellsoft.pages.operations.mediation.EDRsPage;
import com.opencellsoft.tests.admin.PostgreeTests;

public class MediationEDRTests extends TestBase {

	HomePage homePage;
	EDRSettingsPage edrSettingsPage;
	EDRsPage edrsPage;

	public void enableEDRVersioning(String CriteriaEL1, String EventKeyEL1, String NewVersionEL1, String CriteriaEL2, String EventKeyEL2, String NewVersionEL2){

		edrSettingsPage = new EDRSettingsPage();
		// check if EDR versionning is not yet sitting
		if (edrSettingsPage.EnebleEDRsCheckBox() == false)
		{
			edrSettingsPage.clickOnEnableEdrVersionning();
		}
		boolean paramExist = edrSettingsPage.checkEdrVersionningTable(CriteriaEL1,EventKeyEL1,NewVersionEL1,CriteriaEL2, EventKeyEL2,NewVersionEL2 );

		if (paramExist == false) {		
			// Enable Edr Versionning and fill the parameters			
			edrSettingsPage.SetCriteriaEL1(CriteriaEL1);
			edrSettingsPage.SetEventKeyEL1(EventKeyEL1);
			edrSettingsPage.SetIsNewVersionEL1(NewVersionEL1);
			edrSettingsPage.SetCriteriaEL2(CriteriaEL2);
			edrSettingsPage.SetEventKeyEL2(EventKeyEL2);
			edrSettingsPage.SetIsNewVersionEL2(NewVersionEL2);
			waitPageLoaded();
			edrSettingsPage.clickOnSave();
		}
		
	}
	public void filterBySubscription(String subscriptionCode){
		// filter by subscription
		edrsPage = new EDRsPage();
		try {
			edrsPage.clickOnEventKeyDeleteButton();
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		edrsPage.clickOnFilterButton();
		edrsPage.clickOnSubscriptionOption();
		edrsPage.setSubscriptionCode(subscriptionCode);
		waitPageLoaded();
	}
	
	public void closeGroupByEventKey() {
		edrsPage = new EDRsPage();
		edrsPage.clickOnEventKeyDeleteButton();
	}
	public int eventVersion1Size() {
		edrsPage = new EDRsPage();
		return edrsPage.eventVersion1Size();
	}
	// return number of elements with eventVersion= 2
	public int eventVersion2Size() {
		edrsPage = new EDRsPage();
		return edrsPage.eventVersion2Size();
	}
	// return number of elements with eventVersion= 3
	public int eventVersion3Size() {
		edrsPage = new EDRsPage();
		return edrsPage.eventVersion3Size();
	}
	public int getEventKeyAndParameter3FirstLineSize(String param3) {
		edrsPage = new EDRsPage();
		return edrsPage.getEventKeyAndParameter3FirstLineSize(param3);
	}

	public int getEventKeyAndParameter3ELSecondLineSize(String param3EL) {
		edrsPage = new EDRsPage();
		return edrsPage.getEventKeyAndParameter3ELSecondLineSize(param3EL);
	}
	
	public int ratedStatusSize() {
		edrsPage = new EDRsPage();
		return edrsPage.ratedStatusSize();
	}
	
	public int cancelledStatusSize() {
		edrsPage = new EDRsPage();
		return edrsPage.cancelledStatusSize();
	}
	public int checkAccessPoint(String accessPoint) {
		edrsPage = new EDRsPage();
		return edrsPage.checkAccessPoint(accessPoint);
	}	
	public int checkParameter2FirstLine(String param2) {
		edrsPage = new EDRsPage();
		return edrsPage.checkParameter2FirstLine(param2);
	}	
	public int checkParameter2ELSecondLine(String param2EL) {
		edrsPage = new EDRsPage();
		return edrsPage.checkParameter2ELSecondLine(param2EL);
	}
}
