package com.opencellsoft.tests;

import java.awt.AWTException;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.quotes.ListOfQuotesPage;
import com.opencellsoft.pages.quotes.NewQuoteOffersPage;
import com.opencellsoft.pages.quotes.NewQuotePage;
import com.opencellsoft.pages.quotes.QuotePage;
import com.opencellsoft.utility.Constant;

public class QuotesTests extends TestBase {
//	@Parameters({ "customerCode" })
	@Test(priority = 21, groups = { "smoke tests" })
	public void createNewQuote() throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		waitPageLoaded();
		homePage.clickOnCustomerCareButton();
		homePage.clickOnQuotesTab();
		ListOfQuotesPage quoteListPage = PageFactory.initElements(driver, ListOfQuotesPage.class);
		waitPageLoaded();
		quoteListPage.clickOnCreateQuoteButton();
		NewQuotePage newQuotePage = PageFactory.initElements(driver, NewQuotePage.class);
		waitPageLoaded();
		newQuotePage.setQuoteCode(Constant.quoteCode);
		newQuotePage.clickOnCustomerInput();
		waitPageLoaded();
		newQuotePage.clickOnAddFilterButton();
		waitPageLoaded();
		newQuotePage.selectCodeFilterOption();
		waitPageLoaded();
		newQuotePage.setCustomerCode(Constant.customerCode);
		waitPageLoaded();
		newQuotePage.clickOnFoundCustomer(Constant.customerCode);
		waitPageLoaded();
		newQuotePage.clickOnSellerList();
		newQuotePage.clickOnSellerFR();
		newQuotePage.clickOnSaveQuoteButton();
		waitPageLoaded();
		//Assert.assertEquals(newQuotePage.getTextMessage(), "Element created");
	}
	
	@Test(priority = 22)
	public void assignContractToQuote() {
		QuotePage quotePage = PageFactory.initElements(driver, QuotePage.class);
		waitPageLoaded();
		quotePage.clickOnContractInput();
		quotePage.clickOnFilterButton();
		quotePage.selectFilterByCodeOption();
		quotePage.setCodeFilter(Constant.contractCode);
		quotePage.selectContract(Constant.contractCode);

	}

	@Test(priority = 22, groups = { "smoke tests" })
	public void createNewQuoteVersion() throws InterruptedException {
		QuotePage quotePage = PageFactory.initElements(driver, QuotePage.class);
		quotePage.clickOnSaveButton();
		Assert.assertTrue(quotePage.getTextMessage().toLowerCase().contains("element updated"));
		quotePage.clickOnNewQuoteVersionButton();
		NewQuotePage newQuotePage = PageFactory.initElements(driver, NewQuotePage.class);
		Assert.assertEquals(newQuotePage.getTextMessage(), "Action executed successfuly");
	}

	@Parameters({ "quantity" })
	@Test(priority = 23, groups = { "smoke tests" })
	public void addNewQuoteOffers(String quantity) throws InterruptedException, AWTException {
		QuotePage quotePage = PageFactory.initElements(driver, QuotePage.class);
		waitPageLoaded();
		quotePage.clickOnAddQuoteLineButton();
		NewQuoteOffersPage newQuoteOffersPage = PageFactory.initElements(driver, NewQuoteOffersPage.class);
		waitPageLoaded();
		newQuoteOffersPage.clickOnOffer();
		//newQuoteOffersPage.clickOnAddOfferFilterButton();
		//newQuoteOffersPage.clickOnAddOfferFilterCode();
		//newQuoteOffersPage.setCodeOffer(Constant.offerCode);
		newQuoteOffersPage.setOffername(Constant.offerDescription);
		newQuoteOffersPage.selectOfferByName(Constant.offerDescription);
		waitPageLoaded();
		//newQuoteOffersPage.clickOnCodeOfferFound(Constant.offerCode);
		waitPageLoaded();		
		newQuoteOffersPage.clickOnOfferDetails();
		newQuoteOffersPage.setProductQuantity(quantity);
		waitPageLoaded();
		newQuoteOffersPage.clickOnSaveQuoteOffersButton();
		waitPageLoaded();
		//Assert.assertEquals(newQuoteOffersPage.getTextMessage(), "Element updated");
	}

	@Test(priority = 24, groups = { "smoke tests" })
	public void publishQuoteVersion() throws InterruptedException {
		QuotePage quotePage = PageFactory.initElements(driver, QuotePage.class);
		waitPageLoaded();
		quotePage.clickOnActionsButton();
		quotePage.clickOnPublishVersionQuoteButton();
		waitPageLoaded();
		Assert.assertEquals(quotePage.getTextMessage(), "Action executed successfuly");
	}

	@Test(priority = 25, groups = { "smoke tests" })
	public void sendQuote() throws InterruptedException {
		QuotePage quotePage = PageFactory.initElements(driver, QuotePage.class);
		Thread.sleep(2000);
		quotePage.clickOnRequestApprovalButton();
		// Assert.assertEquals(quotePage.getTextMessage(), "Action executed
		// successfuly");
	}

	@Test(priority = 26, groups = { "smoke tests" })
	public void validateQuote() throws InterruptedException {
		QuotePage quotePage = PageFactory.initElements(driver, QuotePage.class);
		quotePage.clickOnValidateQuoteButton();
		// Assert.assertEquals(quotePage.getTextMessage(), "Action executed
		// successfuly");
		Thread.sleep(2000);
	}

	@Test(priority = 27, groups = { "smoke tests" })
	public void generatePDFQuote() throws InterruptedException {
		QuotePage quotePage = PageFactory.initElements(driver, QuotePage.class);
		waitPageLoaded();
		quotePage.clickOnDownloadQuotePDF();
		waitPageLoaded();
		Assert.assertEquals(quotePage.getTextMessage(), "Action executed successfuly");
	}

	@Test(priority = 28, dependsOnMethods = "com.opencellsoft.tests.administration.InvoicingPlansTests.createInvoicingPlanLines")
	public void duplicateQuote() throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCustomerCareButton();
		waitPageLoaded();
		homePage.clickOnQuotesTab();
		ListOfQuotesPage quoteListPage = PageFactory.initElements(driver, ListOfQuotesPage.class);
		waitPageLoaded();
		quoteListPage.clickOnAddFilterQuoteButton();
		quoteListPage.clickOnAddFilterCodeQuoteButton();
		quoteListPage.setCodeQuote(Constant.quoteCode);
		//quoteListPage.setSearchBarText(Constant.quoteCode);
		waitPageLoaded();
		quoteListPage.selectQuote();
		QuotePage quotePage = PageFactory.initElements(driver, QuotePage.class);
		waitPageLoaded();
		quotePage.clickOnDuplicateQuoteButton();
		Assert.assertEquals(quotePage.getTextMessage(), "Action executed successfuly");
	}

	@Test(priority = 29, dependsOnMethods = "duplicateQuote")
	public void publishDuplicateQuote() throws InterruptedException {
		QuotePage quotePage = PageFactory.initElements(driver, QuotePage.class);
		waitPageLoaded();
		quotePage.clickOnActionsButton();
		waitPageLoaded();
		quotePage.clickOnPublishVersionQuoteButton();
	}

	@Test(priority = 30, dependsOnMethods = "publishDuplicateQuote")
	public void sendDuplicateQuote() throws InterruptedException {
		// Thread.sleep(2000);
		// Assert.assertEquals(quotePage.getTextMessage(), "Action executed
		// successfuly");
		QuotePage quotePage = PageFactory.initElements(driver, QuotePage.class);
		quotePage.clickOnRequestApprovalButton();
		// Assert.assertEquals(quotePage.getTextMessage(), "Action executed
		// successfuly");
	
	}

	@Test(priority = 31, dependsOnMethods = "sendDuplicateQuote")
	public void validateDuplicateQuote() throws InterruptedException {
		QuotePage quotePage = PageFactory.initElements(driver, QuotePage.class);
		waitPageLoaded();
		quotePage.clickOnValidateQuoteButton();
		waitPageLoaded();
		//Assert.assertEquals(quotePage.getTextMessage(), "Action executed successfuly");
	}

	@Test(priority = 22)
	public void assignDiscountPlanToQuote() throws InterruptedException {
		QuotePage quotePage = PageFactory.initElements(driver, QuotePage.class);
		quotePage.clickOnDiscountInput();
		quotePage.clickOnFilterButton();
		Thread.sleep(1000);
		quotePage.selectFilterByCodeOption();
		quotePage.setCodeFilter(Constant.discountPlanCode);
		quotePage.selectFoundDiscountPlan();
		quotePage.clickOnSaveButton();
	}
}