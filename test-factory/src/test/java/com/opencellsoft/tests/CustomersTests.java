package com.opencellsoft.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Ignore;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.customercare.balance.BalancePage;
import com.opencellsoft.pages.customercare.rateditems.RatedItemsPage;
import com.opencellsoft.pages.customercare.securitydeposits.ListOfSecurityDepositPage;
import com.opencellsoft.pages.customers.CustomerInvoiceTabPage;
import com.opencellsoft.pages.customers.CustomerPage;
import com.opencellsoft.pages.customers.ListOfCustomersPage;
import com.opencellsoft.pages.customers.ModifyCustomerPage;
import com.opencellsoft.pages.customers.NewConsumerPage;
import com.opencellsoft.pages.customers.NewCustomerPage;
import com.opencellsoft.pages.customers.NewPaymentMethodPage;
import com.opencellsoft.utility.Constant;

public class CustomersTests extends TestBase {
	CustomerPage customerPage;
	CustomerInvoiceTabPage customerInvoiceTabPage;
	BalancePage balancePage;
	ListOfSecurityDepositPage listOfSecurityDepositPage;
	public void searchCustomer(String CustomerCode) {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCustomers();
		ListOfCustomersPage listOfCustomersPage = PageFactory.initElements(driver, ListOfCustomersPage.class);
		listOfCustomersPage.clickOnFilterButton();
		listOfCustomersPage.clickOnFilterByCustomerCode();
		listOfCustomersPage.setCustomerCode(Constant.customerCode);
		listOfCustomersPage.clickOnCustomer(Constant.customerCode);
	}

	@Parameters({ "billingCountry", "billingCycle" })
	@Test(priority = 19, groups = { "smoke tests" })
	public void createCustomerWithPaypalAsPaymentMethod(String billingCountry, String billingCycle) throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCustomerCareButton();
		ListOfCustomersPage listOfCustomersPage = PageFactory.initElements(driver, ListOfCustomersPage.class);
		listOfCustomersPage.clickOnCreateCustomerButton();
		NewCustomerPage newCustomerPage = PageFactory.initElements(driver, NewCustomerPage.class);
		newCustomerPage.selectCustomerCompany();
		newCustomerPage.setCustomerCode(Constant.customerCode);
		newCustomerPage.setCustomerName(Constant.customerName);
		newCustomerPage.clickOnGeneralInfosSection();
		newCustomerPage.clickOnLegalEntityTypeDropDown();
		newCustomerPage.selectEntityTypeOption();
		newCustomerPage.clickOnBillingInfoSection();
		newCustomerPage.clickOnBillingCycleDropdown();
		newCustomerPage.selectBillingCycleOption(billingCycle);
		newCustomerPage.clickOnCountryDropDown();
		newCustomerPage.selectCustomerCountryOption(billingCountry);
		newCustomerPage.clickOnLanguageDropDown();
		newCustomerPage.selectLanguageOption();
		newCustomerPage.clickOnCurrencyDropDown();
		newCustomerPage.selectCurrencyOption();
		newCustomerPage.clickOnContactSection();
		newCustomerPage.fillCustomerEmailInput(Constant.customerEmail);
		newCustomerPage.clickOnGroupSection();
		newCustomerPage.clickOnClientCategoryDropDown();
		newCustomerPage.selectClientCategory();
		newCustomerPage.clickOnPayingAccountSection();
		newCustomerPage.clickOnPaymentMethodList();
		newCustomerPage.selectPaypalPaymentMethod();
		newCustomerPage.setUserID(Constant.paypalUserId);
		//		newCustomerPage.clickOnSecondCurrencyDropDown();
		//		newCustomerPage.selectSecondCurrencyOption();
		newCustomerPage.clickOnSubmitButton();
		//		Assert.assertTrue(newCustomerPage.getTextMessage().toLowerCase().contains("element created"));
		Assert.assertTrue(newCustomerPage.getTextMessage().contains("Element created"));
	}

	@Ignore
	@Parameters({ "billingCountry", "cardNumber", "expirationYear", "expirationMonth", "billingCycle" })
	@Test(priority = 17, groups = { "smoke tests" })
	public void createCustomerWithCardAsPaymentMethod(String billingCountry,String cardNumber, String expirationYear,
			String expirationMonth, String billingCycle) throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCustomerCareButton();
		ListOfCustomersPage listOfCustomersPage = PageFactory.initElements(driver, ListOfCustomersPage.class);
		listOfCustomersPage.clickOnCreateCustomerButton();
		NewCustomerPage newCustomerPage = PageFactory.initElements(driver, NewCustomerPage.class);
		newCustomerPage.selectCustomerCompany();
		newCustomerPage.setCustomerCode(Constant.customerCode);
		newCustomerPage.setCustomerName(Constant.customerName);
		newCustomerPage.clickOnGeneralInfosSection();
		newCustomerPage.clickOnLegalEntityTypeDropDown();
		newCustomerPage.selectEntityTypeOption();
		newCustomerPage.clickOnBillingInfoSection();
		newCustomerPage.clickOnBillingCycleDropdown();
		waitPageLoaded();
		newCustomerPage.selectBillingCycleOption(billingCycle);
		newCustomerPage.clickOnCountryDropDown();
		waitPageLoaded();
		newCustomerPage.selectCustomerCountryOption(billingCountry);
		waitPageLoaded();
		newCustomerPage.clickOnLanguageDropDown();
		newCustomerPage.selectLanguageOption();
		newCustomerPage.clickOnCurrencyDropDown();
		newCustomerPage.selectCurrencyOption();
		newCustomerPage.clickOnContactSection();
		newCustomerPage.fillCustomerEmailInput(Constant.customerEmail);
		newCustomerPage.clickOnGroupSection();
		newCustomerPage.clickOnSellerDropDown();
		newCustomerPage.selectSeller();
		newCustomerPage.clickOnClientCategoryDropDown();
		newCustomerPage.selectClientCategory();
		newCustomerPage.clickOnPayingAccountSection();
		newCustomerPage.clickOnPaymentMethodList();
		newCustomerPage.selectCardPaymentMethod(Constant.customerCode,cardNumber, expirationYear,expirationMonth);
		newCustomerPage.clickOnSubmitButton();
		waitPageLoaded();
	}

	@Parameters({ "billingCountry", "accountAwner", "referenceMandate", "iban", "bic","billingCycle", "acrs" })
	@Test(priority = 18, groups = { "smoke tests" })
	public void createCustomerWithDirectDebitAsPaymentMethod(String billingCountry,String accountAwner, String referenceMandate,
			String iban, String bic, String billingCycle, String acrs) throws InterruptedException {
		createCustomerDirectDebit(Constant.customerName,Constant.customerEmail,billingCountry,accountAwner, referenceMandate,iban, bic, billingCycle, acrs);
	}

	@Test(priority = 19, groups = { "smoke tests" })
	@Parameters({ "billingCountry","taxCategory", "billingCycle" })
	public void createCustomerWithCheckAsPaymentMethod(String billingCountry, String taxCategory, String billingCycle) throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		waitPageLoaded();
		homePage.clickOnCustomerCareButton();
		ListOfCustomersPage listOfCustomersPage = PageFactory.initElements(driver, ListOfCustomersPage.class);
		waitPageLoaded();
		listOfCustomersPage.clickOnCreateCustomerButton();
		NewCustomerPage newCustomerPage = PageFactory.initElements(driver, NewCustomerPage.class);
		waitPageLoaded();
		newCustomerPage.selectCustomerCompany();
		newCustomerPage.setCustomerCode(Constant.customerCode);
		newCustomerPage.setCustomerName(Constant.customerName);	
		newCustomerPage.clickOnGeneralInfosSection();
		waitPageLoaded();
		newCustomerPage.selectCompanyRegistration("0002");
		newCustomerPage.clickOnLegalEntityTypeDropDown();
		newCustomerPage.selectEntityTypeOption();
		newCustomerPage.clickOnBillingInfoSection();
		newCustomerPage.clickOnBillingCycleDropdown();
		waitPageLoaded();
		newCustomerPage.selectBillingCycleOption(billingCycle);
		newCustomerPage.clickOnCountryDropDown();
		newCustomerPage.selectCustomerCountryOption(billingCountry);
		newCustomerPage.clickOnLanguageDropDown();
		waitPageLoaded();
		newCustomerPage.selectLanguageOption();
		waitPageLoaded();
		//		newCustomerPage.selectTaxCategory(taxCategory);
		newCustomerPage.clickOnCurrencyDropDown();
		newCustomerPage.selectCurrencyOption();
		newCustomerPage.clickOnContactSection();
		newCustomerPage.fillCustomerEmailInput(Constant.customerEmail);
		newCustomerPage.clickOnGroupSection();
		newCustomerPage.selectCustomerCompanyRegistration("0002");
		newCustomerPage.clickOnSellerDropDown();
		newCustomerPage.selectSeller();
		newCustomerPage.clickOnClientCategoryDropDown();
		newCustomerPage.selectClientCategory();
		newCustomerPage.clickOnPayingAccountSection();
		newCustomerPage.selectCACompanyRegistration("0002");
		newCustomerPage.clickOnPaymentMethodList();
		newCustomerPage.selectCheckPaymentMethod();
		newCustomerPage.clickOnSubmitButton();
		Assert.assertEquals(newCustomerPage.getTextMessage(), "Element created");
	}

	@Parameters({ "legalForm" })
	@Test(priority = 21, groups = { "smoke tests" })
	public void addNewCompanyConsumer(String legalForm) throws InterruptedException {
		/*HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCustomerCareButton();
		homePage.clickOnCustomers();
		ListOfCustomersPage listOfCustomersPage = PageFactory.initElements(driver, ListOfCustomersPage.class);
		listOfCustomersPage.clickOnFilterButton();
		listOfCustomersPage.clickOnFilterByCustomerCode();
		listOfCustomersPage.setCustomerCode(Constant.customerCode);
		listOfCustomersPage.clickOnCustomer(Constant.customerCode);*/
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		waitPageLoaded();
		customerPage.clickOnCreateConsumerButton();
		NewConsumerPage newConsumerPage = PageFactory.initElements(driver, NewConsumerPage.class);
		waitPageLoaded();
		newConsumerPage.setConsumerCode(Constant.consumerCode);
		newConsumerPage.setConsumerName(Constant.consumerName);
		waitPageLoaded();
		//newConsumerPage.selectLegalForm(legalForm);
		//newConsumerPage.setCompanyRegistration(Constant.companyRegistrationNumber);
		//newConsumerPage.setNumberVAT(Constant.vatNumber);
		newConsumerPage.clickOnSubmitButton();
		waitPageLoaded();
		Assert.assertEquals(customerPage.getTextMessage(), "Element created");
		newConsumerPage.clickOnGoBackButton();
		waitPageLoaded();
		newConsumerPage.clickOnSubmitButton();
		waitPageLoaded();
		//Assert.assertEquals(customerPage.getTextMessage(), "Element updated");
	}

	@Ignore
	@Parameters({ "customerCountry", "billingCycle", "billingCountry", "billingLanguage", "payingAccountCurrency",
		"payingAccountClientCategory", "PayingAccountPaymentMethod" })
	@Test(priority = 3)
	public void createIndividualCustomer(String customerCountry, String billingCycle, String billingCountry,
			String billingLanguage, String payingAccountCurrency, String payingAccountClientCategory,
			String payingAccountPaymentMethod) {
		ListOfCustomersPage listOfCustomersPage = PageFactory.initElements(driver, ListOfCustomersPage.class);
		listOfCustomersPage.clickOnCreateCustomerButton();
		NewCustomerPage newCustomerPage = PageFactory.initElements(driver, NewCustomerPage.class);
		newCustomerPage.clickOnCreateIndividualCustomer();
		newCustomerPage.setIndividualCustomerCode(Constant.individualCustomerCode);
		newCustomerPage.clickOnCustomerInfo();
		newCustomerPage.setCustomerFirstName(Constant.customerFirstName);
		newCustomerPage.setCustomerLastName(Constant.customerLastName);
		newCustomerPage.setCustomerEmail(Constant.customerEmail);
		newCustomerPage.clickOnCustomerCountry(customerCountry);
		newCustomerPage.clickOnCustomerBilling();
		newCustomerPage.selectBillingCycle(billingCycle);
		newCustomerPage.selectBillingCountry(billingCountry);
		newCustomerPage.selectBillingLanguage(billingLanguage);
		newCustomerPage.clickOnPayingAccount();
		newCustomerPage.selectPayingAccountCurrency(payingAccountCurrency);
		newCustomerPage.selectPayingAccountClientCategory(payingAccountClientCategory);
		newCustomerPage.selectPayingAccountPaymentMethod(payingAccountPaymentMethod);
		newCustomerPage.setUserID(Constant.paypalUserId);
		newCustomerPage.clickOnSubmitButton();
		newCustomerPage.clickOnGoBackButton();
	}

	@Parameters({ "customerCode" })
	@Test(priority = 4)
	public void findCustomer(String customerCode) throws InterruptedException {
		ListOfCustomersPage listOfCustomersPage = PageFactory.initElements(driver, ListOfCustomersPage.class);
		listOfCustomersPage.clickOnFilterButton();
		listOfCustomersPage.clickOnFilterByCustomerCode();
		listOfCustomersPage.setCustomerCode(customerCode);
		Thread.sleep(1000);
		listOfCustomersPage.clickCustomer(customerCode);
	}

	@Ignore
	@Parameters({ "customerFirstName", "customerLastName", "customerModifyCountry", "billingModifyCycle",
		"billingModifyCountry", "billingModifyLanguage", "payingAccountModifyCurrency",
	"payingAccountClientCategory" })
	@Test(priority = 5)
	public void modifyCustomer(String customerFirstName, String customerLastName, String customerModifyCountry,
			String billingModifyCycle, String billingModifyCountry, String billingModifyLanguage,
			String payingAccountModifyCurrency, String payingAccountClientCategory) {
		ModifyCustomerPage modifyCustomerPage = PageFactory.initElements(driver, ModifyCustomerPage.class);
		modifyCustomerPage.clickOnCustomerInfo();
		modifyCustomerPage.setCustomerFirstName(customerFirstName);
		modifyCustomerPage.setCustomerLastName(customerLastName);
		modifyCustomerPage.clickOnCustomerCountry(customerModifyCountry);
		modifyCustomerPage.clickOnCustomerBilling();
		modifyCustomerPage.selectBillingCycle(billingModifyCycle);
		modifyCustomerPage.selectBillingCountry(billingModifyCountry);
		modifyCustomerPage.selectBillingLanguage(billingModifyLanguage);
		modifyCustomerPage.clickOnPayingAccount();
		modifyCustomerPage.selectPayingAccountCurrency(payingAccountModifyCurrency);
		modifyCustomerPage.selectPayingAccountClientCategory(payingAccountClientCategory);
		// modifyCustomerPage.clickOnCreatePaymentMethods();
		// modifyCustomerPage.clickOnPaymentType();
	}

	@Parameters({ "customerCode" })
	@Test(priority = 1)
	public void createSubscriptionFromCustomerView(String customerCode) throws InterruptedException {
		HomePage homepage = PageFactory.initElements(driver, HomePage.class);
		homepage.clickOnCustomerCareButton();
		ListOfCustomersPage listOfCustomersPage = PageFactory.initElements(driver, ListOfCustomersPage.class);
		listOfCustomersPage.clickOnFilterButton();
		listOfCustomersPage.clickOnFilterByCustomerCode();
		listOfCustomersPage.setCustomerCode(customerCode);
		Thread.sleep(1000);
		listOfCustomersPage.clickCustomer(customerCode);
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		customerPage.clickOnSubscriptionTab();
		customerPage.clickOnCreateSubscriptionButton();
	}

	@Test(priority = 20)
	public void addDirectDebitPaymentMethodToCustomer() throws InterruptedException {
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		waitPageLoaded();
		customerPage.clickOnPayingAccount();
		waitPageLoaded();
		customerPage.clickOnCreatePaymentMethodButton();
		NewPaymentMethodPage newPaymentMethodPage = PageFactory.initElements(driver, NewPaymentMethodPage.class);
		waitPageLoaded();
		newPaymentMethodPage.selectPaymentType("Direct debit");
		newPaymentMethodPage.setIBAN(Constant.IBAN);
		newPaymentMethodPage.setBic(Constant.BIC);
		newPaymentMethodPage.setAccountOwner(Constant.accountOwner);
		newPaymentMethodPage.setMandateDate();
		newPaymentMethodPage.setReferenceMandate(Constant.referenceMandate);
		newPaymentMethodPage.setPreferred();
		newPaymentMethodPage.clickOnSaveButton();
		Thread.sleep(2000);
		waitPageLoaded();
		newPaymentMethodPage.clickOnGoBackButton();

	}

	@Test(priority = 22, groups = { "perf tests" })
	public void checkCustomersList() { //Check the Customers list page
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCustomerCareButton();
		ListOfCustomersPage listOfCustomersPage = PageFactory.initElements(driver, ListOfCustomersPage.class);
		listOfCustomersPage.checkBreadCrumb("Customer Care");
		listOfCustomersPage.checkBreadCrumb("billing accounts");
		listOfCustomersPage.checkBreadCrumb("Customers");
	}

	@Test(priority = 23, groups = { "perf tests" }) 
	public void checkPaginationCustomersList() { 
		ListOfCustomersPage listOfCustomersPage = PageFactory.initElements(driver, ListOfCustomersPage.class);
		listOfCustomersPage.checkPagination();
	}

	@Parameters({ "customerCode" })
	@Test(priority = 24, groups = { "perf tests" }) 
	public void checkCustomerDetail(String customerCode) { //Check the Customers detail page
		ListOfCustomersPage listOfCustomersPage = PageFactory.initElements(driver, ListOfCustomersPage.class);
		listOfCustomersPage.setSearchBarText(customerCode);
		listOfCustomersPage.clickOnCustomer(customerCode);
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		Assert.assertEquals(customerPage.getCustomerCode(), customerCode);
	}

	@Test(priority = 25, groups = { "perf tests" }) 
	public void checkRatedItemsTab() { //Check Rated Items tab from customer view
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		customerPage.clickOnRatedItemsTab();
		//Assert.assertEquals(customerPage.getSubscriptionColumnLabel(), "subscription.code");
		customerPage.checkPresenceOfTextInPage("Subscription n°");
	}

	@Test(priority = 26, groups = { "perf tests" }) 
	public void checkSubscriptionsTab() { //Check Rated Items tab from customer view
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		customerPage.clickOnSubscriptionTab();
		//Assert.assertEquals(customerPage.getSubscriptionColumnLabel(), "code");
		customerPage.checkPresenceOfTextInPage("Code");
	}		

	@Test(priority = 27, groups = { "perf tests" }) 
	public void checkInvoicesTab() { //Check Invoices tab from customer view
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		customerPage.clickOnInvoicesTab();
		//Assert.assertEquals(customerPage.getInvoiceColumnLabel(), "invoiceNumber");
		customerPage.checkPresenceOfTextInPage("Invoice Type");
	}		

	/***  create Customer  ***/
	public void createCustomerPaypal(String customerCode,String customerName,String customerEmail,String paypalUserId, String billingCountry, String billingCycle) {
		HomePage HomePage = PageFactory.initElements(driver, HomePage.class);
		HomePage.clickOnCustomerCareButton();
		waitPageLoaded();
		ListOfCustomersPage listOfCustomersPage = PageFactory.initElements(driver, ListOfCustomersPage.class);
		listOfCustomersPage.clickOnCreateCustomerButton();
		waitPageLoaded();
		NewCustomerPage newCustomerPage = PageFactory.initElements(driver, NewCustomerPage.class);
		newCustomerPage.selectCustomerCompany();
		newCustomerPage.setCustomerCode(customerCode);
		newCustomerPage.setCustomerName(customerName);
		newCustomerPage.clickOnGeneralInfosSection();
		newCustomerPage.clickOnLegalEntityTypeDropDown();
		newCustomerPage.selectEntityTypeOption();
		newCustomerPage.clickOnBillingInfoSection();
		newCustomerPage.clickOnBillingCycleDropdown();
		waitPageLoaded();
		newCustomerPage.selectBillingCycleOption(billingCycle);
		newCustomerPage.clickOnCountryDropDown();
		waitPageLoaded();
		newCustomerPage.selectCustomerCountryOption(billingCountry);
		waitPageLoaded();
		newCustomerPage.clickOnLanguageDropDown();
		newCustomerPage.selectLanguageOption();
		newCustomerPage.clickOnCurrencyDropDown();
		newCustomerPage.selectCurrencyOption();
		newCustomerPage.clickOnContactSection();
		newCustomerPage.fillCustomerEmailInput(customerEmail);
		newCustomerPage.clickOnPayingAccountSection();
		newCustomerPage.clickOnPaymentMethodList();
		newCustomerPage.selectPaypalPaymentMethod();
		newCustomerPage.setUserID(paypalUserId);		
		newCustomerPage.clickOnGroupSection();
		newCustomerPage.clickOnClientCategoryDropDown();
		newCustomerPage.selectClientCategory();
		newCustomerPage.clickOnSubmitButton();
		waitPageLoaded();
		//System.out.println("customerCode : " + customerCode);
	}

	/***  create Customer  ***/
	public void createCustomerDirectDebit(String customerName,String customerEmail, String billingCountry,String accountAwner, String referenceMandate,
			String iban, String bic, String billingCycle, String acrs) throws InterruptedException {

		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCustomerCareButton();
		ListOfCustomersPage listOfCustomersPage = PageFactory.initElements(driver, ListOfCustomersPage.class);
		waitPageLoaded();
		listOfCustomersPage.clickOnCreateCustomerButton();
		NewCustomerPage newCustomerPage = PageFactory.initElements(driver, NewCustomerPage.class);
		waitPageLoaded();
		newCustomerPage.selectCustomerCompany();
		newCustomerPage.setCustomerCode(accountAwner);
		newCustomerPage.setCustomerName(customerName);
		newCustomerPage.clickOnGeneralInfosSection();
		newCustomerPage.clickOnLegalEntityTypeDropDown();
		newCustomerPage.selectEntityTypeOption();
		newCustomerPage.clickOnBillingInfoSection();
		newCustomerPage.clickOnBillingCycleDropdown();
		waitPageLoaded();
		newCustomerPage.selectBillingCycleOption(billingCycle);
		newCustomerPage.clickOnCountryDropDown();
		waitPageLoaded();
		newCustomerPage.selectCustomerCountryOption(billingCountry);
		waitPageLoaded();
		newCustomerPage.clickOnLanguageDropDown();
		newCustomerPage.selectLanguageOption();
		newCustomerPage.clickOnCurrencyDropDown();
		newCustomerPage.selectCurrencyOption();
		newCustomerPage.clickOnContactSection();
		newCustomerPage.fillCustomerEmailInput(customerEmail);
		newCustomerPage.clickOnGroupSection();
		newCustomerPage.clickOnSellerDropDown();
		newCustomerPage.selectSeller();
		newCustomerPage.clickOnClientCategoryDropDown();
		newCustomerPage.selectClientCategory();
		newCustomerPage.clickOnPayingAccountSection();
		newCustomerPage.clickOnPaymentMethodList();
		newCustomerPage.selectDirectDebitPaymentMethod(accountAwner, referenceMandate, iban, bic);
		newCustomerPage.clickOnSubmitButton();
		waitPageLoaded();
	}

	public void createCustomerCheck(String customerCode ,String customerName ,String customerEmail ,String billingCountry,String billingCycle,String companyRegistration, String version) throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		ListOfCustomersPage listOfCustomersPage = PageFactory.initElements(driver, ListOfCustomersPage.class);
		waitPageLoaded();
		try {
			listOfCustomersPage.clickOnCreateCustomerButton();
		}catch (Exception e) {
			homePage.clickOnCustomerCareButton0();
			listOfCustomersPage.clickOnCreateCustomerButton();
		}
		NewCustomerPage newCustomerPage = PageFactory.initElements(driver, NewCustomerPage.class);
		waitPageLoaded();
		newCustomerPage.selectCustomerCompany();
		newCustomerPage.setCustomerCode(customerCode);
		newCustomerPage.setCustomerName(customerName);
		newCustomerPage.clickOnGeneralInfosSection();
		if(version.equals("16.X") || version.equals("17.X")) {newCustomerPage.selectCompanyRegistration(companyRegistration);}
		else {newCustomerPage.setCompanyRegistration(companyRegistration);}
		if(version.equals("15.X")) {
			try {
				newCustomerPage.setApplicableCompanyRegistrationSystem();	
			}catch (Exception e) {}
		}
		newCustomerPage.clickOnLegalEntityTypeDropDown();
		waitPageLoaded();
		newCustomerPage.selectLegalEntityType();
		waitPageLoaded();
		newCustomerPage.clickOnBillingInfoSection();
		newCustomerPage.clickOnBillingCycleDropdown();
		waitPageLoaded();
		newCustomerPage.selectBillingCycle1(billingCycle);
		waitPageLoaded();
		newCustomerPage.clickOnCountryDropDown();
		newCustomerPage.selectCustomerCountry(billingCountry);
		waitPageLoaded();
		newCustomerPage.clickOnLanguageDropDown();
		waitPageLoaded();
		newCustomerPage.selectLanguageOption();
		newCustomerPage.clickOnCurrencyDropDown();
		waitPageLoaded();
		newCustomerPage.selectCurrencyOption();
		waitPageLoaded();
		newCustomerPage.clickOnContactSection();
		newCustomerPage.fillCustomerEmailInput(customerEmail);
		newCustomerPage.clickOnGroupSection();
		if(version.equals("16.X") || version.equals("17.X")) {newCustomerPage.selectCustomerCompanyRegistration(companyRegistration);}
		newCustomerPage.clickOnSellerDropDown();
		waitPageLoaded();
		newCustomerPage.selectSeller();
		newCustomerPage.clickOnClientCategoryDropDown();
		waitPageLoaded();
		newCustomerPage.selectClientCategory();
		newCustomerPage.clickOnPayingAccountSection();
		if(version.equals("16.X") || version.equals("17.X")) {newCustomerPage.selectCACompanyRegistration(companyRegistration);}
		newCustomerPage.clickOnPaymentMethodList();
		newCustomerPage.selectCheckPaymentMethod();
		newCustomerPage.clickOnSubmitButton();
		//Assert.assertEquals(newCustomerPage.getTextMessage(), "Element created");
	}
	
	
	/***  create Consumer  ***/
	public void createConsumerMethod(String consumerCode,String consumerName,String companyRegistrationNumber,String vatNumber, String acrs,String companyRegistration, String version ) throws InterruptedException {
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		try {
			customerPage.clickOnConsumersTab();
			waitPageLoaded();
		}catch (Exception e) {
			customerPage.clickOnSaveButton();
			waitPageLoaded();
			customerPage.clickOnConsumersTab();
			waitPageLoaded();
		}
		
		try {
			customerPage.clickOnCreateConsumerButton();
		}catch (Exception e) {
			customerPage.clickOnConsumersTab();
			waitPageLoaded();
			customerPage.clickOnCreateConsumerButton();
		}
		
		waitPageLoaded();
		NewConsumerPage newConsumerPage = PageFactory.initElements(driver, NewConsumerPage.class);
		newConsumerPage.setConsumerCode(consumerCode);
		newConsumerPage.setConsumerName(consumerName);
		waitPageLoaded();
		if(version.equals("16.X") || version.equals("17.X")) {
			newConsumerPage.selectCompanyRegistration(companyRegistration);
		}
		newConsumerPage.setLegalFrom();
		newConsumerPage.openApplicableCompanyRegistrationSystemList();
		newConsumerPage.setApplicableCompanyRegistrationSystem(acrs);
		newConsumerPage.setNumberVAT(vatNumber);
		newConsumerPage.clickOnSubmitButton();
		waitPageLoaded();
		newConsumerPage.clickOnGoBackButton();
		waitPageLoaded();
	}
	
	/***  search For Customer Method  ***/
	public void searchForCustomerMethod(String customerCode) {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		ListOfCustomersPage listOfCustomersPage = PageFactory.initElements(driver, ListOfCustomersPage.class);
		try {
			listOfCustomersPage.setSearchBarText(customerCode);
		}catch (Exception e) {
			homePage.clickOnGeneralSettingsMainMenu();
			homePage.clickOnCustomerCareButton();
			listOfCustomersPage.setSearchBarText(customerCode);
		}
		waitPageLoaded();
		listOfCustomersPage.clickCustomer(customerCode);
	}

	/***  Go to rated items tab  ***/
	public void GoToRatedItemsTab(String subCode){
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		customerPage.clickOnRatedItemsTab();
		waitPageLoaded();	
		RatedItemsPage ratedItemsPage = new RatedItemsPage();
		try {
			ratedItemsPage.setSearchValue(subCode);
		}catch (Exception e) {
			customerPage.clickOnRatedItemsTab();
			waitPageLoaded();	
			ratedItemsPage.setSearchValue(subCode);
		}
		waitPageLoaded();
	}
	
	/***  Go to rated items tab  ***/
	public void GoToContractTab(){
		customerPage = PageFactory.initElements(driver, CustomerPage.class);
		customerPage.clickOnRatedItemsTab();
		waitPageLoaded();	
	}
	
	/***   go To Security Deposit from customer page    ***/
	public void goToSecurityDepositTab() {
		customerPage = PageFactory.initElements(driver, CustomerPage.class);
		customerPage.clickOnSecurityDepositTab();
		listOfSecurityDepositPage = PageFactory.initElements(driver, ListOfSecurityDepositPage.class);
		waitPageLoaded();
	}
	/***   go To Invoice tab from customer page    ***/
	public void goToInvoiceTab() {
		customerPage = PageFactory.initElements(driver, CustomerPage.class);
		customerPage.clickOnInvoicesButton();
	}
	
	/***   Go To Balance tab from customer page ***/
	public void goToBalanceTab() {
		customerPage = PageFactory.initElements(driver, CustomerPage.class);
		customerPage.clickOnBalanceTab();
		waitPageLoaded();
		balancePage = new BalancePage();
	}
	
	public String getValueFromBalanceTable (int row , int column) {
		return driver.findElement(By.xpath("(//div[@id = 'payments-logs_CLIENT_LIST']//table[contains(@class , 'MuiTable-root')]//tr)[" + row + "]//td[" + column + "]")).getText();
	}
	
	/***   Go back ***/
	public void goBack() {
		customerPage = PageFactory.initElements(driver, CustomerPage.class);
		customerPage.clickOnGoBackButton();
	}
	
	public String getCustomerCode () {
		customerPage = PageFactory.initElements(driver, CustomerPage.class);
		return customerPage.getCustomerCode();
	}
	
	public void updateCustomer_billingcycle(String value) {
		customerPage = PageFactory.initElements(driver, CustomerPage.class);
		customerPage.clickOnBillingTab();
		customerPage.clickOnBillingCycleInput();
		customerPage.setBillingCycle(value);
		customerPage.clickOnSaveButton();
	}
}