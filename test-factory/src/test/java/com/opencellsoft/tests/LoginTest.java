package com.opencellsoft.tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.LoginPage;

public class LoginTest extends TestBase {

	@Parameters({ "login", "password" })
	@Test(priority = 1, groups = { "smokeTest", "regressionTest", "functionalTest" })
	public void loginTest(String login, String password) throws Exception {
		LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
		driver.navigate().refresh();
		waitPageLoaded();
		loginPage.setLogin(login);
		loginPage.setPassword(password);
		loginPage.clickOnLoginButton();
		waitPageLoaded();
	}
	
	public void login(String login, String password, String version) {
		LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.setLogin(login);
		loginPage.setPassword(password);
		loginPage.clickOnLoginButton();
	}
	
	public void changeLanguageToEnglish() {
		LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
		if(!loginPage.getFrButton().isEmpty()) {
			loginPage.clickOnFRButton();
			waitPageLoaded();
			loginPage.clickOnENButton();
			waitPageLoaded();
		}
	}

}