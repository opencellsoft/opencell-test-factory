package com.opencellsoft.tests;

import java.sql.SQLException;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.admin.AdminHomePage;
import com.opencellsoft.pages.admin.CustomizedEntitiesListePage;
import com.opencellsoft.pages.admin.NewCustomFieldPage;
import com.opencellsoft.pages.admin.NewCustomizedEntityPage;
import com.opencellsoft.pages.catalog.catalogmanager.referencetables.CustomTablePage;
import com.opencellsoft.pages.catalog.catalogmanager.referencetables.CustomTablesAddRows;
import com.opencellsoft.pages.catalog.catalogmanager.referencetables.CustomTablesListPage;
import com.opencellsoft.pages.commons.HomePage;

public class CustomTableTests extends TestBase {
	
	AdminHomePage adminHomePage;
	NewCustomizedEntityPage newCustomizedEntity;
	CustomizedEntitiesListePage customizedEntitiesListePage;
	NewCustomFieldPage newCustomFieldPage;
	HomePage homePage ;
	CustomTablesListPage customTablesListPage;
	CustomTablePage customTablePage;
	CustomTablesAddRows customTablesAddRows;
	
	public CustomTableTests() {
		adminHomePage = new AdminHomePage();
		customTablesListPage = new CustomTablesListPage();
		customTablePage = new CustomTablePage();
		customizedEntitiesListePage = new CustomizedEntitiesListePage();
		newCustomizedEntity = new NewCustomizedEntityPage();
		newCustomFieldPage = new NewCustomFieldPage();
		customTablesAddRows = new CustomTablesAddRows();
	}
	
	/***  go To Customized Entities Page  ***/
	public void goTocustomizedEntitiesPage(){
		adminHomePage = new AdminHomePage();
		adminHomePage.MoveToAdministrationMenu();
		adminHomePage.clickOnEntityCustomizationMenu();
		waitPageLoaded();
	}

	/***  add new Customized Entity ***/
	public void createNewCustomizedEntity(String code,String name, String description){
		customizedEntitiesListePage = new CustomizedEntitiesListePage();
		customizedEntitiesListePage.clickOnNewButton();
		waitPageLoaded();
		
		newCustomizedEntity = new NewCustomizedEntityPage();
		newCustomizedEntity.SetCode(code);
		waitPageLoaded();
		newCustomizedEntity.SetName(name);
		waitPageLoaded();
		newCustomizedEntity.SetDescription(description);
		waitPageLoaded();
		newCustomizedEntity.checkSeparateTableCheckbox();
		waitPageLoaded();
		newCustomizedEntity.clickOnSaveButton();
		waitPageLoaded();
	}
	
	/***  Add new field  ***/
	public void addNewCustomField(String code,String label, String type){
		newCustomizedEntity = new NewCustomizedEntityPage();
		newCustomizedEntity.clickOnAddFieldButton();
		waitPageLoaded();
		
		newCustomFieldPage = new NewCustomFieldPage();
		newCustomFieldPage.setCode(code);
		waitPageLoaded();
		newCustomFieldPage.selectType(type);
		waitPageLoaded();
		newCustomFieldPage.setLabel(label);
		waitPageLoaded();
		newCustomFieldPage.clickOnSaveButton();
		waitPageLoaded();
	}
	
	/***  go To Custom Table Page  ***/
	public void goToCustomTable(){
		homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnGeneralSettingsMainMenu();
		homePage.clickOnCatalogButtonMenu2();
		waitPageLoaded();
		homePage.clickOnCatalogueManagerButton();
		waitPageLoaded();
		homePage.clockOnReferenceTablesButton();
		waitPageLoaded();
		homePage.clockOnCustomTablesButton();
	}
	
	/***  search For Custom Table  ***/
	public void searchForCustomTable(String code){
		customTablesListPage = new CustomTablesListPage();
		customTablesListPage.setSearchInput(code);
		waitPageLoaded();
		customTablesListPage.clickOnResult(code);
	}
	
	/***  insert New Line  ***/
	public void insertNewLine(String year,String city,String country,String resident) {
		customTablePage = new CustomTablePage();
		customTablePage.clickOnEditeButton();
		customTablePage.clickOnAddRowsButton();
		
		customTablesAddRows = new CustomTablesAddRows();
		customTablesAddRows.doubleClickOnYear();
		customTablesAddRows.setYearInput(year);
		customTablesAddRows.doubleClickOnCountry();
		customTablesAddRows.setCountryInput(country);
		customTablesAddRows.doubleClickOnCity();
		customTablesAddRows.setCityInput(city);
		customTablesAddRows.doubleClickOnResident();
		customTablesAddRows.setResidentInput(resident);
		customTablesAddRows.clickOnSaveButton();
	}
	
	public boolean checkValuesInCustomTable(String year,String country,String city,String resident) {
		String dgValues = customTablesListPage.getDataGridElt();
		System.out.println(dgValues);
		if (dgValues.contains(year)&& dgValues.contains(resident))
		//if (dgValues.contains(year)&& dgValues.contains(country) && dgValues.contains(city) && dgValues.contains(resident))
			return true;
		else 
			return false;
	}
	
	public void insertLines(String dbURI, String query) throws SQLException, ClassNotFoundException {
		
		// Open new Tab
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs;
		tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		
		// Open Postgree
		driver.get( dbURI);
		waitPageLoaded();
		WebElement usernameInput = driver.findElement(By.xpath("//input[@id='username']"));
		usernameInput.sendKeys("rajae.test");
		waitPageLoaded();
		WebElement passwordInput = driver.findElement(By.xpath("//input[@id='password']"));
		passwordInput.sendKeys("Rajae.test123!");
		waitPageLoaded();
		WebElement submitButton = driver.findElement(By.xpath("//input[@type='submit']"));
		clickOn(submitButton);
		
		//Open request editor
		WebElement goButton = driver.findElement(By.xpath("//input[@type = 'submit' and @value = 'GO !']"));
		clickOn(goButton);
		
		// Execute SQL request 
		WebElement requeteSQL = null;
		WebElement spanInput = null;
		WebElement executeButton = null;

		requeteSQL = driver.findElement(By.xpath("//div[@id = 'menu']//p//a[contains(text(), 'SQL')]"));
		clickOn(requeteSQL);
		spanInput = driver.findElement(By.xpath("//form[@id = 'form']//pre//span"));
		spanInput.sendKeys(query);
		waitPageLoaded();
		waitPageLoaded();
		executeButton = driver.findElement(By.xpath("(//form[@id = 'form']//input[@type = 'submit'])[1]"));
		clickOn(executeButton);

		try {
			WebElement goButton1 = driver.findElement(By.xpath("//input[@type = 'submit' and @value = 'GO !']"));
			clickOn(goButton1);
		}catch (Exception e) {
			waitPageLoaded();
		}
		
		//Close Postgree
		driver.close();
		
		// switch to portal
		tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(0));
		waitPageLoaded();
	}
	
	public void deleteLine() {
		customTablePage = new CustomTablePage();
		customTablePage.clickOnEditeButton();
		customTablePage.clickOnDeleteButton();
		customTablePage.clickOnSaveButton();
	}
	
	public boolean valueIsPresent(String value) {
		customTablePage = new CustomTablePage();
		return customTablePage.valueIsPresent(value);
	}
	
	public boolean rowIsPresent(String value) {
		customTablePage = new CustomTablePage();
		return customTablePage.rowIsPresent(value);
	}
	
	public void updatePagination25() {
		customTablePage = new CustomTablePage();
		 customTablePage.select25RowsPerPage();
		 waitPageLoaded();
	}
	
	public void nextPage() {
		customTablePage = new CustomTablePage();
		customTablePage.clickOnNextPageButton();
		waitPageLoaded();
	}
	
	public void previousPage() {
		customTablePage = new CustomTablePage();
		customTablePage.clickOnPreviousPageButton();
	}
	public void downlaodXLSX() {
		customTablePage = new CustomTablePage();
		customTablePage.clickOnActionButton();
		customTablePage.clickOnDownlaodXLSXButton();
	}
	
	public void downlaodCSV() {
		customTablePage = new CustomTablePage();
		customTablePage.clickOnActionButton();
		customTablePage.clickOndownlaodCSVButton();
	}
}
