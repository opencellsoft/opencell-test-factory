package com.opencellsoft.tests;

import org.testng.Assert;
import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.operation.reportextracts.ReportExtractsPage;

public class ReportExtractsTests extends TestBase {
	ReportExtractsPage reportExtractsPage;
	
	public void searchReportExtractsByName(String value) {
		reportExtractsPage = new ReportExtractsPage();
		reportExtractsPage.setSearchInput(value);
	}
	
	public int reportExtractsTotal() {
		reportExtractsPage = new ReportExtractsPage();
		return reportExtractsPage.reportExtractsList().size();
	}
	
	public void sortReportExtractsListBy(String value) {
		reportExtractsPage = new ReportExtractsPage();
		reportExtractsPage.sortBy(value);
	}
	
	public void runReport() {
		reportExtractsPage = new ReportExtractsPage();
		reportExtractsPage.clickOnRunReportButton();
		reportExtractsPage.clickOnConfirmButton();
	}

	public void downloadReport() {
		reportExtractsPage = new ReportExtractsPage();
		reportExtractsPage.clickOnDownloadReportButton();
		Assert.assertEquals(reportExtractsPage.message(), "Action executed successfuly");
	}
}
