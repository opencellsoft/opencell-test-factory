package com.opencellsoft.tests;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.catalog.catalogmanager.attributegroups.AttributeGroupPage;
import com.opencellsoft.pages.catalog.catalogmanager.attributegroups.AttributeGroupsListPage;

public class AttributeGroupsTests  extends TestBase  {

	AttributeGroupsListPage attributeGroupsListPage;
	AttributeGroupPage attributeGroupPage;
	
	public void createAttributeGroups(String attributeGroup_Code,String attributeGroup_description,String attribute1) {
		attributeGroupsListPage= new AttributeGroupsListPage();
		attributeGroupsListPage.clickOnCreateButton();
		
		attributeGroupPage = new AttributeGroupPage();
		attributeGroupPage.setCode(attributeGroup_Code);
		attributeGroupPage.setDescription(attributeGroup_description);
		attributeGroupPage.setAttributes(attribute1);

		attributeGroupPage.clickOnSaveButton();
	}
	
	public void updateAttributeGroups(String attributeGroup_description) {
		attributeGroupPage = new AttributeGroupPage();
		if(attributeGroup_description != null) {attributeGroupPage.setDescription(attributeGroup_description);}

		attributeGroupPage.clickOnSaveButton();
	}
	
	public void goBack() {
		attributeGroupPage = new AttributeGroupPage();
		attributeGroupPage.clickOnGoBackButton();
	}
	
	public void searchAttributeGroupByCode(String value) {
		attributeGroupsListPage= new AttributeGroupsListPage();
		attributeGroupsListPage.setSearchInput(value);
	}
	
	public int attributeGroupsTotal() {
		attributeGroupsListPage= new AttributeGroupsListPage();
		return attributeGroupsListPage.attributeGroupsList().size();
	}
	
	public void goToAttributeGroupsDetails (String email) {
		attributeGroupsListPage= new AttributeGroupsListPage();
		attributeGroupsListPage.goToAttributeGroupsDetails(email);
	}
	public void activateFilterBy(String value) {
		attributeGroupsListPage= new AttributeGroupsListPage();
		attributeGroupsListPage.clickOnFilterButton();
		attributeGroupsListPage.activateFilterBy(value);
	}
	
	public void filterAttributeGroupsByEmpty(Boolean value) {
		attributeGroupsListPage= new AttributeGroupsListPage();
		attributeGroupsListPage.setEmptyInput(value);
	}
	
	public void filterAttributeGroupsByDescription(String description) {
		attributeGroupsListPage= new AttributeGroupsListPage();
		attributeGroupsListPage.setSearchByDescriptionInput(description);
	}
	
	public void sortAttributeGroupsListBy(String value) {
		attributeGroupsListPage= new AttributeGroupsListPage();
		attributeGroupsListPage.sortBy(value);
	}
	
	public void closeFilterBy(String value) {
		attributeGroupsListPage= new AttributeGroupsListPage();
		attributeGroupsListPage.clickOnRemoveFilterBy(value);
	}
}
