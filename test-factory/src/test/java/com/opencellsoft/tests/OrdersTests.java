package com.opencellsoft.tests;

import java.awt.AWTException;
import java.util.List;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Ignore;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.customers.CustomerPage;
import com.opencellsoft.pages.customers.ListOfCustomersPage;
import com.opencellsoft.pages.orders.NewOrderLinePage;
import com.opencellsoft.pages.orders.NewOrderPage;
import com.opencellsoft.pages.orders.OrderOfferPage;
import com.opencellsoft.pages.orders.OrderPage;
import com.opencellsoft.pages.orders.OrdersListPage;
import com.opencellsoft.utility.Constant;

public class OrdersTests extends TestBase {

	// @Parameters({"customerCode" })
	@Test(priority = 32, groups = { "smoke tests" })
	public void validateOrder() throws InterruptedException {
		HomePage homepage = PageFactory.initElements(driver, HomePage.class);
		homepage.clickOnCustomerCareButton();
		homepage.clickOnOrdersTab();
		OrdersListPage ordersListPage = PageFactory.initElements(driver, OrdersListPage.class);
		ordersListPage.findQuote(Constant.customerCode);
		waitPageLoaded();
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		//orderPage.clickOnDefaultConsumer();
		//waitPageLoaded();
		//orderPage.clickOnConsumer(Constant.consumerCode);
		Thread.sleep(2000);
		orderPage.clickOnValidateOrderButton();
		waitPageLoaded();
		Assert.assertEquals(orderPage.getTextMessage(), "Action executed successfuly");
	}
	@Test(priority = 33, groups = { "smoke tests" })
	public void viewSubscription() throws InterruptedException {
		HomePage homepage = PageFactory.initElements(driver, HomePage.class);
		homepage.clickOnCustomerCareButton();
		ListOfCustomersPage listOfCustomersPage = PageFactory.initElements(driver, ListOfCustomersPage.class);
		Thread.sleep(2000);
		listOfCustomersPage.clickOnFilterButton();
		Thread.sleep(2000);
		listOfCustomersPage.clickOnFilterByCustomerCode();
		listOfCustomersPage.setCustomerCode(Constant.customerCode);
		listOfCustomersPage.clickOnCustomer(Constant.customerCode);
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		waitPageLoaded();
		customerPage.clickOnSubscriptionTab();
		waitPageLoaded();
		customerPage.clickOnRefreshButton();
		waitPageLoaded();
		customerPage.clickOnGeneratedSubscription();
	}
	@Test(priority = 33, groups = { "smoke tests" })
	public void viewGeneratedSubscription() throws InterruptedException {
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		orderPage.clickOnCustomerLink(Constant.customerCode);
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		customerPage.clickOnSubscriptionTab();
		waitPageLoaded();
		customerPage.clickOnRefreshButton();
		waitPageLoaded();
		customerPage.clickOnGeneratedSubscription();
		customerPage.clickOnGoBackButton();
	}
	@Test(priority = 33, groups = { "smoke tests" })
	public void viewCreatedSubscription() throws InterruptedException {
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		orderPage.clickOnCustomerLink(Constant.customerCode);
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		customerPage.clickOnSubscriptionTab();
		waitPageLoaded();
		customerPage.clickOnRefreshButton();
		waitPageLoaded();
		customerPage.clickOnCreatedSubscription(Constant.subscriptionCode);
	}

	@Ignore
	@Test(priority = 2)
	public void finalizeOrder() throws Exception {
		HomePage homepage = PageFactory.initElements(driver, HomePage.class);
		homepage.clickOnOrdersTab();
		OrdersListPage ordersListPage = PageFactory.initElements(driver, OrdersListPage.class);
		ordersListPage.findQuote(Constant.quoteCode);
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		orderPage.clickOnAddOfferButton();
		OrderOfferPage orderOfferPage = PageFactory.initElements(driver, OrderOfferPage.class);
		orderOfferPage.selectOffer(Constant.offerCode);
		orderOfferPage.clickGoBack();
	}

	@Test(priority = 34, dependsOnMethods = "com.opencellsoft.tests.QuotesTests.validateDuplicateQuote")
	public void addInvoicingPlanToDraftOrder() throws Exception {
		HomePage homepage = PageFactory.initElements(driver, HomePage.class);
		waitPageLoaded();
		homepage.clickOnCustomerCareButton();
		waitPageLoaded();
		homepage.clickOnOrdersTab();
		OrdersListPage ordersListPage = PageFactory.initElements(driver, OrdersListPage.class);
		waitPageLoaded();
		ordersListPage.findQuote(Constant.customerCode);
		waitPageLoaded();
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		waitPageLoaded();
		orderPage.invoicingPlanList();
		orderPage.setInvoicingPlanCode(Constant.invoicingPlanName);
		waitPageLoaded();
		orderPage.selectInvoicingPlan(Constant.invoicingPlanName);
	}

	@Test(priority = 35, dependsOnMethods = "addInvoicingPlanToDraftOrder")
	public void finalizeOrderWithInvoicingPlan() throws Exception {
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		orderPage.clickFinalize();
		Assert.assertEquals(orderPage.getTextMessage(), "Action executed successfuly");
	}

	@Test(priority = 36, dependsOnMethods = "finalizeOrderWithInvoicingPlan")
	public void updateOrderProgressToNextLevel() throws Exception {
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		orderPage.clickUpdateProgressToNextLevel1();
		// Assert.assertTrue(orderPage.getTextMessage().toLowerCase().contains("action
		// executed successfuly"));
		waitPageLoaded();
		orderPage.clickUpdateProgressToNextLevel2();
		//Assert.assertTrue(orderPage.getTextMessage().toLowerCase().contains("action executed successfuly"));
	}

	@Parameters({ "orderInvoiceAmount1","orderInvoiceAmount2" })
	@Test(priority = 36, dependsOnMethods = "updateOrderProgressToNextLevel")
	public void checkInvoicesLinkedToOrder(String orderInvoiceAmount1, String orderInvoiceAmount2) throws AWTException {
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		Assert.assertEquals(orderPage.getInvoiceAmount1(), orderInvoiceAmount1);
		Assert.assertEquals(orderPage.getInvoiceAmount2(), orderInvoiceAmount1);
		Assert.assertEquals(orderPage.getInvoiceAmount3(), orderInvoiceAmount2);
	}

	@Test(priority = 37, dependsOnMethods = "updateOrderProgressToNextLevel")
	public void viewSubscriptionWithInvoicingPlan() throws InterruptedException {
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		//		orderPage.clickOnCustomerLink(Constant.customerCode);
		orderPage.clickOnCustomerLink();
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		Thread.sleep(1000);
		customerPage.clickOnSubscriptionTab();
		Thread.sleep(5000);
		customerPage.clickOnSubscriptionWithInvoicingPlan();
	}


	public void createOrderFromCustomerPage() {
		// Go to order tab
		goToOrderTab();

		// create a new order
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		customerPage.clickOnCreateOrder();		
		waitPageLoaded();
		NewOrderPage newOrderPage = PageFactory.initElements(driver, NewOrderPage.class);
		newOrderPage.clickOnSaveButton();
	}
	
	public void createOrderFromOrdersListPage(String customerName, String version) {

		// create a new order
		OrdersListPage ordersListPage = PageFactory.initElements(driver, OrdersListPage.class);
		if(version.equals("14.1.X")) {
			ordersListPage.clickOnCreateOrderButton();	
		}else{
			ordersListPage.clickOnCreateOrder();
		}
			
		NewOrderPage newOrderPage = PageFactory.initElements(driver, NewOrderPage.class);
		newOrderPage.clickOnCustomerList();
		newOrderPage.setCustomerDescription(customerName);
		newOrderPage.clickOnFirstResult();
		newOrderPage.clickOnSaveButton();
	}
	
	public void goToOrderTab() {
		// Go to order tab
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		customerPage.clickOnOrderTab();
		waitPageLoaded();
	}
	
	public void createOrderLine(String offerCode) {
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		orderPage.clickOnAddOrderLineButton();
		orderPage.clickOnCreateOrderLineButton();
		NewOrderLinePage newOrderLinePage = new NewOrderLinePage();
		newOrderLinePage.clickOnOfferInput();
		newOrderLinePage.clickOnAddFilterButton();
		newOrderLinePage.selectFilterOptionCode();
		newOrderLinePage.setCodefilter(offerCode);
		newOrderLinePage.clickOnselectedResult(offerCode);
		newOrderLinePage.clickOnSaveButton();
		newOrderLinePage.clickOnGoBackButton();
	}
	
	public void createOrderLineOneShot(String SubCode, String ProductCode, String line,String quantity) {
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		try {
			orderPage.clickOnAddOrderLineButton();
		}
		catch (Exception e) {
			try {
				orderPage.clickOnSaveOrderButton();
				orderPage.clickOnAddOrderLineButton();
			}catch (Exception ex) {
				orderPage.clickOnGoBackButton1();
				orderPage.clickOnAddOrderLineButton();
			}
			
		}
		try {
			orderPage.clickOnApplyOneShotButton();
		}catch (Exception e) {
			orderPage.clickOnAddOrderLineButton();
			orderPage.clickOnApplyOneShotButton();
		}
		
		NewOrderLinePage newOrderLinePage = new NewOrderLinePage();
		newOrderLinePage.clickOnSubscriptionInput();
		newOrderLinePage.clickOnAddFilterButton();
		newOrderLinePage.selectFilterOptionCode();
		newOrderLinePage.setCodefilter(SubCode);
		newOrderLinePage.clickOnselectedResult(SubCode);
		newOrderLinePage.clickOnProductTab(ProductCode);
		newOrderLinePage.setQuantityInput(line, quantity);
		
	}
	
	public void addProductToOrderLine(String line,String quantity) {


		NewOrderLinePage newOrderLinePage = new NewOrderLinePage();

		newOrderLinePage.setQuantityInput(line, quantity);
		
	}
	
	public void clickOnProductTab(String ProductCode){
		NewOrderLinePage newOrderLinePage = new NewOrderLinePage();
		newOrderLinePage.clickOnProductTab(ProductCode);
	}
	
	public void save(){
		NewOrderLinePage newOrderLinePage = new NewOrderLinePage();
		newOrderLinePage.clickOnSaveButton();
	}
	
	
	public String validateOrder1() {
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		orderPage.clickOnValidateOrderButton();
		String orderNumber;
		try {
			orderNumber = orderPage.getOrderNumber();
		}catch (Exception e) {
			orderPage.clickOnValidateOrderButton();
			orderNumber = orderPage.getOrderNumber();
		}
		
		return orderNumber;
	}
	
	public void applyChanges(){
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		orderPage.clickOnApplyButton();
	}
	
	public void goBack() {
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		orderPage.clickOnGoBackButton1();
	}
	
	public void duplicateOrder() {
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		orderPage.clickOnDuplicateOrderButton();
	}
	
	public List<String> getOrderDetails(){
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		return orderPage.getOrderDetails();
	}
	
	public List<String> getOrderLineDetails( int i, String version){
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);	
		return orderPage.getOrderLineDetails(i, version);
	}
	
	public void deleteOrderLine(String i){
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		orderPage.checkOrderLine(i);
		orderPage.clickOnDeleteOrderLineButton();
		orderPage.clickOnConfirmButton();
		orderPage.clickOnSaveOrderButton();
	}
	public void goToOrderLineDetails(String order) {
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		orderPage.goToOrderLineDetails(order);
	}
	
	public void changeQuantityOfOrderLine( String ProductCode,String line, String quantity){
		NewOrderLinePage newOrderLinePage = new NewOrderLinePage();
		newOrderLinePage.clickOnProductTab(ProductCode);
		newOrderLinePage.setQuantityInput(line, quantity);
		newOrderLinePage.clickOnSaveButton();
	}
	
	public void displayOtherAttributesPopup(int i){
		NewOrderLinePage newOrderLinePage = new NewOrderLinePage();
		newOrderLinePage.clickOnOtherAttributesButton(i);

	}
	
	public void closeOtherAttributesPopup(){
		NewOrderLinePage newOrderLinePage = new NewOrderLinePage();
		newOrderLinePage.clickOnCloseOtherAttributesButton();

	}
	
	public void updateAttributValueInTheTable(String attributeName, String newAttributeValue) {
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		orderPage.updateAttributValueInTheTable(attributeName, newAttributeValue);
	}
	
	public void updateAttributValueInThePopup(String attributeName, String newAttributeValue) {
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		orderPage.updateAttributValueInThePopup(attributeName, newAttributeValue);
	}
}