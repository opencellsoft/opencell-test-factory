package com.opencellsoft.tests;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.operation.rateditems.RatedItemsPage;

public class RatedItemsTests extends TestBase {
	RatedItemsPage ratedItemsPage;
	
	public void AddfilterBySubscription() {
		ratedItemsPage = new RatedItemsPage();
		ratedItemsPage.clickOnFilterButton();
		ratedItemsPage.selectFilterBySubscription();
	}
	public void filterBySubscription(String value) {
		ratedItemsPage.clickOnFilterBySubscription();
		try {
			ratedItemsPage.setSubscriptionCodeInput(value);
		}catch (Exception e) {
			ratedItemsPage.clickOnFilterBySubscription();
			ratedItemsPage.setSubscriptionCodeInput(value);
		}
		waitPageLoaded();
		waitPageLoaded();
		ratedItemsPage.clickOnResult(value);
	}
	
	public void filterByParamExtra(String value) {
		ratedItemsPage = new RatedItemsPage();
		ratedItemsPage.clickOnFilterButton();
		ratedItemsPage.selectFilterByParamExtra();
		ratedItemsPage.setParamExtraInput(value);
		waitPageLoaded();
	}
	
	// return the number of lines found
	public int elementsFoundSize(String value) {
		ratedItemsPage = new RatedItemsPage();
		return ratedItemsPage.elementsFoundSize(value);
	}
	
	public void showRowsPerPage(String rows){
		ratedItemsPage = new RatedItemsPage();
		ratedItemsPage.selectRowsPerPage(rows);
	}
}
