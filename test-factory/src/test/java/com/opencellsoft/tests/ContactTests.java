package com.opencellsoft.tests;

import java.awt.AWTException;

import org.testng.Assert;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.customercare.contacts.ContactPage;
import com.opencellsoft.pages.customercare.contacts.ContactsListPage;

public class ContactTests extends TestBase {
	ContactPage contactPage;
	ContactsListPage contactListPage;
	
	/***  Create a new contact  ***/
	public void createContact(String title,String name, String surname, String phone, String email, String country, String city, String company, String position, Boolean mainContact, String line ) throws InterruptedException, AWTException {
		ContactsListPage contactListPage = new ContactsListPage();
		contactListPage.clickOnCreateButton();

		ContactPage contactPage = new ContactPage();
		contactPage.setTitle(title);
		contactPage.setName(name);
		contactPage.setSurname(surname);
		contactPage.setPhone(phone);
		contactPage.setEmail(email);
		contactPage.setCountry(country);
		contactPage.setCity(city);
		contactPage.setCompany(company,  line);
		contactPage.setPosition(position, line);
		contactPage.setMainContact(mainContact, line);
		contactPage.clickOnSaveButton();

	}

	/***  update contact  ***/
	public void updateContact(String title,String name, String surname, String phone, String email, String country, String city, String company, String position, Boolean mainContact, String line ) throws InterruptedException, AWTException {

		ContactPage contactPage = new ContactPage();
		contactPage.clickOnUpdateButton();
		if(title != null) {contactPage.setTitle(title);}
		if(name != null) {contactPage.setName(name);}
		if(surname != null) {contactPage.setSurname(surname);}
		if(phone != null) {contactPage.setPhone(phone);}
		if(email != null) {contactPage.setEmail(email);}
		if(country != null) {contactPage.setCountry(country);}
		if(city != null) {contactPage.setCity(city);}
		if(company != null) {contactPage.setCompany(company,  line);}
		if(position != null) {contactPage.setPosition(position, line);}
		if(mainContact != null) {contactPage.setMainContact(mainContact, line);}
		contactPage.clickOnSaveButton();
	}
	
	public void goBack() {
		contactPage = new ContactPage();
		contactPage.clickOnGoBackButton();
	}

	public void searchContact(String value) {
		contactListPage = new ContactsListPage();
		contactListPage.setSearchInput(value);
	}
	
	public int contactTotal() {
		contactListPage = new ContactsListPage();
		return contactListPage.contactList().size();
	}
	
	public void sortContactListBy(String value) {
		contactListPage = new ContactsListPage();
		contactListPage.sortBy(value);
	}
	
	public void goToContactDetails (String email) {
		contactListPage = new ContactsListPage();
		contactListPage.goToContactDetails(email);
	}
}
