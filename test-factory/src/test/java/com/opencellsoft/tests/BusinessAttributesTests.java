package com.opencellsoft.tests;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.catalog.catalogmanager.businessattributes.BusinessAttributesListPage;

public class BusinessAttributesTests extends TestBase  {
	BusinessAttributesListPage businessAttributesListPage;
	
	public void searchBusinessAttributesByName(String value) {
		businessAttributesListPage= new BusinessAttributesListPage();
		businessAttributesListPage.setSearchInput(value);
	}
	
	public int businessAttributesTotal() {
		businessAttributesListPage= new BusinessAttributesListPage();
		return businessAttributesListPage.businessAttributesList().size();
	}
	
	public void activateFilterBy(String value) {
		businessAttributesListPage= new BusinessAttributesListPage();
		businessAttributesListPage.clickOnFilterButton();
		businessAttributesListPage.activateFilterBy(value);
	}
	
	public void filterBusinessAttributesByValue(String value) {
		businessAttributesListPage= new BusinessAttributesListPage();
		businessAttributesListPage.setValueInput(value);
	}
	
	public void sortBusinessAttributesListBy(String value) {
		businessAttributesListPage = new BusinessAttributesListPage();
		businessAttributesListPage.sortBy(value);
	}
	public void delete() {
		businessAttributesListPage = new BusinessAttributesListPage();
		businessAttributesListPage.clickOnCheckAllCheckBox();
		businessAttributesListPage.clickOnDeleteButton();
		businessAttributesListPage.clickOnConfirmButton();
	}
	
	public void closeFilterBy(String value) {
		businessAttributesListPage= new BusinessAttributesListPage();
		businessAttributesListPage.clickOnRemoveFilterBy(value);
	}
}
