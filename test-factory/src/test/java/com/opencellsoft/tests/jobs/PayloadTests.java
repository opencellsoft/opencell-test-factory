package com.opencellsoft.tests.jobs;

import static io.restassured.RestAssured.given;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencellsoft.base.TestBase;
import io.restassured.RestAssured;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class PayloadTests extends TestBase {

	/***  create Customer  ***/
	public void createCustomer(String baseURI, String login, String password, String code, String description, String BcCode) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/account/accountHierarchy/createCRMAccountHierarchy";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		// create API request body
		String body = "{\r\n"
				+ "    \"code\": \"" + code + "\",\r\n"
				+ "    \"description\": \"" + description + "\",\r\n"
				+ "    \"billingCycle\": \"" + BcCode + "\",\r\n"
				+ "    \"crmAccountType\": \"C_UA\",\r\n"
				+ "    \"language\": \"FRA\",\r\n"
				+ "    \"customerCategory\": \"CLIENT\",\r\n"
				+ "    \"currency\": \"EUR\",\r\n"
				+ "    \"country\": \"FR\",\r\n"
				+ "    \"seller\": \"MAIN_SELLER\",\r\n"
				+ "    \"taxCategoryCode\": \"REGULAR\",\r\n"
				+ "    \"isCompany\": true\r\n"
				+ "}";
		System.out.println("create customer : " + body);
		given().header("Content-Type", "application/json").body(body).when().post();

	}

	/***  create Subscription And Instantiate Product  ***/
	public void createSubscriptionAndInstantiateProduct(String baseURI, String login, String password, String subCode,String customerCode, String offerCode, String productCode) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/billing/subscription/subscribeAndInstantiateProducts";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		// Send request
		Calendar calendar;

		// get date format "yyyy-MM-dd"
		calendar = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.FRANCE);
		String date = format.format(calendar.getTime());

		// create API request body
		String body = "{\r\n"
				+ "    \"code\": \"" + subCode + "\",\r\n"
				+ "    \"userAccount\": \"" + customerCode + "\",\r\n"
				+ "    \"offerTemplate\": \"" + offerCode + "\",\r\n"
				+ "    \"subscriptionDate\": \"" + date + "\",\r\n"
				+ "    \"productToInstantiateDto\": [\r\n"
				+ "    		{\r\n"
				+ "    			\"productCode\": \""+ productCode +"\",\r\n"
				+ "    			\"quantity\": 1\r\n"
				+ "    		}\r\n"
				+ "    	]\r\n"
				+ "}";
		System.out.println("InstantiateProduct : " + body);
		given().header("Content-Type", "application/json").body(body).when().post();

	}


	/***  activate Services  ***/
	public void activateServices(String baseURI, String login, String password, String subCode, String productCode) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/billing/subscription/activateServices";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		// Send request
		Calendar calendar;

		// get date format "yyyy-MM-dd"
		calendar = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.FRANCE);
		String date = format.format(calendar.getTime());

		// create API request body
		String body = "{\r\n"
				+ "    \"subscription\": \"" + subCode + "\",\r\n"
				+ "    \"servicesToActivate\":{\r\n"				
				+ "    		\"service\": [\r\n"
				+ "    			{\r\n"
				+ "    				\"code\": \""+ productCode +"\",\r\n"
				+ "    				\"subscriptionDate\": \"" + date + "\",\r\n"
				+ "    				\"quantity\": 1\r\n"
				+ "    			}\r\n"
				+ "    		]\r\n"
				+ "   	}\r\n"
				+ "}";
		System.out.println("activateServices : " + body);
		given().header("Content-Type", "application/json").body(body).when().post();

	}

	/***  create Access Point  ***/
	public void createAccessPoint(String baseURI, String login, String password, String Code, String subCode) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/account/access";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		// create API request body
		String body = "{\r\n"
				+ "    \"subscription\": \"" + subCode + "\",\r\n"
				+ "    \"code\": \""+ Code +"\"\r\n"
				+ "}";
		System.out.println("createAccessPoint : " + body);
		given().header("Content-Type", "application/json").body(body).when().post();

	}

	/***  insert Charge Cdr  ***/
	public void insertSeveralChargeCdr(String baseURI, String login, String password, String AP, String param1, String param2, String param3) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/v2/mediation/cdrs/chargeCdrList";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;
		String date;
		int year = LocalDate.now().getYear();
		int month = LocalDate.now().getMonthValue();
		// create API request body
		String body = "{\r\n"
				+ "    \"cdrs\": [\r\n";
		for (int i=1; i<30; i++){
			date = year + "-" + month + "-" + i + "T00:00:00.00Z";
			body = body + "    \"" + date + ";1;" + AP + ";"+ param1 + ";"+ param2 + ";" + param3 + ";;;;;;;;;;;;;;;\",\r\n";
		}
		date = year + "-" + month + "-30T00:00:00.00Z";
		body = body + "    \"" + date + ";1;" + AP + ";"+ param1 + ";"+ param2 + ";" + param3 + ";;;;;;;;;;;;;;;\"\r\n";
		body = body+ "    ]\r\n"
				+ "}";

		System.out.println("request insert charges : ----------------");
		System.out.println( body);

		given().header("Content-Type", "application/json").body(body).when()
		.post();
	}

	/***  Activate Functions  ***/
	public void activateFunctions(String baseURI, String login, String password) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/v2/accountReceivable/financeSettings/1";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		// create API request body
		String body = "{\r\n"
				+ "    \"activateDunning\": true,\r\n"
				+ "    \"enableBillingRedirectionRules\": true,\r\n"
				+ "    \"enablePriceList\": true,\r\n"
				+ "    \"discountAdvancedMode\": true,\r\n"
				+ "    \"handleFrameworkAgreement\": true,\r\n"
				+ "    \"handleAccountingPeriods\": true,\r\n"
				+ "    \"handleInvoicingPlans\": true\r\n"
				+ "}";
		System.out.println("ActivateFunctions : " + body);
		given().header("Content-Type", "application/json").body(body).when().put();

	}

	/***  Activate dunning settings  ***/
	public void dunningSettings(String baseURI, String login, String password) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/v2/generic/all/DunningSettings";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		// create API request body
		String body = "{\r\n"
				+ "    \"limit\": 1,\r\n"
				+ "    \"nestedEntities\": [\r\n"
				+ "    \"dunningPauseReasons\",\r\n"
				+ "    \"dunningStopReasons\",\r\n"
				+ "    \"dunningCollectionPlanStatuses\"]\r\n"
				+ "}";
		System.out.println("ActivateFunctions : " + body);
		given().header("Content-Type", "application/json").body(body).when().post();

	}

	/***  delete Invocing Job V2  ***/
	public void delete_Invocing_Job_V2(String baseURI, String login, String password) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/jobInstance/Invoicing_Job_V2";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		// create API request body
		String body = "{\r\n"
				+ "    \"code\": \"Invoicing_Job_V2\" \r\n"
				+ "}";
		System.out.println("delete_Invocing_Job_V2 : ");
		System.out.println("URL : " + RestAssured.baseURI + RestAssured.basePath);
		System.out.println("Body : " + body);
		given().header("Content-Type", "application/json").body(body).when().delete();

	}

	/***  create Invoicing Job V3  ***/
	public void create_Invoicing_Job_V3(String baseURI, String login, String password) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/jobInstance/createOrUpdate";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		// create API request body
		String body = "{\r\n"
				+ "    \"code\": \"Invoicing_Job_V2\", \r\n"
				+ "    \"jobTemplate\": \"InvoicingJobV3\", \r\n"
				+ "    \"followingJob\": \"XML_Job\" \r\n"
				+ "}";
		System.out.println("create_Invoicing_Job_V3 : ");
		System.out.println("URL : " + RestAssured.baseURI + RestAssured.basePath);
		System.out.println("Body : " + body);
		given().header("Content-Type", "application/json").body(body).when().post();

	}

	/***  update invoice Lines Job  ***/
	public void update_invoiceLinesJob(String baseURI, String login, String password) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/jobInstance/createOrUpdate";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		// create API request body
		String body = "{\r\n"
				+ "    \"code\": \"Invoice_Lines_Job_V2\", \r\n"
				+ "    \"jobTemplate\": \"InvoiceLinesJob\", \r\n"
				+ "    \"followingJob\": \"Invoicing_Job_V2\" \r\n"
				+ "}";
		System.out.println("update_invoiceLinesJob : ");
		System.out.println("URL : " + RestAssured.baseURI + RestAssured.basePath);
		System.out.println("Body : " + body);
		given().header("Content-Type", "application/json").body(body).when().post();

	}

	/***  create usage Charge  ***/
	public void create_usageCharge(String baseURI, String login, String password, String code, String description) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/catalog/usageChargeTemplate";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		// create API request body
		String body = "{\r\n"
				+ "    \"code\": \"" + code + "\", \r\n"
				+ "    \"description\": \"" + description + "\" \r\n"
				+ "}";
		System.out.println("create_usageCharge : " + body);
		given().header("Content-Type", "application/json").body(body).when().post();

	}

	/*** apply One Shot Charge Instance ***/
	public String applyOneShotChargeInstance(String baseURI, String login, String password,String chargeCode, String subCode, String periode, String time) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/billing/subscription/applyOneShotChargeInstance";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		// Send request
		//Calendar calendar;
		Response resp;
		JsonPath j;
		String l="FAIL";
		//String l="SUCCESS";
		for (int i=0; i<5; i++){
			if(!l.equals("SUCCESS")) {
				Thread.sleep(1000);

				LocalDateTime currentDateTime = LocalDateTime.now();
				if(time.equals("past") ) {
					// Subtract two month
					currentDateTime = currentDateTime.minusMonths(2);
				}
				else if(time.equals("futur") ) {
					// Subtract two month
					currentDateTime = currentDateTime.plusMonths(2);
				}

				if(periode.equals("before") ) {
					// Subtract one month
					currentDateTime = currentDateTime.minusMonths(1);
				}
				else if(periode.equals("after")) {
					// add one month
					currentDateTime = currentDateTime.plusMonths(1);
				}
				else {
					currentDateTime = currentDateTime.plusDays(1);
				}

				// Format the date and time to the desired format
				String date = currentDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.'00Z'"));

				// create API request body

				String body= "{\r\n"
						+ "    \"description\": \"" + chargeCode + "\",\r\n"
						+ "    \"oneShotCharge\": \"" + chargeCode + "\",\r\n"
						+ "    \"subscription\": \"" + subCode + "\",\r\n"
						+ "    \"operationDate\": \"" + date + "\",\r\n"
						+ "    \"quantity\": 1,\r\n"
						+ "    \"generateRTs\": true\r\n"
						+ "}";
				System.out.println("Request body : " + body);
				resp = given().header("Content-Type", "application/json").body(body).when()
						.post();

				j = resp.jsonPath();
				l = (String)j.get("status");

				System.out.println("Response status : " + l);
			}
			else {
				break;
			}
		}

		return l;
	}

	/***  create price Plan Matrix Version ***/
	public void create_pricePlanMatrixVersion(String baseURI, String login, String password, String code) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/catalog/pricePlan/createOrUpdate";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		// create API request body
		String body = "{\r\n"
				+ "    \"code\": \"PPM_" + code + "\", \r\n"
				+ "    \"eventCode\": \"" + code + "\" \r\n"
				+ "}";
		System.out.println("create_pricePlanMatrixVersion : " + body);
		given().header("Content-Type", "application/json").body(body).when().post();

	}

	/***  create price Version  ***/
	public void create_priceVersion(String baseURI, String login, String password, String PVName, String chargeName, String price) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/catalog/pricePlan/pricePlanMatrixVersion";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		// create API request body
		String body = "{\r\n"
				+ "    \"priceVersionType\": \"FIXED\",\r\n"
				+ "    \"pricePlanMatrixCode\": \"" + chargeName +"\",\r\n"
				+ "    \"label\": \"" + PVName + "\",\r\n"
				+ "    \"validity\": {},\r\n"
				+ "    \"isMatrix\": false,\r\n"
				+ "    \"price\": " + price + "\r\n"
				+ "}";
		System.out.println("create_priceVersion : " + body);
		given().header("Content-Type", "application/json").body(body).when().post();

	}

	/***  Publish price Version  ***/
	public void Publish_priceVersion(String baseURI, String login, String password, String chargeName, String version) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/catalog/pricePlan/" + chargeName + "/pricePlanMatrixVersions/" + version + "/status/PUBLISHED";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		// create API request body
		String body = "{\r\n"
				+ "    \"code\": \"" + chargeName +"\",\r\n"
				+ "    \"version\": " + version + "\r\n"
				+ "}";
		System.out.println("Publish_priceVersion : " + body);
		given().header("Content-Type", "application/json").body(body).when().put();

	}

	public void createBillingCycleByAPI (String baseURI, String login, String password, String code, String description, String type, String calendar) {
		String body = "{\r\n" ;
		body = body + "    \"code\": \"" + code + "\",\r\n";
		body = body + "    \"description\": \"" + description + "\",\r\n";
		body = body + "    \"type\": \"" + type + "\",\r\n";
		body = body + "    \"dueDateDelayEL\": \"30\",\r\n";
		body = body + "    \"calendar\": \"" + calendar + "\"\r\n";
		body = body + "} \r\n";
		RestAssured.baseURI = baseURI;
		System.out.println("_______________billing run_______________________");
		System.out.println(body);
		System.out.println("______________________________________");
		// Setting BasePath once
		RestAssured.basePath = "api/rest/billingCycle/createOrUpdate";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;
		given().header("Content-Type", "application/json").body(body).when().post();
	}

	/**** create Exceptional Run By API ****/
	public int createExceptionalRunByAPI(String baseURI, String login, String password, String processType,String invoiceDate,Boolean SkipInvoiceValidation,
			Boolean recomputeDatesAtValidation,Boolean IncrementalInvoiceLines,
			String rejectAutoAction,String suspectAutoAction,Boolean preReportAutoOnCreate,Boolean preReportAutoOnInvoiceLinesJob, String dateAggregation, String invoiceType,List<List<String>> filterFields) throws InterruptedException, JsonMappingException, JsonProcessingException  {

		/////////////////////////////////////// By API ////////////////////////////////////////////
		String body = "{\r\n" ;
		if(processType != null) {body = body + "    \"billingRunTypeEnum\": \"" + processType + "\",\r\n";}
		if(invoiceDate != null) {body = body + "    \"invoiceDate\": \"" + invoiceDate + "\",\r\n";}
		if(SkipInvoiceValidation != null) {body = body + "    \"SkipInvoiceValidation\": " + SkipInvoiceValidation + ",\r\n";}
		if(recomputeDatesAtValidation != null) {body = body + "    \"recomputeDatesAtValidation\": " + recomputeDatesAtValidation + ",\r\n";}
		if(IncrementalInvoiceLines != null) {body = body + "    \"incrementalInvoiceLines\": " + IncrementalInvoiceLines + ",\r\n";}
		if(rejectAutoAction != null) {body = body + "    \"rejectAutoAction\": \"" + rejectAutoAction + "\",\r\n";}
		if(suspectAutoAction != null) {body = body + "    \"suspectAutoAction\": \"" + suspectAutoAction + "\",\r\n";}
		if(preReportAutoOnCreate != null) {body = body + "    \"preReportAutoOnCreate\": " + preReportAutoOnCreate + ",\r\n";}
		if(preReportAutoOnInvoiceLinesJob != null) {body = body + "    \"preReportAutoOnInvoiceLinesJob\": " + preReportAutoOnInvoiceLinesJob + ",\r\n";}
		if(dateAggregation != null) {body = body + "    \"dateAggregation\": \"" + dateAggregation + "\",\r\n";}

		body = body + "    \"filters\": {\r\n";
		body = body + "        \"$filter1\": { \r\n";
		body = body + "            \"$operator\": \"AND\", \r\n";

		for (List<String> field : filterFields) {

			if(field.get(0).equals("billingAcountCode") && field.get(1).equals("equal to")) {

				body = body + "            \"billingAccount.code\": \"" + field.get(2) + "\",";
			}
			if(field.get(0).equals("usageDate") && field.get(1).equals("less than")) {
				body = body + "            \"toRange usageDate\": \"" + field.get(2) + "\",";
			}
		}

		if (body.endsWith(",")) {
			// Remove the last character (comma) 
			body = body.substring(0, body.length() - 1);
		}

		body = body + "        } \r\n";
		body = body + "    } \r\n";
		body = body + "}";

		System.out.println("__________Create an exceptional run _________________");
		System.out.println(body);
		System.out.println("_____________________________________________________");

		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/v2/billing/invoicing/exceptionalBillingRun";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;
		Response resp;
		//		JsonPath j;

		resp = given().header("Content-Type", "application/json").body(body).when()
				.post();

		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode jsonNode = objectMapper.readTree(resp.getBody().asString());
		int id = jsonNode.get("id").asInt();

		return id;
	}

	/**** create Cycle Run By API ****/
	public void createCycleRunByAPI (String baseURI, String login, String password, String processType, String BC_Code) {
		String body = "{\r\n" ;
		body = body + "    \"billingCycleCode\": \"" + BC_Code + "\",\r\n";
		body = body + "    \"billingRunTypeEnum\": \"" + processType + "\"\r\n";
		body = body + "} \r\n";
		RestAssured.baseURI = baseURI;
		System.out.println("________________CYCLE RUN______________________");
		System.out.println(body);
		System.out.println("_______________________________________________");
		// Setting BasePath once
		RestAssured.basePath = "api/rest/billing/invoicing/createOrUpdateBillingRun";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;
		given().header("Content-Type", "application/json").body(body).when().post();
		waitPageLoaded();
	}

	public String createBasicInvoice(String baseURI, String login, String password,String invoiceType, String customerCode, String amountWithTax, String seller ) throws JsonMappingException, JsonProcessingException {
		
		String body = "{\r\n" ;
		body = body + "    \"invoiceTypeCode\": \"" + invoiceType + "\",\r\n";
		body = body + "    \"billingAccountCode\": \"" + customerCode + "\",\r\n";
		body = body + "    \"amountWithTax\": \"" + amountWithTax + "\",\r\n";
		body = body + "    \"seller\": {\r\n";
		body = body + "    \"code\": \"" + seller + "\"\r\n";
		body = body + "					} \r\n";
		body = body + "} \r\n";
		System.out.println("________________create Basic Invoice______________________");
		System.out.println(body);
		System.out.println("__________________________________________________________");
		
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/v2/billing/invoices/basicInvoices";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;
		Response resp;
		//		JsonPath j;

		resp = given().header("Content-Type", "application/json").body(body).when()
				.post();

		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode jsonNode = objectMapper.readTree(resp.getBody().asString());
		String id = jsonNode.get("id").asText();

		return id;
	}
	
	public void validateInvoice(String baseURI, String login, String password,String invoiceId, String refreshExchangeRate, String generateAO, String skipValidation) {
		String body = "{\r\n" ;
		body = body + "    \"invoiceId\": \"" + invoiceId + "\",\r\n";
		body = body + "    \"refreshExchangeRate\": " + refreshExchangeRate + ",\r\n";
		body = body + "    \"generateAO\": " + generateAO + ",\r\n";
		body = body + "    \"skipValidation\": " + skipValidation + "\r\n";
		body = body + "} \r\n";
		System.out.println("________________Validate Invoice______________________");
		System.out.println(body);
		System.out.println("__________________________________________________________");
		
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/invoice/validate";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		given().header("Content-Type", "application/json").body(body).when().put();
		waitPageLoaded();
	}
	
	public void addFunctionalCurrencyByAPI(String baseURI, String login, String password,String code) {
		String body = "{\r\n" ;
		body = body + "    \"code\": \"" + code + "\"\r\n";
		body = body + "} \r\n";
		System.out.println("________________addFunctionalCurrency______________________");
		System.out.println(body);
		System.out.println("__________________________________________________________");
		
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/currency/addFunctionalCurrency";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		given().header("Content-Type", "application/json").body(body).when().post();
		waitPageLoaded();
	}
	
	public void createSecurityDepositByAPI(String baseURI, String login, String password, String billingAccount, String customerAccount, String amount, String template, String seller, String code) {
		String body = "{\r\n"
				+ "    \"amount\": " + amount + ",\r\n"
				+ "    \"billingAccount\": {\"code\": \"" + billingAccount + "\"},\r\n"
				+ "    \"customerAccount\": {\"code\": \"" + customerAccount + "\"},\r\n"
				+ "    \"template\": { \"id\": " + template + "},\r\n"
				+ "    \"seller\": {\"code\": \"" + seller + "\" },\r\n"
				+ "    \"code\": \"" + code + "\"\r\n"
				+ "}" ;
		System.out.println("________________createSecurityDeposit______________________");
		System.out.println(body);
		System.out.println("__________________________________________________________");
		
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/v2/securityDeposit";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		given().header("Content-Type", "application/json").body(body).when().post();
		waitPageLoaded();
	}
}
