package com.opencellsoft.tests.jobs;

import com.opencellsoft.base.TestBase;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;
import static io.restassured.RestAssured.given;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class JobsTests extends TestBase {

	/***  Run Rated Transaction Job (RT_Job)  ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 1)
	public void runRTJob(String baseURI, String login, String password) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;
		// Setting BasePath once
		RestAssured.basePath = "api/rest/job/execute";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;
		try {
			given().header("Content-Type", "application/json").body("{\n" + "    \"code\": \"RT_Job\"\n" + "}").when()
			.post().then().log().all();
			Thread.sleep(2000);
			given().header("Content-Type", "application/json").body("{\n" + "    \"code\": \"RT_Job\"\n" + "}").when()
			.post().then().log().all().assertThat().statusCode(200);
			
		}catch (Exception e) {
			Thread.sleep(1000);
			given().header("Content-Type", "application/json").body("{\n" + "    \"code\": \"RT_Job\"\n" + "}").when()
			.post().then().log().all().assertThat().statusCode(200);
			
		}
		Thread.sleep(3000);
	}

	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 2)
	public void runAOJob(String baseURI, String login, String password) {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;
		// Setting BasePath once
		RestAssured.basePath = "api/rest/job/execute";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;
		given().header("Content-Type", "application/json").body("{\n" + "    \"code\": \"AO_Job\"\n" + "}").when()
		.post().then().log().all().assertThat().statusCode(200);
	}

	/***  Run Usage Job (U_Job)  ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 4)
	public void runUJob(String baseURI, String login, String password) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;
		// Setting BasePath once
		RestAssured.basePath = "api/rest/job/execute";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		given().header("Content-Type", "application/json").body("{\n" + "    \"code\": \"U_Job\"\n" + "}").when()
		.post().then().log().all().assertThat().statusCode(200);
		Thread.sleep(3000);

		given().header("Content-Type", "application/json").body("{\n" + "    \"code\": \"U_Job\"\n" + "}").when()
		.post().then().log().all().assertThat().statusCode(200);
		Thread.sleep(3000);
	}

	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 2)
	public void executeRTJob(String baseURI, String login, String password) throws InterruptedException {
		RestAssured.baseURI = baseURI;
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;
//		JSONObject request = new JSONObject();
//		request.put("code", "RT_Job");
//		given().header("Content-type", "application/json").contentType(ContentType.JSON).body(request.toJSONString())
//		.when().post(baseURI + "api/rest/job/execute").then().statusCode(200).log().all();
		given().header("Content-Type", "application/json").body("{\n" + "    \"code\": \"RT_Job\"\n" + "}").when()
		.post().then().log().all().assertThat().statusCode(200);
		Thread.sleep(3000);
	}

	/***  Run Rerate Job (Rerate_Job)  ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 10)
	public void runRerateJob(String baseURI, String login, String password) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;
		// Setting BasePath once
		RestAssured.basePath = "api/rest/job/execute";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		try {
			given().header("Content-Type", "application/json").body("{\n" + "    \"code\": \"Rerate_Job\"\n" + "}").when()
			.post().then().log().all().assertThat().statusCode(200);
			Thread.sleep(2000);
			given().header("Content-Type", "application/json").body("{\n" + "    \"code\": \"Rerate_Job\"\n" + "}").when()
			.post().then().log().all().assertThat().statusCode(200);
		}
		catch (Exception e) {
			given().header("Content-Type", "application/json").body("{\n" + "    \"code\": \"Rerate_Job\"\n" + "}").when()
			.post().then().log().all().assertThat().statusCode(200);
		}
		Thread.sleep(3000);
	}

	/***  Run Accounting Schemes Job  ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test
	public void runAccountingSchemesJob(String baseURI, String login, String password) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;
		// Setting BasePath once
		RestAssured.basePath = "api/rest/job/execute";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;
		try {
			given().header("Content-Type", "application/json").body("{\n" + "    \"code\": \"AccountingSchemesJob\"\n" + "}").when()
			.post().then().log().all();
			Thread.sleep(3000);
		}catch (Exception e) {
			given().header("Content-Type", "application/json").body("{\n" + "    \"code\": \"AccountingSchemesJob\"\n" + "}").when()
			.post().then().log().all();
		}
		
	}	

	/***  insert Charge Cdr  ***/
	public int insertChargeCdr(String baseURI, String login, String password, String AP, String param1, String param2, String param3) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/v2/mediation/cdrs/chargeCdrList";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		// Send request
		Calendar calendar;
		Response resp;
		JsonPath j;
		int l=0;

		for (int i=0; i<5; i++){
			if(l != 1) {
				Thread.sleep(1000);
				// get date format "yyyy-MM-dd'T'HH:mm:ss.'00Z'"
				calendar = Calendar.getInstance();
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.'00Z'", Locale.FRANCE);
				String date = format.format(calendar.getTime());

				// create API request body
				String body = "{\r\n"
						+ "    \"cdrs\": [\r\n"
						+ "    \"" + date + ";4;" + AP + ";"+ param1 + ";"+ param2 + ";" + param3 + ";param4;param5;param6;param7;param8;param9;" + date + ";" + date + ";" + date + ";" + date + ";" + date + ";0.1;0.2;0.3;0.4;0.5;extraParam\"\r\n"
						+ "    ],\r\n"
						+ "    \"isRateTriggeredEdr\": true\r\n"
						+ "}";
				System.out.println("Request body : " + body);
				resp = given().header("Content-Type", "application/json").body(body).when()
						.post();

				j = resp.jsonPath();
				l = (Integer)j.get("statistics.success");
				System.out.println("statistics.success : " + l);
			}
			else {
				break;
			}
		}

		return l;
	}

}
