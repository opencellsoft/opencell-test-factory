package com.opencellsoft.tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.customers.CustomerPage;
import com.opencellsoft.utility.Constant;

public class PaymentsTests extends TestBase {
	@Parameters({ "partiallyInvoiceAmount","invoiceAmount" })
	@Test(priority = 1)
	public void partiallyPayInvoice(String partiallyInvoiceAmount,String invoiceAmount) throws InterruptedException {
		CustomersTests customersTests = PageFactory.initElements(driver, CustomersTests.class);
		customersTests.searchCustomer(Constant.customerCode);
		Thread.sleep(2000);
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
		Thread.sleep(2000);
		customerPage.clickOnBalanceTab();
		Thread.sleep(2000);
		customerPage.selectInvoice();
		Thread.sleep(2000);
		customerPage.clickOnPayMyInvoiceButton();
		customerPage.setAmountToPay(partiallyInvoiceAmount);
		customerPage.setCheckReference(Constant.checkReference);
		customerPage.clickOnConfirmButton();
		Assert.assertEquals(customerPage.getTextMessage(), "Action executed successfuly");
		
//		Assert.assertEquals(customerPage.getInvoiceStatusSpanValue(), "POSTED");
//		Assert.assertEquals(customerPage.getMachingStatusSpanValue(), "P");

		
	}
	@Parameters({ "invoiceAmount" })
	@Test(priority = 3)
	public void payInvoice(String invoiceAmount) throws InterruptedException {
//		CustomersTests customersTests = PageFactory.initElements(driver, CustomersTests.class);
//		customersTests.searchCustomer(Constant.customerCode);
		Thread.sleep(2000);
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
//		customerPage.clickOnBalanceTab();
		Thread.sleep(2000);
		customerPage.selectInvoiceToPay();
		Thread.sleep(2000);
		customerPage.clickOnPayMyInvoiceButton();
		customerPage.setAmountToPay(invoiceAmount);
		customerPage.setCheckReference(Constant.checkReference);
		customerPage.clickOnConfirmButton();
		Assert.assertEquals(customerPage.getTextMessage(), "Action executed successfuly");
//		Assert.assertEquals(customerPage.getInvoiceStatusSpanValue(), "POSTED");
//		Assert.assertEquals(customerPage.getMachingStatusSpanValue(), "L");

	}
	@Test(priority = 2)
	public void refundInvoice() throws InterruptedException {
//		CustomersTests customersTests = PageFactory.initElements(driver, CustomersTests.class);
//		customersTests.searchCustomer(Constant.customerCode);
		CustomerPage customerPage = PageFactory.initElements(driver, CustomerPage.class);
//		customerPage.clickOnBalanceTab();
		Thread.sleep(2000);
		customerPage.selectPaymentToRefund();
		Thread.sleep(2000);
		customerPage.clickOnRefundButton();
//		customerPage.setIBAN(Constant.IBAN);
		customerPage.clickOnConfirmButton();
		Assert.assertEquals(customerPage.getTextMessage(), "Action executed successfuly");
	}
}
