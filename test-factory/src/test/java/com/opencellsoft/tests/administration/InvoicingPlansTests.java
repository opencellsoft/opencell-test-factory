package com.opencellsoft.tests.administration;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.administration.billing.invoicingplans.InvoicingPlanLinesPage;
import com.opencellsoft.pages.administration.billing.invoicingplans.ListOfInvoicingPlansPage;
import com.opencellsoft.pages.administration.billing.invoicingplans.NewInvoicingPlanPage;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.utility.Constant;

public class InvoicingPlansTests extends TestBase {

	@Test(priority = 34, groups = { "smoke tests" })
	public void createInvoicingPlan() throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		//driver.navigate().refresh();
		waitPageLoaded();
		homePage.clickOnCustomerCareButton();
		homePage.clickOnInvoicingPlansButton();
		ListOfInvoicingPlansPage listOfInvoicingPlansPage = PageFactory.initElements(driver,
				ListOfInvoicingPlansPage.class);
		waitPageLoaded();
		listOfInvoicingPlansPage.clickOnCreateInvoicePlanButton();
		NewInvoicingPlanPage newInvoicingPlanPage = PageFactory.initElements(driver, NewInvoicingPlanPage.class);
		waitPageLoaded();
		newInvoicingPlanPage.setInvoicingName(Constant.invoicingPlanName);
		newInvoicingPlanPage.setInvoicingDescription(Constant.invoicingPlanDescription);
		newInvoicingPlanPage.clickSaveInvoicingPlanButton();
		Assert.assertEquals(newInvoicingPlanPage.getTextMessage(), "Element created");
		Assert.assertEquals(newInvoicingPlanPage.getInvoicingPlanCodeInputText(), Constant.invoicingPlanName);
		Assert.assertEquals(newInvoicingPlanPage.getInvoicingPlanDescriptionInputText(),
				Constant.invoicingPlanDescription);
	}

	@Parameters({ "advancementRate", "downPayementRate", "advancementRate2", "downPayementRate2", "advancementRate3",
	"downPayementRate3" })
	@Test(priority = 35, groups = { "smoke tests" })
	public void createInvoicingPlanLines(String advancementRate, String downPayementRate, String advancementRate2,
			String downPayementRate2, String advancementRate3, String downPayementRate3) throws InterruptedException {
		InvoicingPlanLinesPage invoicingPlanLinesPage = PageFactory.initElements(driver, InvoicingPlanLinesPage.class);
		Thread.sleep(2000);
		invoicingPlanLinesPage.clickOnCreateInvoicePlanLinesButton();
		invoicingPlanLinesPage.setCodeInvoicePlanLines(Constant.invoicingPlanLine1Code);
		invoicingPlanLinesPage.setDescriptionInvoicePlanLines(Constant.invoicingPlanLine1Code);
		invoicingPlanLinesPage.setInvoicingAdvancementRate(advancementRate);
		invoicingPlanLinesPage.setInvoicingDownPayementRate(downPayementRate);
		invoicingPlanLinesPage.clickOnSaveInvoicingPlanLinesButton();
		Assert.assertEquals(invoicingPlanLinesPage.getTextMessage(), "Element created");
		/*
		 * Assert.assertEquals(invoicingPlanLinesPage.getInvoicingPlanLinesCodeInputText
		 * (), Constant.invoicingPlanLine1Code);
		 * Assert.assertEquals(invoicingPlanLinesPage.
		 * getInvoicingPlanLinesDescriptionInputText(),
		 * Constant.invoicingPlanLine1Code);
		 */
		invoicingPlanLinesPage.clickOnGoBackInvoicingPlanLinesButton();
		Thread.sleep(1000);
		invoicingPlanLinesPage.clickOnCreateInvoicePlanLinesButton();
		invoicingPlanLinesPage.setCodeInvoicePlanLines(Constant.invoicingPlanLine2Code);
		invoicingPlanLinesPage.setDescriptionInvoicePlanLines(Constant.invoicingPlanLine2Code);
		invoicingPlanLinesPage.setInvoicingAdvancementRate(advancementRate2);
		invoicingPlanLinesPage.setInvoicingDownPayementRate(downPayementRate2);
		invoicingPlanLinesPage.clickOnSaveInvoicingPlanLinesButton();
		Assert.assertEquals(invoicingPlanLinesPage.getTextMessage(), "Element created");
		/*
		 * Assert.assertEquals(invoicingPlanLinesPage.getInvoicingPlanLinesCodeInputText
		 * (), Constant.invoicingPlanLine2Code);
		 * Assert.assertEquals(invoicingPlanLinesPage.
		 * getInvoicingPlanLinesDescriptionInputText(),
		 * Constant.invoicingPlanLine2Code);
		 */
		invoicingPlanLinesPage.clickOnGoBackInvoicingPlanLinesButton();
		waitPageLoaded();
		invoicingPlanLinesPage.clickOnCreateInvoicePlanLinesButton();
		invoicingPlanLinesPage.setCodeInvoicePlanLines(Constant.invoicingPlanLine3Code);
		invoicingPlanLinesPage.setDescriptionInvoicePlanLines(Constant.invoicingPlanLine3Code);
		invoicingPlanLinesPage.setInvoicingAdvancementRate(advancementRate3);
		invoicingPlanLinesPage.setInvoicingDownPayementRate(downPayementRate3);
		invoicingPlanLinesPage.clickOnSaveInvoicingPlanLinesButton();
		Assert.assertEquals(invoicingPlanLinesPage.getTextMessage(), "Element created");
		/*
		 * Assert.assertEquals(invoicingPlanLinesPage.getInvoicingPlanLinesCodeInputText
		 * (), Constant.invoicingPlanLine3Code);
		 * Assert.assertEquals(invoicingPlanLinesPage.
		 * getInvoicingPlanLinesDescriptionInputText(),
		 * Constant.invoicingPlanLine3Code);
		 */
		invoicingPlanLinesPage.clickOnGoBackInvoicingPlanLinesButton();
		waitPageLoaded();
		NewInvoicingPlanPage newInvoicingPlanPage = PageFactory.initElements(driver, NewInvoicingPlanPage.class);
		newInvoicingPlanPage.clickSaveInvoicingPlanButton();
		waitPageLoaded();
		Assert.assertEquals(newInvoicingPlanPage.getTextMessage(), "Element updated");
	}
}
