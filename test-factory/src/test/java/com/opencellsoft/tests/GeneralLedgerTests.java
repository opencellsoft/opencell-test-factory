package com.opencellsoft.tests;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.finance.reports.GeneralLedgerPage;

public class GeneralLedgerTests extends TestBase {
	GeneralLedgerPage generalLedgerPage;
	
	public void activateFilterBy(String value) {
		generalLedgerPage = new GeneralLedgerPage();
		generalLedgerPage.clickOnFilterButton();
		generalLedgerPage.activateFilterBy(value);
	}
	
	public void filterByAccountingName(String value) {
		generalLedgerPage = new GeneralLedgerPage();
		generalLedgerPage.setFilterByAccountingNameInput(value);
	}
	
	public void filterByFunctionalCurrency(Boolean value) {
		generalLedgerPage = new GeneralLedgerPage();
		generalLedgerPage.setFunctionalCurrency(value);
	}
	
	public void export(String value) {
		generalLedgerPage = new GeneralLedgerPage();
		generalLedgerPage.clickOnDownloadSelectionButton();
		generalLedgerPage.clickOnExportFormat(value);
	}
	
	public Boolean alertMsg() {
		return generalLedgerPage.isAlertMsgDisplayed();
	}
	
}
