package com.opencellsoft.tests;

import java.util.Locale;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Ignore;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;
import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.discountplans.CreateDiscountPlanPage;
import com.opencellsoft.pages.discountplans.DiscountLinePage;
import com.opencellsoft.pages.discountplans.DiscountPlanPage;
import com.opencellsoft.pages.discountplans.DiscountPlansListPage;

public class DiscountPlanTests extends TestBase {
	
	FakeValuesService fakeValuesService = new FakeValuesService(new Locale("en-GB"), new RandomService());
	Faker faker = new Faker();
	String DiscountPlanCode = fakeValuesService.regexify("Discount Plan[1-9]{5}");
	String DiscountPlanLabel = faker.commerce().productName();
	CreateDiscountPlanPage createDiscountPlanPage;

	@Test(priority = 1)
	public void goTODiscountPlanCreationPage() {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCatalogButton();
		//homePage.clickDiscountPlanBtn();

		DiscountPlansListPage discountPlansListPage = PageFactory.initElements(driver, DiscountPlansListPage.class);
		discountPlansListPage.clickCreateNewDiscountPlan();
		createDiscountPlanPage = PageFactory.initElements(driver, CreateDiscountPlanPage.class);
	}
	@Parameters({"invoice"})
	@Test(priority = 2)
	public void createInvoiceTypeDiscountPlan(String invoice) throws Exception {
		createDiscountPlanPage.createDiscountPlan(DiscountPlanCode, DiscountPlanLabel, invoice);
		createDiscountPlanPage.clickSaveDiscountPlanBtn();
	}

	@Ignore
	@Parameters({"invoiceLine"})
	@Test(priority = 2)
	public void createInvoiceLineTypeDiscountPlan(String invoiceLine) throws Exception {
		createDiscountPlanPage.createDiscountPlan(DiscountPlanCode, DiscountPlanLabel, invoiceLine);
		createDiscountPlanPage.clickSaveDiscountPlanBtn();
	}

	@Ignore
	@Parameters({"offer"})
	@Test(priority = 2)
	public void createOfferTypeDiscountPlan(String offer) throws Exception {

		createDiscountPlanPage.createDiscountPlan(DiscountPlanCode, DiscountPlanLabel, offer);
		createDiscountPlanPage.clickSaveDiscountPlanBtn();
	}

	@Ignore
	@Parameters({"product"})
	@Test(priority = 2)
	public void createProductTypeDiscountPlan(String product) throws Exception {
		createDiscountPlanPage.createDiscountPlan(DiscountPlanCode, DiscountPlanLabel, product);
		createDiscountPlanPage.clickSaveDiscountPlanBtn();
	}

	@Ignore
	@Parameters({"promoCode"})
	@Test(priority = 2)
	public void createPromoCodeTypeDiscountPlan(String promoCode) throws Exception {
		createDiscountPlanPage.createDiscountPlan(DiscountPlanCode, DiscountPlanLabel, promoCode);
		createDiscountPlanPage.clickSaveDiscountPlanBtn();
	}

	@Ignore
	@Parameters({"quote"})
	@Test(priority = 2)
	public void createQuoteTypeDiscountPlan(String quote) throws Exception {
		createDiscountPlanPage.createDiscountPlan(DiscountPlanCode, DiscountPlanLabel, quote);
		createDiscountPlanPage.clickSaveDiscountPlanBtn();
	}

	@Test(priority = 3)
	public void editDiscountPlan() throws Exception {

		DiscountPlanPage discountPlanPage = PageFactory.initElements(driver, DiscountPlanPage.class);
		discountPlanPage.editDiscountPlan(fakeValuesService.regexify("Updated Invoice Line Discount Plan[1-9]{5}"));

	}

	@Parameters({"applicationFilter"})
	public void CreateDiscountPlanWithApplicationFilterEL(String applicationFilter) {

		DiscountPlanPage discountPlanPage = PageFactory.initElements(driver, DiscountPlanPage.class);
		discountPlanPage.setApplicationFilter(applicationFilter);

	}

	@Test(priority = 4)
	public void AddDiscountLinesTodiscountPlan() {
		DiscountPlanPage discountPlanPage = PageFactory.initElements(driver, DiscountPlanPage.class);
		discountPlanPage.clickCreate();
		for (int i = 0; i <= 7; i++) {
			DiscountLinePage discountLinePage = PageFactory.initElements(driver, DiscountLinePage.class);
			String discountLineCode = fakeValuesService.regexify("DISCOUNT_LINE[1-9]{5}");
			discountLinePage.createDiscountLine(discountLineCode);
			discountLinePage.saveDiscountLine();
		}
	}

	@Test(priority = 5)
	public void AddPercentageDiscountLines() {

		DiscountPlanPage discountPlanPage = PageFactory.initElements(driver, DiscountPlanPage.class);
		discountPlanPage.clickCreate();
		DiscountLinePage discountLinePage = PageFactory.initElements(driver, DiscountLinePage.class);
		String discountLineCode = fakeValuesService.regexify("DISCOUNT_LINE[1-9]{5}");
		discountLinePage.createDiscountLine(discountLineCode);
		discountLinePage.setPercentage();
		discountLinePage.saveDiscountLine();
	}

	@Test(priority = 6)
	public void AddFixedAmountDiscountLines() {

		DiscountPlanPage discountPlanPage = PageFactory.initElements(driver, DiscountPlanPage.class);
		discountPlanPage.clickCreate();

		DiscountLinePage discountLinePage = PageFactory.initElements(driver, DiscountLinePage.class);
		String discountLineCode = fakeValuesService.regexify("DISCOUNT_LINE[1-9]{5}");
		discountLinePage.createDiscountLine(discountLineCode);
		discountLinePage.setFlatAmount();
		discountLinePage.saveDiscountLine();
	}
}
