package com.opencellsoft.tests;


import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import org.testng.annotations.Test;
import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.commons.LoginPage;


public class LoadTests extends TestBase {
	

	@Test(invocationCount = 3, threadPoolSize = 5)
	public void loadTestThisWebsite() {
		System.out.printf("%n[START] Thread Id : %s is started!", 
                Thread.currentThread().getId());
		System.out.println("Page Title is " + driver.getTitle());
		Assert.assertEquals("Opencell | Portal", driver.getTitle());


	}

}
