package com.opencellsoft.tests;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.operation.querytool.QueryBuilderPage;

public class QueryBuilderTests extends TestBase {

	QueryBuilderPage queryBuilderPage;
	
	public void createQuery(String queryName,String scopeEntity,String selected_field1,String selected_field2,String selected_field3, String filter_field, String filter_operator, String filter_value){
		queryBuilderPage = new QueryBuilderPage();
		queryBuilderPage.selectScopeEntity(scopeEntity);
		if(selected_field1 != null) { queryBuilderPage.addSelectedField(selected_field1);}
		if(selected_field2 != null) { queryBuilderPage.addSelectedField(selected_field2);}
		if(selected_field3 != null) { queryBuilderPage.addSelectedField(selected_field3);}
		queryBuilderPage.addFilterField(filter_field,filter_operator, filter_value);
		queryBuilderPage.clickOnUpdateFilterButton();
		queryBuilderPage.clickOnSaveButton();
		queryBuilderPage.setQueryNameInput(queryName);
		queryBuilderPage.clickOnConfirmButton();
		
	}
	
	public void removeSelectedField(String queryName, String position){
		queryBuilderPage = new QueryBuilderPage();
		queryBuilderPage.clickOnRemoveSelectedField(position);
		queryBuilderPage.clickOnSaveButton();
		queryBuilderPage.setQueryNameInput(queryName);
		queryBuilderPage.clickOnConfirmButton();
	}
	
	public void addSelectedField(String queryName, String selected_field) {
		queryBuilderPage = new QueryBuilderPage();
		queryBuilderPage.addSelectedField(selected_field);
		queryBuilderPage.clickOnSaveButton();
		queryBuilderPage.setQueryNameInput(queryName);
		queryBuilderPage.clickOnConfirmButton();
	}
	
	public void executeQuery(){
		queryBuilderPage = new QueryBuilderPage();
		queryBuilderPage.clickOnExecuteButton();
	}
	
	public void executeQueryOnBackGround(){
		queryBuilderPage = new QueryBuilderPage();
		queryBuilderPage.clickOnExecuteOnBackGroundButton();
	}
	
	public void downloadQuery(String fileType){
		queryBuilderPage = new QueryBuilderPage();
		queryBuilderPage.clickOnDownloadButton();
		queryBuilderPage.selectFileType(fileType);
	}
	
	public void scheduleQuery(String email, String fileFormat, String frequency){
		queryBuilderPage = new QueryBuilderPage();
		queryBuilderPage.clickOnScheduleButton();
		queryBuilderPage.setEmailInput(email);
		queryBuilderPage.checkFileFormat(fileFormat);
		queryBuilderPage.checkfrequency(frequency);
		queryBuilderPage.clickOnScheduleButton();
	}
	
	public void deleteQuery(){
		queryBuilderPage = new QueryBuilderPage();
		queryBuilderPage.clickOnDeleteButton();
		queryBuilderPage.clickOnConfirmButton();
	}
	
	public void searchQuery(String value) {
		queryBuilderPage = new QueryBuilderPage();
		queryBuilderPage.selectQueryName(value);
	}
}
