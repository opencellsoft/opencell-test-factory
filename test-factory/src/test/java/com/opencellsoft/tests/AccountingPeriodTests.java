package com.opencellsoft.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.finance.accountingperiod.AccountingEntriesPage;
import com.opencellsoft.pages.finance.accountingperiod.AccountingOperationsReportPage;
import com.opencellsoft.pages.finance.accountingperiod.AccountingPeriodManagerPage;


public class AccountingPeriodTests extends TestBase {
	AccountingPeriodManagerPage accountingPeriodManagerPage;
	AccountingOperationsReportPage accountingOperationsReportPage;
	AccountingEntriesPage accountingEntriesPage;
	
	public AccountingPeriodTests() {
		PageFactory.initElements(driver, this);
		accountingPeriodManagerPage = new AccountingPeriodManagerPage();
		accountingOperationsReportPage = new AccountingOperationsReportPage();
		accountingEntriesPage = new AccountingEntriesPage();
	}
	// return the number of lines found
	public int elementsFoundSize(String value) {
		accountingPeriodManagerPage = new AccountingPeriodManagerPage();
		return accountingPeriodManagerPage.elementsFoundSize(value);
	}

	// return the number of lines found
	public boolean checkValueInAccoutingPeriodList(String value) {
		accountingPeriodManagerPage = new AccountingPeriodManagerPage();
		return accountingPeriodManagerPage.checkValueInAccoutingPeriodList(value);
	}

	public void showDetailsOfAccountingPeriod(int i) {
		accountingPeriodManagerPage = new AccountingPeriodManagerPage();
		accountingPeriodManagerPage.clickOnAccountingPeriod(i);
	}

	public int subPeriodOpenSize() {
		accountingPeriodManagerPage = new AccountingPeriodManagerPage();
		return accountingPeriodManagerPage.subPeriodOpenSize();
	}

	public int subPeriodCloseSize() {
		accountingPeriodManagerPage = new AccountingPeriodManagerPage();
		return accountingPeriodManagerPage.subPeriodCloseSize();
	}

	public void closePeriod(int i) {
		accountingPeriodManagerPage = new AccountingPeriodManagerPage();
		accountingPeriodManagerPage.clickOnCloseFiscalYearButton(i);
		accountingPeriodManagerPage.clickOnConfirmButton();
		accountingPeriodManagerPage.clickOnRefreshButton();
	}

	public void reopenPeriod(int i) {
		accountingPeriodManagerPage = new AccountingPeriodManagerPage();
		accountingPeriodManagerPage.clickOnReopenFiscalYearButton(i);
		accountingPeriodManagerPage.clickOnConfirmButton();
		accountingPeriodManagerPage.clickOnRefreshButton();
	}

	public void closeSubperiodForRegularUser(int table, int row) {
		accountingPeriodManagerPage = new AccountingPeriodManagerPage();
		accountingPeriodManagerPage.clickOnCloseSubperiodForRegularUser(table,row);
		accountingPeriodManagerPage.clickOnYesButton();
		accountingPeriodManagerPage.clickOnRefreshButton();
	}


	public void closeSubperiodForFinanceUser(int table, int row) {
		accountingPeriodManagerPage = new AccountingPeriodManagerPage();
		accountingPeriodManagerPage.clickOnCloseSubperiodForFinanceUser(table,row);
		accountingPeriodManagerPage.clickOnYesButton();
		accountingPeriodManagerPage.clickOnRefreshButton();
	}

	public void openSubperiodForRegularUser(int table, int row) {
		accountingPeriodManagerPage = new AccountingPeriodManagerPage();
		accountingPeriodManagerPage.clickOnOpenSubperiodForRegularUser(table,row);
		accountingPeriodManagerPage.setReason("1");
		accountingPeriodManagerPage.clickOnValidateButton();
	}

	public void openSubperiodForFinanceUser(int table, int row) {
		accountingPeriodManagerPage = new AccountingPeriodManagerPage();
		accountingPeriodManagerPage.clickOnOpenSubperiodForFinanceUser(table,row);
		accountingPeriodManagerPage.setReason("1");
		accountingPeriodManagerPage.clickOnValidateButton();
		accountingPeriodManagerPage.clickOnRefreshButton();
	}

	public boolean isMessageDisplayed(String msg) {
		accountingPeriodManagerPage = new AccountingPeriodManagerPage();
		return accountingPeriodManagerPage.isMessageDisplayed(msg);
	}

	public void cancelClosingSubperiod() {
		accountingPeriodManagerPage = new AccountingPeriodManagerPage();
		accountingPeriodManagerPage.clickOnCancelButton();
	}

	public void addNewPeriod() {
		accountingPeriodManagerPage = new AccountingPeriodManagerPage();
		accountingPeriodManagerPage.clickOnGenerateAccountingPeriodButton();
		accountingPeriodManagerPage.clickOnConfirmButton();
		accountingPeriodManagerPage.clickOnRefreshButton();
	}
	public void goToReference(String value) {
		accountingOperationsReportPage = new AccountingOperationsReportPage();
		accountingOperationsReportPage.clickOnreference(value);
	}
	
	public void goToReferenceByAccountingEntries(String value) {
		accountingEntriesPage = new AccountingEntriesPage();
		accountingEntriesPage.clickOnreference(value);
	}
	
	public void desableCurrentMonth() {
		accountingOperationsReportPage = new AccountingOperationsReportPage();
		accountingOperationsReportPage.clickOnCurrentMonthInput();
	}

	public String getInvoiceNumber(int row) {
		accountingOperationsReportPage = new AccountingOperationsReportPage();
		return accountingOperationsReportPage.getReferenceNumberOf(row);
	}

	public boolean IsForcedPosting(int i) {
		accountingOperationsReportPage = new AccountingOperationsReportPage();
		return accountingOperationsReportPage.IsForcedPosting(i);
	}

	public void downlaodFinanceAccountingCSV() {
		accountingOperationsReportPage = new AccountingOperationsReportPage();
		accountingOperationsReportPage.selectAllOperations();
		accountingOperationsReportPage.clickOnActionButton();
		accountingOperationsReportPage.clickOndownlaodCSVButton();
		accountingOperationsReportPage.clickOnConfirmButton();
	}
	public void downlaodFinanceAccountingXLSX() {
		accountingOperationsReportPage = new AccountingOperationsReportPage();
		accountingOperationsReportPage.selectAllOperations();
		accountingOperationsReportPage.clickOnActionButton();
		accountingOperationsReportPage.clickOndownlaodXLSXButton();
		try {
			accountingOperationsReportPage.clickOnConfirmButton();
		}catch (Exception e) {		}
	}
	
	public void downlaodJournalEntryCSV() {
		accountingEntriesPage = new AccountingEntriesPage();
		accountingEntriesPage.clickOnActionButton();
		accountingEntriesPage.selectExportCurrentFiltre();
		accountingEntriesPage.clickOndownlaodCSVButton();
		try {
			accountingEntriesPage.clickOndownlaodCSVButton();
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void downlaodJournalEntryXLSX() {
		accountingEntriesPage = new AccountingEntriesPage();
		accountingEntriesPage.clickOnActionButton();
		accountingEntriesPage.selectExportCurrentFiltre();
		accountingEntriesPage.clickOndownlaodXLSXButton();
		try {
			accountingEntriesPage.clickOndownlaodXLSXButton();
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void downlaodAgedTrialBalanceCSV(String version) {
		accountingEntriesPage = new AccountingEntriesPage();
		accountingEntriesPage.clickOnActionButton();
		accountingEntriesPage.selectExportCurrentFiltre();
		accountingEntriesPage.clickOndownlaodCSVButton();
		accountingEntriesPage.hitEnter();
	}
	public void downlaodAgedTrialBalanceXLSX(String version) {
		accountingEntriesPage = new AccountingEntriesPage();
		accountingEntriesPage.clickOnActionButton();
		accountingEntriesPage.selectExportCurrentFiltre();
		accountingEntriesPage.clickOndownlaodXLSXButton();
//		accountingEntriesPage.clickOndownlaodXLSXButton();
		accountingEntriesPage.hitEnter();
	}
	
	public String getValueFromTable (int row , int column) {
		try {
			WebElement element = driver.findElement(By.xpath("(//table[contains(@class , 'MuiTable-root')]//tr)[" + row + "]//td[" + column + "]//span//span"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView(true);", element);
			return element.getText();
		}catch (Exception e) {
			WebElement element = driver.findElement(By.xpath("(//table[contains(@class , 'MuiTable-root')]//tr)[" + row + "]//td[" + column + "]//p"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView(true);", element);
			return element.getText();
		}
		
		
	}

	public String getTotalFromTable (int row , int column) {
		try {
			WebElement element = driver.findElement(By.xpath("(//table[@class = 'MuiTable-root']//tr)[" + row + "]//td[" + column + "]"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView(true);", element);
			return element.getText();
		}catch (Exception e) {
			WebElement element = driver.findElement(By.xpath("(//table[contains(@class , 'MuiTable-root')]//tr)[" + row + "]//td[" + column + "]"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView(true);", element);
			return element.getText();
		}
	}
	
	public String checkDefaultValueOfFiscalYear() {
		accountingEntriesPage = new AccountingEntriesPage();
		return accountingEntriesPage.getFiscalYearlabelText();
	}
	
	public void filterByStepDays(String i) {
		accountingEntriesPage = new AccountingEntriesPage();
		accountingEntriesPage.setStepInDays(i);
	}
	
	public void applyFilterByCustomDateInPast() {
		accountingEntriesPage = new AccountingEntriesPage();
		accountingEntriesPage.selectCustomDate();
		accountingEntriesPage.selectPastDate(1);
		
	}
	
	public void applyFilterByCustomDateInFutur() {
		accountingEntriesPage = new AccountingEntriesPage();
		accountingEntriesPage.selectCustomDate();
		accountingEntriesPage.selectFuturtDate(2);
	}
	
	public void sortByCredit() {
		try {
			clickOn(driver.findElement(By.xpath("//th//*[text() = 'Credit']")));
		}catch (Exception e) {
			clickOn(driver.findElement(By.xpath("//th//*[text() = 'Credit']")));
		}
		
	}
	
	public void sortByAccountingPeriodYear() {
		clickOn(driver.findElement(By.xpath("//span[@data-sort = 'accountingPeriodYear']")));
	}
	
	public void refreshPage() {
		accountingPeriodManagerPage = new AccountingPeriodManagerPage();
		accountingPeriodManagerPage.clickOnRefreshButton();
	}
	
	public void activateFilterBy(String value) {
		accountingOperationsReportPage = new AccountingOperationsReportPage();
		accountingOperationsReportPage.clickOnFilterButton();
		accountingOperationsReportPage.activateFilterBy(value);
	}
	public void filterByStatus(String value) {
		accountingOperationsReportPage = new AccountingOperationsReportPage();
		accountingOperationsReportPage.selectStatus(value);
	}
	
	public void launchAccountingSchemesJob() {
		accountingOperationsReportPage = new AccountingOperationsReportPage();
		accountingOperationsReportPage.clickOnLaunchAccountingSchemesJobButton();
		try {
			accountingOperationsReportPage.clickOnConfirmButton();
		}catch (Exception e) {
			accountingOperationsReportPage.clickOnLaunchAccountingSchemesJobButton();
			accountingOperationsReportPage.clickOnConfirmButton();
		}
		
	}
	
	public void goToJobsReportPage() {
		accountingOperationsReportPage = new AccountingOperationsReportPage();
		accountingOperationsReportPage.clickOnJobsReportButton();
	}
	
}
