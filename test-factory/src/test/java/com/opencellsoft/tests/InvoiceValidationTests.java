package com.opencellsoft.tests;

import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.operations.billingrules.InvoiceValidationListPage;
import com.opencellsoft.pages.operations.billingrules.InvoiceValidationPage;

public class InvoiceValidationTests extends TestBase {
	
	InvoiceValidationListPage invoiceValidationListPage;
	InvoiceValidationPage invoiceValidationPage;
	
	public void goToComInvoice() {
		//invoiceValidationListPage = new InvoiceValidationListPage();
		invoiceValidationListPage = PageFactory.initElements(driver, InvoiceValidationListPage.class);
		invoiceValidationListPage.goToComInvoice();
	}
	
	public void setInvoiceValidationRule1(String label, String fialMode, String amountValue,String version) {
		invoiceValidationPage = new InvoiceValidationPage();
		invoiceValidationPage.setLabel(label);
		if(version.equals("14.1.X")) {
			invoiceValidationPage.setType("script");
		}
		invoiceValidationPage.setFailMode(fialMode, version);
		invoiceValidationPage.clickOnActionButton();
		invoiceValidationPage.clickOnInvoiceValidationList();
		if(version.equals("17.X")) {
			invoiceValidationPage.clickOnInvoiceValidationList();
		}
		invoiceValidationPage.selectCompareInvoiceAmountScript();
		try {
			invoiceValidationPage.setValue(amountValue);
		}catch (Exception e) {
			invoiceValidationPage.clickOnCancelButton2();
			invoiceValidationPage.setValue(amountValue);
		}
		
		invoiceValidationPage.clickOnConfirm();
		invoiceValidationPage.clickOnSave();
	}
	
	public void updateInvoiceValidationRule1(String label, String fialMode, String amountValue,String version) {
		invoiceValidationPage = new InvoiceValidationPage();

		if(fialMode != null) {invoiceValidationPage.setFailMode(fialMode, version);}
		invoiceValidationPage.clickOnActionButton();
		invoiceValidationPage.setValue(amountValue);
		invoiceValidationPage.clickOnConfirm();
		invoiceValidationPage.clickOnSave();
	}
	
}
