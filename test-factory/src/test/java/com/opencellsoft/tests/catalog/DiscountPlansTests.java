package com.opencellsoft.tests.catalog;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.catalog.discountplans.DiscountLinePage;
import com.opencellsoft.pages.catalog.discountplans.DiscountPlanPage;
import com.opencellsoft.pages.catalog.discountplans.DiscountPlansListPage;
import com.opencellsoft.pages.catalog.discountplans.NewDiscountLinePage;
import com.opencellsoft.pages.catalog.discountplans.NewDiscountPlanPage;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.invoices.InvoicePage;
import com.opencellsoft.utility.Constant;

public class DiscountPlansTests extends TestBase {

	@Parameters({ "discountPlanType" })
	@Test(priority = 1)
	public void createDiscountPlan(String discountPlanType) {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCatalogButton();
		homePage.clickOnDiscountPlanButton();
		DiscountPlansListPage listOfDiscountPlansPage = PageFactory.initElements(driver,
				DiscountPlansListPage.class);
		listOfDiscountPlansPage.clickOnCreateDiscountPlanButton();
		NewDiscountPlanPage newDiscountPlanPage = PageFactory.initElements(driver, NewDiscountPlanPage.class);
		newDiscountPlanPage.setDiscountPlanCode(Constant.discountPlanCode);
		newDiscountPlanPage.setDiscountPlanLabel(Constant.discountPlanLabel);
		newDiscountPlanPage.setStartDate();
		newDiscountPlanPage.setDiscountPlanType(discountPlanType);
		newDiscountPlanPage.clickOnSaveDiscountPlanButton();
	}
	
	@Parameters({"discountAmount"})
	@Test(priority = 2)
	public void createFlatAmountDiscountLine(String discountAmount) throws InterruptedException {
		DiscountPlanPage discountPlanPage = PageFactory.initElements(driver, DiscountPlanPage.class);
		discountPlanPage.clickOnCreateDiscountLineButton();
		NewDiscountLinePage newDiscountLinePage = PageFactory.initElements(driver, NewDiscountLinePage.class);
		newDiscountLinePage.setDiscountLineCode(Constant.discountLineCode);
		newDiscountLinePage.clickOnFlatAmountOption();
		newDiscountLinePage.setFlatAmountValue(discountAmount);
		newDiscountLinePage.clickOnSaveDiscountLineButton();
		DiscountLinePage discountLinePage = PageFactory.initElements(driver, DiscountLinePage.class);
		discountLinePage.clickOnGoBackButton();
		discountPlanPage.clickOnSaveButton();	
	}
	@Parameters({"discountPercentage"})
	@Test(priority = 3)
	public void addPercentageDiscountLine(String discountPercentage) {

		DiscountPlanPage discountPlanPage = PageFactory.initElements(driver, DiscountPlanPage.class);
		discountPlanPage.clickOnCreateDiscountLineButton();
		NewDiscountLinePage newDiscountLinePage = PageFactory.initElements(driver, NewDiscountLinePage.class);
		newDiscountLinePage.setDiscountLineCode(Constant.discountLineCode);
		newDiscountLinePage.clickOnPercentageOption();
		newDiscountLinePage.setPercentage(discountPercentage);
		newDiscountLinePage.clickOnSaveDiscountLineButton();
		DiscountLinePage discountLinePage = PageFactory.initElements(driver, DiscountLinePage.class);
		discountLinePage.clickOnGoBackButton();
		discountPlanPage.clickOnSaveButton();		
	}
	@Test(priority=4)
	public void activateDiscountPlan() {
		DiscountPlanPage discountPlanPage = PageFactory.initElements(driver, DiscountPlanPage.class);
		discountPlanPage.clickOnActivateButton();
	}
	
	@Parameters({ "discountAmount" })
	@Test(priority=41, dependsOnMethods="com.opencellsoft.tests.InvoicesTests.validateInvoiceWithDiscount")
	public void checkApplicationOfDiscountOnInvoiceLine(String discountAmount) {
		InvoicePage invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		Assert.assertEquals(invoicePage.getDiscountAmountValue(), "€"+discountAmount);
	}

	@Parameters({ "applicationFilter" })
	public void createDiscountPlanWithApplicationFilterEL(String applicationFilter) {

		DiscountPlanPage discountPlanPage = PageFactory.initElements(driver, DiscountPlanPage.class);
		discountPlanPage.setApplicationFilter(applicationFilter);

	}

	@Test(priority = 4)
	public void addDiscountLinesTodiscountPlan() {
		DiscountPlanPage discountPlanPage = PageFactory.initElements(driver, DiscountPlanPage.class);
		discountPlanPage.clickOnCreateDiscountLineButton();
		for (int i = 0; i <= 7; i++) {
			NewDiscountLinePage discountLinePage = PageFactory.initElements(driver, NewDiscountLinePage.class);
			discountLinePage.createDiscountLine(Constant.discountLineCode);
			discountLinePage.clickOnSaveDiscountLineButton();
		}
	}



}
