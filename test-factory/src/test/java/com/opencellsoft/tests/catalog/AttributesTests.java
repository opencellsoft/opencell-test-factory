package com.opencellsoft.tests.catalog;

import java.awt.AWTException;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Ignore;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.catalog.catalogmanager.attributes.AttributePage;
import com.opencellsoft.pages.catalog.catalogmanager.attributes.AttributesListPage;
import com.opencellsoft.pages.catalog.catalogmanager.attributes.NewAttributePage;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.products.ProductPage;
import com.opencellsoft.utility.Constant;

public class AttributesTests extends TestBase {

	@Parameters({ "attributeTypeText" })
	@Test(priority = 1, groups = { "attributesTests" })
	public void createTextValueAttribute(String attributeTypeText) throws AWTException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCatalogButton();
		homePage.clickOnAttributeButton();
		AttributesListPage attributesListPage = PageFactory.initElements(driver, AttributesListPage.class);
		attributesListPage.clickOnCreateAttributeButton();
		NewAttributePage newAttributePage = PageFactory.initElements(driver, NewAttributePage.class);
		newAttributePage.selectAttributeType(attributeTypeText);
		newAttributePage.setAttributeCode(Constant.textAttributeCode);
		newAttributePage.setAttributeDescription(Constant.textAttributeDescription);
		newAttributePage.clickOnSaveAttributeButton();
		AttributePage attributePage = PageFactory.initElements(driver, AttributePage.class);
		Assert.assertTrue(attributePage.getTextMessage().toLowerCase().contains("element created"));
		attributePage.clickOnGoBackButton();
	}	
	
	@Parameters({ "attributeTypeNumeric", "numberOfDecimals" })
	@Test(priority = 2, groups = { "attributesTests" })
	public void createNumericValueAttribute(String attributeTypeNumeric, String numberOfDecimals) throws AWTException {
		AttributesListPage attributesListPage = PageFactory.initElements(driver, AttributesListPage.class);
		attributesListPage.clickOnCreateAttributeButton();
		NewAttributePage newAttributePage = PageFactory.initElements(driver, NewAttributePage.class);
		newAttributePage.selectAttributeType(attributeTypeNumeric);
		newAttributePage.setAttributeCode(Constant.numericAttributeCode);
		newAttributePage.setAttributeDescription(Constant.numericAttributeDescription);
		newAttributePage.setNumberOfDecimals(numberOfDecimals);
		newAttributePage.clickOnSaveAttributeButton();
		AttributePage attributePage = PageFactory.initElements(driver, AttributePage.class);
		Assert.assertTrue(attributePage.getTextMessage().toLowerCase().contains("element created"));
		attributePage.clickOnGoBackButton();
	}

	@Parameters({ "attributeTypeInformation" })
	@Test(priority = 2, groups = { "attributesTests" })
	public void createInformationTypeAttribute(String attributeTypeInformation) throws AWTException {
		AttributesListPage attributesListPage = PageFactory.initElements(driver, AttributesListPage.class);
		attributesListPage.clickOnCreateAttributeButton();
		NewAttributePage newAttributePage = PageFactory.initElements(driver, NewAttributePage.class);
		newAttributePage.selectAttributeType(attributeTypeInformation);
		newAttributePage.setAttributeCode(Constant.informationAttributeCode);
		newAttributePage.setAttributeDescription(Constant.informationAttributeDescription);
		newAttributePage.clickOnSaveAttributeButton();
		AttributePage attributePage = PageFactory.initElements(driver, AttributePage.class);
		Assert.assertTrue(attributePage.getTextMessage().toLowerCase().contains("element created"));
		attributePage.clickOnGoBackButton();
	}

	@Parameters({ "attributeTypeInteger" })
	@Test(priority = 3, groups = { "attributesTests" })
	public void createIntegerValueAttribute(String attributeTypeInteger) throws AWTException {
		AttributesListPage attributesListPage = PageFactory.initElements(driver, AttributesListPage.class);
		attributesListPage.clickOnCreateAttributeButton();
		NewAttributePage newAttributePage = PageFactory.initElements(driver, NewAttributePage.class);
		newAttributePage.selectAttributeType(attributeTypeInteger);
		newAttributePage.setAttributeCode(Constant.integerAttributeCode);
		newAttributePage.setAttributeDescription(Constant.integerAttributeDescription);
		newAttributePage.clickOnSaveAttributeButton();
		AttributePage attributePage = PageFactory.initElements(driver, AttributePage.class);
		Assert.assertTrue(attributePage.getTextMessage().toLowerCase().contains("element created"));
		attributePage.clickOnGoBackButton();
	}

	@Parameters({ "attributeTypeDate" })
	@Test(priority = 4, groups = { "attributesTests" })
	public void createDateAttribute(String attributeTypeDate) throws AWTException {
		AttributesListPage attributesListPage = PageFactory.initElements(driver, AttributesListPage.class);
		attributesListPage.clickOnCreateAttributeButton();
		NewAttributePage newAttributePage = PageFactory.initElements(driver, NewAttributePage.class);
		newAttributePage.selectAttributeType(attributeTypeDate);
		newAttributePage.setAttributeCode(Constant.dateAttributeCode);
		newAttributePage.setAttributeDescription(Constant.dateAttributeDescription);
		newAttributePage.clickOnSaveAttributeButton();
		AttributePage attributePage = PageFactory.initElements(driver, AttributePage.class);
		Assert.assertTrue(attributePage.getTextMessage().toLowerCase().contains("element created"));
		attributePage.clickOnGoBackButton();
	}

	@Parameters({ "attributeTypeEmail" })
	@Test(priority = 5, groups = { "attributesTests" })
	public void createEmailAttribute(String attributeTypeEmail) throws AWTException {
		AttributesListPage attributesListPage = PageFactory.initElements(driver, AttributesListPage.class);
		attributesListPage.clickOnCreateAttributeButton();
		NewAttributePage newAttributePage = PageFactory.initElements(driver, NewAttributePage.class);
		newAttributePage.selectAttributeType(attributeTypeEmail);
		newAttributePage.setAttributeCode(Constant.emailAttributeCode);
		newAttributePage.setAttributeDescription(Constant.emailAttributeDescription);
		newAttributePage.clickOnSaveAttributeButton();
		AttributePage attributePage = PageFactory.initElements(driver, AttributePage.class);
		Assert.assertTrue(attributePage.getTextMessage().toLowerCase().contains("element created"));
		attributePage.clickOnGoBackButton();
	}

	@Parameters({ "attributeTypePhone" })
	@Test(priority = 6, groups = { "attributesTests" })
	public void createPhoneAttribute(String attributeTypePhone) throws AWTException {
		AttributesListPage attributesListPage = PageFactory.initElements(driver, AttributesListPage.class);
		attributesListPage.clickOnCreateAttributeButton();
		NewAttributePage newAttributePage = PageFactory.initElements(driver, NewAttributePage.class);
		newAttributePage.selectAttributeType(attributeTypePhone);
		newAttributePage.setAttributeCode(Constant.phoneAttributeCode);
		newAttributePage.setAttributeDescription(Constant.phoneAttributeDescription);
		newAttributePage.clickOnSaveAttributeButton();
		AttributePage attributePage = PageFactory.initElements(driver, AttributePage.class);
		Assert.assertTrue(attributePage.getTextMessage().toLowerCase().contains("element created"));
		attributePage.clickOnGoBackButton();
	}

	@Parameters({ "attributeTypeEL" })
	@Test(priority = 7, groups = { "attributesTests" })
	public void createElAttribute(String attributeTypeEL) throws InterruptedException, AWTException {
		AttributesListPage attributesListPage = PageFactory.initElements(driver, AttributesListPage.class);
		attributesListPage.clickOnCreateAttributeButton();
		NewAttributePage newAttributePage = PageFactory.initElements(driver, NewAttributePage.class);
		newAttributePage.selectAttributeType(attributeTypeEL);
		newAttributePage.setAttributeCode(Constant.eLAttributeCode);
		newAttributePage.setAttributeDescription(Constant.eLAttributeDescription);
		newAttributePage.clickOnSaveAttributeButton();
		AttributePage attributePage = PageFactory.initElements(driver, AttributePage.class);
		Assert.assertTrue(attributePage.getTextMessage().toLowerCase().contains("element created"));
		attributePage.clickOnGoBackButton();
	}

	@Parameters({ "attributeTypeBoolean" })
	@Test(priority = 7, groups = { "attributesTests" })
	public void createBooleanAttribute(String attributeTypeBoolean) throws AWTException {
		AttributesListPage attributesListPage = PageFactory.initElements(driver, AttributesListPage.class);
		attributesListPage.clickOnCreateAttributeButton();
		NewAttributePage newAttributePage = PageFactory.initElements(driver, NewAttributePage.class);
		newAttributePage.selectAttributeType(attributeTypeBoolean);
		newAttributePage.setAttributeCode(Constant.booleanAttributeCode);
		newAttributePage.setAttributeDescription(Constant.booleanAttributeDescription);
		newAttributePage.clickOnSaveAttributeButton();
		AttributePage attributePage = PageFactory.initElements(driver, AttributePage.class);
		Assert.assertTrue(attributePage.getTextMessage().toLowerCase().contains("element created"));
		attributePage.clickOnGoBackButton();
	}

	@Ignore
	@Parameters({ "attributeTypeListOfTextvalues" })
	@Test(priority = 8, groups = { "attributesTests" })
	public void createListOfTextvaluesAttribute(String attributeTypeListOfTextvalues) throws AWTException {
		AttributesListPage attributesListPage = PageFactory.initElements(driver, AttributesListPage.class);
		attributesListPage.clickOnCreateAttributeButton();
		NewAttributePage newAttributePage = PageFactory.initElements(driver, NewAttributePage.class);
		newAttributePage.selectAttributeType(attributeTypeListOfTextvalues);
		newAttributePage.setAttributeCode(Constant.ListOfTextValuesAttributeCode);
		newAttributePage.setAttributeDescription(Constant.ListOfTextValuesAttributeDescription);
		newAttributePage.clickOnSaveAttributeButton();
		AttributePage attributePage = PageFactory.initElements(driver, AttributePage.class);
		Assert.assertTrue(attributePage.getTextMessage().toLowerCase().contains("element created"));
		attributePage.clickOnGoBackButton();
	}

	@Parameters({ "attributeTypeText" })
	@Test(priority = 1, groups = { "articlesTests" })
	public void createTextValueAttributeForArticle(String attributeTypeText) throws AWTException {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnAttributesTab();
		productPage.clickOnCreateAttributeButton();
		NewAttributePage newAttributePage = PageFactory.initElements(driver, NewAttributePage.class);
		newAttributePage.selectAttributeType(attributeTypeText);
		newAttributePage.setAttributeCode(Constant.textAttributeCode);
		newAttributePage.setAttributeDescription(Constant.textAttributeDescription);
		newAttributePage.clickOnSaveAttributeButton();
		AttributePage attributePage = PageFactory.initElements(driver, AttributePage.class);
		Assert.assertTrue(attributePage.getTextMessage().toLowerCase().contains("element created"));
		attributePage.clickOnGoBackButton();
		productPage.clickOnSaveButton();
		Assert.assertTrue(productPage.getTextMessage().toLowerCase().contains("element updated"));
	}

	@Parameters({ "attributeTypeNumeric", "numberOfDecimals" })
	@Test(priority = 2, groups = { "articlesTests" })
	public void createNumericValueAttributeForArticle(String attributeTypeNumeric, String numberOfDecimals)
			throws AWTException {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnAttributesTab();
		productPage.clickOnCreateAttributeButton();
		NewAttributePage newAttributePage = PageFactory.initElements(driver, NewAttributePage.class);
		newAttributePage.selectAttributeType(attributeTypeNumeric);
		newAttributePage.setAttributeCode(Constant.numericAttributeCode);
		newAttributePage.setAttributeDescription(Constant.numericAttributeDescription);
		newAttributePage.setNumberOfDecimals(numberOfDecimals);
		newAttributePage.clickOnSaveAttributeButton();
		AttributePage attributePage = PageFactory.initElements(driver, AttributePage.class);
		Assert.assertTrue(attributePage.getTextMessage().toLowerCase().contains("element created"));
		attributePage.clickOnGoBackButton();
		productPage.clickOnSaveButton();
		Assert.assertTrue(productPage.getTextMessage().toLowerCase().contains("element updated"));
	}

}
