package com.opencellsoft.tests;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.operations.billingrules.BillingCyclePage;
import com.opencellsoft.pages.operations.billingrules.BillingCyclesListPage;

public class BillingCycleTests extends TestBase {
	
	BillingCyclesListPage billingCyclesListPage;
	BillingCyclePage BillingCyclePage;

	public void createBillingCycle(String code, String description, String BillingCalendar, Boolean IncrementalInvoiceLines, String applicationEl) {
		billingCyclesListPage = new BillingCyclesListPage();
		BillingCyclePage = new BillingCyclePage();
		billingCyclesListPage.clickOnCreateNewBillingCycleButton();
		BillingCyclePage.setCode(code);
		BillingCyclePage.setDescription(description);
		BillingCyclePage.selectBillingCalendar(BillingCalendar);
		BillingCyclePage.setIncrementalInvoiceLines(IncrementalInvoiceLines);
		if(!applicationEl.equals("")) {BillingCyclePage.setApplicationEl(applicationEl);}
		BillingCyclePage.clickOnSaveButton();
		
	}
	
	public void updateBillingCycleAggregationRules(Boolean enableAggregation,String dateAggregation,String discountAggregation ,Boolean useAccountingArticleLabel,Boolean aggregateUnitPrice, Boolean ignoreSubscriptions, Boolean ignoreOrders, Boolean ignoreConsumers, Boolean businessKey, Boolean parameter1, Boolean parameter2, Boolean parameter3 ) {
		BillingCyclePage = new BillingCyclePage();
		BillingCyclePage.goToAggregationRulesTab();
		BillingCyclePage.setEnableAggregation(enableAggregation);
		BillingCyclePage.setDateAggregation(dateAggregation);
		BillingCyclePage.setDiscountAggregation(discountAggregation);
		BillingCyclePage.setUseAccountingArticleLabel(useAccountingArticleLabel);
		BillingCyclePage.setAggregateUnitPrice(aggregateUnitPrice);
		if(ignoreSubscriptions != null) {BillingCyclePage.setIgnoreSubscriptions(ignoreSubscriptions);}
		if(ignoreOrders != null)    {BillingCyclePage.setIgnoreOrders(ignoreOrders);}
		if(ignoreConsumers != null) {BillingCyclePage.setIgnoreConsumers(ignoreConsumers);}
		if(businessKey != null)     {BillingCyclePage.setBusinessKey(businessKey);}
		if(parameter1 != null)      {BillingCyclePage.setParameter1(parameter1);}
		if(parameter2 != null)      {BillingCyclePage.setParameter2(parameter2);}
		if(parameter3 != null)      {BillingCyclePage.setParameter3(parameter3);}
		BillingCyclePage.clickOnSaveButton();
	}
	
	public void updateBillingCycleSplitRules(String splitLevel) {
		BillingCyclePage = new BillingCyclePage();
		BillingCyclePage.goToSplitRulesTab();
		BillingCyclePage.setSplitLevel(splitLevel);
		BillingCyclePage.clickOnSaveButton();
		
	}
}
