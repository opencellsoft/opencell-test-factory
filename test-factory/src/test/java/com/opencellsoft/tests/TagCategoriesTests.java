package com.opencellsoft.tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.catalog.tagcategories.NewTagCategoriesPage;
import com.opencellsoft.pages.catalog.tagcategories.TagCategoriesListPage;
import com.opencellsoft.pages.catalog.tagcategories.TagCategoriesPage;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.utility.Constant;

public class TagCategoriesTests extends TestBase {
	
	TagCategoriesListPage tagCategoriesListPage;
	TagCategoriesPage tagCategoriesPage;
	public void createTagCategory(String tag_code,String tag_name,String tag_seller) {

		tagCategoriesListPage = new TagCategoriesListPage();
		tagCategoriesListPage.clickOnCreateButton();
		
		tagCategoriesPage = new TagCategoriesPage();
		tagCategoriesPage.setCode(tag_code);
		tagCategoriesPage.setName(tag_name);
		tagCategoriesPage.selectSeller(tag_seller);
		tagCategoriesPage.clickOnSaveButton();
	}
	
	
	public void updateTagCategory(String tag_name) {
		tagCategoriesPage = new TagCategoriesPage();
		if(tag_name != null) {tagCategoriesPage.setName(tag_name);}
		tagCategoriesPage.clickOnSaveButton();
	}
	
	public void delete() {
		tagCategoriesPage = new TagCategoriesPage();
		tagCategoriesPage.clickOnDeleteButton();
		tagCategoriesPage.clickOnConfirmButton();
	}
	
	public void goBack() {
		tagCategoriesPage = new TagCategoriesPage();
		tagCategoriesPage.clickOnGoBackButton();
	}
	
	public void searchTagCategoriesByName(String value) {
		tagCategoriesListPage = new TagCategoriesListPage();
		tagCategoriesListPage.setSearchInput(value);
	}
	
	public int tagCategoriesTotal() {
		tagCategoriesListPage = new TagCategoriesListPage();
		return tagCategoriesListPage.tagCategoriesList().size();
	}
	
	public void goToTagCategoriesDetails (String email) {
		tagCategoriesListPage = new TagCategoriesListPage();
		tagCategoriesListPage.goToTagCategoriesDetails(email);
	}
	
	public void activateFilterBy(String value1, String value2) {
		tagCategoriesListPage = new TagCategoriesListPage();
		tagCategoriesListPage.clickOnFilterButton();
		try {
			tagCategoriesListPage.activateFilterBy(value1);
		}catch (Exception e) {
			tagCategoriesListPage.activateFilterBy(value2);
		}
	}
	
	public void filterByTagCategoriesName(String value) {
		tagCategoriesListPage = new TagCategoriesListPage();
		tagCategoriesListPage.setFilterByTagCategoriesName(value);
	}
	
	public void sortTagCategoriesListBy(String value) {
		tagCategoriesListPage = new TagCategoriesListPage();
		tagCategoriesListPage.sortBy(value);
	}
	
	public void closeFilterBy(String value) {
		tagCategoriesListPage = new TagCategoriesListPage();
		tagCategoriesListPage.clickOnRemoveFilterBy(value);
	}
	
	@Parameters({ "selectSeller" })
	@Test (priority=1)
	public void createTagCategories(String selectSeller) {

		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCatalogButton();
		homePage.clickOnTagCategoriesBtn();
	
		NewTagCategoriesPage newTagCategoriesPage = PageFactory.initElements(driver, NewTagCategoriesPage.class);
		newTagCategoriesPage.clickOnCreateTagCategoriesBtn();
		newTagCategoriesPage.setTagCategoryCode(Constant.tagCategoryCode);
		newTagCategoriesPage.setTagCategoryName(Constant.tagCategoryName);
		newTagCategoriesPage.selectSeller(selectSeller);
		newTagCategoriesPage.clickSave();
		newTagCategoriesPage.clickOnGoBack();
	}

	@Test (priority=2)
	public void searchWithDefaultFilter() {
		TagCategoriesListPage tagCategoryListPage = PageFactory.initElements(driver, TagCategoriesListPage.class);
		tagCategoryListPage.setSearchInput(Constant.tagCategoryName);
	}


	@Test (priority=4)
	public void deleteTagCategory() {
		TagCategoriesPage tagCategoriesPage = PageFactory.initElements(driver, TagCategoriesPage.class);
		tagCategoriesPage.clickOnDeleteButton();
		tagCategoriesPage.clickOnConfirmButton();
	}
}