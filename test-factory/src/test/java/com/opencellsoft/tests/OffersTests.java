package com.opencellsoft.tests;

import java.awt.AWTException;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.articles.ArticlePage;
import com.opencellsoft.pages.catalog.catalogmanager.attributes.AttributePage;
import com.opencellsoft.pages.catalog.catalogmanager.attributes.NewAttributePage;
import com.opencellsoft.pages.catalog.catalogmanager.media.NewMediaPage;
import com.opencellsoft.pages.catalog.tags.TagsListPage;
import com.opencellsoft.pages.catalog.tags.TagsPage;
import com.opencellsoft.pages.commercialrules.NewCommercialRules;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.offers.ListOfOffersPage;
import com.opencellsoft.pages.offers.ModifyOfferPage;
import com.opencellsoft.pages.offers.NewOfferPage;
import com.opencellsoft.pages.offers.OfferPage;
import com.opencellsoft.pages.orders.OrderOfferPage;
import com.opencellsoft.pages.orders.OrderPage;
import com.opencellsoft.pages.orders.OrdersListPage;
import com.opencellsoft.pages.products.NewProductPage;
import com.opencellsoft.pages.products.ProductPage;
import com.opencellsoft.utility.Constant;

import org.testng.annotations.Ignore;
import org.testng.annotations.Parameters;

public class OffersTests extends TestBase {

	@Parameters({ "selectSeller" })
	@Test(priority = 16, groups = { "smoke tests", "administration tests" })
	public void createOffer(String selectSeller) throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		driver.navigate().refresh();
		Thread.sleep(4000);
		homePage.clickOnCatalogButtonMenu();
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		waitPageLoaded();
		offerListPage.clickOnCreateOfferButton();
		NewOfferPage newOfferPage = PageFactory.initElements(driver, NewOfferPage.class);
		//newOfferPage.setOfferCode(Constant.offerCode);
		waitPageLoaded();
		Thread.sleep(2000);
		newOfferPage.setOfferDescription(Constant.offerDescription);
		//newOfferPage.setValidFromDate();
		//		newOfferPage.selectSeller(selectSeller);
		newOfferPage.clickOnSubmitButton();
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		//Assert.assertEquals(offerPage.getTextMessage(), "Element created");
		//Assert.assertEquals(offerPage.getOfferCode(), Constant.offerCode);
		//Assert.assertEquals(offerPage.getOfferDescription(), Constant.offerDescription);
	}

	@Test(priority = 17)
	public void assignDiscountPlanToOffer() {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnDiscountPlanInput();
		offerPage.clickOnAddFilter();
		offerPage.clickOnFilterCode();
		offerPage.setCodeDiscountPlan(Constant.discountPlanCode);
		offerPage.clickOnDiscountPlanSelected();
		offerPage.clickOnSaveButton();
		offerPage.clickOnGoBackButton();
	}

	@Test(priority = 16, groups = { "smoke tests", "administration tests" })
	public void createOfferWithDiscountPlan() throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCatalogButton();
		// homePage.clickOnCommercialOffersBtn();
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnCreateOfferButton();
		NewOfferPage newOfferPage = PageFactory.initElements(driver, NewOfferPage.class);
		newOfferPage.setOfferCode(Constant.offerCode);
		newOfferPage.setOfferDescription(Constant.offerDescription);
		newOfferPage.setValidFromDate();
		newOfferPage.clickOnDiscountPlanInput();
		newOfferPage.clickOnAddFilterButton();
		newOfferPage.clickOnAddFilterByCode();
		newOfferPage.setDiscountCode(Constant.discountPlanCode);
		Thread.sleep(1000);
		newOfferPage.selectDiscountPlan();
		newOfferPage.clickOnSubmitButton();
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		Assert.assertEquals(offerPage.getTextMessage(), "Element created");
		Assert.assertEquals(offerPage.getOfferCode(), Constant.offerCode);
		Assert.assertEquals(offerPage.getOfferDescription(), Constant.offerDescription);
	}

	@Test(priority = 17, groups = { "smoke tests", "administration tests" })
	public void addExistingProduct() throws InterruptedException {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		waitPageLoaded();
		offerPage.clickOnPickProductButton();
		offerPage.clickOnAddFilterButton();
		offerPage.clickOnFilterProductCode();
		offerPage.setCodeProduct(Constant.productCode);
		//offerPage.clickOnSortButton();
		waitPageLoaded();
		// offerPage.clickOnProductList();
		offerPage.clickOnProductCodeFound(Constant.productCode);
		offerPage.clickOnSaveButton();
	}

	@Test(priority = 18, groups = { "smoke tests", "administration tests" })
	public void changeDraftToPublished() throws InterruptedException {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		waitPageLoaded();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// Scrolling down the page till the element is found
		// js.executeScript("arguments[0].scrollIntoView();", activateOfferButton);
		js.executeScript("window.scrollTo(0, 0)");
		Thread.sleep(2000);
		offerPage.clickOnActivateOfferButton();
		waitPageLoaded();
		offerPage.clickOnSaveButton();
		waitPageLoaded();
		offerPage.clickOnGoBackButton();
		//Assert.assertEquals(offerPage.getTextMessage(), "Action executed successfuly");

	}

	@Test(priority = 3, dependsOnMethods = "com.opencellsoft.tests.InvoicesTests.viewPDFInvoice")
	public void duplicateOffer() throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		driver.navigate().refresh();
		waitPageLoaded();
		homePage.clickOnCatalogButton();
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		waitPageLoaded();
		offerListPage.clickOnFilterButton();
		offerListPage.clickOnFilterByOfferCode();
		offerListPage.setOfferCode(Constant.offerCode);
		waitPageLoaded();
		offerListPage.clickOnCodeOfferFound(Constant.offerCode);
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		waitPageLoaded();
		offerPage.clickOnActionsButton();
		offerPage.clickOnDuplicateOffer();
		waitPageLoaded();
		Assert.assertEquals(offerPage.getTextMessage(), "Action executed successfuly");
		//		offerPage.clickOnSaveButton();
	}

	@Test(priority = 3, dependsOnMethods = "com.opencellsoft.tests.OffersTests.duplicateOffer")
	public void createNewProductFromOfferScreen() throws InterruptedException, AWTException {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		waitPageLoaded();
		offerPage.clickOnCreateProductButton();
		NewProductPage newProductPage = PageFactory.initElements(driver, NewProductPage.class);
		waitPageLoaded();
		newProductPage.setProductCode(Constant.productCode2);
		newProductPage.setProductLabel(Constant.productDescription2);
		//newProductPage.selectProductBrand();
		newProductPage.clickOnPricingTab();
		newProductPage.selectPriceVersionDateSetting("Quote date");
		newProductPage.clickOnSaveProductButton();
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		waitPageLoaded();
		Assert.assertEquals(productPage.getTextMessage(), "Element created");
		Assert.assertEquals(productPage.getProductCodeInputText(), Constant.productCode2);
		Assert.assertEquals(productPage.getProductLabelInputText(), Constant.productDescription2);
		productPage.clickOnGoBackButton();
		waitPageLoaded();
		offerPage.clickOnSaveOfferButton();
		Assert.assertEquals(offerPage.getTextMessage(), "Element updated");
	}

	@Test(priority = 4, dependsOnMethods = "com.opencellsoft.tests.OffersTests.createNewProductFromOfferScreen")
	public void addTextAttributeToProduct() throws AWTException, InterruptedException {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		driver.navigate().refresh();
		waitPageLoaded();
		offerPage.clickOnProductCode(Constant.productCode2);
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		waitPageLoaded();
		productPage.clickOnAttributesTab();
		productPage.clickOnAddAttributeButton();
		NewAttributePage newAttributePage = PageFactory.initElements(driver, NewAttributePage.class);
		waitPageLoaded();
		newAttributePage.selectAttributeType("Text value");
		newAttributePage.setAttributeCode(Constant.textAttributeCode);
		newAttributePage.setAttributeDescription(Constant.textAttributeDescription);
		newAttributePage.clickOnSaveAttributeButton();
		Thread.sleep(2000);
		AttributePage attributePage = PageFactory.initElements(driver, AttributePage.class);
		waitPageLoaded();
		Assert.assertEquals(attributePage.getAttributeDescriptionInputText(), Constant.textAttributeDescription);
		newAttributePage.clickOnGoBackButton();
		productPage.clickOnSaveButton();
		waitPageLoaded();
		Assert.assertEquals(productPage.getTextMessage(), "Element updated");
	}

	@Test(priority = 6, dependsOnMethods = "com.opencellsoft.tests.OffersTests.addTextAttributeToProduct")
	public void setProductAttributeParameters() throws InterruptedException, AWTException {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnAttributesTab();
		productPage.clickOnTextAttributeParametersButton();
		productPage.toggleMandatoryAttributeOn();
		//productPage.toggleReadOnlyAttributeOn();
		waitPageLoaded();
		productPage.clickOnConfirmButton();
		productPage.clickOnSaveButton();
		Assert.assertEquals(productPage.getTextMessage(), "Element updated");
		productPage.clickOnPublishProductVersionButton();
		waitPageLoaded();
		productPage.activateProduct();
		productPage.clickOnGoBackButton();
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		waitPageLoaded();
		offerPage.clickOnActivateOfferButton();
	}

	@Test(priority = 7, dependsOnMethods = "com.opencellsoft.tests.OffersTests.setProductAttributeParameters")
	public void duplicateOrder() throws InterruptedException, AWTException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCustomerCareButton();
		homePage.clickOnOrdersButton();
		OrdersListPage ordersListPage = PageFactory.initElements(driver, OrdersListPage.class);
		ordersListPage.clickAddFilterCustomerButton();
		ordersListPage.selectCustomerFilter();
		ordersListPage.enterCustomer(Constant.customerCode);
		Thread.sleep(2000);
		ordersListPage.clickOnOrderFound();
		Thread.sleep(2000);
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		orderPage.clickOnDuplicateOrderButton();
		Assert.assertEquals(orderPage.getTextMessage(), "Duplication successful. Check delivery dates for order lines");
		Thread.sleep(2000);
		orderPage.clickOnSaveOrderButton();
	}

	@Parameters({ "quantity" })
	@Test(priority = 7, dependsOnMethods = "com.opencellsoft.tests.OffersTests.duplicateOrder")
	public void createNewOrderLine(String quantity) throws InterruptedException, AWTException {
		OrderPage orderPage = PageFactory.initElements(driver, OrderPage.class);
		waitPageLoaded();
		orderPage.clickOnAddOrderLineButton();
		orderPage.clickOnCreateOrderLineButton();
		OrderOfferPage orderOfferPage = PageFactory.initElements(driver, OrderOfferPage.class);
		waitPageLoaded();
		orderOfferPage.selectOfferTemplate(Constant.offerCode + "-Copy");
		waitPageLoaded();
		orderOfferPage.clickOnProduct2Tab();	
		//orderOfferPage.selectProduct();
		orderOfferPage.setProductQuantity(quantity);
		//orderOfferPage.clickSaveButton();
		//Assert.assertEquals(orderOfferPage.getTextMessage(), "Element created");
	}

	@Test(priority = 7, dependsOnMethods = "com.opencellsoft.tests.OffersTests.createNewOrderLine")
	public void checkReadOnlyAttributeParameter() throws InterruptedException, AWTException {
		OrderOfferPage orderOfferPage = PageFactory.initElements(driver, OrderOfferPage.class);
		orderOfferPage.clickOnEditAttribute();
		Assert.assertEquals(orderOfferPage.getClassAttributeValue(), "cell disabled");
	}

	@Test(priority = 8, dependsOnMethods = "com.opencellsoft.tests.OffersTests.setProductAttributeParameters")
	public void checkMandatoryAttributeParameter() throws InterruptedException, AWTException {
		OrderOfferPage orderOfferPage = PageFactory.initElements(driver, OrderOfferPage.class);
		waitPageLoaded();
		orderOfferPage.clickOnEditAttribute();
		Assert.assertEquals(orderOfferPage.getClassAttributeValue(), "cell disabled");
	}

	@Ignore
	@Parameters({ "defaultQuantity", "minimalQuantity", "maximalQuantity", "sequence" })
	@Test(priority = 4)
	public void fillProductParameters(String defaultQuantity, String minimalQuantity, String maximalQuantity,
			String sequence) {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnParametersProductButton();
		offerPage.setDefaultQuantity(defaultQuantity);
		offerPage.setMinimalQuantity(minimalQuantity);
		offerPage.setMaximalQuantity(maximalQuantity);
		offerPage.setSequence(sequence);
		offerPage.clickOnConfirm();
		offerPage.clickOnSaveButton();
	}

	@Ignore
	@Test(priority = 6)
	public void addMediaToOffer() {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnMediaTab();
		offerPage.clickOnCreateMedia();
		NewMediaPage newMediaPage = PageFactory.initElements(driver, NewMediaPage.class);
		newMediaPage.selectMediaTypeImage();
		newMediaPage.setMediaCode(Constant.mediaCode);
		newMediaPage.setMediaDescription(Constant.mediaDescription);
		newMediaPage.setURLPath(Constant.urlPathMedia);
		newMediaPage.clickOnSubmitButton();
		newMediaPage.clickOnGoBackButton();
		offerPage.clickOnSaveButton();
		offerPage.clickOnGoBackButton();
	}

	@Ignore
	@Test(priority = 7)
	public void removeMedia() {
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnFilterButton();
		offerListPage.clickOnFilterCodeButton();
		offerListPage.setOfferCode(Constant.offerCode);
		offerListPage.clickOnOffer();
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnMediaTab();
		offerPage.clickOnCheckBoxMedia();
		offerPage.clickOnDeleteMedia();
		offerPage.clickOnConfirmDeleteMedia();
		offerPage.clickOnSaveButton();
		offerPage.clickOnGoBackButton();
	}

	@Ignore
	@Test(priority = 8)
	public void listMedia() {
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnFilterButton();
		offerListPage.clickOnFilterCodeButton();
		offerListPage.setOfferCode(Constant.offerCode);
		offerListPage.clickOnOffer();
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnMediaTab();
		offerPage.clickOnGoBackButton();
	}

	@Ignore
	@Test(priority = 9)
	public void listProduct() {
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnFilterButton();
		offerListPage.clickOnFilterCodeButton();
		offerListPage.setOfferCode(Constant.offerCode);
		offerListPage.clickOnOffer();
	}

	@Ignore
	@Test(priority = 11)
	public void unlinkProduct() {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnCheckBoxProduct();
		offerPage.clickOnDissociateProduct();
		offerPage.clickOnConfirmDissociation();
		offerPage.clickOnSaveButton();
	}

	@Ignore
	@Parameters({ "commercialType" })
	@Test(priority = 12)
	public void createCommercialRule(String commercialType) {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnCommercialRulesTab();
		offerPage.clickOnCreateCommercialRule();
		NewCommercialRules newCommercialRule = PageFactory.initElements(driver, NewCommercialRules.class);
		newCommercialRule.setCommercialRulesCode(Constant.commercialRuleCode);
		newCommercialRule.setCommercialRulesSummary(Constant.commercialRuleDescription);
		newCommercialRule.commercialType(commercialType);
		newCommercialRule.clickOnSubmitButton();
		newCommercialRule.clickOnGoButtonButton();
	}

	@Ignore
	@Test(priority = 13)
	public void ListCommercialRule() {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnGoBackButton();
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnFilterButton();
		offerListPage.clickOnFilterCodeButton();
		offerListPage.setOfferCode(Constant.offerCode);
		offerListPage.clickOnOffer();
		offerPage.clickOnCommercialRulesTab();
	}

	@Ignore
	@Test(priority = 14)
	public void removeCommercialRule() {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnGoBackButton();
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnFilterButton();
		offerListPage.clickOnFilterCodeButton();
		offerListPage.setOfferCode(Constant.offerCode);
		offerListPage.clickOnOffer();
		offerPage.clickOnCommercialRulesTab();
		offerPage.clickOnCheckBoxCommercialRule();
		offerPage.clickOnDeleteCommercialRule();
		offerPage.clickOnConfirmDeleteCommercialRule();
		offerPage.clickOnSaveButton();
	}

	@Ignore
	@Test(priority = 16)
	public void removeDiscountPlan() {
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnFilterButton();
		offerListPage.clickOnFilterCodeButton();
		offerListPage.setOfferCode(Constant.offerCode);
		offerListPage.clickOnOffer();
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnRemoveDiscountPlan();
		offerPage.clickOnSaveButton();
	}

	@Ignore
	@Test(priority = 17)
	public void restrictOffersForSubChanges() {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnRestrictOffersGrid();
		offerPage.clickOnAllowedOffersChange();
		offerPage.clickOnAddFilter();
		offerPage.clickOnAddFilterCode();
		offerPage.setCodeOffer(Constant.offerCode);
		offerPage.clickOffer();
		offerPage.clickOnSaveButton();
	}

	@Ignore
	@Parameters({ "attributType" })
	@Test(priority = 18)
	public void addNewAttribute(String attributType) throws AWTException {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnAttributesTab();
		offerPage.clickOnCreateAttributeButton();
		NewAttributePage newAttributePage = PageFactory.initElements(driver, NewAttributePage.class);
		newAttributePage.selectAttributeType(attributType);
		newAttributePage.setAttributeCode(Constant.ListOfTextValuesAttributeCode);
		newAttributePage.setAttributeDescription(Constant.ListOfTextValuesAttributeDescription);
		newAttributePage.clickOnSaveAttributeButton();
		newAttributePage.clickOnGoBackButton();
		offerPage.clickOnSaveButton();
		offerPage.clickOnGoBackButton();
	}

	@Ignore
	@Test(priority = 19)
	public void removeAttribute() {
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnFilterButton();
		offerListPage.clickOnFilterCodeButton();
		offerListPage.setOfferCode(Constant.offerCode);
		offerListPage.clickOnOffer();
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnAttributesTab();
		offerPage.clickOnCheckBoxAttribute();
		offerPage.clickOnDeleteAttribute();
		offerPage.clickOnConfirmDeleteAttribute();
		offerPage.clickOnSaveButton();
	}

	@Ignore
	@Parameters({ "RenewalConditionsType", "SubscriptionRenewalPeriod", "RenewUnit" })
	@Test(priority = 20)
	public void fillRenewalCondition(String RenewalConditionsType, String SubscriptionRenewalPeriod, String RenewUnit) {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnRenewalConditionsTab();
		offerPage.clickOnRenewalConditionsType(RenewalConditionsType);
		offerPage.setSubscriptionRenewalPeriod(SubscriptionRenewalPeriod);
		offerPage.clickRenewUnit(RenewUnit);
		offerPage.clickOnSaveButton();
	}

	@Ignore
	@Parameters({ "initialTermType", "subscriptionPeriod", "initialyActiveUnit" })
	@Test(priority = 21)
	public void fillInitialTerm(String initialTermType, String subscriptionPeriod, String initialyActiveUnit) {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnInitialTermTab();
		offerPage.clickOnInitialTermType(initialTermType);
		offerPage.setSubscriptionPeriod(subscriptionPeriod);
		offerPage.clickOnInitialyActiveUnit(initialyActiveUnit);
		offerPage.clickOnEndEngagement();
		offerPage.clickOnSaveButton();
		offerPage.clickOnGoBackButton();
	}

	@Ignore
	public void modifyInitialTerm() throws Exception {
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnFilterButton();
		offerListPage.clickOnFilterCodeButton();
		offerListPage.setOfferCode(Constant.offerCode);
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnInitialTermTab();
		ModifyOfferPage modifyOfferPage = PageFactory.initElements(driver, ModifyOfferPage.class);
		modifyOfferPage.clickOnInitialTermType();
		offerPage.clickOnSaveButton();
		offerPage.clickOnGoBackButton();
	}

	@Ignore
	@Test(priority = 24)
	public void modifyOffer() {
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnFilterButton();
		offerListPage.clickOnFilterCodeButton();
		offerListPage.setOfferCode(Constant.offerCode);
		offerListPage.clickOnOffer();
		ModifyOfferPage modifyOfferPage = PageFactory.initElements(driver, ModifyOfferPage.class);
		modifyOfferPage.setOfferCodeUpdate(Constant.newOfferCode);
		modifyOfferPage.setOfferDescriptionUpdate(Constant.newOfferDescription);
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnSaveButton();
		offerPage.clickOnGoBackButton();
	}

	@Ignore
	@Test(priority = 25)
	public void checkTags() {
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnFilterButton();
		offerListPage.clickOnFilterCodeButton();
		offerListPage.setOfferCode(Constant.newOfferCode);
		offerListPage.clickOnOffer();
	}

	@Ignore
	@Test(priority = 26)
	public void addNewTag() {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnTagsBtn();
		TagsListPage tagListePage = PageFactory.initElements(driver, TagsListPage.class);
		tagListePage.clickOnCreateButton();
		TagsPage newTagPage = PageFactory.initElements(driver, TagsPage.class);
		newTagPage.setCode(Constant.tagCode);
		newTagPage.setName(Constant.tagName);
//		newTagPage.selectCategory();
		newTagPage.clickOnSaveButton();
		newTagPage.clickOnGoBackButton();
		homePage.clickOnCommercialOffersButton();
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnFilterButton();
		offerListPage.clickOnFilterCodeButton();
		offerListPage.setOfferCode(Constant.newOfferCode);
		offerListPage.clickOnOffer();
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnTags();
		offerPage.clickOnAddFilterTag();
		offerPage.clickOnTagCodeFilter();
		offerPage.setTagCode(Constant.tagCode);
		offerPage.clickOnTagSelected();
		offerPage.clickOnSaveButton();
		offerPage.clickOnGoBackButton();
	}

	@Ignore
	@Test(priority = 27)
	public void unlinkTags() {
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnFilterButton();
		offerListPage.clickOnFilterCodeButton();
		offerListPage.setOfferCode(Constant.newOfferCode);
		offerListPage.clickOnOffer();
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnCheckBoxTag();
		offerPage.clickOnSaveButton();
		offerPage.clickOnGoBackButton();
	}

	@Ignore
	@Test(priority = 29)
	public void changePublishedToClosed() {
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnFilterButton();
		offerListPage.clickOnFilterCodeButton();
		offerListPage.setOfferCode(Constant.newOfferCode);
		offerListPage.clickOnOffer();
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnCloseOfferButton();
		offerPage.clickOnConfirmCloseOfferButton();
		offerPage.clickOnSaveButton();
		offerPage.clickOnGoBackButton();
	}

	@Ignore
	@Test(priority = 30)
	public void searchWithDefaultFilter() {
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.setFilterName(Constant.newOfferCode);
		offerListPage.clickOnOffer();
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnGoBackButton();
	}

	@Ignore
	@Test(priority = 31)
	public void searchWithAdvancedFilters() {
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnFilterButton();
		offerListPage.clickOnFilterCodeButton();
		offerListPage.setOfferCode(Constant.newOfferCode);
		offerListPage.clickOnFilterButton();
		offerListPage.clickOnFilterStatusButton();
		offerListPage.clickOnOfferStatus();
		offerListPage.clickOnOffer();
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnGoBackButton();
	}

	@Ignore
	@Test(priority = 32)
	public void latestOffersViewed() {
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnLastOfferViewed();
		offerListPage.clickOnRowsPerPage();
		offerListPage.clickOnCancelButton();
	}

	@Ignore
	@Test(priority = 33)
	public void deleteExistingOffer() {
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnFilterButton();
		offerListPage.clickOnFilterCodeButton();
		offerListPage.setOfferCode(Constant.newOfferCode);
		offerListPage.clickOnOffer();
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnDeleteOfferButton();
		offerPage.clickOnConfirmDeleteOfferButton();
	}

	/***  create Offer  ***/
	public void createOfferMethod(String offerCode,String offerDescription ) {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		waitPageLoaded();
		try {homePage.clickOnCatalogButtonMenu22();}catch (Exception e) {}

		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		//		homePage.clickOnCatalogButton();
		offerListPage.clickOnCreateOfferButton();
		NewOfferPage newOfferPage = PageFactory.initElements(driver, NewOfferPage.class);
		waitPageLoaded();
		newOfferPage.setOfferCode(offerCode);
		waitPageLoaded();
		newOfferPage.setOfferDescription(offerDescription);
		//newOfferPage.setValidFromDate();
		waitPageLoaded();
		newOfferPage.clickOnSubmitButton();

	}
	/***  search For Offer Method  ***/
	public void searchForOfferMethod(String offerCode,String offerDescription) {
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.setOfferDescription(offerDescription);
		offerListPage.clickOnOffer(offerDescription);
	}
	/***  pick Product To Offer  ***/
	public void pickProductToOffer(String productCode, String productDescription, String version) {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		try {
			offerPage.clickOnPickButton();
			findProduct( productCode,  productDescription);
		}catch (Exception e) {
			offerPage.clickOnCancelButton();
			offerPage.clickOnPickButton2();
			findProduct( productCode,  productDescription);
		}

		offerPage.clickOnSaveButton();
		waitPageLoaded();
	}

	public void findProduct(String productCode, String productDescription){
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		try {
			offerPage.clickOnAddFilter();
			waitPageLoaded();
			offerPage.clickOnProductCodeOption();
			waitPageLoaded();
		}catch (Exception e) {} 
		offerPage.setCodeProduct(productCode);
		offerPage.selectProductToPick(productDescription);
		waitPageLoaded();
	}
	/***  fill Product Parameters Method  ***/
	public void fillProductParametersMethod(String defaultQuantity, String minimalQuantity, String maximalQuantity,
			String sequence, boolean visible ) {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnProductsTab2();
		waitPageLoaded();
		try {
			offerPage.clickOnParametersProductButton();
			waitPageLoaded();
		}catch (Exception e) {
			offerPage.clickOnProductsTab2();
			waitPageLoaded();
			offerPage.clickOnParametersProductButton();
			waitPageLoaded();
		}
		offerPage.setDefaultQuantity(defaultQuantity);
		offerPage.setMinimalQuantity(minimalQuantity);
		offerPage.setMaximalQuantity(maximalQuantity);
		offerPage.setSequence(sequence);
		offerPage.setMandatory();
		if (visible == true) {
			offerPage.setVisible();
			waitPageLoaded();
		}
		waitPageLoaded();
		offerPage.clickOnConfirm();
		waitPageLoaded();
		offerPage.clickOnSaveButton();
		waitPageLoaded();
	}

	/***  ActivateOffer ***/
	public void ActivateOfferMethod() throws InterruptedException {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		offerPage.clickOnActivateOfferButton2();
	}
}