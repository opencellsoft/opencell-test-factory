package com.opencellsoft.tests;

import java.awt.AWTException;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.articles.ArticleMappingLinePage;
import com.opencellsoft.pages.articles.ArticlePage;
import com.opencellsoft.pages.articles.ListArticlesPage;
import com.opencellsoft.pages.articles.NewArticlePage;
import com.opencellsoft.pages.articles.ProductOrServiceMappingPage;
import com.opencellsoft.pages.catalog.discountplans.DiscountPlanPage;
import com.opencellsoft.pages.catalog.discountplans.DiscountPlansListPage;
import com.opencellsoft.pages.catalog.discountplans.NewDiscountLinePage;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.invoices.InvoicePage;
import com.opencellsoft.pages.invoices.InvoicesList;
import com.opencellsoft.pages.invoices.NewInvoiceLinePage;
import com.opencellsoft.utility.Constant;

public class ArticlesTests extends TestBase {

	@Parameters({ "subCategory", "taxClass", "accountingCode" })
	//@Test(priority = 1, retryAnalyzer = RetryAnalyzer.class)
	@Test(priority = 1)
	public void createArticle(String subCategory, String taxClass, String accountingCode) throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		driver.navigate().refresh();
		Thread.sleep(2000);
		waitPageLoaded();
		homePage.clickOnAdministrationButton();
		waitPageLoaded();
		homePage.clickOnGeneralSettings();
		homePage.clickOnArticlesButton();
		ListArticlesPage listArticlesPage = PageFactory.initElements(driver, ListArticlesPage.class);
		waitPageLoaded();
		listArticlesPage.clickOnCreateArticleButton();
		NewArticlePage newArticlePage = PageFactory.initElements(driver, NewArticlePage.class);
		newArticlePage.setArticleCode(Constant.articleCode);
		newArticlePage.setArticleDescription(Constant.articleDescription);
		newArticlePage.selectSubCategory(subCategory);
		newArticlePage.selectTaxClass(taxClass);
		//newArticlePage.selectAccountingCode(accountingCode);
		newArticlePage.clickSaveButton();
		ArticlePage articlePage = PageFactory.initElements(driver, ArticlePage.class);
		waitPageLoaded();
		Assert.assertEquals(articlePage.getArticleCodeInputText(), Constant.articleCode);
		Assert.assertEquals(articlePage.getArticleDescriptionInputText(), Constant.articleDescription);
	}

	@Test(priority = 2)
	public void addProductArticleMappingLine() throws InterruptedException, AWTException {
		ArticleMappingLinePage articleMappingLinePage = PageFactory.initElements(driver, ArticleMappingLinePage.class);
		ArticlePage articlePage = PageFactory.initElements(driver, ArticlePage.class);
		waitPageLoaded();
		articlePage.clickOnMappingSection();
		waitPageLoaded();
		articlePage.clickOnCreateMapping();
		//articleMappingLinePage.setMappingCode(Constant.articleMappingLineCode1);
		//articleMappingLinePage.setMappingDescription(Constant.articleMappingLineDescription1);
		waitPageLoaded();;
		articleMappingLinePage.clickProduct();
		//articleMappingLinePage.addFilterButton();
		//articleMappingLinePage.addFilterByCodeProductButton();
		articleMappingLinePage.setCodeProduct(Constant.productCode);
		waitPageLoaded();
		articleMappingLinePage.clickOnCodeProductFound(Constant.productCode);
		articleMappingLinePage.clickOnSaveMappingButton();		
		Assert.assertEquals(articleMappingLinePage.getTextMessage(), "Element created");
	}

	@Parameters({ "firstAttributeValue" })
	@Test(priority = 3)
	public void setAttributeValueForFirstArticleMappingLine(String firstAttributeValue) throws InterruptedException {
		ArticleMappingLinePage articleMappingLinePage = PageFactory.initElements(driver, ArticleMappingLinePage.class);
		waitPageLoaded();
		articleMappingLinePage.clickOnAddAttributesButton();
		articleMappingLinePage.clickOnAttributeCode(Constant.ListOfTextValuesAttributeCode);
		articleMappingLinePage.setAttributeMappingValue("firstAttributeValue");
		articleMappingLinePage.clickOnSaveMappingButton();
		Assert.assertTrue(articleMappingLinePage.getTextMessage().toLowerCase().contains("element updated"));
		articleMappingLinePage.clickOnGoBackMappingButton();
		ArticlePage articlePage = PageFactory.initElements(driver, ArticlePage.class);
		articlePage.clickSaveArticleButton();
	}

	@Test(priority = 4)
	//@Test(priority = 4, retryAnalyzer = RetryAnalyzer.class)
	public void addSecondProductArticleMappingLine() throws InterruptedException, AWTException {
		ArticlePage articlePage = PageFactory.initElements(driver, ArticlePage.class);
		waitPageLoaded();
		articlePage.clickOnMappingSection();
		waitPageLoaded();
		articlePage.clickOnCreateMapping();
		ArticleMappingLinePage articleMappingLinePage = PageFactory.initElements(driver, ArticleMappingLinePage.class);
		waitPageLoaded();
		articleMappingLinePage.clickProduct();
		//articleMappingLinePage.addFilterButton();
		//articleMappingLinePage.addFilterByCodeProductButton();
		waitPageLoaded();
		articleMappingLinePage.setCodeProduct(Constant.productCode);
		waitPageLoaded();
		articleMappingLinePage.clickOnCodeProductFound(Constant.productCode);
		articleMappingLinePage.clickOnSaveMappingButton();
		Assert.assertEquals(articleMappingLinePage.getTextMessage(), "Element created");
	}

	@Parameters({ "secondAttributeValue" })
	@Test(priority = 5)
	public void setAttributeValueForSecondArticleMappingLine(String secondAttributeValue) throws InterruptedException {
		ArticleMappingLinePage articleMappingLinePage = PageFactory.initElements(driver, ArticleMappingLinePage.class);
		waitPageLoaded();
		articleMappingLinePage.clickOnAddAttributesButton();
		articleMappingLinePage.clickOnAttributeCode(Constant.ListOfTextValuesAttributeCode);
		articleMappingLinePage.setAttributeMappingValue("secondAttributeValue");
		articleMappingLinePage.clickOnSaveMappingButton();
		Assert.assertTrue(articleMappingLinePage.getTextMessage().toLowerCase().contains("element updated"));
		articleMappingLinePage.clickOnGoBackMappingButton();
		ArticlePage articlePage = PageFactory.initElements(driver, ArticlePage.class);
		articlePage.clickSaveArticleButton();
		waitPageLoaded();
	}

	@Test(priority = 6)
	public void addLabelsByLanguage() throws InterruptedException {
		ArticlePage articlePage = PageFactory.initElements(driver, ArticlePage.class);
		articlePage.selectLanguage();
		articlePage.setLabel(Constant.articleLabelByLanguage);
		articlePage.clickSaveArticleButton();
	}

	@Test(priority = 7)
	public void editLabelsByLanguage() throws InterruptedException {
		ArticlePage articlePage = PageFactory.initElements(driver, ArticlePage.class);
		articlePage.editLanguage();
		articlePage.editLabel(Constant.newArticleLabelByLanguage);
		articlePage.clickSaveArticleButton();
		articlePage.selectEmptyCell();
	}

	@Test(priority = 8)
	public void removeLabelsByLanguage() throws InterruptedException {
		ArticlePage articlePage = PageFactory.initElements(driver, ArticlePage.class);
		articlePage.clickOnRemoveLabelsByLanguageButton();
		articlePage.clickSaveArticleButton();
	}

	@Parameters({ "subCategoryUpdated", "taxClassUpdated" })
	@Test(priority = 9)
	public void modifyArticle(String subCategoryUpdated, String taxClassUpdated) throws InterruptedException {
		NewArticlePage newArticlePage = PageFactory.initElements(driver, NewArticlePage.class);
		newArticlePage.setArticleDescriptionUpdated("_updated");
		newArticlePage.selectSubCategory(subCategoryUpdated);
		newArticlePage.selectTaxClass(taxClassUpdated);
		newArticlePage.clickSaveButton();
		// ArticlePage articlePage = PageFactory.initElements(driver,
		// ArticlePage.class);
		// Assert.assertTrue(articlePage.getTextMessage().toLowerCase().contains("element
		// updated"));
		// Assert.assertTrue(articlePage.getArticleCodeInputText().equalsIgnoreCase(Constant.articleCode
		// + "_updated"));
	}

	@Test(priority = 9)
	public void checkGoBackArticleMapping() throws InterruptedException {
		ProductOrServiceMappingPage productOrServiceMappingPage = PageFactory.initElements(driver,
				ProductOrServiceMappingPage.class);
		productOrServiceMappingPage.clickOnGoBackMappingButton();
	}

	@Parameters({ "offerCodeUpdated", "productCodeUpdated", "chargeNameUpdated" })
	@Test(priority = 10)
	public void modifyArticleMappingLine(String offerCodeUpdated, String productCodeUpdated, String chargeNameUpdated)
			throws InterruptedException {
		ArticlePage articlePage = PageFactory.initElements(driver, ArticlePage.class);
		articlePage.clickOnMappingSection();
		articlePage.selectMapping();
		ProductOrServiceMappingPage productOrServiceMappingPage = PageFactory.initElements(driver,
				ProductOrServiceMappingPage.class);
		productOrServiceMappingPage.setMappingDescriptionUpdated("_Updated");
		productOrServiceMappingPage.clickOffer();
		productOrServiceMappingPage.addFilterButton();
		productOrServiceMappingPage.addFilterByCodeOfferButton();
		productOrServiceMappingPage.setCodeOffer(offerCodeUpdated);
		productOrServiceMappingPage.clickOnCodeOfferFound(offerCodeUpdated);
		productOrServiceMappingPage.clickProduct();
		productOrServiceMappingPage.addFilterButton();
		productOrServiceMappingPage.addFilterByCodeProductButton();
		productOrServiceMappingPage.setCodeProduct(productCodeUpdated);
		productOrServiceMappingPage.clickOnCodeProductFound(productCodeUpdated);
		productOrServiceMappingPage.clickCharge();
		productOrServiceMappingPage.addFilterButton();
		productOrServiceMappingPage.addFilterByNameChargeButton();
		productOrServiceMappingPage.setNameCharge(chargeNameUpdated);
		productOrServiceMappingPage.clickOnNameChargeFound(chargeNameUpdated);
		productOrServiceMappingPage.clickOnSaveMappingButton();
		productOrServiceMappingPage.clickOnGoBackMappingButton();
		// Assert.assertTrue(productOrServiceMappingPage.getTextMessage().toLowerCase().contains("element
		// updated"));
		// Assert.assertTrue(productOrServiceMappingPage.getMappingDescriptionInputText()
		// .equalsIgnoreCase(Constant.mappingDescription + "_Updated"));
	}

	@Test(priority = 11)
	public void createChargeArticleMappingLine() throws InterruptedException, AWTException {
		ArticlePage articlePage = PageFactory.initElements(driver, ArticlePage.class);
		articlePage.clickOnMappingSection();
		articlePage.clickOnCreateMapping();
		ProductOrServiceMappingPage productOrServiceMappingPage = PageFactory.initElements(driver,
				ProductOrServiceMappingPage.class);
		productOrServiceMappingPage.setMappingCode(Constant.articleMappingLineCode);
		Assert.assertTrue(productOrServiceMappingPage.getArticleMappingLineCodeInputText()
				.equalsIgnoreCase(Constant.articleMappingLineCode));
		productOrServiceMappingPage.setMappingDescription(Constant.articleMappingLineDescription);
		Assert.assertTrue(productOrServiceMappingPage.getArticleMappingLineDescriptionInputText()
				.equalsIgnoreCase(Constant.articleMappingLineDescription));
		productOrServiceMappingPage.clickCharge();
		productOrServiceMappingPage.addFilterButton();
		productOrServiceMappingPage.addFilterByNameChargeButton();
		productOrServiceMappingPage.setNameCharge(Constant.oneShotChargeCode);
		Assert.assertTrue(
				productOrServiceMappingPage.getChargeNameInputText().equalsIgnoreCase(Constant.oneShotChargeCode));
		productOrServiceMappingPage.clickOnNameChargeFound(Constant.oneShotChargeCode);
		productOrServiceMappingPage.clickOnSaveMappingButton();
		Assert.assertTrue(productOrServiceMappingPage.getTextMessage().toLowerCase().contains("element created"));
	}

	@Test(priority = 12)
	public void deleteArticleMappingLine() throws InterruptedException {
		ProductOrServiceMappingPage productOrServiceMappingPage = PageFactory.initElements(driver,
				ProductOrServiceMappingPage.class);
		productOrServiceMappingPage.clickOnDeleteButton();
		productOrServiceMappingPage.clickOnConfirmDeleteButton();
	}

	@Test(priority = 13)
	public void createOfferArticleMappingLine() throws InterruptedException, AWTException {

		ProductOrServiceMappingPage productOrServiceMappingPage = PageFactory.initElements(driver,
				ProductOrServiceMappingPage.class);
		ArticlePage articlePage = PageFactory.initElements(driver, ArticlePage.class);
		articlePage.clickOnMappingSection();
		articlePage.clickOnCreateMapping();
		productOrServiceMappingPage.setMappingCode(Constant.articleMappingLineCode);
		Assert.assertTrue(productOrServiceMappingPage.getArticleMappingLineCodeInputText()
				.equalsIgnoreCase(Constant.articleMappingLineCode));
		productOrServiceMappingPage.setMappingDescription(Constant.articleMappingLineDescription);
		Assert.assertTrue(productOrServiceMappingPage.getArticleMappingLineDescriptionInputText()
				.equalsIgnoreCase(Constant.articleMappingLineDescription));
		productOrServiceMappingPage.clickOffer();
		productOrServiceMappingPage.addFilterButton();
		productOrServiceMappingPage.addFilterByCodeOfferButton();
		productOrServiceMappingPage.setCodeOffer(Constant.offerCode);
		Assert.assertTrue(productOrServiceMappingPage.getOfferCodeInputText().equalsIgnoreCase(Constant.offerCode));
		productOrServiceMappingPage.clickOnCodeOfferFound(Constant.offerCode);
		productOrServiceMappingPage.clickOnSaveMappingButton();
		Assert.assertTrue(productOrServiceMappingPage.getTextMessage().toLowerCase().contains("element created"));
		productOrServiceMappingPage.clickOnGoBackMappingButton();
	}

	@Test(priority = 14)
	public void checkGoBackArticleScreen() {
		ArticlePage articlePage = PageFactory.initElements(driver, ArticlePage.class);
		articlePage.clickOnGoBackButton();
	}

	@Test(priority = 15)
	public void searchForArticleWithDefaultFilter() throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnAdministrationButton();
		ListArticlesPage listArticlesPage = PageFactory.initElements(driver, ListArticlesPage.class);
		listArticlesPage.setArticleCode(Constant.articleCode);
		listArticlesPage.clickOnArticle(Constant.articleCode);
	}

	@Test(priority = 16)
	public void checkSortingArticlesScreen() {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnAdministrationButton();
		ListArticlesPage listArticlesPage = PageFactory.initElements(driver, ListArticlesPage.class);
		listArticlesPage.clikOnSortByCodeButton();
	}

	@Test(priority = 17)
	public void exportTheListOfArticles() {

		ListArticlesPage listArticlesPage = PageFactory.initElements(driver, ListArticlesPage.class);
		listArticlesPage.clickOnExportArticlesListButton();
	}

	@Test(priority = 18)
	public void listTheLatestArticlesViewed() {

		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnAdministrationButton();

		ListArticlesPage listArticlesPage = PageFactory.initElements(driver, ListArticlesPage.class);
		listArticlesPage.clickOnLatestArticlesViewed();
		listArticlesPage.clickOnCancelLatestArticlesViewedButton();
	}

	@Parameters({"articleCode", "customerCode", "invoiceQuantity", "invoicePrice" })
	@Test(priority = 19)
	public void assignArticleToInvoiceLine(String articleCode,String customerCode, String invoiceQuantity, String invoicePrice)
			throws InterruptedException, AWTException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCustomerCareButton();
		homePage.clickOnInvoicesButton();
		InvoicesList invoicesList = PageFactory.initElements(driver, InvoicesList.class);
		invoicesList.clickOnAddFilterButton();
		invoicesList.clickOnCustomerFilter();
		invoicesList.searchForCustomer(customerCode);
		Thread.sleep(1000);
		invoicesList.selectCustomer();
		// invoicesList.clickOnCustomer(customerCode);
		InvoicePage invoicePage = PageFactory.initElements(driver, InvoicePage.class);
		invoicePage.clickOnCreateInvoiceLine();
		NewInvoiceLinePage newInvoiceLinePage = PageFactory.initElements(driver, NewInvoiceLinePage.class);
		newInvoiceLinePage.setArticle(articleCode);
		newInvoiceLinePage.setInvoiceLineDescription(Constant.invoiceLineDescription);
		newInvoiceLinePage.setInvoiceLineQuantity(invoiceQuantity);
		newInvoiceLinePage.setInvoiceLinePrice(invoicePrice);
		newInvoiceLinePage.clickOnSaveInvoiceLineButton();
		Assert.assertTrue(newInvoiceLinePage.getTextMessage().toLowerCase().contains("element created"));
		newInvoiceLinePage.clickGoBack();
	}

	@Parameters({ "discountPlansCode" })
	@Test(priority = 20)
	public void assignArticleToDiscountLine(String discountPlansCode) throws InterruptedException {
		NewInvoiceLinePage newInvoiceLinePage = PageFactory.initElements(driver, NewInvoiceLinePage.class);
		newInvoiceLinePage.clickGoBack();
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCatalogButton();
		homePage.clickOnDiscountPlansButton();
		DiscountPlansListPage discountPlansListPage = PageFactory.initElements(driver, DiscountPlansListPage.class);
		discountPlansListPage.clickOnAddDiscountPlanFilterButton();
		discountPlansListPage.clickOnFilterByCodeOption();
		discountPlansListPage.setDiscountPlanCode(discountPlansCode);
		Thread.sleep(1000);
		discountPlansListPage.selectdiscountPlans();
		// discountPlansListPage.clickOnDiscountPlansFound(discountPlansCode);

		DiscountPlanPage discountPlanPage = PageFactory.initElements(driver, DiscountPlanPage.class);
		discountPlanPage.clickOnCreateDiscountLineButton();

		NewDiscountLinePage discountLinePage = PageFactory.initElements(driver, NewDiscountLinePage.class);
		discountLinePage.setDiscountLineCode(Constant.discountLineCode);
		Assert.assertTrue(discountLinePage.getDiscountLineCodeInputText().equalsIgnoreCase(Constant.discountLineCode));
		discountLinePage.clickDiscountArticle();
		discountLinePage.clickOnAddDiscountArticleFilterButton();
		discountLinePage.clickOnDiscountArticleCodeFilterButton();
		discountLinePage.searchForDiscountArticleCodeInput(Constant.articleCode);
		Assert.assertTrue(discountLinePage.getArticleCodeInputText().equalsIgnoreCase(Constant.articleCode));
		discountLinePage.selectDiscountArticle();
		discountLinePage.clickOnSaveDiscountLineButton();
		Assert.assertTrue(discountLinePage.getTextMessage().toLowerCase().contains("element created"));
	}

	@Test(priority = 21)
	public void searchForArticleWithAdvancedFilters() throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnAdministrationButton();

		ListArticlesPage listArticlesPage = PageFactory.initElements(driver, ListArticlesPage.class);
		listArticlesPage.clickOnFilterButton();
		listArticlesPage.clickOnFilterOption();
		listArticlesPage.setFilter(Constant.articleDescription);
		listArticlesPage.clickOnArticle(Constant.articleDescription);
	}

	@Test(priority = 22)
	public void deleteArticle() {
		ArticlePage articlePage = PageFactory.initElements(driver, ArticlePage.class);
		articlePage.clickOnDeleteButton();
		articlePage.clickOnConfirmButton();
	}
}
