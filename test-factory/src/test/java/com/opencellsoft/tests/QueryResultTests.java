package com.opencellsoft.tests;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.operation.querytool.QueryResultsPage;

public class QueryResultTests extends TestBase {

	QueryResultsPage queryResultsPage;
	
	public void searchQueryByName(String value) {
		queryResultsPage = new QueryResultsPage();
		queryResultsPage.setSearchInput(value);
	}
	
	public void sortQueryListBy(String value) {
		queryResultsPage = new QueryResultsPage();
		queryResultsPage.sortBy(value);
	}
	
	public int queriesTotal() {
		queryResultsPage = new QueryResultsPage();
		return queryResultsPage.queriesList().size();
	}
	
	public void deleteQuery() {
		queryResultsPage = new QueryResultsPage();
		queryResultsPage.clickOnTheFirstLine();
		queryResultsPage.clickOnDeleteButton();
		queryResultsPage.clickOnConfirmButton();
		
	}
	
	public void deleteAllQueries() {
		queryResultsPage = new QueryResultsPage();
		queryResultsPage.selectAllList();
		queryResultsPage.clickOnDeleteButton();
		queryResultsPage.clickOnConfirmButton();
		
	}
	
	public void downloadQueryResult(int i) {
		queryResultsPage = new QueryResultsPage();
		queryResultsPage.clickOnDownload(i);
	}
	
	public String messageText() {
		queryResultsPage = new QueryResultsPage();
		return queryResultsPage.messageText();
	}
}
