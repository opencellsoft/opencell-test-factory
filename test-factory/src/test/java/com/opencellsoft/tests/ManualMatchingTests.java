package com.opencellsoft.tests;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.finance.manualmatching.ManualMatchingPage;

public class ManualMatchingTests extends TestBase {

	ManualMatchingPage manualMatchingPage;

	public void createAO (String paymentMethod,String customer,String currency, String amount, String reference, String version) {
		manualMatchingPage = new ManualMatchingPage();
		if(!version.equals("15.X")) {manualMatchingPage.clickOnActionButton();}
		try {
			manualMatchingPage.clickOnCreateAOButton();
		}catch (Exception e) {
			if(!version.equals("15.X")) {manualMatchingPage.clickOnActionButton();}
			manualMatchingPage.clickOnCreateAOButton();
		}

		manualMatchingPage.setPaymentMethod(paymentMethod);
		manualMatchingPage.setCustomer(customer);
		manualMatchingPage.setCurrency(currency);
		manualMatchingPage.setAmount(amount);
		manualMatchingPage.setReference(reference);
		manualMatchingPage.clickOnValidateButton();
	}

	public void activateTranferAOMode ( String version) {
		manualMatchingPage = new ManualMatchingPage();
		if(!version.equals("15.X")) {
			manualMatchingPage.clickOnActionButton();
			manualMatchingPage.clickOnTranferAOMode();}
		else {
			manualMatchingPage.clickOnTranferAOButton();
		}

	}

	public void transferAO (String customerCode, String version) {
		manualMatchingPage = new ManualMatchingPage();
		if(!version.equals("15.X")) {
			manualMatchingPage.clickOnActionButton();
			manualMatchingPage.clickOnTranferAOButton();
		}
		else {
			manualMatchingPage.clickOnTranferButton();
		}
		manualMatchingPage.setCustomerName(customerCode);
		manualMatchingPage.clickOnValidateButton();
	}

	public void searchCreditAccountOperations(String customerCode) {
		manualMatchingPage = new ManualMatchingPage();
		manualMatchingPage.setCreditAccountOperationsSearchInput(customerCode);
	}

	public void searchDebitAccountOperations(String customerCode) {
		manualMatchingPage = new ManualMatchingPage();
		manualMatchingPage.setDebitAccountOperationsSearchInput(customerCode);
	}

	public int elementsFoundSizeDebitAOs(String value) {
		manualMatchingPage = new ManualMatchingPage();
		return manualMatchingPage.elementsFoundSizeDebitAOs(value);
	}

	public int elementsFoundSizeCreditAOs(String value) {
		manualMatchingPage = new ManualMatchingPage();
		return manualMatchingPage.elementsFoundSizeCreditAOs(value);
	}

	public void matchAO() {
		manualMatchingPage = new ManualMatchingPage();
		manualMatchingPage.clickOnMatchButton();
		manualMatchingPage.clickOnConfirmButton();
	}

	public void checkFirstDebitAccountOperation() {
		manualMatchingPage = new ManualMatchingPage();
		manualMatchingPage.selectFirstDebitAO();
	}

	public void checkFirstCreditAccountOperation() {
		manualMatchingPage = new ManualMatchingPage();
		manualMatchingPage.selectFirstCreditAO();
	}

	public void matchAOPartially() {
		manualMatchingPage = new ManualMatchingPage();
		manualMatchingPage.clickOnMatchPartiallyButton();
		manualMatchingPage.clickOnConfirmButton();
	}

	public void activateFilterDebitBy(String value) {
		manualMatchingPage = new ManualMatchingPage();
		manualMatchingPage.clickOnFilterButton();
		manualMatchingPage.clickOnFilterChoise(value);
	}

	public void filterDebitAOByStatus(String value) {
		manualMatchingPage = new ManualMatchingPage();
		manualMatchingPage.clickOnStatusSearchInput();
		manualMatchingPage.clickOnStatusChoise(value);
	}

}
