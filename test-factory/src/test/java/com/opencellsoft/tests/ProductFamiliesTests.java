package com.opencellsoft.tests;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.catalog.catalogmanager.productfamilies.ProductFamiliesListPage;
import com.opencellsoft.pages.catalog.catalogmanager.productfamilies.ProductFamiliesPage;

public class ProductFamiliesTests extends TestBase {
	
	ProductFamiliesListPage productFamiliesListPage;
	ProductFamiliesPage productFamiliesPage;
	
	public void createProductFamilies(String pf_code, String pf_name, String pf_description, String pf_seller, String pf_parent) {
		productFamiliesListPage= new ProductFamiliesListPage();
		productFamiliesListPage.clickOnCreateButton();
		
		productFamiliesPage = new ProductFamiliesPage();
		productFamiliesPage.setCode(pf_code);
		productFamiliesPage.setName(pf_name);
		productFamiliesPage.setDescription(pf_description);
		productFamiliesPage.selectSeller(pf_seller);
		productFamiliesPage.selectParent(pf_parent);
		productFamiliesPage.clickOnSaveButton();
	}
	
	public void updateProductFamilies(String pf_name, String pf_description, String pf_seller, String pf_parent) {
		productFamiliesPage = new ProductFamiliesPage();
		if(pf_name != null) {productFamiliesPage.setName(pf_name);}
		if(pf_description != null) {productFamiliesPage.setDescription(pf_description);}
		if(pf_seller != null) {productFamiliesPage.selectSeller(pf_seller);}
		if(pf_parent != null) {productFamiliesPage.selectParent(pf_parent);}
		productFamiliesPage.clickOnSaveButton();
	}
	
	public void delete() {
		productFamiliesPage = new ProductFamiliesPage();
		productFamiliesPage.clickOnDeleteButton();
		productFamiliesPage.clickOnConfirmButton();
	}
	
	public void goBack() {
		productFamiliesPage = new ProductFamiliesPage();
		productFamiliesPage.clickOnGoBackButton();
	}
	
	public void searchProductFamiliesByName(String value) {
		productFamiliesListPage = new ProductFamiliesListPage();
		productFamiliesListPage.setSearchInput(value);
	}
	
	public int productFamiliesTotal() {
		productFamiliesListPage= new ProductFamiliesListPage();
		return productFamiliesListPage.productFamiliesList().size();
	}
	
	public void goToProductFamiliesDetails (String email) {
		productFamiliesListPage= new ProductFamiliesListPage();
		productFamiliesListPage.goToProductFamiliesDetails(email);
	}
	public void activateFilterBy(String value1, String value2) {
		productFamiliesListPage= new ProductFamiliesListPage();
		productFamiliesListPage.clickOnFilterButton();
		try {
			productFamiliesListPage.activateFilterBy(value1);
		}catch (Exception e) {
			productFamiliesListPage.activateFilterBy(value2);
		}
		
	}
	
	public void filterByProductFamiliesName(String value) {
		productFamiliesListPage= new ProductFamiliesListPage();
		productFamiliesListPage.setFilterByProductFamiliesName(value);
	}
	
	public void sortProductFamiliesListBy(String value) {
		productFamiliesListPage= new ProductFamiliesListPage();
		productFamiliesListPage.sortBy(value);
	}
	
	public void closeFilterBy(String value) {
		productFamiliesListPage= new ProductFamiliesListPage();
		productFamiliesListPage.clickOnRemoveFilterBy(value);
	}
}