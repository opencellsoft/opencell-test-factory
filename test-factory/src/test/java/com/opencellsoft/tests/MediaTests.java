package com.opencellsoft.tests;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.catalog.catalogmanager.media.MediaListPage;
import com.opencellsoft.pages.catalog.catalogmanager.media.MediaPage;

public class MediaTests extends TestBase {

	MediaListPage mediaListPage;
	MediaPage mediaPage;
	
	
	public void createMedia(String media_Type, String media_Code,String media_description,String mediaURL) {
		mediaListPage = new MediaListPage();
		mediaListPage.clickOnCreateButton();
		
		mediaPage = new MediaPage();
		mediaPage.setType(media_Type);
		mediaPage.setCode(media_Code);
		mediaPage.setDescription(media_description);
		mediaPage.setUrlPath(mediaURL);

		mediaPage.clickOnSaveButton();
	}
	
	public void updateMedia(String attributeGroup_description) {
		mediaPage = new MediaPage();
		if(attributeGroup_description != null) {mediaPage.setDescription(attributeGroup_description);}

		mediaPage.clickOnSaveButton();
	}
	
	public void goBack() {
		mediaPage = new MediaPage();
		mediaPage.clickOnGoBackButton();
	}
	
	public void searchMediaByCode(String value) {
		mediaListPage= new MediaListPage();
		mediaListPage.setSearchInput(value);
	}
	
	public int MediaTotal() {
		mediaListPage= new MediaListPage();
		return mediaListPage.MediaList().size();
	}
	
	public void goToMediaDetails (String email) {
		mediaListPage= new MediaListPage();
		mediaListPage.goToMediaDetails(email);
	}
	public void activateFilterBy(String value) {
		mediaListPage= new MediaListPage();
		mediaListPage.clickOnFilterButton();
		mediaListPage.activateFilterBy(value);
	}
	
	public void sortMediaListBy(String value) {
		mediaListPage= new MediaListPage();
		mediaListPage.sortBy(value);
	}
	
	public void closeFilterBy(String value) {
		mediaListPage= new MediaListPage();
		mediaListPage.clickOnRemoveFilterBy(value);
	}
	
	public void filterMediaByDescription(String value) {
		mediaListPage= new MediaListPage();
		mediaListPage.setDescription(value);
	}
	
}
