package com.opencellsoft.tests;

import java.awt.AWTException;
import java.util.List;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Ignore;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.catalog.catalogmanager.attributes.AttributePage;
import com.opencellsoft.pages.catalog.catalogmanager.attributes.NewAttributePage;
import com.opencellsoft.pages.catalog.catalogmanager.media.NewMediaPage;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.offers.ListOfOffersPage;
import com.opencellsoft.pages.offers.OfferPage;
import com.opencellsoft.pages.products.ListOfProductsPage;
import com.opencellsoft.pages.products.NewProductCommercialRulePage;
import com.opencellsoft.pages.products.NewProductPage;
import com.opencellsoft.pages.products.ProductPage;
import com.opencellsoft.utility.Constant;

public class ProductsTests extends TestBase {

	//@Test(priority = 1, retryAnalyzer = RetryAnalyzer.class, groups = { "smokeTest", "regressionTest","functionalTest" })
	@Test(priority = 1, groups = { "smokeTest", "regressionTest","functionalTest" })
	public void createProduct() throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		waitPageLoaded();
		Thread.sleep(5000);
		homePage.clickOnCatalogButton();
		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		waitPageLoaded();
		//homePage.clickOnCatalogButtonMenu();
		offerListPage.clickOnProductsButton();
		ListOfProductsPage listOfProductsPage = PageFactory.initElements(driver, ListOfProductsPage.class);
		waitPageLoaded();
		listOfProductsPage.clickOnCreateProductButton();
		NewProductPage newProductPage = PageFactory.initElements(driver, NewProductPage.class);
		waitPageLoaded();
		newProductPage.setProductCode(Constant.productCode);
		newProductPage.setProductLabel(Constant.productDescription);
		newProductPage.clickOnPricingTab();
		newProductPage.selectPriceVersionDateSetting("Quote date");
		waitPageLoaded();
		newProductPage.clickOnSaveProductButton();
		waitPageLoaded();
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		Assert.assertEquals(productPage.getTextMessage(), "Element created");

	}

	@Test(priority = 2)
	public void addDiscountToProduct() throws InterruptedException {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnDiscountsButton();
		productPage.clickOnDiscountsList();
		productPage.clickOnAddFilterButton();
		productPage.clickOnAddFilterByCode();
		productPage.setDiscountCode(Constant.discountPlanCode);
		Thread.sleep(1000);
		productPage.selectDiscountPlan();
		productPage.clickOnSaveButton();
		Assert.assertEquals(productPage.getTextMessage(), "Element updated");
	}

	@Parameters({ "firstAttributeValue", "secondAttributeValue" })
	@Test(priority = 2, groups = { "smoke tests" })
	public void addListOfTextValuesAttributeToProduct(String firstAttributeValue, String secondAttributeValue)
			throws AWTException, InterruptedException {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnAttributesTab();
		waitPageLoaded();
		productPage.clickOnCreateAttributeButton();
		NewAttributePage newAttributePage = PageFactory.initElements(driver, NewAttributePage.class);
		newAttributePage.selectAttributeType("List of text values");
		newAttributePage.setAttributeCode(Constant.ListOfTextValuesAttributeCode);
		newAttributePage.setAttributeDescription(Constant.ListOfTextValuesAttributeDescription);
		newAttributePage.clickOnAddAttributeValuesButton();
		newAttributePage.setFirstAttributeValue(firstAttributeValue);
		newAttributePage.clickOnAddAttributeValuesButton();
		newAttributePage.setSecondAttributeValue(secondAttributeValue);
		newAttributePage.clickOnSaveAttributeButton();
		waitPageLoaded();
		AttributePage attributePage = PageFactory.initElements(driver, AttributePage.class);
		Assert.assertEquals(attributePage.getAttributeDescriptionInputText(),
				Constant.ListOfTextValuesAttributeDescription);
		waitPageLoaded();
		newAttributePage.clickOnGoBackButton();
		waitPageLoaded();
		productPage.clickOnSaveButton();
		Assert.assertEquals(productPage.getTextMessage(), "Element updated");
	}

	@Parameters({ "attributeDefaultValue" })
	@Test(priority = 4)
	public void setAttributeDefaultValue(String attributeDefaultValue) throws InterruptedException {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		//productPage.clickOnAttributesTab();
		productPage.clickOnCreatedAttributeParameters();
		productPage.setAttributeDefaultValue(attributeDefaultValue);
		productPage.clickOnConfirmButton();
		productPage.clickOnSaveButton();
		Assert.assertEquals(productPage.getTextMessage(), "Element updated");
	}

	@Test(priority = 5, groups = { "smoke tests" })
	public void addTextAttributeToProduct() throws AWTException, InterruptedException {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnAttributesTab();
		productPage.clickOnAddAttributeButton();
		NewAttributePage newAttributePage = PageFactory.initElements(driver, NewAttributePage.class);
		newAttributePage.selectAttributeType("Text value");
		newAttributePage.setAttributeCode(Constant.textAttributeCode);
		newAttributePage.setAttributeDescription(Constant.textAttributeDescription);
		newAttributePage.clickOnSaveAttributeButton();
		waitPageLoaded();
		AttributePage attributePage = PageFactory.initElements(driver, AttributePage.class);
		Assert.assertEquals(attributePage.getAttributeDescriptionInputText(), Constant.textAttributeDescription);
		newAttributePage.clickOnGoBackButton();
		productPage.clickOnSaveButton();
		Assert.assertEquals(productPage.getTextMessage(), "Element updated");
	}

	@Test(priority = 6)
	public void setAttributeParameters() throws InterruptedException, AWTException {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnAttributesTab();
		productPage.clickOnTextAttributeParametersButton();
		productPage.toggleMandatoryAttributeOn();
		productPage.toggleReadOnlyAttributeOn();
		productPage.clickOnConfirmButton();
		productPage.clickOnSaveButton();
		Assert.assertEquals(productPage.getTextMessage(), "Element updated");
	}

	@Test(priority = 4)
	public void addMediaToProduct() {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnMediaButton();
		productPage.clickOnCreateMediaButton();
		NewMediaPage newMediaPage = PageFactory.initElements(driver, NewMediaPage.class);
		newMediaPage.selectMediaTypeImage();
		newMediaPage.setMediaCode(Constant.mediaCode);
		newMediaPage.setMediaDescription(Constant.mediaDescription);
		newMediaPage.setURLPath(Constant.urlPathMedia);
		newMediaPage.clickOnSubmitButton();
		newMediaPage.clickOnGoBackButton();
		productPage.clickOnSaveButton();
		Assert.assertTrue(productPage.getTextMessage().toLowerCase().contains("element updated"));
	}

	@Test(priority = 14, groups = {
	"smoke tests" }, dependsOnMethods = "com.opencellsoft.tests.ChargesTests.activateRecurringChargeWithPriceGrid")
	public void publishProductVersion() {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		waitPageLoaded();
		productPage.clickOnPublishProductVersionButton();
		waitPageLoaded();
		//Assert.assertEquals(productPage.getTextMessage(), "Action executed successfuly");
	}

	@Test(priority = 15, groups = { "smoke tests" }, dependsOnMethods = "publishProductVersion")
	public void productStatusFromDraftToActivated() throws InterruptedException {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		waitPageLoaded();
		productPage.activateProduct();
		waitPageLoaded();
		Assert.assertEquals(productPage.getTextMessage(), "Action executed successfuly");	
	}

	//	@Test(priority = 3, dependsOnMethods = "com.opencellsoft.tests.catalog.AttributesTests.createTextValueAttributeForArticle")
	@Test(priority = 3)
	public void publishProductVersionForArticlesTest() throws InterruptedException {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		waitPageLoaded();
		productPage.clickOnPublishProductVersionButton();
		// Thread.sleep(1000);
		//		Assert.assertTrue(productPage.getTextMessage().toLowerCase().contains("action executed successfuly"));

	}

	@Test(priority = 4, groups = { "smoke tests" }, dependsOnMethods = "publishProductVersionForArticlesTest")
	public void productStatusFromDraftToActivatedForArticlesTest() throws InterruptedException {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		Thread.sleep(1000);
		productPage.activateProduct();
		Thread.sleep(1000);
		Assert.assertTrue(productPage.getTextMessage().toLowerCase().contains("action executed successfuly"));
	}

	@Ignore
	@Test(priority = 2)
	public void createComercialRule() {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnCommercialRulesButton();
		productPage.clickOnCreateRuleButton();
		NewProductCommercialRulePage newProductCommercialRulePage = PageFactory.initElements(driver,
				NewProductCommercialRulePage.class);
		newProductCommercialRulePage.setcommercialRulesInformation(Constant.commercialRuleCode,
				Constant.commercialRuleDescription);
	}

	//	@Test(priority = 4)
	//	public void addDiscountToProduct() {
	//		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
	//		productPage.addDiscount();
	//	}

	@Ignore
	@Test(priority = 5)
	public void addTagToProduct() {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnTagTab();
		productPage.clickOntagDropDown();
		productPage.selectTag();
		productPage.clickOnSaveTagButton();
	}

	@Ignore
	@Test(priority = 6)
	public void createProductVersion() {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnCreateProductVersionButton();
		productPage.setProductVersionExternalDescription(Constant.productExternalDescription);
		productPage.clickOnSaveButton();
	}

	@Ignore
	@Test(priority = 7)
	public void duplicateProductVersion() {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnDuplicateProductVersionButton();
	}

	@Ignore
	@Test(priority = 9)
	public void activateProduct() {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.activateProduct();
	}

	@Ignore
	@Test(priority = 10)
	public void addExistingCharge() {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.clickOnChargesTab();
		productPage.clickOnPickChargeButton();
		productPage.selectChargeTemplate();
		productPage.clickOnSaveButton();
	}

	@Ignore
	@Test(priority = 12)
	public void productStatusFromDraftToClosed() {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.closeProduct();
	}

	@Ignore
	@Test(priority = 14)
	public void productStatusFromActivatedToClosed() {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.closeProduct();
	}

	@Ignore
	@Parameters({ "productDescription" })
	@Test(priority = 15)
	public void searchProductWithDefaultFilter(String productDescription) {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCatalogButton();

		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnProductsButton();

		ListOfProductsPage listOfProductsPage = PageFactory.initElements(driver, ListOfProductsPage.class);
		listOfProductsPage.searchForProduct(productDescription);

	}

	@Ignore
	@Parameters({ "productCode" })
	@Test(priority = 16)
	public void searchProductWithAdvacedFilters(String productCode) {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCatalogButton();

		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnProductsButton();

		ListOfProductsPage listOfProductsPage = PageFactory.initElements(driver, ListOfProductsPage.class);
		listOfProductsPage.advancedSearchForProduct(productCode);
	}

	@Ignore
	@Test(priority = 17)
	public void listLatestProductsViewedByUser() {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCatalogButton();

		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnProductsButton();

		ListOfProductsPage listOfProductsPage = PageFactory.initElements(driver, ListOfProductsPage.class);
		listOfProductsPage.listLatest();
	}

	@Ignore
	@Test(priority = 18)
	public void sortProducts() {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCatalogButton();

		ListOfOffersPage offerListPage = PageFactory.initElements(driver, ListOfOffersPage.class);
		offerListPage.clickOnProductsButton();

		ListOfProductsPage listOfProductsPage = PageFactory.initElements(driver, ListOfProductsPage.class);
		listOfProductsPage.clickOnSortButton();
	}

	@Ignore
	@Test(priority = 19)
	public void closeProductVersion() {
		ProductPage productpage = PageFactory.initElements(driver, ProductPage.class);
		productpage.closeProductVersion();
		Assert.assertTrue(productpage.getTextMessage().toLowerCase().contains("action executed successfuly"));
	}

	/***  search For Product Method  ***/
	public void searchForProductMethod(String productCode,String productDescription ) {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		homePage.clickOnCatalogButtonMenu2();
		waitPageLoaded();
		homePage.clickOnProductButton();
		waitPageLoaded();
		ListOfProductsPage listOfProductsPage = PageFactory.initElements(driver, ListOfProductsPage.class);
		listOfProductsPage.searchForProductByDescription(productCode, productDescription);
	}

	/***  Create a new product from offer page  ***/
	public void createProductMethod(String PriceVersionDate,String productCode, String productDescription ) throws InterruptedException, AWTException {
		OfferPage offerPage = PageFactory.initElements(driver, OfferPage.class);
		NewProductPage newProductPage = PageFactory.initElements(driver, NewProductPage.class);
		try {
			offerPage.clickOnCreateProductButton();
		}catch (Exception e) {
			newProductPage.clickOnSaveButton();
			offerPage.clickOnCreateProductButton();
		}

		waitPageLoaded();
		newProductPage.setProductCode(productCode);
		newProductPage.setProductLabel(productDescription);
		newProductPage.clickOnPricingTab();
		waitPageLoaded();
		newProductPage.selectPriceVersionDateSetting(PriceVersionDate);
		newProductPage.clickOnSaveButton();
	}

	/*** Activate the product and go back to the offer  ***/
	public void activateProductAndGoBack(){
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		productPage.activateProduct2();
		waitPageLoaded();
		productPage.clickOnReturnToOffer();
		waitPageLoaded();
	}

	/*** create Product Attribut   ***/
	public void createProductAttribut(String code, String description, String type, String NumberOfDicimals, String[] values) throws AWTException {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		try {
			productPage.clickOnAttributesTab();
			productPage.clickOnAttributesTab();
		}catch (Exception e) {
			waitPageLoaded();
			productPage.clickOnAttributesTab();
		}
		waitPageLoaded();
		productPage.clickOnCreateAttributeButton2();
		NewAttributePage newAttributePage = PageFactory.initElements(driver, NewAttributePage.class);
		newAttributePage.selectAttributeType2(type);
		newAttributePage.setAttributeCode(code);
		newAttributePage.setAttributeDescription(description);
		if(NumberOfDicimals!=null) {newAttributePage.setNumberOfDecimals(NumberOfDicimals);}
		waitPageLoaded();
		newAttributePage.clickOnSaveAttributeButton();
		waitPageLoaded();
		if(values != null){
			int i = 0;
			for (String item : values) {
				newAttributePage.clickOnAddValue();
				newAttributePage.setValue(i,item);
				i++;
			}
			newAttributePage.clickOnSaveAttributeButton();	
			waitPageLoaded();
		}
		newAttributePage.clickOnGoBackButton();
		waitPageLoaded();
		productPage.clickOnSaveButton();
		waitPageLoaded();
	}

	/*** set Attribute Default Value In Parameter  ***/
	public void setAttributeParameters(String attributeCode, String sequence, String mandatoryWithEL, String defaultValue,String validationType, 
			String validationPattern, String validationLabel, Boolean mandatory, Boolean display , Boolean readOnly) throws InterruptedException {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		//		productPage.clickOnAttributesTab();

		productPage.clickOnAttributeParametersButton(attributeCode);
		if( sequence != null) {productPage.setAttributeSequence(sequence);}
		if( mandatoryWithEL != null) {productPage.setAttributeMandatoryWithEL(mandatoryWithEL);}
		if( defaultValue != null) {productPage.setAttributeDefaultValue(defaultValue);}
		if( validationType != null) {productPage.setAttributeValidationType(validationType);}
		if( validationPattern != null) {productPage.setAttributeValidationPattern(validationPattern);}
		if( validationLabel != null) {productPage.setAttributeValidationLabel(validationLabel);}
		if( mandatory != null) {productPage.setAttributeMandatory(mandatory);}
		if( display != null) {productPage.setAttributeDisplay(display);}
		if( readOnly != null) {productPage.setAttributeReadOnly(readOnly);}
		
		productPage.clickOnConfirmButton();
		waitPageLoaded();
		productPage.clickOnSaveButton();
		waitPageLoaded();
	}

	public void displayAllAttributes(int x) throws InterruptedException {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		//		productPage.clickOnAttributesTab();
		for(int i = 0 ; i<x ; i++) {
			productPage.clickOnAttributeParameterButton(i);
			try {
				productPage.setAttributeDisplay(true);
			}catch (Exception e) {
				productPage.clickOnAttributeParameterButton(i);
				productPage.setAttributeDisplay(true);
			}
			
			productPage.clickOnConfirmButton();
			waitPageLoaded();
		}
		
		productPage.clickOnSaveButton();
		waitPageLoaded();
	}

	/*** pick Attribute to product  ***/
	public void pickAttribute(String attributeCode) {
		ProductPage productPage = PageFactory.initElements(driver, ProductPage.class);
		//		productPage.clickOnAttributesTab();
		productPage.clickOnPickAttributeButton();
		productPage.setAttributeCode(attributeCode);
		productPage.clickOnAttribute(attributeCode);
		productPage.clickOnSaveButton();
	}
}
