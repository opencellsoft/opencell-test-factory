package com.opencellsoft.testsuites.sanity;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.operations.billingrules.InvoiceValidationListPage;
import com.opencellsoft.pages.operations.mediation.CDRListPage;


public class MediationTests extends TestBase {
	
	
	@Test(priority = 1)
	public void listOfCDR() throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		Thread.sleep(5000);
		waitPageLoaded();
		homePage.clickOnOperationMenuButton();
		homePage.clickOnMediationButton();
		homePage.clickOnCDRMenuButton();
	}
	
	@Test(priority = 2)
	public void sortByEventDate() throws InterruptedException {
		CDRListPage cdrListPage = PageFactory.initElements(driver, CDRListPage.class);
		cdrListPage.sortByEventDate();
	}
	
	@Test(priority = 3)
	public void sortByStatus() throws InterruptedException {
		CDRListPage cdrListPage = PageFactory.initElements(driver, CDRListPage.class);
		cdrListPage.sortByStatus();
	}
	
	@Test(priority = 4)
	public void addNewCDR() throws InterruptedException {
		CDRListPage cdrListPage = PageFactory.initElements(driver, CDRListPage.class);
		cdrListPage.clickOnAddButton();
		cdrListPage.clickOnSaveButton();
		cdrListPage.clickOnCloseButton();
		cdrListPage.clickOnGoBackButton();
		
	}
	
	@Test(priority = 5)
	public void importCDR() throws InterruptedException {
		CDRListPage cdrListPage = PageFactory.initElements(driver, CDRListPage.class);
		cdrListPage.clickOnNoteAddIcon();
		cdrListPage.clickOnImportButton();
		cdrListPage.clickOnGoBackButton();
	}

	@Test(priority = 6)
	public void exportCDR() throws InterruptedException {
		CDRListPage cdrListPage = PageFactory.initElements(driver, CDRListPage.class);
		Thread.sleep(2000);
		cdrListPage.clickOnExportButton();

	}

}
