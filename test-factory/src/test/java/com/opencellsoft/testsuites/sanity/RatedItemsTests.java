package com.opencellsoft.testsuites.sanity;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.operations.massadjustments.MassAdjustmentsListPage;
import com.opencellsoft.pages.operations.rating.RatedItemsListPage;
import com.opencellsoft.pages.operations.rating.WalletOperationsListPage;

public class RatedItemsTests extends TestBase {
	
	
	@Test(priority = 1)
	public void simpleFilters() throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		Thread.sleep(5000);
		waitPageLoaded();
		homePage.clickOnOperationMenuButton();
		homePage.clickOnRatingMenuButton();
		homePage.clickOnRatedItemsMenuButton();
		RatedItemsListPage ratedItemsListPage = PageFactory.initElements(driver, RatedItemsListPage.class);
		ratedItemsListPage.checkText("Article_Mapp_Job_V2");
	}
	
	@Test(priority = 2)
	public void advancedFilter() throws InterruptedException {
		RatedItemsListPage ratedItemsListPage = PageFactory.initElements(driver, RatedItemsListPage.class);
		ratedItemsListPage.clickOnAdvancedFilterTab();
	}
	
	@Test(priority = 3)
	public void checkFilter() throws InterruptedException {
		RatedItemsListPage ratedItemsListPage = PageFactory.initElements(driver, RatedItemsListPage.class);
		ratedItemsListPage.clickOnRTJobsReportTab();
	}
	
	@Test(priority = 4)
	public void runRTJob() throws InterruptedException {
		RatedItemsListPage ratedItemsListPage = PageFactory.initElements(driver, RatedItemsListPage.class);
		ratedItemsListPage.clickOnSimpleFiltersTab();
		ratedItemsListPage.clickOnRunJobButton();
		ratedItemsListPage.clickOnRunRTJobButton();
		ratedItemsListPage.clickOnConfirmButton();
		Assert.assertEquals(ratedItemsListPage.getTextMessage(), "Action executed successfuly");
	}
	
	@Test(priority = 5)
	public void runArticleMappingJob() throws InterruptedException {
		RatedItemsListPage ratedItemsListPage = PageFactory.initElements(driver, RatedItemsListPage.class);
		ratedItemsListPage.clickOnRunJobButton();
		ratedItemsListPage.clickOnRunArticleMappingJobButton();
		ratedItemsListPage.clickOnConfirmButton();
		Assert.assertEquals(ratedItemsListPage.getTextMessage(), "Action executed successfuly");
	}
	
	@Test(priority = 6)
	public void adjustmentItemsFilter() throws InterruptedException {
		RatedItemsListPage ratedItemsListPage = PageFactory.initElements(driver, RatedItemsListPage.class);
		ratedItemsListPage.clickOnSimpleFiltersTab();
		ratedItemsListPage.clickOnAdjustmentItemsFilterButton();
		ratedItemsListPage.clickOnConfirmButton();
		Assert.assertEquals(ratedItemsListPage.getTextMessage(), "Action executed successfuly");
	}
	

}
