package com.opencellsoft.testsuites.sanity;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.opencellsoft.tests.MediaTests;
import com.opencellsoft.utility.Constant;

public class MediaSanityTests extends SanityTestsCommons {	
	MediaTests mediaTests;
	String Media_Type = "Image";
	String Media_Code = "Media01-"+ Constant.offerCode;
	String Media_Code2 = "Media02-"+ Constant.offerCode;
	String Media_description = "MediaFirst-"+ Constant.offerCode;
	String mediaUrl = "https://www.capitalgrandest.eu/wp-content/uploads/2020/01/opencell_log-1.jpg";
	
	String Media_description2 = "MediaSecond-"+ Constant.offerCode;
	
	@Test(priority = 0)
	public void mediaSanityTests() {
		mediaTests = new MediaTests();
		System.out.println("**********************************   Media Tests  **********************************");
	}

	/***   create media  ***/
	@Test(priority = 104, enabled = true)
	public void createMedia() throws InterruptedException, AWTException {
		homeTests.goToMediaPage();
		mediaTests.createMedia(Media_Type, Media_Code,Media_description, mediaUrl);
		mediaTests.goBack();
		mediaTests.createMedia(Media_Type, Media_Code2,Media_description, mediaUrl);
		mediaTests.goBack();
	}
	/***   search media By Name  ***/
	@Test(priority = 106, enabled = true)
	public void searchMediaByName() throws InterruptedException, AWTException {
		mediaTests.searchMediaByCode(Media_Code);
		waitPageLoaded();
		Assert.assertEquals(mediaTests.MediaTotal(), 1);
	}
	
	/***   update media  ***/
	@Test(priority = 108, enabled = true)
	public void updateMedia() throws InterruptedException, AWTException {
		mediaTests.goToMediaDetails(Media_Code);
		mediaTests.updateMedia(Media_description2);
		mediaTests.goBack();
	}
	
	/***   filter media By Description  ***/
	@Test(priority = 110, enabled = true)
	public void filterMediaByDescription() throws InterruptedException, AWTException {
		mediaTests.activateFilterBy("Media name");
		waitPageLoaded();
		mediaTests.filterMediaByDescription(Media_description2);
		Assert.assertEquals(mediaTests.MediaTotal(), 1);
		
		mediaTests.closeFilterBy("label");
	}
	
	/***   sort media Description  ***/
	@Test(priority = 114, enabled = true)
	public void sortMediaDescription() throws InterruptedException, AWTException {
		int nbr1 = mediaTests.MediaTotal();
		mediaTests.sortMediaListBy("label");
		waitPageLoaded();
		Assert.assertEquals(mediaTests.MediaTotal(), nbr1);
	}

}
