package com.opencellsoft.testsuites.sanity;

import org.testng.annotations.Test;
import com.opencellsoft.tests.QueryBuilderTests;
import com.opencellsoft.utility.Constant;

public class QueryBuilderSanityTests extends SanityTestsCommons {

	String queryName = "queryTest" + Constant.offerCode;
	String scopeEntity = "BillingCycle";
	String selected_field1 = "code";
	String selected_field2 = "description";
	String selected_field3 = null;
	String selected_field4 = "ignoreOrders";
	String filter_field = "description";
	String filter_operator = "Contains";
	String filter_value = "BR";
	String email = "azerty@opencellsoft.com";
	String fileFormat = "CSV";
	String frequency = "Every month";
	
	@Test(priority = 0)
	public void queryBuilderSanityTests() {
		queryBuilderTests = new QueryBuilderTests();
		System.out.println("**********************************  Query Builder Sanity Tests  **********************************");
	}
	
	/***   create Query  ***/
	@Test(priority = 104, enabled = true)
	public void createQuery() {
		homeTests.goToQueryBuilderPage();
		waitPageLoaded();
		queryBuilderTests.createQuery(queryName, scopeEntity,selected_field1,selected_field2,selected_field3, filter_field, filter_operator, filter_value);
	}
	
	/***   update Query  ***/
	@Test(priority = 108, enabled = true)
	public void updateQuery() {
		queryBuilderTests.removeSelectedField(queryName, "2");
		queryBuilderTests.addSelectedField(queryName,selected_field4);
	}
	
	/***   execute Query  ***/
	@Test(priority = 110, enabled = true)
	public void executeQuery() {
		queryBuilderTests.executeQuery();
	}
	
	/***   download Query  ***/
	@Test(priority = 112, enabled = true)
	public void downloadQuery() {
		queryBuilderTests.downloadQuery("CSV");
	}
	
	/***   schedule Query  ***/
	@Test(priority = 114, enabled = true)
	public void scheduleQuery() {
		queryBuilderTests.scheduleQuery(email,fileFormat, frequency);
	}

	/***   delete Query  ***/
	@Test(priority = 116, enabled = true)
	public void deleteQuery() {
		homeTests.goToQueryBuilderPage();
		waitPageLoaded();
		queryBuilderTests.searchQuery(queryName);
		queryBuilderTests.deleteQuery();
	}
	


}
