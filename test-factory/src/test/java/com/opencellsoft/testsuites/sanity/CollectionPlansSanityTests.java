package com.opencellsoft.testsuites.sanity;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.tests.CollectionPlansTests;
import com.opencellsoft.tests.HomeTests;
import com.opencellsoft.tests.LoginTest;

public class CollectionPlansSanityTests extends SanityTestsCommons {

	CollectionPlansTests collectionPlansTests;
	
	@Test(priority = 0)
	public void collectionPlansSanityTests() throws InterruptedException {
		collectionPlansTests = new CollectionPlansTests();
		System.out.println("**********************************  Collection plans sanity tests  **********************************");
		payloadTests.activateFunctions(baseURI, login, password);
		payloadTests.dunningSettings(baseURI, login, password);
		homeTests.logout();
		loginTest.login(login, password, version);
		
	}
	
	/***   filter by status  ***/
	@Test(priority = 4, enabled = true)
	public void filterByStatus() throws InterruptedException, AWTException {
		homeTests.goToCollectionPlansPage();
		collectionPlansTests.filterCollectionPlansByStatus("Active");
		waitPageLoaded();
		collectionPlansTests.filterCollectionPlansByStatus("Success");
		waitPageLoaded();
		collectionPlansTests.filterCollectionPlansByStatus("Paused");
		waitPageLoaded();
	}
	
	/***   download CSV  ***/
	@Test(priority = 6, enabled = true)
	public void downloadCSV() throws InterruptedException, AWTException {
		collectionPlansTests.exportFormat("CSV");
		waitPageLoaded();
		collectionPlansTests.exportFormat("PDF");
	}
	
}
