package com.opencellsoft.testsuites.sanity;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencellsoft.tests.DiscountPlansTests;
import com.opencellsoft.utility.Constant;

public class DiscountPlansSanityTests extends SanityTestsCommons  {
	String code = "DP-"+ Constant.offerCode;
	String label = "DiscountPlan-"+ Constant.offerCode;
	String type = "Offer";
	String label2 = "NewDiscountPlan-"+ Constant.offerCode;
	String type2 = "Product";
	DiscountPlansTests discountPlansTests;
	
	@Test(priority = 0)
	public void discountPlansSanityTests() {
		discountPlansTests = new DiscountPlansTests();
		System.out.println("**********************************  discount plans sanity tests  **********************************");
	}
	
	
	/***   create Discount Plan  ***/
	@Test(priority = 104, enabled = true)
	public void createDiscountPlan() throws InterruptedException, AWTException {
		homeTests.goToDiscountPlansPage();
		discountPlansTests.createDiscountPlan(code,label, type);
		discountPlansTests.goBack();
	}
	/***   search DiscountPlan by code  ***/
	@Test(priority = 106, enabled = true)
	public void searchDiscountPlanByCode() throws InterruptedException, AWTException {
		discountPlansTests.searchDiscountPlan(code);
		waitPageLoaded();
		Assert.assertEquals(discountPlansTests.discountPlansTotal(), 1);
	}
	
	/***   update Discount Plan  ***/
	@Test(priority = 108, enabled = true)
	public void updateDiscountPlan() throws InterruptedException, AWTException {
		discountPlansTests.goToDiscountPlanDetails(code);
		discountPlansTests.updateDiscountPlan(label2,type2);
	}
	
	/***   activate Discount Plan  ***/
	@Test(priority = 109, enabled = true)
	public void activateDiscountPlan() throws InterruptedException, AWTException {
		discountPlansTests.activateDiscountPlan();
		discountPlansTests.goBack();
	}
	
	/***   filter Discount Plan by type  ***/
	@Test(priority = 110, enabled = true)
	public void filterDiscountPlanByType() throws InterruptedException, AWTException {
		homeTests.goToDiscountPlansPage();
		discountPlansTests.searchDiscountPlan(label2);
		discountPlansTests.activateFilterBy("Type");
		waitPageLoaded();
		
		discountPlansTests.filterDiscountPlanByType(type2);
		waitPageLoaded();
		Assert.assertEquals(discountPlansTests.discountPlansTotal(), 1);
		
		discountPlansTests.closeFilterBy("discountPlanType");
	}
	
	/***   sort Discount Plans By Description  ***/
	@Test(priority = 114, enabled = true)
	public void sortDiscountPlansByDescription() throws InterruptedException, AWTException {
		discountPlansTests.sortDiscountPlansListBy("code");
		waitPageLoaded();
		Assert.assertEquals(discountPlansTests.discountPlansTotal(), 1);
	}
	
	/***   delete Discount Plan  ***/
	@Test(priority = 116, enabled = true)
	public void deleteDiscountPlan() throws InterruptedException, AWTException {
		discountPlansTests.goToDiscountPlanDetails(label2);
		discountPlansTests.delete();
		discountPlansTests.searchDiscountPlan(label2);
		Assert.assertEquals(discountPlansTests.discountPlansTotal(), 0);
	}
}
