package com.opencellsoft.testsuites.sanity;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.tests.AccountingPeriodTests;
import com.opencellsoft.tests.BillingrunTests;
import com.opencellsoft.tests.DunningTests;
import com.opencellsoft.tests.GeneralLedgerTests;
import com.opencellsoft.tests.HomeTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.tests.ManualMatchingTests;
import com.opencellsoft.tests.QueryBuilderTests;
import com.opencellsoft.tests.QueryResultTests;
import com.opencellsoft.tests.ReportExtractHistoryTests;
import com.opencellsoft.tests.ReportExtractsTests;
import com.opencellsoft.tests.SecurityDepositTests;
import com.opencellsoft.tests.TagCategoriesTests;
import com.opencellsoft.tests.TagsTests;
import com.opencellsoft.tests.TrialBalanceTests;
import com.opencellsoft.tests.jobs.PayloadTests;
import com.opencellsoft.utility.Constant;

public class SanityTestsCommons extends TestBase {

	LoginTest loginTest;
	HomeTests homeTests;
	PayloadTests payloadTests;
	BillingrunTests billingrunTests;
	DunningTests dunningTests;
	TagsTests tagsTests;
	TagCategoriesTests tagCategoriesTests;
	ReportExtractsTests reportExtractsTests;
	ReportExtractHistoryTests reportExtractHistoryTests;
	QueryBuilderTests queryBuilderTests;
	QueryResultTests queryResultTests;
	AccountingPeriodTests accountingPeriodTests;
	ManualMatchingTests manualMatchingTests;
	TrialBalanceTests trialBalanceTests;
	GeneralLedgerTests generalLedgerTests;
	SecurityDepositTests securityDepositTests;
	
	String version;
	String baseURI; 
	String login;
	String password;
	
	public SanityTestsCommons() {
		loginTest = new LoginTest();
		homeTests = new HomeTests();
		payloadTests = new PayloadTests();
	}
	
	@BeforeClass
	@Parameters(value = {"baseURI", "browser", "nodeURL", "login", "password", "version" })
	public void setup(String baseURI, String browser, String nodeURL, String login, String password, String version) throws Exception{
		initialize( browser, nodeURL, baseURI); 
		this.version = version;
		this.baseURI = baseURI; 
		this.login = login;
		this.password = password;
		
		loginTest.loginTest(login, password);
		homeTests.goToPortal();
	}
	
	/***  Teardown  ***/
	@AfterClass
	public void Teardown() {
		TeardownTest();
	}
}
