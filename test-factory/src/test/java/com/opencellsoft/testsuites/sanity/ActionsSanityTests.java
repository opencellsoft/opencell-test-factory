package com.opencellsoft.testsuites.sanity;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencellsoft.tests.DunningTests;
import com.opencellsoft.utility.Constant;

public class ActionsSanityTests extends SanityTestsCommons {

	
	String action_name = "Action-" + Constant.offerCode;
	String action_description = "Action-description-" + Constant.offerCode;
	String action_type = "Issue a credit note";
	
	String action_name2 = "Action2-" + Constant.offerCode;
	String action_description2 = "Action-description2-" + Constant.offerCode;
	String action_type2 = "Write-off Invoice";
	
	@Test(priority = 0)
	public void actionsSanityTests() {
		dunningTests = new DunningTests();
		System.out.println("**********************************  Actions sanity tests  **********************************");
	}
	
	/***   create Action  ***/
	@Test(priority = 204, enabled = true)
	public void createAction() throws InterruptedException, AWTException {
		homeTests.goToActionPage();
		dunningTests.createAction(action_name,action_description, action_type);
		dunningTests.goBack();
	}
	
	/***   search Actions by Name  ***/
	@Test(priority = 206, enabled = true)
	public void searchActionsByName() throws InterruptedException, AWTException {
		dunningTests.searchActionByName(action_name);
		waitPageLoaded();
		Assert.assertEquals(dunningTests.actionsTotal(), 1);
	}
	
	/***   update action  ***/
	@Test(priority = 208, enabled = true)
	public void updateAction() throws InterruptedException, AWTException {
		dunningTests.goToActionDetails(action_name);
		dunningTests.updateAction(action_name2,action_description2, action_type2);
		dunningTests.goBack();
	}
	
	/***   sort Actions By Name  ***/
	@Test(priority = 210, enabled = true)
	public void sortActionsByName() throws InterruptedException, AWTException {
		dunningTests.searchActionByName(action_name2);
		dunningTests.sortActionsListBy("code");
		waitPageLoaded();
		Assert.assertEquals(dunningTests.actionsTotal(), 1);
	}
}
