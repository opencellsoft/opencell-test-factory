package com.opencellsoft.testsuites.sanity;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.opencellsoft.tests.BusinessAttributesTests;

public class BusinessAttributesSanityTests extends SanityTestsCommons  {
	BusinessAttributesTests businessAttributesTests;
	
	String BusinessAttribute_name = "Product_quantity";
	String BusinessAttribute_value = "{edr.getParameter1()}";
	
	@Test(priority = 1)
	public void businessAttributesSanityTests() {
		businessAttributesTests = new BusinessAttributesTests();
		System.out.println("**********************************   Business Attributes Tests  **********************************");
	}
	

	/***   search Business Attribute by Name  ***/
	@Test(priority = 106, enabled = true)
	public void searchBusinessAttributesByName() throws InterruptedException, AWTException {
		homeTests.goToBusinessAttributesPage();
		businessAttributesTests.searchBusinessAttributesByName(BusinessAttribute_name);
		waitPageLoaded();
		Assert.assertEquals(businessAttributesTests.businessAttributesTotal(), 1);
	}

	/***   filter by Value  ***/
	@Test(priority = 110, enabled = true)
	public void filterBusinessAttributesByValue() throws InterruptedException, AWTException {
		businessAttributesTests.searchBusinessAttributesByName("edr_text");
		waitPageLoaded();
		Assert.assertEquals(businessAttributesTests.businessAttributesTotal(), 9);
		businessAttributesTests.activateFilterBy("Value");
		waitPageLoaded();
		
		businessAttributesTests.filterBusinessAttributesByValue(BusinessAttribute_value);
		waitPageLoaded();
		Assert.assertEquals(businessAttributesTests.businessAttributesTotal(), 1);
		
		businessAttributesTests.closeFilterBy("elValue");
	}
	
	
	/***   sort Business Attributes By Name  ***/
	@Test(priority = 114, enabled = true)
	public void sortBusinessAttributesByDescription() throws InterruptedException, AWTException {
		businessAttributesTests.searchBusinessAttributesByName("subscription");
		int total = businessAttributesTests.businessAttributesTotal();
		businessAttributesTests.sortBusinessAttributesListBy("description");
		Assert.assertEquals(businessAttributesTests.businessAttributesTotal(), total);
	}
	
	/***   delete Business Attributes  ***/
	@Test(priority = 116, enabled = true)
	public void deleteBusinessAttributes() throws InterruptedException, AWTException {
		businessAttributesTests.searchBusinessAttributesByName("Delivery date");
		waitPageLoaded();
		Assert.assertEquals(businessAttributesTests.businessAttributesTotal(), 1);
		businessAttributesTests.delete();
		businessAttributesTests.searchBusinessAttributesByName("Delivery date");
		Assert.assertEquals(businessAttributesTests.businessAttributesTotal(), 0);
	}
}
