package com.opencellsoft.testsuites.sanity;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.operations.massadjustments.MassAdjustmentsListPage;
import com.opencellsoft.pages.operations.rating.WalletOperationsListPage;

public class WalletOperationsTests extends TestBase {
	
	
	@Test(priority = 1)
	public void simpleFilters() throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		Thread.sleep(5000);
		waitPageLoaded();
		homePage.clickOnOperationMenuButton();
		homePage.clickOnRatingMenuButton();
		homePage.clickOnWalletOperationsMenuButton();
		WalletOperationsListPage walletOperationsListPage = PageFactory.initElements(driver, WalletOperationsListPage.class);
		walletOperationsListPage.checkText("MarkWOToRerateJob");
	}
	
	@Test(priority = 2)
	public void checkFilter() throws InterruptedException {
		WalletOperationsListPage walletOperationsListPage = PageFactory.initElements(driver, WalletOperationsListPage.class);
		walletOperationsListPage.clickOnFilterButton();
	}
	
	@Test(priority = 3)
	public void advancedFilter() throws InterruptedException {
		WalletOperationsListPage walletOperationsListPage = PageFactory.initElements(driver, WalletOperationsListPage.class);
		walletOperationsListPage.clickOnAdvancedFilterTab();
	}
	
	@Test(priority = 4)
	public void reratingBatches() throws InterruptedException {
		WalletOperationsListPage walletOperationsListPage = PageFactory.initElements(driver, WalletOperationsListPage.class);
		walletOperationsListPage.clickOnReratingBatchesTab();
	}

	
	@Test(priority = 5)
	public void rerate() throws InterruptedException {
		WalletOperationsListPage walletOperationsListPage = PageFactory.initElements(driver, WalletOperationsListPage.class);
		walletOperationsListPage.clickOnSimpleFiltersTab();
		walletOperationsListPage.clickOnRunJobButton();
		walletOperationsListPage.clickOnRerateButton();
		walletOperationsListPage.reratingTargetALL();
		walletOperationsListPage.clickOnConfirmButton();
		//Assert.assertEquals(walletOperationsListPage.getTextMessage(), "Action executed successfuly");
	}
	
	@Test(priority = 6)
	public void markToRerate() throws InterruptedException {
		WalletOperationsListPage walletOperationsListPage = PageFactory.initElements(driver, WalletOperationsListPage.class);
		//walletOperationsListPage.clickOnRunJobButton();
		walletOperationsListPage.clickOnMarkToRerateButton();
		walletOperationsListPage.clickOnConfirmButton();
		//Assert.assertEquals(walletOperationsListPage.getTextMessage(), "Action executed successfuly");
	}
}
