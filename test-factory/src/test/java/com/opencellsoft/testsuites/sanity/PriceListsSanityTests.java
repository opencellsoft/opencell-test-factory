package com.opencellsoft.testsuites.sanity;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencellsoft.tests.HomeTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.tests.PriceListsTests;
import com.opencellsoft.tests.jobs.PayloadTests;
import com.opencellsoft.utility.Constant;

public class PriceListsSanityTests extends SanityTestsCommons {
	
	PriceListsTests priceListsTests;
	
	@Test(priority = 0)
	public void priceListsSanityTests() {
		priceListsTests = new PriceListsTests();
		payloadTests = new PayloadTests();
		System.out.println("**********************************  Price Lists sanity tests  **********************************");
	}
	
	String priceList_name = "priceList-" + Constant.offerCode;
	String priceList_description = "priceListDescription-" + Constant.offerCode;
	String priceList_name2 = "NewpriceList-" + Constant.offerCode;
	String priceList_description2 = "NewpriceListDescription-" + Constant.offerCode;
	String priceLine_name = "priceLine-" + Constant.offerCode; 
	String priceLine_description = "priceLineDescription-" + Constant.offerCode;
	String chargeName = "charge-" + Constant.offerCode;
	String price = "25";

	
	/***   create Price List  ***/
	@Test(priority = 104, enabled = true)
	public void createPriceList() throws InterruptedException, AWTException {
		payloadTests.create_usageCharge(baseURI , login , password , chargeName, chargeName);
		homeTests.goToPriceListsPage();
		priceListsTests.createPriceList(priceList_name,priceList_description);
		priceListsTests.addApplicationRulesCountry(null);
		priceListsTests.createPriceLine(priceLine_name, priceLine_description, chargeName, price);
		priceListsTests.goBack();
		priceListsTests.goBack();
	}
	
	/***   search Price Lists By Name  ***/
	@Test(priority = 106, enabled = true)
	public void searchPriceListsByName() throws InterruptedException, AWTException {
		
		priceListsTests.searchPriceListsByName(priceList_name);
		waitPageLoaded();
		Assert.assertEquals(priceListsTests.PriceListsTotal(), 1);
	}
	
	/***   update Price List  ***/
	@Test(priority = 108, enabled = true)
	public void updatePriceList() throws InterruptedException, AWTException {
		priceListsTests.goToPriceListDetails(priceList_name);
		priceListsTests.updatePriceList(priceList_name2,priceList_description2);
		priceListsTests.activatePriceList();
	}
	
	/***   duplicate Price List  ***/
	@Test(priority = 110, enabled = true)
	public void duplicatePriceList() throws InterruptedException, AWTException {
		priceListsTests.duplicatePriceList();
	}
	
	/***   archive Price List  ***/
	@Test(priority = 112, enabled = true)
	public void archivePriceList() throws InterruptedException, AWTException {
		priceListsTests.archivePriceList();
		priceListsTests.goBack();
	}
	
	/***   close Price List  ***/
	@Test(priority = 114, enabled = true)
	public void closePriceList() throws InterruptedException, AWTException {
		priceListsTests.duplicatePriceList();
		priceListsTests.activatePriceList();
		priceListsTests.closePriceList();
		priceListsTests.goBack();
	}
	
	/***   filter Price List By status   ***/
	@Test(priority = 116, enabled = true)
	public void filterPriceListByStatus() throws InterruptedException, AWTException {
		homeTests.goToPriceListsPage();
		priceListsTests.activateFilterBy("Status");
		waitPageLoaded();
		
		priceListsTests.filterPriceListsByStatus("Active");
		waitPageLoaded();
		Assert.assertTrue(priceListsTests.PriceListsTotalOfStatus("Active") >=  1);
		Assert.assertEquals(priceListsTests.PriceListsTotalOfStatus("Closed") ,  0);
		
		priceListsTests.filterPriceListsByStatus("Closed");
		waitPageLoaded();
		Assert.assertTrue(priceListsTests.PriceListsTotalOfStatus("Closed") >=  1);
		Assert.assertEquals(priceListsTests.PriceListsTotalOfStatus("Archived") ,  0);
		
		priceListsTests.filterPriceListsByStatus("Archived");
		waitPageLoaded();
		Assert.assertTrue(priceListsTests.PriceListsTotalOfStatus("Archived") >=  1);
		Assert.assertEquals(priceListsTests.PriceListsTotalOfStatus("Active") ,  0);
		
		priceListsTests.closeFilterBy("status");
	}
	
	/***   sort Policies By Name  ***/
	@Test(priority = 118, enabled = true)
	public void sortPriceListByStatus() throws InterruptedException, AWTException {
		priceListsTests.sortPriceListsListBy("status");
		waitPageLoaded();
		Assert.assertTrue(priceListsTests.PriceListsTotal()>= 3);
	}
}
