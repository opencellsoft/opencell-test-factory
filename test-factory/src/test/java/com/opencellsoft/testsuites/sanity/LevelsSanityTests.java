package com.opencellsoft.testsuites.sanity;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencellsoft.tests.DunningTests;
import com.opencellsoft.utility.Constant;

public class LevelsSanityTests extends SanityTestsCommons {
	String level_name = "level-111";
	String level_description = "level-description-" + Constant.offerCode;
	String level_daysOverdue = "3";
	Boolean level_endOfDunning = true;
	
	String level_name2 = "level2-" + Constant.offerCode;
	String level_description2 = "level-description2-" + Constant.offerCode;
	String level_daysOverdue2 = "5";
	Boolean level_active2 = true;
	
	String action_name = "Action3-" + Constant.offerCode;
	String action_description = "Action-description3-" + Constant.offerCode;
	String action_type = "Issue a credit note";
	
	
	@Test(priority = 0)
	public void levelsSanityTests() {
		dunningTests = new DunningTests();
		System.out.println("**********************************  Levels sanity tests  **********************************");
	}
	
	/***   create level  ***/
	@Test(priority = 304, enabled = true)
	public void createLevel() throws InterruptedException, AWTException {
		homeTests.goToActionPage();
		dunningTests.createAction(action_name,action_description, action_type);
		homeTests.goToLevelPage();
		dunningTests.createLevel(level_name,level_description, level_daysOverdue,level_endOfDunning, null);
		dunningTests.goBack();
	}
	
	/***   search level by Name  ***/
	@Test(priority = 306, enabled = true)
	public void searchLevelsByName() throws InterruptedException, AWTException {
		dunningTests.searchLevelByName(level_name);
		waitPageLoaded();
		Assert.assertEquals(dunningTests.levelsTotal(), 1);
	}
	
	/***   update level  ***/
	@Test(priority = 308, enabled = true)
	public void updateLevel() throws InterruptedException, AWTException {
		dunningTests.goToLevelDetails(level_name);
		dunningTests.pickActionToLevel(action_name);
		dunningTests.updateLevel(level_name2,level_description2, level_daysOverdue2, level_active2);
		dunningTests.goBack();
	}
	
	/***   sort level By Name  ***/
	@Test(priority = 310, enabled = true)
	public void sortLevelsByName() throws InterruptedException, AWTException {
		dunningTests.searchLevelByName(level_name2);
		dunningTests.sortLevelsListBy("code");
		waitPageLoaded();
		Assert.assertEquals(dunningTests.levelsTotal(), 1);
	}

	/***   delete level  ***/
	@Test(priority = 312, enabled = true)
	public void deleteLevel() throws InterruptedException, AWTException {
		dunningTests.goToLevelDetails(level_name2);
		dunningTests.delete();
		dunningTests.searchLevelByName(level_name2);
		waitPageLoaded();
		Assert.assertEquals(dunningTests.levelsTotal(), 0);
		Assert.assertEquals(dunningTests.noResultsFound(), 1);
	}
	
	/***   create level  ***/
	@Test(priority = 316, enabled = true)
	public void createLevel2() throws InterruptedException, AWTException {
		homeTests.goToLevelPage();
		dunningTests.createLevel(level_name,level_description, level_daysOverdue, level_endOfDunning, action_name);
		dunningTests.goBack();
	}
}
