package com.opencellsoft.testsuites.sanity;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencellsoft.tests.ProductFamiliesTests;
import com.opencellsoft.utility.Constant;

public class ProductFamiliesSanityTests extends SanityTestsCommons {	
	ProductFamiliesTests productFamiliesTests;
	
	String pf_code = "pfCode-" + Constant.offerCode;
	String pf_name = "pfName-" + Constant.offerCode ;
	String pf_description = "pfDescription-" + Constant.offerCode;
	String pf_seller = null;
	String pf_parent = null;
	String pf_name2 = "pfNewName-" + Constant.offerCode ;
	String pf_description2 = "pfNewDescription-" + Constant.offerCode;
	@Test(priority = 0)
	public void productFamiliesSanityTests() {
		productFamiliesTests = new ProductFamiliesTests();
		System.out.println("**********************************   Product Families tests  **********************************");
	}

	/***   create productFamily  ***/
	@Test(priority = 104, enabled = true)
	public void createProductFamilies() throws InterruptedException, AWTException {
		homeTests.goToProductFamiliesPage();
		productFamiliesTests.createProductFamilies(pf_code,pf_name, pf_description, pf_seller, pf_parent);
		productFamiliesTests.goBack();
	}
	/***   search Product Families by Name  ***/
	@Test(priority = 106, enabled = true)
	public void searchProductFamiliesByName() throws InterruptedException, AWTException {
		try {
			productFamiliesTests.searchProductFamiliesByName(pf_name);
		}catch (Exception e) {
			productFamiliesTests.goBack();
			productFamiliesTests.searchProductFamiliesByName(pf_name);
		}
		waitPageLoaded();
		Assert.assertEquals(productFamiliesTests.productFamiliesTotal(), 1);
	}
	
	/***   update productFamily  ***/
	@Test(priority = 108, enabled = true)
	public void updateProductFamilies() throws InterruptedException, AWTException {
		productFamiliesTests.goToProductFamiliesDetails(pf_name);
		productFamiliesTests.updateProductFamilies(pf_name2,pf_description2,null, null);
		productFamiliesTests.goBack();
	}
	
	/***   filter by name  ***/
	@Test(priority = 110, enabled = true)
	public void filterProductFamiliesByName() throws InterruptedException, AWTException {
		homeTests.goToProductFamiliesPage();
		productFamiliesTests.searchProductFamiliesByName(pf_name2);
		productFamiliesTests.activateFilterBy("Product family name","Product Family Name");
		
		waitPageLoaded();
		
		productFamiliesTests.filterByProductFamiliesName(pf_name2);
		waitPageLoaded();
		Assert.assertEquals(productFamiliesTests.productFamiliesTotal(), 1);
		
		productFamiliesTests.closeFilterBy("description");
	}
	
	/***   sort Product Families By Name  ***/
	@Test(priority = 114, enabled = true)
	public void sortProductFamiliesByName() throws InterruptedException, AWTException {
		productFamiliesTests.sortProductFamiliesListBy("code");
		waitPageLoaded();
		Assert.assertEquals(productFamiliesTests.productFamiliesTotal(), 1);
	}
	
	/***   delete productFamily  ***/
	@Test(priority = 116, enabled = true)
	public void deleteProductFamilies() throws InterruptedException, AWTException {
		productFamiliesTests.goToProductFamiliesDetails(pf_name2);
		productFamiliesTests.delete();
		productFamiliesTests.searchProductFamiliesByName(pf_name2);
		Assert.assertEquals(productFamiliesTests.productFamiliesTotal(), 0);
		
	}
}
