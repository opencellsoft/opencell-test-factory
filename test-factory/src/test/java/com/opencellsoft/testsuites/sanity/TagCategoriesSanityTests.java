package com.opencellsoft.testsuites.sanity;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencellsoft.tests.TagCategoriesTests;
import com.opencellsoft.utility.Constant;

public class TagCategoriesSanityTests extends SanityTestsCommons {
	
	String tagC_code = "tagCCode-" + Constant.offerCode;
	String tagC_name = "tagCName-" + Constant.offerCode ;
	String tagC_name2 = "tagCName2-" + Constant.offerCode ;
	String tagC_seller = null;
	
	@Test(priority = 0)
	public void tagCategoriesSanityTests() {
		tagCategoriesTests = new TagCategoriesTests();
		System.out.println("**********************************   Tag Categories tests  **********************************");
	}

	/***   create tagCategories  ***/
	@Test(priority = 104, enabled = true)
	public void createTagCategories() throws InterruptedException, AWTException {
		homeTests.goToTagCategoriesPage();
		tagCategoriesTests.createTagCategory(tagC_code,tagC_name,tagC_seller);
		tagCategoriesTests.goBack();
	}
	/***   search Tag Categories by Name  ***/
	@Test(priority = 106, enabled = true)
	public void searchTagCategoriesByName() throws InterruptedException, AWTException {
		try {
			tagCategoriesTests.searchTagCategoriesByName(tagC_name);
		}catch (Exception e) {
			tagCategoriesTests.goBack();
			tagCategoriesTests.searchTagCategoriesByName(tagC_name);
		}
		waitPageLoaded();
		Assert.assertEquals(tagCategoriesTests.tagCategoriesTotal(), 1);
	}
	
	/***   update tagCategories  ***/
	@Test(priority = 108, enabled = true)
	public void updateTagCategories() throws InterruptedException, AWTException {
		tagCategoriesTests.goToTagCategoriesDetails(tagC_name);
		tagCategoriesTests.updateTagCategory(tagC_name2);
		tagCategoriesTests.goBack();
	}
	
	/***   filter by name  ***/
	@Test(priority = 110, enabled = true)
	public void filterTagCategoriesByName() throws InterruptedException, AWTException {
		tagCategoriesTests.activateFilterBy("Tag category name","Tag Category Name");
		waitPageLoaded();
		tagCategoriesTests.filterByTagCategoriesName(tagC_name2);
		waitPageLoaded();
		Assert.assertEquals(tagCategoriesTests.tagCategoriesTotal(), 1);
		
		tagCategoriesTests.closeFilterBy("description");
	}
	
	/***   sort Tag Categories By Name  ***/
	@Test(priority = 114, enabled = true)
	public void sortTagCategoriesByName() throws InterruptedException, AWTException {
		int n = tagCategoriesTests.tagCategoriesTotal();
		tagCategoriesTests.sortTagCategoriesListBy("code");
		waitPageLoaded();
		Assert.assertEquals(tagCategoriesTests.tagCategoriesTotal(), n);
	}
	
	/***   delete tagCategories  ***/
	@Test(priority = 116, enabled = true)
	public void deleteTagCategories() throws InterruptedException, AWTException {
		tagCategoriesTests.goToTagCategoriesDetails(tagC_name2);
		tagCategoriesTests.delete();
		tagCategoriesTests.searchTagCategoriesByName(tagC_name2);
		Assert.assertEquals(tagCategoriesTests.tagCategoriesTotal(), 0);
		
	}
	/***   create tagCategories  ***/
	@Test(priority = 118, enabled = true)
	public void createTagCategories2() throws InterruptedException, AWTException {
		tagCategoriesTests.createTagCategory(tagC_code,tagC_name,tagC_seller);
	}
}
