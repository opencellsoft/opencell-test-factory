package com.opencellsoft.testsuites.sanity;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.operations.billingrules.InvoiceValidationListPage;

public class InvoiceValidationTests extends TestBase {
	
	
	@Test(priority = 1)
	public void searchByCode() throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		Thread.sleep(5000);
		waitPageLoaded();
		homePage.clickOnOperationMenuButton();
		homePage.clickOnBillingRulesButton();
		homePage.clickOnInvoiceValidationButton();
		InvoiceValidationListPage invoiceValidationListPage = PageFactory.initElements(driver, InvoiceValidationListPage.class);
		invoiceValidationListPage.setText("COM");
		Thread.sleep(2000);
		invoiceValidationListPage.ClickOnClearIcon();
	}
	
	@Test(priority = 2)
	public void searchByDescription() throws InterruptedException {
		InvoiceValidationListPage invoiceValidationListPage = PageFactory.initElements(driver, InvoiceValidationListPage.class);
		invoiceValidationListPage.setText("COM");
		invoiceValidationListPage.ClickOnClearIcon();
		Thread.sleep(2000);
	}
	
	@Test(priority = 3)
	public void sortByCode() throws InterruptedException {
		InvoiceValidationListPage invoiceValidationListPage = PageFactory.initElements(driver, InvoiceValidationListPage.class);
		invoiceValidationListPage.sortByCode();
	}
	
	@Test(priority = 4)
	public void sortByDescription() throws InterruptedException {
		InvoiceValidationListPage invoiceValidationListPage = PageFactory.initElements(driver, InvoiceValidationListPage.class);
		invoiceValidationListPage.sortByDescription();
	}
	
	@Test(priority = 5)
	public void editRules() throws InterruptedException {
		InvoiceValidationListPage invoiceValidationListPage = PageFactory.initElements(driver, InvoiceValidationListPage.class);
		invoiceValidationListPage.ClickOnComInvoice();
		Thread.sleep(2000);
		invoiceValidationListPage.ClickOnEditIcon();
		invoiceValidationListPage.clickOnConfirmButton();
		invoiceValidationListPage.clickOnGoBackButton();
	}
	
	@Test(priority = 6)
	public void deleteRules() throws InterruptedException {
		InvoiceValidationListPage invoiceValidationListPage = PageFactory.initElements(driver, InvoiceValidationListPage.class);
		invoiceValidationListPage.ClickOnComInvoice();
		invoiceValidationListPage.ClickOnDeleteIcon();
		invoiceValidationListPage.clickOnGoBackButton();
	}
	
	@Test(priority = 7)
	public void filterByHasValidationScript() throws InterruptedException {
		InvoiceValidationListPage invoiceValidationListPage = PageFactory.initElements(driver, InvoiceValidationListPage.class);
		invoiceValidationListPage.clickOnHasValidationScriptCheckbox();
		invoiceValidationListPage.clickOnHasValidationScriptCheckbox();
	}
	
	@Test(priority = 8)
	public void filterByHasRules() throws InterruptedException {
		InvoiceValidationListPage invoiceValidationListPage = PageFactory.initElements(driver, InvoiceValidationListPage.class);
		invoiceValidationListPage.clickOnHasRulesCheckbox();
		invoiceValidationListPage.clickOnHasRulesCheckbox();
	}
	

}
