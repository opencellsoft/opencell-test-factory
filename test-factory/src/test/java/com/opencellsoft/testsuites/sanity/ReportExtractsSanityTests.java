package com.opencellsoft.testsuites.sanity;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencellsoft.tests.ReportExtractsTests;

public class ReportExtractsSanityTests extends SanityTestsCommons {

	String reportExtractsCode = "EDR";
	@Test(priority = 0)
	public void reportExtractsSanityTests() {
		reportExtractsTests = new ReportExtractsTests();
		System.out.println("**********************************   Report Extracts tests  **********************************");
		homeTests.goToReportExtractsPage();
	}
	
	
	
	/***   sort ReportExtracts By Name  ***/
	@Test(priority = 106, enabled = true)
	public void sortReportExtractsByName() {
		
		int n = reportExtractsTests.reportExtractsTotal();
		reportExtractsTests.sortReportExtractsListBy("code");
		waitPageLoaded();
		Assert.assertEquals(reportExtractsTests.reportExtractsTotal(), n);
	}
	
	/***   search ReportExtracts by Name  ***/
	@Test(priority = 108, enabled = true)
	public void searchReportExtractsByCode(){
		reportExtractsTests.searchReportExtractsByName(reportExtractsCode);
		waitPageLoaded();
		Assert.assertEquals(reportExtractsTests.reportExtractsTotal(), 1);
	}
	
	/***   Run report  ***/
	@Test(priority = 110, enabled = true)
	public void runReport() {
		reportExtractsTests.runReport();
	}
	
	/***   download report  ***/
	@Test(priority = 112, enabled = true)
	public void downloadReport() {
		reportExtractsTests.downloadReport();
		reportExtractsTests.runReport();
	}
}
