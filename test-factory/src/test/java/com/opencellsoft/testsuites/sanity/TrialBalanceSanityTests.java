package com.opencellsoft.testsuites.sanity;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.opencellsoft.tests.TrialBalanceTests;

public class TrialBalanceSanityTests extends SanityTestsCommons {


	@Test(priority = 0)
	public void trialBalanceSanityTests() {
		trialBalanceTests = new TrialBalanceTests();
		System.out.println("**********************************  Trial Balance Sanity Tests  **********************************");
	}
	
	/***   filter By Period  ***/
	@Test(priority = 4, enabled = true)
	public void filterByPeriod(){
		homeTests.goToTrialBalancePage();
		trialBalanceTests.filterByPeriod("This year");
		Assert.assertTrue(trialBalanceTests.alertMsg());
		trialBalanceTests.filterByPeriod("This month");
		Assert.assertTrue(trialBalanceTests.alertMsg());
	}
	
	/***   export  ***/
	@Test(priority = 6, enabled = true)
	public void export(){
		trialBalanceTests.export("CSV");
		Assert.assertTrue(trialBalanceTests.alertMsg());
		trialBalanceTests.export("XLSX");
		Assert.assertTrue(trialBalanceTests.alertMsg());
	}
}
