package com.opencellsoft.testsuites.sanity;

import java.awt.AWTException;

import org.testng.annotations.Test;

import com.opencellsoft.tests.PriceVersionsTests;
import com.opencellsoft.tests.jobs.PayloadTests;
import com.opencellsoft.utility.Constant;

import junit.framework.Assert;

public class PriceVersionsSanityTests extends SanityTestsCommons  {

	PriceVersionsTests priceVersionsTests;
	String chargeName, PVName;
	@Test(priority = 0)
	public void priceListsSanityTests() {
		priceVersionsTests = new PriceVersionsTests();
		payloadTests = new PayloadTests();
		System.out.println("**********************************  Price versions sanity tests  **********************************");
	}
	
	/***   create Price List  ***/
	@Test(priority = 104, enabled = true)
	public void createSeveralPriceVersions() throws InterruptedException, AWTException {
		
		// 4 charges
		for(int i = 1 ; i < 5; i++) {
			chargeName = "Ch00" + i + "-" + Constant.offerCode;
			PVName = "PV00" + i + "-" + Constant.offerCode;
			payloadTests.create_usageCharge(baseURI , login , password , chargeName, chargeName);
			Thread.sleep(500);
			payloadTests.create_pricePlanMatrixVersion(baseURI , login , password , chargeName);
			Thread.sleep(500);
			payloadTests.create_priceVersion(baseURI , login , password , PVName, "PPM_" + chargeName, Integer.toString(i*10));
			Thread.sleep(500);
			// 1 Draft PV
			if(i<4) {
				payloadTests.Publish_priceVersion(baseURI , login , password , "PPM_" + chargeName, "1");
			}
		}
		
		chargeName ="Charge55";
		PVName = "PV55";
		payloadTests.create_usageCharge(baseURI , login , password , chargeName, chargeName);
		Thread.sleep(500);
		payloadTests.create_pricePlanMatrixVersion(baseURI , login , password , chargeName);
		Thread.sleep(500);
		payloadTests.create_priceVersion(baseURI , login , password , PVName, "PPM_" + chargeName, "55");
		Thread.sleep(500);
		
	}
	
	/***   filter Price Versions By Status Published  ***/
	@Test(priority = 106, enabled = true)
	public void filterPriceVersionsByStatus() throws InterruptedException, AWTException {
		homeTests.goToExportPriceVersionsPage();
		priceVersionsTests.filterByStatus("DRAFT","Draft");
		
		Assert.assertTrue(priceVersionsTests.getPriceversionsTotalWithStatus("PUBLISHED","Published") == 0);
		Assert.assertTrue(priceVersionsTests.getPriceversionsTotalWithStatus("DRAFT", "Draft") > 1);
		
		homeTests.goToExportPriceVersionsPage();
		priceVersionsTests.filterByStatus("PUBLISHED","Published");
		
		Assert.assertTrue(priceVersionsTests.getPriceversionsTotalWithStatus("PUBLISHED","Published") >= 2);
		Assert.assertTrue(priceVersionsTests.getPriceversionsTotalWithStatus("DRAFT", "Draft") == 0);
	}
	
	/***   export Price Versions ***/
	@Test(priority = 108, enabled = true)
	public void exportPriceVersions() throws InterruptedException, AWTException {
		priceVersionsTests.exportPriceVersions();
	}
	
	/***   import Price Versions ***/
	@Test(priority = 108, enabled = true)
	public void importPriceVersions() throws InterruptedException, AWTException {
		homeTests.goToImportPriceVersionsPage();
		priceVersionsTests.importPriceVersions();
		Assert.assertEquals(priceVersionsTests.getExecutionMessage(), "Action executed successfuly");
	}
}
