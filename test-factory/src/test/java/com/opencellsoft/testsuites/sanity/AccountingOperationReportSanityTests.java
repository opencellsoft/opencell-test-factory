package com.opencellsoft.testsuites.sanity;

import java.awt.AWTException;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.opencellsoft.tests.AccountingPeriodTests;

public class AccountingOperationReportSanityTests extends SanityTestsCommons {

	@Test(priority = 0)
	public void accountingOperationReportSanityTests() {
		 accountingPeriodTests = new AccountingPeriodTests();
		System.out.println("**********************************  Accounting Operation Report Sanity Tests  **********************************");
	}
	
	/***   filter By Status  ***/
	@Test(priority = 4, enabled = true)
	public void filterByStatusPosted() throws InterruptedException, AWTException {
		homeTests.goToAccountingPeriodReportPage();
		accountingPeriodTests.activateFilterBy("Status");
		accountingPeriodTests.filterByStatus("Posted");
		Assert.assertEquals(accountingPeriodTests.elementsFoundSize("exported"), 0);	
	}
	
	/***   launch Accounting Schemes Job  ***/
	@Test(priority = 6, enabled = true)
	public void launchAccountingSchemesJob() throws InterruptedException, AWTException {
		accountingPeriodTests.launchAccountingSchemesJob();
	}
	
	/***   filter By Status  ***/
	@Test(priority = 8, enabled = true)
	public void filterByStatusExported() throws InterruptedException, AWTException {
		accountingPeriodTests.filterByStatus("Exported");
		Assert.assertEquals(accountingPeriodTests.elementsFoundSize("Posted"), 0);
	}
	
	/***   filter By Status  ***/
	@Test(priority = 10, enabled = true)
	public void launchAccountingSchemesJobInJobReport() throws InterruptedException, AWTException {
		accountingPeriodTests.goToJobsReportPage();
		accountingPeriodTests.launchAccountingSchemesJob();
		System.out.println(" AccountingSchemesJob :  " +accountingPeriodTests.elementsFoundSize("AccountingSchemesJob"));
		Assert.assertTrue(accountingPeriodTests.elementsFoundSize("accountingschemesjob")> 1);
	}
}
