package com.opencellsoft.testsuites.sanity;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencellsoft.tests.TagCategoriesTests;
import com.opencellsoft.tests.TagsTests;
import com.opencellsoft.utility.Constant;

public class TagsSanityTests extends SanityTestsCommons {

	String tagC_code = "NewtagCCode-" + Constant.offerCode;
	String tagC_name = "tagCName-" + Constant.offerCode ;
	String tagC_seller = null;
	
	String tag_code = "tagCode-" + Constant.offerCode;
	String tag_name = "tagName-" + Constant.offerCode ;
	String tag_seller = null;
	String tag_parent = null;
	String tag_name2 = "tagNewName-" + Constant.offerCode ;
	
	
	@Test(priority = 0)
	public void tagsSanityTests() {
		tagCategoriesTests = new TagCategoriesTests();
		tagsTests = new TagsTests();
		System.out.println("**********************************   Tags tests  **********************************");
	}

	/***   create tags  ***/
	@Test(priority = 104, enabled = true)
	public void createTag() throws InterruptedException, AWTException {
		homeTests.goToTagCategoriesPage();
		tagCategoriesTests.createTagCategory(tagC_code,tagC_name,tagC_seller);
		homeTests.goToTagsPage();
		tagsTests.createTag(tag_code,tag_name, tagC_code, tag_seller, tag_parent);
		tagsTests.goBack();
	}
	
	/***   search Tags by Name  ***/
	@Test(priority = 106, enabled = true)
	public void searchTagsByName() throws InterruptedException, AWTException {
		try {
			tagsTests.searchTagsByName(tag_name);
		}catch (Exception e) {
			tagsTests.goBack();
			tagsTests.searchTagsByName(tag_name);
		}
		waitPageLoaded();
		Assert.assertEquals(tagsTests.tagsTotal(), 1);
	}
	
	/***   update tags  ***/
	@Test(priority = 108, enabled = true)
	public void updateTag() throws InterruptedException, AWTException {
		tagsTests.goToTagsDetails(tag_name);
		tagsTests.updateTag(tag_name2,null,null, null);
		tagsTests.goBack();
	}
	
	/***   filter by name  ***/
	@Test(priority = 110, enabled = true)
	public void filterTagsByName() throws InterruptedException, AWTException {
		try {
			tagsTests.goBack();
			tagsTests.activateFilterBy("Tag name","Tag Name");
		}catch (Exception e) {
			tagsTests.activateFilterBy("Tag name","Tag Name");
		}
		waitPageLoaded();
		tagsTests.filterByTagsName(tag_name2);
		waitPageLoaded();
		Assert.assertEquals(tagsTests.tagsTotal(), 1);
		
		tagsTests.closeFilterBy("name");
	}
	
	/***   sort tags By Name  ***/
	@Test(priority = 114, enabled = true)
	public void sortTagsByName() throws InterruptedException, AWTException {
		int n = tagsTests.tagsTotal();
		tagsTests.sortTagsListBy("code");
		waitPageLoaded();
		Assert.assertEquals(tagsTests.tagsTotal(), n);
	}
	
	/***   delete tags  ***/
	@Test(priority = 116, enabled = true)
	public void deleteTags() throws InterruptedException, AWTException {
		tagsTests.goToTagsDetails(tag_name2);
		tagsTests.delete();
		tagsTests.searchTagsByName(tag_name2);
		Assert.assertEquals(tagsTests.tagsTotal(), 0);
	}
}
