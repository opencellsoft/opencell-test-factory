package com.opencellsoft.testsuites.sanity;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.opencellsoft.tests.ManualMatchingTests;
import com.opencellsoft.utility.Constant;

public class ManualMatchingSanityTests extends SanityTestsCommons {
	String BC_code 		 = "MBC-"+Constant.offerCode;
	String customerCode  = "MCUS-"+Constant.offerCode;
	String customerCode2 = "MCUS2-"+Constant.offerCode;
	String invoiceType   = "COM";
	String amountWithTax = "700";
	String seller        = "MAIN_SELLER" ;
	String refreshExchangeRate = "false";
	String generateAO	 = "true";
	String skipValidation = "false";
	String paymentMethod = "CHECK";
	String currency      = "EUR";
	String amount        = "700";
	String reference     = "Ref700";
	
	@Test(priority = 0)
	public void manualMatchingSanityTests() {
		manualMatchingTests = new ManualMatchingTests();
		System.out.println("**********************************  manual Matching Sanity Tests  **********************************");
	}
	
	/***   create account operation   ***/
	@Test(priority = 4, enabled = true)
	public void dataSet() throws InterruptedException, AWTException, JsonMappingException, JsonProcessingException {
		payloadTests.addFunctionalCurrencyByAPI(baseURI, login, password,"EUR");
		// Create customer
		payloadTests.createBillingCycleByAPI(baseURI, login, password,BC_code,BC_code,"BILLINGACCOUNT","MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,BC_code,BC_code,"BILLINGACCOUNT","CAL_PERIOD_MONTHLY");
		Thread.sleep(500);
		payloadTests.createCustomer(baseURI, login, password, customerCode, customerCode, BC_code);
		Thread.sleep(500);
		
		// Create invoice
		String invoiceId = payloadTests.createBasicInvoice(baseURI, login, password,invoiceType, customerCode, amountWithTax, seller );
		payloadTests.validateInvoice(baseURI, login, password,invoiceId, refreshExchangeRate, generateAO, skipValidation);

	}
	
	/***   create account operation  ***/
	@Test(priority = 6, enabled = true)
	public void createAccountOperation() throws InterruptedException, AWTException {
		homeTests.goToManualMatchingPage();
		manualMatchingTests.createAO(paymentMethod,customerCode, currency, amount, reference, version );
	}
	
	/***    search Debit Account Operations By Cutomer  ***/
	@Test(priority = 8, enabled = true)
	public void searchDebitAccountOperationsByCutomer() throws InterruptedException, AWTException {
		homeTests.goToManualMatchingPage();
		manualMatchingTests.searchDebitAccountOperations(customerCode);
		waitPageLoaded();
		Assert.assertEquals(manualMatchingTests.elementsFoundSizeDebitAOs(customerCode), 1);
	}
	
	/***    search Credit Account Operations By Cutomer  ***/
	@Test(priority = 10, enabled = true)
	public void searchCreditAccountOperationsByCutomer() throws InterruptedException, AWTException {
		manualMatchingTests.searchCreditAccountOperations(customerCode);
		waitPageLoaded();
		Assert.assertEquals(manualMatchingTests.elementsFoundSizeCreditAOs(customerCode), 1);
	}
	
	/***    match account operation  ***/
	@Test(priority = 12, enabled = true)
	public void matchAccountOperation() throws InterruptedException, AWTException {
		manualMatchingTests.checkFirstDebitAccountOperation();
		waitPageLoaded();
		manualMatchingTests.checkFirstCreditAccountOperation();
		waitPageLoaded();
		manualMatchingTests.matchAO();
	}
	
	/***   transfer Account Operation  ***/
	@Test(priority = 14, enabled = true)
	public void transferAccountOperation() throws InterruptedException, AWTException {
		payloadTests.createCustomer(baseURI, login, password, customerCode2, customerCode2, BC_code);
		Thread.sleep(500);
		manualMatchingTests.createAO(paymentMethod,customerCode, currency, "500", reference, version );
		homeTests.goToManualMatchingPage();
		waitPageLoaded();
		manualMatchingTests.activateTranferAOMode(version);
		waitPageLoaded();
		manualMatchingTests.searchCreditAccountOperations(customerCode);
		waitPageLoaded();
		manualMatchingTests.checkFirstCreditAccountOperation();
		waitPageLoaded();
		manualMatchingTests.transferAO(customerCode2, version);
		waitPageLoaded();
	}
	
	/***   match Account Operation Partially   ***/
	@Test(priority = 16, enabled = true)
	public void matchAccountOperationPartially() throws InterruptedException, AWTException, JsonMappingException, JsonProcessingException {
		homeTests.goToManualMatchingPage();
		String invoiceId = payloadTests.createBasicInvoice(baseURI, login, password,invoiceType, customerCode2, "550", seller );
		payloadTests.validateInvoice(baseURI, login, password,invoiceId, refreshExchangeRate, generateAO, skipValidation);
		manualMatchingTests.searchDebitAccountOperations(customerCode2);
		manualMatchingTests.searchCreditAccountOperations(customerCode2);
		waitPageLoaded();
		manualMatchingTests.checkFirstDebitAccountOperation();
		manualMatchingTests.checkFirstCreditAccountOperation();
		waitPageLoaded();
		manualMatchingTests.matchAOPartially();
	}
	
	/***   filter By Partially Matched  ***/
	@Test(priority = 18, enabled = true)
	public void filterDebitAOByStatusPartiallyMatched() throws InterruptedException, AWTException {
		manualMatchingTests.activateFilterDebitBy("Status");
		manualMatchingTests.filterDebitAOByStatus("Partially matched");
		manualMatchingTests.searchDebitAccountOperations(customerCode2);
		waitPageLoaded();
		Assert.assertEquals(manualMatchingTests.elementsFoundSizeDebitAOs(customerCode2), 1);
		Assert.assertTrue(manualMatchingTests.elementsFoundSizeDebitAOs("50.00") == 1 || manualMatchingTests.elementsFoundSizeDebitAOs("50") == 1);
	}

}
