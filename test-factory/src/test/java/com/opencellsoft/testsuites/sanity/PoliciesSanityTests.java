package com.opencellsoft.testsuites.sanity;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencellsoft.tests.DunningTests;
import com.opencellsoft.utility.Constant;

public class PoliciesSanityTests extends SanityTestsCommons  {
	String policy_name = "policy-" + Constant.offerCode;
	String policy_description = "policyDescription-" + Constant.offerCode;

	String policy_name2 = "policy2-" + Constant.offerCode;
	String policy_description2 = "policyDescription2-" + Constant.offerCode;
	Boolean policy_active2 = true;
	
	String level_name = "level-111";
	
	@Test(priority = 0)
	public void policiesSanityTests() {
		dunningTests = new DunningTests();
		System.out.println("**********************************  Policies sanity tests  **********************************");
	}
	/***   create Policy  ***/
	@Test(priority = 404, enabled = true)
	public void createPolicy() throws InterruptedException, AWTException {
		homeTests.goToPolicyPage();
		dunningTests.createPolicy(policy_name,policy_description,level_name);
		dunningTests.goBack();
	}
	/***   search Policies By Name  ***/
	@Test(priority = 406, enabled = true)
	public void searchPoliciesByName() throws InterruptedException, AWTException {
		dunningTests.searchPolicyByName(policy_name);
		waitPageLoaded();
		Assert.assertEquals(dunningTests.policyTotal(), 1);
	}
	
	/***   update Policy  ***/
	@Test(priority = 408, enabled = true)
	public void updatePolicy() throws InterruptedException, AWTException {
		dunningTests.goToPolicyDetails(policy_name);
		dunningTests.ApplyPolicyto("Company",null,"Yes");
		dunningTests.updatePolicy(policy_name2,policy_description2,policy_active2);
		dunningTests.goBack();
	}
	
	/***   filter Policies By Active   ***/
	@Test(priority = 410, enabled = true)
	public void filterPoliciesByActive() throws InterruptedException, AWTException {
		homeTests.goToPolicyPage();
		dunningTests.searchPolicyByName(policy_name2);
		dunningTests.activateFilterBy("Active");
		waitPageLoaded();
		
		dunningTests.filterPolicyByActive(policy_active2);
		waitPageLoaded();
		Assert.assertEquals(dunningTests.policyTotal(), 1);
		
		dunningTests.closeFilterBy("isActivePolicy");
	}
	
	/***   sort Policies By Name  ***/
	@Test(priority = 414, enabled = true)
	public void sortPoliciesByName() throws InterruptedException, AWTException {
		dunningTests.sortPoliciesListBy("policyName");
		waitPageLoaded();
		Assert.assertEquals(dunningTests.policyTotal(), 1);
	}
	
	/***   filter Policies By Description  ***/
	@Test(priority = 418, enabled = true)
	public void filterPoliciesByDescription() throws InterruptedException, AWTException {
		homeTests.goToPolicyPage();
		dunningTests.activateFilterBy("Policy description");
		waitPageLoaded();
		dunningTests.filterPolicyByDescription(policy_description2);
		waitPageLoaded();
		Assert.assertEquals(dunningTests.policyTotal(), 1);
		
	}
}
