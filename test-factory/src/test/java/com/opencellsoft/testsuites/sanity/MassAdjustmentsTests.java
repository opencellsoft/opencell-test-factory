package com.opencellsoft.testsuites.sanity;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.operations.massadjustments.MassAdjustmentsListPage;

public class MassAdjustmentsTests extends TestBase {
	
	
	@Test(priority = 1)
	public void listAndFilters() throws InterruptedException {
		HomePage homePage = PageFactory.initElements(driver, HomePage.class);
		Thread.sleep(5000);
		homePage.clickOnOperationMenuButton();
		homePage.clickOnMassAdjustmentsButton();
		MassAdjustmentsListPage massAdjustmentsListPage = PageFactory.initElements(driver, MassAdjustmentsListPage.class);
		massAdjustmentsListPage.checkText("Mass Adjustment Job");
	}

	@Test(priority = 2)
	public void markwholeResult() {
		MassAdjustmentsListPage massAdjustmentsListPage = PageFactory.initElements(driver, MassAdjustmentsListPage.class);
		massAdjustmentsListPage.clickOnMarkWholeResultButton();
		Assert.assertEquals(massAdjustmentsListPage.getTextMessage(), "Action executed successfuly");
	}
	
	@Test(priority = 3)
	public void createCreditNotes() {
		MassAdjustmentsListPage massAdjustmentsListPage = PageFactory.initElements(driver, MassAdjustmentsListPage.class);
		massAdjustmentsListPage.clickOnCreateCreditNotesButton();
		massAdjustmentsListPage.clickOnConfirmButton();
		Assert.assertEquals(massAdjustmentsListPage.getTextMessage(), "Action executed successfuly");
	}
	
	@Test(priority = 4)
	public void unMarkwholeResult() {
		MassAdjustmentsListPage massAdjustmentsListPage = PageFactory.initElements(driver, MassAdjustmentsListPage.class);
		massAdjustmentsListPage.clickOnUnMarkWholeResultButton();
		Assert.assertEquals(massAdjustmentsListPage.getTextMessage(), "Action executed successfuly");
	}
	
	@Test(priority = 5)
	public void jobHistory() {
		MassAdjustmentsListPage massAdjustmentsListPage = PageFactory.initElements(driver, MassAdjustmentsListPage.class);
		massAdjustmentsListPage.clickOnJobHistoryTab();
		massAdjustmentsListPage.checkText("Mass Adjustment Job");
	}

}
