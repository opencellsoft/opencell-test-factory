package com.opencellsoft.testsuites.sanity;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.opencellsoft.tests.DunningTests;

public class DunningsettingsSanityTests extends SanityTestsCommons {	
	
	@Test(priority = 0)
	public void dunningsettingsSanityTests() {
		dunningTests = new DunningTests();
		System.out.println("**********************************   dunning-settings sanity tests  **********************************");
	}
	
	String pauseReason = "5 working days before payment";
	String pauseDescription = "Customer asks for 5 (working) days before paying";
	String stopReason = "Customer disputes invoice";
	String stopDescription = "Customer disputes invoice. Accounting department continues";
	
	/***   Update Maximum Number Of Dunning Levels  ***/
	@Test(priority = 502, enabled = true)
	public void UpdateMaximumNumberOfDunningLevels(){
		homeTests.goToDunningSettingPage();
		try {
			dunningTests.UpdateMaximumNumberOfDunningLevels("10");
		}catch (Exception e) {
			dunningTests.UpdateMaximumNumberOfDunningLevels("10");
		}
		
		homeTests.goToDunningSettingPage();
		waitPageLoaded();
		Assert.assertEquals(dunningTests.MaximumNumberOfDunningLevelsValue(), "10");
	}
	
	/***   Add Pause Reason  ***/
	@Test(priority = 508, enabled = true)
	public void updatePauseReason(){
		dunningTests.AddPauseReason(pauseReason, pauseDescription,"1");
		homeTests.goToDunningSettingPage();
		Assert.assertTrue(dunningTests.searchInPauseReasonTable(pauseReason));
	}
	
	/***   Add Stop Reason  ***/
	@Test(priority = 510, enabled = true)
	public void updateStopReason(){
		dunningTests.openStopReasontab();
		dunningTests.AddStopReason(stopReason, stopDescription,"1");
		homeTests.goToDunningSettingPage();
//		dunningTests.openStopReasontab();
		Assert.assertTrue(dunningTests.searchInStopReasonTable(stopReason));
	}
}
