package com.opencellsoft.testsuites.sanity;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencellsoft.tests.DunningTests;
import com.opencellsoft.utility.Constant;

public class TemplatesSanityTests extends SanityTestsCommons {
	DunningTests dunningTests;
	String template_name = "Template-" + Constant.offerCode;
	String template_language = "English";
	String template_channel = "Email";
	String template_object = "reminder of unpaid invoice";
	String template_body = "Mr, we would like to remind you that you have an unpaid invoice for the month of September. "
						+ "Please remember to pay your outstanding invoices so that you do not lose service.";
	
	String template_name2 = "Template2-" + Constant.offerCode;
	String template_language2 = "Français";
	Boolean template_active2 = true;
	String template_channel2 = "Letter";
	String template_object2 = null;
	String template_body2 = "Sir, we would like to remind you that you have until the end of the month to pay your balance "
							+ "before stopping the service. Thank you and good day ";
	
	@Test(priority = 0)
	public void templatesSanityTests() {
		dunningTests = new DunningTests();
		System.out.println("**********************************  Templates sanity tests  **********************************");
	}
	
	/***   create template  ***/
	@Test(priority = 104, enabled = true)
	public void createTemplate() throws InterruptedException, AWTException {
		homeTests.goToTemplatePage();
		dunningTests.createTemplate(template_name,template_language, template_channel, template_object, template_body);
		dunningTests.goBack();
	}
	/***   search templates by Name  ***/
	@Test(priority = 106, enabled = true)
	public void searchTemplatesByName() throws InterruptedException, AWTException {
		dunningTests.searchTemplateByName(template_name);
		waitPageLoaded();
		Assert.assertEquals(dunningTests.templateTotal(), 1);
	}
	
	/***   update template  ***/
	@Test(priority = 108, enabled = true)
	public void updateTemplate() throws InterruptedException, AWTException {
		dunningTests.goToTemplateDetails(template_name);
		dunningTests.updateTemplate(template_name2,template_language2,template_active2, template_channel2, template_object2, template_body2);
		dunningTests.goBack();
	}
	
	/***   filter by language  ***/
	@Test(priority = 110, enabled = true)
	public void filterTemplatesByLanguage() throws InterruptedException, AWTException {
		homeTests.goToTemplatePage();
		dunningTests.searchTemplateByName(template_name2);
		dunningTests.activateFilterBy("Language");
		waitPageLoaded();
		
		dunningTests.filterTemplateByLanguage(template_language2);
		waitPageLoaded();
		Assert.assertEquals(dunningTests.templateTotal(), 1);
		
		dunningTests.closeFilterBy("language");
	}
	
	/***   filter by channel  ***/
	@Test(priority = 112, enabled = true)
	public void filterTemplatesByChannel() throws InterruptedException, AWTException {
		homeTests.goToTemplatePage();
		dunningTests.searchTemplateByName(template_name2);
		dunningTests.activateFilterBy("Channel");
		waitPageLoaded();
		
		dunningTests.filterTemplateByChannel(template_channel2);
		waitPageLoaded();
		Assert.assertEquals(dunningTests.templateTotal(), 1);
		
		dunningTests.closeFilterBy("channel");
	}
	
	/***   sort Templates By Name  ***/
	@Test(priority = 114, enabled = true)
	public void sortTemplatesByName() throws InterruptedException, AWTException {
		dunningTests.sortTemplatesListBy("code");
		waitPageLoaded();
		Assert.assertEquals(dunningTests.templateTotal(), 1);
	}
	
	/***   delete template  ***/
	@Test(priority = 116, enabled = true)
	public void deleteTemplate() throws InterruptedException, AWTException {
		dunningTests.goToTemplateDetails(template_name2);
		dunningTests.delete();
	}
	
	/***   filter by channel  ***/
	@Test(priority = 118, enabled = true)
	public void filterTemplatesByChannel2() throws InterruptedException, AWTException {
		dunningTests.searchTemplateByName(template_name2);
		dunningTests.activateFilterBy("Channel");
		waitPageLoaded();
		
		dunningTests.filterTemplateByChannel(template_channel2);
		waitPageLoaded();
		Assert.assertEquals(dunningTests.templateTotal(), 0);
		
	}
}
