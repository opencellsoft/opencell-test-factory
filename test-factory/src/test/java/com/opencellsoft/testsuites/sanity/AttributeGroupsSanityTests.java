package com.opencellsoft.testsuites.sanity;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencellsoft.tests.AttributeGroupsTests;
import com.opencellsoft.utility.Constant;

public class AttributeGroupsSanityTests extends SanityTestsCommons {	
	AttributeGroupsTests attributeGroupsTests;
	String attributeGroup_Code = "AttrG-"+ Constant.offerCode;
	String attributeGroup_description = "AttrG_firstDescription-"+ Constant.offerCode;
	String attribute1 = "product_quantity";
	String attributeGroup_description2 = "AttrG_secondDescription-"+ Constant.offerCode;
	
	@Test(priority = 0)
	public void attributeGroupsSanityTestsextends() {
		attributeGroupsTests = new AttributeGroupsTests();
		System.out.println("**********************************   attribute Groups Tests  **********************************");
	}

	/***   create Attribute Group  ***/
	@Test(priority = 104, enabled = true)
	public void createAttributeGroup() throws InterruptedException, AWTException {
		homeTests.goToAttributeGroupsPage();
		attributeGroupsTests.createAttributeGroups(attributeGroup_Code,attributeGroup_description, attribute1);
		attributeGroupsTests.goBack();
	}
	/***   search Attribute Groups By Name  ***/
	@Test(priority = 106, enabled = true)
	public void searchAttributeGroupsByName() throws InterruptedException, AWTException {
		attributeGroupsTests.searchAttributeGroupByCode(attributeGroup_Code);
		waitPageLoaded();
		Assert.assertEquals(attributeGroupsTests.attributeGroupsTotal(), 1);
	}
	
	/***   update Attribute Group  ***/
	@Test(priority = 108, enabled = true)
	public void updateAttributeGroup() throws InterruptedException, AWTException {
		attributeGroupsTests.goToAttributeGroupsDetails(attributeGroup_Code);
		attributeGroupsTests.updateAttributeGroups(attributeGroup_description2);
		attributeGroupsTests.goBack();
	}
	
	/***   filter Attribute Group By Empty  ***/
	@Test(priority = 110, enabled = false)
	public void filterAttributeGroupByEmpty() throws InterruptedException, AWTException {
		attributeGroupsTests.filterAttributeGroupsByEmpty(false);
		waitPageLoaded();
		Assert.assertEquals(attributeGroupsTests.attributeGroupsTotal(), 1);
	}
	
	/***   filter Attribute Group By Description  ***/
	@Test(priority = 110, enabled = true)
	public void filterAttributeGroupByDescription() throws InterruptedException, AWTException {
		attributeGroupsTests.activateFilterBy("Description");
		waitPageLoaded();
		
		attributeGroupsTests.filterAttributeGroupsByDescription(attributeGroup_description2);
		waitPageLoaded();
		Assert.assertEquals(attributeGroupsTests.attributeGroupsTotal(), 1);
		
		attributeGroupsTests.closeFilterBy("description");
	}
	
	/***   sort Attribute Groups Description  ***/
	@Test(priority = 114, enabled = true)
	public void sortAttributeGroupsDescription() throws InterruptedException, AWTException {
		attributeGroupsTests.sortAttributeGroupsListBy("description");
		waitPageLoaded();
		Assert.assertEquals(attributeGroupsTests.attributeGroupsTotal(), 1);
	}
}
