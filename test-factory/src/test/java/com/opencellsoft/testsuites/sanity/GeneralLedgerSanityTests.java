package com.opencellsoft.testsuites.sanity;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.opencellsoft.tests.GeneralLedgerTests;

public class GeneralLedgerSanityTests extends SanityTestsCommons {

	@Test(priority = 0)
	public void generalLedgerSanityTests() {
		generalLedgerTests = new GeneralLedgerTests();
		System.out.println("**********************************  General Ledger Sanity Tests  **********************************");
	}
	
	/***   filter By Acounting Name  ***/
	@Test(priority = 4, enabled = true)
	public void filterByAcountingName() {
		homeTests.goToGeneralLedgerPage();
		generalLedgerTests.activateFilterBy("Accounting name");
		generalLedgerTests.filterByAccountingName("Revenue");
		Assert.assertTrue(generalLedgerTests.alertMsg());
		generalLedgerTests.filterByAccountingName("VAT collected");
		Assert.assertTrue(generalLedgerTests.alertMsg());
	}
	
	/***   filter By Acounting Name  ***/
	@Test(priority = 4, enabled = true)
	public void filterByFunctionalCurrency() {
		homeTests.goToGeneralLedgerPage();
		generalLedgerTests.filterByFunctionalCurrency(true);
		Assert.assertTrue(generalLedgerTests.alertMsg());
		generalLedgerTests.filterByFunctionalCurrency(false);
		Assert.assertTrue(generalLedgerTests.alertMsg());
	}
	/***   export  ***/
	@Test(priority = 6, enabled = true)
	public void export(){
		generalLedgerTests.export("CSV");
		Assert.assertTrue(generalLedgerTests.alertMsg());
		generalLedgerTests.export("XLSX");
		Assert.assertTrue(generalLedgerTests.alertMsg());
	}
	
}
