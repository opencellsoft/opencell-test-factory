package com.opencellsoft.testsuites.sanity;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.opencellsoft.tests.SecurityDepositTests;
import com.opencellsoft.utility.Constant;

public class SecurityDepositSanityTests extends SanityTestsCommons {

	String BC_code 		 = "SDBC-" + Constant.offerCode;
	String customerCode  = "SDCUS-" + Constant.offerCode;
	String SDName1       = "SDName1-" + Constant.offerCode;
	String SDName2       = "SDName2-" + Constant.offerCode;
	String amount = "100";
	String template = "1";
	String seller = "MAIN_SELLER";
	
	@Test(priority = 0)
	public void securityDepositSanityTests() throws InterruptedException {
		securityDepositTests = new SecurityDepositTests();
		System.out.println("**********************************  security Deposit Sanity Tests  **********************************");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,BC_code,BC_code,"BILLINGACCOUNT","MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,BC_code,BC_code,"BILLINGACCOUNT","CAL_PERIOD_MONTHLY");
		Thread.sleep(500);
		payloadTests.createCustomer(baseURI, login, password, customerCode, customerCode, BC_code);
		Thread.sleep(500);
		payloadTests.createSecurityDepositByAPI(baseURI, login, password, customerCode, customerCode, amount, template, seller, SDName1);
		Thread.sleep(500);
		payloadTests.createSecurityDepositByAPI(baseURI, login, password, customerCode, customerCode, amount, template, seller, SDName2);
		Thread.sleep(500);
	}
	
	/***   search By Customer  ***/
	@Test(priority = 4, enabled = true)
	public void searchByCustomer() {
		homeTests.goToSecurityDepositPage();
		securityDepositTests.searchByCustomerInSDReport(customerCode);
		Assert.assertEquals(securityDepositTests.elementsFoundSize(customerCode), 2);		
	}
	
	/***   search By Security Deposit Name  ***/
	@Test(priority = 6, enabled = true)
	public void searchBySecurityDepositName() {
		homeTests.goToSecurityDepositPage();
		securityDepositTests.searchBySecurityDepositName(SDName2);
		waitPageLoaded();
		Assert.assertEquals(securityDepositTests.elementsFoundSize(customerCode), 1);	
		Assert.assertEquals(securityDepositTests.elementsFoundSize(SDName2), 1);	
	}
	
	/***   search By Security Deposit Name  ***/
	@Test(priority = 8, enabled = true)
	public void filterByCurrency() {
		securityDepositTests.activateFilterBy("Currency");
		securityDepositTests.searchBySecurityDepositName("");
		securityDepositTests.searchByCustomerInSDReport(customerCode);
		securityDepositTests.filterByCurrency("EUR");
		Assert.assertEquals(securityDepositTests.elementsFoundSize(customerCode), 2);
		securityDepositTests.deleteFilterByCurrency("EUR");
		securityDepositTests.filterByCurrency("USD");
		Assert.assertEquals(securityDepositTests.elementsFoundSize(customerCode), 0);	
	}
}
