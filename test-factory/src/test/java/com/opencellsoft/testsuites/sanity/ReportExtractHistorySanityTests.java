package com.opencellsoft.testsuites.sanity;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencellsoft.tests.ReportExtractHistoryTests;
import com.opencellsoft.tests.ReportExtractsTests;

public class ReportExtractHistorySanityTests extends SanityTestsCommons {
	
	String reportExtractsCode = "EDR";
	
	@Test(priority = 0)
	public void reportExtractHistorySanityTests() {
		reportExtractsTests = new ReportExtractsTests();
		System.out.println("**********************************   Report Extract History tests  **********************************");
	}
	
	/***   sort ReportExtracts By Name  ***/
	@Test(priority = 106, enabled = true)
	public void sortReportExtractsByName() {
		homeTests.goToReportExtractsHistoryPage();
		int n = reportExtractsTests.reportExtractsTotal();
		reportExtractsTests.sortReportExtractsListBy("category");
		waitPageLoaded();
		Assert.assertEquals(reportExtractsTests.reportExtractsTotal(), n);
	}
	
	/***   search ReportExtracts by Name  ***/
	@Test(priority = 108, enabled = true)
	public void searchReportExtractsByCode(){
		reportExtractsTests.searchReportExtractsByName(reportExtractsCode);
		waitPageLoaded();
		Assert.assertTrue(reportExtractsTests.reportExtractsTotal()> 1);
	}
	
	/***   download report  ***/
	@Test(priority = 112, enabled = true)
	public void downloadReport() {
		reportExtractsTests.downloadReport();
	}
}
