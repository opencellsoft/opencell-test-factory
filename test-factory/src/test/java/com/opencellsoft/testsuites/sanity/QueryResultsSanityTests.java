package com.opencellsoft.testsuites.sanity;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencellsoft.tests.QueryBuilderTests;
import com.opencellsoft.tests.QueryResultTests;
import com.opencellsoft.utility.Constant;

public class QueryResultsSanityTests extends SanityTestsCommons {

	String queryName = "queryTest2" + Constant.offerCode;
	String scopeEntity = "BillingCycle";
	String selected_field1 = "code";
	String selected_field2 = "description";
	String selected_field3 = null;
	String selected_field4 = "ignoreOrders";
	String filter_field = "description";
	String filter_operator = "Contains";
	String filter_value = "BR";
	int total;
	
	@Test(priority = 0)
	public void queryResultSanityTests() {
		queryResultTests = new QueryResultTests();
		queryBuilderTests = new QueryBuilderTests();
		System.out.println("**********************************  Query Result Sanity Tests  **********************************");
	}
	
	/***   create Query   ***/
	@Test(priority = 102, enabled = true)
	public void createQuery() throws InterruptedException {
		homeTests.goToQueryBuilderPage();
		waitPageLoaded();
		queryBuilderTests.createQuery(queryName, scopeEntity,selected_field1,selected_field2,selected_field3, filter_field, filter_operator, filter_value);
		queryBuilderTests.executeQuery();
		Thread.sleep(1000);
		queryBuilderTests.executeQueryOnBackGround();
		Thread.sleep(1000);
		queryBuilderTests.executeQueryOnBackGround();
		Thread.sleep(1000);
		queryBuilderTests.executeQueryOnBackGround();
		Thread.sleep(1000);
	}
	
	/***   sort Query By Status  ***/
	@Test(priority = 104, enabled = true)
	public void sortQueryByStatus() throws InterruptedException, AWTException {
		homeTests.goToQueryResultsPage();
		total = queryResultTests.queriesTotal();
		queryResultTests.sortQueryListBy("queryStatus");
		waitPageLoaded();
		Assert.assertEquals(queryResultTests.queriesTotal(), total);
	}
	
	/***   search Query By Name  ***/
	@Test(priority = 106, enabled = true)
	public void searchQueryByName() {
		queryResultTests.searchQueryByName(queryName);
		Assert.assertEquals(queryResultTests.queriesTotal() , 3);
	}
	
	/***   search Query By Name  ***/
	@Test(priority = 108, enabled = true)
	public void deleteQueryResultFromTheList() {
		queryResultTests.deleteQuery();
		queryResultTests.searchQueryByName(queryName);
		Assert.assertEquals(queryResultTests.queriesTotal(), 2);
	}
	
	/***   download Query  ***/
	@Test(priority = 110, enabled = true)
	public void downloadQueryResult() {
		queryResultTests.downloadQueryResult(1);
		Assert.assertEquals(queryResultTests.messageText(), "Action executed successfuly");
	}
	
	/***   search Query By Name  ***/
	@Test(priority = 112, enabled = true)
	public void deleteAllQueriesFromTheList() {
		queryResultTests.deleteAllQueries();
		queryResultTests.searchQueryByName(queryName);
		Assert.assertEquals(queryResultTests.queriesTotal(), 0);
	}
	
	/***   search Query By Name  ***/
	@Test(priority = 114, enabled = true)
	public void searchQueryByName2() {
		queryResultTests.searchQueryByName(queryName);
		Assert.assertEquals(queryResultTests.queriesTotal(), 0);
	}
}
