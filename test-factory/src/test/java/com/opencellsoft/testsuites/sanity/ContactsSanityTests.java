package com.opencellsoft.testsuites.sanity;

import java.awt.AWTException;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencellsoft.tests.BillingrunTests;
import com.opencellsoft.tests.ContactTests;
import com.opencellsoft.tests.jobs.PayloadTests;
import com.opencellsoft.utility.Constant;

public class ContactsSanityTests extends SanityTestsCommons {
	ContactTests contactTests;
	String customerCode = "Contact_Customer-"+ Constant.offerCode;
	String bcCode = "Contact_bc-" + Constant.offerCode;
	String name2 = "Contact_name-" + Constant.offerCode;
	String surname2 = "Contact_surname-" + Constant.offerCode;
	String email =  "Contact_" + Constant.offerCode + "@opencellsoft.com";
	
	@Test(priority = 0)
	public void contactsSanityTests() {
		contactTests = new ContactTests();
		payloadTests = new PayloadTests();
		billingrunTests= new BillingrunTests();
		System.out.println("********************************** Contact sanity tests  **********************************");
	}
	
	
	/***   create contact  ***/
	@Test(priority = 4, enabled = true)
	public void createContact() throws InterruptedException, AWTException {
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bcCode,bcCode,"BILLINGACCOUNT", "MONTHLY");
		Thread.sleep(500);
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bcCode,bcCode,"BILLINGACCOUNT", "CAL_PERIOD_MONTHLY");
		Thread.sleep(500);
		payloadTests.createCustomer(baseURI, login, password, customerCode, customerCode , bcCode);
		homeTests.goToContactsPage();
		contactTests.createContact(null, "Open", "Cell", "+33445566", email, "fr", "Paris", customerCode, "1", true, "2");
		contactTests.goBack();
	}
	
	/***   search for contact  ***/
	@Test(priority = 4, enabled = true)
	public void searchForContactByEmail() throws InterruptedException, AWTException {
		contactTests.searchContact(email);
		waitPageLoaded();
		contactTests.searchContact(email);
		waitPageLoaded();
		Assert.assertEquals(contactTests.contactTotal(), 1);
		
	}
	
	/***   search for contact  ***/
	@Test(priority = 6, enabled = false)
	public void sortContactListBySurname() throws InterruptedException, AWTException {
		contactTests.sortContactListBy("lastName");
		waitPageLoaded();
		Assert.assertEquals(contactTests.contactTotal(), 1);
		
		contactTests.sortContactListBy("firstName");
		Assert.assertEquals(contactTests.contactTotal(), 1);
	}
	
	/***   update contact  ***/
	@Test(priority = 8, enabled = true)
	public void updateContact() throws InterruptedException, AWTException {
		contactTests.goToContactDetails(email);
		contactTests.updateContact(null, name2, surname2, "+212334455", null, null, null, null, null, null, null);
		contactTests.goBack();
	}
	
	/***   search for contact  ***/
	@Test(priority = 10, enabled = true)
	public void searchForContactByUsername() throws InterruptedException, AWTException {
		contactTests.searchContact(name2);
		waitPageLoaded();
		Assert.assertEquals(contactTests.contactTotal(), 1);
		
	}
}
