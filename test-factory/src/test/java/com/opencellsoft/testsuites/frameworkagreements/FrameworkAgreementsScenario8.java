package com.opencellsoft.testsuites.frameworkagreements;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.utility.Constant;

public class FrameworkAgreementsScenario8 extends FrameworkAgreementsCommons {
//	String code = code;
	String code = "SDG4984";
	String offerCode            = "FAOFF8-" + code;
	String productCode          = "FAPROD8-" + code;
	String recurringChargeCode  = "FARecCH8-" + code;
	String oneShotSubChargeCode = "FAOneShotCH8-" + code;
	
	String bcCode               = "FABC8-" + Constant.offerCode;
	String customerCode         = "FACUST8_" + Constant.offerCode;
	String consumerCode         = "FACONS8_" + Constant.offerCode;
	String offerDescription     = "FAOFF8" + Constant.offerCode;
	String productDescription   = "FAPROD8" + Constant.offerCode;
	String recurringChargeDescription  = "FARecurringChargeDesc8" + Constant.offerCode;
	String oneShotChargeDescription    = "FAOneShotChargeDesc8" + Constant.offerCode;
	String pricePlanVersionCode        = "FAPV8" + Constant.offerCode;
	String recurringChargeAmount       = "88.00";
	String oneShotSubscriptionChargeAmount = "66.00";
	String newprice1 ="44.00";
	String newprice2 ="33.00";
	String subCode1  = "FASUB81_"+ Constant.offerCode;
	String AP1       = "FAAP81_"+code;
	String subCode2  = "FASUB82_"+ Constant.offerCode;
	String AP2       = "FAAP82_"+code;
	String contractCode        = "FACode8_" + Constant.offerCode;
	String contractDescription = "FADesc8_" + Constant.offerCode ;
	String contractlineCode1   = "C8CL1_" + Constant.offerCode;
	String contractlineCode2   = "C8CL2_" + Constant.offerCode;
	String companyRegistration = getRandomNumberbetween1And200();


	@Test(priority = 0, enabled = true)
	public void _FA_T08() {
		System.out.println("************************** 8 - OneShot Subscription charge type and Recurring charge type ***************************");
		System.out.println("String code = \"" + Constant.offerCode  + "\";");
	}

	/***  create a New Offer  ***/
	@Parameters({ "PriceVersionDate", "repetitionCalendar", "defaultQuantity", "minimalQuantity", "maximalQuantity", "sequence"})
	@Test(priority = 6, enabled = true)
	public void _FA_T08_createOffer(String PriceVersionDate, String repetitionCalendar, String defaultQuantity, String minimalQuantity, String maximalQuantity, String sequence) throws InterruptedException, AWTException {
		offersTests.createOfferMethod(offerCode, offerDescription);
		waitPageLoaded();
		productsTests.createProductMethod(PriceVersionDate,productCode,productDescription );
		
		//  Create a new Recurring charge 
		chargesTests.createRecurringCharge(recurringChargeCode,recurringChargeDescription,repetitionCalendar,version);
		Thread.sleep(3000);
		chargesTests.createNewPriceVersion(pricePlanVersionCode, recurringChargeAmount);
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(recurringChargeDescription, version);
		
		//  Create a new Oneshot charge
		chargesTests.createOneshotCharge(oneShotSubChargeCode, oneShotChargeDescription, "Subscription",version);
		Thread.sleep(2000);
		chargesTests.createNewPriceVersion(pricePlanVersionCode,oneShotSubscriptionChargeAmount );
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(oneShotChargeDescription, version);
		productsTests.publishProductVersion();
		productsTests.activateProductAndGoBack();
		homeTests.goToOffersPage();
		offersTests.searchForOfferMethod(offerCode, offerDescription);
		offersTests.pickProductToOffer( productCode,productDescription, version);
		offersTests.fillProductParametersMethod(defaultQuantity,minimalQuantity, maximalQuantity, sequence, false );
		offersTests.ActivateOfferMethod();
		waitPageLoaded();
	}

	/***   Create and Activate a new Subscription   ***/
	@Test(priority = 20, enabled = true)
	public void _FA_T08_createAndActivateSubscription() throws AWTException, InterruptedException{
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bcCode,bcCode,"BILLINGACCOUNT", "MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bcCode,bcCode,"BILLINGACCOUNT", "CAL_PERIOD_MONTHLY");
		Thread.sleep(500);
		payloadTests.createCustomer(baseURI, login, password, customerCode, customerCode ,bcCode);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode1, customerCode, this.offerCode, this.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode1, this.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP1, subCode1);
		Thread.sleep(1000);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 22, enabled = true)
	public void _FA_T08_checkwalletOperations1() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode1);

		// Verify the number of lines found 
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(subCode1), 2);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(recurringChargeAmount), 1);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(oneShotSubscriptionChargeAmount), 1);
	}

	/***   Create new Framework Agreement   ***/
	@Parameters({ "level"})
	@Test(priority = 24)
	public void _FA_T08_createFrameworkAgreement( String level) {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();
		frameworkAgreementsTests.createNewframeworkAgreementInPresent(contractCode, contractDescription, level,customerCode);
	}

	/***   Add contract line to Framework Agreement   ***/
	@Parameters({ "CLTypeGlobalPrice"})
	@Test(priority = 26, enabled = true)
	public void _FA_T08_addContractLineRecurringCharge(String CLTypeGlobalPrice) {
		frameworkAgreementsTests.AddContractLineGlobalPrice(contractlineCode1, contractlineCode1, offerCode, productCode, recurringChargeCode,CLTypeGlobalPrice, newprice1 );
	}

	/***   Add contract line to Framework Agreement   ***/
	@Parameters({ "CLTypeGlobalPrice"})
	@Test(priority = 28, enabled = true)
	public void _FA_T08_addContractLineOneshotCharge(String CLTypeGlobalPrice) {
		frameworkAgreementsTests.AddContractLineGlobalPrice(contractlineCode2, contractlineCode2, offerCode, productCode, oneShotSubChargeCode,CLTypeGlobalPrice, newprice2 );
	}

	/***   activate contract   ***/
	@Test(priority = 30)
	public void _FA_T08_activateContract() {
		frameworkAgreementsTests.activateContract();
	}
	/***   Create and Activate a new Subscription   ***/
	@Parameters({ "seller" })
	@Test(priority = 32, enabled = true)
	public void _FA_T08_createAndActivateSubscription2(String seller) throws AWTException, InterruptedException{
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode2, customerCode, this.offerCode, this.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode2, this.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP2, subCode2);
		Thread.sleep(1000);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 34, enabled = true)
	public void _FA_T08_checkwalletOperations2() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode2);

		// Verify the number of lines found 
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(subCode2), 2);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(newprice1), 1);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(newprice2), 1);
	}

	/***   Cancel contract ***/
	@Test(priority = 36)
	public void _FA_T08_cancelFrameworkAgreement() {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();

		// search FA by code
		frameworkAgreementsTests.searchContractByCode(contractCode);

		// close FA
		frameworkAgreementsTests.closeContract();
	}
}
