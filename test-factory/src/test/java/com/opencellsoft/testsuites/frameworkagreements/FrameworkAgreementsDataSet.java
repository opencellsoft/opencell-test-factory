package com.opencellsoft.testsuites.frameworkagreements;

import java.awt.AWTException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.utility.Constant;

public class FrameworkAgreementsDataSet extends FrameworkAgreementsCommons {
	
//	String offerCode = "FAOFF-" + Constant.offerCode;
//	String productCode = "FAPROD-" + Constant.offerCode;
//	String usageChargeCode = "FAusage1-" + Constant.offerCode;
//	String oneShotOtChargeCode = "FAoneShotOt1-" + Constant.offerCode;
//	String customerCode = "FACUST1-" + Constant.offerCode;
	String offerCode = "FAOFF1-001";
	String productCode = "FAPROD1-001";
	String usageChargeCode = "FAusage1-001";
	String oneShotOtChargeCode = "FAoneShotOt1-001";
	String customerCode = "FACUST1-001";
	String bcCode = "FABC-001";
	
	String offerDescription = Constant.offerDescription;
	String productDescription = Constant.productDescription;
	String oneShotOtChargeDescription= "oneShotOt1" + Constant.oneShotChargeDescription;
	String pricePlanVersionCode_Charge2 = "FAPV2-" + Constant.oneShotChargeCode;
	String usageChargeDescription= Constant.chargeDescriptionUsage;
	String pricePlanVersionCode_Charge3 = "FAPV3-" + Constant.usageChargeCode;
	String customerName = Constant.customerName;
	String customerEmail = Constant.customerEmail;
	String paypalUserId = Constant.paypalUserId;
	String consumerCode = "FACONS-" +Constant.consumerCode;
	String consumerName = Constant.consumerName;
	String companyRegistrationNumber = Constant.companyRegistrationNumber;
	String vatNumber = Constant.vatNumber;
	String usageChargeAmount = "10.00";
	String oneShotOtherChargeAmount = "20.00";
	String oneShotSubscriptionChargeAmount = "30.00";
	String recurringChargeAmount = "40.00";
	String companyRegistration = getRandomNumberbetween1And200();
	

	@Test(priority = 0, enabled = true)
	public void _FA_DS(){
		System.out.println("***************** 1 - FrameworkAgreements : data set  *****************");
		System.out.println("String offerCode = \"" + offerCode + "\";");
		System.out.println("String productCode = \"" + productCode + "\";");
		System.out.println("String usageChargeCode = \"" + usageChargeCode + "\";");
		System.out.println("String oneShotOtChargeCode = \"" + oneShotOtChargeCode + "\";");
		System.out.println("String customerCode = \"" + customerCode + "\";");
	}

	/***  create a New Offer   ***/
	@Parameters({ "PriceVersionDate", "defaultQuantity", "minimalQuantity", "maximalQuantity", "sequence"})
	@Test(priority = 4, enabled = true)
	public void _FA_DS_createOffer(String PriceVersionDate,String defaultQuantity, String minimalQuantity, String maximalQuantity, String sequence ) throws InterruptedException, AWTException {
		offersTests.createOfferMethod(offerCode, offerDescription);
		productsTests.createProductMethod(PriceVersionDate,productCode,productDescription );
		chargesTests.createUsageCharge(usageChargeCode,usageChargeDescription,"","", version);
		Thread.sleep(3000);
		chargesTests.createNewPriceVersionPriceGrid(pricePlanVersionCode_Charge3);
		chargesTests.addUnitPriceToPriceVersion(usageChargeAmount);
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(usageChargeDescription, version);
		chargesTests.createOneshotCharge(oneShotOtChargeCode, oneShotOtChargeDescription, "Other", version);
		Thread.sleep(2000);
		chargesTests.createNewPriceVersion(pricePlanVersionCode_Charge2, oneShotOtherChargeAmount);
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(oneShotOtChargeDescription, version);
		productsTests.publishProductVersion();
		productsTests.activateProductAndGoBack();
		homeTests.goToOffersPage();
		offersTests.searchForOfferMethod(offerCode, offerDescription);
		offersTests.pickProductToOffer( productCode,productDescription, version);
		offersTests.fillProductParametersMethod(defaultQuantity,minimalQuantity, maximalQuantity, sequence, false );
		offersTests.ActivateOfferMethod();
	}
	
	@Test(priority = 6, enabled = true)
	/***  Activate Functions   ***/
	public void _APM_DS_ActivateFunctions() throws InterruptedException {
		payloadTests.activateFunctions( baseURI, login, password);
	}
	
	@Test(priority = 8, enabled = true)
	/***   create Billing Cycle   ***/
	public void _APM_DS_createBillingCycle() throws InterruptedException {
			payloadTests.createBillingCycleByAPI(baseURI, login, password,bcCode,bcCode,"BILLINGACCOUNT", "MONTHLY");
			payloadTests.createBillingCycleByAPI(baseURI, login, password,bcCode,bcCode,"BILLINGACCOUNT", "CAL_PERIOD_MONTHLY");
	}
}
