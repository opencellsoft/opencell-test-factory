package com.opencellsoft.testsuites.frameworkagreements;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class FrameworkAgreementsScenario7 extends FrameworkAgreementsCommons {

	FrameworkAgreementsDataSet dataSet = new FrameworkAgreementsDataSet();
	String customerCode = "FACUST7_"+Constant.offerCode;
	String consumerCode = "FACONS7_"+Constant.offerCode;
	String offerCode = dataSet.offerCode;
	String productCode = dataSet.productCode;
	String usageChargeCode = dataSet.usageChargeCode;
	String usageChargeAmount = dataSet.usageChargeAmount;
	String subCode = "FASUB7_"+ Constant.subscriptionCode;
	String AP = "FAAP7"+Constant.accessPointCode;

	String contractCode1 = "FAC71_" + Constant.contractCode;
	String contractDescription1 = "FAC71_" + Constant.contractDescription ;
	String contractlineCode1 = "FAC71CL1_" + Constant.contractLineCode;
	String contractlineDescription1 = "FAC71CL1_" + Constant.contractLineDescription;
	String contractCode2 = "FAC72_" + Constant.contractCode;
	String contractDescription2 = "FAC72_" + Constant.contractDescription ;
	String contractlineCode2 = "FAC72CL1_" + Constant.contractLineCode;
	String contractlineDescription2 = "FAC72CL1_" + Constant.contractLineDescription;
	String newprice1 = "8.00";
	String newprice2 = "9.00";
	String companyRegistration = getRandomNumberbetween1And200();


	@Test(priority = 2, enabled = true)
	public void _FA_T07() {
		System.out.println("**************************   7 - 2 framworkagreements ( 1 in the past & 1 in the futur ) ***************************");
	}

	/***   Create a new customer &  consumer  ***/
	@Test(priority = 3, enabled = true)
	public void _FA_T07_createCustomerAndSubscription() throws InterruptedException {
		// create new billing cycle
		payloadTests.createBillingCycleByAPI(baseURI, login, password,dataSet.bcCode, dataSet.bcCode,"BILLINGACCOUNT", "MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,dataSet.bcCode, dataSet.bcCode,"BILLINGACCOUNT", "CAL_PERIOD_MONTHLY");
		Thread.sleep(500);
		payloadTests.createCustomer(baseURI, login, password, customerCode, dataSet.customerName , dataSet.bcCode);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, offerCode, productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);
	}

	/***   Create new Framework Agreement   ***/
	@Parameters({ "level", "CLTypeGlobalPrice"})
	@Test(priority = 6)
	public void _FA_T07_createFrameworkAgreementInThePast( String level, String CLTypeGlobalPrice) {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();
		frameworkAgreementsTests.createNewframeworkAgreementInPast(contractCode1, contractDescription1, level,customerCode);
		frameworkAgreementsTests.AddContractLineGlobalPrice(contractlineCode1, contractlineDescription1, offerCode, productCode, usageChargeCode,CLTypeGlobalPrice, newprice1 );
		frameworkAgreementsTests.activateContract();
	}

	/***   Create new Framework Agreement   ***/
	@Parameters({ "level", "CLTypeGlobalPrice"})
	@Test(priority = 12)
	public void _FA_T07_createFrameworkAgreementInTheFutur( String level, String CLTypeGlobalPrice) {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();
		frameworkAgreementsTests.createNewframeworkAgreementInFutur(contractCode2, contractDescription2, level,customerCode);
		frameworkAgreementsTests.AddContractLineGlobalPrice(contractlineCode2, contractlineDescription2, offerCode, productCode, usageChargeCode,CLTypeGlobalPrice, newprice2 );
		frameworkAgreementsTests.activateContract();
	}

	/***   Insert EDRs   ***/
	@Test(priority = 18, enabled = true)
	public void _FA_T07_insertChargeCdrInThePast() throws InterruptedException {
		int result1 = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","","past");
		Assert.assertEquals(result1, 1);
	}

	/***   Insert EDRs   ***/
	@Test(priority = 22, enabled = true)
	public void _FA_T07_insertChargeCdrInThePresent() throws InterruptedException {
		int result1 = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","","");
		Assert.assertEquals(result1, 1);
	}

	/***   Insert EDRs   ***/
	@Test(priority = 26, enabled = true)
	public void _FA_T07_insertChargeCdrInTheFutur() throws InterruptedException {
		int result1 = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","","futur");
		Assert.assertEquals(result1, 1);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 30, enabled = true)
	public void _FA_T07_checkwalletOperations3() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);

		// Verify the number of lines found 
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(subCode), 3);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(newprice1), 1);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(usageChargeAmount), 1);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(newprice2), 1);
	}

	/***   Cancel contract ***/
	@Test(priority = 32)
	public void _FA_T07_cancelFrameworkAgreements() {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();

		// search FA by code
		frameworkAgreementsTests.searchContractByCode(contractCode1);

		// close FA
		frameworkAgreementsTests.closeContract();

		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();

		// search FA by code
		frameworkAgreementsTests.searchContractByCode(contractCode2);

		// close FA
		frameworkAgreementsTests.closeContract();

	}

}
