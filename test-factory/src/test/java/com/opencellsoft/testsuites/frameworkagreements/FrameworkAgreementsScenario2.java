package com.opencellsoft.testsuites.frameworkagreements;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.utility.Constant;

public class FrameworkAgreementsScenario2 extends FrameworkAgreementsCommons {
	FrameworkAgreementsDataSet dataSet = new FrameworkAgreementsDataSet();
	String customerCode = "FACUST2_"+Constant.offerCode;
	String consumerCode = "FACONS2_"+Constant.offerCode;
	String offerCode = dataSet.offerCode;
	String productCode = dataSet.productCode;
	String usageChargeCode = dataSet.usageChargeCode;
	String oneShotChargeCode1 = dataSet.oneShotOtChargeCode;
	String usageChargeAmount = dataSet.usageChargeAmount;
	String oneShotOtherChargeAmount = dataSet.oneShotOtherChargeAmount;
	String subCode = "FASUB2_"+Constant.subscriptionCode;
	String AP = "FAAP2"+Constant.accessPointCode;
	String contractCode = "FAC2_" + Constant.contractCode;
	String contractDescription = "FAC2_" + Constant.contractDescription ;
	String contractlineCode1 = "FAC2CL1_" + Constant.contractLineCode;
	String contractlineDescription1 = "FAC2CL1_" + Constant.contractLineDescription;
	String contractlineCode2 = "FAC2CL2_" + Constant.contractLineCode;
	String contractlineDescription2 = "FAC2CL2_" + Constant.contractLineDescription;

	String discount1 = "40";
	String discount2 = "20";
	String amountWithDiscount1 = "6.00";
	String amountWithDiscount2 = "16.00";
	String companyRegistration = getRandomNumberbetween1And200();


	@Test(priority = 0, enabled = true)
	public void _FA_T02() {
		System.out.println("************************** 2 - FrameworkAgreements with CLType : Global Discount  ***************************");
	}

	/***   Create a new customer &  consumer  ***/
	@Test(priority = 3, enabled = true)
	public void _FA_T02_createCustomerAndSubscription() throws InterruptedException {
		payloadTests.createCustomer(baseURI, login, password, customerCode, dataSet.customerName , dataSet.bcCode);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, offerCode, productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);
	}

	/***   Create new Framework Agreement   ***/
	@Parameters({ "level"})
	@Test(priority = 18, enabled = true)
	public void _FA_T02_createFrameworkAgreement( String level) {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();
		frameworkAgreementsTests.createNewframeworkAgreementInPresent(contractCode, contractDescription, level, customerCode);
	}

	/***   Add contract line to Framework Agreement   ***/
	@Parameters({ "CLTypeGlobalDiscount"})
	@Test(priority = 20, enabled = true)
	public void _FA_T02_addContractLineUsageCharge(String CLTypeGlobalDiscount) {
		frameworkAgreementsTests.AddContractLineGlobalDiscount(contractlineCode1, contractlineDescription1, offerCode, productCode, usageChargeCode,CLTypeGlobalDiscount, discount1, false );
	}

	/***   Add contract line to Framework Agreement   ***/
	@Test(priority = 22, enabled = true)
	@Parameters({ "CLTypeGlobalDiscount"})
	public void _FA_T02_addContractLineOneShotCharge(String CLTypeGlobalDiscount) {
		frameworkAgreementsTests.AddContractLineGlobalDiscount(contractlineCode2, contractlineDescription2, offerCode, productCode, oneShotChargeCode1,CLTypeGlobalDiscount, discount2, false );
	}

	/***   activate contract   ***/
	@Test(priority = 24, enabled = true)
	public void _FA_T02_activateContract() {
		frameworkAgreementsTests.activateContract();
	}

	/***   Insert EDRs, Before Framework Agreement StartDate   ***/
	@Test(priority = 26, enabled = true)
	public void _FA_T02_insertUsageChargeCdrBeforedFAStartDate() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","before","");
		Assert.assertEquals(result, 1);
	}	

	/***   Insert EDRs, Before Framework Agreement StartDate   ***/
	@Test(priority = 28, enabled = false)
	public void _FA_T02_applyOneShotChargeInstanceBeforeFAStartDate() throws InterruptedException {
		String result = payloadTests.applyOneShotChargeInstance(baseURI, login, password, oneShotChargeCode1, subCode,"before","");
		Assert.assertEquals(result, "SUCCESS");
	}

	/***   Insert EDRs, In Framework Agreement  period ***/
	@Test(priority = 32)
	public void _FA_T02_insertUsageChargeCdrInFAPeriod() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","in","");
		Assert.assertEquals(result, 1);
	}

	/***   Insert EDRs, In Framework Agreement  period ***/

	@Test(priority = 34)
	public void _FA_T02_applyOneShotChargeInstanceInFAPeriod() throws InterruptedException {
		String result = payloadTests.applyOneShotChargeInstance(baseURI, login, password, oneShotChargeCode1, subCode,"in","");
		Assert.assertEquals(result, "SUCCESS");
	}

	/***   Insert EDRs, after Framework Agreement End Date ***/
	@Test(priority = 38)
	public void _FA_T02_insertUsageChargeAfterFAEndDate() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","after","");
		Assert.assertEquals(result, 1);
	}

	/***   Insert EDRs, after Framework Agreement End Date ***/
	@Test(priority = 40)
	public void _FA_T02_applyOneShotChargeInstanceAfterFAEndDate() throws InterruptedException {
		String result = payloadTests.applyOneShotChargeInstance(baseURI, login, password, oneShotChargeCode1, subCode,"after","");
		Assert.assertEquals(result, "SUCCESS");
	}

	/***   Cancel contract ***/
	@Test(priority = 44)
	public void _FA_T02_cancelFrameworkAgreement() {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();

		// search FA by code
		frameworkAgreementsTests.searchContractByCode(contractCode);

		// close FA
		frameworkAgreementsTests.closeContract();
	}

	/***   Insert EDRs, After canceling Framework Agreement  ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 46)
	public void _FA_T02_insertUsageChargeCdrInFAPeriod2(String baseURI, String login, String password) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","in","");
		Assert.assertEquals(result, 1);
	}

	/***   Insert EDRs, After canceling Framework Agreement  ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 48)
	public void _FA_T02_applyOneShotChargeInstanceInFAPeriod2(String baseURI, String login, String password) throws InterruptedException {
		String result = payloadTests.applyOneShotChargeInstance(baseURI, login, password, oneShotChargeCode1, subCode,"in","");
		Assert.assertEquals(result, "SUCCESS");
	}
	
	/***   Run Usage Job (U_Job)   ***/
	@Test(priority = 50, enabled = false)
	public void _FA_T02_runUJob() throws InterruptedException {
		jobsTests.runUJob(baseURI, login, password);
	} 

	/***   Run Rated Transaction Job (RT_Job)   ***/
	@Test(priority = 52, enabled = false)
	public void _FA_T02_runRTJob() throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}
	
	/***   Check Wallet operations    ***/
	@Test(priority = 54)
	public void _FA_T02_checkwalletOperations5(){
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		// check the number of lines found
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(subCode), 7);
		//(Before FA period, after FA period , after Canceling FA)
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(usageChargeAmount), 3);
		//( in FA period)
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(amountWithDiscount1), 1);
		//(Before FA period, after FA period , after Canceling FA)
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(oneShotOtherChargeAmount), 2);
		//( in FA period)
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(amountWithDiscount2), 1);
	}
}
