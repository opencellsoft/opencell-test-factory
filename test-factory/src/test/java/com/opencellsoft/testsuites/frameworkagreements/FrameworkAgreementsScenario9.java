package com.opencellsoft.testsuites.frameworkagreements;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.tests.WalletOperationsTests;
import com.opencellsoft.utility.Constant;

public class FrameworkAgreementsScenario9 extends FrameworkAgreementsCommons {
	FrameworkAgreementsDataSet dataSet = new FrameworkAgreementsDataSet();
	String offerCode = dataSet.offerCode;
	String productCode = dataSet.productCode;
	String usageChargeCode = dataSet.usageChargeCode;
	String usageChargeAmount = dataSet.usageChargeAmount;

	String customerCode1 = "FACUST9_1-" + Constant.customerCode;
	String customerCode2 = "FACUST9_2-" + Constant.customerCode;
	String customerName = Constant.customerName;
	String customerEmail = Constant.customerEmail;
	String paypalUserId = Constant.paypalUserId;
	String consumerCode1 = "FACONS1-" + Constant.consumerCode;
	String consumerCode2 = "FACONS2-" +Constant.consumerCode;
	String consumerName = Constant.consumerName;
	String companyRegistrationNumber = Constant.companyRegistrationNumber;
	String vatNumber = Constant.vatNumber;
	String subCode1 = "FASUB9-1_"+ Constant.subscriptionCode;
	String AP1 = "FAAP91"+Constant.accessPointCode;
	String subCode2 = "FASUB9-2_"+ Constant.subscriptionCode;
	String AP2 = "FAAP92"+Constant.accessPointCode;

	String contractCode1 = "FAC91_" + Constant.contractCode;
	String contractDescription1 = "FAC91_" + Constant.contractDescription ;
	String contractlineCode1 = "FAC91CL1_" + Constant.contractLineCode;
	String contractlineDescription1 = "FAC91CL1_" + Constant.contractLineDescription;
	String contractCode2 = "FAC92_" + Constant.contractCode;
	String contractDescription2 = "FAC92_" + Constant.contractDescription ;
	String contractlineCode2 = "FAC92CL1_" + Constant.contractLineCode;
	String contractlineDescription2 = "FAC92CL1_" + Constant.contractLineDescription;
	String newprice1 = "4.40";
	String newprice2 = "3.30";
	String companyRegistration = getRandomNumberbetween1And200();

	@Test(priority = 0, enabled = true)
	public void _FA_T09() throws Exception {
		System.out.println("************************** 9 - 2 contracts for 2 customers with the same offer ***************************");
	}

	/***   Create a new customer &  consumer  ***/
	@Test(priority = 4, enabled = true)
	public void _FA_T09_createCustomers() throws InterruptedException {
		payloadTests.createCustomer(baseURI, login, password, customerCode1, customerName , dataSet.bcCode);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode1, customerCode1, offerCode, productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode1, productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP1, subCode1);
		Thread.sleep(1000);
		
		payloadTests.createCustomer(baseURI, login, password, customerCode2, customerName , dataSet.bcCode);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode2, customerCode2, offerCode, productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode2, productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP2, subCode1);
		Thread.sleep(1000);
	}

	/***   Create new Framework Agreement   ***/
	@Parameters({ "level", "CLTypeGlobalPrice"})
	@Test(priority = 8)
	public void _FA_T09_createContract1( String level, String CLTypeGlobalPrice) {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();
		// Create new Framework Agreement
		frameworkAgreementsTests.createNewframeworkAgreementInPresent(contractCode1, contractDescription1, level,customerCode1);
		// Add contract line to Framework Agreement
		frameworkAgreementsTests.AddContractLineGlobalPrice(contractlineCode1, contractlineDescription1, offerCode, productCode, usageChargeCode,CLTypeGlobalPrice, newprice1 );
		// activate contract
		frameworkAgreementsTests.activateContract();
	}

	/***   Create new Framework Agreement   ***/
	@Parameters({ "level", "CLTypeGlobalPrice"})
	@Test(priority = 14)
	public void _FA_T09_createContract2( String level, String CLTypeGlobalPrice) {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();
		// Create new Framework Agreement
		frameworkAgreementsTests.createNewframeworkAgreementInPresent(contractCode2, contractDescription2, level,customerCode2);
		// Add contract line to Framework Agreement
		frameworkAgreementsTests.AddContractLineGlobalPrice(contractlineCode2, contractlineDescription2, offerCode, productCode, usageChargeCode,CLTypeGlobalPrice, newprice2 );
		// activate contract
		frameworkAgreementsTests.activateContract();
	}

	/***   Insert EDRs, Before Framework Agreement StartDate   ***/
	@Test(priority = 16)
	public void _FA_T09_insertChargeCdrInFAPeriod() throws InterruptedException {
		int result1 = insertChargeCdr(baseURI, login, password, AP1,"1", "", "", "","in","");
		Assert.assertEquals(result1, 1);
		int result2 = insertChargeCdr(baseURI, login, password, AP2,"1", "", "", "","in","");
		Assert.assertEquals(result2, 1);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 18)
	public void _FA_T09_checkwalletOperations() {

		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code 1
		walletOperationsTests.searchBySubscriptionCode(subCode1);

		// Verify the number of lines found
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(subCode1),1 );
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(newprice1), 1);

		// search operations by subscription code 2
		walletOperationsTests.searchBySubscriptionCode(subCode2);

		// Verify the number of lines found
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(subCode2),1 );
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(newprice2), 1);
	}
	/***   Cancel contract ***/
	@Test(priority = 20)
	public void _FA_T09_cancelFrameworkAgreements() {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();
		// search FA by code
		frameworkAgreementsTests.searchContractByCode(contractCode1);
		// close FA
		frameworkAgreementsTests.closeContract();

		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();
		// search FA by code
		frameworkAgreementsTests.searchContractByCode(contractCode2);
		// close FA
		frameworkAgreementsTests.closeContract();
	}

	/***   Insert EDRs, Before Framework Agreement StartDate   ***/
	@Test(priority = 22)
	public void _FA_T09_insertChargeCdrInFAPeriod3() throws InterruptedException {
		int result1 = insertChargeCdr(baseURI, login, password, AP1,"1", "", "", "","","");
		Assert.assertEquals(result1, 1);
		int result2 = insertChargeCdr(baseURI, login, password, AP2,"1", "", "", "","","");
		Assert.assertEquals(result2, 1);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 24)
	public void _FA_T09_checkwalletOperationsAfterCancelingFA() {

		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code 1
		walletOperationsTests = new WalletOperationsTests();
		walletOperationsTests.searchBySubscriptionCode(subCode1);
		// Check WO
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(usageChargeAmount), 1);

		// search operations by subscription code 2
		walletOperationsTests.searchBySubscriptionCode(subCode2);
		// Check WO
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(usageChargeAmount), 1);
	}
}
