package com.opencellsoft.testsuites.frameworkagreements;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class FrameworkAgreementsscenario4 extends FrameworkAgreementsCommons {
	FrameworkAgreementsDataSet dataSet = new FrameworkAgreementsDataSet();

	String customerCode = "FACUST4_"+Constant.offerCode;
	String consumerCode = "FACONS4_"+Constant.offerCode;

	String offerCode = dataSet.offerCode;
	String productCode = dataSet.productCode;
	String usageChargeCode = dataSet.usageChargeCode;
	String oneShotChargeCode1 = dataSet.oneShotOtChargeCode;
	String usageChargeAmount = dataSet.usageChargeAmount;
	String oneShotOtherChargeAmount = dataSet.oneShotOtherChargeAmount;

	String subCode = "FASUB4_"+Constant.subscriptionCode;
	String AP = "FAAP4_"+Constant.accessPointCode;
	String contractCode = "FAC4_" + Constant.contractCode;
	String contractDescription = "FAC4_" + Constant.contractDescription ;
	String contractlineCode1 = "FAC4CL1_" + Constant.contractLineCode;
	String contractlineDescription1 = "FAC4CL1_" + Constant.contractLineDescription;
	String contractlineCode2 = "FAC4CL2_" + Constant.contractLineCode;
	String contractlineDescription2 = "FAC4CL2_" + Constant.contractLineDescription;
	String discount1 = "70";
	String discount2 = "10";
	String amountWithDiscount1 = "3.00";
	String amountWithDiscount2 = "18.00";
	String companyRegistration = getRandomNumberbetween1And200();


	@Test(priority = 0, enabled = true)
	public void _FA_T04_login() {
		System.out.println("************************** 4 - FrameworkAgreements with CLType :  Custom Contract Discount ***************************");
	}

	/***   Create a new customer & consumer & subscription ***/
	@Test(priority = 3, enabled = true)
	public void _FA_T04_createCustomerAndSubscription() throws InterruptedException {
		payloadTests.createCustomer(baseURI, login, password, customerCode,dataSet.customerName , dataSet.bcCode);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, offerCode, productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);
	}

	/***   Create new Framework Agreement   ***/
	@Parameters({ "level"})
	@Test(priority = 18, enabled = true)
	public void _FA_T04_createFrameworkAgreement( String level) {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();
		frameworkAgreementsTests.createNewframeworkAgreementInPresent(contractCode, contractDescription, level,customerCode);
	}

	/***   Add contract line to Framework Agreement  ***/
	@Parameters({ "CLTypeCustomContractDiscount"})
	@Test(priority = 20, enabled = true)
	public void _FA_T04_addContractLineUsageCharge(String CLTypeCustomContractDiscount) throws InterruptedException {
		frameworkAgreementsTests.AddContractLineCustomContractDiscountWithDuplicateCharge(contractlineCode1, contractlineDescription1, offerCode, productCode, usageChargeCode,CLTypeCustomContractDiscount, discount1, false );
	}

	/***   Add contract line to Framework Agreement ***/
	@Test(priority = 22, enabled = true)
	@Parameters({ "CLTypeCustomContractDiscount"})
	public void _FA_T04_addContractLineOneShotCharge(String CLTypeCustomContractDiscount) throws InterruptedException {
		frameworkAgreementsTests.AddContractLineCustomContractDiscountWithCreatePriceVersion(contractlineCode2, contractlineDescription2, offerCode, productCode, oneShotChargeCode1,CLTypeCustomContractDiscount, discount2, false );
	}

	/***   activate contract   ***/
	@Test(priority = 24, enabled = true)
	public void _FA_T04_activateContract() {
		frameworkAgreementsTests.activateContract();
	}

	/***   Insert EDRs, Before Framework Agreement StartDate   ***/
	@Test(priority = 26, enabled = true)
	public void _FA_T04_insertChargeCdrBeforedFAStartDate() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","before","");
		Assert.assertEquals(result, 1);
	}	

	/***   Insert EDRs, In Framework Agreement  period ***/
	@Test(priority = 32)
	public void _FA_T04_insertChargeCdrInFAPeriod() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","in","");
		Assert.assertEquals(result, 1);
	}

	/***   Insert EDRs, In Framework Agreement  period ***/
	@Test(priority = 34)
	public void _FA_T04_applyOneShotChargeInstanceInFAPeriod1() throws InterruptedException {
		String result = payloadTests.applyOneShotChargeInstance(baseURI, login, password, oneShotChargeCode1, subCode,"in","");
		Assert.assertEquals(result, "SUCCESS");
	}


	/***   Insert EDRs, after Framework Agreement End Date ***/
	@Test(priority = 38)
	public void _FA_T04_insertChargeCdrAfterFAEndDate() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","after","");
		Assert.assertEquals(result, 1);
	}

	/***   Insert EDRs, after Framework Agreement End Date ***/
	@Test(priority = 40)
	public void _FA_T04_applyOneShotChargeInstanceAfterFAEndDate() throws InterruptedException {
		String result = payloadTests.applyOneShotChargeInstance(baseURI, login, password, oneShotChargeCode1, subCode,"after","");
		Assert.assertEquals(result, "SUCCESS");
	}

	/***   Cancel contract ***/
	@Test(priority = 44)
	public void _FA_T04_cancelFrameworkAgreement() {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();

		// search FA by code
		frameworkAgreementsTests.searchContractByCode(contractCode);

		// close FA
		frameworkAgreementsTests.closeContract();
	}

	/***   Insert EDRs, In Framework Agreement  period  ***/
	@Test(priority = 46)
	public void _FA_T04_insertChargeCdrInFAPeriod2() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","in","");
		Assert.assertEquals(result, 1);
	}

	/***   Insert EDRs, In Framework Agreement  period  ***/
	@Test(priority = 48)
	public void _FA_T04_applyOneShotChargeInstanceInFAPeriod2() throws InterruptedException {
		String result = payloadTests.applyOneShotChargeInstance(baseURI, login, password, oneShotChargeCode1, subCode,"in","");
		Assert.assertEquals(result, "SUCCESS");
	}

	/***   Check Wallet operations  ***/
	@Test(priority = 50)
	public void _FA_T04_checkwalletOperations5(){
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		// check the number of lines found (1 line)
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(subCode), 7);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(usageChargeAmount), 3);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(amountWithDiscount1), 1);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(oneShotOtherChargeAmount), 2);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(amountWithDiscount2), 1);
	}
}
