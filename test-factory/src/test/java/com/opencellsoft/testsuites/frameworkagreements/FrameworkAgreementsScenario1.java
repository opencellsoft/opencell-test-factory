package com.opencellsoft.testsuites.frameworkagreements;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class FrameworkAgreementsScenario1 extends FrameworkAgreementsCommons {
	FrameworkAgreementsDataSet dataSet = new FrameworkAgreementsDataSet();
	String subCode = "FASUB1_"+Constant.subscriptionCode;
	String AP = "FAAP1_"+Constant.accessPointCode;
	String customerCode = "FACUST1_"+Constant.offerCode;
	String consumerCode = "FACONS1_"+Constant.offerCode;
	String billingCycleCode = "FABC1-" + Constant.offerCode;
	String offerCode = dataSet.offerCode;
	String productCode = dataSet.productCode;
	String usageChargeCode = dataSet.usageChargeCode;
	String oneShotChargeCode = dataSet.oneShotOtChargeCode;

	String usageChargeAmount = dataSet.usageChargeAmount;
	String oneShotOtherChargeAmount = dataSet.oneShotOtherChargeAmount;

	String contractCode = "FAC1_" + Constant.contractCode;
	String contractDescription ="FAC1_" + Constant.contractDescription ;
	String contractlineCode1 = "FAC1CL1_" + Constant.contractLineCode;
	String contractlineDescription1 = "FAC1CL1_" + Constant.contractLineDescription;
	String contractlineCode2 = "FAC1CL2_" + Constant.contractLineCode;
	String contractlineDescription2 = "FAC1CL2_" + Constant.contractLineDescription;

	String newprice1 = "5.00";
	String newprice2 = "15.00";
	String companyRegistration = getRandomNumberbetween1And200();


	@Test(priority = 2, enabled = true)
	public void _FA_T01() throws Exception {
		System.out.println("***************** 1 - FrameworkAgreements with CLType : Global Price  *****************");
	}

	/***   Create a new customer &  subscription ***/
	@Test(priority = 3, enabled = true)
	public void _FA_T01_createCustomerAndSubscription() throws InterruptedException {
		// create Billing cycle
		payloadTests.createBillingCycleByAPI(baseURI, login, password,billingCycleCode,billingCycleCode,"BILLINGACCOUNT", "MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,billingCycleCode,billingCycleCode,"BILLINGACCOUNT", "CAL_PERIOD_MONTHLY");

		// create customer
		payloadTests.createCustomer(baseURI, login, password, customerCode,customerCode , billingCycleCode);
		Thread.sleep(500);
		
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, offerCode, productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);
		
	}

	/***   Insert EDRs, Before Create Framework Agreement   ***/
	@Test(priority = 6, enabled = true)
	public void _FA_T01_insertChargeCdrBeforeFA() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","","");
		Assert.assertEquals(result, 1);
	}

	/***   Insert EDRs, Before Create Framework Agreement   ***/
	@Test(priority = 8, enabled = true)
	public void _FA_T01_applyOneShotChargeInstanceBeforeFA() throws InterruptedException {
		String result = payloadTests.applyOneShotChargeInstance(baseURI, login, password, oneShotChargeCode, subCode,"","");
		Assert.assertEquals(result, "SUCCESS");
	}

	/***   Run Usage Job (U_Job)   ***/
	@Test(priority = 10, enabled = true)
	public void _FA_T01_runUJob() throws InterruptedException {
		jobsTests.runUJob(baseURI, login, password);
	} 

	/***   Run Rated Transaction Job (RT_Job)   ***/
	@Test(priority = 12, enabled = true)
	public void _FA_T01_runRTJob() throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 14, enabled = true)
	public void _FA_T01_checkwalletOperations1() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);

		// Verify the number of lines found 
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 2);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(usageChargeAmount), 1);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(oneShotOtherChargeAmount), 1);

	}

	/***   Check Rated items on costumer page   ***/
	@Test(priority = 16, enabled = false)
	public void _FA_T01_checkRatedItemsOnConstumerPage() {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Go to rated items tab
		customersTests.GoToRatedItemsTab(subCode);
		waitPageLoaded();

		//Check 2 open items
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("OPEN"), 2);
	}	

	/***   Create new Framework Agreement   ***/
	@Parameters({ "level"})
	@Test(priority = 18)
	public void _FA_T01_createFrameworkAgreement( String level) {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();
		frameworkAgreementsTests.createNewframeworkAgreementInPresent(contractCode, contractDescription, level,customerCode);
	}

	/***   Add contract line to Framework Agreement   ***/
	@Parameters({ "CLTypeGlobalPrice"})
	@Test(priority = 20, enabled = true)
	public void _FA_T01_addContractLineUsageCharge(String CLTypeGlobalPrice) {
		frameworkAgreementsTests.AddContractLineGlobalPrice(contractlineCode1, contractlineDescription1, offerCode, productCode, usageChargeCode,CLTypeGlobalPrice, newprice1 );
	}

	/***   Add contract line to Framework Agreement   ***/
	@Parameters({ "CLTypeGlobalPrice"})
	@Test(priority = 22, enabled = true)
	public void _FA_T01_addContractLineOneShotCharge(String CLTypeGlobalPrice) {
		frameworkAgreementsTests.AddContractLineGlobalPrice(contractlineCode2, contractlineDescription2, offerCode, productCode, oneShotChargeCode,CLTypeGlobalPrice, newprice2 );
	}

	/***   activate contract   ***/
	@Test(priority = 24)
	public void _FA_T01_activateContract() {
		frameworkAgreementsTests.activateContract();
	}

	/***   Insert EDRs, Before Framework Agreement StartDate   ***/
	@Test(priority = 26)
	public void _FA_T01_insertChargeCdrBeforedFAStartDate() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","before","");
		Assert.assertEquals(result, 1);
	}

	/***   Insert EDRs, Before Framework Agreement StartDate   ***/
	@Test(priority = 28, enabled = false)
	public void _FA_T01_applyOneShotChargeInstanceBeforeFAStartDate() throws InterruptedException {
		String result = payloadTests.applyOneShotChargeInstance(baseURI, login, password, oneShotChargeCode, subCode,"before","");
		Assert.assertEquals(result, "SUCCESS");
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 30, enabled = true)
	public void _FA_T01_checkwalletOperations2() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		// Verify the number of lines found
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode),3 );
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(usageChargeAmount), 2);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(oneShotOtherChargeAmount), 1);
	}

	/***   Insert EDRs, In Framework Agreement  period ***/
	@Test(priority = 32)
	public void _FA_T01_insertChargeCdrInFAPeriod() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","in","");
		Assert.assertEquals(result, 1);
	}

	/***   Insert EDRs, In Framework Agreement  period ***/
	@Test(priority = 34)
	public void _FA_T01_applyOneShotChargeInstanceInFAPeriod() throws InterruptedException {
		String result = payloadTests.applyOneShotChargeInstance(baseURI, login, password, oneShotChargeCode, subCode,"in","");
		Assert.assertEquals(result, "SUCCESS");
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 36, enabled = true)
	public void _FA_T01_checkwalletOperations3() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		// Verify the number of lines found 
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode),5);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(usageChargeAmount), 2);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(newprice1), 1);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(oneShotOtherChargeAmount), 1);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(newprice2), 1);
	}

	/***   Insert EDRs, after Framework Agreement End Date ***/
	@Test(priority = 38)
	public void _FA_T01_insertChargeCdrAfterFAEndDate() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","after","");
		Assert.assertEquals(result, 1);
	}

	/***   Insert EDRs, after Framework Agreement End Date ***/
	@Test(priority = 40)
	public void _FA_T01_applyOneShotChargeInstanceAfterFAEndDate() throws InterruptedException {
		String result = payloadTests.applyOneShotChargeInstance(baseURI, login, password, oneShotChargeCode, subCode,"after","");
		Assert.assertEquals(result, "SUCCESS");
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 42, enabled = true)
	public void _FA_T01_checkwalletOperations4() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		// Verify the number of lines found 
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 7);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(usageChargeAmount), 3);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(newprice1), 1);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(oneShotOtherChargeAmount), 2);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(newprice2), 1);
	}

	/***   Cancel contract ***/
	@Test(priority = 44)
	public void _FA_T01_cancelFrameworkAgreement() {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();

		// search FA by code
		frameworkAgreementsTests.searchContractByCode(contractCode);

		// close FA
		frameworkAgreementsTests.closeContract();
	}

	/***   Insert EDRs, In Framework Agreement  period  ***/
	@Test(priority = 46)
	public void _FA_T01_insertChargeCdrInFAPeriod2() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","in","");
		Assert.assertEquals(result, 1);
	}

	/***   Insert EDRs, In Framework Agreement  period  ***/
	@Test(priority = 48)
	public void _FA_T01_applyOneShotChargeInstanceInFAPeriod2() throws InterruptedException {
		String result = payloadTests.applyOneShotChargeInstance(baseURI, login, password, oneShotChargeCode, subCode,"in","");
		Assert.assertEquals(result, "SUCCESS");
	}

	/***   Check Wallet operations    ***/
	@Test(priority = 50)
	public void _FA_T01_checkwalletOperations5(){
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		// check the number of lines found
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 9);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(usageChargeAmount), 4);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(newprice1), 1);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(oneShotOtherChargeAmount), 3);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(newprice2), 1);
	}
}
