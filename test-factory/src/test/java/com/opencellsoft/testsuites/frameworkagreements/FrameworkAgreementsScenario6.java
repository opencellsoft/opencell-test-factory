package com.opencellsoft.testsuites.frameworkagreements;

import java.awt.AWTException;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.utility.Constant;

public class FrameworkAgreementsScenario6 extends FrameworkAgreementsCommons {
	FrameworkAgreementsDataSet dataSet = new FrameworkAgreementsDataSet();

//	String offerCode = "FAOFF6-001";
//	String productCode = "FAPROD6-001";
//	String usageChargeCode = "FACH6-001";
//	String code = Constant.offerCode;
	String code = "HJU1329";
	String offerCode = "FAOFF6-001" + code;
	String productCode = "FAPROD6-001" + code;
	String usageChargeCode = "FACH6-001" + code;

	String customerCode = "FACUST6_" + code;
	String consumerCode = "FACONS6_" + code;
	String offerDescription = "FAOFFDesc6-" + Constant.offerDescription;
	String productDescription = "FAPRODDesc6-" + Constant.productDescription;
	String usageChargeDescription= "FACHDesc6-" + Constant.chargeDescriptionUsage;
	String pricePlanVersionCode = "FAPV6-" + code;
	String unitPrice1 = "500.00";
	String unitPrice2 = "0.00";
	String discount = "0.00";

	String subCode = "FASUB6_"+ Constant.offerCode;
	String AP = "FAAP6"+Constant.offerCode;
	String pricePlanMatScript = "*ValoCap*";

	String contractCode = "FAC6_" + Constant.contractCode;
	String contractDescription = "FAC6_" + Constant.contractDescription ;
	String contractlineCode = "FAC6CL1_" + Constant.contractLineCode;
	String companyRegistration = getRandomNumberbetween1And200();


	@Test(priority = 0, enabled = true)
	public void _FA_T06() {
		System.out.println("************************** 6 - FrameworkAgreements : Custom Contract Discount = 0%  ***************************");
		System.out.println("String offerCode = \"" + offerCode  + "\";");
		System.out.println("String productCode = \"" + productCode  + "\";");
		System.out.println("String usageChargeCode = \"" + usageChargeCode  + "\";" );
	}

	/***  create a New Offer    ***/
	@Parameters({ "PriceVersionDate", "defaultQuantity", "minimalQuantity", "maximalQuantity", "sequence"})
	@Test(priority = 4, enabled = true)
	public void _FA_T06_createOffer(String PriceVersionDate, String defaultQuantity, String minimalQuantity, String maximalQuantity, String sequence) throws InterruptedException, AWTException {

		offersTests.createOfferMethod(offerCode, offerDescription);
		productsTests.createProductMethod(PriceVersionDate,productCode,productDescription );
		chargesTests.createUsageCharge(usageChargeCode,usageChargeDescription,"","", version);
		Thread.sleep(3000);
		chargesTests.updateUsageChargeType();// to delete
		chargesTests.createNewPriceVersionPriceGrid(pricePlanVersionCode);
		chargesTests.addUnitPriceToPriceVersionWithAttribute(true,"parameter 1", "EDR_text_parameter_1","A", unitPrice1);
		waitPageLoaded();
		chargesTests.addUnitPrice2(unitPrice2);
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(usageChargeDescription, version);
		productsTests.publishProductVersion();
		productsTests.activateProductAndGoBack();
		homeTests.goToOffersPage();
		offersTests.searchForOfferMethod(offerCode, offerDescription);
		offersTests.pickProductToOffer( productCode,productDescription, version);
		offersTests.fillProductParametersMethod(defaultQuantity,minimalQuantity, maximalQuantity, sequence, false );
		offersTests.ActivateOfferMethod();
		waitPageLoaded();
	}

	/***   Create and Activate a new Subscription   ***/
	@Test(priority = 16, enabled = true)
	public void _FA_T06_createAndActivateSubscription() throws AWTException, InterruptedException{
		// create new billing cycle
		payloadTests.createBillingCycleByAPI(baseURI, login, password,dataSet.bcCode,dataSet.bcCode,"BILLINGACCOUNT", "MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,dataSet.bcCode,dataSet.bcCode,"BILLINGACCOUNT", "CAL_PERIOD_MONTHLY");

		payloadTests.createCustomer(baseURI, login, password, customerCode, dataSet.customerName , dataSet.bcCode);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, offerCode, productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);

	}

	/***   Insert EDRs, Before Create Framework Agreement   ***/
	@Test(priority = 18, enabled = true)
	public void _FA_T06_insertChargeCdrBeforeFA() throws InterruptedException {
		int result1 = insertChargeCdr(baseURI, login, password, AP,"1", "A", "", "","","");
		Assert.assertEquals(result1, 1);
		int result2 = insertChargeCdr(baseURI, login, password, AP,"1",  "", "", "","","");
		Assert.assertEquals(result2, 1);
	}

	/***   Create new Framework Agreement    ***/
	@Parameters({ "level", "CLTypeCustomContractDiscount"})
	@Test(priority = 24)
	public void _FA_T06_createFrameworkAgreement( String level, String CLTypeCustomContractDiscount) throws InterruptedException {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();
		frameworkAgreementsTests.createNewframeworkAgreementInPresent(contractCode, contractDescription, level,customerCode);
		frameworkAgreementsTests.AddContractLineCustomContractDiscountWithDuplicateCharge0(contractlineCode, contractlineCode, offerCode, productCode, usageChargeCode,CLTypeCustomContractDiscount, discount, true );
		frameworkAgreementsTests.activateContract();
	}

	/***   Insert EDRs, Before Create Framework Agreement   ***/
	@Test(priority = 30, enabled = true)
	public void _FA_T06_insertChargeCdrInFAPeriod() throws InterruptedException {
		int result1 = insertChargeCdr(baseURI, login, password, AP,"1", "A", "", "","","");
		Assert.assertEquals(result1, 1);
		int result2 = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","","");
		Assert.assertEquals(result2, 1);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 32, enabled = true)
	public void _FA_T06_checkwalletOperations2() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		// Verify the number of lines found 
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(subCode), 6);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(unitPrice1), 2);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(unitPrice2), 4);
	}

	/***   Cancel contract ***/
	@Test(priority = 34)
	public void _FA_T06_cancelFrameworkAgreement() {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();

		// search FA by code
		frameworkAgreementsTests.searchContractByCode(contractCode);

		// close FA
		frameworkAgreementsTests.closeContract();
	}
}
