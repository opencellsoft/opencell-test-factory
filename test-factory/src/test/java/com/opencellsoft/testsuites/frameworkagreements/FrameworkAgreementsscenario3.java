package com.opencellsoft.testsuites.frameworkagreements;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class FrameworkAgreementsscenario3 extends FrameworkAgreementsCommons {

	FrameworkAgreementsDataSet dataSet = new FrameworkAgreementsDataSet();

	String subCode = "FASUB3_"+Constant.subscriptionCode;
	String AP = "FAAP3_"+Constant.accessPointCode;

	String customerCode = "FACUST3_"+Constant.offerCode;
	String consumerCode = "FACONS3_"+Constant.offerCode;
	String offerCode = dataSet.offerCode;
	String productCode = dataSet.productCode;
	String usageChargeCode = dataSet.usageChargeCode;
	String oneShotChargeCode1 = dataSet.oneShotOtChargeCode;
	String usageChargeAmount = dataSet.usageChargeAmount;
	String oneShotOtherChargeAmount = dataSet.oneShotOtherChargeAmount;

	String contractCode = "FAC3_" + Constant.contractCode;
	String contractDescription = "FAC3_" + Constant.contractDescription ;
	String contractlineCode1 = "FAC3CL1_" + Constant.contractLineCode;
	String contractlineDescription1 = "FAC3CL1_" + Constant.contractLineDescription;
	String contractlineCode2 = "FAC3CL2_" + Constant.contractLineCode;
	String contractlineDescription2 = "FAC3CL2_" + Constant.contractLineDescription;

	String newPrice1 = "7.00";
	String newPrice2 = "12.00";
	String companyRegistration = getRandomNumberbetween1And200();

	@Test(priority = 2, enabled = true)
	public void _FA_T03() {
		System.out.println("**************************  3 - FrameworkAgreements with CLType : Custom Contract Price ***************************");
	}

	/***   Create a new customer &  subscription ***/
	@Test(priority = 3, enabled = true)
	public void _FA_T03_createCustomerAndSubscription() throws InterruptedException {
		payloadTests.createCustomer(baseURI, login, password, customerCode,dataSet.customerName , dataSet.bcCode);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, offerCode, productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);
	}

	/***   Create new Framework Agreement   ***/
	@Parameters({ "level"})
	@Test(priority = 18, enabled = true)
	public void _FA_T03_createFrameworkAgreement( String level) {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();
		frameworkAgreementsTests.createNewframeworkAgreementInPresent(contractCode, contractDescription, level,customerCode);
	}

	/***   Add contract line to Framework Agreement  ***/
	@Parameters({ "CLTypeCustomContractPrice"})
	@Test(priority = 20, enabled = true)
	public void _FA_T03_addContractLineUsageCharge(String CLTypeCustomContractPrice) throws InterruptedException {
		frameworkAgreementsTests.AddContractLineCustomContractPriceWithCreatePriceVersion(contractlineCode1, contractlineDescription1, offerCode, productCode, usageChargeCode,CLTypeCustomContractPrice, newPrice1 );
	}

	/***   Add contract line to Framework Agreement ***/
	@Test(priority = 22, enabled = true)
	@Parameters({ "CLTypeCustomContractPrice"})
	public void _FA_T03_addContractLineOneShotCharge(String CLTypeCustomContractPrice) throws InterruptedException {
		frameworkAgreementsTests.AddContractLineCustomContractPriceWithCreatePriceVersion(contractlineCode2, contractlineDescription2, offerCode, productCode, oneShotChargeCode1,CLTypeCustomContractPrice, newPrice2 );
	}

	/***   activate contract   ***/
	@Test(priority = 24, enabled = true)
	public void _FA_T03_activateContract() {
		frameworkAgreementsTests.activateContract();
	}

	/***   Insert EDRs, Before Framework Agreement StartDate   ***/
	@Test(priority = 26)
	public void _FA_T03_insertChargeCdrBeforedFAStartDate() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","before","");
		Assert.assertEquals(result, 1);
	}

	/***   Insert EDRs, In Framework Agreement  period ***/
	@Test(priority = 32)
	public void _FA_T03_insertChargeCdrInFAPeriod() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","in","");
		Assert.assertEquals(result, 1);
	}

	/***   Insert EDRs, In Framework Agreement  period ***/
	@Test(priority = 34)
	public void _FA_T03_applyOneShotChargeInstanceInFAPeriod() throws InterruptedException {
		String result = payloadTests.applyOneShotChargeInstance(baseURI, login, password, oneShotChargeCode1, subCode,"in","");
		Assert.assertEquals(result, "SUCCESS");
	}

	/***   Insert EDRs, after Framework Agreement End Date ***/
	@Test(priority = 38)
	public void _FA_T03_insertChargeCdrAfterFAEndDate() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","after","");
		Assert.assertEquals(result, 1);
	}

	/***   Insert EDRs, after Framework Agreement End Date ***/
	@Test(priority = 40)
	public void _FA_T03_applyOneShotChargeInstanceAfterFAEndDate() throws InterruptedException {
		String result = payloadTests.applyOneShotChargeInstance(baseURI, login, password, oneShotChargeCode1, subCode,"after","");
		Assert.assertEquals(result, "SUCCESS");
	}

	/***   Cancel contract ***/
	@Test(priority = 44)
	public void _FA_T03_cancelFrameworkAgreement() {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();

		// search FA by code
		frameworkAgreementsTests.searchContractByCode(contractCode);

		// close FA
		frameworkAgreementsTests.closeContract();
	}

	/***   Insert EDRs, In Framework Agreement  period  ***/
	@Test(priority = 46)
	public void _FA_T03_insertChargeCdrInFAPeriod2() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "","in","");
		Assert.assertEquals(result, 1);
	}

	/***   Insert EDRs, In Framework Agreement  period  ***/
	@Test(priority = 48)
	public void _FA_T03_applyOneShotChargeInstanceInFAPeriod2() throws InterruptedException {
		String result = payloadTests.applyOneShotChargeInstance(baseURI, login, password, oneShotChargeCode1, subCode,"in","");
		Assert.assertEquals(result, "SUCCESS");
	}

	/***   Check Wallet operations  ***/
	@Test(priority = 50)
	public void _FA_T03_checkwalletOperations4(){
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		// check the number of lines found (1 line)
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(subCode), 7);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(usageChargeAmount), 3);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(newPrice1), 1);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(oneShotOtherChargeAmount), 2);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(newPrice2), 1);
	}
}
