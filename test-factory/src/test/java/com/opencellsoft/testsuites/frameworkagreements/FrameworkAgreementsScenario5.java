package com.opencellsoft.testsuites.frameworkagreements;

import java.awt.AWTException;
import java.util.ArrayList;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class FrameworkAgreementsScenario5 extends FrameworkAgreementsCommons {
	FrameworkAgreementsDataSet dataSet = new FrameworkAgreementsDataSet();

	String offerCode = "FAOFF5-" + Constant.offerCode;
	String productCode = "FAPROD5-" + Constant.offerCode;
	String usageChargeCode1 = "FACH51-" + Constant.offerCode;
	String usageChargeCode2 = "FACH52-" + Constant.offerCode;

	String customerCode = "FACUST5-" + Constant.offerCode;
	String offerDescription = "FAOFF5-" + Constant.offerDescription;
	String productDescription = "FAPROD5" + Constant.productDescription;
	String pricePlanVersionCode_Charge1 = "FAPV51" + Constant.offerCode;
	String pricePlanVersionCode_Charge2 = "FAPV52" + Constant.offerCode;	
	String customerName = Constant.customerName;
	String customerEmail = Constant.customerEmail;
	String paypalUserId = Constant.paypalUserId;
	String consumerCode = "FACONS5-" +Constant.offerCode;
	String consumerName = Constant.consumerName;
	String companyRegistrationNumber = Constant.companyRegistrationNumber;
	String vatNumber = Constant.vatNumber;

	String subCode = "FASUB5_"+ Constant.offerCode;
	String AP = "FAAP5_"+Constant.offerCode;
	String pricePlanMatScript = "*ValoCap*";

	String contractCode = "FAC51_" + Constant.offerCode;
	String contractCode2 = "FAC52_" + Constant.offerCode;
	String contractDescription = "FAC5_" + Constant.contractDescription ;
	String contractlineCode1 = "FAC5CL1_" + Constant.offerCode;
	String contractlineCode2 = "FAC5CL2_" + Constant.offerCode;
	String contractlineCode3 = "FAC5CL3_" + Constant.offerCode;
	String contractlineCode4 = "FAC5CL4_" + Constant.offerCode;

	String usageChargeAmount = "0.00";
	String ratingAmount = "5.00";
	String discount1 = "40";
	String discount2 = "50";
	String discount3 = "20";
	String discount4 = "60";
	String amountWithDiscount1 = "3.00";
	String amountWithDiscount2 = "2.50";
	String amountWithDiscount3 = "-1.00";
	String amountWithDiscount4 = "-3.00";
	ArrayList<String> tabs;
	String companyRegistration = getRandomNumberbetween1And200();	

	@Test(priority = 0, enabled = true)
	public void _FA_T05() {
		System.out.println("************************** 5 - FrameworkAgreements with Custom Contract Discount AND charges with rating script  ***************************");
		System.out.println("String offerCode = \"" + offerCode  + "\";");
		System.out.println("String productCode = \"" + productCode  + "\";");
		System.out.println("String usageChargeCode1 = \"" + usageChargeCode1  + "\";" );
		System.out.println("String usageChargeCode2 = \"" + usageChargeCode2  + "\";");
	}

	/***  create a New Offer  ***/
	@Parameters({ "PriceVersionDate", "defaultQuantity", "minimalQuantity", "maximalQuantity", "sequence"})
	@Test(priority = 6, enabled = true)
	public void _FA_T05_createOffer(String PriceVersionDate,String defaultQuantity, String minimalQuantity, String maximalQuantity, String sequence) throws InterruptedException, AWTException {
		homeTests.goToPortal();
		offersTests.createOfferMethod(offerCode, offerDescription);
		productsTests.createProductMethod(PriceVersionDate,productCode,productDescription);
		createUsageCharge(usageChargeCode1,"UN","",pricePlanVersionCode_Charge1,usageChargeAmount);
		createUsageCharge(usageChargeCode2,"DEUX","",pricePlanVersionCode_Charge2,usageChargeAmount);
		productsTests.publishProductVersion();
		productsTests.activateProductAndGoBack();
		homeTests.goToOffersPage();
		offersTests.searchForOfferMethod(offerCode, offerDescription);
		offersTests.pickProductToOffer(productCode,productDescription, version);
		offersTests.fillProductParametersMethod(defaultQuantity,minimalQuantity, maximalQuantity, sequence, false );
		offersTests.ActivateOfferMethod();
		waitPageLoaded();
	}

	/***   Create custumer and Activate a new Subscription   ***/
	@Test(priority = 8, enabled = true)
	public void _FA_T05_createAndActivateSubscription() throws AWTException, InterruptedException{

		payloadTests.createBillingCycleByAPI(baseURI, login, password,dataSet.bcCode,dataSet.bcCode,"BILLINGACCOUNT", "MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,dataSet.bcCode, dataSet.bcCode,"BILLINGACCOUNT", "CAL_PERIOD_MONTHLY");
		// create new customer
		payloadTests.createCustomer(baseURI, login, password, customerCode,customerName , dataSet.bcCode);
		Thread.sleep(500);
		// create new subscription 
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, offerCode, productCode);
		Thread.sleep(500);
		// activate product instance
		payloadTests.activateServices(baseURI, login, password, subCode, productCode);
		Thread.sleep(500);
		// create Access point
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);
	}

	/***   Insert EDRs   ***/
	@Test(priority = 20)
	public void _FA_T05_insertChargeCdr1() throws InterruptedException {
		int result1 = insertChargeCdr(baseURI, login, password, AP,"1", "UN", "", "","","");
		Assert.assertEquals(result1, 1);
		int result2 = insertChargeCdr(baseURI, login, password, AP,"1", "DEUX", "", "","","");
		Assert.assertEquals(result2, 1);
	}

	/***  Open admin in other window  ***/
	@Test(priority = 26, enabled = true)
	public void _FA_T05_openAdmin() {
		// Open new Tab (for admin app)
		((JavascriptExecutor)driver).executeScript("window.open()");
		tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.get(baseURI);
		waitPageLoaded();
	}	

	/***  Create new rating script  ***/
	@Test(priority = 28, enabled = true)
	public void _FA_T05_createValoCapScript() {
		// Go to Scripts list page
		scriptTests.goToScriptListPage();

		// create new script
		scriptTests.addValoCapScript();
	}

	/***   Add Rating Script For PPM_charge1   ***/
	@Test(priority = 30, enabled = true)
	public void _FA_T05_AddRatingScriptForPPM_charge1() {

		// Go to price plan list
		pricePlanMatrixTests.goToPricePlanList();

		// search for pricePlanMatrixe by charge
		pricePlanMatrixTests.searchForPricePlanMatrixeByCharge(usageChargeCode1);

		// Add rating script to price plan
		pricePlanMatrixTests.AddScriptToPricePlan(pricePlanMatScript, usageChargeAmount);
	}

	/***   Add Rating Script For PPM_charge1   ***/
	@Test(priority = 32, enabled = true)
	public void _FA_T05_AddScriptToPPM_charge2() {
		// Go to price plan list
		pricePlanMatrixTests.goToPricePlanList();

		// search for pricePlanMatrixe by charge
		pricePlanMatrixTests.searchForPricePlanMatrixeByCharge(usageChargeCode2);

		// Add script to price plan
		pricePlanMatrixTests.AddScriptToPricePlan(pricePlanMatScript, usageChargeAmount);
	}

	/***  close admin  ***/
	@Test(priority = 34, enabled = true)
	public void _FA_T05_closeAdmin() {
		tabs = new ArrayList<String>(driver.getWindowHandles());
		// clos eAdmin
		driver.close();
		// Go back to the portal tab
		driver.switchTo().window(tabs.get(0));
	}

	/***   Insert EDRs, Before Framework Agreement StartDate   ***/
	@Test(priority = 36)
	public void _FA_T05_insertChargeCdr2() throws InterruptedException {
		int result1 = insertChargeCdr(baseURI, login, password, AP,"1", "UN", "", "","","");
		Assert.assertEquals(result1, 1);
		int result2 = insertChargeCdr(baseURI, login, password, AP,"1", "DEUX", "", "","","");
		Assert.assertEquals(result2, 1);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 40, enabled = true)
	public void _FA_T05_checkwalletOperations2() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		// Verify the number of lines found (1 line)
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(subCode), 4);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(usageChargeAmount), 2);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(ratingAmount), 2);
	}

	/***   Create new Framework Agreement    ***/
	@Parameters({ "level", "CLTypeGlobalDiscount", "CLTypeCustomContractDiscount"})
	@Test(priority = 42, enabled = false)
	public void _FA_T05_createFrameworkAgreement( String level, String CLTypeGlobalDiscount, String CLTypeCustomContractDiscount) throws InterruptedException {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();
		// Create new Framework Agreement 
		frameworkAgreementsTests.createNewframeworkAgreementInPresent(contractCode, contractDescription, level,customerCode);
		frameworkAgreementsTests.AddContractLineGlobalDiscount(contractlineCode1, contractlineCode1, offerCode, productCode, usageChargeCode1,CLTypeGlobalDiscount, discount1, false );
		frameworkAgreementsTests.AddContractLineCustomContractDiscountWithDuplicateCharge(contractlineCode2, contractlineCode2, offerCode, productCode, usageChargeCode2,CLTypeCustomContractDiscount, discount2, false );
		frameworkAgreementsTests.activateContract();
	}

	/***   Insert EDRs Before Framework Agreement StartDate   ***/
	@Test(priority = 50, enabled = false)
	public void _FA_T05_insertChargeCdrBeforeFrameworkAgreementStartDate() throws InterruptedException {
		int result1 = insertChargeCdr(baseURI, login, password, AP,"1", "UN", "", "","before","");
		Assert.assertEquals(result1, 1);
		int result2 = insertChargeCdr(baseURI, login, password, AP,"1", "DEUX", "", "","before","");
		Assert.assertEquals(result2, 1);
	}

	/***   Insert EDRs in  Framework Agreement period   ***/
	@Test(priority = 54, enabled = false)
	public void _FA_T05_insertChargeCdrInFrameworkAgreementPeriod() throws InterruptedException {
		int result1 = insertChargeCdr(baseURI, login, password, AP,"1", "UN", "", "","in","");
		Assert.assertEquals(result1, 1);
		int result2 = insertChargeCdr(baseURI, login, password, AP,"1", "DEUX", "", "","in","");
		Assert.assertEquals(result2, 1);
	}

	/***   Insert EDRs after Framework Agreement period   ***/
	@Test(priority = 58, enabled = false)
	public void _FA_T05_insertChargeCdrAfterFrameworkAgreementPeriod() throws InterruptedException {
		int result1 = insertChargeCdr(baseURI, login, password, AP,"1", "UN", "", "","after","");
		Assert.assertEquals(result1, 1);
		int result2 = insertChargeCdr(baseURI, login, password, AP,"1", "DEUX", "", "","after","");
		Assert.assertEquals(result2, 1);
	}

	/***   Cancel contract ***/
	@Test(priority = 62, enabled = false)
	public void _FA_T05_cancelFrameworkAgreement() {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();

		// search FA by code
		frameworkAgreementsTests.searchContractByCode(contractCode);

		// close FA
		frameworkAgreementsTests.closeContract();
	}

	/***   Insert EDRs, in Framework Agreement period   ***/
	@Test(priority = 64, enabled = false)
	public void _FA_T05_insertChargeCdr6() throws InterruptedException {
		int result1 = insertChargeCdr(baseURI, login, password, AP,"1", "UN", "", "","in","");
		Assert.assertEquals(result1, 1);
		int result2 = insertChargeCdr(baseURI, login, password, AP,"1", "DEUX", "", "","in","");
		Assert.assertEquals(result2, 1);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 66, enabled = false)
	public void _FA_T05_checkwalletOperations6() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		// Verify the number of lines found (1 line)
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(subCode), 12);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(usageChargeAmount), 2);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(ratingAmount), 8);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(amountWithDiscount1), 1);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(amountWithDiscount2), 1);
	}

	/***   Create new Framework Agreement   ***/
	@Parameters({ "level"})
	@Test(priority = 68, enabled = false)
	public void _FA_T05_createFrameworkAgreement2( String level) {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();
		frameworkAgreementsTests.createNewframeworkAgreementInPresent(contractCode2, contractDescription, level,customerCode);
	}

	/***   Add contract line to Framework Agreement   ***/
	@Parameters({ "CLTypeGlobalDiscount"})
	@Test(priority = 70, enabled = false)
	public void _FA_T05_addContractLineUsageCharge4(String CLTypeGlobalDiscount) {
		frameworkAgreementsTests.AddContractLineGlobalDiscount(contractlineCode3, contractlineCode3, offerCode, productCode, usageChargeCode1,CLTypeGlobalDiscount, discount3,true );
	}
	/***   Add contract line to Framework Agreement  ***/
	@Parameters({ "CLTypeCustomContractDiscount"})
	@Test(priority = 72, enabled = false)
	public void _FA_T05_addContractLineUsageCharge5(String CLTypeCustomContractDiscount) throws InterruptedException {
		frameworkAgreementsTests.AddContractLineCustomContractDiscountWithDuplicateCharge(contractlineCode4, contractlineCode4, offerCode, productCode, usageChargeCode2,CLTypeCustomContractDiscount, discount4, true );
	}

	/***   activate contract   ***/
	@Test(priority = 74, enabled = false)
	public void _FA_T05_activateContract2() {
		frameworkAgreementsTests.activateContract();
	}

	/***   Insert EDRs, in Framework Agreement period   ***/
	@Test(priority = 76, enabled = false)
	public void _FA_T05_insertChargeCdr7() throws InterruptedException {
		int result1 = insertChargeCdr(baseURI, login, password, AP,"1", "UN", "", "","in","");
		Assert.assertEquals(result1, 1);
		int result2 = insertChargeCdr(baseURI, login, password, AP,"1", "DEUX", "", "","in","");
		Assert.assertEquals(result2, 1);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 78, enabled = false)
	public void _FA_T05_checkwalletOperations7() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		// Verify the number of lines found (1 line)
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(subCode), 16);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(usageChargeAmount), 2);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(ratingAmount), 10);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(amountWithDiscount1), 1);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(amountWithDiscount2), 1);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(amountWithDiscount3), 1);
		Assert.assertEquals(walletOperationsPage.elementsFoundSize(amountWithDiscount4), 1);
	}

	/***   Cancel contract ***/
	@Test(priority = 80, enabled = false)
	public void _FA_T05_cancelFrameworkAgreement2() {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();

		// search FA by code
		frameworkAgreementsTests.searchContractByCode(contractCode2);

		// close FA
		frameworkAgreementsTests.closeContract();
	}


	/***  Create a new charge type Usage from product page  ***/
	public void createUsageCharge(String usageChargeCode1, String p1, String p2, String pricePlanVersionCode_Charge1,String usageChargeAmount  ) throws InterruptedException{	
		chargesTests.createUsageCharge(usageChargeCode1,usageChargeCode1,p1,p2, version);
		Thread.sleep(3000);
		chargesTests.createNewPriceVersionPriceGrid(pricePlanVersionCode_Charge1);
		chargesTests.addUnitPriceToPriceVersion(usageChargeAmount);
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(usageChargeCode1, version);
	}
}
