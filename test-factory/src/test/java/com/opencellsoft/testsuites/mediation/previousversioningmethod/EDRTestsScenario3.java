package com.opencellsoft.testsuites.mediation.previousversioningmethod;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.pages.customercare.rateditems.RatedItemsPage;
import com.opencellsoft.tests.CustomersTests;
import com.opencellsoft.tests.HomeTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.tests.MediationEDRTests;
import com.opencellsoft.tests.WalletOperationsTests;
import com.opencellsoft.tests.customercare.SubscriptionsTests;
import com.opencellsoft.tests.jobs.JobsTests;
import com.opencellsoft.testsuites.mediation.EDRTestsCommons;
import com.opencellsoft.testsuites.mediation.EDRTestsDataSet;
import com.opencellsoft.utility.Constant;

public class EDRTestsScenario3 extends EDRTestsCommons {
	/*********************************************************************************************
	 *********************    Scenario 3 : add 3 versions  => RT_job *****************************
	- Update Triggered EDR
	- Create And Activate a new Subscription
	- Insert Charge Cdr 1st Version
	- Insert Charge Cdr 2 nd Version
	- Insert Charge Cdr 3 rd Version
	- Check Edr third Version  ====>  (2 EDRS version 1 CANCELED,2 EDRS version 2 CANCELED,2 EDRS version 3 RATED )
	- Run Rerate_Job
	- Run U_Job
	- Run RT_Job
	- Check Wallet operations (2 TREATED, 1 CANCELLED, 1 RERATEED )
	- Check Rated Items On Costumer Page   ====>  2 OPEN items
	 **********************************************************************************************/
	EDRTestsDataSet EDRTestsDataSet = new EDRTestsDataSet();
	String customerCode = EDRTestsDataSet.customerCode;
	String offerCode = EDRTestsDataSet.offerCode;
	String triggerEdrCode = EDRTestsDataSet.triggerEdrCode;
	String subCode = "EDRSUB03_" + Constant.subscriptionCode;
	String AP = "EDRAP03" + Constant.accessPointCode;
	String param3 = Constant.param3 + "_03";
	String param3EL = param3 + "_TRDEDR";	

	public EDRTestsScenario3() {
		loginTest = new LoginTest();
		customersTests = new CustomersTests();
		subscriptionsTests = new SubscriptionsTests();
		mediationEDRTests = new MediationEDRTests();
		homeTests = new HomeTests();
		walletOperationsTests = new WalletOperationsTests();
		jobsTests =  new JobsTests();
		ratedItemsPage = new RatedItemsPage();
	}

	// login
	@Parameters({ "login", "password" })
	@Test(priority = 2, enabled = true)
	public void login(String login, String password) throws Exception {
		System.out.println("**********************************  EDR Versionning Scenario 3 : add 3 versions  => RT_job  *******************************");
		loginTest.loginTest(login, password);
	}

	// Create and Activate a new Subscription
	@Parameters({ "seller" })
	@Test(priority = 6)
	public void createAndActivateSubscription(String seller) throws AWTException, InterruptedException{
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Create a new subscription from subscription tab in customer details page
		subscriptionsTests.create_subscription_FromCustomerPage(seller, subCode, offerCode);

		// Create Access point
		subscriptionsTests.createAccessPointMethod(AP);

		// Activate the product instance for the subscription
		subscriptionsTests.activateProductInstanceMethod();
	}

	// Insert EDRs, first version 
	@Parameters({ "baseURI", "login", "password","param2" })
	@Test(priority = 8)
	public void insertChargeCdrFirstVersion(String baseURI, String login, String password, String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"4", "", param2, param3);
		Assert.assertEquals(result, 1);
	}

	// Insert EDRs, second version 
	@Parameters({ "baseURI", "login", "password","param2" })
	@Test(priority = 10)
	public void insertChargeCdrSecondVersion(String baseURI, String login, String password, String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"4", "", param2, param3);
		Assert.assertEquals(result, 1);
	}

	// Insert EDRs, third version 
	@Parameters({ "baseURI", "login", "password","param2" })
	@Test(priority = 12)
	public void insertChargeCdrThridVersion(String baseURI, String login, String password, String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"4", "", param2, param3);
		Assert.assertEquals(result, 1);
	}

	// Verify the 2 lines of the EDRs Version 2
	@Parameters({ "param2", "param2EL"})
	@Test(priority = 14, enabled = false)
	public void checkEdrThreeVersions(String param2, String param2EL) {
		/* Go to EDRs Page */
		homeTests.goToEDRsPage();

		/* Filter by subscription */
		mediationEDRTests.filterBySubscription(subCode);
		waitPageLoaded();

		/*** check result ***/
		//check Event Version size (First version)
		Assert.assertEquals(mediationEDRTests.eventVersion1Size(), 2);

		//check Event Version size (Second version)
		Assert.assertEquals(mediationEDRTests.eventVersion2Size(), 2);

		//check Event Version size (Second version)
		Assert.assertEquals(mediationEDRTests.eventVersion3Size(), 2);

		//check Event Key and Param3 size first EDR;
		Assert.assertEquals(mediationEDRTests.getEventKeyAndParameter3FirstLineSize(param3), 6);

		//check Event Key and Param3 size second EDR;
		Assert.assertEquals(mediationEDRTests.getEventKeyAndParameter3ELSecondLineSize(param3EL), 6);

		//check Status size (CANCELLED)
		Assert.assertEquals(mediationEDRTests.cancelledStatusSize(), 4);

		//check Status size (RATED)
		Assert.assertEquals(mediationEDRTests.ratedStatusSize(), 2);

		// check param2 first EDR 
		Assert.assertEquals(mediationEDRTests.checkParameter2FirstLine(param2), 3);

		// check param2EL second EDR
		Assert.assertEquals(mediationEDRTests.checkParameter2ELSecondLine(param2EL), 3);

		// check Access Point
		Assert.assertEquals(mediationEDRTests.checkAccessPoint(AP), 3);
	}

	// Verify Wallet operations (1 TO_RERATE, 1 CANCELLED)
	@Test(priority = 16)
	public void checkwalletOperations() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		/*** check result ***/
		// Verify the number of lines found (2 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 2);

		// Verify Status size (1 TO RERATE)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("TO RERATE"), 1);

		// Verify Status size (1 CANCELED)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 1);
	}

	// Run Rerate Job (Rerate_Job)
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 18)
	public void runRerateJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runRerateJob(baseURI, login, password);
	}

	// Run Usage Job (U_Job)
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 20)
	public void runUJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runUJob(baseURI, login, password);
	}	

	// Run Rated Transaction Job (RT_Job)
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 22)
	public void runRTJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}

	// Verify Wallet operations
	@Parameters({ "param2", "param2EL" })
	@Test(priority = 24)
	public void checkwalletOperationsAfterRunningRTjob(String param2, String param2EL) {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		/*** check result ***/
		// Verify the number of lines found (3 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 4);

		// Verify Status size (2 OPEN)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("TREATED"), 2);

		// Verify Status size (1 RERATED)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("RERATED"), 1);

		// Verify Status size (1 CANCELED)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 1);

		// Verify the number of lines found for parameter 2 (AA) (2 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(param2), 2);

		// Verify the number of lines found for parameter 2 EL (BB)(2 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(param2EL), 2);
	}

	// Verify Rated items on costumer page (2 open items)
	@Parameters({ "param2", "param2EL" })
	@Test(priority = 26, enabled = true)
	public void checkRatedItemsOnConstumerPage(String param2, String param2EL) {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Go to rated items tab
		customersTests.GoToRatedItemsTab(subCode);
		waitPageLoaded();

		/*** check result ***/
		//Check 2 open items
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("OPEN"), 2);

		//Check parameter 2
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param2), 1);

		//Check parameter 2 EL
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param2EL), 1);

		// Check parameter 3
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param3), 1);

		//Check parameter 2 EL
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param3EL), 1);
	}
}
