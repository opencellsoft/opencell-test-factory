package com.opencellsoft.testsuites.mediation;

import static io.restassured.RestAssured.given;
import java.awt.AWTException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;
import io.restassured.RestAssured;
import io.restassured.authentication.PreemptiveBasicAuthScheme;

public class EDRTestsDataSet extends EDRTestsCommons {

	/********************************************************************************************
	 ***********************************      Pre-request    ************************************
	 - Create a New Offer
	 - Create a new product
	 - Create a new charge type Usage
	 - Create a new price version for the charge
	 - Add a unit price to the price version
	 - Publish the price version and go back to the usage charge
	 - Activate the usage charge and go back to the product
	 - Publish the product version
	 - Activate the product and go back to the offer
	 - Fill Product Parameters	
	 - Activate offer
	 - Enable EDR Versionning
	 - Create a new customer
	 - Create a new consumer 
	 - Create triggered EDR in admin 
	 - Add triggered EDR to charge
	 ********************************************************************************************/

//	public String code = Constant.offerCode;
	public String code = "001";
	public String offerCode ="EDROFF-" + code;
	public String productCode= "EDRPROD-" + code;
	public String usageChargeCode= "EDRCHA-" + code;
	public String triggerEdrCode = "EDRTRG-" + code;
	
	public String customerCode = "EDRCUST-" + code;
	String bcCode = "EDRBC-" + code;
	String consumerCode = "EDRCUST-" + code;
	
	String offerDescription = Constant.offerDescription;
	String productDescription = Constant.productDescription;
	String chargeDescriptionUsage= Constant.chargeDescriptionUsage;
	String pricePlanVersionCode = "EDRPV-" + Constant.pricePlanVersionCode;
	String customerName = Constant.customerName;
	String customerEmail = Constant.customerEmail;
	String paypalUserId = Constant.paypalUserId;
	String consumerName = Constant.consumerName;
	String companyRegistrationNumber = Constant.companyRegistrationNumber;
	String vatNumber = Constant.vatNumber;
	String param3EL = "#{edr.parameter3}_TRDEDR";
	String companyRegistration = getRandomNumberbetween1And200();

	@Test(priority = 0, enabled = true)
	public void _EDRV_DS() {
		System.out.println("**************************      EDR Versionning : data set     ******************************");
		System.out.println("public String code =\"" + code + "\";");

	}

	/***  create a New Offer   
	 * @throws AWTException 
	 * @throws InterruptedException ***/
	@Parameters({ "PriceVersionDate", "UnitPrice",  "defaultQuantity", "minimalQuantity", "maximalQuantity", "sequence"})
	@Test(priority = 4, enabled = true)
	public void _EDRV_DS_createOffer(String PriceVersionDate, String UnitPrice, String defaultQuantity, String minimalQuantity, String maximalQuantity,
			String sequence) throws InterruptedException, AWTException {
		offersTests.createOfferMethod(offerCode, offerDescription);
		productsTests.createProductMethod(PriceVersionDate,productCode,productDescription );
		chargesTests.createUsageCharge(usageChargeCode,chargeDescriptionUsage,"","", version);
		chargesTests.createNewPriceVersionPriceGrid(pricePlanVersionCode);
		chargesTests.addUnitPriceToPriceVersion(UnitPrice);
		try {
			chargesTests.publishPriceVersion();
		}catch (Exception e) {
			chargesTests.addUnitPriceToPriceVersion(UnitPrice);
			chargesTests.publishPriceVersion();
		}

		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(chargeDescriptionUsage, version);
		try {
			productsTests.publishProductVersion();
		}
		catch (Exception e) {
			chargesTests.goBack();
			productsTests.publishProductVersion();
		}
		productsTests.activateProductAndGoBack();
		offersTests.pickProductToOffer( productCode,productDescription, version);
		offersTests.fillProductParametersMethod(defaultQuantity,minimalQuantity, maximalQuantity, sequence, false );
		offersTests.ActivateOfferMethod();
		waitPageLoaded();
	}

	/***   Enable EDR Versionning   ***/
	@Parameters({"CriteriaEL1", "EventKeyEL1", "NewVersionEL1", "CriteriaEL2", "EventKeyEL2", "NewVersionEL2", "version", "baseURI", "login", "password"})
	@Test(priority = 6, enabled = true)
	public void _EDRV_DS_enableEDRVersioning(String CriteriaEL1, String EventKeyEL1, String NewVersionEL1, String CriteriaEL2, String EventKeyEL2, String NewVersionEL2, String version, String baseURI, String login, String password){
		//********************************** By API ***********************************/
		RestAssured.baseURI = baseURI;
		String body = "{\r\n"
				+ "    \"rules\": [\r\n"
				+ "        {\r\n"
				+ "            \"priority\": 1,\r\n"
				+ "            \"criteriaEL\": \"#{edr.parameter2.equals('AA')}\",\r\n"
				+ "            \"keyEL\": \"#{edr.parameter3}\",\r\n"
				+ "            \"isNewVersionEL\": \"#{edr.eventDate.after(previous.eventDate)}\"\r\n"
				+ "        },\r\n"
				+ "        {\r\n"
				+ "            \"priority\": 2,\r\n"
				+ "            \"criteriaEL\": \"#{edr.parameter2.equals('BB')}\",\r\n"
				+ "            \"keyEL\": \"#{edr.parameter3}\",\r\n"
				+ "            \"isNewVersionEL\": \"#{edr.eventDate.after(previous.eventDate)}\"\r\n"
				+ "        }\r\n"
				+ "    ],\r\n"
				+ "    \"enableEdrVersioning\": true\r\n"
				+ "}";
		// Setting BasePath once
		System.out.println(body);
		RestAssured.basePath = "api/rest/v2/mediationSetting";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;
		given().header("Content-Type", "application/json").body(body).when()
				.post();	

		//********************************** By portal ***********************************/
		//Go to Edr Settings page
		homeTests.goToEDRsSettingsPage(version);

		// Enable Edr Versionning and fill the parameters
		mediationEDRTests.enableEDRVersioning(CriteriaEL1, EventKeyEL1, NewVersionEL1, CriteriaEL2, EventKeyEL2, NewVersionEL2);

	}	

	/***   Create a new customer   
	 * @throws InterruptedException ***/
	@Parameters({ "billingCountry", "billingCycle", "acrs" })
	@Test(priority = 8, enabled = true)
	public void _EDRV_DS_createCustomer(String billingCountry, String billingCycle, String acrs) throws InterruptedException {
		homeTests.goToCustomTablesListPage();
		customersTests.createCustomerCheck(customerCode, customerName, customerEmail, billingCountry,  null,companyRegistration, version);
		customersTests.createConsumerMethod(consumerCode,consumerName, companyRegistrationNumber,vatNumber, acrs,companyRegistration, version );
	}

	/***   Create triggered EDR in admin   ***/
	@Parameters({"baseURI","ConditionEL", "QuantityEL", "param2EL"})
	@Test(priority = 10, enabled = true)
	public void _EDRV_DS_createTriggeredEDR(String baseURI, String ConditionEL, String QuantityEL , String param2EL ) {
		triggeredEDRTests.openNewTab(baseURI);
		triggeredEDRTests.createTriggeredEDR(baseURI, ConditionEL, QuantityEL, param2EL, param3EL, triggerEdrCode, Constant.triggerEdrDescription);
		triggeredEDRTests.addTriggeredEDRToCharge(usageChargeCode, triggerEdrCode, false);
		triggeredEDRTests.closeTab();
	}

	@Test(priority = 12, enabled = true)
	/***   create Billing Cycle   ***/
	public void _APM_DS_createBillingCycle() throws InterruptedException {
		payloadTests.createBillingCycleByAPI(this.baseURI, this.login, this.password,bcCode,bcCode,"BILLINGACCOUNT", "MONTHLY");
		payloadTests.createBillingCycleByAPI(this.baseURI, this.login, this.password,bcCode,bcCode,"BILLINGACCOUNT", "CAL_PERIOD_MONTHLY");
	}

	@Test(priority = 14, enabled = true)
	/***  Activate Functions   ***/
	public void _APM_DS_ActivateFunctions() throws InterruptedException {
		payloadTests.activateFunctions( this.baseURI, this.login, this.password);
	}
}
