package com.opencellsoft.testsuites.mediation;

import java.awt.AWTException;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.utility.Constant;

public class EDRTestsScenario2_NewRerateFonc extends EDRTestsCommons {

	/********************************************************************************************	
	 **********   Scenario 2 : 1st EDR version => RT_job => 2nd EDR version   *******************	
	- Update Triggered EDR
	- Create And Activate a new Subscription
	- Insert ChargeCdr first Version
	- Run RT_Job
	- Check Rated Items On Costumer Page //2 open items 
	- Update Triggered EDR
	- Insert Charge Cdr Second Version 
	- Check cdr Second Version  ( 2 CANCELLED , 2 RATED)
	- Check Rated Items On Costumer Page // 2 CANCELED items 
	- Check wallet Operations  ( 1 CANCELLED , 1 TO RERATE)
	- Run Rerate_Job
	- Check wallet Operations after Running Rerate_job  ( 1 CANCELLED , 1 RERATED, 1 OPEN)
	- Run U_Job
	- Check wallet Operations after Running U_job  ( 1 CANCELLED , 1 RERATED, 2 OPEN)
	- Run RT_Job
	- Check Rated Items On Costumer Page ( 2 CANCELED items , 2 OPEN items)
	/********************************************************************************************/
	EDRTestsDataSet dataSet = new EDRTestsDataSet();
	String customerCode = dataSet.customerCode;
	String offerCode = dataSet.offerCode;
	String triggerEdrCode = dataSet.triggerEdrCode;
	String subCode = "EDRSUB02_" + Constant.subscriptionCode;
	String AP = "EDRAP02" + Constant.accessPointCode;
	String param3 = Constant.param3 + "_02";
	String param3EL = param3 + "_TRDEDR";

	@Test(priority = 0, enabled = true)
	public void _EDRV_T02() {
		System.out.println("***************************      EDR Versionning Scenario 2 : 1st EDR version => RT_job => 2nd EDR version   ************************************");
	}

	// Create and Activate a new Subscription
	@Test(priority = 6)
	public void _EDRV_T02_createAndActivateSubscription() throws AWTException, InterruptedException{
	
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);
	}

	// Insert EDRs, first version 
	@Parameters({"param2" })
	@Test(priority = 8)
	public void _EDRV_T02_insertChargeCdrFirstVersion( String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"4","", param2, param3);
		Assert.assertEquals(result, 1);
	}

	// Run Rated Transaction Job (RT_Job)
	@Test(priority = 10)
	public void _EDRV_T02_runRTJob() throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}

	// Insert EDRs, second version 
	@Parameters({"param2" })
	@Test(priority = 14)
	public void _EDRV_T02_insertChargeCdrSecondVersion(String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"4", "", param2, param3);
		Assert.assertEquals(result, 1);
	}

	// Verify Wallet operations (1 TO_RERATE, 1 CANCELLED)
	@Test(priority = 20)
	public void _EDRV_T02_checkwalletOperations() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		/*** check result ***/
		// Verify the number of lines found (2 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 4);

		// Verify Status size (1 TO RERATE)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("OPEN"), 2);

		// Verify Status size (1 CANCELED)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 2);
	}

	// Run Rated Transaction Job (RT_Job)
	@Test(priority = 32)
	public void _EDRV_T02_runRTJob2() throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}	

	// Verify Rated items on costumer page (2 open items, 2 Canceled items )
	@Parameters({ "param2", "param2EL" })
	@Test(priority = 34)
	public void _EDRV_T02_checkRatedItemsOnCustomerPage(String param2, String param2EL) {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Go to rated items tab
		customersTests.GoToRatedItemsTab(subCode);
		waitPageLoaded();

		/*** check result ***/
		//Check 2 open items
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("OPEN"), 2);

		//Check 2 open items
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("Canceled"), 2);

		//Check parameter 2
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param2), 2);

		//Check parameter 2 EL
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param2EL), 2);

		// Check parameter 3
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param3), 2);

		//Check parameter 2 EL
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param3EL), 2);
	}
}
