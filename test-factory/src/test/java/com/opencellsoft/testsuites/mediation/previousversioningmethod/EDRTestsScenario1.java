package com.opencellsoft.testsuites.mediation.previousversioningmethod;

import java.awt.AWTException;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.pages.customercare.rateditems.RatedItemsPage;
import com.opencellsoft.tests.CustomersTests;
import com.opencellsoft.tests.HomeTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.tests.MediationEDRTests;
import com.opencellsoft.tests.WalletOperationsTests;
import com.opencellsoft.tests.customercare.SubscriptionsTests;
import com.opencellsoft.tests.jobs.JobsTests;
import com.opencellsoft.testsuites.mediation.EDRTestsCommons;
import com.opencellsoft.testsuites.mediation.EDRTestsDataSet;
import com.opencellsoft.utility.Constant;

public class EDRTestsScenario1 extends EDRTestsCommons {

	/********************************************************************************************
	 ********  Scenario 1 : 1st EDR version =>  2nd EDR version => RT_job  **********************
	-  Create And Activate a new Subscription
	-  Insert Charge Cdr FirstVersion
	-  Check Edr FirstVersion (2 EDRs Version 1 RATED)
	-  Insert Charge Cdr Second Version
	-  Check Edr Second Version (4 EDRS version : 2 EDRs of version1 are CANCELED,2 EDRs of version2 are 2 RATED)
	-  Check wallet Operations (1 TO_RERATE, 1 CANCELLED)
	-  Run Rerate Job
	-  Check Wallet operations (1 OPEN,1 RERATED, 1 CANCELLED)
	-  Run Usage Job (U_Job)
	-  Check EDRs (3 CANCELLED, 2 RATED)
	-  Check Wallet operations (2 OPEN , 1 RERATEED, 1 CANCELLED)
	-  Run Rated Transaction Job (RT_Job)
	-  Check Rated items on costumer page (2 items open)
	 *********************************************************************************************/
	EDRTestsDataSet EDRTestsDataSet = new EDRTestsDataSet();
	String customerCode = EDRTestsDataSet.customerCode;
	String offerCode = EDRTestsDataSet.offerCode;
	String triggerEdrCode = EDRTestsDataSet.triggerEdrCode;
	String param3 = Constant.param3 + "_01";
	String param3EL = param3 + "_TRDEDR";
	String subCode = "EDRSUB01_" + Constant.subscriptionCode;
	String AP = "EDRAP01" + Constant.accessPointCode;
	String version="";
	public EDRTestsScenario1() {
		loginTest = new LoginTest();
		customersTests = new CustomersTests();
		subscriptionsTests = new SubscriptionsTests();
		jobsTests =  new JobsTests();
		mediationEDRTests = new MediationEDRTests();
		ratedItemsPage = new RatedItemsPage();
		homeTests = new HomeTests();
		walletOperationsTests = new WalletOperationsTests();
	}

	/***   login   ***/
	@Parameters({ "login", "password","version" })
	@Test(priority = 2, enabled = true)
	public void login(String login, String password, String version) throws Exception {
		System.out.println("**************************   EDR Versionning  EDR Versionning Scenario 1 : 1st EDR version =>  2nd EDR version => RT_job   ******************************");
		loginTest.loginTest(login, password);
		this.version = version;
	}

	/***   Create a new Subscription   ***/
	@Parameters({ "seller" })
	@Test(priority = 6, enabled = true)
	public void createAndActivateSubscription(String seller) throws AWTException, InterruptedException{
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Create a new subscription from subscription tab in customer details page
		subscriptionsTests.create_subscription_FromCustomerPage(seller, subCode, offerCode);
		// Create Access Point
		subscriptionsTests.createAccessPointMethod(AP);
		// Activate the product instance for the subscription
		try {
			subscriptionsTests.activateProductInstanceMethod();
		}catch (Exception e) {
			subscriptionsTests.goBack();
			subscriptionsTests.activateProductInstanceMethod();
		}
	}

	/***   Insert EDRs, first version   ***/ 
	@Parameters({ "baseURI", "login", "password","param2" })
	@Test(priority = 12)
	public void insertChargeCdrFirstVersion(String baseURI, String login, String password, String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"4", "", param2, param3);
		Assert.assertEquals(result, 1);
	}

	/***   Check EDRs Version 1   ***/
	@Parameters({ "param2", "param2EL"})
	@Test(priority = 14)
	public void checkEdrFirstVersion(String param2, String param2EL) throws InterruptedException {	
		/* Go to EDRs Page */
		homeTests.goToEDRsPage();


		/* Filter by subscription */
		mediationEDRTests.filterBySubscription(subCode);
		waitPageLoaded();

		/*  close Group By Event Key  */
		if(version.equals("15.0.X")) {mediationEDRTests.closeGroupByEventKey();}

		/* check result */
		//check Event Version size
		Assert.assertEquals(mediationEDRTests.eventVersion1Size(), 2);

		//check Event Key and Param3 size first EDR;
		Assert.assertEquals(mediationEDRTests.getEventKeyAndParameter3FirstLineSize(param3), 2);

		//check Event Key and Param3 size second EDR;
		Assert.assertEquals(mediationEDRTests.getEventKeyAndParameter3ELSecondLineSize(param3EL), 2);

		//check Status size (RATED)
		Assert.assertEquals(mediationEDRTests.ratedStatusSize(), 2);

		// check param2 first EDR 
		Assert.assertEquals(mediationEDRTests.checkParameter2FirstLine(param2), 1);

		// check param2EL second EDR
		Assert.assertEquals(mediationEDRTests.checkParameter2ELSecondLine(param2EL), 1);

		// check Access Point first line
		Assert.assertEquals(mediationEDRTests.checkAccessPoint(AP), 1);
	}	

	/***   Insert EDRs, second version   ***/ 
	@Parameters({ "baseURI", "login", "password","param2" })
	@Test(priority = 16)
	public void insertChargeCdrSecondVersion(String baseURI, String login, String password, String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"4", "", param2, param3);
		Assert.assertEquals(result, 1);
	}

	/***   Check EDRs Version 2 (2 RATED, 2 CANCELLED)   ***/
	@Parameters({ "param2", "param2EL"})
	@Test(priority = 18)
	public void checkEdrSecondVersion(String param2, String param2EL) {
		try {
			/* Go to EDRs Page */
			homeTests.goToEDRsPage();
			/*  close Group By Event Key  */
			if(version.equals("15.0.X")) {mediationEDRTests.closeGroupByEventKey();}
			/* Filter by subscription */
			mediationEDRTests.filterBySubscription(subCode);
			waitPageLoaded();

			/* check result */
			//check Event Version size =2 (First version) 
			Assert.assertEquals(mediationEDRTests.eventVersion1Size(), 2);

			//check Event Version size (Second version)
			Assert.assertEquals(mediationEDRTests.eventVersion2Size(), 2);

			//check Event Key and Param3 size first EDR;
			Assert.assertEquals(mediationEDRTests.getEventKeyAndParameter3FirstLineSize(param3), 4);

			//check Event Key and Param3 size second EDR;
			Assert.assertEquals(mediationEDRTests.getEventKeyAndParameter3ELSecondLineSize(param3EL), 4);

			//check Status size (CANCELLED)
			Assert.assertEquals(mediationEDRTests.cancelledStatusSize(), 2);

			//check Status size (RATED)
			Assert.assertEquals(mediationEDRTests.ratedStatusSize(), 2);

			// check param2 first EDR 
			Assert.assertEquals(mediationEDRTests.checkParameter2FirstLine(param2), 2);

			// check param2EL second EDR
			Assert.assertEquals(mediationEDRTests.checkParameter2ELSecondLine(param2EL), 2);

			// check Access Point
			Assert.assertEquals(mediationEDRTests.checkAccessPoint(AP), 2);

		}catch (Exception e) {
			// pass
		}
	}

	/***   Verify Wallet operations (1 TO_RERATE, 1 CANCELLED)   ***/
	@Test(priority = 20)
	public void checkwalletOperations() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();
		// Verify the number of lines found (2 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 2);

		// Verify Status size (1 TO RERATE)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("TO RERATE"), 1);

		// Verify Status size (1 CANCELED)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 1);
	}

	/***   Run Rerate Job (Rerate_Job)   ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 22)
	public void runRerateJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runRerateJob(baseURI, login, password);
	}

	/***   Verify Wallet operations (1 OPEN,1 RERATED, 1 CANCELLED)   ***/
	@Parameters({ "param2", "param2EL" })
	@Test(priority = 24)
	public void checkwalletOperationsAfterRunningRerateJob( String param2, String param2EL) {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();
		// Verify the number of lines found (3 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 3);

		// Verify Status size (1 OPEN)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("OPEN"), 1);

		// Verify Status size (1 RERATED)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("RERATED"), 1);

		// Verify Status size (1 CANCELED)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 1);

		// Verify the number of lines found (2 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(param2), 2);

		// Verify the number of lines found (1 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(param2EL), 1);
	}

	/***   Run Usage Job (U_Job)   ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 26)
	public void runUJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runUJob(baseURI, login, password);
	}	

	/***   Verify EDRs (3 CANCELLED, 2 RATED)   ***/
	@Parameters({ "param2", "param2EL"})
	@Test(priority = 28)
	public void checkEdrsAfterRunningUjob(String param2, String param2EL) {
		try {
			/* Go to EDRs Page */
			homeTests.goToEDRsPage();
			/*  close Group By Event Key  */
			if(version.equals("15.0.X")) {mediationEDRTests.closeGroupByEventKey();}
			/* Filter by subscription */
			mediationEDRTests.filterBySubscription(subCode);
			waitPageLoaded();

			/* check result */

			//check Event Version size (First version)
			waitPageLoaded();
			Assert.assertEquals(mediationEDRTests.eventVersion1Size(), 2);

			//check Event Version size (Second version)
			Assert.assertEquals(mediationEDRTests.eventVersion2Size(), 3);

			//check Event Key and Param3 size first EDR;
			Assert.assertEquals(mediationEDRTests.getEventKeyAndParameter3FirstLineSize(param3), 4);

			//check Event Key and Param3 size second EDR;
			Assert.assertEquals(mediationEDRTests.getEventKeyAndParameter3ELSecondLineSize(param3EL), 6);

			//check Status size (3 CANCELLED)
			Assert.assertEquals(mediationEDRTests.cancelledStatusSize(), 3);

			//check Status size (2 RATED)
			Assert.assertEquals(mediationEDRTests.ratedStatusSize(), 2);

			// check param2 first EDR (3)
			Assert.assertEquals(mediationEDRTests.checkParameter2FirstLine(param2), 2);

			// check param2EL second EDR (3)
			Assert.assertEquals(mediationEDRTests.checkParameter2ELSecondLine(param2EL), 3);

			// check Access Point first line (2)
			Assert.assertEquals(mediationEDRTests.checkAccessPoint(AP), 2);			
		}catch (Exception e) {
			// pass
		}

	}	

	/***   Verify Wallet operations (2 OPEN , 1 RERATEED, 1 CANCELLED)   ***/
	@Parameters({ "param2", "param2EL" })
	@Test(priority = 30)
	public void checkwalletOperationsAfterRunningUjob(String param2, String param2EL) {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		// Verify the number of lines found (3 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 4);

		// Verify Status size (2 OPEN)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("OPEN"), 2);

		// Verify Status size (1 RERATED)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("RERATED"), 1);

		// Verify Status size (1 CANCELED)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 1);

		// Verify the number of lines found for parameter 2 (AA) (2 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(param2), 2);

		// Verify the number of lines found for parameter 2 EL (BB)(2 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(param2EL), 2);
	}	

	/***   Run Rated Transaction Job (RT_Job)   ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 32)
	public void runRTJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}	

	/***   Verify Rated items on costumer page (2 items open)   ***/
	@Parameters({ "param2", "param2EL" })
	@Test(priority = 34, enabled = true)
	public void checkRatedItemsOnConstumerPage(String param2, String param2EL) {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Go to rated items tab
		customersTests.GoToRatedItemsTab(subCode);
		waitPageLoaded();

		//Check 2 open items
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("OPEN"), 2);

		//Check parameter 2
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param2), 1);

		//Check parameter 2 EL
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param2EL), 1);

		// Check parameter 3
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param3), 1);

		//Check parameter 3 EL
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param3EL), 1);
	}	
}
