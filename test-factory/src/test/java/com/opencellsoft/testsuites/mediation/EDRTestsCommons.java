package com.opencellsoft.testsuites.mediation;

import static io.restassured.RestAssured.given;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.customercare.rateditems.RatedItemsPage;
import com.opencellsoft.tests.BillingCycleTests;
import com.opencellsoft.tests.BillingrunTests;
import com.opencellsoft.tests.ChargesTests;
import com.opencellsoft.tests.CustomersTests;
import com.opencellsoft.tests.FrameworkAgreementsTests;
import com.opencellsoft.tests.HomeTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.tests.MediationEDRTests;
import com.opencellsoft.tests.OffersTests;
import com.opencellsoft.tests.ProductsTests;
import com.opencellsoft.tests.ScriptTests;
import com.opencellsoft.tests.WalletOperationsTests;
import com.opencellsoft.tests.admin.JobInstancesTests;
import com.opencellsoft.tests.admin.PricePlanMatrixTests;
import com.opencellsoft.tests.admin.TriggeredEDRTests;
import com.opencellsoft.tests.customercare.SubscriptionsTests;
import com.opencellsoft.tests.jobs.JobsTests;
import com.opencellsoft.tests.jobs.PayloadTests;

import io.restassured.RestAssured;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EDRTestsCommons extends TestBase {
	
	protected RatedItemsPage ratedItemsPage;
	protected LoginTest loginTest;
	protected HomeTests homeTests;
	protected OffersTests offersTests;
	protected ChargesTests chargesTests;
	protected CustomersTests customersTests;
	protected ProductsTests productsTests;
	protected WalletOperationsTests walletOperationsTests;
	protected SubscriptionsTests subscriptionsTests;
	protected MediationEDRTests mediationEDRTests;
	protected JobsTests jobsTests;
	protected TriggeredEDRTests triggeredEDRTests;
	protected FrameworkAgreementsTests frameworkAgreementsTests;
	protected ScriptTests scriptTests;
	protected PricePlanMatrixTests pricePlanMatrixTests;
	protected ArrayList<String> tabs;
	protected BillingCycleTests billingCycleTests;
	protected BillingrunTests billingrunTests;
	protected JobInstancesTests jobInstancesTests;
	PayloadTests payloadTests;
	
	protected String version;
	String baseURI; 
	String login;
	String password;
	
	public EDRTestsCommons() {
		loginTest = new LoginTest();
		homeTests = new HomeTests();
		offersTests = new OffersTests();
		productsTests = new ProductsTests();
		chargesTests = new ChargesTests();
		mediationEDRTests = new MediationEDRTests();
		customersTests = new CustomersTests();
		triggeredEDRTests = new TriggeredEDRTests();
		billingrunTests = new BillingrunTests();
		payloadTests = new PayloadTests();
		subscriptionsTests = new SubscriptionsTests();
		jobsTests =  new JobsTests();
		ratedItemsPage = new RatedItemsPage();
		walletOperationsTests = new WalletOperationsTests();
		triggeredEDRTests = new TriggeredEDRTests();
		frameworkAgreementsTests = new FrameworkAgreementsTests();
		scriptTests = new ScriptTests();
		billingCycleTests = new BillingCycleTests();
		jobInstancesTests =  new JobInstancesTests();
	}
	
	@BeforeClass
	@Parameters(value = {"baseURI", "browser", "nodeURL", "login", "password", "version" })
	public void setup(String baseURI, String browser, String nodeURL, String login, String password, String version) throws Exception{
		initialize( browser, nodeURL, baseURI); 
		this.version = version;
		this.baseURI = baseURI; 
		this.login = login;
		this.password = password;
		
		loginTest.loginTest(login, password);
		homeTests.goToPortal();
	}
	
	/***  Teardown  ***/
	@AfterClass
	public void Teardown() {
		TeardownTest();
	}
	
	/***  insert Charge Cdr  ***/
	public int insertChargeCdr(String baseURI, String login, String password, String AP, String quantity, String param1, String param2, String param3) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;
		
		// Setting BasePath once
		RestAssured.basePath = "api/rest/v2/mediation/cdrs/chargeCdrList";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;
		
		// Send request
		Calendar calendar;
		Response resp;
		JsonPath j;
		int l=0;
		
		for (int i=0; i<5; i++){
			if(l != 1) {
				Thread.sleep(1000);
				// get date format "yyyy-MM-dd'T'HH:mm:ss.'00Z'"
				calendar = Calendar.getInstance();
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.'00Z'", Locale.FRANCE);
				String date = format.format(calendar.getTime());

				// create API request body
				String body = "{\r\n"
						+ "    \"cdrs\": [\r\n"
						+ "    \"" + date + ";" + quantity + ";" + AP + ";"+ param1 + ";"+ param2 + ";" + param3 + ";param4;param5;param6;param7;param8;param9;" + date + ";" + date + ";" + date + ";" + date + ";" + date + ";0.1;0.2;0.3;0.4;0.5;extraParam\"\r\n"
						+ "    ],\r\n"
						+ "    \"isRateTriggeredEdr\": true\r\n"
						+ "}";
				System.out.println("Request body : " + body);
				resp = given().header("Content-Type", "application/json").body(body).when()
						.post();

				j = resp.jsonPath();
				l = (Integer)j.get("statistics.success");
				System.out.println("statistics.success : " + l);
			}
			else {
				break;
			}
		}
		
		return l;
	}
	
	public String getRandomNumberbetween1And200() {
		int min = 2;
        int max = 200;

        Random rand = new Random();
        int randomNumber = rand.nextInt(max - min + 2) + min;
        return  String.format("%04d", randomNumber);
	}
}
