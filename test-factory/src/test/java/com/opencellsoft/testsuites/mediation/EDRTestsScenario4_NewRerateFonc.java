package com.opencellsoft.testsuites.mediation;

import java.awt.AWTException;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class EDRTestsScenario4_NewRerateFonc extends EDRTestsCommons {
	/********************************************************************************************
	 **********    Scenario 4  : Triggered EDR with Triggered EDR next charge     ***************

	 1 - Create a New Offer
	 2 - Create a new product
	 3 - Create charge 01 type Usage (Param2 = AA)
	       - Create and Publish a new price version for the charge
	       - Activate the usage charge and go back to the product
	 4 - Create charge 02 type Usage (Param2 = BB)
	       - Create and Publish a new price version for the charge
	       - Activate the usage charge and go back to the product
	 5 - Create charge Deux type Usage (Param1 = Deux, Param2 = AA)
	       - Create and Publish a new price version for the charge
	       - Activate the usage charge and go back to the product
	 6 - Create charge Trois type Usage (Param1 = Trois, Param2 = AA)
	       - Create and Publish a new price version for the charge
	       - Activate the usage charge and go back to the product
	 7 - publish the product version
	 8 - Activate the product and go back to the offer
	 9 - Activate offer
	10 - Create Triggered EDR (Admin)
	11 - Add Triggered EDR to charge 01, check "Trigger next charge" checkbox (Admin)
	12 - Create And Activate a new Subscription
	13 - Insert Charge Cdr 1st Version (param1 = DEUX)
	14 - Check Wallet operations (3 OPEN, 1 charge_001, 1 Charge_002, 1 Deux, 2 AA, 1 BB )
	15 - Insert Charge Cdr 2nd Version (param1 = Trois)
	16 - Check Wallet operations (1 CANCELLED, 2 TO RERATE)
	17 - Run Rerate_Job
	18 - Check Wallet operations (2 OPEN, 1 CANCELLED, 2 RERATED )
	19 - Run U_Job
	20 - Check Wallet operations (3 OPEN, 1 CANCELLED, 2 RERATED )
	21 - Run RT_Job
	22 - Check Rated Items On Costumer Page (3 OPEN items, 2 AA, 1 BB)
	 *********************************************************************************************/

	EDRTestsDataSet dataSet = new EDRTestsDataSet();
	String customerCode = dataSet.customerCode;
	String triggerEdrCode = dataSet.triggerEdrCode;

	String offerCode = "EDROF4-" + Constant.offerCode;
	String productCode = "EDRPR4-" +Constant.productCode + "_001";
	String usageChargeCode_Charge1 = "Charge1_" + Constant.usageChargeCode;
	String usageChargeCode_Charge2 = "Charge2_" + Constant.usageChargeCode;
	String usageChargeCode_Charge3 = "Charge3_" + Constant.usageChargeCode;
	String usageChargeCode_Charge4 = "Charge4_" + Constant.usageChargeCode;

	
	String subCode = "EDRSUB04_" + Constant.subscriptionCode;
	String AP = "EDRAP04" + Constant.accessPointCode;

	String param3 = Constant.param3 + "_04";
	String param3EL = param3 + "_TRDEDR";
	boolean TriggerNextCharge = true;
	String offerDescription = "EDROF4-" + Constant.offerDescription;
	String productDescription = Constant.productDescription + "_001";
	String chargeDescriptionUsage_Charge1 = "Charge1:" + Constant.chargeDescriptionUsage;
	String pricePlanVersionCode_Charge1 = "Charge1_" + Constant.pricePlanVersionCode;
	String chargeDescriptionUsage_Charge2= "Charge2:" + Constant.chargeDescriptionUsage;
	String pricePlanVersionCode_Charge2 = "Charge2_" + Constant.pricePlanVersionCode;
	String chargeDescriptionUsage_Charge3 = "Charge3:" + Constant.chargeDescriptionUsage;
	String pricePlanVersionCode_Charge3 = "Charge3_" + Constant.pricePlanVersionCode;
	String chargeDescriptionUsage_Charge4 = "Charge4:" + Constant.chargeDescriptionUsage;
	String pricePlanVersionCode_Charge4 = "Charge4_" + Constant.pricePlanVersionCode;

	@Test(priority = 1, enabled = true)
	public void _EDRV_T04() {
		System.out.println("*****************************  EDR Versionning Scenario 4  : Triggered EDR with Triggered EDR next charge  ****************************");
		System.out.println("String offerCode = \"" + offerCode + "\";");
		System.out.println("String productCode = \"" + productCode + "\";");
		System.out.println("String usageChargeCode_Charge1 = \"" + usageChargeCode_Charge1 + "\";");
		System.out.println("String usageChargeCode_Charge2 = \"" + usageChargeCode_Charge2 + "\";");
		System.out.println("String usageChargeCode_Charge3 = \"" + usageChargeCode_Charge3 + "\";");
		System.out.println("String usageChargeCode_Charge4 = \"" + usageChargeCode_Charge4 + "\";");
		System.out.println("String AP = \"" + AP + "\";");
		System.out.println("String customerCode = \"" + customerCode + "\";");
	}

	/*** Create a New Offer ***/
	@Parameters({ "PriceVersionDate", "param2", "param2EL", "defaultQuantity", "minimalQuantity", "maximalQuantity", "sequence"})
	@Test(priority = 2, enabled = true)
	public void _EDRV_T04_createOffer(String PriceVersionDate, String param2, String param2EL, String defaultQuantity, String minimalQuantity, String maximalQuantity, String sequence) throws InterruptedException, AWTException {
		offersTests.createOfferMethod(offerCode, offerDescription);
		productsTests.createProductMethod(PriceVersionDate,productCode,productDescription );
		
		// Charge 1
		chargesTests.createUsageCharge(usageChargeCode_Charge1, chargeDescriptionUsage_Charge1,"",param2, version);
		Thread.sleep(2000);
		chargesTests.createNewPriceVersion(pricePlanVersionCode_Charge1, "10");
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(chargeDescriptionUsage_Charge1, version);
		
		// Charge 2
		chargesTests.createUsageCharge(usageChargeCode_Charge2, chargeDescriptionUsage_Charge2,"",param2EL, version);
		Thread.sleep(2000);
		chargesTests.createNewPriceVersion(pricePlanVersionCode_Charge2,"15");
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(chargeDescriptionUsage_Charge2, version);
		
		// Charge 3
		chargesTests.createUsageCharge(usageChargeCode_Charge3, chargeDescriptionUsage_Charge3,usageChargeCode_Charge3,param2, version);
		Thread.sleep(2000);
		chargesTests.createNewPriceVersion(pricePlanVersionCode_Charge3,"20");
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(chargeDescriptionUsage_Charge3, version);
		
		// Charge 4
		chargesTests.createUsageCharge(usageChargeCode_Charge4, chargeDescriptionUsage_Charge4,usageChargeCode_Charge4,param2, version);
		Thread.sleep(2000);
		chargesTests.createNewPriceVersion(pricePlanVersionCode_Charge4,"30");
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(chargeDescriptionUsage_Charge4, version);
		
		productsTests.publishProductVersion();
		productsTests.activateProductAndGoBack();
		
		// Activate offer
		homeTests.goToOffersPage();
		offersTests.searchForOfferMethod(offerCode, offerDescription);
		offersTests.pickProductToOffer(productCode,productDescription, version);
		offersTests.fillProductParametersMethod(defaultQuantity,minimalQuantity, maximalQuantity, sequence, false );
		offersTests.ActivateOfferMethod();
		waitPageLoaded();
	}

	/*** Add triggered EDR to charge  ***/
	@Test(priority = 12, enabled = true)
	public void _EDRV_T04_addTriggeredEDRToCharge() {
		triggeredEDRTests.openNewTab(baseURI);
		triggeredEDRTests.addTriggeredEDRToCharge(usageChargeCode_Charge1,triggerEdrCode, TriggerNextCharge);
		triggeredEDRTests.closeTab();
	}

	/*** Create and Activate a new Subscription  ***/
	@Test(priority = 13)
	public void _EDRV_T04_createAndActivateSubscription() throws AWTException, InterruptedException{
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, offerCode, productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);
	}

	/*** Insert Charge Cdr 1st Version (param1 = DEUX)   ***/
	@Parameters({"param2" })
	@Test(priority = 14)
	public void _EDRV_T04_insertChargeCdrFirstVersion(String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"4", usageChargeCode_Charge3, param2, param3);
		Assert.assertEquals(result, 1);
	}

	/*** Insert Charge Cdr 2nd Version (param1 = Trois)  ***/
	@Parameters({ "param2" })
	@Test(priority = 16)
	public void _EDRV_T04_insertChargeCdrSecondVersion(String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"4", usageChargeCode_Charge4, param2, param3);
		Assert.assertEquals(result, 1);
	}

	/*** Check Wallet operations (1 CANCELLED, 2 TO RERATE)  ***/
	@Parameters({ "param2", "param2EL" })
	@Test(priority = 17)
	public void _EDRV_T04_checkwalletOperations2(String param2, String param2EL) {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		/*** check result ***/
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 6);

		Assert.assertEquals(walletOperationsTests.elementsFoundSize("OPEN"), 3);

		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 3);
	}	

	/*** Run Rated Transaction Job (RT_Job)  ***/
	@Test(priority = 22)
	public void _EDRV_T04_runRTJob() throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}

	/*** Check Rated Items On Costumer Page (3 OPEN items, 2 AA, 1 BB)  ***/
	@Parameters({ "param2", "param2EL"})
	@Test(priority = 23)
	public void _EDRV_T04_checkRatedItemsOnCustomerPage(String param2, String param2EL) {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Go to rated items tab
		customersTests.GoToRatedItemsTab(subCode);
		waitPageLoaded();

		//Check 2 open items
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("OPEN"), 3);

		//Check parameter 2
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param2), 2);

		//Check parameter 2 EL
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param2EL), 1);

		// Check parameter 3
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param3), 2);

		//Check parameter 2 EL
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param3EL), 1);
	}
}
