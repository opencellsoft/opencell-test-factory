package com.opencellsoft.testsuites.mediation;

import java.awt.AWTException;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class EDRTestsScenario3_NewRerateFonc extends EDRTestsCommons {
	/*********************************************************************************************
	 *********************    Scenario 3 : add 3 versions  => RT_job *****************************
	- Update Triggered EDR
	- Create And Activate a new Subscription
	- Insert Charge Cdr 1st Version
	- Insert Charge Cdr 2 nd Version
	- Insert Charge Cdr 3 rd Version
	- Check Edr third Version  ====>  (2 EDRS version 1 CANCELED,2 EDRS version 2 CANCELED,2 EDRS version 3 RATED )
	- Run Rerate_Job
	- Run U_Job
	- Run RT_Job
	- Check Wallet operations (2 TREATED, 1 CANCELLED, 1 RERATEED )
	- Check Rated Items On Costumer Page   ====>  2 OPEN items
	 **********************************************************************************************/
	EDRTestsDataSet dataSet = new EDRTestsDataSet();
	String customerCode = dataSet.customerCode;
	String offerCode = dataSet.offerCode;
	String triggerEdrCode = dataSet.triggerEdrCode;
	String subCode = "EDRSUB03_" + Constant.subscriptionCode;
	String AP = "EDRAP03" + Constant.accessPointCode;
	String param3 = Constant.param3 + "_03";
	String param3EL = param3 + "_TRDEDR";	


	@Test(priority = 2, enabled = true)
	public void _EDRV_T03(){
		System.out.println("**********************************  EDR Versionning Scenario 3 : add 3 versions  => RT_job  *******************************");
	}

	// Create and Activate a new Subscription
	@Test(priority = 6)
	public void _EDRV_T03_createAndActivateSubscription() throws AWTException, InterruptedException{
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);
	}

	// Insert EDRs, first version 
	@Parameters({"param2" })
	@Test(priority = 8)
	public void _EDRV_T03_insertChargeCdrFirstVersion(String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"4", "", param2, param3);
		Assert.assertEquals(result, 1);
	}

	// Insert EDRs, second version 
	@Parameters({"param2" })
	@Test(priority = 10)
	public void _EDRV_T03_insertChargeCdrSecondVersion(String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"4", "", param2, param3);
		Assert.assertEquals(result, 1);
	}

	// Insert EDRs, third version 
	@Parameters({"param2" })
	@Test(priority = 12)
	public void _EDRV_T03_insertChargeCdrThridVersion(String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"4", "", param2, param3);
		Assert.assertEquals(result, 1);
	}

	// Verify the 2 lines of the EDRs Version 2
	@Parameters({ "param2", "param2EL"})
	@Test(priority = 14)
	public void _EDRV_T03_checkEdrThreeVersions(String param2, String param2EL) {
		/* Go to EDRs Page */
		homeTests.goToEDRsPage();

		/* Filter by subscription */
		mediationEDRTests.filterBySubscription(subCode);
		waitPageLoaded();

		/*** check result ***/
		//check Event Version size (First version)
		Assert.assertEquals(mediationEDRTests.eventVersion1Size(), 2);

		//check Event Version size (Second version)
		Assert.assertEquals(mediationEDRTests.eventVersion2Size(), 2);

		//check Event Version size (Second version)
		Assert.assertEquals(mediationEDRTests.eventVersion3Size(), 2);

		//check Event Key and Param3 size first EDR;
		Assert.assertEquals(mediationEDRTests.getEventKeyAndParameter3FirstLineSize(param3), 6);

		//check Event Key and Param3 size second EDR;
		Assert.assertEquals(mediationEDRTests.getEventKeyAndParameter3ELSecondLineSize(param3EL), 6);

		//check Status size (CANCELLED)
		Assert.assertEquals(mediationEDRTests.cancelledStatusSize(), 4);

		//check Status size (RATED)
		Assert.assertEquals(mediationEDRTests.ratedStatusSize(), 2);

		// check param2 first EDR 
		Assert.assertEquals(mediationEDRTests.checkParameter2FirstLine(param2), 3);

		// check param2EL second EDR
		Assert.assertEquals(mediationEDRTests.checkParameter2ELSecondLine(param2EL), 3);

		// check Access Point
		Assert.assertEquals(mediationEDRTests.checkAccessPoint(AP), 3);
	}

	// Run Rated Transaction Job (RT_Job)
	@Test(priority = 22)
	public void _EDRV_T03_runRTJob() throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}

	// Verify Wallet operations
	@Parameters({ "param2", "param2EL" })
	@Test(priority = 24, enabled = false)
	public void _EDRV_T03_checkwalletOperations(String param2, String param2EL) {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		/*** check result ***/

		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 6);

		Assert.assertEquals(walletOperationsTests.elementsFoundSize("TREATED"), 2);

		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 4);
	}

	// Verify Rated items on costumer page (2 open items)
	@Parameters({ "param2", "param2EL" })
	@Test(priority = 26)
	public void _EDRV_T03_checkRatedItemsOnCustomerPage(String param2, String param2EL) {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Go to rated items tab
		customersTests.GoToRatedItemsTab(subCode);
		waitPageLoaded();

		/*** check result ***/
		//Check 2 open items
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("OPEN"), 2);

		//Check parameter 2
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param2), 1);

		//Check parameter 2 EL
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param2EL), 1);

		// Check parameter 3
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param3), 1);

		//Check parameter 2 EL
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param3EL), 1);
	}
}
