package com.opencellsoft.testsuites.mediation.previousversioningmethod;

import java.awt.AWTException;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.pages.customercare.rateditems.RatedItemsPage;
import com.opencellsoft.pages.subscriptions.AccessPointsPage;
import com.opencellsoft.tests.CustomersTests;
import com.opencellsoft.tests.HomeTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.tests.MediationEDRTests;
import com.opencellsoft.tests.WalletOperationsTests;
import com.opencellsoft.tests.customercare.SubscriptionsTests;
import com.opencellsoft.tests.jobs.JobsTests;
import com.opencellsoft.testsuites.mediation.EDRTestsCommons;
import com.opencellsoft.testsuites.mediation.EDRTestsDataSet;
import com.opencellsoft.utility.Constant;

public class EDRTestsScenario2 extends EDRTestsCommons {

	/********************************************************************************************	
	 **********   Scenario 2 : 1st EDR version => RT_job => 2nd EDR version   *******************	
	- Update Triggered EDR
	- Create And Activate a new Subscription
	- Insert ChargeCdr first Version
	- Run RT_Job
	- Check Rated Items On Costumer Page //2 open items 
	- Update Triggered EDR
	- Insert Charge Cdr Second Version 
	- Check cdr Second Version  ( 2 CANCELLED , 2 RATED)
	- Check Rated Items On Costumer Page // 2 CANCELED items 
	- Check wallet Operations  ( 1 CANCELLED , 1 TO RERATE)
	- Run Rerate_Job
	- Check wallet Operations after Running Rerate_job  ( 1 CANCELLED , 1 RERATED, 1 OPEN)
	- Run U_Job
	- Check wallet Operations after Running U_job  ( 1 CANCELLED , 1 RERATED, 2 OPEN)
	- Run RT_Job
	- Check Rated Items On Costumer Page ( 2 CANCELED items , 2 OPEN items)
	/********************************************************************************************/
	EDRTestsDataSet EDRTestsDataSet = new EDRTestsDataSet();
	String customerCode = EDRTestsDataSet.customerCode;
	String offerCode = EDRTestsDataSet.offerCode;
	String triggerEdrCode = EDRTestsDataSet.triggerEdrCode;
	String subCode = "EDRSUB02_" + Constant.subscriptionCode;
	String AP = "EDRAP02" + Constant.accessPointCode;
	String param3 = Constant.param3 + "_02";
	String param3EL = param3 + "_TRDEDR";

	public EDRTestsScenario2() {
		loginTest = new LoginTest();
		customersTests = new CustomersTests();
		subscriptionsTests = new SubscriptionsTests();
		jobsTests =  new JobsTests();
		ratedItemsPage = new RatedItemsPage();
		homeTests = new HomeTests();
		walletOperationsTests = new WalletOperationsTests();
		mediationEDRTests = new MediationEDRTests();
	}

	// login
	@Parameters({ "login", "password" })
	@Test(priority = 2, enabled = true)
	public void login(String login, String password) throws Exception {
		System.out.println("***************************      EDR Versionning Scenario 2 : 1st EDR version => RT_job => 2nd EDR version   ************************************");
		loginTest.loginTest(login, password);
	}

	// Create and Activate a new Subscription
	@Parameters({ "seller" })
	@Test(priority = 6)
	public void createAndActivateSubscription(String seller) throws AWTException, InterruptedException{
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Create a new subscription from subscription tab in customer details page
		subscriptionsTests.create_subscription_FromCustomerPage(seller, subCode, offerCode);

		// Create Access point
		subscriptionsTests.createAccessPointMethod(AP);
		try {
			// Activate the product instance for the subscription
			subscriptionsTests.activateProductInstanceMethod();
		}catch (Exception e) {
			AccessPointsPage accessPointsPage = new AccessPointsPage();
			accessPointsPage.clickOnGoBackButton();
			subscriptionsTests.activateProductInstanceMethod();
		}
	}

	// Insert EDRs, first version 
	@Parameters({ "baseURI", "login", "password","param2" })
	@Test(priority = 8)
	public void insertChargeCdrFirstVersion(String baseURI, String login, String password, String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"4","", param2, param3);
		Assert.assertEquals(result, 1);
	}

	// Run Rated Transaction Job (RT_Job)
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 10)
	public void runRTJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}

	// Verify Rated items on costumer page (2 items open)
	@Parameters({ "param2", "param2EL" })
	@Test(priority = 12, enabled = false)
	public void checkRatedItemsOnCustumerPageOpen(String param2, String param2EL) {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Go to rated items tab
		customersTests.GoToRatedItemsTab(subCode);
		waitPageLoaded();

		/*** check result ***/
		//Check 2 open items
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("OPEN"), 2);
		//Check parameter 2
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param2), 1);

		//Check parameter 2 EL
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param2EL), 1);

		// Check parameter 3
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param3), 1);

		//Check parameter 2 EL
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param3EL), 1);
	}

	// Insert EDRs, second version 
	@Parameters({ "baseURI", "login", "password","param2" })
	@Test(priority = 14)
	public void insertChargeCdrSecondVersion(String baseURI, String login, String password, String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"4", "", param2, param3);
		Assert.assertEquals(result, 1);
	}

	// Verify the 2 lines of the EDRs Version 2
	@Parameters({ "param2", "param2EL"})
	@Test(priority = 16, enabled = false)
	public void checkEdrTwoVersion(String param2, String param2EL) {
		/* Go to EDRs Page */
		homeTests.goToEDRsPage();

		/* Filter by subscription */
		mediationEDRTests.filterBySubscription(subCode);
		waitPageLoaded();

		/*** check result ***/
		//check Event Version size (First version)
		Assert.assertEquals(mediationEDRTests.eventVersion1Size(), 2);

		//check Event Version size (Second version)
		Assert.assertEquals(mediationEDRTests.eventVersion2Size(), 2);

		//check Event Key and Param3 size first EDR;
		Assert.assertEquals(mediationEDRTests.getEventKeyAndParameter3FirstLineSize(param3), 4);

		//check Event Key and Param3 size second EDR;
		Assert.assertEquals(mediationEDRTests.getEventKeyAndParameter3ELSecondLineSize(param3EL), 4);

		//check Status size (CANCELLED)
		Assert.assertEquals(mediationEDRTests.cancelledStatusSize(), 2);

		//check Status size (RATED)
		Assert.assertEquals(mediationEDRTests.ratedStatusSize(), 2);

		// check param2 first EDR 
		Assert.assertEquals(mediationEDRTests.checkParameter2FirstLine(param2), 2);

		// check param2EL second EDR
		Assert.assertEquals(mediationEDRTests.checkParameter2ELSecondLine(param2EL), 2);

		// check Access Point
		Assert.assertEquals(mediationEDRTests.checkAccessPoint(AP), 2);
	}

	// Verify Rated items on costumer page (2 items CANCELED)
	@Parameters({ "param2", "param2EL" })
	@Test(priority = 18, enabled = false)
	public void checkRatedItemsOnConstumerPageCanceled(String param2, String param2EL) {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Go to rated items tab
		try {
			customersTests.GoToRatedItemsTab(subCode);
		}catch (Exception e) {
			//Search a customer
			customersTests.searchForCustomerMethod(customerCode);
			customersTests.GoToRatedItemsTab(subCode);
		}

		waitPageLoaded();

		/*** check result ***/
		//Check 2 open items
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("Canceled"), 2);
		//Check parameter 2
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param2), 1);

		//Check parameter 2 EL
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param2EL), 1);

		// Check parameter 3
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param3), 1);

		//Check parameter 2 EL
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param3EL), 1);
	}

	// Verify Wallet operations (1 TO_RERATE, 1 CANCELLED)
	@Test(priority = 20)
	public void checkwalletOperations() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		/*** check result ***/
		// Verify the number of lines found (2 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 2);

		// Verify Status size (1 TO RERATE)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("TO RERATE"), 1);

		// Verify Status size (1 CANCELED)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 1);
	}

	// Run Rerate Job (Rerate_Job)
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 22)
	public void runRerateJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runRerateJob(baseURI, login, password);
	}

	// Verify Wallet operations (1 OPEN,1 RERATED, 1 CANCELLED)
	@Parameters({ "param2", "param2EL" })
	@Test(priority = 24)
	public void checkwalletOperationsAfterRunningRerateJob( String param2, String param2EL) {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		/*** check result ***/
		// Verify the number of lines found (3 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 3);

		// Verify Status size (1 OPEN)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("OPEN"), 1);

		// Verify Status size (1 RERATED)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("RERATED"), 1);

		// Verify Status size (1 CANCELED)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 1);

		// Verify the number of lines found (2 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(param2), 2);

		// Verify the number of lines found (1 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(param2EL), 1);
	}

	// Run Usage Job (U_Job)
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 26)
	public void runUJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runUJob(baseURI, login, password);
	}	

	// Verify EDRs (3 lines CANCELLED, 2 lines RATED)
	@Parameters({ "param2", "param2EL"})
	@Test(priority = 28, enabled = false)
	public void checkEdrsAfterRunningUjob(String param2, String param2EL) {
		/* Go to EDRs Page */
		homeTests.goToEDRsPage();

		/* Filter by subscription */
		mediationEDRTests.filterBySubscription(subCode);
		waitPageLoaded();

		/*** check result ***/
		//check Event Version size (First version)
		Assert.assertEquals(mediationEDRTests.eventVersion1Size(), 2);

		//check Event Version size (Second version)
		Assert.assertEquals(mediationEDRTests.eventVersion2Size(), 3);

		//check Event Key and Param3 size first EDR;
		Assert.assertEquals(mediationEDRTests.getEventKeyAndParameter3FirstLineSize(param3), 4);

		//check Event Key and Param3 size second EDR;
		Assert.assertEquals(mediationEDRTests.getEventKeyAndParameter3ELSecondLineSize(param3EL), 6);

		//check Status size (CANCELLED)
		Assert.assertEquals(mediationEDRTests.cancelledStatusSize(), 3);

		//check Status size (RATED)
		Assert.assertEquals(mediationEDRTests.ratedStatusSize(), 2);

		// check param2 first EDR 
		Assert.assertEquals(mediationEDRTests.checkParameter2FirstLine(param2), 2);

		// check param2EL second EDR
		Assert.assertEquals(mediationEDRTests.checkParameter2ELSecondLine(param2EL), 3);

		// check Access Point first line
		Assert.assertEquals(mediationEDRTests.checkAccessPoint(AP), 2);
	}	

	// Verify Wallet operations (2 open , 1 RERATEED, 1 CANCELLED)
	@Parameters({ "param2", "param2EL" })
	@Test(priority = 30, enabled = false)
	public void checkwalletOperationsAfterRunningUjob(String param2, String param2EL) {

		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		/*** check result ***/
		// Verify the number of lines found (3 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 4);

		// Verify Status size (2 OPEN)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("OPEN"), 2);

		// Verify Status size (1 RERATED)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("RERATED"), 1);

		// Verify Status size (1 CANCELED)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 1);

		// Verify the number of lines found for parameter 2 (AA) (2 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(param2), 2);

		// Verify the number of lines found for parameter 2 EL (BB)(2 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(param2EL), 2);
	}

	// Run Rated Transaction Job (RT_Job)
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 32)
	public void runRTJob2(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}	

	// Verify Rated items on costumer page (2 open items, 2 Canceled items )
	@Parameters({ "param2", "param2EL" })
	@Test(priority = 34, enabled = true)
	public void checkRatedItemsOnConstumerPage(String param2, String param2EL) {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Go to rated items tab
		customersTests.GoToRatedItemsTab(subCode);
		waitPageLoaded();

		/*** check result ***/
		//Check 2 open items
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("OPEN"), 2);

		//Check 2 open items
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("Canceled"), 2);

		//Check parameter 2
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param2), 2);

		//Check parameter 2 EL
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param2EL), 2);

		// Check parameter 3
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param3), 2);

		//Check parameter 2 EL
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param3EL), 2);
	}
}
