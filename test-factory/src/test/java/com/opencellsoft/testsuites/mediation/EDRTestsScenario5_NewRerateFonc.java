package com.opencellsoft.testsuites.mediation;

import java.awt.AWTException;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class EDRTestsScenario5_NewRerateFonc extends EDRTestsCommons  {

	String offerCode = "EDROFF5-" + Constant.offerCode;
	String productCode = "EDRPROD5-" +Constant.offerCode;
	String usageChargeCode = "EDRCHA5-" + Constant.offerCode;

	EDRTestsDataSet dataSet = new EDRTestsDataSet();
	String customerCode = dataSet.customerCode;	
	String offerDescription = "EDROFF5_" + Constant.offerCode;
	String productDescription = "EDRPROD5_" +Constant.offerCode ;
	String chargeDescriptionUsage = "EDRPROD5_" +Constant.offerCode;
	String pricePlanVersionCode_Charge1 = "EDRPV5-" + Constant.offerCode;
	String subCode = "EDRSUB05-" + Constant.offerCode;
	String AP = "EDRAP5-" + Constant.offerCode;
	String param3 = Constant.param3 + "_05";
	String contractCode = "EDRC5-" + Constant.offerCode;
	String contractDescription = "EDRC5_" + Constant.offerCode ;
	String contractlineCode = "EDRC5CL1-" + Constant.offerCode;
	String discount = "50";

	@Test(priority = 0, enabled = true)
	public void _EDRV_T05() {
		System.out.println("*****************************  EDR Versionning Scenario 5  : EDR versionning & FA  ****************************");
		System.out.println("String offerCode =\"" + offerCode + "\";");
		System.out.println("String productCode= \"" + productCode + "\";");
		System.out.println("String usageChargeCode= \"" + usageChargeCode + "\";");
	}

	/*** Create a New Offer  ***/
	@Parameters({ "PriceVersionDate", "defaultQuantity", "minimalQuantity", "maximalQuantity", "sequence" })
	@Test(priority = 6, enabled = true)
	public void _EDRV_T05_createOffer(String PriceVersionDate, String defaultQuantity, String minimalQuantity, String maximalQuantity, String sequence) throws InterruptedException, AWTException {
		// Create a New Offer
		offersTests.createOfferMethod(offerCode, offerDescription);

		// Create a new product
		productsTests.createProductMethod(PriceVersionDate,productCode,productDescription );

		// add new usage charge "Charge1" to the product
		productsTests.searchForProductMethod(productCode, productDescription);
		chargesTests.createUsageCharge(usageChargeCode, chargeDescriptionUsage,"","", version);
		Thread.sleep(2000);
		chargesTests.createNewPriceVersion(pricePlanVersionCode_Charge1, "10");
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(chargeDescriptionUsage, version);

		// publish the product
		productsTests.publishProductVersion();
		// activate the product
		productsTests.activateProductAndGoBack();
		// search for the offer
		homeTests.goToOffersPage();
		offersTests.searchForOfferMethod(offerCode, offerDescription);
		// pick product to the offer
		offersTests.pickProductToOffer(productCode,productDescription, version);
		offersTests.fillProductParametersMethod(defaultQuantity,minimalQuantity, maximalQuantity, sequence, false );
		// Activate the offer
		offersTests.ActivateOfferMethod();
		waitPageLoaded();
	}

	/***   Create and Activate a new Subscription   ***/
	@Test(priority = 20, enabled = true)
	public void _EDRV_T05_createAndActivateSubscription() throws AWTException, InterruptedException{
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, offerCode, productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);
	}

	/***   Create new Framework Agreement   ***/
	@Parameters({ "level", "CLTypeGlobalDiscount"})
	@Test(priority = 23, enabled = true)
	public void _EDRV_T05_createFrameworkAgreement( String level, String CLTypeGlobalDiscount) {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();
		frameworkAgreementsTests.createNewframeworkAgreementInPresent(contractCode, contractDescription, level,customerCode);
		// Add contract line to Framework Agreement 
		frameworkAgreementsTests.AddContractLineGlobalDiscount(contractlineCode, contractlineCode, offerCode, productCode, usageChargeCode,CLTypeGlobalDiscount, discount, true );
		// activate contract
		frameworkAgreementsTests.activateContract();
	}

	/***   Insert EDRs   ***/
	@Parameters({"param2" })
	@Test(priority = 28)
	public void _EDRV_T05_insertChargeCdrFirstVersion(String param2) throws InterruptedException {
		int result = jobsTests.insertChargeCdr(baseURI, login, password, AP,"", param2, param3);
		Assert.assertEquals(result, 1);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 30, enabled = false)
	public void _EDRV_T05_checkwalletOperations1() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		// Verify the number of lines found (1 line)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 2);
	}

	/***   Insert EDRs   ***/
	@Parameters({"param2" })
	@Test(priority = 32)
	public void _EDRV_T05_insertChargeCdrSecondVersion(String param2) throws InterruptedException {
		int result = jobsTests.insertChargeCdr(baseURI, login, password, AP,"", param2, param3);
		Assert.assertEquals(result, 1);
	}

	/*** Run Rated Transaction Job (RT_Job)  ***/
	@Test(priority = 35)
	public void _EDRV_T05_runRTJob() throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 36, enabled = true)
	public void _EDRV_T05_checkwalletOperations2() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		// Verify the number of lines found (4 lines)
		int size = walletOperationsTests.elementsFoundSize(subCode);
		if(size == 0) {
			walletOperationsTests.searchBySubscriptionCode(subCode);
			waitPageLoaded();
		}
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 4);
	}

	@Parameters({ "param2", "param2EL"})
	@Test(priority = 38)
	public void _EDRV_T05_checkRatedItemsOnCustomerPage(String param2, String param2EL) {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Go to rated items tab
		customersTests.GoToRatedItemsTab(subCode);
		waitPageLoaded();

		//Check 2 open items
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("OPEN"), 2);
	}

	/***   Cancel contract ***/
	@Test(priority = 40)
	public void _EDRV_T05_cancelFrameworkAgreement() {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();

		// search FA by code
		frameworkAgreementsTests.searchContractByCode(contractCode);

		// close FA
		frameworkAgreementsTests.closeContract();
	}
}
