package com.opencellsoft.testsuites.mediation.previousversioningmethod;

import java.awt.AWTException;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.testsuites.mediation.EDRTestsCommons;
import com.opencellsoft.testsuites.mediation.EDRTestsDataSet;
import com.opencellsoft.utility.Constant;

public class EDRTestsScenario5 extends EDRTestsCommons  {

	String offerCode = "EDROFF5-" + Constant.offerCode;
	String productCode = "EDRPROD5-" +Constant.productCode;
	String usageChargeCode = "EDRCHA5-" + Constant.usageChargeCode;

	EDRTestsDataSet dataSet = new EDRTestsDataSet();
	String customerCode = dataSet.customerCode;	
	String offerDescription = "EDROFF5" + Constant.offerDescription;
	String subCode = "EDRSUB05_" + Constant.subscriptionCode;
	String AP = "EDRAP5" + Constant.accessPointCode;
	String productDescription = Constant.productDescription ;
	String chargeDescriptionUsage = Constant.chargeDescriptionUsage;
	String pricePlanVersionCode_Charge1 = "EDRPV5" + Constant.pricePlanVersionCode;
	String param3 = Constant.param3 + "_05";
	String contractCode = "EDRC5_" + Constant.contractCode;
	String contractDescription = "EDRC5_" + Constant.contractDescription ;
	String contractlineCode = "EDRC5CL1_" + Constant.contractLineCode;
	String discount = "50";


	@Test(priority = 2, enabled = true)
	public void login() throws Exception {
		System.out.println("*****************************  EDR Versionning Scenario 5  : EDR versionning & FA  ****************************");
	}

	/*** Create a New Offer ***/
	@Test(priority = 6, enabled = true)
	public void createOffer() {
		offersTests.createOfferMethod(offerCode, offerDescription);
	}

	/*** Create a new product ***/
	@Parameters({ "PriceVersionDate"})
	@Test(priority = 8, enabled = true)
	public void createProduct(String PriceVersionDate) throws InterruptedException, AWTException {
		productsTests.createProductMethod(PriceVersionDate,productCode,productDescription );
	}

	/*** add new usage charge "Charge1" to the product ***/
	@Test(priority = 10, enabled = true)
	public void addNewUsageCharge() throws InterruptedException{
		productsTests.searchForProductMethod(productCode, productDescription);
		chargesTests.createUsageCharge(usageChargeCode, chargeDescriptionUsage,"","", version);
		Thread.sleep(2000);
		chargesTests.createNewPriceVersion(pricePlanVersionCode_Charge1, "10");
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
	}

	/*** publish the product version  ***/
	@Test(priority = 12, enabled = true)
	public void publishProductVersion() {	
		productsTests.publishProductVersion();
	}

	/*** Activate the product and go back to the offer  ***/
	@Test(priority = 14, enabled = true)
	public void activateProduct(){
		productsTests.activateProductAndGoBack();
	}

	/*** Activate offer  ***/
	@Parameters({ "defaultQuantity", "minimalQuantity", "maximalQuantity", "sequence" })
	@Test(priority = 16, enabled = true)
	public void activateOffer(String defaultQuantity, String minimalQuantity, String maximalQuantity, String sequence) throws InterruptedException{
		homeTests.goToOffersPage();
		offersTests.searchForOfferMethod(offerCode, offerDescription);
		offersTests.pickProductToOffer(productCode,productDescription, version);
		offersTests.fillProductParametersMethod(defaultQuantity,minimalQuantity, maximalQuantity, sequence, false );
		offersTests.ActivateOfferMethod();
		waitPageLoaded();
	}

	/***   Create and Activate a new Subscription   ***/
	@Parameters({ "seller" })
	@Test(priority = 20, enabled = true)
	public void createAndActivateSubscription(String seller) throws AWTException, InterruptedException{
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);
		// Create a new subscription from subscription tab in customer details page
		subscriptionsTests.create_subscription_FromCustomerPage(seller, subCode, offerCode);

		// Create Access point
		subscriptionsTests.createAccessPointMethod(AP);

		// Activate the product instance for the subscription
		subscriptionsTests.activateProductInstanceMethod();
	}

	/***   Create new Framework Agreement   ***/
	@Parameters({ "level"})
	@Test(priority = 23, enabled = true)
	public void createFrameworkAgreement( String level) {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();
		frameworkAgreementsTests.createNewframeworkAgreementInPresent(contractCode, contractDescription, level,customerCode);
	}

	/***   Add contract line to Framework Agreement   ***/
	@Parameters({ "CLTypeGlobalDiscount"})
	@Test(priority = 24, enabled = true)
	public void addContractLineUsageCharge(String CLTypeGlobalDiscount) {
		frameworkAgreementsTests.AddContractLineGlobalDiscount(contractlineCode, contractlineCode, offerCode, productCode, usageChargeCode,CLTypeGlobalDiscount, discount, true );
	}

	/***   activate contract   ***/
	@Test(priority = 26, enabled = true)
	public void activateContract() {
		frameworkAgreementsTests.activateContract();
	}

	/***   Insert EDRs   ***/
	@Parameters({ "baseURI", "login", "password","param2" })
	@Test(priority = 28)
	public void insertChargeCdrFirstVersion(String baseURI, String login, String password, String param2) throws InterruptedException {
		int result = jobsTests.insertChargeCdr(baseURI, login, password, AP,"", param2, param3);
		Assert.assertEquals(result, 1);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 30, enabled = true)
	public void checkwalletOperations1() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		// Verify the number of lines found (1 line)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 2);
	}

	/***   Insert EDRs   ***/
	@Parameters({ "baseURI", "login", "password","param2" })
	@Test(priority = 32)
	public void insertChargeCdrSecondVersion(String baseURI, String login, String password, String param2) throws InterruptedException {
		int result = jobsTests.insertChargeCdr(baseURI, login, password, AP,"", param2, param3);
		Assert.assertEquals(result, 1);
	}

	/*** Run Rerate Job (Rerate_Job)  ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 33)
	public void runRerateJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runRerateJob(baseURI, login, password);
	}

	/*** Run Usage Job (U_Job)  ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 34)
	public void runUJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runUJob(baseURI, login, password);
	}

	/*** Run Rated Transaction Job (RT_Job)  ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 35)
	public void runRTJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 36, enabled = true)
	public void checkwalletOperations2() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		// Verify the number of lines found (4 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 4);
	}

	/*** Check Rated Items On Costumer Page (3 OPEN items, 2 AA, 1 BB)  ***/
	@Parameters({ "param2", "param2EL"})
	@Test(priority = 38, enabled = false)
	public void checkRatedItemsOnConstumerPage(String param2, String param2EL) {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Go to rated items tab
		customersTests.GoToRatedItemsTab(subCode);
		waitPageLoaded();

		//Check 2 open items
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("OPEN"), 2);
	}

	/***   Cancel contract ***/
	@Test(priority = 40)
	public void cancelFrameworkAgreement() {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();

		// search FA by code
		frameworkAgreementsTests.searchContractByCode(contractCode);

		// close FA
		frameworkAgreementsTests.closeContract();
	}
}
