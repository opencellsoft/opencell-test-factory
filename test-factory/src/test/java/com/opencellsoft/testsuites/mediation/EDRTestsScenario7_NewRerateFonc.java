package com.opencellsoft.testsuites.mediation;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.tests.admin.PostgreeTests;
import com.opencellsoft.utility.Constant;

public class EDRTestsScenario7_NewRerateFonc extends EDRTestsCommons  {
	EDRTestsDataSet dataSet = new EDRTestsDataSet();
	EDRTestsScenario6_NewRerateFonc EDRTest6 = new EDRTestsScenario6_NewRerateFonc();
	String offerCode = EDRTest6.offerCode;
	String productCode = EDRTest6.productCode;
	String usageChargeCode1 = EDRTest6.usageChargeCode1;
	String usageChargeCode2 = EDRTest6.usageChargeCode2;
	String triggerEdrCode = EDRTest6.triggerEdrCode;

	String customerCode = "EDRCUST7-" + Constant.offerCode;
	String billingCycleCode = "BC01_"+ Constant.offerCode;

	String description = "BC01_"+ Constant.offerCode;
	String BillingCalendar = "MONTHLY";
	Boolean IncrementalInvoiceLines = true;
	String applicationEl = "";
	Boolean enableAggregation = true;
	String dateAggregation = "Aggregate by month";
	String discountAggregation = "";
	Boolean useAccountingArticleLabel = true;
	Boolean aggregateUnitPrice = false;
	Boolean ignoreSubscriptions = false;
	Boolean ignoreOrders = false;
	Boolean ignoreConsumers = null;
	Boolean businessKey = null;
	Boolean parameter1 = null;
	Boolean parameter2 = null;
	Boolean parameter3 = null ;

	String customerName = Constant.customerName;
	String customerEmail = Constant.customerEmail;
	String paypalUserId = Constant.paypalUserId;
	String consumerCode = "EDRCONS7-" + Constant.offerCode;
	String consumerName = Constant.consumerName;
	String companyRegistrationNumber = Constant.companyRegistrationNumber;
	String vatNumber = Constant.vatNumber;
	String subCode = "EDRSUB07_" + Constant.subscriptionCode;
	String AP = "EDRAP7_" + Constant.accessPointCode;
	String contractCode = "EDRC7_" + Constant.contractCode;
	String contractDescription = "EDRC7_" + Constant.contractDescription ;
	String contractlineCode = "EDRC7CL1_" + Constant.contractLineCode;
	String discount = "10";
	String unitPrice2 = "150";
	String request = "select quantity,unit_price,amount_without_tax,status from billing_invoice_line ORDER BY id DESC LIMIT 3;";
	String companyRegistration = getRandomNumberbetween1And200();


	@Test(priority = 0, enabled = true)
	public void _EDRV_T07() {
		System.out.println("**************************      EDR Versionning Scenario 7 : EDR versionning - triggered EDR - FA - separation discount WO - billing rule - EligibilityUtils script - OPEN billing run   ******************************");
		System.out.println("String offerCode =\"" + offerCode + "\";");
		System.out.println("String productCode = \"" + productCode + "\";");
		System.out.println("String usageChargeCode1 = \"" + usageChargeCode1 + "\";");
		System.out.println("String usageChargeCode2 = \"" + usageChargeCode2 + "\";");
		System.out.println("String triggerEdrCode = \"" + triggerEdrCode + "\";");
		System.out.println("String customerCode = \"" + consumerCode + "\";");
		System.out.println("String billingCycleCode = \"" + billingCycleCode + "\";");
	}

	/***   create Billing Cycle   ***/
	@Parameters()
	@Test(priority = 4, enabled = true)
	public void _EDRV_T07_createBillingCycle(){
		// Go to billing cycle page
		homeTests.goToBillingCyclePage();
		// Create a new billing cycle
		billingCycleTests.createBillingCycle(billingCycleCode,description,BillingCalendar,IncrementalInvoiceLines,applicationEl);
		billingCycleTests.updateBillingCycleAggregationRules(enableAggregation,dateAggregation,discountAggregation ,useAccountingArticleLabel,aggregateUnitPrice,ignoreSubscriptions,ignoreOrders,ignoreConsumers,businessKey,parameter1,parameter2,parameter3);
	}

	/***   Create a new customer    ***/
	@Parameters({ "billingCountry","acrs" })
	@Test(priority = 6, enabled = true)
	public void _EDRV_T07_createCustomer(String billingCountry, String acrs) throws InterruptedException {
		payloadTests.createCustomer(baseURI, login, password, customerCode, customerName , billingCycleCode);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, offerCode, productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);
	}

	/***   Create new Framework Agreement   ***/
	@Parameters({ "level", "CLTypeGlobalDiscount"})
	@Test(priority = 8, enabled = true)
	public void _EDRV_T07_createFrameworkAgreement( String level, String CLTypeGlobalDiscount) {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();
		// Create new contract
		frameworkAgreementsTests.createNewframeworkAgreementInPresent(contractCode, contractDescription, level,customerCode);
		// AddcContract line
		frameworkAgreementsTests.AddContractLineGlobalDiscount(contractlineCode, contractlineCode, offerCode, productCode, usageChargeCode1,CLTypeGlobalDiscount, discount, true );
		// Activate contract
		frameworkAgreementsTests.activateContract();
	}



	@Test(priority = 12, enabled = true)
	public void _EDRV_T07_updateRT_JOB(){
		// Open new tab and go to admin
		triggeredEDRTests.openNewTab(baseURI);
		// Update job,field: NextJob
		jobInstancesTests.openJobInstancesPage();
		jobInstancesTests.searchForJob("RT_Job");
		jobInstancesTests.updateNextJob("Article_Mapp_Job_V2");
	}

	@Test(priority = 12)
	public void _EDRV_T07_updateTriggeredEDRparam3EL1(){
		// Update Triggered EDR,field: param3EL
		triggeredEDRTests.openTriggeredEDRListPage();
		triggeredEDRTests.searchTriggeredEDR(triggerEdrCode);
		triggeredEDRTests.updateparam3EL("CHINA");
		triggeredEDRTests.searchTriggeredEDR(triggerEdrCode);
		triggeredEDRTests.updateQuantity("1");
	}

	/***   Insert EDRs, first version   ***/ 
	@Test(priority = 14)
	public void _EDRV_T07_insertChargeCdr1() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1" , "", "AA", "JAPAN");
		Assert.assertEquals(result, 1);
	}

	@Test(priority = 16)
	public void _EDRV_T07_updateTriggeredEDRparam3EL2(){
		// Update Triggered EDR,field: param3EL
		triggeredEDRTests.searchTriggeredEDR(triggerEdrCode);
		triggeredEDRTests.updateparam3EL("FRANCE");
	} 

	/***   Insert EDRs, first version   ***/ 
	@Test(priority = 18)
	public void _EDRV_T07_insertChargeCdr2() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1" , "", "AA", "SPAIN");
		Assert.assertEquals(result, 1);
	}	

	@Test(priority = 20)
	public void _EDRV_T07_updateTriggeredEDRparam3EL3(){
		// Update Triggered EDR,field: param3EL
		triggeredEDRTests.searchTriggeredEDR(triggerEdrCode);
		triggeredEDRTests.updateparam3EL("MOROCCO");
	}

	/***   Insert EDRs, first version   ***/ 
	@Test(priority = 22)
	public void _EDRV_T07_insertChargeCdr3() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1" , "", "AA", "TUNISIA");
		Assert.assertEquals(result, 1);
	}	


	@Test(priority = 24)
	public void _EDRV_T07_updateTriggeredEDRparam3EL4(){
		// Update Triggered EDR,field: param3EL
		triggeredEDRTests.searchTriggeredEDR(triggerEdrCode);
		triggeredEDRTests.updateparam3EL("CANADA");
		triggeredEDRTests.closeTab();
	}

	/***   Insert EDRs, first version   ***/ 
	@Test(priority = 26)
	public void _EDRV_T07_insertChargeCdr4() throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1" , "", "AA", "BRASIL");
		Assert.assertEquals(result, 1);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 28)
	public void _EDRV_T07_checkwalletOperations() {

		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 12);

		Assert.assertEquals(walletOperationsTests.elementsFoundSize("OPEN"), 12);
	}

	/***   Run Rated Transaction Job (RT_Job)   ***/
	@Test(priority = 30)
	public void _EDRV_T07_runRTJob() throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   create Billing Run   ***/
	@Test(priority = 32, enabled = true)
	public void _EDRV_T07_createBillingRun(){
//		homeTests.logout();
//		loginTest.login(login, password, version);
//		homeTests.goToPortal();
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.createCycleRun(billingCycleCode,"FULL_AUTOMATIC",null,true,"Quarantine","Quarantine",null,null,null);
		if(version.equals("14.1.X") || version.equals("15.0.X")){
			billingrunTests.searchForBillingRun(null,billingCycleCode,null,null,null);
			billingrunTests.goToBillingRunDetails();
		}
	}

	/***   process Billing Run  ***/
	@Parameters()
	@Test(priority = 34, enabled = true)
	public void _EDRV_T07_processBillingRun(){
		billingrunTests.processBillingRun();
	}

	/***   Check Invoice Lines   ***/
	@Test(priority = 36)
	public void _EDRV_T07_checkInvoiceLines1() {
		String dbURI = baseURI.replace("/opencell/", "/db/");
		PostgreeTests postgreeTests = new PostgreeTests();
		try {
			postgreeTests.openPostgree(dbURI);
			postgreeTests.login();
			postgreeTests.goToRequeteSql();
			postgreeTests.executeRequeteSql(request);
			postgreeTests.checkResult("//td[text() = '4.000000000000']", 3);
			postgreeTests.closeTab();
		}catch (Exception e) {
			postgreeTests.closeTab();
		}
	}

	/***   Insert EDRs, second version   ***/ 
	@Parameters({"param2" })
	@Test(priority = 38)
	public void _EDRV_T07_insertChargeCdrSecondVersion(String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"1" , "", "AA", "BRASIL");
		Assert.assertEquals(result, 1);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 40)
	public void _EDRV_T07_checkwalletOperations2() {

		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 15);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("OPEN"), 3);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 3);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("TREATED"), 9);
	}

	/***   Check Invoice Lines    ***/
	@Test(priority = 42)
	public void _EDRV_T07_checkInvoiceLines2() throws InterruptedException {
		String dbURI = baseURI.replace("/opencell/", "/db/");
		PostgreeTests postgreeTests = new PostgreeTests();
		postgreeTests.openPostgree(dbURI);
		postgreeTests.goToRequeteSql();
		postgreeTests.executeRequeteSql(request);
		postgreeTests.checkResult("//td[text() = '3.000000000000']", 3);
		postgreeTests.closeTab();
	}

	/***   Run Rated Transaction Job (RT_Job)   ***/
	@Test(priority = 44)
	public void _EDRV_T07_runRTJob2() throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   Import New Rated Items And Keep Invoice Lines Open   ***/
	@Parameters()
	@Test(priority = 46, enabled = true)
	public void _EDRV_T07_importNewRatedItemsAndKeepInvoiceLinesOpen(){
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,billingCycleCode,null,null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.importNewRatedItemsAndKeepInvoiceLinesOpen();
	}

	/***   Check Invoice Lines    ***/
	@Test(priority = 48)
	public void _EDRV_T07_checkInvoiceLines3() throws InterruptedException {
		String dbURI = baseURI.replace("/opencell/", "/db/");
		PostgreeTests postgreeTests = new PostgreeTests();
		postgreeTests.openPostgree(dbURI);
		postgreeTests.goToRequeteSql();
		postgreeTests.executeRequeteSql(request);
		postgreeTests.checkResult("//td[text() = '4.000000000000']", 3);
		postgreeTests.closeTab();
	}

	/***   Check Rated items on costumer page   ***/
	@Test(priority = 50, enabled = true)
	public void _EDRV_T07_checkRatedItemsOnCustomerPage() {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Go to rated items tab
		customersTests.GoToRatedItemsTab(subCode);
		waitPageLoaded();

		//Check items
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("BILLED"), 12);
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("Canceled"), 3);
	}

}
