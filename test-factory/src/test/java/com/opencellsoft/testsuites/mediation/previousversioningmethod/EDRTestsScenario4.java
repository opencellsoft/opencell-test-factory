package com.opencellsoft.testsuites.mediation.previousversioningmethod;

import java.awt.AWTException;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.testsuites.mediation.EDRTestsCommons;
import com.opencellsoft.testsuites.mediation.EDRTestsDataSet;
import com.opencellsoft.utility.Constant;

public class EDRTestsScenario4 extends EDRTestsCommons {
	/********************************************************************************************
	 **********    Scenario 4  : Triggered EDR with Triggered EDR next charge     ***************

	 1 - Create a New Offer
	 2 - Create a new product
	 3 - Create charge 01 type Usage (Param2 = AA)
	       - Create and Publish a new price version for the charge
	       - Activate the usage charge and go back to the product
	 4 - Create charge 02 type Usage (Param2 = BB)
	       - Create and Publish a new price version for the charge
	       - Activate the usage charge and go back to the product
	 5 - Create charge Deux type Usage (Param1 = Deux, Param2 = AA)
	       - Create and Publish a new price version for the charge
	       - Activate the usage charge and go back to the product
	 6 - Create charge Trois type Usage (Param1 = Trois, Param2 = AA)
	       - Create and Publish a new price version for the charge
	       - Activate the usage charge and go back to the product
	 7 - publish the product version
	 8 - Activate the product and go back to the offer
	 9 - Activate offer
	10 - Create Triggered EDR (Admin)
	11 - Add Triggered EDR to charge 01, check "Trigger next charge" checkbox (Admin)
	12 - Create And Activate a new Subscription
	13 - Insert Charge Cdr 1st Version (param1 = DEUX)
	14 - Check Wallet operations (3 OPEN, 1 charge_001, 1 Charge_002, 1 Deux, 2 AA, 1 BB )
	15 - Insert Charge Cdr 2nd Version (param1 = Trois)
	16 - Check Wallet operations (1 CANCELLED, 2 TO RERATE)
	17 - Run Rerate_Job
	18 - Check Wallet operations (2 OPEN, 1 CANCELLED, 2 RERATED )
	19 - Run U_Job
	20 - Check Wallet operations (3 OPEN, 1 CANCELLED, 2 RERATED )
	21 - Run RT_Job
	22 - Check Rated Items On Costumer Page (3 OPEN items, 2 AA, 1 BB)
	 *********************************************************************************************/

	EDRTestsDataSet dataset = new EDRTestsDataSet();
	String customerCode = dataset.customerCode;
	String triggerEdrCode = dataset.triggerEdrCode;

	String offerCode = "EDROF4-" + Constant.offerCode;
	String productCode = "EDRPR4-" +Constant.productCode + "_001";
	String usageChargeCode_Charge1 = "Charge1_" + Constant.usageChargeCode;
	String usageChargeCode_Charge2 = "Charge2_" + Constant.usageChargeCode;
	String usageChargeCode_Deux = "Deux_" + Constant.usageChargeCode;
	String usageChargeCode_Trois = "Trois_" + Constant.usageChargeCode;

	String subCode = "EDRSUB04_" + Constant.subscriptionCode;
	String AP = "EDRAP04" + Constant.accessPointCode;

	String param3 = Constant.param3 + "_04";
	String param3EL = param3 + "_TRDEDR";
	boolean TriggerNextCharge = true;
	String offerDescription = "EDROF4-" + Constant.offerDescription;
	String productDescription = Constant.productDescription + "_001";
	String chargeDescriptionUsage_Charge1 = "Charge1:" + Constant.chargeDescriptionUsage;
	String pricePlanVersionCode_Charge1 = "Charge1_" + Constant.pricePlanVersionCode;
	String chargeDescriptionUsage_Charge2= "Charge2:" + Constant.chargeDescriptionUsage;
	String pricePlanVersionCode_Charge2 = "Charge2_" + Constant.pricePlanVersionCode;
	String chargeDescriptionUsage_Deux = "Deux:" + Constant.chargeDescriptionUsage;
	String pricePlanVersionCode_Deux = "Deux_" + Constant.pricePlanVersionCode;
	String chargeDescriptionUsage_Trois = "Trois:" + Constant.chargeDescriptionUsage;
	String pricePlanVersionCode_Trois = "Trois_" + Constant.pricePlanVersionCode;

	@Test(priority = 1, enabled = true)
	public void login() throws Exception {
		System.out.println("*****************************  EDR Versionning Scenario 4  : Triggered EDR with Triggered EDR next charge  ****************************");
		System.out.println("String customerCode = \"" + customerCode + "\";");
		System.out.println("String offerCode = \"" + offerCode + "\";");
		System.out.println("String subCode = \"" + subCode + "\";");
		System.out.println("String triggerEdrCode = \"" + triggerEdrCode + "\";");
		System.out.println("String usageChargeCode_Charge1 = \"" + usageChargeCode_Charge1 + "\";");
		System.out.println("String usageChargeCode_Charge2 = \"" + usageChargeCode_Charge2 + "\";");
		System.out.println("String usageChargeCode_Deux = \"" + usageChargeCode_Deux + "\";");
		System.out.println("String usageChargeCode_Trois = \"" + usageChargeCode_Trois + "\";");
		System.out.println("String AP = \"" + AP + "\";");
	}

	/*** Create a New Offer ***/
	@Test(priority = 2, enabled = true)
	public void createOffer() {
		offersTests.createOfferMethod(offerCode, offerDescription);
	}

	/*** Create a new product ***/
	@Parameters({ "PriceVersionDate"})
	@Test(priority = 3, enabled = true)
	public void createProduct(String PriceVersionDate) throws InterruptedException, AWTException {
		productsTests.createProductMethod(PriceVersionDate,productCode,productDescription );
	}

	/*** add new usage charge "Charge1" to the product  
	 * @throws InterruptedException ***/
	@Parameters({"param2"})
	@Test(priority = 4, enabled = true)
	public void addNewUsageCharge01(String param2) throws InterruptedException{
		chargesTests.createUsageCharge(usageChargeCode_Charge1, chargeDescriptionUsage_Charge1,"",param2, version);
		Thread.sleep(2000);
		chargesTests.createNewPriceVersion(pricePlanVersionCode_Charge1, "10");
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(chargeDescriptionUsage_Charge1, version);
	}

	/*** add new usage charge "Charge2" to the product 
	 * @throws InterruptedException ***/
	@Parameters({"param2EL"})
	@Test(priority = 5, enabled = true)
	public void addNewUsageCharge02(String param2EL) throws InterruptedException{
		chargesTests.createUsageCharge(usageChargeCode_Charge2, chargeDescriptionUsage_Charge2,"",param2EL, version);
		Thread.sleep(2000);
		chargesTests.createNewPriceVersion(pricePlanVersionCode_Charge2,"15");
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(chargeDescriptionUsage_Charge2, version);
	}

	/*** add new usage charge "Deux" to the product  
	 * @throws InterruptedException ***/
	@Parameters({"param2"})
	@Test(priority = 6, enabled = true)
	public void addNewUsageChargeDeux(String param2) throws InterruptedException{
		chargesTests.createUsageCharge(usageChargeCode_Deux, chargeDescriptionUsage_Deux,usageChargeCode_Deux,param2, version);
		Thread.sleep(2000);
		chargesTests.createNewPriceVersion(pricePlanVersionCode_Deux,"20");
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(chargeDescriptionUsage_Deux, version);
	}

	/*** add new usage charge "Trois" to the product  
	 * @throws InterruptedException ***/
	@Parameters({"param2"})
	@Test(priority = 7, enabled = true)
	public void addNewUsageChargeTrois(String param2) throws InterruptedException{
		chargesTests.createUsageCharge(usageChargeCode_Trois, chargeDescriptionUsage_Trois,usageChargeCode_Trois,param2, version);
		Thread.sleep(2000);
		chargesTests.createNewPriceVersion(pricePlanVersionCode_Trois,"30");
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(chargeDescriptionUsage_Trois, version);
	}

	/*** publish the product version  ***/
	@Test(priority = 8, enabled = true)
	public void publishProductVersion() {	
		productsTests.publishProductVersion();
	}

	/*** Activate the product and go back to the offer  ***/
	@Test(priority = 9, enabled = true)
	public void activateProduct(){
		productsTests.activateProductAndGoBack();
	}

	/*** Activate offer  ***/
	@Parameters({ "defaultQuantity", "minimalQuantity", "maximalQuantity", "sequence" })
	@Test(priority = 10, enabled = true)
	public void activateOffer(String defaultQuantity, String minimalQuantity, String maximalQuantity, String sequence) throws InterruptedException{
		homeTests.goToOffersPage();
		offersTests.searchForOfferMethod(offerCode, offerDescription);
		offersTests.pickProductToOffer(productCode,productDescription, version);
		offersTests.fillProductParametersMethod(defaultQuantity,minimalQuantity, maximalQuantity, sequence, false );
		offersTests.ActivateOfferMethod();
		waitPageLoaded();
	}

	/*** Add triggered EDR to charge  ***/
	@Parameters({"baseURI"})
	@Test(priority = 12, enabled = true)
	public void addTriggeredEDRToCharge(String baseURI) {
		triggeredEDRTests.openNewTab(baseURI);
		triggeredEDRTests.addTriggeredEDRToCharge(usageChargeCode_Charge1,triggerEdrCode, TriggerNextCharge);
		triggeredEDRTests.closeTab();
	}

	/*** Create and Activate a new Subscription  ***/
	@Parameters({ "seller"})
	@Test(priority = 13)
	public void createAndActivateSubscription(String seller) throws AWTException, InterruptedException{
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Create a new subscription from subscription tab in customer details page
		try {
			subscriptionsTests.create_subscription_FromCustomerPage(seller, subCode, offerCode);
		}catch (Exception e) {
			customersTests.searchForCustomerMethod(customerCode);
			subscriptionsTests.create_subscription_FromCustomerPage(seller, subCode, offerCode);
		}

		// Create Access point
		subscriptionsTests.createAccessPointMethod(AP);

		// Activate the product instance for the subscription
		subscriptionsTests.activateProductInstanceMethod();
	}

	/*** Insert Charge Cdr 1st Version (param1 = DEUX)   ***/
	@Parameters({ "baseURI", "login", "password","param2" })
	@Test(priority = 14)
	public void insertChargeCdrFirstVersion(String baseURI, String login, String password, String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"4", usageChargeCode_Deux, param2, param3);
		Assert.assertEquals(result, 1);
	}

	/*** Check Wallet operations (3 OPEN, 1 charge_001, 1 Charge_002, 1 Deux, 2 AA, 1 BB )  ***/
	@Parameters({ "param2", "param2EL"})
	@Test(priority = 15)
	public void checkwalletOperations1(String param2, String param2EL) {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		/*** check result ***/
		// Verify the number of lines found (2 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 3);

		// Verify Status size (1 OPEN)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("OPEN"), 3);

		// Verify Status size (1 charge1)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(usageChargeCode_Charge1), 1);

		// Verify Status size (1 charge2)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(usageChargeCode_Charge2), 1);

		// Verify Status size (1 DEUX)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(usageChargeCode_Deux), 3);

		// Verify the number of lines found (2 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(param2), 2);

		// Verify the number of lines found (1 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(param2EL), 1);
	}

	/*** Insert Charge Cdr 2nd Version (param1 = Trois)  ***/
	@Parameters({ "baseURI", "login", "password","param2" })
	@Test(priority = 16)
	public void insertChargeCdrSecondVersion(String baseURI, String login, String password, String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"4", usageChargeCode_Trois, param2, param3);
		Assert.assertEquals(result, 1);
	}

	/*** Check Wallet operations (1 CANCELLED, 2 TO RERATE)  ***/
	@Parameters({ "param2", "param2EL" })
	@Test(priority = 17)
	public void checkwalletOperations2(String param2, String param2EL) {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		/*** check result ***/
		// Verify the number of lines found (2 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 3);

		// Verify Status size (1 TO RERATE)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("TO RERATE"), 2);

		// Verify Status size (1 CANCELED)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 1);
	}

	/*** Run Rerate Job (Rerate_Job)  ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 18)
	public void runRerateJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runRerateJob(baseURI, login, password);
	}

	/*** Check Wallet operations (2 OPEN, 1 CANCELLED, 2 RERATED )  ***/
	@Parameters({ "param2", "param2EL"})
	@Test(priority = 19)
	public void checkwalletOperations3(String param2, String param2EL) {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		/*** check result ***/
		// Verify the number of lines found (5 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 5);

		// Verify Status size (2 OPEN)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("OPEN"), 2);

		// Verify Status size (1 CANCELED)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 1);

		// Verify Status size (2 RERATED)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("RERATED"), 2);
	}	

	/*** Run Usage Job (U_Job)  ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 20)
	public void runUJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runUJob(baseURI, login, password);
	}

	/*** Check Wallet operations (3 OPEN, 1 CANCELLED, 2 RERATED )  ***/
	@Test(priority = 21)
	public void checkwalletOperations4() {
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		/*** check result ***/
		// Verify the number of lines found (5 lines)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 6);

		// Verify Status size (2 OPEN)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("OPEN"), 3);

		// Verify Status size (1 CANCELED)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 1);

		// Verify Status size (2 RERATED)
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("RERATED"), 2);
	}	

	/*** Run Rated Transaction Job (RT_Job)  ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 22)
	public void runRTJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}

	/*** Check Rated Items On Costumer Page (3 OPEN items, 2 AA, 1 BB)  ***/
	@Parameters({ "param2", "param2EL"})
	@Test(priority = 23, enabled = false)
	public void checkRatedItemsOnConstumerPage(String param2, String param2EL) {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Go to rated items tab
		customersTests.GoToRatedItemsTab(subCode);
		waitPageLoaded();

		//Check 2 open items
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("OPEN"), 3);

		//Check parameter 2
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param2), 2);

		//Check parameter 2 EL
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param2EL), 1);

		// Check parameter 3
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param3), 2);

		//Check parameter 2 EL
		Assert.assertEquals(ratedItemsPage.elementsFoundSize(param3EL), 1);
	}
}
