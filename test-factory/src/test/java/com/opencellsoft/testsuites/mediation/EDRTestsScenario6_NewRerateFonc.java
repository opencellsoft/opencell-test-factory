package com.opencellsoft.testsuites.mediation;

import java.awt.AWTException;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class EDRTestsScenario6_NewRerateFonc extends EDRTestsCommons  {

	EDRTestsDataSet dataSet = new EDRTestsDataSet();
	//	public String offerCode = "EDROFF6-" + Constant.offerCode;
	//	public String productCode = "EDRPROD6-" +Constant.offerCode;
	//	public String usageChargeCode1 = "EDRChargeSTD-" + Constant.offerCode;
	//	public String usageChargeCode2 = "EDRChargeTRG-" + Constant.offerCode;
	//	public String triggerEdrCode = "EDRTRG6-" + Constant.offerCode;	
	//	public String customerCode = "EDRCUST6-" + Constant.offerCode;	

	public String offerCode = "EDROFF6-001";
	public String productCode = "EDRPROD6-001";
	public String usageChargeCode1 = "EDRChargeSTD-001";
	public String usageChargeCode2 = "EDRChargeTRG-001";
	public String triggerEdrCode = "EDRTRG6-001";	
	public String customerCode = "EDRCUST6-001";

	String offerDescription = "EDROFF6" + Constant.offerDescription;
	String subCode1 = "EDRSUB061_" + Constant.subscriptionCode;
	String subCode2 = "EDRSUB062_" + Constant.subscriptionCode;
	String AP1 = "EDRAP61" + Constant.accessPointCode;
	String AP2 = "EDRAP62" + Constant.accessPointCode;
	String productDescription = Constant.productDescription ;
	String pricePlanVersionCode = "EDRPV2-" + Constant.pricePlanVersionCode;
	String customerName = Constant.customerName;
	String customerEmail = Constant.customerEmail;
	String paypalUserId = Constant.paypalUserId;
	String consumerCode = "EDRCONS6-" + Constant.offerCode;
	String consumerName = Constant.consumerName;
	String companyRegistrationNumber = Constant.companyRegistrationNumber;
	String vatNumber = Constant.vatNumber;
	String contractCode = "EDRC6_" + Constant.contractCode;
	String contractDescription = "EDRC6_" + Constant.contractDescription ;
	String contractlineCode = "EDRC6CL1_" + Constant.contractLineCode;
	String discount = "10";
	String param31 = Constant.param3 + "_061";
	String param32 = Constant.param3 + "_062";
	String param3EL = Constant.param3EL + "_06";
	String unitPrice2 = "150";
	String conditionEL = "#{edr.parameter2.equals('AA') && mv:executeScript(op,'org.meveo.service.script.EligibilityUtils','')}";
	String companyRegistration = getRandomNumberbetween1And200();

	@Test(priority = 2, enabled = true)
	public void _EDRV_T06(){
		System.out.println("**************************      EDR Versionning Scenario 6 : EDR versionning - triggered EDR - FA - separation discount WO - billing rule - EligibilityUtils script   ******************************");
		System.out.println("public String offerCode =\"" + offerCode + "\";");
		System.out.println("public String productCode = \"" + productCode + "\";");
		System.out.println("public String usageChargeCode1 = \"" + usageChargeCode1 + "\";");
		System.out.println("public String usageChargeCode2 = \"" + usageChargeCode2 + "\";");
		System.out.println("public String triggerEdrCode = \"" + triggerEdrCode + "\";");
	}

	/*** Create a New Offer  ***/
	@Parameters({ "PriceVersionDate", "UnitPrice", "defaultQuantity", "minimalQuantity", "maximalQuantity", "sequence", "baseURI","ConditionEL", "QuantityEL", "param2EL"})
	@Test(priority = 4, enabled = true)
	public void _EDRV_T06_createOffer(String PriceVersionDate, String UnitPrice,String defaultQuantity, String minimalQuantity, String maximalQuantity,
			String sequence, String baseURI, String ConditionEL, String QuantityEL , String param2EL) throws InterruptedException, AWTException {
		offersTests.createOfferMethod(offerCode, offerDescription);
		productsTests.createProductMethod(PriceVersionDate,productCode,productDescription );
		createUsageChargeSTD(UnitPrice);
		createUsageChargeTRG();
		publishProductVersion();
		productsTests.activateProductAndGoBack();
		pickProductToOfferAndfillProductParameters(defaultQuantity, minimalQuantity, maximalQuantity, sequence);
		offersTests.ActivateOfferMethod();
		createTriggeredEDRAndAddTriggeredEDRToCharge(baseURI, ConditionEL, QuantityEL , param2EL );
	}

	/***   Create a new customer    ***/
	@Test(priority = 24, enabled = true)
	public void _EDRV_T06_createCustomerAndSubscription() throws InterruptedException {
		payloadTests.createCustomer(baseURI, login, password, customerCode, customerName , dataSet.bcCode);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode1, customerCode, this.offerCode, this.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode1, this.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP1, subCode1);
		Thread.sleep(1000);
	}

	/***   Create new Framework Agreement   ***/
	@Parameters({ "level",  "CLTypeGlobalDiscount"})
	@Test(priority = 26, enabled = true)
	public void _EDRV_T06_createFrameworkAgreement( String level, String CLTypeGlobalDiscount) {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();
		frameworkAgreementsTests.createNewframeworkAgreementInPresent(contractCode, contractDescription, level,customerCode);
		frameworkAgreementsTests.AddContractLineGlobalDiscount(contractlineCode, contractlineCode, offerCode, productCode, usageChargeCode1,CLTypeGlobalDiscount, discount, true );
		frameworkAgreementsTests.activateContract();
	}

	/***   Insert EDRs, first version   ***/ 
	@Parameters({ "baseURI", "login", "password","param2" })
	@Test(priority = 38)
	public void _EDRV_T06_insertChargeCdrFirstVersion(String baseURI, String login, String password, String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP1,"4", "", param2, param31);
		Assert.assertEquals(result, 1);
	}

	/***   Insert EDRs, second version   ***/ 
	@Parameters({"param2" })
	@Test(priority = 42)
	public void _EDRV_T06_insertChargeCdrSecondVersion(String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP1,"4", "", param2, param31);
		Assert.assertEquals(result, 1);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 44)
	public void _EDRV_T06_checkwalletOperations2() {

		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode1);
		waitPageLoaded();

		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode1), 8);

		Assert.assertEquals(walletOperationsTests.elementsFoundSize("OPEN"), 4);

		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 4);
	}

	/***   Run Rated Transaction Job (RT_Job)   ***/
	@Test(priority = 50)
	public void _EDRV_T06_runRTJob() throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   Check Rated items on costumer page (2 items open)   ***/
	@Parameters({ "param2", "param2EL" })
	@Test(priority = 52)
	public void _EDRV_T06_checkRatedItemsOnCustomerPage(String param2, String param2EL) {
		//Search a customer
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Go to rated items tab
		customersTests.GoToRatedItemsTab(subCode1);
		waitPageLoaded();
		waitPageLoaded();

		//Check 2 open items
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("OPEN"), 4);
	}	

	/***  Create new rating script  ***/
	@Test(priority = 58, enabled = true)
	public void _EDRV_T06_createEligibilityUtilsScript() {
		// Open new tab and go to admin
		triggeredEDRTests.openNewTab(baseURI);
		// Go to Scripts list page
		scriptTests.goToScriptListPage();
		// create new script
		scriptTests.addEligibilityUtilsScript();
	}

	@Test(priority = 62)
	public void _EDRV_T06_updateTriggeredEDRConditionalEL() throws AWTException, InterruptedException{
		triggeredEDRTests.updateConditionELInTriggeredEDR( triggerEdrCode, conditionEL);
		triggeredEDRTests.closeTab();
	}

	/*** Create and Activate a new Subscription  ***/
	@Parameters({ "seller"})
	@Test(priority = 66)
	public void _EDRV_T06_createAndActivateSubscription2(String seller) throws AWTException, InterruptedException{
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode2, customerCode, this.offerCode, this.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode2, this.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP2, subCode2);
		Thread.sleep(1000);
	}

	/***   Insert EDRs, first version   ***/ 
	@Parameters({"param2" })
	@Test(priority = 68)
	public void _EDRV_T06_insertChargeCdrFirstVersion2(String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP2,"4", "", param2, param32);
		Assert.assertEquals(result, 1);
	}

	/***   Insert EDRs, second version   ***/ 
	@Parameters({"param2" })
	@Test(priority = 72)
	public void _EDRV_T06_insertChargeCdrSecondVersion2( String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP2,"4", "", param2, param32);
		Assert.assertEquals(result, 1);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 74)
	public void _EDRV_T06_checkwalletOperations5() {

		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode2);
		waitPageLoaded();

		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode2), 6);

		Assert.assertEquals(walletOperationsTests.elementsFoundSize("OPEN"), 3);

		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 3);
	}

	/***   Run Rated Transaction Job (RT_Job)   ***/
	@Test(priority = 80)
	public void _EDRV_T06_runRTJob2() throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   Check Rated items on costumer page (2 items open)   ***/
	@Parameters({ "param2", "param2EL" })
	@Test(priority = 82)
	public void _EDRV_T06_checkRatedItemsOnCustomerPage2(String param2, String param2EL) {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Go to rated items tab
		customersTests.GoToRatedItemsTab(subCode2);
		waitPageLoaded();

		//Check 2 open items
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("OPEN"), 3);
	}

	//  Create a new charge type Usage from product page   
	public void createUsageChargeSTD(String UnitPrice) throws InterruptedException {	
		chargesTests.createUsageCharge(usageChargeCode1,usageChargeCode1,"","AA", version);
		chargesTests.createNewPriceVersionPriceGrid(pricePlanVersionCode);
		chargesTests.addUnitPriceToPriceVersion(UnitPrice);
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		try {
			chargesTests.pickChargeToProduct(usageChargeCode1, version);
		}catch (Exception e) {
			chargesTests.goBack();
			chargesTests.pickChargeToProduct(usageChargeCode1, version);
		}
	}

	//  Create a new charge type Usage from product page   
	public void createUsageChargeTRG() throws InterruptedException {	
		chargesTests.createUsageCharge(usageChargeCode2,usageChargeCode2,"","BB", version);
		chargesTests.createNewPriceVersion(pricePlanVersionCode, unitPrice2);
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		try {
			chargesTests.pickChargeToProduct(usageChargeCode2, version);
		}catch (Exception e) {
			chargesTests.goBack();
			chargesTests.pickChargeToProduct(usageChargeCode2, version);
		}
	}

	/***   publish the product version   ***/
	public void publishProductVersion() {	
		try {
			productsTests.publishProductVersion();
		}
		catch (Exception e) {
			chargesTests.goBack();
			productsTests.publishProductVersion();
		}
	}

	/***   fill Product Parameters   ***/
	public void pickProductToOfferAndfillProductParameters(String defaultQuantity, String minimalQuantity, String maximalQuantity,
			String sequence) {
		offersTests.pickProductToOffer( productCode,productDescription, version);
		offersTests.fillProductParametersMethod(defaultQuantity,minimalQuantity, maximalQuantity, sequence, false );
	}

	/***   Create triggered EDR in admin   ***/
	public void createTriggeredEDRAndAddTriggeredEDRToCharge(String baseURI, String ConditionEL, String QuantityEL , String param2EL ) {
		triggeredEDRTests.openNewTab(baseURI);
		triggeredEDRTests.createTriggeredEDR(baseURI, ConditionEL, QuantityEL, param2EL, param3EL, triggerEdrCode, Constant.triggerEdrDescription);
		triggeredEDRTests.addTriggeredEDRToCharge(usageChargeCode1, triggerEdrCode, false);
		triggeredEDRTests.closeTab();
	}	

}
