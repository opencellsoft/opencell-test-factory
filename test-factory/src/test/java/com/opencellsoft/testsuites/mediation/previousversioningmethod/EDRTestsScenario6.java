package com.opencellsoft.testsuites.mediation.previousversioningmethod;

import java.awt.AWTException;
import java.util.ArrayList;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.tests.admin.TriggeredEDRTests;
import com.opencellsoft.testsuites.mediation.EDRTestsCommons;
import com.opencellsoft.testsuites.mediation.EDRTestsDataSet;
import com.opencellsoft.utility.Constant;

public class EDRTestsScenario6 extends EDRTestsCommons  {

	String offerCode = "EDROFF6-" + Constant.offerCode;
	String productCode = "EDRPROD6-" +Constant.productCode;
	String usageChargeCode1 = "EDRChargeSTD-" + Constant.usageChargeCode;
	String usageChargeCode2 = "EDRChargeTRG-" + Constant.usageChargeCode;
	String customerCode2 = "EDRCUST2-" + Constant.customerCode;

	EDRTestsDataSet dataSet = new EDRTestsDataSet();
	String customerCode1 = dataSet.customerCode;
	String triggerEdrCode = dataSet.triggerEdrCode;

	String offerDescription = "EDROFF6" + Constant.offerDescription;
	String subCode1 = "EDRSUB061_" + Constant.subscriptionCode;
	String subCode2 = "EDRSUB062_" + Constant.subscriptionCode;
	String AP = "EDRAP61" + Constant.accessPointCode;
	String AP2 = "EDRAP62" + Constant.accessPointCode;
	String productDescription = Constant.productDescription ;
	String pricePlanVersionCode = "EDRPV2-" + Constant.pricePlanVersionCode;
	String customerName = Constant.customerName;
	String customerEmail = Constant.customerEmail;
	String paypalUserId = Constant.paypalUserId;
	String consumerCode1 = "EDRCONS61-" + Constant.consumerCode;
	String consumerCode2 = "EDRCONS62-" + Constant.consumerCode;
	String consumerName = Constant.consumerName;
	String companyRegistrationNumber = Constant.companyRegistrationNumber;
	String vatNumber = Constant.vatNumber;
	String contractCode = "EDRC6_" + Constant.contractCode;
	String contractDescription = "EDRC6_" + Constant.contractDescription ;
	String contractlineCode = "EDRC6CL1_" + Constant.contractLineCode;
	String discount = "10";
	String param31 = Constant.param3 + "_061";
	String param32 = Constant.param3 + "_062";
	String param3EL = Constant.param3EL + "_06";
	String unitPrice2 = "150";
	String conditionEL = "#{edr.parameter2.equals('AA') && mv:executeScript(op,'org.meveo.service.script.EligibilityUtils','')}";
	String companyRegistration = getRandomNumberbetween1And200();


	@Test(priority = 2, enabled = true)
	public void login() throws Exception {
		System.out.println("**************************      EDR Versionning Scenario 6 : EDR versionning - triggered EDR - FA - separation discount WO - billing rule - EligibilityUtils script   ******************************");
		System.out.println("String offerCode =\"" + offerCode + "\";");
		System.out.println("String productCode = \"" + productCode + "\";");
		System.out.println("String usageChargeCode1 = \"" + usageChargeCode1 + "\";");
		System.out.println("String usageChargeCode2 = \"" + usageChargeCode2 + "\";");
		System.out.println("String customerCode2 = \"" + consumerCode2 + "\";");
	}

	/*** Create a New Offer ***/
	@Test(priority = 4, enabled = true)
	public void createOffer() {
		offersTests.createOfferMethod(offerCode, offerDescription);
	}

	/*** Create a new product ***/
	@Parameters({ "PriceVersionDate"})
	@Test(priority = 6, enabled = true)
	public void createProduct(String PriceVersionDate) throws InterruptedException, AWTException {
		productsTests.createProductMethod(PriceVersionDate,productCode,productDescription );
	}

	/***  Create a new charge type Usage from product page   ***/
	@Parameters({ "UnitPrice"})
	@Test(priority = 8, enabled = true)
	public void createUsageChargeSTD(String UnitPrice) throws InterruptedException {	
		chargesTests.createUsageCharge(usageChargeCode1,usageChargeCode1,"","AA",version);
		chargesTests.createNewPriceVersionPriceGrid(pricePlanVersionCode);
		chargesTests.addUnitPriceToPriceVersion(UnitPrice);
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		try {
			chargesTests.pickChargeToProduct(usageChargeCode1, version);
		}catch (Exception e) {
			chargesTests.goBack();
			chargesTests.pickChargeToProduct(usageChargeCode1, version);
		}
	}

	/***  Create a new charge type Usage from product page   
	 * @throws InterruptedException ***/
	@Test(priority = 10, enabled = true)
	public void createUsageChargeTRG() throws InterruptedException {	
		chargesTests.createUsageCharge(usageChargeCode2,usageChargeCode2,"","BB",version);
		chargesTests.createNewPriceVersion(pricePlanVersionCode, unitPrice2);
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		try {
			chargesTests.pickChargeToProduct(usageChargeCode2, version);
		}catch (Exception e) {
			chargesTests.goBack();
			chargesTests.pickChargeToProduct(usageChargeCode2, version);
		}
	}

	/***   publish the product version   ***/
	@Test(priority = 12, enabled = true)
	public void publishProductVersion() {	
		try {
			productsTests.publishProductVersion();
		}
		catch (Exception e) {
			chargesTests.goBack();
			productsTests.publishProductVersion();
		}
	}

	/***   Activate the product and go back to the offer   ***/
	@Test(priority = 14, enabled = true)
	public void activateProduct(){
		productsTests.activateProductAndGoBack();
	}

	/***   fill Product Parameters   ***/
	@Parameters({ "defaultQuantity", "minimalQuantity", "maximalQuantity", "sequence" })
	@Test(priority = 16, enabled = true)
	public void fillProductParameters(String defaultQuantity, String minimalQuantity, String maximalQuantity,
			String sequence) {
		offersTests.pickProductToOffer( productCode,productDescription, version);
		offersTests.fillProductParametersMethod(defaultQuantity,minimalQuantity, maximalQuantity, sequence, false );
	}


	/***   Activate offer   ***/
	@Test(priority = 18, enabled = true)
	public void activateOffer() throws InterruptedException{
		offersTests.ActivateOfferMethod();
		waitPageLoaded();
	}

	/*** Create and Activate a new Subscription  ***/
	@Parameters({ "seller"})
	@Test(priority = 22)
	public void createAndActivateSubscription1(String seller) throws AWTException, InterruptedException{
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode1);

		// Create a new subscription from subscription tab in customer details page
		subscriptionsTests.create_subscription_FromCustomerPage(seller, subCode1, offerCode);

		// Create Access point
		subscriptionsTests.createAccessPointMethod(AP);

		// Activate the product instance for the subscription
		subscriptionsTests.activateProductInstanceMethod();
	}

	/***   Create a new customer    ***/
	@Parameters({ "billingCountry", "billingCycle", "acrs" })
	@Test(priority = 24, enabled = false)
	public void createCustomer(String billingCountry, String billingCycle, String acrs) throws InterruptedException {
		homeTests.goToCustomTablesListPage();
		customersTests.createCustomerCheck(customerCode2, customerName, customerEmail, billingCountry,  billingCycle,companyRegistration, version);
		customersTests.createConsumerMethod(consumerCode2,consumerName, companyRegistrationNumber,vatNumber, acrs ,companyRegistration, version);
	}

	/***   Create new Framework Agreement   ***/
	@Parameters({ "level"})
	@Test(priority = 26, enabled = true)
	public void createFrameworkAgreement( String level) {
		// Go To Framework Agreement page
		homeTests.goToFrameworkAgreementPage();
		frameworkAgreementsTests.createNewframeworkAgreementInPresent(contractCode, contractDescription, level,customerCode1);
	}

	/***   Add contract line to Framework Agreement   
	 * @throws InterruptedException ***/
	@Parameters({ "CLTypeGlobalDiscount"})
	@Test(priority = 28, enabled = true)
	public void addContractLineUsageCharge(String CLTypeGlobalDiscount) {
		frameworkAgreementsTests.AddContractLineGlobalDiscount(contractlineCode, contractlineCode, offerCode, productCode, usageChargeCode1,CLTypeGlobalDiscount, discount, true );
	}

	/***   add billing rule (Cratiria EL = #{true} then redirecte RT to customer2)  ***/
	@Test(priority = 30, enabled = true)
	public void addBillingRule() {
		//		frameworkAgreementsTests.addBillingRule("#{true}", customerCode2);
	}

	/***   activate contract   ***/
	@Test(priority = 32, enabled = true)
	public void activateContract() {
		frameworkAgreementsTests.activateContract();
	}

	/***   Add triggered EDR to charge   ***/
	@Parameters({ "baseURI"})
	@Test(priority = 36, enabled = true)
	public void addTriggeredEDRToCharge(String baseURI) {
		triggeredEDRTests.openNewTab(baseURI);
		triggeredEDRTests.addTriggeredEDRToCharge(usageChargeCode1, triggerEdrCode, false);
		triggeredEDRTests.closeTab();
	}

	/***   Insert EDRs, first version   ***/ 
	@Parameters({ "baseURI", "login", "password","param2" })
	@Test(priority = 38)
	public void insertChargeCdrFirstVersion(String baseURI, String login, String password, String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"4", "", param2, param31);
		Assert.assertEquals(result, 1);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 40)
	public void checkwalletOperations() {

		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode1);
		waitPageLoaded();

		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode1), 4);

		Assert.assertEquals(walletOperationsTests.elementsFoundSize("OPEN"), 4);
	}

	/***   Insert EDRs, second version   ***/ 
	@Parameters({ "baseURI", "login", "password","param2" })
	@Test(priority = 42)
	public void insertChargeCdrSecondVersion(String baseURI, String login, String password, String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP,"4", "", param2, param31);
		Assert.assertEquals(result, 1);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 44)
	public void checkwalletOperations2() {

		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode1);
		waitPageLoaded();

		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode1), 4);

		Assert.assertEquals(walletOperationsTests.elementsFoundSize("TO RERATE"), 1);

		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 3);
	}

	/***   Run Rerate Job (Rerate_Job)   ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 46)
	public void runRerateJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runRerateJob(baseURI, login, password);
	}

	/***   Run Usage Job (U_Job)   ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 48)
	public void runUJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runUJob(baseURI, login, password);
		jobsTests.runUJob(baseURI, login, password);
	}

	/***   Run Rated Transaction Job (RT_Job)   ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 50)
	public void runRTJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   Check Rated items on costumer page (2 items open)   ***/
	@Parameters({ "param2", "param2EL" })
	@Test(priority = 52, enabled = false)
	public void checkRatedItemsOnConstumerPage(String param2, String param2EL) {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode1);

		// Go to rated items tab
		customersTests.GoToRatedItemsTab(subCode1);
		waitPageLoaded();

		//Check 2 open items
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("OPEN"), 4);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 54, enabled = false)
	public void checkwalletOperations3() {

		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode1);
		waitPageLoaded();

		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode1), 8);

		Assert.assertEquals(walletOperationsTests.elementsFoundSize("TREATED"), 4);

		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 4);
	}

	/***  Open admin in other window  ***/
	@Parameters({ "baseURI" })
	@Test(priority = 56, enabled = true)
	public void openAdmin(String baseURI) {
		// Open new Tab (for admin app)
		((JavascriptExecutor)driver).executeScript("window.open()");
		tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.get(baseURI);
		waitPageLoaded();
	}	

	/***  Create new rating script  ***/
	@Test(priority = 58, enabled = true)
	public void createEligibilityUtilsScript() {

		// Go to Scripts list page
		scriptTests.goToScriptListPage();

		// create new script
		scriptTests.addEligibilityUtilsScript();
	}

	@Parameters({"baseURI"})
	@Test(priority = 62)
	public void updateTriggeredEDR(String baseURI) throws AWTException, InterruptedException{
		TriggeredEDRTests triggeredEDRTests = new TriggeredEDRTests();
		triggeredEDRTests.updateConditionELInTriggeredEDR( triggerEdrCode, conditionEL);
	}

	/***  close admin  ***/
	@Test(priority = 64, enabled = true)
	public void closeAdmin() {

		tabs = new ArrayList<String>(driver.getWindowHandles());
		// clos eAdmin
		driver.close();
		// Go back to the portal tab
		driver.switchTo().window(tabs.get(0));
	}


	/*** Create and Activate a new Subscription  ***/
	@Parameters({ "seller"})
	@Test(priority = 66)
	public void createAndActivateSubscription2(String seller) throws AWTException, InterruptedException{
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode1);

		// Create a new subscription from subscription tab in customer details page
		subscriptionsTests.create_subscription_FromCustomerPage(seller, subCode2, offerCode);

		// Create Access point
		subscriptionsTests.createAccessPointMethod(AP2);

		// Activate the product instance for the subscription
		subscriptionsTests.activateProductInstanceMethod();
	}

	/***   Insert EDRs, first version   ***/ 
	@Parameters({ "baseURI", "login", "password","param2" })
	@Test(priority = 68)
	public void insertChargeCdrFirstVersion2(String baseURI, String login, String password, String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP2,"4", "", param2, param32);
		Assert.assertEquals(result, 1);
	}


	/***   Check Wallet operations   ***/
	@Test(priority = 70, enabled = true)
	public void checkwalletOperations4() {

		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode2);
		waitPageLoaded();

		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode2), 3);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize("OPEN"), 3);
	}

	/***   Insert EDRs, second version   ***/ 
	@Parameters({ "baseURI", "login", "password","param2" })
	@Test(priority = 72)
	public void insertChargeCdrSecondVersion2(String baseURI, String login, String password, String param2) throws InterruptedException {
		int result = insertChargeCdr(baseURI, login, password, AP2,"4", "", param2, param32);
		Assert.assertEquals(result, 1);
	}

	/***   Check Wallet operations   ***/
	@Test(priority = 74)
	public void checkwalletOperations5() {

		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code
		walletOperationsTests.searchBySubscriptionCode(subCode2);
		waitPageLoaded();

		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode2), 3);

		Assert.assertEquals(walletOperationsTests.elementsFoundSize("TO RERATE"), 1);

		Assert.assertEquals(walletOperationsTests.elementsFoundSize("CANCELED"), 2);
	}

	/***   Run Rerate Job (Rerate_Job)   ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 76)
	public void runRerateJob2(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runRerateJob(baseURI, login, password);
	}

	/***   Run Usage Job (U_Job)   ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 78)
	public void runUJob2(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runUJob(baseURI, login, password);
		jobsTests.runUJob(baseURI, login, password);
	}

	/***   Run Rated Transaction Job (RT_Job)   ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 80)
	public void runRTJob2(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   Check Rated items on costumer page (2 items open)   ***/
	@Parameters({ "param2", "param2EL" })
	@Test(priority = 82, enabled = false)
	public void checkRatedItemsOnConstumerPage2(String param2, String param2EL) {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode1);

		// Go to rated items tab
		customersTests.GoToRatedItemsTab(subCode2);
		waitPageLoaded();

		//Check 2 open items
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("OPEN"), 3);
	}
}
