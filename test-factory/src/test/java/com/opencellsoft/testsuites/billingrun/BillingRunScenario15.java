package com.opencellsoft.testsuites.billingrun;

import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class BillingRunScenario15  extends BillingRunCommons {
	BillingRunDataSet dataSet = new BillingRunDataSet();
	String bc        = "BRBC15-"   + Constant.offerCode;
	String custCode1 = "BRCUST15-" + Constant.offerCode + "-01"; 
	String subCode1  = "BRSUB15-"  + Constant.offerCode + "-01";
	String custCode2 = "BRCUST15-" + Constant.offerCode + "-02"; 
	String subCode2  = "BRSUB15-"  + Constant.offerCode + "-02";
	String subCode3  = "BRSUB15-"  + Constant.offerCode + "-03";
	
	@Test(priority = 0, enabled = true)
	public void _BR_T15_InvoiceValidation_ActionOnSuspectQuarantine(){
		System.out.println("***************** 15 Invoice validation : \r\n"
				+ "Fail mode = SUSPECT \r\n"
				+ "Cycle Run :  FULL AUTOMATIC \r\n"
				+ "Action on suspect  = Automatic validation   *****************");
	}

	/***   Configure invoice validation rule   ***/
	@Test(priority = 2, enabled = false)
	public void _BR_T15_configureInvoiceValidationRule() {
		// Go to invoice validation page
		homeTests.goToInvoiceValidationPage();
		// select com invoice 
		invoiceValidationTests.goToComInvoice();
		// set (rule : suspect), (scriptException : compareInvoiceAmmountScript ( amountWithoutTaw,>, 500) )
		invoiceValidationTests.setInvoiceValidationRule1("validationRule1","suspect", "500", version);
	}
	
	/***   Create data  ***/
	@Test(priority = 4, enabled = true)
	public void _BR_T15_createCustomersAndSubscriptions() throws InterruptedException {
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bc,bc,"BILLINGACCOUNT", "MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bc,bc,"BILLINGACCOUNT", "CAL_PERIOD_MONTHLY");

		payloadTests.createCustomer(baseURI, login, password, custCode1,custCode1 , bc);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode1, custCode1, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode1, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, subCode1, subCode1);
		Thread.sleep(1000);
		payloadTests.insertSeveralChargeCdr(baseURI, login, password, subCode1, "p1", "p2", "p3");

		
		payloadTests.createCustomer(baseURI, login, password, custCode2,custCode2 , bc);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode2, custCode2, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode2, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, subCode2, subCode2);
		Thread.sleep(1000);
		payloadTests.insertSeveralChargeCdr(baseURI, login, password, subCode2, "p1", "p2", "p3");

		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode3, custCode2, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode3, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, subCode3, subCode3);
		Thread.sleep(1000);
		payloadTests.insertSeveralChargeCdr(baseURI, login, password, subCode3, "p1", "p2", "p3");
		
		jobsTests.runRTJob(baseURI, login, password);
	}
	
	

	/***   Create Cycle run (Action en suspect = "Automatic validation)   ***/
	@Test(priority = 6, enabled = true)
	public void _BR_T15_createCycleRun() {
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.createCycleRun(bc,"FULL_AUTOMATIC","nextMonth",false,"Request manual action","Automatic validation",null,null,null);
	}

	/***   Process BR   ***/
	@Test(priority = 8, enabled = true)
	public void _BR_T15_processBR() {
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc,"NEW",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.processBillingRun();
	}

	/***   check BR status   ***/
	@Test(priority = 16, enabled = true)
	public void _BR_T15_checkBillingRunStatusValidated() {
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc,"VALIDATED",null,null);
		billingrunTests.goToBillingRunDetails();
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "VALIDATED");
	}

	/***   Check invoices generated  ***/
	@Test(priority = 18, enabled = true)
	public void _BR_T15_checkInvoicesAreValidated() {
		List<String> ExpectedAmountWOTaxValue = List.of("EUR 660.00","€660.00", "EUR 330.00","€330.00");
		String invoiceType;
		
		List<String> invoiceDetails1 = billingrunTests.getInvoiceGeneratedDetails(1, version);
		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails1.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertTrue(invoiceDetails1.get(5).equals("Validated")|| invoiceDetails1.get(5).equals("VALIDATED"));
			Assert.assertTrue(ExpectedAmountWOTaxValue.contains(invoiceDetails1.get(6)));
			Assert.assertTrue(invoiceDetails1.get(8).contains(""));
		}
		else {
			invoiceType = invoiceDetails1.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertTrue(invoiceDetails1.get(7).equals("Validated")|| invoiceDetails1.get(7).equals("VALIDATED"));
			Assert.assertTrue(ExpectedAmountWOTaxValue.contains(invoiceDetails1.get(8)));
			Assert.assertTrue(invoiceDetails1.get(9).contains(""));
		}

		List<String> invoiceDetails2 = billingrunTests.getInvoiceGeneratedDetails(2, version);
		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails2.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertTrue(invoiceDetails1.get(5).equals("Validated")|| invoiceDetails1.get(5).equals("VALIDATED"));
			Assert.assertTrue(ExpectedAmountWOTaxValue.contains(invoiceDetails1.get(6)));
			Assert.assertEquals(invoiceDetails2.get(8), "");
		}
		else {
			invoiceType = invoiceDetails2.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertTrue(invoiceDetails1.get(7).equals("Validated")|| invoiceDetails1.get(7).equals("VALIDATED"));
			Assert.assertTrue(ExpectedAmountWOTaxValue.contains(invoiceDetails1.get(8)));
			Assert.assertTrue(invoiceDetails2.get(9).contains(""));
		}
	}
}
