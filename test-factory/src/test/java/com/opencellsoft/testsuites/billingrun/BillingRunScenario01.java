package com.opencellsoft.testsuites.billingrun;

import java.awt.AWTException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.tests.InvoicesTests;
import com.opencellsoft.utility.Constant;

public class BillingRunScenario01 extends BillingRunCommons {
	BillingRunDataSet dataSet = new BillingRunDataSet();

	String BC_code        = "BRBC1-" + Constant.offerCode;
	String BC_description = "BRBC1_" + Constant.offerCode;
	String customerCode   = "BRCUST1-" + Constant.offerCode;
	String consumerCode   = "BRCUST1-" + Constant.offerCode;
	String customerName   = "BRCUST1-" + Constant.customerName;
	String subCode        = "BRSUB1_" + Constant.offerCode;
	String AP             = "BRAP1_" + Constant.offerCode;

	@Test(priority = 0, enabled = true)
	public void _BR_T01_CycleRun_Automatic() {
		System.out.println("***************** 1 - Cycle run : automatic  *****************");
	}

	/***   create Billing Cycle   ***/
	@Parameters()
	@Test(priority = 4, enabled = true)
	public void _BR_T01_createBillingCycle(){
		// Go to billing cycle page
		homeTests.goToBillingCyclePage();
		// Create a new billing cycle
		billingCycleTests.createBillingCycle(BC_code,BC_description,dataSet.BillingCalendar,dataSet.IncrementalInvoiceLines,dataSet.applicationEl);
		billingCycleTests.updateBillingCycleAggregationRules(dataSet.enableAggregation,dataSet.dateAggregation,dataSet.DiscountAggregation ,dataSet.UseAccountingArticleLabel,dataSet.AggregateUnitPrice, dataSet.IgnoreSubscriptions, dataSet.IgnoreOrders, dataSet.IgnoreConsumers, dataSet.BusinessKey, dataSet.Parameter1, dataSet.Parameter2, dataSet.Parameter3 );
	}
	
	/***   Create and Activate a new Subscription   ***/
	@Test(priority = 6, enabled = true)
	public void _BR_T01_createCustomerAndActivateSubscription() throws AWTException, InterruptedException{
		payloadTests.createCustomer(baseURI, login, password, customerCode, customerName, BC_code);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);
	}

	/***   Insert EDRs   ***/
	@Test(priority = 8, enabled = true)
	public void _BR_T01_insertChargeCdr() throws InterruptedException {
		int result;
		for(int i = 0; i<5; i++) {
			result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "",subCode,null);
			Assert.assertEquals(result, 1);
		}

		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   create Billing Run   ***/
	@Test(priority = 10, enabled = true)
	public void _BR_T01_createBillingRun(){
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.createCycleRun(BC_description,"AUTOMATIC","first of next month",false,"Quarantine","Quarantine",null,null,null);
	}

	/***   go To Billing Run Details   ***/
	@Test(priority = 12, enabled = true)
	public void _BR_T01_searchForBillingRun(){
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_description,"NEW",null,null);
		billingrunTests.goToBillingRunDetails();
	}

	/***   process Billing Run  ***/
	@Test(priority = 14, enabled = true)
	public void _BR_T01_processBillingRun(){
		billingrunTests.processBillingRun();
	}

	/***   Check Billing Run Status   ***/
	@Test(priority = 16, enabled = true)
	public void _BR_T01_checkBRStatus() throws InterruptedException{
		dataSet.checkBRStatus(BC_description,"DRAFT INVOICES");
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "DRAFT INVOICES");
	}

	/***   process Billing Run  ***/
	@Test(priority = 18, enabled = true)
	public void _BR_T01_processBillingRun2(){
		billingrunTests.processBillingRun();
	}

	/***   Check Billing Run Status   ***/
	@Test(priority = 20, enabled = true)
	public void _BR_T01_checkBRStatusValidated() throws InterruptedException{
		dataSet.checkBRStatus(BC_description,"VALIDATED");
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "VALIDATED");
	}

	/***   Check Invoice Generated  ***/
	@Test(priority = 22, enabled = true)
	public void _BR_T01_checkInvoiceGenerated(){
		List<String> invoiceDetails1 = billingrunTests.getInvoiceGeneratedDetails(1, version);
		String invoiceType;
		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails1.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(5), "VALIDATED");
			Assert.assertTrue(invoiceDetails1.get(6).equals("€80.00") || invoiceDetails1.get(6).equals("EUR 80.00") );
		}
		else {
			invoiceType = invoiceDetails1.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(7), "VALIDATED");
			Assert.assertTrue(invoiceDetails1.get(8).equals("€80.00") || invoiceDetails1.get(8).equals("EUR 80.00") );
		}
	}

	/***   Check Invoice Link  ***/
	@Test(priority = 24, enabled = true)
	public void _BR_T01_checkInvoiceLink(){
		String invoiceNumber = null;
		if(version.equals("14.1.X")) { invoiceNumber = billingrunTests.getInvoiceNumber();}
		else { invoiceNumber = billingrunTests.getInvoiceDetails("3");}
		billingrunTests.goToInvoicePage(2);
		InvoicesTests invoicesTests = new InvoicesTests();
		Assert.assertEquals(invoicesTests.getInvoiceNumber(), invoiceNumber); 
		if(!version.equalsIgnoreCase("17.X")) {
			Assert.assertTrue(invoicesTests.getInvoiceType().equals("Commercial invoice") || invoicesTests.getInvoiceType().equals( "Invoice") );	
		}
		if(version.equals("17.X")) {invoicesTests.navigateBack();}else {invoicesTests.goBack();}
	}

	/***   Check customer Link  ***/
	@Test(priority = 26, enabled = true)
	public void _BR_T01_checkCustomerLink(){
		String index = "4";
		if(version.equals("14.1.X")) {index = "3" ;}
		billingrunTests.goToCustomerPage(index);
		Assert.assertEquals(customersTests.getCustomerCode(), customerCode); 
		customersTests.goBack();
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "VALIDATED");
	}

	/***   check Billing Run List  ***/

	@Test(priority = 28, enabled = true)
	public void _BR_T01_checkBillingRunList(){
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_description,"VALIDATED",null,null);
		if(version.equals("14.1.X")){
			billingrunTests.sortListBy("Id");
			// check billing cycle
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(1), BC_description); 
			// check run type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(2), "CYCLE");
			// check split level
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(3), "BILLING ACCOUNT");
			// check process type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(5), "AUTOMATIC");
			// check status
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(6), "VALIDATED");
			// check accounts
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(8), "1");
			// check invoices
			//		Assert.assertEquals(billingrunTests.getBillingRunDetails().get(9), "1");
			// check amount without tax
			Assert.assertTrue(billingrunTests.getBillingRunDetailsFromList().get(10).equals("€80.00") || billingrunTests.getBillingRunDetailsFromList().get(10).equals("EUR 80.00") );
		}
		else {
			// check billing cycle
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(1), BC_description); 
			// check run type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(2), "CYCLE");
			// check split level
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(3), "BILLING ACCOUNT");
			// check process type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(5), "AUTOMATIC");
			// check status
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(6), "VALIDATED");
			// check accounts
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(9), "1");
			// check invoices
			//			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(10), "1");
			// check amount without tax
			Assert.assertTrue(billingrunTests.getBillingRunDetailsFromList().get(11).equals("€80.00") || billingrunTests.getBillingRunDetailsFromList().get(11).equals("EUR 80.00") );
		}
	}

}
