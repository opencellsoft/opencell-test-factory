package com.opencellsoft.testsuites.billingrun;

import java.util.Arrays;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class BillingRunScenario10  extends BillingRunCommons {
	BillingRunDataSet dataSet = new BillingRunDataSet();
	
	String BC_code        = "BRBC10-"   + Constant.offerCode;
	String BC_description = "BRBC10_"   + Constant.offerCode;
	String customerCode   = "BRCUST10-" + Constant.offerCode;
	String subCode        = "BRSUB10-"  + Constant.offerCode;	
	
	String BillingCalendar = "MONTHLY";
	Boolean IncrementalInvoiceLines = false;
	String applicationEl = "";
	Boolean enableAggregation = true;
	String dateAggregation = "Aggregate by day";
	String DiscountAggregation = "";
	Boolean UseAccountingArticleLabel = false;
	Boolean AggregateUnitPrice = false;
	Boolean IgnoreSubscriptions = true;
	Boolean IgnoreOrders = true;
	Boolean IgnoreConsumers = null;
	Boolean BusinessKey = null;
	Boolean Parameter1 = null;
	Boolean Parameter2 = null;
	Boolean Parameter3 = null ;
	String splitLevel ="ORDER";
	String AP = "BRAP10-"+Constant.accessPointCode;
	String order1Number = null;
	String order2Number = null;
	String order3Number = null;
	String companyRegistration = getRandomNumberbetween1And200();


	@Test(priority = 0, enabled = true)
	public void _BR_T10_ManualInvoicingLAPOSTE() {
		System.out.println("***************** 10 - Cycle run: split by order + manual invoicing LAPOSTE  *****************");
	}
	
	/***   create Billing Cycle   ***/
	@Parameters()
	@Test(priority = 4, enabled = true)
	public void _BR_T10_createBillingCycleSplitByOrder(){
		// Go to billing cycle page
		homeTests.goToBillingCyclePage();
		// Create a new billing cycle
		billingCycleTests.createBillingCycle(BC_code,BC_description,BillingCalendar,IncrementalInvoiceLines,applicationEl);
		billingCycleTests.updateBillingCycleAggregationRules(enableAggregation,dateAggregation,DiscountAggregation ,UseAccountingArticleLabel,AggregateUnitPrice, IgnoreSubscriptions, IgnoreOrders, IgnoreConsumers, BusinessKey, Parameter1, Parameter2, Parameter3 );
		billingCycleTests.updateBillingCycleSplitRules(splitLevel);
	}
	
	/***   Create a new customer &  consumer  ***/
	@Parameters({ "billingCountry", "acrs" })
	@Test(priority = 6, enabled = true)
	public void _BR_T10_createCustomerAndSubscription(String billingCountry, String acrs) throws InterruptedException {
		payloadTests.createCustomer(baseURI, login, password, customerCode, customerCode , BC_code);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);

	}
	
	/***   Create and Activate a new Subscription   ***/
	@Test(priority = 10, enabled = true)
	public void _BR_T10_createAndActivateOrder1WithTwoOrderlines() {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();
		
		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);
		
		// Create a new order from order tab in customer details page
		ordersTests.createOrderFromCustomerPage();

		// Create order line
		ordersTests.createOrderLineOneShot(subCode, dataSet.productCode, "1", "1");

		ordersTests.save();
		
		ordersTests.goBack();
		
		// Create order line
		ordersTests.createOrderLineOneShot(subCode, dataSet.productCode, "1", "2");
		
		ordersTests.save();
		
		ordersTests.goBack();
		
		// Activate 
		order1Number = ordersTests.validateOrder1();
	}
	
	/***   check Order 1   ***/
	@Test(priority = 12, enabled = true)
	public void _BR_T10_checkOrder1() {
		List<String> orderDetails = ordersTests.getOrderDetails();
		Assert.assertEquals(orderDetails.get(1), "VALIDATED");
		if(orderDetails.get(2).equals("5.00%")) {
			Assert.assertTrue(orderDetails.get(3).equals("€3.00") || orderDetails.get(3).equals("EUR 3.00"));
			Assert.assertTrue(orderDetails.get(4).equals("€63.00") || orderDetails.get(4).equals("EUR 63.00"));
		}
		if(orderDetails.get(2).equals("20.00%")) {
			Assert.assertTrue(orderDetails.get(3).equals("€12.00") || orderDetails.get(3).equals("EUR 12.00"));
			Assert.assertTrue(orderDetails.get(4).equals("€72.00") || orderDetails.get(4).equals("EUR 72.00"));
		}
		
		if(!version.equals("14.1.X")) {
			List<String> orderLineDetails = ordersTests.getOrderLineDetails(1, version);
			Assert.assertEquals(orderLineDetails.get(0), dataSet.offerCode );
			Assert.assertEquals(orderLineDetails.get(1), subCode);
			Assert.assertEquals(orderLineDetails.get(2), customerCode);
//			Assert.assertEquals(orderLineDetails.get(3), productDescription);
			Assert.assertEquals(orderLineDetails.get(4), "1");
			
			orderLineDetails = ordersTests.getOrderLineDetails(2, version);
			Assert.assertEquals(orderLineDetails.get(0), dataSet.offerCode );
			Assert.assertEquals(orderLineDetails.get(1), subCode);
			Assert.assertEquals(orderLineDetails.get(2), customerCode);
//			Assert.assertEquals(orderLineDetails.get(3), productDescription);
			Assert.assertEquals(orderLineDetails.get(4), "2");
		}
		
	}
	
	/***   duplicate Order1   ***/
	@Test(priority = 14, enabled = true)
	public void _BR_T10_duplicateAndActivateOrder1() {
		// duplicate order
		ordersTests.duplicateOrder();
		// Activate 
		order2Number = ordersTests.validateOrder1();
	}
	
	/***   check Order 2   ***/
	@Test(priority = 16, enabled = true)
	public void _BR_T10_checkOrder2() {
		if(!version.equals("14.1.X")) {
			List<String> orderDetails = ordersTests.getOrderDetails();
			Assert.assertEquals(orderDetails.get(1), "VALIDATED");
			if(orderDetails.get(2).equals("5.00%")) {
				Assert.assertTrue(orderDetails.get(3).equals("€3.00") || orderDetails.get(3).equals("EUR 3.00"));
				Assert.assertTrue(orderDetails.get(4).equals("€63.00") || orderDetails.get(4).equals("EUR 63.00"));
			}
			if(orderDetails.get(2).equals("20.00%")) {
				Assert.assertTrue(orderDetails.get(3).equals("€12.00") || orderDetails.get(3).equals("EUR 12.00"));
				Assert.assertTrue(orderDetails.get(4).equals("€72.00") || orderDetails.get(4).equals("EUR 72.00"));
			}
			
			List<String> orderLineDetails1 = ordersTests.getOrderLineDetails(1, version);
			Assert.assertEquals(orderLineDetails1.get(0), dataSet.offerCode );
			Assert.assertEquals(orderLineDetails1.get(1), subCode);
			Assert.assertEquals(orderLineDetails1.get(2), customerCode);
//			Assert.assertEquals(orderLineDetails1.get(3), productDescription);
			Assert.assertEquals(orderLineDetails1.get(4), "1");
			
			List<String> orderLineDetails2 = ordersTests.getOrderLineDetails(2, version);
			Assert.assertEquals(orderLineDetails2.get(0), dataSet.offerCode );
			Assert.assertEquals(orderLineDetails2.get(1), subCode);
			Assert.assertEquals(orderLineDetails2.get(2), customerCode);
//			Assert.assertEquals(orderLineDetails2.get(3), productDescription);
			Assert.assertEquals(orderLineDetails2.get(4), "2");
		}
		
	}
	
	/***   duplicate Order2   ***/
	@Test(priority = 18, enabled = true)
	public void _BR_T10_duplicateOrder2() {
		// duplicate order
		ordersTests.duplicateOrder();
	}
	
	/***   delete One Line Of Order3   ***/
	@Test(priority = 20, enabled = true)
	public void _BR_T10_deleteOrderLine() {
		ordersTests.deleteOrderLine("2");
	}
	
	/***   change Quantity Of One Line   ***/
	@Test(priority = 21, enabled = true)
	public void _BR_T10_changeOrderLineQuantity() {
		ordersTests.goToOrderLineDetails("1");
		ordersTests.changeQuantityOfOrderLine( dataSet.productCode, "1", "3");
		ordersTests.goBack();
	}
	
	/***   change Quantity Of One Line   ***/
	@Test(priority = 22, enabled = true)
	public void _BR_T10_activateOrder3() {
		try {
			// Activate 
			order3Number = ordersTests.validateOrder1();
		}catch (Exception e) {
			ordersTests.goBack();
			order3Number = ordersTests.validateOrder1();
		}
		
	}
	
	/***   check Order 3   ***/
	@Test(priority = 23, enabled = true)
	public void _BR_T10_checkOrder3() {
		if(!version.equals("14.1.X")) {
			List<String> orderDetails = ordersTests.getOrderDetails();
			Assert.assertEquals(orderDetails.get(1), "VALIDATED");
			if(orderDetails.get(2).equals("5.00%")) {
				Assert.assertTrue(orderDetails.get(3).equals("€3.00") || orderDetails.get(3).equals("EUR 3.00"));
				Assert.assertTrue(orderDetails.get(4).equals("€63.00") || orderDetails.get(4).equals("EUR 63.00"));
			}
			if(orderDetails.get(2).equals("20.00%")) {
				Assert.assertTrue(orderDetails.get(3).equals("€12.00") || orderDetails.get(3).equals("EUR 12.00"));
				Assert.assertTrue(orderDetails.get(4).equals("€72.00") || orderDetails.get(4).equals("EUR 72.00"));
			}
			if(orderDetails.get(2).equals("0.00%")) {
				Assert.assertTrue(orderDetails.get(3).equals("€0.00") || orderDetails.get(3).equals("EUR 0.00"));
				Assert.assertTrue(orderDetails.get(4).equals("€60.00") || orderDetails.get(4).equals("EUR 60.00"));
			}
			System.out.println( "orderDetails.get(5) = " + orderDetails.get(5));
			Assert.assertTrue(orderDetails.get(5).equals("€60.00") || orderDetails.get(5).equals("EUR 60.00"));
			
			List<String> orderLineDetails = ordersTests.getOrderLineDetails(1, version);
			Assert.assertEquals(orderLineDetails.get(0), dataSet.offerCode );
			Assert.assertEquals(orderLineDetails.get(1), subCode);
			Assert.assertEquals(orderLineDetails.get(2), customerCode);
//			Assert.assertEquals(orderLineDetails.get(3), productDescription);
			Assert.assertEquals(orderLineDetails.get(4), "3");
		}
	}
	
	/***   Run Rated Transaction Job (RT_Job)   ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 24)
	public void _BR_T10_runRTJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}	
	
	/***   create Billing Run   ***/
	@Test(priority = 26, enabled = true)
	public void _BR_T10_createBillingRun(){
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.createCycleRun(BC_description,"AUTOMATIC",null,false,"Quarantine","Quarantine",null,true,null);
	}
	
	/***   process Billing Run   ***/

	@Test(priority = 28, enabled = true)
	public void _BR_T10_processBillingRun(){
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_description,"NEW",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.processBillingRun();
	}
	
	/***   Check Billing Run Status  
	 * @throws InterruptedException ***/
	@Test(priority = 30, enabled = true)
	public void _BR_T10_checkBRStatusDRAFTINVOICES() throws InterruptedException{
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_description,null,null,null);
		billingrunTests.goToBillingRunDetails();
		dataSet.checkBRStatus(BC_description,"DRAFT INVOICES");
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "DRAFT INVOICES");
	}

	@Test(priority = 32, enabled = true)
	public void _BR_T10_processBillingRun2(){
		billingrunTests.processBillingRun();
	}

	
	/***   Check Billing Run Status  ***/
	@Test(priority = 34, enabled = true)
	public void _BR_T10_checkBRStatusVALIDATED(){
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_description,null,null,null);
		billingrunTests.goToBillingRunDetails();
		String billingRunStatus = billingrunTests.getBillingRunStatus();

		for(int i = 0; i<5; i++) {
			billingrunTests.refreshPage();
			if(billingRunStatus.equals("VALIDATED")) {
				break;
			}
		}
		
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "VALIDATED");
	}
	
	/***   Check Invoice Generated  ***/
	@Test(priority = 36, enabled = true)
	public void _BR_T10_checkInvoicesGenerated(){
		 List<String> amountWOTax = List.of("€60.00","€90.00", "EUR 60.00","EUR 90.00");
		if(version.equals("14.1.X")) {
			checkinvoicedetails(1,2, 5, 6, amountWOTax);
			checkinvoicedetails(2,2, 5, 6, amountWOTax);
			checkinvoicedetails(3,2, 5, 6, amountWOTax);
		}else {
			checkinvoicedetails(1,4, 7, 8, amountWOTax);
			checkinvoicedetails(2,4, 7, 8, amountWOTax);
			checkinvoicedetails(3,4, 7, 8, amountWOTax);
		}
	}
	
	public void checkinvoicedetails(int line,int Type, int status,int amountWOTax, List<String> ExpectedAmountWOTaxValue) {
		List<String> invoiceDetails = billingrunTests.getInvoiceGeneratedDetails(line, version);
		String invoiceType;
		invoiceType = invoiceDetails.get(Type);
		Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
		Assert.assertEquals(invoiceDetails.get(status), "VALIDATED");
		Assert.assertTrue( ExpectedAmountWOTaxValue.contains(invoiceDetails.get(amountWOTax)));
	}
	/***   check Billing Repport  ***/
	@Parameters({"version"})
	@Test(priority = 38, enabled = true)
	public void _BR_T10_checkBillingRepport(String version){
		List<String> stringList = Arrays.asList("14.1.X", "15.0.X");
		if(!stringList.contains(version)) {
			billingrunTests.openRepport();
			try {
				billingrunTests.generateRepport();
			}catch (Exception e) {
				// TODO: handle exception
			}
			homeTests.goToBillingRunPage();
			billingrunTests.searchForBillingRun(null,BC_description,"VALIDATED",null,null);
			billingrunTests.goToBillingRunDetails();
			billingrunTests.openRepport();
			// check number of billing accounts
			Assert.assertEquals(billingrunTests.checkRepport("1","1","1"), "1");
			// check number of subscriptions
			Assert.assertEquals(billingrunTests.checkRepport("1","1","2"), "1");
			
			// check Number of rated items :  OneShot
			Assert.assertEquals(billingrunTests.checkRepport("2","1","2"), "5");
			// check Number of rated items :  Recurring
			Assert.assertEquals(billingrunTests.checkRepport("2","2","2"), "0");
			// check Number of rated items :  Usage
			Assert.assertEquals(billingrunTests.checkRepport("2","3","2"), "0");
		}
	}
	
	/***   check Billing Run List  ***/
	@Test(priority = 40, enabled = true)
	public void _BR_T10_checkBillingRunList(){

		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_description,"VALIDATED",null,null);
		List<String> billingRunDetails = billingrunTests.getBillingRunDetailsFromList();
		if(version.equals("14.1.X")){
			billingRunDetails = billingrunTests.getBillingRunDetailsFromList();
			// check billing cycle
			Assert.assertEquals(billingRunDetails.get(1), BC_description); 
			// check run type
			Assert.assertEquals(billingRunDetails.get(2), "CYCLE");
			// check split level
			Assert.assertEquals(billingRunDetails.get(3), "ORDER");
			// check process type
			Assert.assertEquals(billingRunDetails.get(5), "AUTOMATIC");
			// check status
			Assert.assertEquals(billingRunDetails.get(6), "VALIDATED");
			// check accounts
			Assert.assertEquals(billingRunDetails.get(8), "1");
			// check invoices
			Assert.assertEquals(billingRunDetails.get(9), "3");
			// check amount without tax
			Assert.assertTrue(billingRunDetails.get(10).equals("€180.00") || billingRunDetails.get(10).equals("EUR 180.00"));
		}
		else {
			// check billing cycle
			Assert.assertEquals(billingRunDetails.get(1), BC_description); 
			// check run type
			Assert.assertEquals(billingRunDetails.get(2), "CYCLE");
			// check split level
			Assert.assertEquals(billingRunDetails.get(3), "ORDER");
			// check process type
			Assert.assertEquals(billingRunDetails.get(5), "AUTOMATIC");
			// check status
			Assert.assertEquals(billingRunDetails.get(6), "VALIDATED");
			// check accounts
			Assert.assertEquals(billingRunDetails.get(9), "1");
			// check invoices
			Assert.assertEquals(billingRunDetails.get(10), "3");
			// check amount without tax
			Assert.assertTrue(billingRunDetails.get(11).equals("€180.00") || billingRunDetails.get(11).equals("EUR 180.00"));
		}
	}
	
	
}
