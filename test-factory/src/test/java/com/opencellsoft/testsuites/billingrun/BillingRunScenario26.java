package com.opencellsoft.testsuites.billingrun;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class BillingRunScenario26  extends BillingRunCommons {
	BillingRunDataSet dataSet = new BillingRunDataSet();
	
	@Test(priority = 0, enabled = true)
	public void _BR_T26_filterBySplitLevel() throws Exception {
		System.out.println("***************** 26 - Filter by split level   *****************");
	}
	
	/***   filterByBillingAccount   ***/
	@Test(priority = 4, enabled = true)
	public void _BR_T26_filterByBillingAccount() {
		try {
			homeTests.goToBillingRunPage();
		}catch (Exception e) {
			homeTests.goToPortal();
			homeTests.goToBillingRunPage();
		}
		// go to billing run list page
		
		billingrunTests.filterByBillingAccount();
	}
	
	/***   check pagination info   ***/
	@Test(priority = 6, enabled = true)
	public void _BR_T26_checkResult() {
		Assert.assertEquals(billingrunTests.elementSize("Order"), 0);
		Assert.assertEquals(billingrunTests.elementSize("Subscription"), 0);
		if(version.equals("14.1.X")) {
			Assert.assertEquals(billingrunTests.elementSize("Billing account"), 20);
		}else {
			Assert.assertEquals(billingrunTests.elementSize("Billing account"), 25);
		}
		
	}
	
	/***   filterBySubscription   ***/
	@Test(priority = 10, enabled = true)
	public void _BR_T26_filterBySubscription() {
		billingrunTests.filterBySubscription();
	}
	
	/***   checkResult2   ***/
	@Test(priority = 12, enabled = true)
	public void _BR_T26_checkResult2() {
		Assert.assertEquals(billingrunTests.elementSize("Order"), 0);
		Assert.assertEquals(billingrunTests.elementSize("Billing account"), 0);
		if(version.equals("14.1.X")) {
			Assert.assertEquals(billingrunTests.elementSize("Subscription"), 20);
		}else {
			Assert.assertEquals(billingrunTests.elementSize("Subscription"), 25);
		}
	}
	
	/***   filterByOrder   ***/
	@Test(priority = 14, enabled = true)
	public void _BR_T26_filterByOrder() {
		billingrunTests.filterByOrder();
	}
	
	/***   checkResult3   ***/
	@Test(priority = 16, enabled = true)
	public void _BR_T26_checkResult3() {
		Assert.assertEquals(billingrunTests.elementSize("Billing account"), 0);
		Assert.assertEquals(billingrunTests.elementSize("Subscription"), 0);
		if(version.equals("14.1.X")) {
			Assert.assertEquals(billingrunTests.elementSize("Order"), 20);
		}else {
			Assert.assertEquals(billingrunTests.elementSize("Order"), 25);
		}
	}

}
