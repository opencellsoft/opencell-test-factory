package com.opencellsoft.testsuites.billingrun;

import java.awt.AWTException;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.tests.InvoicesTests;
import com.opencellsoft.utility.Constant;

public class BillingRunScenario02  extends BillingRunCommons {
	BillingRunDataSet dataSet = new BillingRunDataSet();
	String BC_code                 = "BRBC2-" + Constant.offerCode;
	String customerCode            = "BRCUST2-" + Constant.offerCode;
	String subCode                 = "BRSUB2_" + Constant.offerCode ;
	String AP                      = "BRAP2_" + Constant.offerCode;

	@Test(priority = 2, enabled = true)
	public void _BR_T02_CycleRun_FullAutomatic() {
		System.out.println("***************** 2 - Cycle run : Full automatic  *****************");
	}

	/***   Create and Activate a new Subscription   ***/
	@Test(priority = 4, enabled = true)
	public void _BR_T02_createAndActivateSubscription() throws AWTException, InterruptedException{
		payloadTests.createBillingCycleByAPI(baseURI, login, password,BC_code,BC_code,"BILLINGACCOUNT","MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,BC_code,BC_code,"BILLINGACCOUNT","CAL_PERIOD_MONTHLY");
		
		payloadTests.createCustomer(baseURI, login, password, customerCode, customerCode, BC_code);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);
	}
	/***   Insert EDRs   ***/
	@Test(priority = 8, enabled = true)
	public void _BR_T02_insertChargeCdr() throws InterruptedException {
		int result;
		for(int i = 0; i<5; i++) {
			result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "",subCode,null);
			Assert.assertEquals(result, 1);
		}
	}

	/***   Run Rated Transaction Job (RT_Job)   ***/
	@Test(priority = 9, enabled = true)
	public void _BR_T02_runRTJob() throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   create Billing Run   ***/
	@Parameters()
	@Test(priority = 10, enabled = true)
	public void _BR_T02_createBillingRun(){
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.createCycleRun(BC_code,"FULL_AUTOMATIC",null,false,"Quarantine","Quarantine",null,null,null);
	}

	/***   go To Billing Run Details   ***/
	@Parameters()
	@Test(priority = 12, enabled = true)
	public void _BR_T02_searchForBillingRun(){
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_code,"NEW",null,null);
		billingrunTests.goToBillingRunDetails();
	}

	/***   process Billing Run  ***/
	@Parameters()
	@Test(priority = 14, enabled = true)
	public void _BR_T02_processBillingRun(){
		billingrunTests.processBillingRun();
	}

	/***   Check Billing Run Status  
	 * @throws InterruptedException ***/
	@Parameters()
	@Test(priority = 20, enabled = true)
	public void _BR_T02_checkBRStatusValidated() throws InterruptedException{
		dataSet.checkBRStatus(BC_code, "VALIDATED");
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_code,"VALIDATED",null,null);
		billingrunTests.goToBillingRunDetails();
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "VALIDATED");
	}

	/***   Check Invoice Generated  ***/
	@Parameters()
	@Test(priority = 22, enabled = true)
	public void _BR_T02_checkInvoiceGenerated(){
		List<String> invoiceDetails1 = billingrunTests.getInvoiceGeneratedDetails(1, version);
		String invoiceType;
		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails1.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(5), "VALIDATED");
			Assert.assertTrue(invoiceDetails1.get(6).equals("€80.00") || invoiceDetails1.get(6).equals("EUR 80.00") );
		}
		else {
			invoiceType = invoiceDetails1.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(7), "VALIDATED");
			Assert.assertTrue(invoiceDetails1.get(8).equals("€80.00") || invoiceDetails1.get(8).equals("EUR 80.00") );
		}
	}

	/***   Check Invoice Link  ***/
	@Parameters()
	@Test(priority = 24, enabled = true)
	public void _BR_T02_checkInvoiceLink(){
		String invoiceNumber = null;
		if(version.equals("14.1.X")) { invoiceNumber = billingrunTests.getInvoiceNumber();}
		else { invoiceNumber = billingrunTests.getInvoiceDetails("3");}
		billingrunTests.goToInvoicePage(2);
		InvoicesTests invoicesTests = new InvoicesTests();
		Assert.assertEquals(invoicesTests.getInvoiceNumber(), invoiceNumber); 
		if(!version.equalsIgnoreCase("17.X")) {
			Assert.assertTrue(invoicesTests.getInvoiceType().equals("Commercial invoice") || invoicesTests.getInvoiceType().equals( "Invoice") );	
		}
		if(version.equals("17.X")) {invoicesTests.navigateBack();}else {invoicesTests.goBack();}
	}

	/***   Check customer Link  ***/
	@Parameters()
	@Test(priority = 26, enabled = true)
	public void _BR_T02_checkCustomerLink(){
		String index = "4";
		if(version.equals("14.1.X")) {index = "3" ;}
		billingrunTests.goToCustomerPage(index);
		Assert.assertEquals(customersTests.getCustomerCode(), customerCode); 
		customersTests.goBack();
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "VALIDATED");
	}

	/***   check Billing Run List  ***/
	@Parameters()
	@Test(priority = 28, enabled = true)
	public void _BR_T02_checkBillingRunList(){
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_code,null,null,null);

		if(version.equals("14.1.X")){
			// check billing cycle
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(1), BC_code); 
			// check run type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(2), "CYCLE");
			// check split level
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(3), "BILLING ACCOUNT");
			// check process type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(5), "FULL_AUTOMATIC");
			// check status
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(6), "VALIDATED");
			// check accounts
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(8), "1");
			// check invoices
			//		Assert.assertEquals(billingrunTests.getBillingRunDetails().get(9), "1");
			// check amount without tax
			Assert.assertTrue(billingrunTests.getBillingRunDetailsFromList().get(10).equals("€80.00") || billingrunTests.getBillingRunDetailsFromList().get(10).equals("EUR 80.00") );
		}
		else {
			// check billing cycle
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(1), BC_code); 
			// check run type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(2), "CYCLE");
			// check split level
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(3), "BILLING ACCOUNT");
			// check process type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(5), "FULL_AUTOMATIC");
			// check status
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(6), "VALIDATED");
			// check accounts
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(9), "1");
			// check invoices
			//			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(10), "1");
			// check amount without tax
			Assert.assertTrue(billingrunTests.getBillingRunDetailsFromList().get(11).equals("€80.00") || billingrunTests.getBillingRunDetailsFromList().get(11).equals("EUR 80.00") );
		}
	}
}
