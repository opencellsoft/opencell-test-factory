package com.opencellsoft.testsuites.billingrun;

import java.awt.AWTException;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class BillingRunScenario07  extends BillingRunCommons {
	BillingRunDataSet dataSet = new BillingRunDataSet();
	String subCode             = "BRSUB7_"  + Constant.offerCode;
	String AP                  = "BRAP7_"   + Constant.offerCode;

	String BC_code             = "BC7-"     + Constant.offerCode;
	String BC_description      = "BC7_"     + Constant.offerCode;
	String customerCode        = "BRCUST7-" + Constant.offerCode;
	String consumerCode        = "BRCONS7-" + Constant.offerCode;
	String companyRegistration = getRandomNumberbetween1And200();

	@Test(priority = 0, enabled = true)
	public void _BR_T07_CycleRunIncrementalInvoiceLines() {
		System.out.println("***************** 7 - Cycle run: incremental invoice lines  *****************");
	}

	/***   create Billing Cycle   ***/
	@Parameters()
	@Test(priority = 4, enabled = true)
	public void _BR_T07_createBillingCycle(){
		// Go to billing cycle page
		homeTests.goToBillingCyclePage();
		// Create a new billing cycle
		billingCycleTests.createBillingCycle(BC_code,BC_description,dataSet.BillingCalendar,dataSet.IncrementalInvoiceLines,dataSet.applicationEl);
		billingCycleTests.updateBillingCycleAggregationRules(dataSet.enableAggregation,dataSet.dateAggregation,dataSet.DiscountAggregation ,dataSet.UseAccountingArticleLabel,dataSet.AggregateUnitPrice, dataSet.IgnoreSubscriptions, dataSet.IgnoreOrders, dataSet.IgnoreConsumers, dataSet.BusinessKey, dataSet.Parameter1, dataSet.Parameter2, dataSet.Parameter3 );
	}
	
	/***   Create and Activate a new Subscription   ***/
	@Test(priority = 8, enabled = true)
	public void _BR_T07_createAndActivateSubscription() throws AWTException, InterruptedException{
		payloadTests.createCustomer(baseURI, login, password, customerCode,customerCode , BC_code);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);
	}

	/***   Insert EDRs   ***/
	@Test(priority = 10, enabled = true)
	public void _BR_T07_addCharges() throws InterruptedException {
		int result;
		for(int i = 0; i<5; i++) {
			result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "",subCode,null);
			Assert.assertEquals(result, 1);
		}

		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   create Billing Run   ***/
	@Test(priority = 11, enabled = true)
	public void _BR_T07_createBillingRun(){
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.createCycleRun(BC_description,"AUTOMATIC","nextMonth",true,"Quarantine","Quarantine",true,true,null);
	}

	/***   process Billing Run   ***/
	@Test(priority = 12, enabled = true)
	public void _BR_T07_processBillingRun(){
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_description,"NEW","CYCLE",null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.processBillingRun();
	}

	/***   Check Billing Run Status  
	 * @throws InterruptedException ***/
	@Test(priority = 14, enabled = true)
	public void _BR_T07_checkBRStatus() throws InterruptedException{
		dataSet.checkBRStatus(BC_description,"OPEN");
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "OPEN");
	}

	/***   check Billing Repport  ***/
	@Parameters({"version"})
	@Test(priority = 16, enabled = true)
	public void _BR_T07_checkBillingRepport1(String version){
		if(!version.equals("14.1.X") && !version.equals("15.0.X")) {
			try {
				billingrunTests.regenerateRepport();
				billingrunTests.openRepport();
			}catch (Exception e) {
				billingrunTests.generateRepport();
				homeTests.goToBillingRunPage();
				billingrunTests.searchForBillingRun(null,BC_description,null,null,null);
				billingrunTests.goToBillingRunDetails();
				billingrunTests.openRepport();
				billingrunTests.regenerateRepport();
				billingrunTests.openRepport();
			}
			
			// check number of billing accounts
			Assert.assertEquals(billingrunTests.checkRepport("1","1","1"), "1");
			// check number of subscriptions
			Assert.assertEquals(billingrunTests.checkRepport("1","1","2"), "1");

			// check Number of rated items :  OneShot
			Assert.assertEquals(billingrunTests.checkRepport("2","1","2"), "0");
			// check Number of rated items :  Recurring
			Assert.assertEquals(billingrunTests.checkRepport("2","2","2"), "1");
			// check Number of rated items :  Usage
			Assert.assertEquals(billingrunTests.checkRepport("2","3","2"), "5");
		}
	}

	/***   Insert EDRs   ***/
	@Test(priority = 18, enabled = true)
	public void _BR_T07_addCharges2() throws InterruptedException {
		String result2 = applyOneShotChargeInstance(baseURI, login, password,dataSet.oneShotOtChargeCode, subCode,null,null);
		Assert.assertEquals(result2, "SUCCESS");
		int result;
		for(int i = 0; i<5; i++) {
			result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "",subCode,null);
			Assert.assertEquals(result, 1);
		}
		Thread.sleep(5000);
		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   Import New Rated Items And Keep Invoice Lines Open   ***/
	@Test(priority = 24, enabled = true)
	public void _BR_T07_importNewRatedItemsAndKeepInvoiceLinesOpen(){
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_description,"OPEN","CYCLE",null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.importNewRatedItemsAndKeepInvoiceLinesOpen();
	}

	/***   Check Billing Run Status  
	 * @throws InterruptedException ***/
	@Test(priority = 26, enabled = true)
	public void _BR_T07_checkBRStatus2() throws InterruptedException{
		dataSet.checkBRStatus(BC_description,"OPEN");
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "OPEN");
	}

	/***   check Billing Repport  ***/
	@Parameters({"version"})
	@Test(priority = 28, enabled = true)
	public void _BR_T07_checkBillingRepport2(String version){
		if(!version.equals("14.1.X") && !version.equals("15.0.X")) {
			billingrunTests.openRepport();
			// check number of billing accounts
			Assert.assertEquals(billingrunTests.checkRepport("1","1","1"), "1");
			// check number of subscriptions
			Assert.assertEquals(billingrunTests.checkRepport("1","1","2"), "1");

			// check Number of rated items :  OneShot
			Assert.assertEquals(billingrunTests.checkRepport("2","1","2"), "1");
			// check Number of rated items :  Recurring
			Assert.assertEquals(billingrunTests.checkRepport("2","2","2"), "1");
			// check Number of rated items :  Usage
			Assert.assertEquals(billingrunTests.checkRepport("2","3","2"), "10");
		}
	}

	/***   check Rated Items Status : OPEN  ***/
	@Parameters()
	@Test(priority = 30, enabled = true)
	public void _BR_T07_checkRatedItemsStatusOpen(){
		if(!version.equals("15.0.X")) {
			homeTests.goToRatedItemsPage();
			ratedItemsTests.AddfilterBySubscription();
			ratedItemsTests.filterBySubscription(subCode);
			Assert.assertEquals(ratedItemsTests.elementsFoundSize("OPEN"), 0);
			Assert.assertEquals(ratedItemsTests.elementsFoundSize("BILLED"), 12);
		}
	}

	/***   check Billing Run List  ***/
	@Parameters({"version"})
	@Test(priority = 32, enabled = true)
	public void _BR_T07_checkBillingRunList(String version){
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_description,null,null,null);	
		List<String> billingRunDetails = billingrunTests.getBillingRunDetailsFromList();
		if(version.equals("14.1.X")){
			billingRunDetails = billingrunTests.getBillingRunDetailsFromList();
			// check billing cycle
			Assert.assertEquals(billingRunDetails.get(1), BC_description); 
			// check run type
			Assert.assertEquals(billingRunDetails.get(2), "CYCLE");
			// check split level
			Assert.assertEquals(billingRunDetails.get(3), "BILLING ACCOUNT");
			// check process type
			Assert.assertEquals(billingRunDetails.get(5), "AUTOMATIC");
			// check status
			Assert.assertEquals(billingRunDetails.get(6), "OPEN");
			// check accounts
			Assert.assertEquals(billingRunDetails.get(8), "1");
			// check invoices
			Assert.assertEquals(billingRunDetails.get(9), "");
			// check amount without tax
			String result = billingRunDetails.get(10);
			Assert.assertTrue(result.equals("€150.00") || result.equals("EUR 150.00"));
		}
		else {
			// check billing cycle
			Assert.assertEquals(billingRunDetails.get(1), BC_description); 
			// check run type
			Assert.assertEquals(billingRunDetails.get(2), "CYCLE");
			// check split level
			Assert.assertEquals(billingRunDetails.get(3), "BILLING ACCOUNT");
			// check process type
			Assert.assertEquals(billingRunDetails.get(5), "AUTOMATIC");
			// check status
			Assert.assertEquals(billingRunDetails.get(6), "OPEN");
			// check accounts
			Assert.assertEquals(billingRunDetails.get(9), "1");
			// check invoices
			Assert.assertEquals(billingRunDetails.get(10), "");
			// check amount without tax
			String result = billingRunDetails.get(11);
			Assert.assertTrue(result.equals("€150.00") || result.equals("EUR 150.00"));
		}
	}

	/***   Insert EDRs   ***/
	@Test(priority = 34, enabled = true)
	public void _BR_T07_addCharges3() throws InterruptedException {
		int result;
		for(int i = 0; i<5; i++) {
			result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "",subCode,null);
			Assert.assertEquals(result, 1);
		}

		String result2 = applyOneShotChargeInstance(baseURI, login, password,dataSet.oneShotOtChargeCode, subCode,null,null);
		Assert.assertEquals(result2, "SUCCESS");
		String result3 = applyOneShotChargeInstance(baseURI, login, password,dataSet.oneShotOtChargeCode, subCode,null,null);
		Assert.assertEquals(result3, "SUCCESS");

		jobsTests.runRTJob(baseURI, login, password);
		
	}

	/***   Import New Rated Items And Keep Invoice Lines Open   ***/
	@Parameters({"version"})
	@Test(priority = 36, enabled = true)
	public void _BR_T07_importNewRatedItemsAndKeepInvoiceLinesOpen2(String version){
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_description,"OPEN","CYCLE",null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.importNewRatedItemsAndKeepInvoiceLinesOpen();
	}


	/***   close Invoice Lines And Generate Invoices  ***/
	@Parameters()
	@Test(priority = 38, enabled = true)
	public void _BR_T07_closeInvoiceLinesAndGenerateInvoices(){
		billingrunTests.closeInvoiceLinesAndGenerateInvoices();
		billingrunTests.processBillingRun();
	}


	/***   Check Billing Run Status  
	 * @throws InterruptedException ***/
	@Test(priority = 40, enabled = true)
	public void _BR_T07_checkBRStatus3() throws InterruptedException{
		dataSet.checkBRStatus(BC_description,"VALIDATED");
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "VALIDATED");
	}

	/***   check Billing Repport  ***/
	@Test(priority = 42, enabled = true)
	public void _BR_T07_checkBillingRepport3(){
		if(!version.equals("14.1.X") && !version.equals("15.0.X")) {
			billingrunTests.openRepport();
			// check number of billing accounts
			Assert.assertEquals(billingrunTests.checkRepport("1","1","1"), "1");
			// check number of subscriptions
			Assert.assertEquals(billingrunTests.checkRepport("1","1","2"), "1");

			// check Number of rated items :  OneShot
			Assert.assertEquals(billingrunTests.checkRepport("2","1","2"), "3");
			// check Number of rated items :  Recurring
			Assert.assertEquals(billingrunTests.checkRepport("2","2","2"), "1");
			// check Number of rated items :  Usage
			Assert.assertEquals(billingrunTests.checkRepport("2","3","2"), "15");
		}
	}

	/***   check Billing Run List  ***/
	@Test(priority = 44, enabled = true)
	public void _BR_T07_checkBillingRunList3(){

		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_description,"VALIDATED",null,null);
		List<String> billingRunDetails = billingrunTests.getBillingRunDetailsFromList();
		if(version.equals("14.1.X")){
			billingRunDetails = billingrunTests.getBillingRunDetailsFromList();
			// check billing cycle
			Assert.assertEquals(billingRunDetails.get(1), BC_description); 
			// check run type
			Assert.assertEquals(billingRunDetails.get(2), "CYCLE");
			// check split level
			Assert.assertEquals(billingRunDetails.get(3), "BILLING ACCOUNT");
			// check process type
			Assert.assertEquals(billingRunDetails.get(5), "AUTOMATIC");
			// check status
			Assert.assertEquals(billingRunDetails.get(6), "VALIDATED");
			// check accounts
			Assert.assertEquals(billingRunDetails.get(8), "1");
			// check invoices
			Assert.assertEquals(billingRunDetails.get(9), "1");
			// check amount without tax
			String result = billingRunDetails.get(10);
			Assert.assertTrue(result.equals("€240.00") || result.equals("EUR 240.00"));
		}
		else {
			// check billing cycle
			Assert.assertEquals(billingRunDetails.get(1), BC_description); 
			// check run type
			Assert.assertEquals(billingRunDetails.get(2), "CYCLE");
			// check split level
			Assert.assertEquals(billingRunDetails.get(3), "BILLING ACCOUNT");
			// check process type
			Assert.assertEquals(billingRunDetails.get(5), "AUTOMATIC");
			// check status
			Assert.assertEquals(billingRunDetails.get(6), "VALIDATED");
			// check accounts
			Assert.assertEquals(billingRunDetails.get(9), "1");
			// check invoices
			Assert.assertEquals(billingRunDetails.get(10), "1");
			// check amount without tax
			String result = billingRunDetails.get(11);
			Assert.assertTrue(result.equals("€240.00") || result.equals("EUR 240.00"));
		}
	}
}
