package com.opencellsoft.testsuites.billingrun;

import java.awt.AWTException;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class BillingRunScenario03  extends BillingRunCommons {
	BillingRunDataSet dataSet = new BillingRunDataSet();

	String BC_code        = "BRBC3-"  + Constant.offerCode;
	String BC_Description = "BRBC3_"  + Constant.offerCode;
	String customerCode   = "BRCUST3-"+ Constant.offerCode;
	String subCode        = "BRSUB3_" + Constant.offerCode ;
	String AP             = "BRAP3_"  + Constant.offerCode;

	@Test(priority = 0, enabled = true)
	public void _BR_T03_CancelationWithReopenRT() {
		System.out.println("***************** 3 - Cancelation with re-open rated transactions  *****************");
	}

	/***   Create and Activate a new Subscription   ***/
	@Test(priority = 4, enabled = true)
	public void _BR_T03_createAndActivateSubscription() throws AWTException, InterruptedException{
		payloadTests.createBillingCycleByAPI(baseURI, login, password,BC_code,BC_Description,"BILLINGACCOUNT","MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,BC_code,BC_Description,"BILLINGACCOUNT","CAL_PERIOD_MONTHLY");
		
		payloadTests.createCustomer(baseURI, login, password, customerCode, customerCode, BC_code);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);
	}

	/***   Insert EDRs   ***/
	@Test(priority = 6, enabled = true)
	public void _BR_T03_insertChargeCdr() throws InterruptedException {
		int result;
		for(int i = 0; i<5; i++) {
			result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "",subCode,null);
			Assert.assertEquals(result, 1);
		}
		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   create Billing Run   ***/
	@Test(priority = 10, enabled = true)
	public void _BR_T03_createBillingRun(){
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.createCycleRun(BC_Description,"AUTOMATIC",null,false,"Quarantine","Quarantine",null,null,null);
	}

	/***   go To Billing Run Details   ***/
	@Test(priority = 12, enabled = true)
	public void _BR_T03_processBillingRun(){
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_Description,"NEW",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.processBillingRun();
	}

	/***   Check Billing Run Status  
	 * @throws InterruptedException ***/
	@Test(priority = 14, enabled = true)
	public void _BR_T03_checkBRStatus() throws InterruptedException{
		Thread.sleep(5000);
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_Description,null,null,null);
		billingrunTests.goToBillingRunDetails();
		dataSet.checkBRStatus(BC_Description,"DRAFT INVOICES");
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "DRAFT INVOICES");
	}

	/***   check Rated Items Status : Billed  ***/
	@Test(priority = 16, enabled = true)
	public void _BR_T03_checkRatedItemsStatusBilled(){
		if(!version.equals("15.0.X")) {
			homeTests.goToRatedItemsPage();
			ratedItemsTests.AddfilterBySubscription();
			ratedItemsTests.filterBySubscription(subCode);
			Assert.assertEquals(ratedItemsTests.elementsFoundSize("OPEN"), 0);
			Assert.assertEquals(ratedItemsTests.elementsFoundSize("BILLED"), 6);
		}
	}

	/***   Check customer Link  ***/
	@Test(priority = 18, enabled = true)
	public void _BR_T03_CancelBillingRunWithReopenRTs(){
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_Description,null,null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.cancelBillingRun();
	}

	/***   check Billing Run List   ***/
	@Test(priority = 20, enabled = true)
	public void _BR_T03_checkBillingRunStatus() throws InterruptedException{
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_Description,null,null,null);
		billingrunTests.goToBillingRunDetails();
		dataSet.checkBRStatus(BC_Description,"CANCELED");
	}

	/***   check Rated Items Status : OPEN  ***/
	@Test(priority = 22, enabled = true)
	public void _BR_T03_checkRatedItemsStatusOpen(){
		if(!version.equals("15.0.X")) {
			homeTests.goToRatedItemsPage();
			ratedItemsTests.AddfilterBySubscription();
			ratedItemsTests.filterBySubscription(subCode);
			Assert.assertEquals(ratedItemsTests.elementsFoundSize("OPEN"), 6);
			Assert.assertEquals(ratedItemsTests.elementsFoundSize("BILLED"), 0);
		}
	}

	/***   check Billing Run List  ***/
	@Test(priority = 24, enabled = true)
	public void _BR_T03_checkBillingRunList(){
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_Description,null,null,null);

		if(version.equals("14.1.X")){
			// check billing cycle
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(1), BC_Description); 
			// check run type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(2), "CYCLE");
			// check split level
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(3), "BILLING ACCOUNT");
			// check process type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(5), "AUTOMATIC");
			// check status
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(6), "CANCELED");
			// check accounts
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(8), "-");
			// check invoices
			//		Assert.assertEquals(billingrunTests.getBillingRunDetails().get(9), "-");
			// check amount without tax
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(10), "-");
		}
		else {
			// check billing cycle
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(1), BC_Description); 
			// check run type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(2), "CYCLE");
			// check split level
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(3), "BILLING ACCOUNT");
			// check process type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(5), "AUTOMATIC");
			// check status
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(6), "CANCELED");
			// check accounts
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(9), "-");
			// check invoices
			//			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(10), "-");
			// check amount without tax
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(11), "-");
		}
	}

	@Parameters()
	@Test(priority = 25, enabled = true)
	public void _BR_T03_invoiceTheRTs() throws InterruptedException{
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.createCycleRun(BC_Description,"FULL_AUTOMATIC",null,false,"Quarantine","Quarantine",null,null,null);
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_Description,"NEW",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.processBillingRun();

		dataSet.checkBRStatus(BC_Description,"VALIDATED");

		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "VALIDATED");
	}
}
