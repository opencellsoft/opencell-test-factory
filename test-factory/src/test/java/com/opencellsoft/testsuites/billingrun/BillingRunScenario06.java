package com.opencellsoft.testsuites.billingrun;

import static org.testng.Assert.assertTrue;
import java.awt.AWTException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.opencellsoft.utility.Constant;

public class BillingRunScenario06  extends BillingRunCommons {
	BillingRunDataSet dataSet = new BillingRunDataSet();
	String BC_code          = "BRBC6-"  + Constant.offerCode;
	String customerCode     = "BRCUST6-"+ Constant.offerCode;
	String subCode          = "BRSUB6_" + Constant.offerCode ;
	String ExceptionalRunId = "";

	@Test(priority = 0, enabled = true)
	public void _BR_T06_exceptionalRunFullAutomatic( ) throws Exception {
		System.out.println("***************** 6 - Exceptional run  : Full automatic  *****************");
	}

	/***   Create and Activate a new Subscription   ***/
	@Test(priority = 4, enabled = true)
	public void _BR_T06_createAndActivateSubscription() throws AWTException, InterruptedException{
		payloadTests.createBillingCycleByAPI(baseURI, login, password,BC_code,BC_code,"BILLINGACCOUNT", "MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,BC_code,BC_code,"BILLINGACCOUNT", "CAL_PERIOD_MONTHLY");
		Thread.sleep(500);
		payloadTests.createCustomer(baseURI, login, password, customerCode,customerCode , BC_code);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, subCode, subCode);
		Thread.sleep(1000);
	}

	/***   Insert EDRs   ***/
	@Test(priority = 6, enabled = true)
	public void _BR_T06_insertChargeCdr() throws InterruptedException {
		// Get the current date
		LocalDate currentDate = LocalDate.now();

		// Set the date to the first day of the current month
		LocalDate firstDayOfMonth = currentDate.withDayOfMonth(1);

		// Set the date to the last day of the current month
		LocalDate twentiethDayOfMonth = LocalDate.of(currentDate.getYear(), currentDate.getMonth(), 20);

		// Define the desired date-time format pattern
		String pattern = "yyyy-MM-dd'T'HH:mm:ss.'00Z'";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);

		// Iterate through the dates from the first to the last day of the month
		LocalDate currentDateIterator = firstDayOfMonth;
		while (!currentDateIterator.isAfter(twentiethDayOfMonth)) {
			// Format and print each date
			String formattedDate = currentDateIterator.atStartOfDay().format(formatter);
			System.out.println("Formatted Date: " + formattedDate);
			insertChargeCdr(baseURI, login, password, subCode,"1", "", "", "",subCode,formattedDate);

			// Move to the next date
			currentDateIterator = currentDateIterator.plusDays(1);
		}

		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   create exceptional Run  ***/
	@Test(priority = 10, enabled = true)
	public void _BR_T06_createExceptionalRun() throws InterruptedException, JsonMappingException, JsonProcessingException{
		List<List<String>> filterFields = new ArrayList<>() ;

		List<String> firstField = new ArrayList<>();
		firstField.add("billingAcountCode");
		firstField.add("equal to");
		firstField.add(customerCode);

		List<String> secondField = new ArrayList<>();
		secondField.add("usageDate");
		secondField.add("less than");
		LocalDate currentDate = LocalDate.now();
		LocalDate tenthDayOfMonth = LocalDate.of(currentDate.getYear(), currentDate.getMonth(), 10);
		secondField.add(tenthDayOfMonth.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

		filterFields.add(firstField);
		filterFields.add(secondField);
		// Go to billing run page
		homeTests.goToBillingRunPage();
		// Create exceptional run
		String invoiceDate =  LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		ExceptionalRunId = "" + payloadTests.createExceptionalRunByAPI(baseURI, login, password,"FULL_AUTOMATIC",invoiceDate,null,null,null,"MOVE","MOVE",null,null,null,null, filterFields);
	}

	/***   process Billing Run   ***/
	@Test(priority = 12, enabled = true)
	public void _BR_T06_processBillingRun(){
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(ExceptionalRunId,null,"NEW","EXCEPTIONAL",null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.processBillingRun();
	}

	/***   Check Billing Run Status  
	 * @throws InterruptedException ***/
	@Test(priority = 14, enabled = true)
	public void _BR_T06_checkBRStatus() throws InterruptedException{
		dataSet.checkBRStatusExceptional(ExceptionalRunId,"VALIDATED");
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "VALIDATED");
	}

	/***   Check Invoice Generated  ***/
	@Parameters()
	@Test(priority = 16, enabled = true)
	public void _BR_T06_checkInvoiceGenerated(){
		List<String> invoiceDetails1 = billingrunTests.getInvoiceGeneratedDetails(1, version);
		String invoiceType;
		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails1.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(5), "VALIDATED");
			String result = invoiceDetails1.get(6);
			Assert.assertTrue(result.equals("€120.00") || result.equals("EUR 120.00") || result.equals("€90.00") || result.equals("EUR 90.00"));
		}else {
			invoiceType = invoiceDetails1.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(7), "VALIDATED");
			String result = invoiceDetails1.get(8);
			Assert.assertTrue(result.equals("€120.00") || result.equals("EUR 120.00") || result.equals("€90.00") || result.equals("EUR 90.00"));
		}
	}

	/***   check Billing Run List  ***/
	@Parameters()
	@Test(priority = 18, enabled = true)
	public void _BR_T06_checkBillingRunList(){
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(ExceptionalRunId,null,null,null,null);
		if(version.equals("14.1.X")){
			// check billing cycle
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(1), ""); 
			// check run type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(2), "EXCEPTIONAL");
			// check split level
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(3), "");
			// check process type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(5), "FULL_AUTOMATIC");
			// check status
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(6), "VALIDATED");
			// check accounts
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(8), "1");
			// check invoices
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(9), "1");
			// check amount without tax
			String result = billingrunTests.getBillingRunDetailsFromList().get(10);
			Assert.assertTrue(result.equals("€120.00") || result.equals("EUR 120.00") || result.equals("€90.00") || result.equals("EUR 90.00"));
		}
		else {
			// check billing cycle
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(1), ""); 
			// check run type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(2), "EXCEPTIONAL");
			// check split level
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(3), "");
			// check process type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(5), "FULL_AUTOMATIC");
			// check status
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(6), "VALIDATED");
			// check accounts
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(9), "1");
			// check invoices
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(10), "1");
			// check amount without tax
			String result = billingrunTests.getBillingRunDetailsFromList().get(11);
			Assert.assertTrue(result.equals("€120.00") || result.equals("EUR 120.00") || result.equals("€90.00") || result.equals("EUR 90.00"));
		}
	}


	/***   check Rated Items Status : OPEN  ***/
	@Test(priority = 20, enabled = true)
	public void _BR_T06_checkRatedItemsStatus(){
		if(!version.equals("15.0.X")) {
			homeTests.goToRatedItemsPage();
			ratedItemsTests.AddfilterBySubscription();
			ratedItemsTests.filterBySubscription(subCode);
			ratedItemsTests.showRowsPerPage("50");
			int openRT = ratedItemsTests.elementsFoundSize("OPEN");
			int billedRT = ratedItemsTests.elementsFoundSize("BILLED");
			System.out.println("openRT : " + openRT);
			System.out.println("billedRT : " + billedRT);
			assertTrue(((openRT == 12) && (billedRT == 9)) ||((openRT == 11) && (billedRT == 10)));
		}else {
			// Go to customers list page
			homeTests.goToCustomTablesListPage();

			// search for a customer and go to customer details page
			customersTests.searchForCustomerMethod(customerCode);

			// Go to rated items tab
			customersTests.GoToRatedItemsTab(subCode);
			waitPageLoaded();
			int openRT = ratedItemsTests.elementsFoundSize("OPEN");
			int billedRT = ratedItemsTests.elementsFoundSize("BILLED");
			System.out.println("openRT : " + openRT);
			System.out.println("billedRT : " + billedRT);
			assertTrue(((openRT == 12) && (billedRT == 9)) ||((openRT == 11) && (billedRT == 10)));
		}
	}
}
