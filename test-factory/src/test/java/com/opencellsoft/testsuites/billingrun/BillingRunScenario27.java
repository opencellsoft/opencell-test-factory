package com.opencellsoft.testsuites.billingrun;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class BillingRunScenario27  extends BillingRunCommons {
	BillingRunDataSet dataSet = new BillingRunDataSet();

	@Test(priority = 0, enabled = true)
	public void _BR_T27_filterByRunType_Login() {
		System.out.println("***************** 27 - Filter by Run type   *****************");
	}
	
	/***   filter By Cycle   ***/
	@Test(priority = 4, enabled = true)
	public void _BR_T27_filterByCycle() {
		// go to billing run list page
		homeTests.goToBillingRunPage();
		billingrunTests.filterByCycle();
	}
	
	/***   check pagination info   ***/
	@Test(priority = 6, enabled = true)
	public void _BR_T27_checkResult() {
		Assert.assertEquals(billingrunTests.elementMarkSize("EXCEPTIONAL"), 0);
		if(version.equals("14.1.X")) {
			Assert.assertEquals(billingrunTests.elementMarkSize("CYCLE"), 20);
		}else {
			Assert.assertEquals(billingrunTests.elementMarkSize("CYCLE"), 25);
		}
	}
	
	/***   filter By Exceptional   ***/
	@Test(priority = 10, enabled = true)
	public void _BR_T27_filterByExceptional() {
		billingrunTests.filterByExceptional();
	}
	
	/***   checkResult2   ***/
	@Test(priority = 12, enabled = true)
	public void _BR_T27_checkResult2() {
		Assert.assertEquals(billingrunTests.elementMarkSize("CYCLE"), 0);		
	}
	
	/***   filterByOrder   ***/
	@Test(priority = 14, enabled = true)
	public void _BR_T27_filterByAll() {
		billingrunTests.filterByAllRunTypes();
	}
	
	/***   checkResult3   ***/
	@Test(priority = 16, enabled = true)
	public void _BR_T27_checkResult3() {
		Assert.assertNotEquals(billingrunTests.elementSize("CYCLE"), 0);
		Assert.assertNotEquals(billingrunTests.elementSize("EXCEPTIONAL"), 0);
	}

}
