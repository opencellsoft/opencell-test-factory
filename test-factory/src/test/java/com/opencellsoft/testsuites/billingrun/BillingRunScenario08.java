package com.opencellsoft.testsuites.billingrun;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class BillingRunScenario08  extends BillingRunCommons {
	BillingRunDataSet dataSet = new BillingRunDataSet();
	String customerCode            = "BRCUST8-" + Constant.offerCode;
	String BC_code                 = "BRBC8-"   + Constant.offerCode;
	String BC_description          = "BRBC8_"   + Constant.offerCode;
	String subCode1                = "BRSUB81_" + Constant.subscriptionCode;
	String AP1                     = "BRAP81_"  + Constant.accessPointCode;
	String subCode2                = "BRSUB82_" + Constant.subscriptionCode;
	String AP2                     = "BRAP82_"  + Constant.accessPointCode;
	String BillingCalendar         = "MONTHLY";
	Boolean IncrementalInvoiceLines= true;
	String applicationEl           = "";
	
	Boolean enableAggregation      = true;
	String dateAggregation         = "Aggregate by day";
	String DiscountAggregation     = "";
	Boolean UseAccountingArticleLabel = false;
	Boolean AggregateUnitPrice     = false;
	Boolean IgnoreSubscriptions    = true;
	Boolean IgnoreOrders           = true;
	Boolean IgnoreConsumers        = null;
	Boolean BusinessKey            = null;
	Boolean Parameter1             = null;
	Boolean Parameter2             = null;
	Boolean Parameter3             = null ;
	String splitLevel              = "SUBSCRIPTION";
	String companyRegistration     = getRandomNumberbetween1And200();
	

	@Test(priority = 2, enabled = true)
	public void _BR_T08_CycleRunSpliBySubscription_login() {
		System.out.println("***************** 8 - Cycle run: split by subscription & openBR  *****************");
	}
	
	/***   create Billing Cycle   ***/
	@Parameters()
	@Test(priority = 4, enabled = true)
	public void _BR_T08_createBillingCycleSplitBySubscription(){
		// Go to billing cycle page
		homeTests.goToBillingCyclePage();
		// Create a new billing cycle
		billingCycleTests.createBillingCycle(BC_code,BC_description,BillingCalendar,IncrementalInvoiceLines,applicationEl);
		billingCycleTests.updateBillingCycleSplitRules(splitLevel);
	}
	
	/***   Create a new customer  ***/
	@Test(priority = 6, enabled = true)
	public void _BR_T08_createCustomerAndTwoSubscriptions() throws InterruptedException {
		payloadTests.createCustomer(baseURI, login, password, customerCode,customerCode, BC_code);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode1, customerCode, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode1, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP1, subCode1);
		Thread.sleep(1000);
		
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode2, customerCode, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode2, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP2, subCode2);
		Thread.sleep(1000);
		
	}
	
	/***   Insert EDRs   ***/
	@Test(priority = 12, enabled = true)
	public void _BR_T08_addCharges() throws InterruptedException {
		int result;
		for(int i = 0; i<2; i++) {
			result = insertChargeCdr(baseURI, login, password, AP1,"1", "", "", "",subCode1,null);
			Assert.assertEquals(result, 1);
		}
		for(int i = 0; i<3; i++) {
			result = insertChargeCdr(baseURI, login, password, AP2,"1", "", "", "",subCode2,null);
			Assert.assertEquals(result, 1);
		}
		jobsTests.runRTJob(baseURI, login, password);
	}
	
	/***   create Billing Run   ***/
	@Test(priority = 14, enabled = true)
	public void _BR_T08_createBillingRun(){
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.createCycleRun(BC_description,"AUTOMATIC","nextMonth",true,null,null,true,true,null);
	}
	
	/***   process Billing Run   ***/

	@Test(priority = 16, enabled = true)
	public void _BR_T08_processBillingRun(){
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_description,"NEW","CYCLE",null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.processBillingRun();
	}
	
	/***   Check Billing Run Status  
	 * @throws InterruptedException ***/
	@Test(priority = 18, enabled = true)
	public void _BR_T08_checkBRStatus() throws InterruptedException{
		dataSet.checkBRStatus(BC_description,"OPEN");
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "OPEN");
	}
	
	/***   check Billing Repport  ***/
	@Test(priority = 20, enabled = true)
	public void _BR_T08_checkBillingRepport1(){
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_description,null,null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.openRepport();
		if(!version.equals("14.1.X") && !version.equals("15.0.X")) {
			try {
				billingrunTests.regenerateRepport();
				billingrunTests.openRepport();
			}catch (Exception e) {
				billingrunTests.generateRepport();
				homeTests.goToBillingRunPage();
				billingrunTests.searchForBillingRun(null,BC_description,null,null,null);
				billingrunTests.goToBillingRunDetails();
				billingrunTests.openRepport();
				billingrunTests.regenerateRepport();
				billingrunTests.openRepport();
			}
			// check number of billing accounts
			Assert.assertEquals(billingrunTests.checkRepport("1","1","1"), "1");
			// check number of subscriptions
			Assert.assertEquals(billingrunTests.checkRepport("1","1","2"), "2");
			
			// check Number of rated items :  OneShot
			Assert.assertEquals(billingrunTests.checkRepport("2","1","2"), "0");
			// check Number of rated items :  Recurring
			Assert.assertEquals(billingrunTests.checkRepport("2","2","2"), "2");
			// check Number of rated items :  Usage
			Assert.assertEquals(billingrunTests.checkRepport("2","3","2"), "5");
		}
	}
	
	/***   Insert EDRs   ***/
	@Test(priority = 22, enabled = true)
	public void _BR_T08_addCharges2() throws InterruptedException {
		String result2 = applyOneShotChargeInstance(baseURI, login, password,dataSet.oneShotOtChargeCode, subCode1,null,null);
		Assert.assertEquals(result2, "SUCCESS");
		int result;
		for(int i = 0; i<3; i++) {
			result = insertChargeCdr(baseURI, login, password, AP1,"1", "", "", "",subCode1,null);
			Assert.assertEquals(result, 1);
		}
		for(int i = 0; i<2; i++) {
			result = insertChargeCdr(baseURI, login, password, AP2,"1", "", "", "",subCode2,null);
			Assert.assertEquals(result, 1);
		}		
		
		Thread.sleep(5000);
		jobsTests.runRTJob(baseURI, login, password);
	}
	
	/***   Import New Rated Items And Keep Invoice Lines Open   ***/
	@Test(priority = 24, enabled = true)
	public void _BR_T08_importNewRatedItemsAndKeepInvoiceLinesOpen(){
		billingrunTests.importNewRatedItemsAndKeepInvoiceLinesOpen();
	}
	
	/***   Check Billing Run Status  ***/
	@Test(priority = 26, enabled = true)
	public void _BR_T08_checkBRStatus2() throws InterruptedException{
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_description,null,null,null);
		billingrunTests.goToBillingRunDetails();
		dataSet.checkBRStatus(BC_description,"OPEN");
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "OPEN");
	}
	
	/***   check Billing Repport  ***/
	@Test(priority = 28, enabled = true)
	public void _BR_T08_checkBillingRepport2(){
		if(!version.equals("14.1.X") && !version.equals("15.0.X")) {
			billingrunTests.openRepport();
			billingrunTests.regenerateRepport();
			billingrunTests.openRepport();
			// check number of billing accounts
			Assert.assertEquals(billingrunTests.checkRepport("1","1","1"), "1");
			// check number of subscriptions
			Assert.assertEquals(billingrunTests.checkRepport("1","1","2"), "2");
			
			// check Number of rated items :  OneShot
			Assert.assertEquals(billingrunTests.checkRepport("2","1","2"), "1");
			// check Number of rated items :  Recurring
			Assert.assertEquals(billingrunTests.checkRepport("2","2","2"), "2");
			// check Number of rated items :  Usage
			Assert.assertEquals(billingrunTests.checkRepport("2","3","2"), "10");
		}
	}
	
	/***   close Invoice Lines And Generate Invoices  ***/
	@Parameters()
	@Test(priority = 30, enabled = true)
	public void _BR_T08_closeInvoiceLinesAndGenerateInvoices(){
		billingrunTests.closeInvoiceLinesAndGenerateInvoices();
		billingrunTests.processBillingRun();
	}	
	
	/***   Check Billing Run Status  
	 * @throws InterruptedException ***/
	@Test(priority = 32, enabled = true)
	public void _BR_T08_checkBRStatus3() throws InterruptedException{
		dataSet.checkBRStatus(BC_description,"VALIDATED");
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "VALIDATED");
	}
	
	/***   Check Invoice Generated  ***/
	@Test(priority = 34, enabled = true)
	public void _BR_T08_checkInvoicesGenerated(){
		List<String> invoiceDetails1 = billingrunTests.getInvoiceGeneratedDetails(1, version);
		List<String> invoiceDetails2 = billingrunTests.getInvoiceGeneratedDetails(2, version);
		String invoiceType;
		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails1.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(5), "VALIDATED");
			String result = invoiceDetails1.get(6);
			Assert.assertTrue(result.equals("€100.00") || result.equals("EUR 100.00") || result.equals("€80.00") || result.equals("EUR 80.00"));
			
			invoiceType = invoiceDetails2.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails2.get(5), "VALIDATED");
			String result2 = invoiceDetails1.get(6);
			if(result.equals("€100.00") || result.equals("EUR 100.00")) {
				Assert.assertTrue(result2.equals("€80.00") || result2.equals("EUR 80.00"));
			}else {
				Assert.assertTrue(result2.equals("€100.00") || result2.equals("EUR 100.00"));
			}
			
		}else {
			
			invoiceType = invoiceDetails1.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(7), "VALIDATED");
			String result = invoiceDetails1.get(8);
			Assert.assertTrue(result.equals("€100.00") || result.equals("EUR 100.00") || result.equals("€80.00") || result.equals("EUR 80.00"));
			
			invoiceType = invoiceDetails2.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails2.get(7), "VALIDATED");
			String result2 = invoiceDetails2.get(8);
			if(result.equals("€100.00") || result.equals("EUR 100.00")) {
				Assert.assertTrue(result2.equals("€80.00") || result2.equals("EUR 80.00"));
			}else {
				Assert.assertTrue(result2.equals("€100.00") || result2.equals("EUR 100.00"));
			}
		}
	}
	
	/***   check Billing Repport  ***/
	@Test(priority = 36, enabled = true)
	public void _BR_T08_checkBillingRepport3(){
		if(!version.equals("14.1.X") && !version.equals("15.0.X")) {
			billingrunTests.openRepport();
			billingrunTests.regenerateRepport();
			billingrunTests.openRepport();
			// check number of billing accounts
			Assert.assertEquals(billingrunTests.checkRepport("1","1","1"), "1");
			// check number of subscriptions
			Assert.assertEquals(billingrunTests.checkRepport("1","1","2"), "2");
			
			// check Number of rated items :  OneShot
			Assert.assertEquals(billingrunTests.checkRepport("2","1","2"), "1");
			// check Number of rated items :  Recurring
			Assert.assertEquals(billingrunTests.checkRepport("2","2","2"), "2");
			// check Number of rated items :  Usage
			Assert.assertEquals(billingrunTests.checkRepport("2","3","2"), "10");
		}
	}
	
	/***   check Billing Run List  ***/
	@Test(priority = 38, enabled = true)
	public void _BR_T08_checkBillingRunList3(){

		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_description,"VALIDATED",null,null);
		List<String> billingRunDetails = billingrunTests.getBillingRunDetailsFromList();
		if(version.equals("14.1.X")){
			billingRunDetails = billingrunTests.getBillingRunDetailsFromList();
			// check billing cycle
			Assert.assertEquals(billingRunDetails.get(1), BC_description); 
			// check run type
			Assert.assertEquals(billingRunDetails.get(2), "CYCLE");
			// check split level
			Assert.assertEquals(billingRunDetails.get(3), "SUBSCRIPTION");
			// check process type
			Assert.assertEquals(billingRunDetails.get(5), "AUTOMATIC");
			// check status
			Assert.assertEquals(billingRunDetails.get(6), "VALIDATED");
			// check accounts
			Assert.assertEquals(billingRunDetails.get(8), "1");
			// check invoices
			Assert.assertEquals(billingRunDetails.get(9), "2");
			// check amount without tax
			String result = billingRunDetails.get(10);
			Assert.assertTrue(result.equals("€180.00") || result.equals("EUR 180.00"));
		}
		else {
			// check billing cycle
			Assert.assertEquals(billingRunDetails.get(1), BC_description); 
			// check run type
			Assert.assertEquals(billingRunDetails.get(2), "CYCLE");
			// check split level
			Assert.assertEquals(billingRunDetails.get(3), "SUBSCRIPTION");
			// check process type
			Assert.assertEquals(billingRunDetails.get(5), "AUTOMATIC");
			// check status
			Assert.assertEquals(billingRunDetails.get(6), "VALIDATED");
			// check accounts
			Assert.assertEquals(billingRunDetails.get(9), "1");
			// check invoices
			Assert.assertEquals(billingRunDetails.get(10), "2");
			// check amount without tax
			String result = billingRunDetails.get(11);
			Assert.assertTrue(result.equals("€180.00") || result.equals("EUR 180.00"));
		}
	}
}
