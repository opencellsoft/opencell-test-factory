package com.opencellsoft.testsuites.billingrun;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.opencellsoft.utility.Constant;

public class BillingRunScenario23_V3  extends BillingRunCommons {
	BillingRunDataSet dataSet = new BillingRunDataSet();
	String ref = Constant.offerCode;
//	String ref = "WQN2274";
	String bc1 = "BRBC23V3-" + ref;

	String custCode = "BRCUST23V3-" + ref + "-0010"; 
	String custDesc = "BRCUST23V3-" + ref + "_0010";

	String subCode = null;

	@Test(priority = 2, enabled = true)
	public void _BR_T23V3_InvoiceValidation_ActionOnSuspectAutomaticValidation() throws Exception {
		System.out.println("***************** 23 (V3) - Invoice validation : Action on reject  = Request manual action   *****************");
	}

	/***   Create data  ***/
	@Test(priority = 4, enabled = true)
	public void _BR_T23V3_createSeveralCustomersAndSubscriptions() throws InterruptedException {
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bc1,bc1,"BILLINGACCOUNT", "MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bc1,bc1,"BILLINGACCOUNT", "CAL_PERIOD_MONTHLY");

		for(int i = 1; i < 11; i++) {
			custCode = "BRCUST23V3-"+ref+"-00"+i;
			custDesc = "BRCUST23V3_"+ref+"_00"+i;
			subCode = "BRSUB23V3-"+ref+"-00"+i;

			payloadTests.createCustomer(baseURI, login, password, custCode,custDesc , bc1);
			Thread.sleep(500);
			payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, custCode, dataSet.offerCode, dataSet.productCode);
			Thread.sleep(500);
			payloadTests.activateServices(baseURI, login, password, subCode, dataSet.productCode);
			Thread.sleep(500);
			payloadTests.createAccessPoint(baseURI, login, password, subCode, subCode);
			Thread.sleep(1000);
			payloadTests.insertSeveralChargeCdr(baseURI, login, password, subCode, "p1", "p2", "p3");
		}

		// add other sub for the 5 first customers => objective : generate validated invoices for them
		for(int i = 1; i < 6; i++) {
			custCode = "BRCUST23V3-"+ref+"-00"+i;
			subCode = "BRSUB23V3-"+ref+"-01"+i;

			payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, custCode, dataSet.offerCode, dataSet.productCode);
			Thread.sleep(500);
			payloadTests.activateServices(baseURI, login, password, subCode, dataSet.productCode);
			Thread.sleep(500);
			payloadTests.createAccessPoint(baseURI, login, password, subCode, subCode);
			Thread.sleep(1000);
			payloadTests.insertSeveralChargeCdr(baseURI, login, password, subCode, "p1", "p2", "p3");
		}

		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   Configure invoice validation rule   ***/
	@Test(priority = 5, enabled = true)
	public void _BR_T23V3_configureInvoiceValidationRule() {
		// Go to invoice validation page
		homeTests.goToInvoiceValidationPage();
		// select com invoice 
		invoiceValidationTests.goToComInvoice();
		// set (rule : suspect), (scriptException : compareInvoiceAmmountScript ( amountWithoutTaw,>, 500) )
		invoiceValidationTests.setInvoiceValidationRule1("validationRule1","rejected", "500", version);
	}

	/***   Create Cycle run (Action en suspect = Cancel)   ***/
	@Test(priority = 6, enabled = true)
	public void _BR_T23V3_createCycleRun() {
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.createCycleRun(bc1,"AUTOMATIC","nextMonth",false,"Request manual action",null,null,null,null);
	}

	/***   Process BR   ***/
	@Test(priority = 8, enabled = true)
	public void _BR_T23V3_processBR() {
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"NEW",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.processBillingRun();
	}

	/***   check BR status   ***/
	@Test(priority = 10, enabled = true)
	public void _BR_T23V3_checkBillingRunStatus() {
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"REJECTED",null,null);
		billingrunTests.goToBillingRunDetails();
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "REJECTED");
	}

	/***   filter invoices by status   ***/
	@Test(priority = 11, enabled = true)
	public void _BR_T23V3_filterByInvoiceStatusDRAFT() {
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"REJECTED",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.clearFilter();
		billingrunTests.searchForInvoice(null,null,null,"DRAFT",null,null,version);
		Assert.assertEquals(billingrunTests.filteredInvoicesNumber(), 5);
	}

	/***   filter invoices by status   ***/
	@Test(priority = 11, enabled = true)
	public void _BR_T23V3_filterByInvoiceStatusREJECTED() {
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"REJECTED",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.clearFilter();
		billingrunTests.searchForInvoice(null,null,null,"REJECTED",null,null,version);
		Assert.assertEquals(billingrunTests.filteredInvoicesNumber(), 5);
	}
}
