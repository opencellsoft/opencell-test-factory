package com.opencellsoft.testsuites.billingrun;

import java.awt.AWTException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class BillingRunDataSet extends BillingRunCommons {
//	String code = Constant.offerCode; 
//	String code = "PSW5085"; // 15X
//	String code = "XCQ1169"; // 16X
	String code = "TEL0670"; // 17X
	
	String offerCode = "BROFF1-" + code;
	String productCode = "BRPROD1-" + code;
	String usageChargeCode = "BRUsage1-" + code;
	String oneShotOtChargeCode = "BROneShotOt1-" + code;
	String recurringChargeCode = "BRRecurring1-" + code;
	String BC_code = "BRBC1-" + code;
	String billingCycleDescription = "BRBC1_"+ code;
	String customerCode = "BRCUST1-" + code;

	
	String offerDescription = Constant.offerDescription;
	String productDescription = Constant.productDescription;
	String usageChargeDescription = Constant.chargeDescriptionUsage;
	String oneShotOtChargeDescription = Constant.oneShotChargeDescription;
	String recurringChargeDescription = Constant.chargeDescriptionRecurring;
	String pricePlanVersionCode1 = "BRPV1-" + code;
	String pricePlanVersionCode2 = "BRPV2-" + code;
	String pricePlanVersionCode3 = "BRPV3-" + code;
	String usageChargeAmount = "10.00";
	String oneShotOtherChargeAmount = "20.00";
	String recurringChargeAmount = "30.00";
	
	String BillingCalendar = "MONTHLY";
	Boolean IncrementalInvoiceLines = false;
	String applicationEl = "";
	Boolean enableAggregation = true;
	String dateAggregation = "Aggregate by day";
	String DiscountAggregation = "";
	Boolean UseAccountingArticleLabel = false;
	Boolean AggregateUnitPrice = false;
	Boolean IgnoreSubscriptions = true;
	Boolean IgnoreOrders = true;
	Boolean IgnoreConsumers = null;
	Boolean BusinessKey = null;
	Boolean Parameter1 = null;
	Boolean Parameter2 = null;
	Boolean Parameter3 = null ;
	String companyRegistration = getRandomNumberbetween1And200();

	@Test(priority = 0, enabled = true)
	public void _BR_DS_login(){
		System.out.println("***************** 1 - billing run : data set  *****************");
		System.out.println("String offerCode = \"" + offerCode + "\";");
		System.out.println("String productCode = \"" + productCode + "\";");
		System.out.println("String usageChargeCode = \"" + usageChargeCode + "\";");
		System.out.println("String oneShotOtChargeCode = \"" + oneShotOtChargeCode + "\";");
		System.out.println("String recurringChargeCode = \"" + recurringChargeCode + "\";");
	}
	
	/***  create a New Offer    ***/
	@Parameters({ "PriceVersionDate", "defaultQuantity", "minimalQuantity", "maximalQuantity", "sequence", "repetitionCalendar"})
	@Test(priority = 4, enabled = true)
	public void _BR_DS_createOffer(String PriceVersionDate, String defaultQuantity, String minimalQuantity, String maximalQuantity, String sequence, String repetitionCalendar) throws InterruptedException, AWTException {
		offersTests.createOfferMethod(offerCode, offerDescription);
		productsTests.createProductMethod(PriceVersionDate,productCode,productDescription );
		createOneShotOtherCharge();
		createUsageCharge();
		createRecurringCharge(repetitionCalendar);
		productsTests.publishProductVersion();
		productsTests.activateProductAndGoBack();
		homeTests.goToOffersPage();
		offersTests.searchForOfferMethod(offerCode, offerDescription);
		offersTests.pickProductToOffer( productCode,productDescription, version);
		offersTests.fillProductParametersMethod(defaultQuantity,minimalQuantity, maximalQuantity, sequence, false );
		offersTests.ActivateOfferMethod();
	}
	
	public void createOneShotOtherCharge() throws InterruptedException{	
		chargesTests.createOneshotCharge(oneShotOtChargeCode, oneShotOtChargeDescription, "Other", version);
		Thread.sleep(2000);
		chargesTests.createNewPriceVersion(pricePlanVersionCode1, oneShotOtherChargeAmount);
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(oneShotOtChargeDescription, version);
	}

	public void createRecurringCharge(String repetitionCalendar) throws InterruptedException{	
		chargesTests.createRecurringCharge(recurringChargeCode,recurringChargeDescription,repetitionCalendar, version);
		Thread.sleep(3000);
		chargesTests.createNewPriceVersion(pricePlanVersionCode3, recurringChargeAmount);
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(recurringChargeDescription, version);
	}
	
	public void createUsageCharge() throws InterruptedException{	
		chargesTests.createUsageCharge(usageChargeCode,usageChargeDescription,"","", version);
		Thread.sleep(3000);
		chargesTests.createNewPriceVersionPriceGrid(pricePlanVersionCode2);
		chargesTests.addUnitPriceToPriceVersion(usageChargeAmount);
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(usageChargeDescription, version);
	}
	
	public void checkBRStatus(String billingCycle, String status) throws InterruptedException{
		String billingRunStatus = billingrunTests.getBillingRunStatus();
		for(int i = 0; i < 5 ; i++) {
			billingRunStatus = billingrunTests.getBillingRunStatus();
			if(billingRunStatus.equals(status)) {
				break;
			}
//			else if(billingRunStatus.equals("INVOICE LINES CREATED")) {
//				billingrunTests.processBillingRun();
//			}
			else {
				Thread.sleep(5000);
				homeTests.goToBillingRunPage();
				billingrunTests.searchForBillingRun(null,billingCycle,null,null,null);
				billingrunTests.goToBillingRunDetails();
			}
		}
	}
	
	public void checkBRStatusExceptional(String ExceptionalRunId, String status) throws InterruptedException{
		String billingRunStatus = billingrunTests.getBillingRunStatus();
		for(int i = 0; i < 5 ; i++) {
			billingRunStatus = billingrunTests.getBillingRunStatus();
			if(billingRunStatus.equals(status)) {
				break;
			}
//			else if(billingRunStatus.equals("INVOICE LINES CREATED")) {
//				billingrunTests.processBillingRun();
//			}
			else {
				Thread.sleep(5000);
				homeTests.goToBillingRunPage();
				billingrunTests.searchForBillingRun(ExceptionalRunId,null,null,null,null);
				billingrunTests.goToBillingRunDetails();
			}
		}
	}
}
