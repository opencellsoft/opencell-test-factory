package com.opencellsoft.testsuites.billingrun;

import java.awt.AWTException;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class BillingRunScenario04  extends BillingRunCommons {
	BillingRunDataSet dataSet = new BillingRunDataSet();
	String BC_code        = "BRBC4-"   + Constant.offerCode;
	String BC_description = "BRBC4_"   + Constant.offerCode;
	String customerCode   = "BRCUST4-" + Constant.offerCode;
	String subCode        = "BRSUB4_"  + Constant.offerCode ;
	String AP             = "BRAP4_"   + Constant.offerCode;


	@Test(priority = 2, enabled = true)
	public void _BR_T04_CancelationwithCancelRatedTransactions_login() throws Exception {
		System.out.println("***************** 4 - Cancelation with cancel rated transactions  *****************");

		if(!version.equals("14.1.X")) {
			createAndActivateSubscription();
			insertChargeCdr();
			createBillingRun();
			processBillingRun();
			checkBRStatus();
			if(!version.equals("15.0.X")) { checkRatedItemsStatusBilled();}
			CancelBillingRunWithCancelRTs();
			checkBillingRunStatus();
			checkRatedItemsStatusCanceled();
			checkBillingRunList();
		}
	}

	/***   Create and Activate a new Subscription   ***/
	public void createAndActivateSubscription() throws AWTException, InterruptedException{
		payloadTests.createBillingCycleByAPI(baseURI, login, password,BC_code,BC_description,"BILLINGACCOUNT","MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,BC_code,BC_description,"BILLINGACCOUNT","CAL_PERIOD_MONTHLY");
		
		payloadTests.createCustomer(baseURI, login, password, customerCode, customerCode, BC_code);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);
	}

	/***   Insert EDRs   ***/
	public void insertChargeCdr() throws InterruptedException {
		int result;
		for(int i = 0; i<5; i++) {
			result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "",subCode,null);
			Assert.assertEquals(result, 1);
		}
		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   create Billing Run   ***/
	public void createBillingRun(){
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.createCycleRun(BC_description,"AUTOMATIC",null,false,"Quarantine","Quarantine",null,null,null);
	}

	/***   go To Billing Run Details   ***/
	public void processBillingRun(){
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_description,"NEW",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.processBillingRun();
	}

	/***   Check Billing Run Status  
	 * @throws InterruptedException ***/
	@Parameters()
	public void checkBRStatus() throws InterruptedException{
		dataSet.checkBRStatus(BC_description,"DRAFT INVOICES");
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "DRAFT INVOICES");
	}

	/***   check Rated Items Status : Billed  ***/
	public void checkRatedItemsStatusBilled(){
		homeTests.goToRatedItemsPage();
		ratedItemsTests.AddfilterBySubscription();
		ratedItemsTests.filterBySubscription(subCode);
		Assert.assertEquals(ratedItemsTests.elementsFoundSize("OPEN"), 0);
		Assert.assertEquals(ratedItemsTests.elementsFoundSize("BILLED"), 6);
	}

	/***   Check customer Link  ***/
	public void CancelBillingRunWithCancelRTs(){
		if(!version.equals("14.1.X")) {
			homeTests.goToBillingRunPage();
			billingrunTests.searchForBillingRun(null,BC_description,null,null,null);
			billingrunTests.goToBillingRunDetails();
			billingrunTests.cancelBillingRunCancelRTs();
		}
	}

	/***   check Billing Run List  
	 * @throws InterruptedException ***/
	public void checkBillingRunStatus() throws InterruptedException{
		if(!version.equals("14.1.X")) {
			dataSet.checkBRStatus(BC_description,"CANCELED");
			Assert.assertEquals(billingrunTests.getBillingRunStatus(), "CANCELED");
		}
	}

	/***   check Rated Items Status : CANCELED  ***/
	public void checkRatedItemsStatusCanceled(){
		if(!version.equals("15.0.X")) {
			homeTests.goToRatedItemsPage();
			ratedItemsTests.AddfilterBySubscription();
			ratedItemsTests.filterBySubscription(subCode);
			Assert.assertEquals(ratedItemsTests.elementsFoundSize("OPEN"), 0);
			Assert.assertEquals(ratedItemsTests.elementsFoundSize("BILLED"), 0);
			Assert.assertEquals(ratedItemsTests.elementsFoundSize("CANCELED"), 6);
		}

	}

	/***   check Billing Run List  ***/
	@Parameters()
	public void checkBillingRunList(){
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_description,null,null,null);

		if(version.equals("14.1.X")){
			// check billing cycle
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(1), BC_description); 
			// check run type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(2), "CYCLE");
			// check split level
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(3), "BILLING ACCOUNT");
			// check process type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(5), "AUTOMATIC");
			// check status
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(6), "CANCELED");
			// check accounts
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(8), "-");
			// check invoices
			//		Assert.assertEquals(billingrunTests.getBillingRunDetails().get(9), "-");
			// check amount without tax
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(10), "-");
		}
		else {
			// check billing cycle
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(1), BC_description); 
			// check run type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(2), "CYCLE");
			// check split level
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(3), "BILLING ACCOUNT");
			// check process type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(5), "AUTOMATIC");
			// check status
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(6), "CANCELED");
			// check accounts
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(9), "-");
			// check invoices
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(10), "-");
			// check amount without tax
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(11), "-");
		}
	}

}
