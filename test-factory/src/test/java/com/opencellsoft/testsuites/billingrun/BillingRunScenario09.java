package com.opencellsoft.testsuites.billingrun;

import java.util.Arrays;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class BillingRunScenario09  extends BillingRunCommons {
	BillingRunDataSet dataSet = new BillingRunDataSet();
	
	String BC_code                  = "BRBC9-"   + Constant.offerCode;
	String BC_description           = "BRBC9_"   + Constant.offerCode;
	String customerCode             = "BRCUST9-" + Constant.offerCode;
	String BillingCalendar          = "MONTHLY";
	Boolean IncrementalInvoiceLines = true;
	String applicationEl            = "";
	Boolean enableAggregation       = true;
	String dateAggregation          = "Aggregate by day";
	String DiscountAggregation      = "";
	Boolean UseAccountingArticleLabel = false;
	Boolean AggregateUnitPrice      = false;
	Boolean IgnoreSubscriptions     = true;
	Boolean IgnoreOrders            = true;
	Boolean IgnoreConsumers         = null;
	Boolean BusinessKey             = null;
	Boolean Parameter1 = null;
	Boolean Parameter2 = null;
	Boolean Parameter3 = null ;
	String splitLevel  = "ORDER";
	String companyRegistration = getRandomNumberbetween1And200();
	

	@Test(priority = 0, enabled = true)
	public void _BR_T09_CycleRunSpliByOrder() {
		System.out.println("***************** 9 - Cycle run: split by order  *****************");
	}
	
	/***   create Billing Cycle   ***/
	@Parameters()
	@Test(priority = 4, enabled = true)
	public void _BR_T09_createBillingCycleSplitByOrder(){
		// Go to billing cycle page
		homeTests.goToBillingCyclePage();
		// Create a new billing cycle
		billingCycleTests.createBillingCycle(BC_code,BC_description,BillingCalendar,IncrementalInvoiceLines,applicationEl);
		billingCycleTests.updateBillingCycleAggregationRules(enableAggregation,dateAggregation,DiscountAggregation ,UseAccountingArticleLabel,AggregateUnitPrice, IgnoreSubscriptions, IgnoreOrders, IgnoreConsumers, BusinessKey, Parameter1, Parameter2, Parameter3 );
		billingCycleTests.updateBillingCycleSplitRules(splitLevel);
	}
	
	/***   Create a new customer &  consumer  ***/
	@Parameters({ "billingCountry", "acrs" })
	@Test(priority = 6, enabled = true)
	public void _BR_T09_createCustomer(String billingCountry, String acrs) throws InterruptedException {
		payloadTests.createCustomer(baseURI, login, password, customerCode,customerCode , BC_code);
		Thread.sleep(500);
		homeTests.goToCustomTablesListPage();
		customersTests.searchForCustomerMethod(customerCode);
	}
	
	/***   Create and Activate a new order   ***/
	@Test(priority = 8, enabled = true)
	public void _BR_T09_createAndActivateOrder1() {

		// Create a new order from order tab in customer details page
		ordersTests.createOrderFromCustomerPage();

		// Create order line
		ordersTests.createOrderLine(dataSet.offerCode);

		// Activate 
		ordersTests.validateOrder1();
		
		ordersTests.goBack();
	}
	
	
	/***   Create and Activate a new order   ***/

	@Test(priority = 10, enabled = true)
	public void _BR_T09_createAndActivateOrder2() {

		// Create a new order from order tab in customer details page
		ordersTests.createOrderFromCustomerPage();

		// Create order line
		ordersTests.createOrderLine(dataSet.offerCode);

		// Activate 
		ordersTests.validateOrder1();

		ordersTests.goBack();
	}

	/***   Run Rated Transaction Job (RT_Job)   ***/
	@Test(priority = 12)
	public void _BR_T09_runRTJob() throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}	
	
	/***   create Billing Run   ***/
	@Test(priority = 14, enabled = true)
	public void _BR_T09_createBillingRun(){
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.createCycleRun(BC_description,"FULL_AUTOMATIC",null,false,"Quarantine","Quarantine",null,null,null);
	}
	
	/***   process Billing Run   ***/
	@Test(priority = 16, enabled = true)
	public void _BR_T09_processBillingRun(){
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_description,"NEW","CYCLE",splitLevel);
//		if(version.equals("14.1.X")){billingrunTests.sortListBy("Id");}
		billingrunTests.goToBillingRunDetails();
		billingrunTests.processBillingRun();
	}
	
	/***   Check Billing Run Status  
	 * @throws InterruptedException ***/
	@Test(priority = 32, enabled = true)
	public void _BR_T09_checkBRStatus3() throws InterruptedException{
		dataSet.checkBRStatus(BC_description,"VALIDATED");
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "VALIDATED");
	}
	
	/***   Check Invoice Generated  ***/
	@Test(priority = 34, enabled = true)
	public void _BR_T09_checkInvoicesGenerated(){
		List<String> invoiceDetails1 = billingrunTests.getInvoiceGeneratedDetails(1, version);
		List<String> invoiceDetails2 = billingrunTests.getInvoiceGeneratedDetails(2, version);
		String invoiceType;
		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails1.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(5), "VALIDATED");
			Assert.assertTrue(invoiceDetails1.get(6).equals("€30.00")  || invoiceDetails1.get(6).equals("EUR 30.00"));
			
			invoiceType = invoiceDetails2.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails2.get(5), "VALIDATED");
			Assert.assertTrue(invoiceDetails2.get(6).equals("€30.00")  || invoiceDetails2.get(6).equals("EUR 30.00"));
		}else {
			
			invoiceType = invoiceDetails1.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(7), "VALIDATED");
			Assert.assertTrue(invoiceDetails1.get(8).equals("€30.00")  || invoiceDetails1.get(8).equals("EUR 30.00"));
			
			invoiceType = invoiceDetails2.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails2.get(7), "VALIDATED");
			Assert.assertTrue(invoiceDetails2.get(8).equals("€30.00")  || invoiceDetails2.get(8).equals("EUR 30.00"));
		}
	}
	
	/***   check Billing Repport   ***/
	@Test(priority = 36, enabled = true)
	public void _BR_T09_checkBillingRepport3() throws InterruptedException{
		List<String> stringList = Arrays.asList("14.1.X", "15.0.X");
		if(!stringList.contains(version)) {
			billingrunTests.openRepport();
			try {
				billingrunTests.generateRepport();

			}catch (Exception e) {
				// TODO: handle exception
			}
			homeTests.goToBillingRunPage();
			billingrunTests.searchForBillingRun(null,BC_description,"VALIDATED","CYCLE",splitLevel);
			billingrunTests.goToBillingRunDetails();
			billingrunTests.openRepport();
			// check number of billing accounts
			Assert.assertEquals(billingrunTests.checkRepport("1","1","1"), "1");
			// check number of subscriptions
			Assert.assertEquals(billingrunTests.checkRepport("1","1","2"), "2");
			
			// check Number of rated items :  OneShot
			Assert.assertEquals(billingrunTests.checkRepport("2","1","2"), "0");
			// check Number of rated items :  Recurring
			Assert.assertEquals(billingrunTests.checkRepport("2","2","2"), "2");
			// check Number of rated items :  Usage
			Assert.assertEquals(billingrunTests.checkRepport("2","3","2"), "0");
		}
	}
	
	/***   check Billing Run List  ***/
	@Test(priority = 38, enabled = true)
	public void _BR_T09_checkBillingRunList1(){

		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,BC_description,"VALIDATED",null,null);
		List<String> billingRunDetails = billingrunTests.getBillingRunDetailsFromList();
		if(version.equals("14.1.X")){
//			billingrunTests.sortListBy("Id");
			billingRunDetails = billingrunTests.getBillingRunDetailsFromList();
			// check billing cycle
			Assert.assertEquals(billingRunDetails.get(1), BC_description); 
			// check run type
			Assert.assertEquals(billingRunDetails.get(2), "CYCLE");
			// check split level
			Assert.assertEquals(billingRunDetails.get(3), "ORDER");
			// check process type
			Assert.assertEquals(billingRunDetails.get(5), "FULL_AUTOMATIC");
			// check status
			Assert.assertEquals(billingRunDetails.get(6), "VALIDATED");
			// check accounts
			Assert.assertEquals(billingRunDetails.get(8), "1");
			// check invoices
			Assert.assertEquals(billingRunDetails.get(9), "2");
			// check amount without tax
			Assert.assertTrue(billingRunDetails.get(10).equals("€60.00") || billingRunDetails.get(10).equals("EUR 60.00"));
		}
		else {
			// check billing cycle
			Assert.assertEquals(billingRunDetails.get(1), BC_description); 
			// check run type
			Assert.assertEquals(billingRunDetails.get(2), "CYCLE");
			// check split level
			Assert.assertEquals(billingRunDetails.get(3), "ORDER");
			// check process type
			Assert.assertEquals(billingRunDetails.get(5), "FULL_AUTOMATIC");
			// check status
			Assert.assertEquals(billingRunDetails.get(6), "VALIDATED");
			// check accounts
			Assert.assertEquals(billingRunDetails.get(9), "1");
			// check invoices
			Assert.assertEquals(billingRunDetails.get(10), "2");
			// check amount without tax
			Assert.assertTrue(billingRunDetails.get(11).equals("€60.00") || billingRunDetails.get(11).equals("EUR 60.00"));
		}
	}
}
