package com.opencellsoft.testsuites.billingrun;

import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class BillingRunScenario19_20_21  extends BillingRunCommons {
	BillingRunDataSet dataSet = new BillingRunDataSet();
	String ref = Constant.offerCode;
	String bc1 = "BRBC19-" + ref;

	String custCode = "BRCUST19-" + ref + "-0010"; 
	String custDesc = "BRCUST19-" + ref + "_0010";

	String subCode = null;

	@Test(priority = 0, enabled = true)
	public void _BR_T19_InvoiceValidation_ActionOnSuspectCancel() {
		System.out.println("***************** 19 - Invoice validation : Action on suspect  = Cancel   *****************");
	}

	/***   Create data  ***/
	@Test(priority = 4, enabled = true)
	public void _BR_T19_createSeveralCustomersAndSubscriptions() throws InterruptedException {
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bc1,bc1,"BILLINGACCOUNT", "MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bc1,bc1,"BILLINGACCOUNT", "CAL_PERIOD_MONTHLY");

		for(int i = 1; i < 11; i++) {
			custCode = "BRCUST19-"+ref+"-00"+i;
			custDesc = "BRCUST19_"+ref+"_00"+i;
			subCode  = "BRSUB19-"+ref+"-00"+i;

			payloadTests.createCustomer(baseURI, login, password, custCode,custDesc , bc1);
			Thread.sleep(500);
			payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, custCode, dataSet.offerCode, dataSet.productCode);
			Thread.sleep(500);
			payloadTests.activateServices(baseURI, login, password, subCode, dataSet.productCode);
			Thread.sleep(500);
			payloadTests.createAccessPoint(baseURI, login, password, subCode, subCode);
			Thread.sleep(1000);
			payloadTests.insertSeveralChargeCdr(baseURI, login, password, subCode, "p1", "p2", "p3");
		}

		// add other sub for the 5 first customers => objective : generate validated invoices for them
		for(int i = 1; i < 6; i++) {
			custCode = "BRCUST19-"+ref+"-00"+i;
			subCode = "BRSUB19-"+ref+"-01"+i;

			payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, custCode, dataSet.offerCode, dataSet.productCode);
			Thread.sleep(500);
			payloadTests.activateServices(baseURI, login, password, subCode, dataSet.productCode);
			Thread.sleep(500);
			payloadTests.createAccessPoint(baseURI, login, password, subCode, subCode);
			Thread.sleep(1000);
			payloadTests.insertSeveralChargeCdr(baseURI, login, password, subCode, "p1", "p2", "p3");
		}

		jobsTests.runRTJob(baseURI, login, password);
	}


	/***   Configure invoice validation rule   ***/
	@Test(priority = 5, enabled = true)
	public void _BR_T19_configureInvoiceValidationRule() {
		// Go to invoice validation page
		homeTests.goToInvoiceValidationPage();
		// select com invoice 
		invoiceValidationTests.goToComInvoice();
		// set (rule : suspect), (scriptException : compareInvoiceAmmountScript ( amountWithoutTaw,>, 500) )
		invoiceValidationTests.setInvoiceValidationRule1("validationRule1","suspect", "500", version);
	}

	/***   Create Cycle run (Action en suspect = Cancel)   ***/
	@Test(priority = 6, enabled = true)
	public void _BR_T19_createCycleRun() {
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.createCycleRun(bc1,"AUTOMATIC","nextMonth",false,"Cancel invoice and re-open its rated transactions","Cancel invoice and re-open its rated transactions",null,null,null);
	}

	/***   Process BR   ***/
	@Test(priority = 8, enabled = true)
	public void _BR_T19_processBR() {
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"NEW",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.processBillingRun();
	}

	/***   check BR status   
	 * @throws InterruptedException ***/
	@Test(priority = 10, enabled = true)
	public void _BR_T19_checkBillingRunStatus() throws InterruptedException {
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"DRAFT INVOICES",null,null);
		billingrunTests.goToBillingRunDetails();
		dataSet.checkBRStatus(bc1,"DRAFT INVOICES");
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "DRAFT INVOICES");
	}

	/***   filter invoices by status   ***/
	@Test(priority = 11, enabled = true)
	public void _BR_T19_filterByInvoiceStatusDRAFT() {
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"DRAFT INVOICES",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.searchForInvoice(null,null,null,"DRAFT",null,null,version);
		Assert.assertEquals(billingrunTests.filteredInvoicesNumber(), 5);
	}

	/***   filter invoices by status   ***/
	@Test(priority = 12, enabled = true)
	public void _BR_T19_filterByInvoiceStatusCANCELED() {
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"DRAFT INVOICES",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.searchForInvoice(null,null,null,"CANCELED",null,null,version);
		Assert.assertEquals(billingrunTests.filteredInvoicesNumber(), 5);
	}	
	/***   check Rated Items Status ***/
	@Parameters()
	@Test(priority = 14, enabled = true)
	public void _BR_T19_checkRatedItemsStatusOpen(){
		if(!version.equals("15.0.X")) {
			homeTests.goToRatedItemsPage();
			ratedItemsTests.AddfilterBySubscription();

			subCode = "BRSUB19-"+ref+"-006";
			ratedItemsTests.filterBySubscription(subCode);
			Assert.assertEquals(ratedItemsTests.elementsFoundSize("BILLED"), 0);
			Assert.assertEquals(ratedItemsTests.elementsFoundSize("OPEN"), 25);

			subCode = "BRSUB19-"+ref+"-007";
			ratedItemsTests.filterBySubscription(subCode);
			Assert.assertEquals(ratedItemsTests.elementsFoundSize("BILLED"), 0);
			Assert.assertEquals(ratedItemsTests.elementsFoundSize("OPEN"), 25);

		}
	}

	/***   Process BR   ***/
	@Test(priority = 16, enabled = true)
	public void _BR_T19_processBR2() {
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"DRAFT INVOICES",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.processBillingRun();
	}

	/***   filter invoices by status   ***/
	@Test(priority = 18, enabled = true)
	public void _BR_T20_filterByInvoiceStatusVALIDATED() {	
		System.out.println(" ");
		System.out.println("***************** 20 - Invoices filter : Filter by STATUS   *****************");
		System.out.println(" ");
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"VALIDATED",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.searchForInvoice(null,null,null,"VALIDATED",null,null,version);
		Assert.assertEquals(billingrunTests.filteredInvoicesNumber(), 5);
	}
	/***   filter invoices by status   ***/
	@Test(priority = 20, enabled = true)
	public void _BR_T20_filterByInvoiceStatusCANCELED() {		
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"VALIDATED",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.searchForInvoice(null,null,null,"CANCELED",null,null,version);
		Assert.assertEquals(billingrunTests.filteredInvoicesNumber(), 5);
	}	

	/***   filter invoices by status   ***/
	@Test(priority = 22, enabled = true)
	public void _BR_T21_filterByInvoiceNumber() {	
		System.out.println(" ");
		System.out.println("***************** 21 - Invoices filter : Filter by NUMBER   *****************");
		System.out.println(" ");
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"VALIDATED",null,null);
		billingrunTests.goToBillingRunDetails();
		String number;

		// search by  number1
		List<String> invoiceDetails1 = billingrunTests.getInvoiceGeneratedDetails(1, version);
		List<String> invoiceDetails2 = billingrunTests.getInvoiceGeneratedDetails(2, version);

		if(version.equals("14.1.X")) {number = invoiceDetails1.get(0);}
		else {number = invoiceDetails1.get(1);}

		billingrunTests.searchForInvoice(number,null,null,null,null,null,version);
		Assert.assertEquals(billingrunTests.filteredInvoicesNumber(), 1);

		// search by  number2
		if(version.equals("14.1.X")) {number = invoiceDetails2.get(0);}
		else {number = invoiceDetails2.get(1);}

		billingrunTests.searchForInvoice(number,null,null,null,null,null,version);
		Assert.assertEquals(billingrunTests.filteredInvoicesNumber(), 1);
	}

}
