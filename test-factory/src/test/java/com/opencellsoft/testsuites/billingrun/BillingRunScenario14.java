package com.opencellsoft.testsuites.billingrun;

import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class BillingRunScenario14  extends BillingRunCommons {
	BillingRunDataSet dataSet = new BillingRunDataSet();
	String bc        = "BRBC14-"   + Constant.offerCode;
	String custCode1 = "BRCUST14-" + Constant.offerCode + "-01"; 
	String subCode1  = "BRSUB14-"  + Constant.offerCode + "-01";
	
	String custCode2 = "BRCUST14-" + Constant.offerCode + "-02"; 
	String subCode2  = "BRSUB14-"  + Constant.offerCode + "-02";
	String subCode3  = "BRSUB14-"  + Constant.offerCode + "-03";

	@Test(priority = 0, enabled = true)
	public void _BR_T14_InvoiceValidation_ActionOnSuspectQuarantine(){
		System.out.println("***************** 14 Invoice validation : \r\n"
				+ "         Fail mode = SUSPECT \r\n"
				+ "         Cycle Run : AUTOMATIC \r\n"
				+ "         Action on suspect  =  Quarantine \r\n"
				+ "*****************");
	}

	/***   Configure invoice validation rule   ***/
	@Test(priority = 2, enabled = false)
	public void _BR_T14_configureInvoiceValidationRule() {
		// Go to invoice validation page
		homeTests.goToInvoiceValidationPage();
		// select com invoice 
		invoiceValidationTests.goToComInvoice();
		// set (rule : suspect), (scriptException : compareInvoiceAmmountScript ( amountWithoutTaw,>, 500) )
		invoiceValidationTests.setInvoiceValidationRule1("validationRule1","suspect", "500", version);
	}

	/***   Create data  ***/
	@Test(priority = 4, enabled = true)
	public void _BR_T14_createCustomersAndSubscriptions() throws InterruptedException {
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bc,bc,"BILLINGACCOUNT", "MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bc,bc,"BILLINGACCOUNT", "CAL_PERIOD_MONTHLY");

		payloadTests.createCustomer(baseURI, login, password, custCode1,custCode1 , bc);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode1, custCode1, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode1, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, subCode1, subCode1);
		Thread.sleep(1000);
		payloadTests.insertSeveralChargeCdr(baseURI, login, password, subCode1, "p1", "p2", "p3");


		payloadTests.createCustomer(baseURI, login, password, custCode2,custCode2 , bc);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode2, custCode2, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode2, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, subCode2, subCode2);
		Thread.sleep(1000);
		payloadTests.insertSeveralChargeCdr(baseURI, login, password, subCode2, "p1", "p2", "p3");

		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode3, custCode2, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode3, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, subCode3, subCode3);
		Thread.sleep(1000);
		payloadTests.insertSeveralChargeCdr(baseURI, login, password, subCode3, "p1", "p2", "p3");

		jobsTests.runRTJob(baseURI, login, password);
	}



	/***   Create Cycle run (Action en suspect = "Automatic validation)   ***/
	@Test(priority = 6, enabled = true)
	public void _BR_T14_createCycleRun() {
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.createCycleRun(bc,"AUTOMATIC","nextMonth",false,null,"Quarantine",null,null,null);
	}

	/***   Process BR   ***/
	@Test(priority = 8, enabled = true)
	public void _BR_T14_processBR() {
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc,"NEW",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.processBillingRun();
	}

	/***   check BR status    ***/
	@Test(priority = 10, enabled = true)
	public void _BR_T14_checkBillingRunStatus() throws InterruptedException {
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc,null,null,null);
		billingrunTests.goToBillingRunDetails();
		dataSet.checkBRStatus(bc,"DRAFT INVOICES");
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "DRAFT INVOICES");
	}

	/***   Check invoices generated  ***/
	@Test(priority = 12, enabled = true)
	public void _BR_T14_checkInvoicesGenerated() {
		// filter by customer 2
		billingrunTests.searchForInvoice(null,custCode2,null,null,null,null,version);
		// get invoice details		
		List<String> invoiceDetails1 = billingrunTests.getInvoiceGeneratedDetails(1, version);
		String invoiceType;
		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails1.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(5), "DRAFT");
			Assert.assertTrue(invoiceDetails1.get(6).equals("€660.00") || invoiceDetails1.get(6).equals("EUR 660.00") );
		}
		else {
			invoiceType = invoiceDetails1.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(7), "DRAFT");
			Assert.assertTrue(invoiceDetails1.get(8).equals("€660.00") || invoiceDetails1.get(8).equals("EUR 660.00") );
		}

		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc,"DRAFT INVOICES",null,null);
		billingrunTests.goToBillingRunDetails();
		// filter by customer 1
		billingrunTests.searchForInvoice(null,custCode1,null,null,null,null,version);
		List<String> invoiceDetails2 = billingrunTests.getInvoiceGeneratedDetails(1, version);
		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails2.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails2.get(5), "SUSPECT");
			Assert.assertTrue(invoiceDetails2.get(6).equals("€330.00") || invoiceDetails2.get(6).equals("EUR 330.00") );
			Assert.assertEquals(invoiceDetails2.get(8), "validationRule1");
		}
		else {
			invoiceType = invoiceDetails2.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails2.get(7), "SUSPECT");
			Assert.assertTrue(invoiceDetails2.get(8).equals("€330.00") || invoiceDetails2.get(8).equals("EUR 330.00") );
			Assert.assertTrue(invoiceDetails2.get(9).contains("validationRule1"));
		}
	}

	/***   Process BR   ***/
	@Test(priority = 14, enabled = true)
	public void _BR_T14_processBRSecondTime() {
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc,null,null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.processBillingRun();
	}

	/***   check BR status   ***/
	@Test(priority = 16, enabled = true)
	public void _BR_T14_checkBillingRunStatusValidated() {
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc,"VALIDATED",null,null);
		billingrunTests.goToBillingRunDetails();
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "VALIDATED");
	}

	/***   Check invoices generated  ***/
	@Test(priority = 18, enabled = true)
	public void _BR_T14_checkInvoiceStatusValidated() {
		// get invoice details		
		List<String> invoiceDetails = billingrunTests.getInvoiceGeneratedDetails(1, version);
		String invoiceType;
		List<String> ExpectedAmountWOTaxValue = List.of("EUR 660.00","€660.00");
		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertTrue(invoiceDetails.get(5).equals("Validated")|| invoiceDetails.get(5).equals("VALIDATED"));
			Assert.assertTrue(ExpectedAmountWOTaxValue.contains(invoiceDetails.get(6)));
			Assert.assertTrue(invoiceDetails.get(8).contains(""));
		}
		else {
			invoiceType = invoiceDetails.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertTrue(invoiceDetails.get(7).equals("Validated")|| invoiceDetails.get(7).equals("VALIDATED"));
			Assert.assertTrue(ExpectedAmountWOTaxValue.contains(invoiceDetails.get(8)));
			Assert.assertTrue(invoiceDetails.get(9).contains(""));
		}

	}
	
	/***   check BR status   ***/
	@Test(priority = 20, enabled = true)
	public void _BR_T14_checkBillingRunStatusRejected() {
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc,"REJECTED",null,null);
		billingrunTests.goToBillingRunDetails();
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "REJECTED");
	}
	
	/***   Check invoices generated  ***/
	@Test(priority = 22, enabled = true)
	public void _BR_T14_checkInvoiceStatusSuspect() {
		// get invoice details
		List<String> invoiceDetails = billingrunTests.getInvoiceGeneratedDetails(1, version);
		List<String> ExpectedAmountWOTaxValue = List.of("EUR 330.00","€330.00");
		String invoiceType;
		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertTrue(invoiceDetails.get(5).equals("Suspect")|| invoiceDetails.get(5).equals("SUSPECT"));
			Assert.assertTrue(ExpectedAmountWOTaxValue.contains(invoiceDetails.get(6)));
			Assert.assertEquals(invoiceDetails.get(8), "validationRule1");
		}
		else {
			invoiceType = invoiceDetails.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertTrue(invoiceDetails.get(7).equals("Suspect")|| invoiceDetails.get(7).equals("SUSPECT"));
			Assert.assertTrue(ExpectedAmountWOTaxValue.contains(invoiceDetails.get(8)));
			Assert.assertTrue(invoiceDetails.get(9).contains("validationRule1"));
		}
	}
	
	/***   check Rated Items Status : Billed  ***/
	@Test(priority = 24, enabled = true)
	public void checkRatedItemsStatusBilled(){
		homeTests.goToRatedItemsPage();
		ratedItemsTests.AddfilterBySubscription();
		ratedItemsTests.filterBySubscription(subCode1);
		Assert.assertEquals(ratedItemsTests.elementsFoundSize("OPEN"), 0);
		Assert.assertEquals(ratedItemsTests.elementsFoundSize("CANCELED"), 0);
		if(version.equals("14.1.X")) {
			Assert.assertEquals(ratedItemsTests.elementsFoundSize("BILLED"), 20);
		}else {
			Assert.assertEquals(ratedItemsTests.elementsFoundSize("BILLED"), 25);
		}
		
	}
}
