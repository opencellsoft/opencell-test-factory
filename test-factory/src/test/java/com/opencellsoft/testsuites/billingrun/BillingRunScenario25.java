package com.opencellsoft.testsuites.billingrun;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.opencellsoft.utility.Constant;

public class BillingRunScenario25  extends BillingRunCommons {
	BillingRunDataSet dataSet = new BillingRunDataSet();
	String bc1 = "BRBC100-" + Constant.offerCode;
	String bc2 = "BRBC200-" + Constant.offerCode;
	String bc3 = "BRBC300-" + Constant.offerCode;
	String customerCode = "BRCUST11-" + Constant.offerCode;
	@Test(priority = 2, enabled = true)
	public void _BR_T25_Pagination_Login() throws Exception {
		System.out.println("***************** 25 - Pagination   *****************");
	}
	
	/***   create Several Billing Runs   ***/
	@Test(priority = 4, enabled = true)
	public void _BR_T25_createSeveralBillingRuns() throws JsonMappingException, JsonProcessingException, InterruptedException {
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bc1,bc1,"BILLINGACCOUNT","CAL_PERIOD_MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bc2,bc2,"SUBSCRIPTION","CAL_PERIOD_MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bc3,bc3,"ORDER","CAL_PERIOD_MONTHLY");
		
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bc1,bc1,"BILLINGACCOUNT","MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bc2,bc2,"SUBSCRIPTION","MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bc3,bc3,"ORDER","MONTHLY");
		
		
		for(int i = 0; i<15;i++) {
			payloadTests.createCycleRunByAPI(baseURI, login, password,"AUTOMATIC",bc1);
			payloadTests.createCycleRunByAPI(baseURI, login, password,"FULL_AUTOMATIC",bc1);
			
			payloadTests.createCycleRunByAPI(baseURI, login, password,"AUTOMATIC",bc2);
			payloadTests.createCycleRunByAPI(baseURI, login, password,"FULL_AUTOMATIC",bc2);
			
			payloadTests.createCycleRunByAPI(baseURI, login, password,"AUTOMATIC",bc3);
			payloadTests.createCycleRunByAPI(baseURI, login, password,"FULL_AUTOMATIC",bc3);
			
			List<List<String>> filterFields = new ArrayList<>() ;
			List<String> firstField = new ArrayList<>();
			firstField.add("billingAcountCode");
			firstField.add("equal to");
			firstField.add(customerCode);
			filterFields.add(firstField);
			// Create exceptional run
			String invoiceDate =  LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			payloadTests.createExceptionalRunByAPI(baseURI, login, password,"AUTOMATIC",invoiceDate,null,null,null,"MANUAL_ACTION","AUTOMATIC_VALIDATION",null,null,null,null, filterFields);
		}
		
	}
	
	/***   check pagination info   ***/
	@Test(priority = 6, enabled = true)
	public void _BR_T25_checkpaginationInfo() {
		// go to billing run list page
		homeTests.goToBillingRunPage();
		// check pagination 
		if(version.equals("14.1.X")) {
			Assert.assertTrue( billingrunTests.paginationIsDisplayed());
			Assert.assertTrue( billingrunTests.displayfrom1to20RowInThePage());
			Assert.assertEquals( billingrunTests.thereIs20RowDisplayed(), 20);
		}else {
			Assert.assertTrue( billingrunTests.paginationIsDisplayed());
			Assert.assertTrue( billingrunTests.displayfrom1to25RowInThePage());
			Assert.assertEquals( billingrunTests.thereIs25RowDisplayed(), 25);
		}
		
	}
	
	/***   goTotheNextPage   ***/
	@Test(priority = 10, enabled = true)
	public void _BR_T25_goTotheNextPage() {
		billingrunTests.goTotheNextPage();
	}
	
	/***   check pagination info   ***/
	@Test(priority = 12, enabled = true)
	public void _BR_T25_checkpaginationInTheNextPage() {
		
		// check pagination 
		if(version.equals("14.1.X")) {
			Assert.assertTrue( billingrunTests.paginationIsDisplayed());
			Assert.assertTrue( billingrunTests.displayfrom21to40RowInThePage());
			Assert.assertEquals( billingrunTests.thereIs20RowDisplayed(), 20);
		}else {
			Assert.assertTrue( billingrunTests.paginationIsDisplayed());
			Assert.assertTrue( billingrunTests.displayfrom26to50RowInThePage());
			Assert.assertEquals( billingrunTests.thereIs25RowDisplayed(), 25);
		}
	}
	
	/***   goBackToTheFirstPage   ***/
	@Test(priority = 14, enabled = true)
	public void _BR_T25_goBackToTheFirstPage() {
		billingrunTests.goToThePreviousPage();
	}
	
	/***   check pagination info   ***/
	@Test(priority = 16, enabled = true)
	public void _BR_T25_checkpaginationInTheFirstPage() {
		// check pagination 
		if(version.equals("14.1.X")) {
			Assert.assertTrue( billingrunTests.paginationIsDisplayed());
			Assert.assertTrue( billingrunTests.displayfrom1to20RowInThePage());
			Assert.assertEquals( billingrunTests.thereIs20RowDisplayed(), 20);
		}else {
			Assert.assertTrue( billingrunTests.paginationIsDisplayed());
			Assert.assertTrue( billingrunTests.displayfrom1to25RowInThePage());
			Assert.assertEquals( billingrunTests.thereIs25RowDisplayed(), 25);
		}
	}
	
	/***   changeRowsPerPageTo50   ***/
	@Test(priority = 18, enabled = true)
	public void _BR_T25_changeRowsPerPage() {
		if(version.equals("14.1.X")) {
			billingrunTests.changeRowsPerPageTo10();
		}else {
			billingrunTests.changeRowsPerPageTo50();
		}
	}
	
	/***   delete One Line Of Order3   ***/
	@Test(priority = 20, enabled = true)
	public void _BR_T25_checkPaginationInfo() {
		// check pagination 
		if(version.equals("14.1.X")) {
			Assert.assertTrue( billingrunTests.paginationIsDisplayed());
			Assert.assertTrue( billingrunTests.displayfrom1to10RowInThePage());
			Assert.assertEquals( billingrunTests.thereIs10RowDisplayed(), 10);
		}else {
			Assert.assertTrue( billingrunTests.paginationIsDisplayed());
			Assert.assertTrue( billingrunTests.displayfrom1to50RowInThePage());
			Assert.assertEquals( billingrunTests.thereIs50RowDisplayed(), 50);
		}
	}
}
