package com.opencellsoft.testsuites.billingrun;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.customercare.rateditems.RatedItemsPage;
import com.opencellsoft.tests.BillingCycleTests;
import com.opencellsoft.tests.BillingrunTests;
import com.opencellsoft.tests.ChargesTests;
import com.opencellsoft.tests.CustomersTests;
import com.opencellsoft.tests.FrameworkAgreementsTests;
import com.opencellsoft.tests.HomeTests;
import com.opencellsoft.tests.InvoiceValidationTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.tests.OffersTests;
import com.opencellsoft.tests.OrdersTests;
import com.opencellsoft.tests.ProductsTests;
import com.opencellsoft.tests.RatedItemsTests;
import com.opencellsoft.tests.ScriptTests;
import com.opencellsoft.tests.WalletOperationsTests;
import com.opencellsoft.tests.admin.PostgreeTests;
import com.opencellsoft.tests.admin.PricePlanMatrixTests;
import com.opencellsoft.tests.customercare.SubscriptionsTests;
import com.opencellsoft.tests.jobs.JobsTests;
import com.opencellsoft.tests.jobs.PayloadTests;

import io.restassured.RestAssured;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class BillingRunCommons extends TestBase{

	LoginTest loginTest;
	HomeTests homeTests;
	OffersTests offersTests;
	ProductsTests productsTests;
	ChargesTests chargesTests;
	CustomersTests customersTests;
	OrdersTests ordersTests;
	SubscriptionsTests subscriptionsTests;
	WalletOperationsTests walletOperationsTests;
//	WalletOperationsPage walletOperationsPage;
	JobsTests jobsTests;
	ScriptTests scriptTests;
	RatedItemsPage ratedItemsPage;
	FrameworkAgreementsTests frameworkAgreementsTests;
	PricePlanMatrixTests pricePlanMatrixTests;
//	ContractPage contractPage;
	BillingCycleTests billingCycleTests;
	BillingrunTests billingrunTests;
	RatedItemsTests ratedItemsTests;
	InvoiceValidationTests invoiceValidationTests;
	PayloadTests payloadTests;
	PostgreeTests postgreeTests;
	String version;
	String baseURI; 
	String login;
	String password;
	
	public BillingRunCommons() {
		loginTest = new LoginTest();
		homeTests = new HomeTests();
		offersTests = new OffersTests();
		ordersTests = new OrdersTests();
		productsTests = new ProductsTests();
		chargesTests = new ChargesTests();
		customersTests = new CustomersTests();
		billingCycleTests = new BillingCycleTests();
		billingrunTests = new BillingrunTests();
		subscriptionsTests = new SubscriptionsTests();
		jobsTests = new JobsTests();
		ratedItemsTests = new RatedItemsTests();
		payloadTests = new PayloadTests();
		invoiceValidationTests = new InvoiceValidationTests();
		postgreeTests = new PostgreeTests();
	}
	
	/***  setup  ***/
	@BeforeClass
	@Parameters(value = {"baseURI", "browser", "nodeURL", "login", "password", "version" })
	public void setup(String baseURI, String browser, String nodeURL, String login, String password, String version) throws Exception{
		initialize( browser, nodeURL, baseURI); 
		this.version = version;
		this.baseURI = baseURI; 
		this.login = login;
		this.password = password;
		
		loginTest.loginTest(login, password);
		homeTests.goToPortal();
	}

	/***  Teardown  ***/
//	@AfterClass
	public void Teardown() {
		TeardownTest();
	}

	/***  insert Charge Cdr  ***/
	public int insertChargeCdr(String baseURI, String login, String password, String AP,String qte, String param1, String param2, String param3,String param4, String date) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/v2/mediation/cdrs/chargeCdrList";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		// Send request
		//Calendar calendar;
		Response resp;
		JsonPath j;
		int l=0;

		for (int i=0; i<5; i++){
			if(l != 1) {
				Thread.sleep(1000);
				if(date == null) {
					LocalDateTime currentDateTime = LocalDateTime.now();
					String pattern = "yyyy-MM-dd'T'HH:mm:ss'.00Z'";
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
					date = currentDateTime.format(formatter);
				}
				// create API request body
				String body = "{\r\n"
						+ "    \"cdrs\": [\r\n"
						+ "    \"" + date + ";"+ qte +";" + AP + ";"+ param1 + ";"+ param2 + ";" + param3 + ";" + param4 + ";param5;param6;param7;param8;param9;" + date + ";" + date + ";" + date + ";" + date + ";" + date + ";0.1;0.2;0.3;0.4;0.5;extraParam\"\r\n"
						+ "    ]\r\n"
						+ "}";
				System.out.println("Request body : " + body);
				resp = given().header("Content-Type", "application/json").body(body).when()
						.post();

				j = resp.jsonPath();
				l = (Integer)j.get("statistics.success");

				System.out.println("statistics.success : " + l);
			}
			else {
				break;
			}
		}

		return l;
	}

	public String applyOneShotChargeInstance(String baseURI, String login, String password,String chargeCode, String subCode, String periode, String time) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;

		// Setting BasePath once
		RestAssured.basePath = "api/rest/billing/subscription/applyOneShotChargeInstance";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;

		// Send request
		//Calendar calendar;
		Response resp;
		JsonPath j;
		String l="FAIL";
		//String l="SUCCESS";
		for (int i=0; i<5; i++){
			if(!l.equals("SUCCESS")) {
				Thread.sleep(1000);

				LocalDateTime currentDateTime = LocalDateTime.now();
				if(time != null) {
					if(time.equals("past") ) {
						// Subtract two month
						currentDateTime = currentDateTime.minusMonths(2);
					}
					else if(time.equals("futur") ) {
						// Subtract two month
						currentDateTime = currentDateTime.plusMonths(2);
					}
				}
				if(periode != null) {
					if(periode.equals("before") ) {
						// Subtract one month
						currentDateTime = currentDateTime.minusMonths(1);
					}
					else if(periode.equals("after")) {
						// add one month
						currentDateTime = currentDateTime.plusMonths(1);
					}
					else {
						currentDateTime = currentDateTime.plusDays(1);
					}
				}
				// Format the date and time to the desired format
				String date = currentDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.'00Z'"));

				// create API request body

				String body= "{\r\n"
						+ "    \"description\": \"" + chargeCode + "\",\r\n"
						+ "    \"oneShotCharge\": \"" + chargeCode + "\",\r\n"
						+ "    \"subscription\": \"" + subCode + "\",\r\n"
						+ "    \"operationDate\": \"" + date + "\",\r\n"
						+ "    \"quantity\": 1,\r\n"
						+ "    \"criteria3\": \"" + subCode + "\",\r\n"
						+ "    \"generateRTs\": true\r\n"
						+ "}";
				System.out.println("Request body : " + body);
				resp = given().header("Content-Type", "application/json").body(body).when()
						.post();

				j = resp.jsonPath();
				l = (String)j.get("status");

				System.out.println("Response status : " + l);
			}
			else {
				break;
			}
		}

		return l;
	}
	
	public String getRandomNumberbetween1And200() {
		int min = 2;
        int max = 200;

        Random rand = new Random();
        int randomNumber = rand.nextInt(max - min + 2) + min;
        return  String.format("%04d", randomNumber);
	}
}
