package com.opencellsoft.testsuites.billingrun;

import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class BillingRunScenario17  extends BillingRunCommons {
	BillingRunDataSet dataSet = new BillingRunDataSet();
	String bc        = "BRBC17-"   + Constant.offerCode;
	String custCode1 = "BRCUST17-" + Constant.offerCode + "-01"; 
	String subCode1  = "BRSUB17-"  + Constant.offerCode + "-01";
	
	String custCode2 = "BRCUST17-" + Constant.offerCode + "-02"; 
	String subCode2  = "BRSUB17-"  + Constant.offerCode + "-02";
	String subCode3  = "BRSUB17-"  + Constant.offerCode + "-03";

	@Test(priority = 0, enabled = true)
	public void _BR_T17_InvoiceValidation_ActionOnSuspectQuarantine(){
		System.out.println("***************** 17 Invoice validation : \r\n"
				+ "         Fail mode = SUSPECT \r\n"
				+ "         Cycle Run : FULL AUTOMATIC \r\n"
				+ "         Action on suspect  =  Cancel invoice and re-open its rated transactions   \r\n"
				+ "*****************");
	}

	/***   Configure invoice validation rule   ***/
	@Test(priority = 2, enabled = false)
	public void _BR_T17_configureInvoiceValidationRule() {
		// Go to invoice validation page
		homeTests.goToInvoiceValidationPage();
		// select com invoice 
		invoiceValidationTests.goToComInvoice();
		// set (rule : suspect), (scriptException : compareInvoiceAmmountScript ( amountWithoutTaw,>, 500) )
		invoiceValidationTests.setInvoiceValidationRule1("validationRule1","suspect", "500", version);
	}

	/***   Create data  ***/
	@Test(priority = 4, enabled = true)
	public void _BR_T17_createCustomersAndSubscriptions() throws InterruptedException {
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bc,bc,"BILLINGACCOUNT", "MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bc,bc,"BILLINGACCOUNT", "CAL_PERIOD_MONTHLY");

		payloadTests.createCustomer(baseURI, login, password, custCode1,custCode1 , bc);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode1, custCode1, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode1, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, subCode1, subCode1);
		Thread.sleep(1000);
		payloadTests.insertSeveralChargeCdr(baseURI, login, password, subCode1, "p1", "p2", "p3");


		payloadTests.createCustomer(baseURI, login, password, custCode2,custCode2 , bc);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode2, custCode2, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode2, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, subCode2, subCode2);
		Thread.sleep(1000);
		payloadTests.insertSeveralChargeCdr(baseURI, login, password, subCode2, "p1", "p2", "p3");

		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode3, custCode2, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode3, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, subCode3, subCode3);
		Thread.sleep(1000);
		payloadTests.insertSeveralChargeCdr(baseURI, login, password, subCode3, "p1", "p2", "p3");

		jobsTests.runRTJob(baseURI, login, password);
	}



	/***   Create Cycle run (Action en suspect = "Automatic validation)   ***/
	@Test(priority = 6, enabled = true)
	public void _BR_T17_createCycleRun() {
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.createCycleRun(bc,"FULL_AUTOMATIC","nextMonth",false,"Request manual action","Cancel invoice and re-open its rated transactions",null,null,null);
	}

	/***   Process BR   ***/
	@Test(priority = 8, enabled = true)
	public void _BR_T17_processBR() {
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc,"NEW",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.processBillingRun();
	}

	/***   check BR status   ***/
	@Test(priority = 16, enabled = true)
	public void _BR_T17_checkBillingRunStatusValidated() {
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc,"VALIDATED",null,null);
		billingrunTests.goToBillingRunDetails();
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "VALIDATED");
	}

	/***   Check invoices generated  ***/
	@Test(priority = 18, enabled = true)
	public void _BR_T17_checkInvoicesStatus() {
		// filter by customer 2
		billingrunTests.searchForInvoice(null,custCode2,null,null,null,null,version);
		// get invoice details		
		List<String> invoiceDetails = billingrunTests.getInvoiceGeneratedDetails(1, version);
		String invoiceType;
		List<String> ExpectedAmountWOTaxValue = List.of("EUR 660.00","€660.00");
		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertTrue(invoiceDetails.get(5).equals("Validated")|| invoiceDetails.get(5).equals("VALIDATED"));
			Assert.assertTrue(ExpectedAmountWOTaxValue.contains(invoiceDetails.get(6)));
			Assert.assertTrue(invoiceDetails.get(8).contains(""));
		}
		else {
			invoiceType = invoiceDetails.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertTrue(invoiceDetails.get(7).equals("Validated")|| invoiceDetails.get(7).equals("VALIDATED"));
			Assert.assertTrue(ExpectedAmountWOTaxValue.contains(invoiceDetails.get(8)));
			Assert.assertTrue(invoiceDetails.get(9).contains(""));
		}

		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc,null,null,null);
		billingrunTests.goToBillingRunDetails();
		// filter by customer 1
		billingrunTests.searchForInvoice(null,custCode1,null,null,null,null,version);
		invoiceDetails = billingrunTests.getInvoiceGeneratedDetails(1, version);
		ExpectedAmountWOTaxValue = List.of("EUR 330.00","€330.00");
		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertTrue(invoiceDetails.get(5).equals("Canceled")|| invoiceDetails.get(5).equals("CANCELED"));
			Assert.assertTrue(ExpectedAmountWOTaxValue.contains(invoiceDetails.get(6)));
			Assert.assertEquals(invoiceDetails.get(8), "");
		}
		else {
			invoiceType = invoiceDetails.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertTrue(invoiceDetails.get(7).equals("Canceled")|| invoiceDetails.get(7).equals("CANCELED"));
			Assert.assertTrue(ExpectedAmountWOTaxValue.contains(invoiceDetails.get(8)));
			Assert.assertTrue(invoiceDetails.get(9).contains(""));
		}
	}
	
	/***   check Rated Items Status : OPEN  ***/
	@Test(priority = 20, enabled = true)
	public void checkRatedItemsStatusOPEN(){
		homeTests.goToRatedItemsPage();
		ratedItemsTests.AddfilterBySubscription();
		ratedItemsTests.filterBySubscription(subCode1);
		Assert.assertEquals(ratedItemsTests.elementsFoundSize("CANCELED"), 0);
		Assert.assertEquals(ratedItemsTests.elementsFoundSize("BILLED"), 0);
		if(version.equals("14.1.X")) {
			Assert.assertEquals(ratedItemsTests.elementsFoundSize("OPEN"), 20);
		}else {
			Assert.assertEquals(ratedItemsTests.elementsFoundSize("OPEN"), 25);
		}
		
	}
}
