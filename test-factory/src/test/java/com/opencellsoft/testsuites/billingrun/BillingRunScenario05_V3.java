package com.opencellsoft.testsuites.billingrun;

import java.awt.AWTException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.opencellsoft.utility.Constant;

public class BillingRunScenario05_V3  extends BillingRunCommons {
	BillingRunDataSet dataSet = new BillingRunDataSet();
	String BC_code        = "BRBC5V3-" + Constant.offerCode;
	String customerCode   = "BRCUST5V3-" + Constant.offerCode;
	String subCode        = "BRSUB5V3_"+Constant.offerCode ;
	String AP             = "BRAP5V3_"+Constant.offerCode;
	String ExceptionalRunId;



	@Test(priority = 0, enabled = true)
	public void _BR_T05V3_exceptionalRunAutomatic() {
		System.out.println("***************** 5 (V3)- Exceptional run  : automatic  *****************");
	}


	/***  Configure V3 ***/
	@Test(priority = 4, enabled = false)
	public void _BR_T05V3_ConfigureV3() throws InterruptedException{
		
		String dbURI = baseURI.replace("/opencell/", "/db/");
		postgreeTests.openPostgree(dbURI);
		postgreeTests.login();
		postgreeTests.goToRequeteSql();
		String request = "update \"meveo_job_instance\" set job_template = 'InvoicingJobV3' where code = 'Invoicing_Job_V2'";
		postgreeTests.executeRequeteSql(request);
		postgreeTests.closeTab();
//		payloadTests.delete_Invocing_Job_V2(baseURI, login, password);
//		payloadTests.create_Invoicing_Job_V3(baseURI, login, password);
		payloadTests.update_invoiceLinesJob(baseURI, login, password);
	}
	
	/***   Create and Activate a new Subscription   ***/
	@Test(priority = 4, enabled = true)
	public void _BR_T05V3_createAndActivateSubscription() throws AWTException, InterruptedException{
		payloadTests.createBillingCycleByAPI(baseURI, login, password,BC_code,BC_code,"BILLINGACCOUNT","MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,BC_code,BC_code,"BILLINGACCOUNT","CAL_PERIOD_MONTHLY");
		
		payloadTests.createCustomer(baseURI, login, password, customerCode, customerCode, BC_code);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);
	}

	/***   Insert EDRs   ***/
	@Test(priority = 6, enabled = true)
	public void _BR_T05V3_insertChargeCdr() throws InterruptedException {
		int result;
		for(int i = 0; i<5; i++) {
			result = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "",subCode,null);
			Assert.assertEquals(result, 1);
		}
		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   create exceptional Run   ***/
	@Test(priority = 10, enabled = true)
	public void _BR_T05V3_createExceptionalRun() throws InterruptedException, JsonMappingException, JsonProcessingException{
		List<List<String>> filterFields = new ArrayList<>() ;
		List<String> firstField = new ArrayList<>();
		firstField.add("billingAcountCode");
		firstField.add("equal to");
		firstField.add(customerCode);
		filterFields.add(firstField);
		// Create exceptional run
		String invoiceDate =  LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		ExceptionalRunId = "" + payloadTests.createExceptionalRunByAPI(baseURI, login, password,"AUTOMATIC",invoiceDate,null,null,null,"MANUAL_ACTION","AUTOMATIC_VALIDATION",null,null,null,null, filterFields);
		System.out.println("ExceptionalRunId  : " + ExceptionalRunId);
	}

	/***   process Billing Run   ***/
	@Test(priority = 12, enabled = true)
	public void _BR_T05V3_processBillingRun(){
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(ExceptionalRunId,null,"NEW","EXCEPTIONAL",null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.processBillingRun();
	}

	/***   Check Billing Run Status  ***/
	@Test(priority = 14, enabled = true)
	public void _BR_T05V3_checkBRStatus() throws InterruptedException{
		dataSet.checkBRStatusExceptional(ExceptionalRunId,"DRAFT INVOICES");
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "DRAFT INVOICES");
	}

	/***   check Billing Run List   ***/
	@Parameters()
	@Test(priority = 16, enabled = true)
	public void _BR_T05V3_processAndCheckBillingRunStatus() throws InterruptedException{
		billingrunTests.processBillingRun();
		dataSet.checkBRStatusExceptional(ExceptionalRunId,"VALIDATED");
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "VALIDATED");
	}

	/***   Check Invoice Generated  ***/
	@Parameters()
	@Test(priority = 18, enabled = true)
	public void _BR_T05V3_checkInvoiceGenerated(){
		List<String> invoiceDetails1 = billingrunTests.getInvoiceGeneratedDetails(1, version);
		String invoiceType;
		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails1.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(5), "VALIDATED");
			Assert.assertEquals(invoiceDetails1.get(6), "€80.00");
		}else {
			invoiceType = invoiceDetails1.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(7), "VALIDATED");
			Assert.assertEquals(invoiceDetails1.get(8), "€80.00");
		}
	}

	/***   check Billing Run List  ***/
	@Parameters()
	@Test(priority = 20, enabled = true)
	public void _BR_T05V3_checkBillingRunList(){
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(ExceptionalRunId,null,null,null,null);
		if(version.equals("14.1.X")){
			// check billing cycle
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(1), ""); 
			// check run type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(2), "EXCEPTIONAL");
			// check split level
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(3), "");
			// check process type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(5), "AUTOMATIC");
			// check status
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(6), "VALIDATED");
			// check accounts
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(8), "1");
			// check invoices
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(9), "1");
			// check amount without tax
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(10), "€80.00");
		}
		else {
			// check billing cycle
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(1), ""); 
			// check run type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(2), "EXCEPTIONAL");
			// check split level
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(3), "");
			// check process type
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(5), "AUTOMATIC");
			// check status
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(6), "VALIDATED");
			// check accounts
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(9), "1");
			// check invoices
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(10), "1");
			// check amount without tax
			Assert.assertEquals(billingrunTests.getBillingRunDetailsFromList().get(11), "€80.00");
		}
	}

	/***   check Rated Items Status : BILLED  ***/
	@Test(priority = 22, enabled = true)
	public void _BR_T05V3_checkRatedItemsStatus(){
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		// Go to rated items tab
		customersTests.GoToRatedItemsTab(subCode);
		waitPageLoaded();
		Assert.assertEquals(ratedItemsTests.elementsFoundSize("OPEN"), 0);
		Assert.assertEquals(ratedItemsTests.elementsFoundSize("BILLED"), 6);
	}
}
