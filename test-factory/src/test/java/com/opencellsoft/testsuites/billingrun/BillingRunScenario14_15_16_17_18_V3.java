package com.opencellsoft.testsuites.billingrun;

import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.tests.InvoicesTests;
import com.opencellsoft.utility.Constant;

public class BillingRunScenario14_15_16_17_18_V3  extends BillingRunCommons {
	BillingRunDataSet dataSet = new BillingRunDataSet();
//	String code = Constant.offerCode;
	String code = "RDS4218";
	String bc1      = "BRBC14V3-" + code;
	String custCode = "BRCUST14V3-" + code + "-001"; 
	String custDesc = "BRCUST14V3_" + code + "_001";

	String subCode = null;

	@Test(priority = 0, enabled = true)
	public void _BR_T14_InvoiceValidation_ActionOnSuspectQuarantine(){
		System.out.println("***************** 14 (V3) - Invoice validation : Action on suspect  = Quarantine   *****************");
	}

	/***  Configure V3 ***/
	@Test(priority = 2, enabled = false)
	public void _BR_T14_ConfigureV3() throws InterruptedException{
		
		String dbURI = baseURI.replace("/opencell/", "/db/");
		postgreeTests.openPostgree(dbURI);
		postgreeTests.login();
		postgreeTests.goToRequeteSql();
		String request = "update \"meveo_job_instance\" set job_template = 'InvoicingJobV3' where code = 'Invoicing_Job_V2'";
		postgreeTests.executeRequeteSql(request);
		postgreeTests.closeTab();
//		payloadTests.delete_Invocing_Job_V2(baseURI, login, password);
//		payloadTests.create_Invoicing_Job_V3(baseURI, login, password);
		payloadTests.update_invoiceLinesJob(baseURI, login, password);
	}
	
	/***   Configure invoice validation rule   ***/
	@Test(priority = 4, enabled = true)
	public void _BR_T14_configureInvoiceValidationRule() {
		// Go to invoice validation page
		homeTests.goToInvoiceValidationPage();
		// select com invoice 
		invoiceValidationTests.goToComInvoice();
		// set (rule : suspect), (scriptException : compareInvoiceAmmountScript ( amountWithoutTaw,>, 500) )
		try {
			invoiceValidationTests.updateInvoiceValidationRule1("validationRule1","suspect", "500", version);
		}catch (Exception e) {
			invoiceValidationTests.setInvoiceValidationRule1("validationRule1","suspect", "500", version);
		}
	}
	
	/***   Create data  ***/
	@Test(priority = 6, enabled = true)
	public void _BR_T14_createSeveralCustomersAndSubscriptions() throws InterruptedException {
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bc1,bc1,"BILLINGACCOUNT", "MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bc1,bc1,"BILLINGACCOUNT", "CAL_PERIOD_MONTHLY");

		for(int i = 1; i < 11; i++) {
			custCode = "BRCUST14V3-"+ code +"-00"+i;
			custDesc = "BRCUST14V3_"+ code +"_00"+i;
			subCode  = "BRSUB14V3-"+ code +"-00"+i;
			payloadTests.createCustomer(baseURI, login, password, custCode,custDesc , bc1);
			Thread.sleep(500);
			payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, custCode, dataSet.offerCode, dataSet.productCode);
			Thread.sleep(500);
			payloadTests.activateServices(baseURI, login, password, subCode, dataSet.productCode);
			Thread.sleep(500);
			payloadTests.createAccessPoint(baseURI, login, password, subCode, subCode);
			Thread.sleep(1000);
			payloadTests.insertSeveralChargeCdr(baseURI, login, password, subCode, "p1", "p2", "p3");
		}
		// add other sub for the last customer => goal : generate validated invoice
		subCode = "BRSUB14V3-"+Constant.offerCode+"-00100";
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, custCode, dataSet.offerCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, dataSet.productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, subCode, subCode);
		Thread.sleep(1000);
		payloadTests.insertSeveralChargeCdr(baseURI, login, password, subCode, "p1", "p2", "p3");

		jobsTests.runRTJob(baseURI, login, password);
	}
	
	/***   Create Cycle run (Action en suspect = Quarantine)   ***/
	@Test(priority = 10, enabled = true)
	public void _BR_T14_createCycleRun() {
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.createCycleRun(bc1,"AUTOMATIC","nextMonth",false,"Quarantine","Quarantine",null,null,null);
	}

	/***   Process BR   ***/
	@Test(priority = 12, enabled = true)
	public void _BR_T14_processBR() {
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"NEW",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.processBillingRun();
	}

	/***   check BR status    ***/
	@Test(priority = 14, enabled = true)
	public void _BR_T14_checkBillingRunStatus() throws InterruptedException {
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"DRAFT INVOICES",null,null);
		try {
			billingrunTests.goToBillingRunDetails();
		}
		catch (Exception e) {
			Thread.sleep(10000);
			homeTests.goToBillingRunPage();
			billingrunTests.searchForBillingRun(null,bc1,"DRAFT INVOICES",null,null);
			billingrunTests.goToBillingRunDetails();
		}
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "DRAFT INVOICES");
	}

	/***   Check invoices generated(status = DRAFT)   ***/
	@Test(priority = 16, enabled = true)
	public void _BR_T14_checkInvoicesGenerated() {
		List<String> invoiceDetails1 = billingrunTests.getInvoiceGeneratedDetails(1,version);
		String invoiceType;
		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails1.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(5), "DRAFT");
			Assert.assertEquals(invoiceDetails1.get(6), "€660.00");
		}
		else {
			invoiceType = invoiceDetails1.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(7), "DRAFT");
			Assert.assertEquals(invoiceDetails1.get(8), "€660.00");
		}
	}

	/***   check the creation of a new BR with invoices generated(status = SUSPECT, rejected by rule =  invoice validation rule)   ***/
	@Test(priority = 18, enabled = true)
	public void _BR_T14_checkRejectedBR() {
		//   Check BR status (REJECTED) 
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"REJECTED",null,null);
		billingrunTests.goToBillingRunDetails();
	}

	/***   Check invoice details (Number = Draft, Rejected by =invoice validation rule)   ***/
	@Test(priority = 20, enabled = true)
	public void _BR_T14_checkInvoiceDetails() {
		String invoiceNumber = null;
		if(version.equals("14.1.X")) { invoiceNumber = billingrunTests.getInvoiceNumber();}
		else { invoiceNumber = billingrunTests.getInvoiceDetails("3");}
		//		Assert.assertEquals(invoiceNumber,"Draft" ); 
		billingrunTests.goToInvoicePage(2);

		InvoicesTests invoicesTests = new InvoicesTests();
		//		Assert.assertEquals(invoicesTests.getInvoiceNumber(), invoiceNumber); 
		Assert.assertTrue(invoicesTests.getInvoiceType().equals("Commercial invoice") || invoicesTests.getInvoiceType().equals( "Invoice") );
		Assert.assertEquals(invoicesTests.getInvoiceStatus(), "SUSPECT");
		Assert.assertEquals(invoicesTests.getRejectionByRule(), "validationRule1");
		invoicesTests.goBack();
	}

	/***   Cancel Billing Run With Reopen RTs ***/
	@Parameters()
	@Test(priority = 22, enabled = true)
	public void _BR_T15_CancelBillingRunWithReopenRTs(){
		System.out.println("***************** 15 (V3) - Invoice validation : Action on suspect  = NULL   *****************");
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"REJECTED",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.cancelBillingRun();
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"DRAFT INVOICES",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.cancelBillingRun();
	}

	/***   Create Cycle run (Action en suspect = Quarantine)   ***/
	@Test(priority = 24, enabled = true)
	public void _BR_T15_createCycleRun() {
		// Go to billing run page
		homeTests.goToBillingRunPage();
		// create Billing run
		billingrunTests.createCycleRun(bc1,"AUTOMATIC","nextMonth",false,null,null,null,null,null);
	}

	/***   Process BR   ***/
	@Test(priority = 26, enabled = true)
	public void _BR_T15_processBR() {
		// Go to billing run page
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"NEW",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.processBillingRun();
	}

	/***   check BR status   ***/
	@Test(priority = 28, enabled = true)
	public void _BR_T15_checkBillingRunStatus() {
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"DRAFT INVOICES",null,null);
		billingrunTests.goToBillingRunDetails();
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "DRAFT INVOICES");
	}

	/***   Check invoices generated  ***/
	@Test(priority = 30, enabled = true)
	public void _BR_T15_checkInvoicesGenerated() {
		billingrunTests.sortByStatus();
		billingrunTests.sortByStatus();

		List<String> invoiceDetails1 = billingrunTests.getInvoiceGeneratedDetails(1, version);
		String invoiceType;
		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails1.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(5), "DRAFT");
			Assert.assertEquals(invoiceDetails1.get(6), "€660.00");
		}
		else {
			invoiceType = invoiceDetails1.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(7), "DRAFT");
			Assert.assertEquals(invoiceDetails1.get(8), "€660.00");
		}

		List<String> invoiceDetails2 = billingrunTests.getInvoiceGeneratedDetails(2, version);
		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails2.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails2.get(5), "SUSPECT");
			Assert.assertEquals(invoiceDetails2.get(6), "€330.00");
			Assert.assertEquals(invoiceDetails2.get(8), "validationRule1");
		}
		else {
			invoiceType = invoiceDetails2.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails2.get(7), "SUSPECT");
			Assert.assertEquals(invoiceDetails2.get(8), "€330.00");
			Assert.assertTrue(invoiceDetails2.get(9).contains("validationRule1"));
		}
	}

	/***   Check invoice details (Number = Draft, Rejected by =invoice validation rule)   ***/
	@Test(priority = 32, enabled = false)
	public void _BR_T15_invoiceDetails() {
		//		String invoiceNumber = null;
		//		if(version.equals("14.1.X")) { invoiceNumber = billingrunTests.getInvoiceNumber();}
		//		else { invoiceNumber = billingrunTests.getInvoiceDetails("3");}
		billingrunTests.goToInvoicePage(2);
		InvoicesTests invoicesTests = new InvoicesTests();
		//		Assert.assertEquals(invoicesTests.getInvoiceNumber(), invoiceNumber); 
		Assert.assertTrue(invoicesTests.getInvoiceType().equals("Commercial invoice") || invoicesTests.getInvoiceType().equals( "Invoice") );
		Assert.assertTrue(invoicesTests.getRejectionByRule().contains("validationRule1"));
		//		Assert.assertEquals(invoicesTests.getRejectionByRule(), "validationRule1");
		invoicesTests.goBack();
	}

	/***   filter invoices by Customer   ***/
	@Test(priority = 34, enabled = true)
	public void _BR_T16_filterByCustomer() {
		System.out.println("***************** 16 (V3) - Validate invoices manually (Validate filter)   *****************");
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"DRAFT INVOICES",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.searchForInvoice(null,custDesc,null,null,null,null,version);
		Assert.assertEquals(billingrunTests.filteredInvoicesNumber(), 1);
	}

	/***   validate filtered invoices   ***/
	@Test(priority = 36, enabled = true)
	public void _BR_T16_validateFilteredInvoices() {
		billingrunTests.validateFilteredInvoices();
	}

	/***   Check invoices validated  ***/
	@Test(priority = 38, enabled = true)
	public void _BR_T16_checkInvoicesValidated() {
		billingrunTests.searchForInvoice(null,custDesc,null,null,null,null,version);
		Assert.assertEquals(billingrunTests.filteredInvoicesNumber(), 1);

		List<String> invoiceDetails1 = billingrunTests.getInvoiceGeneratedDetails(1, version);
		String invoiceType;
		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails1.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(5), "VALIDATED");
			Assert.assertEquals(invoiceDetails1.get(7), "");
		}
		else {
			invoiceType = invoiceDetails1.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails1.get(7), "VALIDATED");
			Assert.assertEquals(invoiceDetails1.get(9), "");
		}

	}

	/***   filter invoices by Customer   ***/
	@Test(priority = 40, enabled = true)
	public void _BR_T17_selectInvoices() {
		System.out.println("***************** 17 (V3) - Validate invoices manually (Validate selected liste)   *****************");
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"DRAFT INVOICES",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.sortByCustomer();
		billingrunTests.selectinvoice(1);
		billingrunTests.selectinvoice(2);

	}	

	/***   validate filtered invoices   ***/
	@Test(priority = 42, enabled = true)
	public void _BR_T17_validateSelectedInvoices() {
		billingrunTests.validateSelectedInvoices();
	}


	/***   Check invoices validated  ***/
	@Test(priority = 44, enabled = true)
	public void _BR_T17_checkInvoicesValidated2() {
		billingrunTests.searchForInvoice(null,null,null,"VALIDATED",null,null,version);
		Assert.assertEquals(billingrunTests.filteredInvoicesNumber(), 3);

	}

	/***   Check invoices validated details ***/
	@Test(priority = 46, enabled = true)
	public void _BR_T17_checkInvoicesValidatedDetails() {
		String invoiceType;

		List<String> invoiceDetails = billingrunTests.getInvoiceGeneratedDetails(1, version);

		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails.get(5), "VALIDATED");
			Assert.assertEquals(invoiceDetails.get(7), "");
		}
		else {
			invoiceType = invoiceDetails.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails.get(7), "VALIDATED");
			Assert.assertEquals(invoiceDetails.get(9), "");
		}

		invoiceDetails = billingrunTests.getInvoiceGeneratedDetails(2, version);

		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails.get(5), "VALIDATED");
			Assert.assertEquals(invoiceDetails.get(7), "");
		}
		else {
			invoiceType = invoiceDetails.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails.get(7), "VALIDATED");
			Assert.assertEquals(invoiceDetails.get(9), "");
		}

		invoiceDetails = billingrunTests.getInvoiceGeneratedDetails(3, version);

		if(version.equals("14.1.X")) {
			invoiceType = invoiceDetails.get(2);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails.get(5), "VALIDATED");
			Assert.assertEquals(invoiceDetails.get(7), "");
		}
		else {
			invoiceType = invoiceDetails.get(4);
			Assert.assertTrue( invoiceType.equals("Commercial invoice") || invoiceType.equals("Invoice"));
			Assert.assertEquals(invoiceDetails.get(7), "VALIDATED");
			Assert.assertEquals(invoiceDetails.get(9), "");
		}

	}

	/***   filter invoices by status   ***/
	@Test(priority = 48, enabled = true)
	public void _BR_T18_filterByStatus() {
		System.out.println("***************** 18 (V3) - Validate invoices manually (Validate all)   *****************");
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"DRAFT INVOICES",null,null);
		billingrunTests.goToBillingRunDetails();
		billingrunTests.searchForInvoice(null,null,null,"SUSPECT",null,null,version);
		Assert.assertEquals(billingrunTests.filteredInvoicesNumber(), 7);
	}

	/***   validate filtered invoices   ***/
	@Test(priority = 50, enabled = true)
	public void _BR_T18_validateFilteredInvoices() {
		billingrunTests.validateFilteredInvoices();
	}

	/***   Process BR   ***/
	@Test(priority = 52, enabled = true)
	public void _BR_T18_processBR() {
		billingrunTests.processBillingRun();
	}

	/***   check BR status   ***/
	@Test(priority = 54, enabled = true)
	public void _BR_T18_checkBillingRunStatus() {
		homeTests.goToBillingRunPage();
		billingrunTests.searchForBillingRun(null,bc1,"VALIDATED",null,null);
		billingrunTests.goToBillingRunDetails();
		Assert.assertEquals(billingrunTests.getBillingRunStatus(), "VALIDATED");
	}
}
