package com.opencellsoft.testsuites.accountingperiodmanager;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

import org.apache.poi.ss.usermodel.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.tests.AccountingPeriodTests;
import com.opencellsoft.tests.CustomersTests;
import com.opencellsoft.tests.HomeTests;
import com.opencellsoft.tests.InvoicesTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.tests.admin.PostgreeTests;
import com.opencellsoft.tests.jobs.JobsTests;
import com.opencellsoft.tests.jobs.PayloadTests;
import com.opencellsoft.utility.Constant;

public class APMTestsCommons extends TestBase {

	LoginTest loginTest;
	ArrayList<String> tabs;
	HomeTests homeTests;
	AccountingPeriodTests accountingPeriodTests;
	CustomersTests customersTests;
	InvoicesTests invoicesTests;
	String invoiceNumber1, invoiceNumber2, invoiceNumber3;
	JobsTests jobsTests;
	PayloadTests payloadTests;
	PostgreeTests postgreeTests;
	

	String customerCode = "APMCUST-001" + Constant.customerCode;
	String consumerCode = "APMCONS-001" + Constant.consumerCode;
	String customerName = customerCode;
	String customerEmail = Constant.customerEmail;
	String paypalUserId = Constant.paypalUserId;
	
	String consumerName = Constant.consumerName;
	String companyRegistrationNumber = Constant.companyRegistrationNumber;
	String vatNumber = Constant.vatNumber;
	String articleCode = "ART-STD";
	String version;
	String baseURI; 
	String login;
	String password;
	
	public APMTestsCommons() {
		loginTest = new LoginTest();
		homeTests = new HomeTests();
		accountingPeriodTests = new AccountingPeriodTests();
		customersTests = new CustomersTests();
		invoicesTests = new InvoicesTests();
	}
	
	/***  setup  ***/
	@BeforeClass
	@Parameters(value = {"baseURI", "browser", "nodeURL", "login", "password", "version" })
	public void setup(String baseURI, String browser, String nodeURL, String login, String password, String version) throws Exception{
		initialize( browser, nodeURL, baseURI); 
		this.version = version;
		this.baseURI = baseURI; 
		this.login = login;
		this.password = password;
		
		loginTest.loginTest(login, password);
		homeTests.goToPortal();
	}

	/***  Teardown  ***/
	@AfterClass
	public void Teardown() {
		TeardownTest();
	}

	public String getCurrentDate() {
		// Get the current date
		LocalDate currentDate = LocalDate.now();

		// Create a date formatter with the desired format
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		System.out.println(currentDate.format(formatter));
		// Format the current date using the formatter
		return currentDate.format(formatter);
	}

	public String getTheFirstOfTheCurrentMonth() {
		LocalDate today = LocalDate.now();
		LocalDate firstDayOfMonth = today.withDayOfMonth(1);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String formattedDate = firstDayOfMonth.format(formatter);

		return formattedDate;
	}

	public String getTheLastOfTheCurrentMonth() {
		LocalDate today = LocalDate.now();
		LocalDate lastDayOfMonth = YearMonth.from(today).atEndOfMonth();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String formattedDate = lastDayOfMonth.format(formatter);

		return formattedDate;
	}
	
	public int getTheCurrentMonth() {
		LocalDate currentDate = LocalDate.now();
		int currentMonth = currentDate.getMonthValue();
		return currentMonth;
	}
	
	public String getTheFirstOfThePreviousMonthBeforeCurrentDate(int i) {
		LocalDate today = LocalDate.now();
		LocalDate MonthBefore = today.minus(i, ChronoUnit.MONTHS);
		LocalDate firstDayOfMonth = MonthBefore.withDayOfMonth(1);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String formattedDate = firstDayOfMonth.format(formatter);

		return formattedDate;
	}

	public String getTheFirstOfTheNextMonthAfterCurrentDate(int i) {
		LocalDate today = LocalDate.now();
		LocalDate MonthAfter = today.plus(i, ChronoUnit.MONTHS);
		LocalDate firstDayOfMonth = MonthAfter.withDayOfMonth(1);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String formattedDate = firstDayOfMonth.format(formatter);

		return formattedDate;
	}

	public static void waitForTheFileToDownload(String fileName)
			throws IOException, InterruptedException {
		String home = System.getProperty("user.home");
		File dir = new File( home + "\\Downloads\\"); 
		System.out.println(home + "\\Downloads\\");

		for(int i = 0 ; i<3; i++) {
			File[] dirContents = dir.listFiles();
			if (dirContents != null) {
				boolean fileFound = false;
				for (File file : dirContents) {
					if (file.isFile() && file.getName().equals(fileName)) {
						fileFound = true;
						break;
					}
				}
				if(!fileFound) {
					Thread.sleep(2000);
				}
			}
		}
	}

	/***  this function returns the last file modified from the list supplied as parameters  ***/
	private static File getLastDownloadedFile(File[] files) {
        if (files == null || files.length == 0) {
            return null;
        }
        // Sort files by last modified time
        Arrays.sort(files, Comparator.comparingLong(File::lastModified).reversed());

        // Return the first file in the sorted array (last downloaded file)
        return files[0];
	}

	/***  this function returns the file path of the last file downloaded  ***/
	public static String LastDownloadedFile() {
		String downloadFilePath = downloadFilePath();
		File downloadFile = new File(downloadFilePath);
		File[] files = downloadFile.listFiles();
		String filePath= null;
		if (files != null && files.length > 0) {
			File lastDownloadedFile = getLastDownloadedFile(files);
			if (lastDownloadedFile != null) {
				String localUrl = lastDownloadedFile.toURI().toString();
				System.out.println("Local URL of the last downloaded file: " + localUrl);
				System.out.println("File path: " + lastDownloadedFile.getAbsolutePath());
				filePath = lastDownloadedFile.getAbsolutePath();
				System.out.println("File path is : " + filePath );
			} else {
				System.out.println("No downloaded files found in the download directory.");
			}
		} else {
			System.out.println("The download directory is empty.");
		}
		return filePath;
	}

	/***  this function reads a CSV file and copies it into a table with two dimensions  ***/
	public static String[][] csvReader(String FilePath, String split){
		String line;
		String cvsSplitBy = split;
		int rowCount = 0;
		int columnCount = 0;
		String[][] table = null;

		try{
			BufferedReader br = new BufferedReader(new FileReader(FilePath));
			// Count the number of rows and columns
			while ((line = br.readLine()) != null) {
				rowCount++;
				String[] cells = line.split(cvsSplitBy);
				columnCount = Math.max(columnCount, cells.length);
			}

			// Initialize the table with the correct dimensions
			table = new String[rowCount][columnCount];

			// Read the CSV file again to populate the table
			br.close();
			br = new BufferedReader(new FileReader(FilePath));
			int row = 0;
			while ((line = br.readLine()) != null) {
				String[] cells = line.split(cvsSplitBy);
				for (int col = 0; col < cells.length; col++) {
					table[row][col] = cells[col].replace("\"", "");
				}
				row++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return table;
	}

	/***  this function reads a XLSX file and copies it into a table with two dimensions  ***/
	public static Object[][] xlsxReader(String filePath) throws IOException{
		Object[][] table = null;
		try (Workbook workbook = WorkbookFactory.create(new FileInputStream(filePath))) {
			Sheet sheet = workbook.getSheetAt(0);

			int rows = sheet.getLastRowNum() + 1;
			int columns = 0;

			for (Row row : sheet) {
				int lastColumn = row.getLastCellNum();
				if (lastColumn > columns) {
					columns = lastColumn;
				}
			}

			table = new Object[rows][columns];

			for (int r = 0; r < rows; r++) {
				Row row = sheet.getRow(r);
				if (row != null) {
					for (int c = 0; c < columns; c++) {
						Cell cell = row.getCell(c);
						if (cell != null) {
							CellType cellType = cell.getCellType();
							if (cellType == CellType.STRING) {
								table[r][c] = cell.getStringCellValue();
							} else if (cellType == CellType.NUMERIC) {
								table[r][c] = cell.getNumericCellValue();
							} else if (cellType == CellType.BOOLEAN) {
								table[r][c] = cell.getBooleanCellValue();
							} else if (cellType == CellType.BLANK) {
								table[r][c] = "";
							}
						}
					}
				}
			}    
		} catch (IOException e) {
			e.printStackTrace();
		}
		return table;
	}
	
	public static String downloadFilePath() {
		String os = System.getProperty("os.name").toLowerCase();
		String userHome = System.getProperty("user.home");

		String downloadPath = null;

		if (os.contains("win")) {
			downloadPath = userHome + "\\Downloads";
		} else if (os.contains("mac")) {
			downloadPath = userHome + "/Downloads";
		} else if (os.contains("nix") || os.contains("nux") || os.contains("sunos")) {
			downloadPath = userHome + "/Downloads";
		}

		return downloadPath;
	}
	
	public String getRandomNumberbetween1And200() {
		int min = 1;
        int max = 200;

        Random rand = new Random();
        int randomNumber = rand.nextInt(max - min + 1) + min;
        return  String.format("%04d", randomNumber);
	}
}
