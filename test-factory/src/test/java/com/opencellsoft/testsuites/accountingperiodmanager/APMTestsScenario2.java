package com.opencellsoft.testsuites.accountingperiodmanager;

import org.testng.Assert;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.tests.admin.PostgreeTests;
import com.opencellsoft.tests.jobs.JobsTests;

public class APMTestsScenario2 extends APMTestsCommons {
	APMTestsDataSet dataSet ;
	String request0 = "update  \"ar_account_operation\" set operation_number = null, invoice_id = null;\r\n"
			+ "delete from \"billing_rated_transaction\" ;\r\n"
			+ "delete from \"billing_invoice_line\" ;\r\n"
			+ "delete from \"billing_invoices_subscriptions\" ;\r\n"
			+ "delete from \"billing_invoice\" ;\r\n"
			+ "delete from \"accounting_journal_entry\" ;\r\n"
			+ "delete from \"ar_account_operation\" ;";
	
	String fileName = "finance_accounting-period_accounting-operations-report";
	String companyRegistration = getRandomNumberbetween1And200();

	

	@Test(priority = 0, enabled = true)
	public void _APM_T2() {
		System.out.println("**************************      Scenario 2 : Add invoices and check accounting period report / Balance  ******************************");

	}
	
	/***   Go to Postgree   ***/
	@Parameters({"baseURI" })
	@Test(priority = 3, enabled = true)
	public void _APM_T2_resetDB(String baseURI) {
		String dbURI = baseURI.replace("/opencell/", "/db/");
		PostgreeTests postgreeTests = new PostgreeTests();
		postgreeTests.openPostgree(dbURI);
		postgreeTests.login();
		postgreeTests.goToRequeteSql();
		postgreeTests.executeRequeteSql(request0);
		postgreeTests.closeTab();
	}
	
	/***   Create customer   ***/
	@Parameters({ "billingCountry", "billingCycle", "acrs" })
	@Test(priority = 4, enabled = true)
	public void _APM_T2_createCustomer(String billingCountry, String billingCycle, String acrs) throws InterruptedException {
		// Create customer
		homeTests.goToCustomTablesListPage();
		customersTests.createCustomerCheck(customerCode, customerName, customerEmail, billingCountry,  null,companyRegistration , version);
		// Create consumer
		customersTests.createConsumerMethod(consumerCode,consumerName, companyRegistrationNumber,vatNumber, acrs , companyRegistration, version);
	}

	/***   Create invoice   ***/
	@Test(priority = 6, enabled = true)
	public void _APM_T2_createInvoiceInThePast() throws AWTException {
		
		// Go To Invoice Tab
		try {
			customersTests.goToInvoiceTab();
		}catch (Exception e) {
			// Go to customers list page
			homeTests.goToCustomTablesListPage();
			
			// search for a customer and go to customer details page
			customersTests.searchForCustomerMethod(customerCode);
			
			customersTests.goToInvoiceTab();
		}
		
		// Create invoice
		invoicesTests.createCommercialInvoice();
		// Create invoice line
		invoicesTests.createInvoiceLine( articleCode, "1111", "4", "20");
		// update Invoice
		invoicesTests.putTheInvoiceInThePast(4);
		// Validate invoice
		invoiceNumber1 = invoicesTests.validateTheInvoice();
	}	

	/***   Create invoice   ***/
	@Test(priority = 8, enabled = true)
	public void _APM_T2_createInvoiceInPresente() throws AWTException {
		// Create invoice
		invoicesTests.createCommercialInvoice();
		// Create invoice line
		invoicesTests.createInvoiceLine( articleCode, "1111", "5", "20");
		// Validate invoice
		invoiceNumber2 = invoicesTests.validateTheInvoice();
	}	

	/***   Create invoice   ***/
	@Test(priority = 10, enabled = true)
	public void _APM_T2_createInvoiceInTheFutur() throws AWTException {
		// Create invoice
		invoicesTests.createCommercialInvoice();
		// Create invoice line
		invoicesTests.createInvoiceLine( articleCode, "1111", "6", "20");
		// update Invoice
		invoicesTests.putTheInvoiceInThefutur(1);
		
		// Validate invoice
		invoiceNumber3 = invoicesTests.validateTheInvoice();
	}

	/***   Check accounting date in balance (current date )   ***/
	@Test(priority = 12, enabled = true)
	public void _APM_T2_checkAccountingDateInBalance() throws AWTException {
		
//		homeTests.goToCustomTablesListPage();
//		customersTests.searchForCustomerMethod(customerCode);
		
		// Go to balance tab
		customersTests.goToBalanceTab();
		// Check accounting date in balance (current date )
		Assert.assertEquals(customersTests.getValueFromBalanceTable(2, 2), invoiceNumber1) ;
		Assert.assertEquals(customersTests.getValueFromBalanceTable(2, 5), getTheFirstOfTheCurrentMonth());
		Assert.assertEquals(customersTests.getValueFromBalanceTable(2, 12), "POSTED");
		
		Assert.assertEquals(customersTests.getValueFromBalanceTable(3, 2), invoiceNumber2) ;
		Assert.assertEquals(customersTests.getValueFromBalanceTable(3, 5), getCurrentDate());
		Assert.assertEquals(customersTests.getValueFromBalanceTable(3, 12), "POSTED");
		
		Assert.assertEquals(customersTests.getValueFromBalanceTable(4, 2), invoiceNumber3) ;
		Assert.assertEquals(customersTests.getValueFromBalanceTable(4, 5), getTheFirstOfTheNextMonthAfterCurrentDate(1));
		Assert.assertEquals(customersTests.getValueFromBalanceTable(4, 12), "POSTED");
	}
	
	/***   Go to accounting period report and Check invoice status : POSTED  ***/
	@Test(priority = 14, enabled = true)
	public void _APM_T2_checkInvoiceStatusPOSTEDInAccountingPeriodReportPage() {
		// Go to accounting period report
		homeTests.goToAccountingPeriodReportPage();
		accountingPeriodTests.desableCurrentMonth();

		// Check invoice status : POSTED
		Assert.assertEquals(accountingPeriodTests.elementsFoundSize("posted"), 3);
	}

	/***   check referencing   ***/
	@Test(priority = 16, enabled = true)
	public void _APM_T2_checkReferencing() throws AWTException {
		//Go to invoice details by link reference
		accountingPeriodTests.goToReference(invoiceNumber1);
		Assert.assertEquals(invoicesTests.getInvoiceNumber(), invoiceNumber1); 
		// Go back to accounting period report
		if(version.equals("17.X")) {invoicesTests.navigateBack();}else {invoicesTests.goBack();}
		invoicesTests.goBack();
		accountingPeriodTests.desableCurrentMonth();
		accountingPeriodTests.goToReference(invoiceNumber2);
		Assert.assertEquals(invoicesTests.getInvoiceNumber(), invoiceNumber2); 
		// Go back to accounting period report
		invoicesTests.goBack();
		accountingPeriodTests.desableCurrentMonth();
		accountingPeriodTests.goToReference(invoiceNumber3);
		Assert.assertEquals(invoicesTests.getInvoiceNumber(), invoiceNumber3); 
		// Go back to accounting period report
		invoicesTests.goBack();
		accountingPeriodTests.desableCurrentMonth();
		Assert.assertEquals(accountingPeriodTests.elementsFoundSize("posted"), 3);
	}	

	/***   execute AccountingSchemesJob   ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 18, enabled = true)
	public void _APM_T2_runAccountingSchemesJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests = new JobsTests();
		jobsTests.runAccountingSchemesJob(baseURI, login, password);
	}

	/***   Go to accounting period report and check Invoice Status = EXPORTED  ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 20, enabled = true)
	public void _APM_T2_checkInvoiceStatusEXPORTEDInAccountingPeriodReportPage(String baseURI, String login, String password) throws InterruptedException {
		// Go to accounting period report
		homeTests.goToAccountingPeriodReportPage();
		accountingPeriodTests.desableCurrentMonth();

		// Check invoice status : EXPORTED
		try {
			Assert.assertEquals(accountingPeriodTests.elementsFoundSize("exported"), 3);	
		}catch (Exception e) {
			jobsTests.runAccountingSchemesJob(baseURI, login, password);
			waitPageLoaded();
			Assert.assertEquals(accountingPeriodTests.elementsFoundSize("exported"), 3);
		}
		
	}

	/***   Check Force posting = true   ***/
	@Test(priority = 22, enabled = true)
	public void _APM_T2_checkForcePosting() {
		// Check Force posting = true
		Assert.assertTrue(accountingPeriodTests.IsForcedPosting(2));
	}
	
	/***   Export CSV file (finance_accounting)    ***/
	@Test(priority = 24)
	public void _APM_T2_exportFinanceAccountingCSV() throws IOException, InterruptedException {
		accountingPeriodTests.downlaodFinanceAccountingCSV();
		waitForTheFileToDownload(fileName);
		accountingPeriodTests.downlaodFinanceAccountingCSV();
		waitForTheFileToDownload(fileName);
	}
	
	/***   Check file contents   ***/
	@Test(priority = 26)
	public void _APM_T2_checkFinanceAccountingCSVFile() {
		Object[][] table = csvReader(LastDownloadedFile(), ",");
		Assert.assertEquals(table[0][5], "Debit") ;
		Assert.assertEquals(table[1][5], "5332.8") ;
		Assert.assertEquals(table[2][5], "6666") ;
		Assert.assertEquals(table[3][5], "7999.2") ;
		
		Assert.assertEquals(table[0][6], "Credit") ;
		Assert.assertEquals(table[1][6], "0") ;
		Assert.assertEquals(table[2][6], "0") ;
		Assert.assertEquals(table[3][6], "0") ;
		
		Assert.assertEquals(table[0][8], "Forced Posting") ;
		Assert.assertEquals(table[1][8], "true") ;
		Assert.assertEquals(table[2][8], "false") ;
		Assert.assertEquals(table[3][8], "false") ;
	}	
	
	/***   Export XLSX file (finance_accounting)   ***/
	@Test(priority = 28)
	public void _APM_T2_exportFinanceAccountingXLSX() throws IOException, InterruptedException {
		accountingPeriodTests.downlaodFinanceAccountingXLSX();
		waitForTheFileToDownload(fileName);
	}	
	
	/***   Check file contents   
	 * @throws IOException ***/
	@Test(priority = 30)
	public void _APM_T2_checkFinanceAccountingXLSXFile() throws IOException {
		Object[][] table = xlsxReader(LastDownloadedFile());
		Assert.assertEquals(table[0][5].toString(), "Debit") ;
		Assert.assertEquals(table[1][5].toString(), "5332.8") ;
		Assert.assertEquals(table[2][5].toString(), "6666.0") ;
		Assert.assertEquals(table[3][5].toString(), "7999.2") ;
		
		Assert.assertEquals(table[0][6].toString(), "Credit") ;
		Assert.assertEquals(table[1][6].toString(), "0.0") ;
		Assert.assertEquals(table[2][6].toString(), "0.0") ;
		Assert.assertEquals(table[3][6].toString(), "0.0") ;
		
		Assert.assertEquals(table[0][8].toString(), "Forced Posting") ;
		Assert.assertEquals(table[1][8].toString(), "true") ;
		Assert.assertEquals(table[2][8].toString(), "false") ;
		Assert.assertEquals(table[3][8].toString(), "false") ;
	}	
}
