package com.opencellsoft.testsuites.accountingperiodmanager;

import org.testng.Assert;

import org.testng.annotations.Test;

public class APMTestsScenario1 extends APMTestsCommons {
	
	String previousPeriode  = "2023-2024";
	String actualPeriode  = "2024-2025";
	String nextPeriode  = "2025-2026";
	String firstMonthOfNextPeriod = "2025-09-01";	

	@Test(priority = 2, enabled = true)
	public void _APM_T1() throws Exception {
		System.out.println("**************************      Scenario 1 : Close and Open Accouting period & sub period  ******************************");
	}
	
	/***   Go to Acouting period manager   ***/
	@Test(priority = 4, enabled = true)
	public void _APM_T1_GoToAcoutingPeriodManager() throws Exception {
		homeTests.goToAccountingPeriodManagerPage();
	}
	
	/***   Check periods   ***/
	@Test(priority = 6, enabled = true)
	public void _APM_T1_CheckPeriods() throws Exception {
		waitPageLoaded();
		Assert.assertEquals(accountingPeriodTests.elementsFoundSize("open"), 2);
		Assert.assertEquals(accountingPeriodTests.elementsFoundSize("monthly"), 2);
		Assert.assertTrue(accountingPeriodTests.checkValueInAccoutingPeriodList(previousPeriode));
		Assert.assertTrue(accountingPeriodTests.checkValueInAccoutingPeriodList(actualPeriode));
	}
	
	/***   Check Sub-periods   ***/
	@Test(priority = 6, enabled = true)
	public void _APM_T1_CheckSubPeriods() throws Exception {
		accountingPeriodTests.showDetailsOfAccountingPeriod(1);
		Assert.assertEquals(accountingPeriodTests.subPeriodOpenSize(), 12);
		Assert.assertEquals(accountingPeriodTests.subPeriodCloseSize(), 0);
	}
	
	/***   Close period   ***/
	@Test(priority = 8, enabled = true)
	public void _APM_T1_ClosePeriod() throws Exception {
		accountingPeriodTests.closePeriod(1);
	}
	
	/***   Check period status CLOSED   ***/
	@Test(priority = 10, enabled = true)
	public void _APM_T1_CheckPeriodStatusCLOSED() throws Exception {
		Assert.assertEquals(accountingPeriodTests.elementsFoundSize("open"), 1);
		Assert.assertEquals(accountingPeriodTests.elementsFoundSize("closed"), 1);
	}
	
	/***   Check sub-periods status CLOSED   ***/
	@Test(priority = 12, enabled = true)
	public void _APM_T1_CheckSubPeriodsStatusCLOSED() throws Exception {
		Assert.assertEquals(accountingPeriodTests.subPeriodOpenSize(), 0);
		Assert.assertEquals(accountingPeriodTests.subPeriodCloseSize(), 12);
	}
	
	
	/***   Reopen period   ***/
	@Test(priority = 14, enabled = true)
	public void _APM_T1_ReopenPeriod() throws Exception {
		accountingPeriodTests.reopenPeriod(1);
	}
	
	/***   Check period status CLOSED   ***/
	@Test(priority = 16, enabled = true)
	public void _APM_T1_CheckPeriodStatusOpenAgain() throws Exception {
		Assert.assertEquals(accountingPeriodTests.elementsFoundSize("open"), 2);
		Assert.assertEquals(accountingPeriodTests.elementsFoundSize("closed"), 0);
	}
	
	/***   Check sub-periods status CLOSED   ***/
	@Test(priority = 18, enabled = true)
	public void _APM_T1_CheckSubPeriodsStatusStillCLOSED() throws Exception {
		try {
			Assert.assertEquals(accountingPeriodTests.subPeriodOpenSize(), 0);
			Assert.assertEquals(accountingPeriodTests.subPeriodCloseSize(), 12);
			accountingPeriodTests.showDetailsOfAccountingPeriod(1);
		}catch (Exception e) {
			accountingPeriodTests.refreshPage();
			Assert.assertEquals(accountingPeriodTests.subPeriodOpenSize(), 0);
			Assert.assertEquals(accountingPeriodTests.subPeriodCloseSize(), 12);
			accountingPeriodTests.showDetailsOfAccountingPeriod(1);
		}
		
	}
	
	/***   Check sub-periods status CLOSED   ***/
	@Test(priority = 19, enabled = true)
	public void _APM_T1_ReopenSubperiod() throws Exception {
		accountingPeriodTests.showDetailsOfAccountingPeriod(1);
		for(int i=1 ; i < 13 ; i++) {
			accountingPeriodTests.openSubperiodForFinanceUser(2, i);
		}
		accountingPeriodTests.showDetailsOfAccountingPeriod(1);
	}	
	
	/***   Close sub-period for regular user   ***/
	@Test(priority = 20, enabled = true)
	public void _APM_T1_CloseSubperiodForRegularUser() throws Exception {
		accountingPeriodTests.showDetailsOfAccountingPeriod(2);
		Assert.assertEquals(accountingPeriodTests.subPeriodOpenSize(), 12);
		Assert.assertEquals(accountingPeriodTests.subPeriodCloseSize(), 0);
		accountingPeriodTests.closeSubperiodForRegularUser(2,1);
		Assert.assertEquals(accountingPeriodTests.subPeriodOpenSize(), 11);
		Assert.assertEquals(accountingPeriodTests.subPeriodCloseSize(), 1);
	}
	
	/***   Check close date for regular user   ***/
	@Test(priority = 22, enabled = true)
	public void _APM_T1_CheckCloseDateForRegularUser() throws Exception {
		if(getTheFirstOfTheCurrentMonth().equals(getCurrentDate()) || getTheLastOfTheCurrentMonth().equals(getCurrentDate()) ){
			Assert.assertEquals(accountingPeriodTests.elementsFoundSize(getCurrentDate()), 2);
		}else {
			Assert.assertEquals(accountingPeriodTests.elementsFoundSize(getCurrentDate()), 1);
		}
		
	}	
	
	/***   Close sub-period for finance user   ***/
	@Test(priority = 24, enabled = true)
	public void _APM_T1_CloseSubperiodForFinanceUser() throws Exception {
		accountingPeriodTests.closeSubperiodForFinanceUser(2,2);
	}
	
	/***   Check close date for finance user   ***/
	@Test(priority = 26, enabled = true)
	public void _APM_T1_CheckCloseDateForFinanceUser() throws Exception {
		if(getTheFirstOfTheCurrentMonth().equals(getCurrentDate()) || getTheLastOfTheCurrentMonth().equals(getCurrentDate())){
			Assert.assertEquals(accountingPeriodTests.elementsFoundSize(getCurrentDate()), 4);
		}else {
			Assert.assertEquals(accountingPeriodTests.elementsFoundSize(getCurrentDate()), 3);
		}
	}
	
	/***   Open sub-period for regular user   ***/
	@Test(priority = 28, enabled = true)
	public void _APM_T1_OpensubperiodForRegularUser() throws Exception {
		accountingPeriodTests.openSubperiodForRegularUser(2, 1);
		if(getTheFirstOfTheCurrentMonth().equals(getCurrentDate()) || getTheLastOfTheCurrentMonth().equals(getCurrentDate())){
			Assert.assertEquals(accountingPeriodTests.elementsFoundSize(getCurrentDate()), 3);
		}else {
			Assert.assertEquals(accountingPeriodTests.elementsFoundSize(getCurrentDate()), 2);
		}
	}
	
	/***   Re-open for regular user a closed sub-period for finance user   ***/
	@Test(priority = 30, enabled = true)
	public void _APM_T1_ReopenForRegularUserClosedSubperiodForFinanceUser() throws Exception {
		accountingPeriodTests.openSubperiodForRegularUser(2, 2);
		waitPageLoaded();
		accountingPeriodTests.cancelClosingSubperiod();
		
		if(getTheFirstOfTheCurrentMonth().equals(getCurrentDate()) || getTheLastOfTheCurrentMonth().equals(getCurrentDate())){
			Assert.assertEquals(accountingPeriodTests.elementsFoundSize(getCurrentDate()), 3);
		}else {
			Assert.assertEquals(accountingPeriodTests.elementsFoundSize(getCurrentDate()), 2);
		}
	}
	
	/***   Open sub-period for finance user   ***/
	@Test(priority = 32, enabled = true)
	public void _APM_T1_openSubperiodForFinanceUser() throws Exception {
		accountingPeriodTests.openSubperiodForFinanceUser(2, 2);
		if(getTheFirstOfTheCurrentMonth().equals(getCurrentDate()) || getTheLastOfTheCurrentMonth().equals(getCurrentDate())){
			Assert.assertEquals(accountingPeriodTests.elementsFoundSize(getCurrentDate()), 1);
		}else {
			Assert.assertEquals(accountingPeriodTests.elementsFoundSize(getCurrentDate()), 0);
		}
		accountingPeriodTests.showDetailsOfAccountingPeriod(2);
	}
	
	/***      ***/
	@Test(priority = 34, enabled = true)
	public void _APM_T1_addNewPeriod() throws Exception {
		for (int i = 0; i < 8; i++) {
			accountingPeriodTests.addNewPeriod();
			if (accountingPeriodTests.checkValueInAccoutingPeriodList(nextPeriode) == true){
				break;
			}
		}
		accountingPeriodTests.showDetailsOfAccountingPeriod(3);
		if(accountingPeriodTests.elementsFoundSize(firstMonthOfNextPeriod) == 0) {
			accountingPeriodTests.showDetailsOfAccountingPeriod(3);
			accountingPeriodTests.sortByAccountingPeriodYear();
			accountingPeriodTests.showDetailsOfAccountingPeriod(3);
			Assert.assertEquals(accountingPeriodTests.elementsFoundSize(firstMonthOfNextPeriod), 1);
		}
		
		Assert.assertEquals(accountingPeriodTests.subPeriodOpenSize(), 12);
		Assert.assertEquals(accountingPeriodTests.subPeriodCloseSize(), 0);
		Assert.assertEquals(accountingPeriodTests.elementsFoundSize(firstMonthOfNextPeriod), 1);
		accountingPeriodTests.showDetailsOfAccountingPeriod(3);
	}	
	
	/***  Close all past periods    ***/
	@Test(priority = 36, enabled = true)
	public void _APM_T1_closeAllPastPeriods() throws Exception {
		accountingPeriodTests.closePeriod(1);
	}
	
	/***  Close all past sub-periods    ***/
	@Test(priority = 38, enabled = true)
	public void _APM_T1_closeAllPastSubperiods() throws Exception {
		accountingPeriodTests.showDetailsOfAccountingPeriod(2); 
		int currentMonth = getTheCurrentMonth();
		if(currentMonth < 9) {
			int j = currentMonth + 4;
			for(int i = 1; i < j; i++) {
				if(i != currentMonth) {
					accountingPeriodTests.closeSubperiodForFinanceUser(2,i);
				}
				
			}
		}
		else {
			int j = currentMonth - 8;
			for(int i = 1; i< j; i++ ) {
				if(i != currentMonth) {
				accountingPeriodTests.closeSubperiodForFinanceUser(2,i);
				}
			}
			
		}
	}
	
}
