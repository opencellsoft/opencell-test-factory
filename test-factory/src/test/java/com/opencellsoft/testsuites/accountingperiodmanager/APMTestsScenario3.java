package com.opencellsoft.testsuites.accountingperiodmanager;

import  org.testng.Assert;
import java.awt.AWTException;
import java.io.IOException;
import org.testng.annotations.Test;

public class APMTestsScenario3 extends APMTestsCommons {

	String fileName = "journalEntry";
	String currentPeriod = "2023-2024";


	@Test(priority = 2, enabled = true)
	public void _APM_T3() throws Exception {
		System.out.println("**************************      Scenario 3 : accounting entries  ******************************");
	}

	/***   Go to accounting entries   ***/
	@Test(priority = 4)
	public void _APM_T3_goToAccountingEntriesPage() {
		// Go to accounting period report
		homeTests.goToAccountingEntriesPage();
	}

	/***   Check (debit, Curreny amount, Credit, VAT) for invoice1   ***/
	@Test(priority = 6, enabled = false)
	public void _APM_T3_CheckAccountingEntries() {

		// Sort by category
		accountingPeriodTests.sortByCredit();
		
		Assert.assertEquals(accountingPeriodTests.getValueFromTable( 2, 6), "€5,332.80");
		Assert.assertEquals(accountingPeriodTests.getValueFromTable( 4, 6), "€6,666.00");
		Assert.assertEquals(accountingPeriodTests.getValueFromTable( 6, 6), "€7,999.20");
		
		Assert.assertEquals(accountingPeriodTests.getValueFromTable( 8, 7), "€888.80");
		Assert.assertEquals(accountingPeriodTests.getValueFromTable(10, 7), "€1,111.00");
		Assert.assertEquals(accountingPeriodTests.getValueFromTable(12, 7), "€1,333.20");
		Assert.assertEquals(accountingPeriodTests.getValueFromTable(14, 7), "€4,444.00");
		Assert.assertEquals(accountingPeriodTests.getValueFromTable(16, 7), "€5,555.00");
		Assert.assertEquals(accountingPeriodTests.getValueFromTable(18, 7), "€6,666.00");
		
		Assert.assertEquals(accountingPeriodTests.getValueFromTable(2, 18), "-5,332.8");
		Assert.assertEquals(accountingPeriodTests.getValueFromTable(4, 18), "-6,666");
		Assert.assertEquals(accountingPeriodTests.getValueFromTable(6, 18), "-7,999.2");
		Assert.assertEquals(accountingPeriodTests.getValueFromTable(8, 18), "888.8");
		Assert.assertEquals(accountingPeriodTests.getValueFromTable(10, 18), "1,111");
		Assert.assertEquals(accountingPeriodTests.getValueFromTable(12, 18), "1,333.2");
		Assert.assertEquals(accountingPeriodTests.getValueFromTable(14, 18), "4,444");
		Assert.assertEquals(accountingPeriodTests.getValueFromTable(16, 18), "5,555");
		Assert.assertEquals(accountingPeriodTests.getValueFromTable(18, 18), "6,666");
	}

	/***   Check Total debit, Total credit, Total curreny amount   ***/
	@Test(priority = 8, enabled = false)
	public void _APM_T3_checkTotals() {
		// Check total debit
		Assert.assertEquals(accountingPeriodTests.getTotalFromTable(20, 6), "€19,998.00");
		// Check total credit
		Assert.assertEquals(accountingPeriodTests.getTotalFromTable(20, 7), "€19,998.00");
		// Check total Curreny amount
		Assert.assertEquals(accountingPeriodTests.getTotalFromTable(20, 18), "€0.00");
	}


	/***   check referencing   ***/
	@Test(priority = 10, enabled = false)
	public void _APM_T3_checkReferencing() throws AWTException {
		invoiceNumber1 = accountingPeriodTests.getValueFromTable(2, 16);
		//Go to invoice details by link reference
		accountingPeriodTests.goToReferenceByAccountingEntries(invoiceNumber1);
		Assert.assertEquals(invoicesTests.getInvoiceNumber(),invoiceNumber1 ); 
		// Go back to accounting period report
		if(version.equals("17.X")) {invoicesTests.navigateBack();}else {invoicesTests.goBack();}
		invoicesTests.goBack();
		// Check total debit
		Assert.assertEquals(accountingPeriodTests.getTotalFromTable(20, 6), "€19,998.00");
		// Check total credit
		Assert.assertEquals(accountingPeriodTests.getTotalFromTable(20, 7), "€19,998.00");
		// Check total Curreny amount
		Assert.assertEquals(accountingPeriodTests.getTotalFromTable(20, 18), "€0.00");
	}

	/***   Check default value (Current period)   ***/
	@Test(priority = 12, enabled = false)
	public void _APM_T3_checkDefaultValueOfFiscalYear() {
		Assert.assertEquals(accountingPeriodTests.checkDefaultValueOfFiscalYear(), currentPeriod)	;
	}

	/***   Export CSV file ()   ***/
	@Test(priority = 16)
	public void _APM_T3_exportJournalEntryCSV() throws IOException, InterruptedException {
		// Sort by category
		accountingPeriodTests.sortByCredit();
		
		accountingPeriodTests.downlaodJournalEntryCSV();
		waitForTheFileToDownload(fileName);
//		accountingPeriodTests.downlaodJournalEntryCSV();
//		waitForTheFileToDownload(fileName);
		Thread.sleep(5000);
	}

	/***   Check file contents  ***/
	@Test(priority = 18)
	public void _APM_T3_checkJournalEntryCSVFileContents() throws IOException, InterruptedException {
		String[][] table = csvReader(LastDownloadedFile(), ";");
		String Category = null;
		try {
			Category = table[0][3];
		}catch (Exception e) {
			accountingPeriodTests.downlaodJournalEntryCSV();
			waitForTheFileToDownload(fileName);
			Thread.sleep(5000);
			table = csvReader(LastDownloadedFile(), ",");
			Category = table[0][3];
		}
		
		Assert.assertEquals(table[0][3], "Category") ;
		Assert.assertEquals(table[1][3], "ASSETS") ;
		Assert.assertEquals(table[2][3], "ASSETS") ;
		Assert.assertEquals(table[3][3], "ASSETS") ;
		
		Assert.assertEquals(table[4][3], "LIABILITIES") ;
		Assert.assertEquals(table[5][3], "LIABILITIES") ;
		Assert.assertEquals(table[6][3], "LIABILITIES") ;
		
		Assert.assertEquals(table[7][3], "REVENUE") ;
		Assert.assertEquals(table[8][3], "REVENUE") ;
		Assert.assertEquals(table[9][3], "REVENUE") ;
		
		Assert.assertEquals(table[0][5], "Debit") ;
		Assert.assertEquals(table[1][5], "5332.80") ;
		Assert.assertEquals(table[2][5], "6666.00") ;
		Assert.assertEquals(table[3][5], "7999.20") ;
		Assert.assertEquals(table[4][5], "0.00") ;
		Assert.assertEquals(table[5][5], "0.00") ;
		
		Assert.assertEquals(table[0][6], "Credit") ;
		Assert.assertEquals(table[3][6], "0.00") ;
		Assert.assertEquals(table[4][6], "888.80") ;
		Assert.assertEquals(table[5][6], "1111.00") ;
		Assert.assertEquals(table[6][6], "1333.20") ;
		Assert.assertEquals(table[7][6], "4444.00") ;
		Assert.assertEquals(table[8][6], "5555.00") ;
		Assert.assertEquals(table[9][6], "6666.00") ;
		
		Assert.assertEquals(table[0][7], "Account") ;
		Assert.assertEquals(table[1][7], "411000000") ;
		Assert.assertEquals(table[4][7], "445710000") ;
		Assert.assertEquals(table[7][7], "701000000") ;
		if(version.equals("16.X") || version.equals("17.X")) {
			Assert.assertEquals(table[0][16], "Currency amount") ;
			Assert.assertEquals(table[1][16], "-5332.80") ;
			Assert.assertEquals(table[2][16], "-6666.00") ;
			Assert.assertEquals(table[3][16], "-7999.20") ;
			Assert.assertEquals(table[4][16], "888.80") ;
		}else {
			Assert.assertEquals(table[0][17], "Currency amount") ;
			Assert.assertEquals(table[1][17], "-5332.80") ;
			Assert.assertEquals(table[2][17], "-6666.00") ;
			Assert.assertEquals(table[3][17], "-7999.20") ;
			Assert.assertEquals(table[4][17], "888.80") ;
		}
		
	}

	/***   Export XLSX file ()   ***/
	@Test(priority = 20)
	public void _APM_T3_exportJournalEntryXLSX() throws IOException, InterruptedException {
		accountingPeriodTests.downlaodJournalEntryXLSX();
		waitForTheFileToDownload(fileName);
	}

	/***   Check file contents    ***/
	@Test(priority = 22)
	public void _APM_T3_checkJournalEntryXLSXFileContents() throws IOException, InterruptedException {
		Object[][] table = null;
		try {
			table = xlsxReader(LastDownloadedFile());
		}catch (Exception e) {
			Thread.sleep(5000);
			table = xlsxReader(LastDownloadedFile());
		}
		if(table.equals(null)) {
			_APM_T3_exportJournalEntryXLSX();
			Thread.sleep(5000);
			table = xlsxReader(LastDownloadedFile());
		}
		
		Assert.assertEquals(table[0][3].toString(), "Category") ;
		Assert.assertEquals(table[1][3].toString(), "ASSETS") ;
		Assert.assertEquals(table[2][3].toString(), "ASSETS") ;
		Assert.assertEquals(table[3][3].toString(), "ASSETS") ;
		
		Assert.assertEquals(table[4][3].toString(), "LIABILITIES") ;
		Assert.assertEquals(table[5][3].toString(), "LIABILITIES") ;
		Assert.assertEquals(table[6][3].toString(), "LIABILITIES") ;
		
		Assert.assertEquals(table[7][3].toString(), "REVENUE") ;
		Assert.assertEquals(table[8][3].toString(), "REVENUE") ;
		Assert.assertEquals(table[9][3].toString(), "REVENUE") ;
		
		Assert.assertEquals(table[0][5].toString(), "Debit") ;
		
		String totalCurrent= table[1][5].toString();
		Assert.assertTrue(totalCurrent.equals("5332.8") || totalCurrent.equals("5332.80"));
		
		totalCurrent= table[2][5].toString();
		Assert.assertTrue(totalCurrent.equals("6666") || totalCurrent.equals("6666.0") || totalCurrent.equals("6666.00"));
		
		totalCurrent= table[3][5].toString();
		Assert.assertTrue(totalCurrent.equals("7999.2") || totalCurrent.equals("7999.20"));
		
		totalCurrent= table[4][5].toString();
		Assert.assertTrue(totalCurrent.equals("0.0") || totalCurrent.equals("0")|| totalCurrent.equals("0.00"));
		
		totalCurrent= table[5][5].toString();
		Assert.assertTrue(totalCurrent.equals("0.0") || totalCurrent.equals("0")|| totalCurrent.equals("0.00"));
		
		Assert.assertEquals(table[0][6].toString(), "Credit") ;
		
		totalCurrent= table[3][6].toString();
		Assert.assertTrue(totalCurrent.equals("0.0") || totalCurrent.equals("0")|| totalCurrent.equals("0.00"));
		
		totalCurrent= table[4][6].toString();
		Assert.assertTrue(totalCurrent.equals("888.8") || totalCurrent.equals("888.80"));
		
		totalCurrent= table[5][6].toString();
		Assert.assertTrue(totalCurrent.equals("1111.0") || totalCurrent.equals("1111")|| totalCurrent.equals("1111.00"));
		
		totalCurrent= table[6][6].toString();
		Assert.assertTrue(totalCurrent.equals("1333.2") || totalCurrent.equals("1333.20"));
		
		totalCurrent= table[7][6].toString();
		Assert.assertTrue(totalCurrent.equals("4444.0") || totalCurrent.equals("4444.00")|| totalCurrent.equals("4444"));
		
		totalCurrent= table[8][6].toString();
		Assert.assertTrue(totalCurrent.equals("5555.0") || totalCurrent.equals("5555.00")|| totalCurrent.equals("5555"));
		
		totalCurrent= table[9][6].toString();
		Assert.assertTrue(totalCurrent.equals("6666.0") || totalCurrent.equals("6666.00")|| totalCurrent.equals("6666"));
		
		Assert.assertEquals(table[0][7].toString(), "Account") ;
		Assert.assertEquals(table[1][7].toString(), "411000000") ;
		Assert.assertEquals(table[4][7].toString(), "445710000") ;
		Assert.assertEquals(table[7][7].toString(), "701000000") ;
		if(version.equals("16.X") || version.equals("17.X")) {
			Assert.assertEquals(table[0][16].toString(), "Currency amount") ;
			
			totalCurrent= table[1][16].toString();
			Assert.assertTrue(totalCurrent.equals("-5332.8") || totalCurrent.equals("-5332.80"));
			
			totalCurrent= table[2][16].toString();
			Assert.assertTrue(totalCurrent.equals("-6666.0") || totalCurrent.equals("-6666.00")|| totalCurrent.equals("-6666"));
			
			totalCurrent= table[3][16].toString();
			Assert.assertTrue(totalCurrent.equals("-7999.2") || totalCurrent.equals("-7999.20"));
			
			totalCurrent= table[4][16].toString();
			Assert.assertTrue(totalCurrent.equals("888.8") || totalCurrent.equals("888.80"));
		}else {
			Assert.assertEquals(table[0][17].toString(), "Currency amount") ;
			
			totalCurrent= table[1][17].toString();
			Assert.assertTrue(totalCurrent.equals("-5332.8") || totalCurrent.equals("-5332.80"));
			
			totalCurrent= table[2][17].toString();
			Assert.assertTrue(totalCurrent.equals("-6666.0") || totalCurrent.equals("-6666.00")|| totalCurrent.equals("-6666"));
			
			totalCurrent= table[3][17].toString();
			Assert.assertTrue(totalCurrent.equals("-7999.2") || totalCurrent.equals("-7999.20"));
			
			totalCurrent= table[4][17].toString();
			Assert.assertTrue(totalCurrent.equals("888.8") || totalCurrent.equals("888.80"));
		}
		
	}
}
