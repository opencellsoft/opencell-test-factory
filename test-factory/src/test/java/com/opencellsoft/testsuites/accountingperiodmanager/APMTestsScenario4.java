package com.opencellsoft.testsuites.accountingperiodmanager;

import static org.testng.Assert.assertEquals;
import java.io.IOException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class APMTestsScenario4 extends APMTestsCommons {

	String fileName = "Aged_trial_balance";


	@Test(priority = 0, enabled = true)
	public void _APM_T4_login()  {
		System.out.println("**************************    Scenario 4 : aged trial balance    ******************************");
	}
	
	/***   Go to aged trial balance   ***/
	@Test(priority = 4)
	public void _APM_T4_goToAgedTrialBalancePage() {
		// Go to aged trial balance
		homeTests.goToAgedTrialBalancePage();
	}
	
	/***   StepsDays30   ***/
	@Test(priority = 6)
	public void _APM_T4_checkStepInDays30() {
		assertEquals(accountingPeriodTests.getValueFromTable(2,7), "€6,666.00") ;
		assertEquals(accountingPeriodTests.getValueFromTable(3,11), "€5,332.80") ;
		assertEquals(accountingPeriodTests.getTotalFromTable(4, 7), "€6,666.00");
		assertEquals(accountingPeriodTests.getTotalFromTable(4, 11), "€5,332.80");
		assertEquals(accountingPeriodTests.getTotalFromTable(4, 13), "€11,998.80");
	}	
	
	/***   checkStepsDays50   ***/
	@Test(priority = 8)
	public void _APM_T4_checkStepInDays50() {
		accountingPeriodTests.filterByStepDays("50");
		assertEquals(accountingPeriodTests.getValueFromTable(2,7), "€6,666.00") ;
		assertEquals(accountingPeriodTests.getValueFromTable(3,10), "€5,332.80") ;
		assertEquals(accountingPeriodTests.getTotalFromTable(4, 7), "€6,666.00");
		assertEquals(accountingPeriodTests.getTotalFromTable(4, 10), "€5,332.80");
		assertEquals(accountingPeriodTests.getTotalFromTable(4, 13), "€11,998.80");
	}
	
	/***   checkStepsDays100   ***/
	@Test(priority = 10)
	public void _APM_T4_checkStepInDays100() {
		accountingPeriodTests.filterByStepDays("100");
		assertEquals(accountingPeriodTests.getValueFromTable(2,7), "€6,666.00") ;
		assertEquals(accountingPeriodTests.getValueFromTable(3,9), "€5,332.80") ;
		assertEquals(accountingPeriodTests.getTotalFromTable(4, 7), "€6,666.00");
		assertEquals(accountingPeriodTests.getTotalFromTable(4, 9), "€5,332.80");
		assertEquals(accountingPeriodTests.getTotalFromTable(4, 13), "€11,998.80");
	}
	
	/***   checkStepsDays140   ***/
	@Test(priority = 12)
	public void _APM_T4_checkStepInDays140() {
		accountingPeriodTests.filterByStepDays("150");
		assertEquals(accountingPeriodTests.getValueFromTable(2,7), "€6,666.00") ;
		assertEquals(accountingPeriodTests.getValueFromTable(3,8), "€5,332.80") ;
		assertEquals(accountingPeriodTests.getTotalFromTable(4, 7), "€6,666.00");
		assertEquals(accountingPeriodTests.getTotalFromTable(4, 8), "€5,332.80");
		assertEquals(accountingPeriodTests.getTotalFromTable(4, 13), "€11,998.80");
	}
	
	/***   Apply filter Steps in the past   ***/
	@Test(priority = 14)
	public void _APM_T4_applyFilterStepsInThePast() {
		accountingPeriodTests.applyFilterByCustomDateInPast();
	}
	
	/***   Check result   ***/
	@Test(priority = 16)
	public void _APM_T4_checkResult() {
		assertEquals(accountingPeriodTests.getValueFromTable(2,8), "€5,332.80") ;
	}		
	
	/***   Apply filter Steps in the futur   ***/
	@Test(priority = 18)
	public void _APM_T4_applyFilterStepsInTheFutur() {
		accountingPeriodTests.applyFilterByCustomDateInFutur();
	}	
	
	/***   Check result   ***/
	@Test(priority = 20)
	public void _APM_T4_checkResult2(){
		assertEquals(accountingPeriodTests.getValueFromTable(2,7), "€7,999.20") ;
		assertEquals(accountingPeriodTests.getValueFromTable(3,8), "€6,666.00") ;
		assertEquals(accountingPeriodTests.getValueFromTable(4,9), "€5,332.80") ;
	}	
	
	/***   Export CSV file (aged_trial_balance)   
	 * @throws InterruptedException 
	 * @throws IOException ***/
	@Test(priority = 24)
	public void _APM_T4_exportCSVFile() throws IOException, InterruptedException {
		accountingPeriodTests.downlaodAgedTrialBalanceCSV(version);
		waitForTheFileToDownload(fileName);
		accountingPeriodTests.downlaodAgedTrialBalanceCSV(version);
		waitForTheFileToDownload(fileName);
	}	
	
	/***   Check file contents  ***/
	@Test(priority = 26, enabled = true)
	public void _APM_T4_checkCSVContents() throws InterruptedException, IOException {
		String[][] table = null;
		table = csvReader(LastDownloadedFile(), ",");
		if(table.equals(null)) {
			accountingPeriodTests.downlaodAgedTrialBalanceCSV(this.version);
			waitForTheFileToDownload(fileName);
			Thread.sleep(5000);
			table = csvReader(LastDownloadedFile(), ",");
		}
		
		String total = null;
		try {
			total = table[0][6];
		}catch (Exception e) {
			table = csvReader(LastDownloadedFile(), ";");
			total = table[0][6];
		}
		
		Assert.assertEquals(total, "Total current") ;
		String totalCurrent= table[1][6];
		Assert.assertTrue(totalCurrent.equals("7999.20") || totalCurrent.equals("7999.2"));
		
		Assert.assertEquals(table[0][7], "1-150 days") ;
		String total150 = table[2][7];
		Assert.assertTrue(total150.equals("6666.00") || total150.equals("6666"));
		
		Assert.assertEquals(table[0][8], "151-300 days") ;
		String total300 = table[3][8];
		Assert.assertTrue(total300.equals("5332.80") || total300.equals("5332.8"));
	}	
	
	/***   Export XLSX file  (aged_trial_balance)   
	 * @throws InterruptedException 
	 * @throws IOException ***/
	@Test(priority = 28)
	public void _APM_T4_exportXLSXFile() throws IOException, InterruptedException {
		accountingPeriodTests.downlaodAgedTrialBalanceXLSX(this.version);
		waitForTheFileToDownload(fileName);
	}	
	
	/***   Check file contents   
	 * @throws IOException 
	 * @throws InterruptedException ***/
	@Test(priority = 30)
	public void _APM_T4_checkXLSXContents() throws IOException, InterruptedException {
		Object[][] table;
		try {
			table = xlsxReader(LastDownloadedFile());
		}catch (Exception e) {
			Thread.sleep(5000);
			table = xlsxReader(LastDownloadedFile());
		}
		
		if(table.equals(null)) {
			_APM_T4_exportXLSXFile();
			Thread.sleep(5000);
			table = xlsxReader(LastDownloadedFile());
		}
		assertEquals(table[0][6].toString(), "Total current") ;
		String value= table[1][6].toString();
		Assert.assertTrue(value.equals("7999.2") || value.equals("7999.20"));
		
		assertEquals(table[0][7].toString(), "1-150 days") ;
		
		value= table[2][7].toString();
		Assert.assertTrue(value.equals("6666.0") || value.equals("6666.00") || value.equals("6666"));
		assertEquals(table[0][8].toString(), "151-300 days") ;
		
		value= table[3][8].toString();
		Assert.assertTrue(value.equals("5332.8") || value.equals("5332.80"));
	}	
}
