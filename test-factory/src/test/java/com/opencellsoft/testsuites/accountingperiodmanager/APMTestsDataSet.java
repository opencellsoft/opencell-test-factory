package com.opencellsoft.testsuites.accountingperiodmanager;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.tests.HomeTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.tests.admin.PostgreeTests;
import com.opencellsoft.tests.jobs.PayloadTests;

public class APMTestsDataSet extends APMTestsCommons {

	String request0 = "delete from sub_accounting_period;\r\n"
			+ "delete from accounting_period;\r\n"
			+ "update  \"ar_account_operation\" set operation_number = null, invoice_id = null;\r\n"
			+ "delete from \"billing_rated_transaction\" ;\r\n"
			+ "delete from \"billing_invoice_line\" ;\r\n"
			+ "delete from \"billing_invoices_subscriptions\" ;\r\n"
			+ "delete from \"billing_invoice\" ;\r\n"
			+ "delete from \"accounting_journal_entry\" ;\r\n"
			+ "delete from \"ar_account_operation\" ;";
	
	String request1 = "INSERT INTO \"accounting_period\" (\"id\", \"version\", \"created\", \"updated\", \"creator\", \"updater\", \"accounting_period_year\", \"start_date\", \"end_date\", \"use_sub_accounting_cycles\", \"status\", \"sub_accounting_period_type\", \"sub_accounting_period_progress\", \"ongoing_sub_accounting_periods\", \"accounting_operation_action\", \"regular_user_lock_option\", \"custom_lock_number_days\", \"custom_lock_option\", \"force_option\", \"force_custom_day\") VALUES \r\n"
			+ "(3, 0,'2023-09-01',NULL,'opencell.admin',NULL,'2023-2024','2023-09-01 00:00:00','2024-08-31 00:00:00',1,'OPEN','MONTHLY',NULL,NULL,'FORCE','END_OF_SUB_AP',NULL,NULL,'FIRST_DAY',NULL), \r\n"
			+ "(4, 0,'2024-09-01',NULL,'opencell.admin',NULL,'2024-2025','2024-09-01 00:00:00','2025-08-31 00:00:00',1,'OPEN','MONTHLY',NULL,NULL,'FORCE','END_OF_SUB_AP',NULL,NULL,'FIRST_DAY',NULL);\r\n";

	String request1_1 ="INSERT INTO \"sub_accounting_period\" (\"id\", \"version\", \"created\", \"updated\", \"creator\", \"updater\", \"period_number\", \"start_date\", \"end_date\", \"regular_users_sub_period_status\", \"regular_users_closed_date\", \"effective_closed_date\", \"accounting_period_id\", \"all_users_sub_period_status\", \"regularusers_reopening_reason\", \"allusers_reopening_reason\") VALUES "
			+ "(1, 0,'2024-04-28',NULL,'opencell.admin',NULL, 1,'2020-09-01','2020-09-30','OPEN',NULL,NULL,1,'OPEN',NULL,NULL),"
			+ "(2, 0,'2024-04-28',NULL,'opencell.admin',NULL, 2,'2020-10-01','2020-10-31','OPEN',NULL,NULL,1,'OPEN',NULL,NULL),"
			+ "(3, 0,'2024-04-28',NULL,'opencell.admin',NULL, 3,'2020-11-01','2020-11-30','OPEN',NULL,NULL,1,'OPEN',NULL,NULL),"
			+ "(4, 0,'2023-04-28',NULL,'opencell.admin',NULL, 4,'2020-12-01','2020-12-31','OPEN',NULL,NULL,1,'OPEN',NULL,NULL),"
			+ "(5, 0,'2023-04-28',NULL,'opencell.admin',NULL, 5,'2021-01-01','2021-01-31','OPEN',NULL,NULL,1,'OPEN',NULL,NULL),"
			+ "(6, 0,'2023-04-28',NULL,'opencell.admin',NULL, 6,'2021-02-01','2021-02-28','OPEN',NULL,NULL,1,'OPEN',NULL,NULL),"
			+ "(7, 0,'2023-04-28',NULL,'opencell.admin',NULL, 7,'2021-03-01','2021-03-31','OPEN',NULL,NULL,1,'OPEN',NULL,NULL),"
			+ "(8, 0,'2023-04-28',NULL,'opencell.admin',NULL, 8,'2021-04-01','2021-04-30','OPEN',NULL,NULL,1,'OPEN',NULL,NULL),"
			+ "(9, 0,'2023-04-28',NULL,'opencell.admin',NULL, 9,'2021-05-01','2021-05-31','OPEN',NULL,NULL,1,'OPEN',NULL,NULL),"
			+ "(10,0,'2023-04-28',NULL,'opencell.admin',NULL,10,'2021-06-01','2021-06-30','OPEN',NULL,NULL,1,'OPEN',NULL,NULL),"
			+ "(11,0,'2023-04-28',NULL,'opencell.admin',NULL,11,'2021-07-01','2021-07-31','OPEN',NULL,NULL,1,'OPEN',NULL,NULL),"
			+ "(12,0,'2023-04-28',NULL,'opencell.admin',NULL,12,'2021-08-01','2021-08-31','OPEN',NULL,NULL,1,'OPEN',NULL,NULL);";

	String request2_1 ="INSERT INTO \"sub_accounting_period\" (\"id\", \"version\", \"created\", \"updated\", \"creator\", \"updater\", \"period_number\", \"start_date\", \"end_date\", \"regular_users_sub_period_status\", \"regular_users_closed_date\", \"effective_closed_date\", \"accounting_period_id\", \"all_users_sub_period_status\", \"regularusers_reopening_reason\", \"allusers_reopening_reason\") VALUES "
			+ "(13,0,'2023-04-28',NULL,'opencell.admin',NULL, 1,'2021-09-01','2021-09-30','OPEN',NULL,NULL,2,'OPEN',NULL,NULL),"
			+ "(14,0,'2023-04-28',NULL,'opencell.admin',NULL, 2,'2021-10-01','2021-10-31','OPEN',NULL,NULL,2,'OPEN',NULL,NULL),"
			+ "(15,0,'2023-04-28',NULL,'opencell.admin',NULL, 3,'2021-11-01','2021-11-30','OPEN',NULL,NULL,2,'OPEN',NULL,NULL),"
			+ "(16,0,'2023-04-28',NULL,'opencell.admin',NULL, 4,'2021-12-01','2021-12-31','OPEN',NULL,NULL,2,'OPEN',NULL,NULL),"
			+ "(17,0,'2023-04-28',NULL,'opencell.admin',NULL, 5,'2022-01-01','2022-01-31','OPEN',NULL,NULL,2,'OPEN',NULL,NULL),"
			+ "(18,0,'2023-04-28',NULL,'opencell.admin',NULL, 6,'2022-02-01','2022-02-28','OPEN',NULL,NULL,2,'OPEN',NULL,NULL),"
			+ "(19,0,'2023-04-28',NULL,'opencell.admin',NULL, 7,'2022-03-01','2022-03-31','OPEN',NULL,NULL,2,'OPEN',NULL,NULL),"
			+ "(20,0,'2023-04-28',NULL,'opencell.admin',NULL, 8,'2022-04-01','2022-04-30','OPEN',NULL,NULL,2,'OPEN',NULL,NULL),"
			+ "(21,0,'2023-04-28',NULL,'opencell.admin',NULL, 9,'2022-05-01','2022-05-31','OPEN',NULL,NULL,2,'OPEN',NULL,NULL),"
			+ "(22,0,'2023-04-28',NULL,'opencell.admin',NULL,10,'2022-06-01','2022-06-30','OPEN',NULL,NULL,2,'OPEN',NULL,NULL),"
			+ "(23,0,'2023-04-28',NULL,'opencell.admin',NULL,11,'2022-07-01','2022-07-31','OPEN',NULL,NULL,2,'OPEN',NULL,NULL),"
			+ "(24,0,'2023-04-28',NULL,'opencell.admin',NULL,12,'2022-08-01','2022-08-31','OPEN',NULL,NULL,2,'OPEN',NULL,NULL);";

	String request3_1 ="INSERT INTO \"sub_accounting_period\" (\"id\", \"version\", \"created\", \"updated\", \"creator\", \"updater\", \"period_number\", \"start_date\", \"end_date\", \"regular_users_sub_period_status\", \"regular_users_closed_date\", \"effective_closed_date\", \"accounting_period_id\", \"all_users_sub_period_status\", \"regularusers_reopening_reason\", \"allusers_reopening_reason\") VALUES "
			+ "(25,0,'2023-09-01',NULL,'opencell.admin',NULL, 1,'2023-09-01','2023-09-30','OPEN', NULL,NULL,3,'OPEN',NULL,NULL),"
			+ "(26,0,'2023-10-01',NULL,'opencell.admin',NULL, 2,'2023-10-01','2023-10-31','OPEN', NULL,NULL,3,'OPEN',NULL,NULL),"
			+ "(27,0,'2023-11-01',NULL,'opencell.admin',NULL, 3,'2023-11-01','2023-11-30','OPEN', NULL,NULL,3,'OPEN',NULL,NULL),"
			+ "(28,0,'2023-12-01',NULL,'opencell.admin',NULL, 4,'2023-12-01','2023-12-31','OPEN', NULL,NULL,3,'OPEN',NULL,NULL),"
			+ "(29,0,'2024-01-01',NULL,'opencell.admin',NULL, 5,'2024-01-01','2024-01-31','OPEN', NULL,NULL,3,'OPEN',NULL,NULL),"
			+ "(30,0,'2024-02-01',NULL,'opencell.admin',NULL, 6,'2024-02-01','2024-02-28','OPEN', NULL,NULL,3,'OPEN',NULL,NULL),"
			+ "(31,0,'2024-03-01',NULL,'opencell.admin',NULL, 7,'2024-03-01','2024-03-31','OPEN', NULL,NULL,3,'OPEN',NULL,NULL),"
			+ "(32,0,'2024-04-01',NULL,'opencell.admin',NULL, 8,'2024-04-01','2024-04-30','OPEN', NULL,NULL,3,'OPEN',NULL,NULL),"
			+ "(33,0,'2024-05-01',NULL,'opencell.admin',NULL, 9,'2024-05-01','2024-05-31','OPEN', NULL,NULL,3,'OPEN',NULL,NULL),"
			+ "(34,0,'2024-06-01',NULL,'opencell.admin',NULL,10,'2024-06-01','2024-06-30','OPEN', NULL,NULL,3,'OPEN',NULL,NULL),"
			+ "(35,0,'2024-07-01',NULL,'opencell.admin',NULL,11,'2024-07-01','2024-07-31','OPEN', NULL,NULL,3,'OPEN',NULL,NULL),"
			+ "(36,0,'2024-08-01',NULL,'opencell.admin',NULL,12,'2024-08-01','2024-08-31','OPEN', NULL,NULL,3,'OPEN',NULL,NULL);";
	
	String request4_1 ="INSERT INTO \"sub_accounting_period\" (\"id\", \"version\", \"created\", \"updated\", \"creator\", \"updater\", \"period_number\", \"start_date\", \"end_date\", \"regular_users_sub_period_status\", \"regular_users_closed_date\", \"effective_closed_date\", \"accounting_period_id\", \"all_users_sub_period_status\", \"regularusers_reopening_reason\", \"allusers_reopening_reason\") VALUES "
			+ "(37,0,'2024-09-01',NULL,'opencell.admin',NULL,1,'2024-09-01','2024-09-30','OPEN',NULL,NULL,4,'OPEN',NULL,NULL),"
			+ "(38,0,'2024-10-01',NULL,'opencell.admin',NULL,2,'2024-10-01','2024-10-31','OPEN',NULL,NULL,4,'OPEN',NULL,NULL),"
			+ "(39,0,'2024-11-01',NULL,'opencell.admin',NULL,3,'2024-11-01','2024-11-30','OPEN',NULL,NULL,4,'OPEN',NULL,NULL),"
			+ "(40,0,'2024-12-01',NULL,'opencell.admin',NULL,4,'2024-12-01','2024-12-31','OPEN',NULL,NULL,4,'OPEN',NULL,NULL),"
			+ "(41,0,'2025-01-01',NULL,'opencell.admin',NULL,5,'2025-01-01','2025-01-31','OPEN',NULL,NULL,4,'OPEN',NULL,NULL),"
			+ "(42,0,'2025-02-01',NULL,'opencell.admin',NULL,6,'2025-02-01','2025-02-28','OPEN',NULL,NULL,4,'OPEN',NULL,NULL),"
			+ "(43,0,'2025-03-01',NULL,'opencell.admin',NULL,7,'2025-03-01','2025-03-31','OPEN',NULL,NULL,4,'OPEN',NULL,NULL),"
			+ "(44,0,'2025-04-01',NULL,'opencell.admin',NULL,8,'2025-04-01','2025-04-30','OPEN',NULL,NULL,4,'OPEN',NULL,NULL),"
			+ "(45,0,'2025-05-01',NULL,'opencell.admin',NULL,9,'2025-05-01','2025-05-31','OPEN',NULL,NULL,4,'OPEN',NULL,NULL),"
			+ "(46,0,'2025-06-01',NULL,'opencell.admin',NULL,10,'2025-06-01','2025-06-30','OPEN',NULL,NULL,4,'OPEN',NULL,NULL),"
			+ "(47,0,'2025-07-01',NULL,'opencell.admin',NULL,11,'2025-07-01','2025-07-31','OPEN',NULL,NULL,4,'OPEN',NULL,NULL),"
			+ "(48,0,'2025-08-01',NULL,'opencell.admin',NULL,12,'2025-08-01','2025-08-31','OPEN',NULL,NULL,4,'OPEN',NULL,NULL);";

	public APMTestsDataSet() {
		loginTest = new LoginTest();
		homeTests = new HomeTests();
		payloadTests = new PayloadTests();
		postgreeTests = new PostgreeTests();
	}
	
	@Test(priority = 1, enabled = true)
	public void _APM_DataSet( ) throws Exception {
		System.out.println("**************************      Accounting periode manager : data set  ******************************");
	}

	/***   Go to Postgree   ***/
	@Parameters({"baseURI" })
	@Test(priority = 2, enabled = true)
	public void _APM_DS_openPostgree(String baseURI) {
		// Open new Tab
		String dbURI = baseURI.replace("/opencell/", "/db/");
		postgreeTests.openPostgree(dbURI);
		postgreeTests.login();
	}

	@Test(priority = 3, enabled = true)
	/***   Open requete sql   ***/
	public void _APM_DS_openRequeteSql() {
		postgreeTests.goToRequeteSql();
	}

	@Test(priority = 4, enabled = true)
	/***   execute requete sql   ***/
	public void _APM_DS_AddAccountingPriods() {
		System.out.println("===========================================================");
		System.out.println(request0);
		System.out.println("===========================================================");
		System.out.println(request1);
		System.out.println("===========================================================");
		System.out.println(request3_1);
		System.out.println("===========================================================");
		System.out.println(request4_1);
		System.out.println("===========================================================");
		postgreeTests.executeRequeteSql(request0);
		postgreeTests.executeRequeteSql(request1);
		postgreeTests.executeRequeteSql(request3_1);
		postgreeTests.executeRequeteSql(request4_1);
	}

	@Test(priority = 5, enabled = false)
	/***   Close Postgree   ***/
	public void _APM_DS_closeTab() {
		postgreeTests.closeTab();
	}

	@Test(priority = 6, enabled = true)
	@Parameters({ "login", "password","baseURI" })
	/***   Close Postgree   ***/
	public void _APM_DS_ActivateFunctions(String login, String password, String baseURI) throws InterruptedException {
		payloadTests.activateFunctions( baseURI, login, password);
	}

}
