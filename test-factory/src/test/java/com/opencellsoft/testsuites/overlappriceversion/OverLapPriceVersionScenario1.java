package com.opencellsoft.testsuites.overlappriceversion;

import java.awt.AWTException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.pages.operations.walletoperations.WalletOperationsPage;
import com.opencellsoft.utility.Constant;

public class OverLapPriceVersionScenario1 extends OverLapPriceVersionCommons {

	String offerCode = "OVOF-" + Constant.offerCode;
	String productCode = "OVPROD-" + Constant.offerCode;
	String usageChargeCode = "OVUC-" + Constant.offerCode;
	String pricePlanVersionCode1 = "OVPV1-" + Constant.offerCode;

	//	String offerCode = "OVOF-001" ;
	//	String productCode = "OVPROD-001";
	//	String usageChargeCode = "OVUC-001";
	//	String pricePlanVersionCode1 = "OVPV1-001";

	String bcCode = "OVBC" + Constant.offerCode;

	String offerDescription = Constant.offerCode;
	String productDescription = Constant.offerCode;



	String usageChargeDescription= Constant.chargeDescriptionUsage;
	String pricePlanVersion1 = "0.88";
	String pricePlanVersionCode2 = "OVPV1" + Constant.offerCode;
	String pricePlanVersion2 = "0.99";
	String customerCode = "OVCUST1" + Constant.offerCode;
	String customerName = Constant.customerName;
	String customerEmail = Constant.customerEmail;
	String paypalUserId = Constant.paypalUserId;
	String consumerCode = "OVCONS-" +Constant.consumerCode;
	String consumerName = Constant.consumerName;
	String companyRegistrationNumber = Constant.companyRegistrationNumber;
	String vatNumber = Constant.vatNumber;
	String subCode = "OVSUB1_"+Constant.subscriptionCode;
	String AP = "OVAP1_"+Constant.accessPointCode;
	String companyRegistration = getRandomNumberbetween1And200();

	@Test(priority = 0, enabled = true)
	public void _OVLPV_T01() {
		System.out.println("**************************   OverLap Price version : Scenario 1   ***************************");
	}

	/***  create a New Offer    ***/
	@Parameters({ "PriceVersionDate", "defaultQuantity", "minimalQuantity", "maximalQuantity", "sequence"})
	@Test(priority = 4, enabled = true)
	public void _OVLPV_T01_createOffer(String PriceVersionDate, String defaultQuantity, String minimalQuantity, String maximalQuantity, String sequence) throws InterruptedException, AWTException {
		offersTests.createOfferMethod(offerCode, offerDescription);
		productsTests.createProductMethod(PriceVersionDate,productCode,productDescription );
		chargesTests.createUsageCharge(usageChargeCode,usageChargeDescription,"","", version);
		Thread.sleep(3000);
		chargesTests.createNewPriceVersionInPresent(pricePlanVersionCode1, pricePlanVersion1);
		chargesTests.publishPriceVersion();
		chargesTests.createNewPriceVersionInFutur(pricePlanVersionCode2, pricePlanVersion2);
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(usageChargeDescription, version);
		productsTests.publishProductVersion();
		productsTests.activateProductAndGoBack();
		homeTests.goToOffersPage();
		offersTests.searchForOfferMethod(offerCode, offerDescription);
		offersTests.pickProductToOffer( productCode,productDescription, version);
		offersTests.fillProductParametersMethod(defaultQuantity,minimalQuantity, maximalQuantity, sequence, false );
		offersTests.ActivateOfferMethod();
		waitPageLoaded();
	}

	/***   Create a new customer &  consumer  ***/

	@Test(priority = 16, enabled = true)
	public void _OVLPV_T01_createCustomerAndSubscription() throws InterruptedException {
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bcCode,bcCode,"BILLINGACCOUNT", "MONTHLY");
		Thread.sleep(500);
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bcCode,bcCode,"BILLINGACCOUNT", "CAL_PERIOD_MONTHLY");
		Thread.sleep(500);
		payloadTests.createCustomer(baseURI, login, password, customerCode,customerName , bcCode);
		Thread.sleep(500);
		payloadTests.createSubscriptionAndInstantiateProduct(baseURI, login, password, subCode, customerCode, offerCode, productCode);
		Thread.sleep(500);
		payloadTests.activateServices(baseURI, login, password, subCode, productCode);
		Thread.sleep(500);
		payloadTests.createAccessPoint(baseURI, login, password, AP, subCode);
		Thread.sleep(1000);
	}

	/***   Insert EDRs, In Framework Agreement  period  ***/
	@Test(priority = 20)
	public void _OVLPV_T01_insertChargeCdr() throws InterruptedException {
		LocalDateTime currentDateTime = LocalDateTime.now();
		LocalDateTime endOfMonth = currentDateTime.with(TemporalAdjusters.lastDayOfMonth());

		// Format the date and time to the desired format
		String date = endOfMonth.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.'00Z'"));
		int result1 = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "",date);
		Assert.assertEquals(result1, 1);

		LocalDateTime firstOfNextMonth = currentDateTime.with(TemporalAdjusters.firstDayOfNextMonth());
		date = firstOfNextMonth.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.'00Z'"));
		int result2 = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "",date);
		Assert.assertEquals(result2, 1);

		LocalDateTime secondOfNextMonth = firstOfNextMonth.plusDays(1);
		date = secondOfNextMonth.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.'00Z'"));
		int result3 = insertChargeCdr(baseURI, login, password, AP,"1", "", "", "",date);
		Assert.assertEquals(result3, 1);
	}

	/***   Run Rated Transaction Job (RT_Job)   ***/
	@Test(priority = 22)
	public void _OVLPV_T01_runRTJob() throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   Check Wallet operations (10 operations, amount= default amount)   ***/
	@Test(priority = 24)
	public void _OVLPV_T01_checkwalletOperations5(){
		// Go to Wallet operations page
		homeTests.goToWalletOperationsPage();

		// search operations by subscription code

		walletOperationsTests.searchBySubscriptionCode(subCode);
		waitPageLoaded();

		// check the number of lines found
		walletOperationsPage = new WalletOperationsPage();
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(subCode), 3);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(pricePlanVersion1), 1);
		Assert.assertEquals(walletOperationsTests.elementsFoundSize(pricePlanVersion2), 2);
	}
}
