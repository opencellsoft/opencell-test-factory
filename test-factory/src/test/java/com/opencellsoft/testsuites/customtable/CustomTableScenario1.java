package com.opencellsoft.testsuites.customtable;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import java.io.IOException;
import java.sql.SQLException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.tests.CustomTableTests;
import com.opencellsoft.utility.Constant;
import com.opencellsoft.utility.TestUtil;

public class CustomTableScenario1 extends CustomTableCommons {
	
	String tableCode = "db_table_" + Constant.consumerCode.toLowerCase();
//	String tableCode = "db_table_wps9616";
	String tableName = tableCode;
	String description = tableCode;
	String year = "501";
	String city = "Paris";
	String country = "France";
	String resident = "201";

	@Test(priority = 0, enabled = true)
	public void _CT_T1() {
		System.out.println("***************** Custom Table tests  *****************");
	}
	
	/***   Create a new entity customization  in admin ***/
	@Test(priority = 6, enabled = true)
	public void _CT_T1_createEntityCustomization() {
		/***   open admin   ***/
		driver.get(baseURI);
		waitPageLoaded();
		
		/***   Create a new entity customization   ***/
		customTableTests.goTocustomizedEntitiesPage();
		customTableTests.createNewCustomizedEntity(tableCode, tableName, description);
		
		/***   Add custom field  ***/
		customTableTests.addNewCustomField("Annee", "Year", "Long");
		customTableTests.addNewCustomField("Pays", "Country", "String");
		customTableTests.addNewCustomField("Ville", "City", "String");
		customTableTests.addNewCustomField("habitant", "resident", "Long");
		
		/***   close admin   ***/
		driver.get(baseURI + "frontend/DEMO/portal");
		homeTests.goToPortal();
	}
	
	/***   go to custom tables page  ***/
	@Test(priority = 12, enabled = true)
	public void _CT_T1_goToCustomTables() {
		customTableTests.goToCustomTable();
	}
	
	/***   search for a custom table  ***/
	@Test(priority = 14, enabled = true)
	public void _CT_T1_searchForCustomTable() {
		customTableTests.searchForCustomTable(tableCode);
	}
	
	/***   add a new line  ***/
	@Test(priority = 16, enabled = true)
	public void _CT_T1_insertNewLine() {
		//customTableTests.insertNewLine(year, city, country, resident);
		customTableTests.insertNewLine(year, city, country, resident);
	}
	
	/***   display the line of the custom table  ***/
	@Test(priority = 18, enabled = true)
	public void _CT_T1_displayTheLineOfTheCustomTable() {
		assertTrue(customTableTests.checkValuesInCustomTable(year, country, city, resident));
	}
	
	/***   add a new line   ***/
	@Parameters({"baseURI" })
	@Test(priority = 20, enabled = true)
	public void _CT_T1_insert1000Line(String baseURI) throws SQLException, ClassNotFoundException {
		testUtil = new TestUtil();
		String dbURI = baseURI.replace("/opencell/", "/db/");
		String request = "DO $$ \r\n"
						+ "BEGIN\r\n"
						+ "   FOR i IN 2..1000 LOOP\r\n"
						+ "INSERT INTO \"" + tableName +"\" (\"id\", \"annee\", \"pays\", \"ville\", \"habitant\") \r\n"
						+ "VALUES (i, i+500, 'France', 'Paris', i+1000);\r\n"
						+ "   END LOOP;\r\n"
						+ "END $$;";
		System.out.println(request);
		customTableTests = new CustomTableTests();
		customTableTests.insertLines(dbURI, request);
	}
	
	/***   go to custom tables page  ***/
	@Test(priority = 22, enabled = true)
	public void _CT_T1_goToCustomTablesAgain() {
		customTableTests.goToCustomTable();
	}
	
	/***   search for a custom table  ***/
	@Test(priority = 23, enabled = true)
	public void _CT_T1_searchForCustomTableAgain() {
		customTableTests.searchForCustomTable(tableCode);
	}
	
	/***   check the pagination  ***/
	@Test(priority = 24, enabled = true)
	public void _CT_T1_checkPagination() {
		if(customTableTests.valueIsPresent("of") == false) {
			customTableTests.searchForCustomTable(tableCode);
		}
		assertTrue(customTableTests.valueIsPresent("of"));
		assertTrue(customTableTests.valueIsPresent("Rows per page:"));
	}
	
	/***   check the pagination  ***/
	@Test(priority = 24, enabled = true)
	public void _CT_T1_updatePagination() {
		customTableTests.updatePagination25();
	}
	
	/***   go To The Second Page  ***/
	@Test(priority = 26, enabled = true)
	public void _CT_T1_goToTheSecondPage() {
		customTableTests.nextPage();
	}
	
	/***   come Back To The First Page  ***/
	@Test(priority = 28, enabled = true)
	public void _CT_T1_comeBackToTheFirstPage() {
		customTableTests.previousPage();
	}
	
	/***   delete Line  ***/
	@Test(priority = 30, enabled = true)
	public void _CT_T1_deleteFirstLine() {
		assertFalse(customTableTests.rowIsPresent(year));
		customTableTests.deleteLine();
		assertTrue(customTableTests.rowIsPresent(year));
	}
	
	/***    downlaod CSV  ***/
	@Test(priority = 32, enabled = true)
	public void _CT_T1_downlaodCSV() throws IOException, InterruptedException {
		customTableTests.downlaodCSV();
//		customTableTests.downlaodCSV();
		waitForTheFileToDownload(tableCode);
	} 
	
	/***    downlaod XLSX  
	 * @throws InterruptedException 
	 * @throws IOException ***/
	@Test(priority = 34, enabled = true)
	public void _CT_T1_downlaodXLSX() throws IOException, InterruptedException {
		customTableTests.downlaodXLSX();
		waitForTheFileToDownload(tableCode);
	}
	
}
