package com.opencellsoft.testsuites.customtable;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.tests.CustomTableTests;
import com.opencellsoft.tests.HomeTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.utility.TestUtil;

public class CustomTableCommons extends TestBase {

	LoginTest loginTest;
	HomeTests homeTests;
	CustomTableTests customTableTests;
	TestUtil testUtil;
	
	String version;
	String baseURI; 
	String login;
	String password;
	
	public CustomTableCommons() {
		loginTest = new LoginTest();
		homeTests = new HomeTests();
		customTableTests = new CustomTableTests();
		testUtil = new TestUtil();
	}
	
	/***  setup  ***/
	@BeforeClass
	@Parameters(value = {"baseURI", "browser", "nodeURL", "login", "password", "version" })
	public void setup(String baseURI, String browser, String nodeURL, String login, String password, String version) throws Exception{
		initialize( browser, nodeURL, baseURI); 
		this.version = version;
		this.baseURI = baseURI; 
		this.login = login;
		this.password = password;
		
		loginTest.loginTest(login, password);
		homeTests.goToPortal();
	}

	/***  Teardown  ***/
	@AfterClass
	public void Teardown() {
		TeardownTest();
	}
	
	 public static void waitForTheFileToDownload(String fileName)
             throws IOException, InterruptedException {
		 String home = System.getProperty("user.home");
         File dir = new File( home + "/Downloads/"); 
         System.out.println(home + "/Downloads/");
         File[] dirContents = dir.listFiles();

         for (int i = 0; i < 3; i++) {
             if (dirContents[i].getName().contains(fileName)) {
            	 
                 break;
             }else {
                 Thread.sleep(5);
             }
         }
     }
}
