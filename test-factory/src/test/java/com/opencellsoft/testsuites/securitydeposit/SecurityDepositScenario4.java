package com.opencellsoft.testsuites.securitydeposit;

import java.awt.AWTException;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.tests.CustomersTests;
import com.opencellsoft.tests.HomeTests;
import com.opencellsoft.tests.InvoicesTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.tests.SecurityDepositTests;
import com.opencellsoft.utility.Constant;

public class SecurityDepositScenario4 extends SecurityDepositCommons {
	/****************************************************************************************************
	 **    Security Deposit : pay an invoice with part of the SD amount  and refund the rest  ***********
	 - Create a New Security Deposit (Amount = 1000)
	 - Instantiate Security Deposit => new invoice is generated
	 - CREDIT security deposit()
	 - Check Security Deposit Status : Locked
	 - Check Invoice Status : Paid
	 - Create an invoice total = 500
	 - Go to balance
	 - Pay the invoice with the SD
	 - Check the security deposit status = UNLOCKED, current balance = 500
	 - Check invoice status = PAID, Payement ref =  SD
	 - Refund the rest of SD 
	 - Check invoice generated type = Adjustement ,amount = 500
	 - Check the security deposit status = UNLOCKED(to be changed), current balance = 0
	 ***************************************************************************************************/
	SecurityDepositDataSet sd = new SecurityDepositDataSet();
	String customerCode = sd.customerCode;
	
	String securityDepositName = Constant.securityDepositName + "_04";
	String securityDepositDescription = Constant.securityDepositDescription + "_04";
	String articleCode = "ART_SECURITY_DEPOSIT";
	String amount = "1000";
	String reference = "Ref" + amount;
	String invoiceQuantity = "50";
	String invoicePrice = "10";
	String balance = "€500.00";

	/***   login   ***/
	@Parameters({ "login", "password" })
	@Test(priority = 1, enabled = true)
	public void login(String login, String password) throws Exception {
		loginTest = new LoginTest();
		loginTest.loginTest(login, password);
	}
	
	/***  Create Security Deposit   ***/
	@Parameters({ "securityDepositTemplateName", "securityDepositExpectedBalance" })
	@Test(priority = 2)
	public void createSecurityDeposit(String securityDepositTemplateName,String securityDepositExpectedBalance) {
		customersTests = new CustomersTests();
		customersTests.searchForCustomerMethod(customerCode);
		customersTests.goToSecurityDepositTab();
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.createSecurityDeposit(securityDepositTemplateName, securityDepositName, securityDepositDescription, securityDepositExpectedBalance);
	}

	/***  Instantiate Security Deposit   ***/
	@Test(priority = 4)
	public void instantiateSecurityDeposit() {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.InstantiateSecurityDeposit();
	}

	/***  credit Security Deposit   ***/
	@Test(priority = 6)
	public void creditSecurityDeposit1000() {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.creditSecurityDeposit( amount, reference);
	}

	/***  create Invoice  ***/
	@Test(priority = 8)
	public void createInvoice500() throws AWTException {
		customersTests = new CustomersTests();
		invoicesTests = new InvoicesTests();
		homeTests = new HomeTests();
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);
		customersTests.goToInvoiceTab();
		securityDepositTests = new SecurityDepositTests();
		invoicesTests.createCommercialInvoice();
		invoicesTests.createInvoiceLine( articleCode, invoiceQuantity, invoicePrice, "00");
		invoiceNumber = invoicesTests.validateTheInvoice();
//		invoiceNumber = invoicesTests.getInvoiceNumber();
	}

	/***  create Invoice  ***/
	@Test(priority = 10)
	public void GoToBalance() throws AWTException {
		customersTests.goToBalanceTab();
	}

	/***  pay Invoice With SD  ***/
	@Test(priority = 12)
	public void payInvoiceWithSD() throws AWTException {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.payInvoiceWithSD(securityDepositName);
	}

	/***  Check the security deposit status = UNLOCKED, current balance = 500  ***/
	@Test(priority = 14)
	public void checkSecurityDepositStatusUnlockedAndBalance500() throws AWTException {
		customersTests.goToSecurityDepositTab() ;
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.searchForSecurityDeposit();
		securityDepositTests.checkSDStatusUnlocked();
		securityDepositTests.checkSDBalanceEqual(balance);
	}
	
	/***  Check invoice status = PAID, Payement ref =  SD  ***/
	@Test(priority = 16)
	public void checkInvoiceStatusPaid() throws AWTException {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.checkInvoiceStatusPaid();
	}
	
	/***  Refund the rest of SD  ***/
	@Parameters({ "refundSecurityDepositReason"})
	@Test(priority = 18, enabled = false)
	public void refundSecurityDeposit500(String reason) {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.refundSecurityDeposit( reason);
	}
	
	/***  Check invoice generated type = Adjustement ,amount = 500  ***/
	@Test(priority = 20, enabled = false)
	public void checkGeneratedInvoiceTypeIsAdjustement() {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.checkGeneratedInvoiceTypeAdjustement();
	}
	
	/***   Check the security deposit status = UNLOCKED(to be changed), current balance = 0   ***/
	@Test(priority = 22, enabled = false)
	public void checkSecurityDepositStatusAndBalance0() throws AWTException {
		customersTests.goToSecurityDepositTab() ;
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.searchForSecurityDeposit();
		securityDepositTests.checkSDStatusUnlocked();
		securityDepositTests.checkSDBalanceEqual("€0.00");
	}
}
