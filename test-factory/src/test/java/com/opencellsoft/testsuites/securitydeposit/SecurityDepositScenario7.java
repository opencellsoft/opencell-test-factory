package com.opencellsoft.testsuites.securitydeposit;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.tests.CustomersTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.tests.SecurityDepositTests;
import com.opencellsoft.utility.Constant;

public class SecurityDepositScenario7 extends SecurityDepositCommons {
	/****************************************************************************************************
	 ********************            Security Desposit : HOLD + REFUND            **********************
	 - Create a New Security Deposit  amount 1000
	 - Instantiate Security Deposit => new invoice is generated & ( SD status =  VALIDATED )
	 - CREDIT Security Deposit 500
	 - Check Security Deposit Status = HOLD
	 - Refund SD
	 - Check Security Deposit Status = REFUNDED
	 - Check invoice generated type = Adjustement , amount = 500, status = REFUNDED
	 ***************************************************************************************************/
	SecurityDepositDataSet sd = new SecurityDepositDataSet();
	String customerCode = sd.customerCode;
	
	String securityDepositName = Constant.securityDepositName + "_07";
	String securityDepositDescription = Constant.securityDepositDescription + "_07";
	//String articleCode = Constant.articleCode;
	String articleCode = "ART_SECURITY_DEPOSIT";
	String amount = "500";
	String reference = "Ref" + amount;
	
	/***   login   ***/
	@Parameters({ "login", "password" })
	@Test(priority = 1, enabled = true)
	public void login(String login, String password) throws Exception {
		loginTest = new LoginTest();
		loginTest.loginTest(login, password);
	}
	
	/***  Create Security Deposit   ***/
	@Parameters({ "securityDepositTemplateName", "securityDepositExpectedBalance" })
	@Test(priority = 2)
	public void createSecurityDeposit(String securityDepositTemplateName,String securityDepositExpectedBalance) {
		customersTests = new CustomersTests();
		customersTests.searchForCustomerMethod(customerCode);
		customersTests.goToSecurityDepositTab();
		waitPageLoaded();
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.createSecurityDeposit(securityDepositTemplateName, securityDepositName, securityDepositDescription, securityDepositExpectedBalance);
	}

	/***  Instantiate Security Deposit   ***/
	@Test(priority = 4)
	public void instantiateSecurityDeposit() {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.InstantiateSecurityDeposit();
	}

	/***  credit Security Deposit   ***/
	@Test(priority = 6)
	public void creditSecurityDeposit500() {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.creditSecurityDeposit( amount, reference);
	}
	
	/***  check Security Deposit Status : Hold  ***/
	@Test(priority = 8)
	public void checkSecurityDepositStatusIsHold() {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.checkSDStatusHold();
	}
	
	/***  refund Security Deposit   ***/
	@Parameters({ "refundSecurityDepositReason"})
	@Test(priority = 10, enabled = true)
	public void refundSecurityDeposit(String reason) {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.refundSecurityDeposit( reason);
	}

	/***  check Security Deposit Status : Refunded   ***/
	@Test(priority = 12, enabled = true)
	public void checkSecurityDepositStatusIsRefunded() {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.checkSDStatusRefunded();
	}
	
	/***  check Generated Invoice Type : Adjustement   ***/
	@Test(priority = 14, enabled = true)
	public void checkGeneratedInvoiceTypeIsAdjustement() {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.checkGeneratedInvoiceTypeAdjustement();
	}
}
