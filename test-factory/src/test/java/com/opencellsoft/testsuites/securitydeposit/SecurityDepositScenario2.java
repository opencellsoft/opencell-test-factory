package com.opencellsoft.testsuites.securitydeposit;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.tests.CustomersTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.tests.SecurityDepositTests;
import com.opencellsoft.utility.Constant;

public class SecurityDepositScenario2 extends SecurityDepositCommons {
	/****************************************************************************************************
	 ***************************      Security Deposit : CANCEL before credit  **************************
	 - Create a New Security Deposit
	 - Instantiate Security Deposit => new invoice is generated
	 - Check the generated invoice (status = VALIDATED, Payment status = PENDING, invoice type = SD - Security Deposit)
	 - Cancel Security Deposit
	 - check Security Deposit Status : Canceled
	 - check the invoice status = Abondoned
	 ***************************************************************************************************/
	SecurityDepositDataSet sd = new SecurityDepositDataSet();
	String customerCode = sd.customerCode;
	
	String securityDepositName = Constant.securityDepositName + "_02";
	String securityDepositDescription = Constant.securityDepositDescription + "_02";

	
	/***   login   ***/
	@Parameters({ "login", "password" })
	@Test(priority = 1, enabled = true)
	public void login(String login, String password) throws Exception {
		loginTest = new LoginTest();
		loginTest.loginTest(login, password);
	}
	
	/***  Create Security Deposit   ***/
	@Parameters({ "securityDepositTemplateName", "securityDepositExpectedBalance" })
	@Test(priority = 2)
	public void createSecurityDeposit(String securityDepositTemplateName,String securityDepositExpectedBalance) {
		customersTests = new CustomersTests();
		customersTests.searchForCustomerMethod(customerCode);
		customersTests.goToSecurityDepositTab();
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.createSecurityDeposit(securityDepositTemplateName, securityDepositName, securityDepositDescription, securityDepositExpectedBalance);
	}
	
	/***  Instantiate Security Deposit   ***/
	@Test(priority = 3)
	public void instantiateSecurityDeposit() {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.InstantiateSecurityDeposit();
	}
	
	/***  Cancel Security Deposit   ***/
	@Parameters({ "cancelSecurityDepositReason"})
	@Test(priority = 4)
	public void cancelSecurityDeposit(String reason) {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.cancelSecurityDeposit(reason);
	}
	
	/***  check Security Deposit Status = Canceled   ***/
	@Test(priority = 5)
	public void checkSecurityDepositStatusIsCanceled (){
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.checkSDStatusCanceled();
	}
	
	/***  check Invoice Status = Abondoned   ***/
	@Test(priority = 6, enabled = false)
	public void CheckInvoicestatusIsAbondoned (){
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.checkInvoiceStatusAbondoned();
	}
	

}
