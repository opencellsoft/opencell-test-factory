package com.opencellsoft.testsuites.securitydeposit;

import java.awt.AWTException;
import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.pages.finance.reports.SecurityDepositReportPage;
import com.opencellsoft.tests.CustomersTests;
import com.opencellsoft.tests.HomeTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.tests.SecurityDepositTests;
import com.opencellsoft.utility.Constant;

public class SecurityDepositScenario8 extends SecurityDepositCommons {
	/****************************************************************************************************
	 ********************            Security Desposit : Report              ****************************
	 - Create a New Security Deposit  amount 1000
	 - Instantiate Security Deposit => new invoice is generated & ( SD status =  VALIDATED )
	 - CREDIT Security Deposit 500
	 - Check Security Deposit Status = HOLD
	 - Refund SD
	 - Check Security Deposit Status = REFUNDED
	 - Check invoice generated type = Adjustement , amount = 500, status = REFUNDED
	 ***************************************************************************************************/
	String securityDepositName = Constant.securityDepositName;
	String securityDepositDescription = Constant.securityDepositDescription;
	String customerCode = "SDCUST2-" + Constant.customerCode;
	String articleCode = "ART_SECURITY_DEPOSIT";
	String amount = "1000";
	String reference = "Ref" + amount;
	String invoiceQuantity = "50";
	String invoicePrice = "10";
	String balance = "€500.00";

	/***   login   ***/
	@Parameters({ "login", "password" })
	@Test(priority = 1, enabled = true)
	public void login(String login, String password) throws Exception {
		loginTest = new LoginTest();
		homeTests = new HomeTests();
		loginTest.loginTest(login, password);
		customersTests = new CustomersTests();
	}

	/***  Create a New Customer   ***/
	@Parameters({ "billingCountry", "referenceMandate", "iban", "bic", "billingCycle", "acrs" })
	@Test(priority = 2)
	public void createCustomer(String billingCountry, String referenceMandate, String iban, String bic, String billingCycle, String acrs) throws InterruptedException {
		customersTests.createCustomerWithDirectDebitAsPaymentMethod(billingCountry,customerCode, referenceMandate, iban, bic,billingCycle, acrs);
	}

	/***  Create a New Consumer   ***/
	@Parameters({ "legalForm" })
	@Test(priority = 3)
	public void createConsumer(String legalForm) throws InterruptedException {
		customersTests.addNewCompanyConsumer(legalForm);
	}

	/***  Create DRAFT SD   ***/
	@Parameters({ "securityDepositTemplateName", "securityDepositExpectedBalance" })
	@Test(priority = 4)
	public void createDraftSD(String securityDepositTemplateName,String securityDepositExpectedBalance) {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		customersTests.goToSecurityDepositTab();
		waitPageLoaded();
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.createSecurityDeposit(securityDepositTemplateName, securityDepositName + "_10", securityDepositDescription, securityDepositExpectedBalance);
	}

	/***  Create Validated SD  ***/
	@Parameters({ "securityDepositTemplateName", "securityDepositExpectedBalance" })
	@Test(priority = 5)
	public void createValidatedSD(String securityDepositTemplateName,String securityDepositExpectedBalance) {
		customersTests.searchForCustomerMethod(customerCode);
		customersTests.goToSecurityDepositTab();
		waitPageLoaded();
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.createSecurityDeposit(securityDepositTemplateName, securityDepositName + "_11", securityDepositDescription, securityDepositExpectedBalance);
		securityDepositTests.InstantiateSecurityDeposit();
	}

	/***  Create Refunded SD  ***/
	@Parameters({ "securityDepositTemplateName", "securityDepositExpectedBalance", "refundSecurityDepositReason" })
	@Test(priority = 6, enabled = false)
	public void createRefundedSD(String securityDepositTemplateName,String securityDepositExpectedBalance, String reason) {
		customersTests.searchForCustomerMethod(customerCode);
		customersTests.goToSecurityDepositTab();
		waitPageLoaded();
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.createSecurityDeposit(securityDepositTemplateName, securityDepositName + "_12", securityDepositDescription, securityDepositExpectedBalance);
		securityDepositTests.InstantiateSecurityDeposit();
		securityDepositTests.creditSecurityDeposit(amount, reference);
		securityDepositTests.refundSecurityDeposit(reason);
	}

	/***  Create Hold SD  ***/
	@Parameters({ "securityDepositTemplateName", "securityDepositExpectedBalance", })
	@Test(priority = 8, enabled = true)
	public void createHoldSD(String securityDepositTemplateName,String securityDepositExpectedBalance) {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);
		customersTests.goToSecurityDepositTab();
		waitPageLoaded();
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.createSecurityDeposit(securityDepositTemplateName, securityDepositName + "_13", securityDepositDescription, securityDepositExpectedBalance);
		securityDepositTests.InstantiateSecurityDeposit();
		securityDepositTests.creditSecurityDeposit("500", reference);
		securityDepositTests.checkSDStatusHold();
	}

	/***  Create Canceled SD  ***/
	@Parameters({ "securityDepositTemplateName", "securityDepositExpectedBalance", "cancelSecurityDepositReason" })
	@Test(priority = 10)
	public void createCanceledSD(String securityDepositTemplateName,String securityDepositExpectedBalance, String reason) {
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);

		customersTests.goToSecurityDepositTab();
		waitPageLoaded();
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.createSecurityDeposit(securityDepositTemplateName, securityDepositName + "_14", securityDepositDescription, securityDepositExpectedBalance);
		securityDepositTests.InstantiateSecurityDeposit();
		securityDepositTests.cancelSecurityDeposit(reason);
		securityDepositTests.checkSDStatusCanceled();
	}

	/***  Create Locked SD  ***/
	@Parameters({ "securityDepositTemplateName", "securityDepositExpectedBalance" })
	@Test(priority = 12)
	public void createLockedSD(String securityDepositTemplateName,String securityDepositExpectedBalance) {
		customersTests.searchForCustomerMethod(customerCode);
		customersTests.goToSecurityDepositTab();
		waitPageLoaded();
		securityDepositTests.createSecurityDeposit(securityDepositTemplateName, securityDepositName + "_15", securityDepositDescription, securityDepositExpectedBalance);
		securityDepositTests.InstantiateSecurityDeposit();
		securityDepositTests.creditSecurityDeposit(amount , reference);
		securityDepositTests.checkSDStatusLocked();
	}

	/***  Create Unlocked SD  ***/
	@Parameters({ "securityDepositTemplateName", "securityDepositExpectedBalance" })
	@Test(priority = 14)
	public void createUnlockedSD(String securityDepositTemplateName,String securityDepositExpectedBalance) throws AWTException {
		customersTests.searchForCustomerMethod(customerCode);
		customersTests.goToSecurityDepositTab();
		waitPageLoaded();
		securityDepositTests.createSecurityDeposit(securityDepositTemplateName, securityDepositName + "_16", securityDepositDescription, securityDepositExpectedBalance);
		securityDepositTests.InstantiateSecurityDeposit();
		securityDepositTests.creditSecurityDeposit(amount , reference);
		customersTests.searchForCustomerMethod(customerCode);
		customersTests.goToInvoiceTab();
		invoicesTests.createCommercialInvoice();
		invoicesTests.createInvoiceLine( articleCode, invoiceQuantity, invoicePrice, "00");
		invoiceNumber = invoicesTests.validateTheInvoice();
		customersTests.goToBalanceTab();
		securityDepositTests.payInvoiceWithSD(securityDepositName);
		customersTests.goToSecurityDepositTab() ;
		securityDepositTests.searchForSecurityDeposit();
		securityDepositTests.checkSDStatusUnlocked();
		securityDepositTests.checkSDBalanceEqual(balance);
	}

	/***  Go to SD report  ***/
	@Test(priority = 16)
	public void GoToSDReport() {
		securityDepositTests.GoToSDReport();
	}

	/***  search By Customer In SD Report  ***/
	@Test(priority = 18)
	public void searchByCustomerInSDReport() {
		securityDepositTests.searchByCustomerInSDReport(customerCode);
	}

	/***  Check Security Deposit Report table  ***/
	@Test(priority = 20)
	public void checkSecurityDepositReportTable() {
		List<SecurityDepositReportPage> securityList = new ArrayList<SecurityDepositReportPage>();
		securityList.add(new SecurityDepositReportPage(customerCode,"DRAFT","€1,500.00"));
		securityList.add(new SecurityDepositReportPage("","VALIDATED",""));
		securityList.add(new SecurityDepositReportPage("","CANCELED",""));
		securityList.add(new SecurityDepositReportPage("","LOCKED",""));
		securityList.add(new SecurityDepositReportPage("","UNLOCKED",""));
		new SecurityDepositReportPage().checkSecurityDepositTable(securityList);
	}
}
