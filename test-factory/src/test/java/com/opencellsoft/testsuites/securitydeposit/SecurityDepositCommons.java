package com.opencellsoft.testsuites.securitydeposit;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.commons.HomePage;
import com.opencellsoft.pages.commons.LoginPage;
import com.opencellsoft.pages.customercare.balance.BalancePage;
import com.opencellsoft.pages.customercare.securitydeposits.ListOfSecurityDepositPage;
import com.opencellsoft.pages.customercare.securitydeposits.NewSecurityDepositPage;
import com.opencellsoft.pages.customercare.securitydeposits.SecurityDepositPage;
import com.opencellsoft.pages.customers.CustomerInvoiceTabPage;
import com.opencellsoft.pages.customers.CustomerPage;
import com.opencellsoft.pages.customers.ListOfCustomersPage;
import com.opencellsoft.pages.customers.NewCustomerPage;
import com.opencellsoft.pages.finance.reports.SecurityDepositReportPage;
import com.opencellsoft.pages.invoices.InvoicePage;
import com.opencellsoft.pages.invoices.NewInvoiceLinePage;
import com.opencellsoft.pages.invoices.NewInvoicePage;
import com.opencellsoft.tests.ArticlesTests;
import com.opencellsoft.tests.CustomersTests;
import com.opencellsoft.tests.HomeTests;
import com.opencellsoft.tests.InvoicesTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.tests.SecurityDepositTests;
import com.opencellsoft.testsuites.mediation.EDRTestsCommons;

public class SecurityDepositCommons extends TestBase {

	HomePage homePage;
	LoginPage loginPage;
	ListOfCustomersPage listOfCustomersPage;
	NewCustomerPage newCustomerPage;
	EDRTestsCommons edrTestsCommons;
	CustomerPage customerPage;
	CustomerInvoiceTabPage customerInvoiceTabPage;
	ListOfSecurityDepositPage  listOfSecurityDepositPage;
	NewSecurityDepositPage newSecurityDepositPage;
	SecurityDepositPage securityDepositPage;
	InvoicePage invoicePage;
	NewInvoicePage newInvoicePage;
	NewInvoiceLinePage newInvoiceLinePage;
	BalancePage balancePage;
	SecurityDepositReportPage securityDepositReportPage;
	LoginTest loginTest;
	HomeTests homeTests;
	CustomersTests customersTests;
	ArticlesTests articlesTests;
	SecurityDepositTests securityDepositTests;
	InvoicesTests invoicesTests;
	String invoiceNumber;
	String SDName;
	/***  setup  
	 * @throws Exception ***/
	@BeforeClass
	@Parameters(value = { "browser", "nodeURL", "baseURI" })
	public void setup(String browser, String nodeURL, String baseURI) throws IOException{
		initialize( browser, nodeURL, baseURI); 
	}

	/***  Teardown  ***/
	@AfterClass
	public void Teardown() {
		TeardownTest();
	}


	


}
