package com.opencellsoft.testsuites.securitydeposit;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.pages.customercare.securitydeposits.SecurityDepositPage;
import com.opencellsoft.tests.CustomersTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.tests.SecurityDepositTests;
import com.opencellsoft.utility.Constant;

public class SecurityDepositScenario3 extends SecurityDepositCommons {
	/****************************************************************************************************
	 ***************************      Security Deposit : CANCEL after credit  **************************
	 - Create a New Security Deposit
	 - Instantiate Security Deposit => new invoice is generated
	 - Check the generated invoice (status = VALIDATED, Payment status = PENDING, invoice type = SD - Security Deposit)
	 - CREDIT security deposit()
	 - Check Security Deposit Status : Locked
	 - Check Invoice Status : Paid
	 - Cancel Security Deposit
	 - check Security Deposit Status : CANCELED
	 - check Invoice
	 - check payment
	 ***************************************************************************************************/
	SecurityDepositDataSet sd = new SecurityDepositDataSet();
	String customerCode = sd.customerCode;
	
	String securityDepositName = Constant.securityDepositName + "_03";
	String securityDepositDescription = Constant.securityDepositDescription + "_03";

	String amount = "1000";
	String reference = "Ref" + amount;
	
	/***   login   ***/
	@Parameters({ "login", "password" })
	@Test(priority = 1)
	public void login(String login, String password) throws Exception {
		loginTest = new LoginTest();
		loginTest.loginTest(login, password);
	}
	
	/***  Create Security Deposit   ***/
	@Parameters({ "securityDepositTemplateName", "securityDepositExpectedBalance" })
	@Test(priority = 2)
	public void createSecurityDeposit(String securityDepositTemplateName,String securityDepositExpectedBalance) {
		customersTests = new CustomersTests();
		customersTests.searchForCustomerMethod(customerCode);
		customersTests.goToSecurityDepositTab();
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.createSecurityDeposit(securityDepositTemplateName, securityDepositName, securityDepositDescription, securityDepositExpectedBalance);
	}
	
	/***  Instantiate Security Deposit   ***/
	@Test(priority = 4)
	public void instantiateSecurityDeposit() {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.InstantiateSecurityDeposit();
	}
	
	/***  credit Security Deposit   ***/
	@Test(priority = 6)
	public void creditSecurityDeposit() {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.creditSecurityDeposit( amount, reference);
	}
	
	/***  Cancel Security Deposit   ***/
	@Parameters({ "cancelSecurityDepositReason"})
	@Test(priority = 8)
	public void cancelSecurityDeposit(String reason) {
		securityDepositPage = new SecurityDepositPage();
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.cancelSecurityDeposit(reason);
//		Assert.assertTrue(securityDepositPage.getTextMessage().toLowerCase().contains("Only Security Deposit with validated status can be canceled"));
	}
	
	/***  check Security Deposit Status = Canceled   ***/
	@Test(priority = 10, enabled = false)
	public void checkSecurityDepositStatusIsLocked (){
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.checkSDStatusLocked();
	}
}
