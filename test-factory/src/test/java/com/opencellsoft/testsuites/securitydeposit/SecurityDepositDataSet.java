package com.opencellsoft.testsuites.securitydeposit;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.tests.ArticlesTests;
import com.opencellsoft.tests.CustomersTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.utility.Constant;

public class SecurityDepositDataSet extends SecurityDepositCommons {

	/********************************************************************************************
	 ***********************************      Pre-request    ************************************
	 - Create a New Customer (payment by card , type : visa , number : 4111 1111 1111 1111)
	 - Create article
	 ********************************************************************************************/
//	String customerCode = "SDCUST1-" + Constant.customerCode;
	String customerCode = "SDCUST1-FCI4578";
	String customerName = Constant.customerName;
	String customerEmail = Constant.customerEmail;
	String paypalUserId = Constant.paypalUserId;

	String consumerCode = "SD" + Constant.consumerCode;
	String consumerName = Constant.consumerName;
	String companyRegistrationNumber = Constant.companyRegistrationNumber;
	String vatNumber = Constant.vatNumber;
	
	/***   login   ***/
	@Parameters({ "login", "password" })
	@Test(priority = 1, enabled = true)
	public void login(String login, String password) throws Exception {
		loginTest = new LoginTest();
		loginTest.loginTest(login, password);
	}
	
	/***  Create a New Customer & New Consumer  ***/
	@Parameters({ "billingCountry", "referenceMandate", "iban", "bic", "billingCycle", "acrs", "legalForm" })
	@Test(priority = 2)
	public void createCustomer(String billingCountry, String referenceMandate, String iban, String bic, String billingCycle, String acrs, String legalForm) throws InterruptedException {
		customersTests = new CustomersTests();
		customersTests.createCustomerWithDirectDebitAsPaymentMethod(billingCountry,customerCode, referenceMandate, iban, bic,billingCycle, acrs);
		customersTests.addNewCompanyConsumer(legalForm);
	}

}
