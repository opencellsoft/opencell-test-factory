package com.opencellsoft.testsuites.securitydeposit;

import java.awt.AWTException;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.tests.CustomersTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.tests.SecurityDepositTests;
import com.opencellsoft.utility.Constant;

public class SecurityDepositScenario6 extends SecurityDepositCommons  {
	/****************************************************************************************************
	 ********************            Security Desposit : HOLD to LOCKED            **********************
	 - Create a New Security Deposit  amount 1000
	 - Instantiate Security Deposit => new invoice is generated & ( SD status =  VALIDATED )
	 - CREDIT Security Deposit 500
	 - Check Security Deposit Status = HOLD
	 - Check the invoice  (status VALIDATED, Payment status PART-PAID, payement MATCHED)
	 - Credit Security Deposit 500
	 - Check Security Deposit Status = LOCKED
	 - Check the invoice  (status VALIDATED, Payment status PAID, payement MATCHED with 2 SD)

	 ***************************************************************************************************/
	SecurityDepositDataSet sd = new SecurityDepositDataSet();
	String customerCode = sd.customerCode;
	
	String securityDepositName = Constant.securityDepositName + "_06";
	String securityDepositDescription = Constant.securityDepositDescription + "_06";
	String articleCode = "ART_SECURITY_DEPOSIT";
	String amount = "500";
	String reference = "Ref" + amount;
	
	/***   login   ***/
	@Parameters({ "login", "password" })
	@Test(priority = 1, enabled = true)
	public void login(String login, String password) throws Exception {
		loginTest = new LoginTest();
		loginTest.loginTest(login, password);
	}
	
	/***  Create Security Deposit   ***/
	@Parameters({ "securityDepositTemplateName", "securityDepositExpectedBalance" })
	@Test(priority = 2)
	public void createSecurityDeposit(String securityDepositTemplateName,String securityDepositExpectedBalance) {
		customersTests = new CustomersTests();
		customersTests.searchForCustomerMethod(customerCode);
		customersTests.goToSecurityDepositTab();
		waitPageLoaded();
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.createSecurityDeposit(securityDepositTemplateName, securityDepositName, securityDepositDescription, securityDepositExpectedBalance);
	}

	/***  Instantiate Security Deposit   ***/
	@Test(priority = 4)
	public void instantiateSecurityDeposit() {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.InstantiateSecurityDeposit();
	}

	/***  credit Security Deposit   ***/
	@Test(priority = 6)
	public void creditSecurityDepositFirst500() {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.creditSecurityDeposit( amount, reference);
	}
	
	/***  check Security Deposit Status : Hold  ***/
	@Test(priority = 8)
	public void checkSecurityDepositStatusIsHold() {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.checkSDStatusHold();
	}
	
	/***  check Invoice : status = VALIDATED, Payment status = PART-PAID, payement = MATCHED  ***/
	@Test(priority = 10)
	public void checkInvoiceStatusIsPartPaid() {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.checkInvoiceStatusPartPaid();
	}
	
	/***  credit Security Deposit   ***/
	@Parameters({"creditSecurityDepositReference" })
	@Test(priority = 12)
	public void creditSecurityDepositSeconf500( String reference) {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.creditSecurityDeposit( amount, reference);
	}
	
	/***  check Security Deposit Status : Locked  ***/
	@Test(priority = 14)
	public void checkSecurityDepositStatusIsLocked() {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.checkSDStatusLocked();
	}
	
	/***  Check invoice : status = VALIDATED, Payment status = PAID, payement = MATCHED with 2 SD  ***/
	@Test(priority = 16)
	public void checkInvoiceStatusPaid() throws AWTException {
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.checkInvoiceStatusPaid();
	}
	
}
