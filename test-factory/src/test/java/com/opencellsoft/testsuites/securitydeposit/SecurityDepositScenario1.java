package com.opencellsoft.testsuites.securitydeposit;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.tests.CustomersTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.tests.SecurityDepositTests;
import com.opencellsoft.utility.Constant;

public class SecurityDepositScenario1 extends SecurityDepositCommons {

	/****************************************************************************************************
	 ***********************************      Security Deposit S1    ************************************
	 - Create a New Security Deposit(Expected balance = 1000 €)
	 - Instantiate Security Deposit => new invoice is generated
	 - Check the generated invoice (status = VALIDATED, Payment status = PENDING, invoice type = SD - Security Deposit)
	 - CREDIT security deposit()
	 - Check Security Deposit Status : Locked
	 - Check Invoice Status : Paid
	 - Refund Security Deposit
	 - check Security Deposit Status : Refunded
	 - check Generated Invoice  (status = VALIDATED, Payment status = REFUNDED, Type = Adjustement)
	 ***************************************************************************************************/
	SecurityDepositDataSet sd = new SecurityDepositDataSet();
	String customerCode =sd.customerCode;
	
	String securityDepositName = Constant.securityDepositName + "_01";
	String securityDepositDescription = Constant.securityDepositDescription + "_01";
	String amount = "1000";
	String reference = "Ref" + amount;
	
	/***   login   ***/
	@Parameters({ "login", "password" })
	@Test(priority = 1, enabled = true)
	public void login(String login, String password) throws Exception {
		loginTest = new LoginTest();
		loginTest.loginTest(login, password);
	}
	
	/***  Create Security Deposit   ***/
	@Parameters({ "securityDepositTemplateName", "securityDepositExpectedBalance" })
	@Test(priority = 2)
	public void createSecurityDeposit(String securityDepositTemplateName,String securityDepositExpectedBalance) {
		customersTests = new CustomersTests();
		// Go to customers list page
		homeTests.goToCustomTablesListPage();

		// search for a customer and go to customer details page
		customersTests.searchForCustomerMethod(customerCode);
		customersTests.goToSecurityDepositTab();
		waitPageLoaded();
		securityDepositTests = new SecurityDepositTests();
		securityDepositTests.createSecurityDeposit(securityDepositTemplateName, securityDepositName, securityDepositDescription, securityDepositExpectedBalance);
	}

	/***  Instantiate Security Deposit   ***/
	@Test(priority = 3)
	public void instantiateSecurityDeposit() {
		//securityDepositTests = new SecurityDepositTests();
		securityDepositTests.InstantiateSecurityDeposit();
	}

	/***  check Generated Invoice Type : Security Deposit  ***/
	@Test(priority = 4)
	public void checkGeneratedInvoiceTypeSD() {
		//securityDepositTests = new SecurityDepositTests();
		securityDepositTests.checkGeneratedInvoiceTypeSD();
	}

	/***  credit Security Deposit   ***/
	@Test(priority = 5)
	public void creditSecurityDeposit() {
		//securityDepositTests = new SecurityDepositTests();
		securityDepositTests.creditSecurityDeposit( amount, reference);
	}

	/***  check Security Deposit Status : Locked  ***/
	@Test(priority = 6)
	public void checkSecurityDepositStatusIsLocked() {
		//securityDepositTests = new SecurityDepositTests();
		securityDepositTests.checkSDStatusLocked();
	}
	
	/***  check Invoice Status : Paid  ***/
	@Test(priority = 7)
	public void checkInvoiceStatusIsPaid() {
		//securityDepositTests = new SecurityDepositTests();
		securityDepositTests.checkInvoiceStatusPaid();
	}
	
	/***  refund Security Deposit   ***/
	@Parameters({ "refundSecurityDepositReason"})
	@Test(priority = 8, enabled = false)
	public void refundSecurityDeposit(String reason) {
		//securityDepositTests = new SecurityDepositTests();
		securityDepositTests.refundSecurityDeposit( reason);
	}

	/***  check Security Deposit Status : Refunded   ***/
	@Test(priority = 9, enabled = false)
	public void checkSecurityDepositStatusIsRefunded() {
		//securityDepositTests = new SecurityDepositTests();
		securityDepositTests.checkSDStatusRefunded();
	}
	
	/***  check Generated Invoice Type : Adjustement   ***/
	@Test(priority = 10, enabled = false)
	public void checkGeneratedInvoiceTypeIsAdjustement() {
		//securityDepositTests = new SecurityDepositTests();
		securityDepositTests.checkGeneratedInvoiceTypeAdjustement();
	}
}
