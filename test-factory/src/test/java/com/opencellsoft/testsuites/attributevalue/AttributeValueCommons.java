package com.opencellsoft.testsuites.attributevalue;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.util.Random;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.pages.customercare.rateditems.RatedItemsPage;
import com.opencellsoft.tests.ChargesTests;
import com.opencellsoft.tests.CustomersTests;
import com.opencellsoft.tests.HomeTests;
import com.opencellsoft.tests.LoginTest;
import com.opencellsoft.tests.OffersTests;
import com.opencellsoft.tests.OrdersTests;
import com.opencellsoft.tests.ProductsTests;
import com.opencellsoft.tests.customercare.SubscriptionsTests;
import com.opencellsoft.tests.jobs.JobsTests;
import com.opencellsoft.tests.jobs.PayloadTests;

import io.restassured.RestAssured;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class AttributeValueCommons extends TestBase {

	LoginTest loginTest;
	HomeTests homeTests;
	OffersTests offersTests;
	ChargesTests chargesTests;
	CustomersTests customersTests;
	ProductsTests productsTests;
	SubscriptionsTests subscriptionsTests;
	JobsTests jobsTests;
	RatedItemsPage ratedItemsPage;
	OrdersTests ordersTests;
	PayloadTests payloadTests;
	String version;
	String baseURI; 
	String login;
	String password;
	
	public AttributeValueCommons() {
		loginTest = new LoginTest();
		homeTests = new HomeTests();
		offersTests = new OffersTests();
		productsTests = new ProductsTests();
		chargesTests = new ChargesTests();
		customersTests = new CustomersTests();
		subscriptionsTests = new SubscriptionsTests();
		jobsTests =  new JobsTests();
		ratedItemsPage = new RatedItemsPage();
		payloadTests = new PayloadTests();
		ordersTests = new OrdersTests();
	}
	
	/***  setup  ***/
	@BeforeClass
	@Parameters(value = {"baseURI", "browser", "nodeURL", "login", "password", "version" })
	public void setup(String baseURI, String browser, String nodeURL, String login, String password, String version) throws Exception{
		initialize( browser, nodeURL, baseURI); 
		this.version = version;
		this.baseURI = baseURI; 
		this.login = login;
		this.password = password;
		
		loginTest.loginTest(login, password);
		homeTests.goToPortal();
	}
	
	/***  Teardown  ***/
	@AfterClass
	public void Teardown() {
		TeardownTest();
	}
	
	/***  insert Charge Cdr  ***/
	public int insertChargeCdr(String baseURI, String login, String password, String body) throws InterruptedException {
		// Setting BaseURI once
		RestAssured.baseURI = baseURI;
		
		// Setting BasePath once
		RestAssured.basePath = "api/rest/v2/mediation/cdrs/chargeCdrList";
		PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
		authScheme.setUserName(login);
		authScheme.setPassword(password);
		RestAssured.authentication = authScheme;
		
		// Send request
		Response resp;
		JsonPath j;
		int l=0;
		
		for (int i=0; i<5; i++){
			if(l != 5) {
				Thread.sleep(1000);
				// get date format "yyyy-MM-dd'T'HH:mm:ss.'00Z'"

				System.out.println("Request body : " + body);
				resp = given().header("Content-Type", "application/json").body(body).when()
						.post();

				j = resp.jsonPath();
				l = (Integer)j.get("statistics.success");
				System.out.println("statistics.success : " + l);
			}
			else {
				break;
			}
		}
		
		return l;
	}
	
	public String getRandomNumberbetween1And200() {
		int min = 2;
        int max = 200;

        Random rand = new Random();
        int randomNumber = rand.nextInt(max - min + 2) + min;
        return  String.format("%04d", randomNumber);
	}
}
