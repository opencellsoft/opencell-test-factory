package com.opencellsoft.testsuites.attributevalue;

import static org.testng.Assert.assertEquals;
import java.awt.AWTException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class AttributeValueScenario2 extends AttributeValueCommons {
	AttributeValueDataSet2 dataSet = new AttributeValueDataSet2();
	String offerCode = dataSet.offerCode2;
	String productCode = dataSet.productCode2;
	String oneshotChargeCode = dataSet.oneshotChargeCode2;
	
	String bcCode = "Attrbc-" + Constant.customerCode;
	String customerCode = "AttrCUST2-" + Constant.offerCode;
	String customerName = "Vons and Sons";
	String subCode1 = "Attr2SUB1-" + Constant.offerCode;
	
	String customerEmail = Constant.customerEmail;
	String paypalUserId = Constant.paypalUserId;
	String consumerCode = "AttrCONS2-" + Constant.customerCode;
	String consumerName = Constant.customerCode;
	String companyRegistrationNumber = Constant.companyRegistrationNumber;
	String vatNumber = Constant.vatNumber;
	String AP1 = "Attr2AP1_"+Constant.customerCode;
	String AP2 = "Attr2AP2_"+Constant.customerCode;
	String orderCode1 = "Attr2Order1_"+Constant.customerCode;
	String companyRegistration = getRandomNumberbetween1And200();
	String order1Number = null;
	
	
	String dateValue = "10";
	String emailValue =  "ABcd10@gmail.com";
	String infoValue = "First Price";
	String integerValue = "1";
	String listOfNumericValueDot = "11.1";
	String listOfNumericValueComma = "11,1";
	String listOfTextValue = "AAA";
	String multipleNumericValueDot = "4.04";
	String multipleNumericValueComma = "4,04";
	String multipleTextValue = "JAUNE";
	String numericValueDot = "1.11";
	String numericValueComma = "1,11";
	String phoneValue = "+33221100";
	String textValue = "A";
	

	@Test(priority = 0, enabled = true)
	public void _ATTRV_T02(){
		System.out.println("**************************      attribute value : scenario 2     ******************************");
	}

	/***   Create customer   ***/
	@Parameters({ "billingCountry", "billingCycle", "acrs" })
	@Test(priority = 4, enabled = true)
	public void _ATTRV_T02_createCustomer(String billingCountry, String billingCycle, String acrs) throws InterruptedException {
		// Create customer
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bcCode,bcCode,"BILLINGACCOUNT", "MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bcCode,bcCode,"BILLINGACCOUNT", "CAL_PERIOD_MONTHLY");
		payloadTests.createCustomer(baseURI, login, password, customerCode,customerName , bcCode);
	}

	/***   Create and Activate a new Subscription   ***/
	@Parameters({ "seller", "version" })
	@Test(priority = 6, enabled = true)
	public void _ATTRV_T02_createAndActivateSubscription1(String seller, String version) throws AWTException, InterruptedException{
		
		// Go to customers list page
		homeTests.goToSubscriptionsListPage();

		// Create a new subscription from subscription tab in customer details page
		subscriptionsTests.create_subscription_FromSubscriptionsListPage(seller, customerCode, subCode1, offerCode);

		// Create Access point
		subscriptionsTests.createAccessPointMethod(AP1);

		// Activate the product instance for the subscription
		subscriptionsTests.activateProductInstanceWithAttribute2("true",dateValue,productCode,emailValue,infoValue,integerValue,listOfNumericValueDot, listOfTextValue,multipleNumericValueDot,multipleTextValue, numericValueDot,phoneValue, textValue, version);

	}

	/***   Create and Activate a new Subscription   ***/
	@Test(priority = 10, enabled = true)
	public void _ATTRV_T02_createOrder() {
		// Go to customers list page
		homeTests.goToOrdersListPage();

		// Create a new order from order tab in customer details page
		ordersTests.createOrderFromOrdersListPage(customerName, version);

		// Create order line
		ordersTests.createOrderLineOneShot(subCode1, productCode, "1", "1");

		ordersTests.save();
	}

	/***   Check order attribute   ***/
	@Test(priority = 14, enabled = true)
	public void _ATTRV_T02_CheckAttributeValues() throws InterruptedException {
		// open product details
		ordersTests.clickOnProductTab(productCode);

		// Check that 7 attributes are displayed
		int size = driver.findElements(By.xpath("(//table)[1]//thead//th")).size() - 4;
		assertEquals(size, 7);

		// Check that the attribute values are the same as the subscription
		String tenthOfTheMonthDate = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), 10).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		for(int i = 2; i<9; i++) {
			String attributeName  = driver.findElements(By.xpath("(//table)[1]//thead//th")).get(i).getText();
			String attributeValue = null;
			try {
				attributeValue = driver.findElements(By.xpath("(//table)[1]//tbody//td")).get(i).getText();
			}catch (Exception e) {
				System.out.println("maybe the attribut is a boolean or expression ! ");
			}
			//			System.out.println("<<<<<< table => Attribute name : "+ attributeName + " , Attribute value : " + attributeValue );
			switch(attributeName) {
			case "dateattribut": 						Assert.assertEquals(attributeValue, tenthOfTheMonthDate); 							break;
			case "emailattribut":        				Assert.assertEquals(attributeValue, emailValue); 									break;
			case "informationattribut":     			Assert.assertEquals(attributeValue, infoValue);									break;
			case "integerattribut":         			Assert.assertEquals(attributeValue, integerValue); 											break;
			case "listofnumericvaluesattribut": 		Assert.assertTrue(attributeValue.equals(listOfNumericValueDot) || attributeValue.equals(listOfNumericValueComma));	break;
			case "listoftextvaluesattribut":    		Assert.assertEquals(attributeValue, listOfTextValue); 		 								break;
			case "multiplelistnumericvaluesattribut":	Assert.assertTrue(attributeValue.equals(multipleNumericValueDot) || attributeValue.equals(multipleNumericValueComma));	break;
			case "multiplelisttextvaluesattribut":   	Assert.assertEquals(attributeValue, multipleTextValue); 		 								break;
			case "numericattribut":                   	Assert.assertTrue(attributeValue.equals(numericValueDot) || attributeValue.equals(numericValueComma));	break;
			case "phoneattribut":                    	Assert.assertEquals(attributeValue, phoneValue); 									break;
			case "textattribut":                      	Assert.assertEquals(attributeValue, textValue); 			 								break;
			}
		}

		//Check that the other attributes are displayed in the pop-up window when we click the attributes button
		ordersTests.displayOtherAttributesPopup(0);
		// Check that 6 other attributes are displayed in the popup
		size = driver.findElements(By.xpath("//div[@role = 'dialog']//table//tbody//tr")).size();
		assertEquals(size, 6);
		// check the attributes values

		for(int i = 1 ; i<6 ; i++) {
			String xpath1 = "//div[@role = 'dialog']//table//tbody//tr[" + i + "]//td[1]//div";
			String xpath2 = "//div[@role = 'dialog']//table//tbody//tr[" + i + "]//td[2]//div";
			String attributeName = driver.findElement(By.xpath(xpath1)).getText();
			String attributeValue = null;
			try {
				attributeValue = driver.findElement(By.xpath(xpath2)).getText();
			}catch (Exception e) {
				System.out.println("maybe the attribut is a boolean or an expression ! ");
			}
			//			System.out.println("<<<<<< popup => Attribute name : "+ attributeName + " , Attribute value : " + attributeValue );

			switch(attributeName) {
			case "dateattribut": 						Assert.assertEquals(attributeValue, tenthOfTheMonthDate); 							break;
			case "emailattribut":        				Assert.assertEquals(attributeValue, emailValue); 									break;
			case "informationattribut":     			Assert.assertEquals(attributeValue, infoValue);									break;
			case "integerattribut":         			Assert.assertEquals(attributeValue, integerValue); 											break;
			case "listofnumericvaluesattribut": 		Assert.assertTrue(attributeValue.equals(listOfNumericValueDot) || attributeValue.equals(listOfNumericValueComma));	break;
			case "listoftextvaluesattribut":    		Assert.assertEquals(attributeValue, listOfTextValue); 		 								break;
			case "multiplelistnumericvaluesattribut":	Assert.assertTrue(attributeValue.equals(multipleNumericValueDot) || attributeValue.equals(multipleNumericValueComma)); 	break;
			case "multiplelisttextvaluesattribut":   	Assert.assertEquals(attributeValue, multipleTextValue); 		 								break;
			case "numericattribut":                   	Assert.assertTrue(attributeValue.equals(numericValueDot) || attributeValue.equals(numericValueComma)); 	break;
			case "phoneattribut":                    	Assert.assertEquals(attributeValue, phoneValue); 									break;
			case "textattribut":                      	Assert.assertEquals(attributeValue, textValue); 			 								break;
			}
		}

		ordersTests.closeOtherAttributesPopup();

	}

	/***   Verify Rated items on costumer page (2 items open)   ***/
	@Test(priority = 16, enabled = true)
	public void _ATTRV_T02_updateAttibutesValueInTheTable() {
		ordersTests.updateAttributValueInTheTable("dateattribut", "5");
		ordersTests.updateAttributValueInTheTable("informationattribut", "Third Price");
		ordersTests.updateAttributValueInTheTable("listofnumericvaluesattribut", "33.3");

		ordersTests.save();

	}

	@Test(priority = 18, enabled = true)
	public void _ATTRV_T02_CheckThatAttributeValuesAreUpdated() throws InterruptedException {
		// open product details
		ordersTests.clickOnProductTab(productCode);

		// Check that 7 attributes are displayed
		int size = driver.findElements(By.xpath("//table//thead//th")).size() - 4;
		assertEquals(size, 7);

		// Check that the attribute values are the same as the subscription
		String fifthOfTheMonthDate = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), 5).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		for(int i = 2; i<9; i++) {
			String attributeName  = driver.findElements(By.xpath("//table//thead//th")).get(i).getText();
			String attributeValue = null;
			try {
				attributeValue = driver.findElements(By.xpath("//table//tbody//td")).get(i).getText();
			}catch (Exception e) {
				System.out.println("maybe the attribut is a boolean or expression ! ");
			}
			//			System.out.println("<<<<<< table => Attribute name : "+ attributeName + " , Attribute value : " + attributeValue );
			switch(attributeName) {
			case "dateattribut": 						Assert.assertEquals(attributeValue, fifthOfTheMonthDate); 	break;
			case "emailattribut":        				Assert.assertEquals(attributeValue, emailValue); 			break;
			case "informationattribut":     			Assert.assertEquals(attributeValue, "Third Price");			break;
			case "integerattribut":         			Assert.assertEquals(attributeValue, integerValue); 			break;
			case "listofnumericvaluesattribut": 		Assert.assertEquals(attributeValue, "33.3"); 		 		break;
			case "listoftextvaluesattribut":    		Assert.assertEquals(attributeValue, listOfTextValue); 		break;
			case "multiplelistnumericvaluesattribut":	Assert.assertTrue(attributeValue.equals(multipleNumericValueDot) || attributeValue.equals(multipleNumericValueComma)); 	break;
			case "multiplelisttextvaluesattribut":   	Assert.assertEquals(attributeValue, multipleTextValue); 	break;
			case "numericattribut":                   	Assert.assertTrue(attributeValue.equals(numericValueDot) || attributeValue.equals(numericValueComma)); 	break;
			case "phoneattribut":                    	Assert.assertEquals(attributeValue, phoneValue); 			break;
			case "textattribut":                      	Assert.assertEquals(attributeValue, textValue); 			break;
			}
		}

	}

	/***   Verify Rated items on costumer page (2 items open)   ***/
	@Test(priority = 20, enabled = true)
	public void _ATTRV_T02_updateAttibutesValueInThePopup() {
		ordersTests.displayOtherAttributesPopup(0);

		ordersTests.updateAttributValueInThePopup("listoftextvaluesattribut", "CCC");
		ordersTests.updateAttributValueInThePopup("multiplelistnumericvaluesattribut", "6.06");
		ordersTests.updateAttributValueInThePopup("textattribut", "");

		ordersTests.applyChanges();

		ordersTests.save();

	}

	/***   Check order attribute   ***/

	@Test(priority = 22, enabled = true)
	public void _ATTRV_T02_CheckThatTheAttiributeValuesAreUpdated() throws InterruptedException {
		//Check that the other attributes are displayed in the pop-up window when we click the attributes button
		ordersTests.displayOtherAttributesPopup(0);
		// Check that 6 other attributes are displayed in the popup
		int size = driver.findElements(By.xpath("//div[@role = 'dialog']//table//tbody//tr")).size();
		assertEquals(size, 6);
		// check the attributes values
		String fifthOfTheMonthDate = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), 5).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		for(int i = 1 ; i<6 ; i++) {
			String xpath1 = "//div[@role = 'dialog']//table//tbody//tr[" + i + "]//td[1]//div";
			String xpath2 = "//div[@role = 'dialog']//table//tbody//tr[" + i + "]//td[2]//div";
			String attributeName = driver.findElement(By.xpath(xpath1)).getText();
			String attributeValue = null;
			try {
				attributeValue = driver.findElement(By.xpath(xpath2)).getText();
			}catch (Exception e) {
				System.out.println("maybe the attribut is a boolean or an expression ! ");
			}
			//			System.out.println("<<<<<< popup => Attribute name : "+ attributeName + " , Attribute value : " + attributeValue );

			switch(attributeName) {
			case "dateattribut": 						Assert.assertEquals(attributeValue, fifthOfTheMonthDate); 							break;
			case "emailattribut":        				Assert.assertEquals(attributeValue, emailValue); 									break;
			case "informationattribut":     			Assert.assertEquals(attributeValue, "Third Price");									break;
			case "integerattribut":         			Assert.assertEquals(attributeValue, integerValue); 									break;
			case "listofnumericvaluesattribut": 		Assert.assertTrue(attributeValue.equals("33.3") || attributeValue.equals("33.3"));	break;
			case "listoftextvaluesattribut":    		Assert.assertEquals(attributeValue, "CCC"); 		 								break;
			case "multiplelistnumericvaluesattribut":	Assert.assertTrue(attributeValue.equals("6,06") || attributeValue.equals("6.06")); 	break;
			case "multiplelisttextvaluesattribut":   	Assert.assertEquals(attributeValue, multipleTextValue); 		 					break;
			case "numericattribut":                   	Assert.assertTrue(attributeValue.equals(numericValueDot) || attributeValue.equals(numericValueComma)); 	break;
			case "phoneattribut":                    	Assert.assertEquals(attributeValue, phoneValue); 									break;
			case "textattribut":                      	Assert.assertEquals(attributeValue, ""); 			 								break;
			}
		}
		ordersTests.closeOtherAttributesPopup();
	}

}
