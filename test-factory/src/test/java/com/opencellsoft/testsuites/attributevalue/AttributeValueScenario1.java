package com.opencellsoft.testsuites.attributevalue;

import java.awt.AWTException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.opencellsoft.utility.Constant;

public class AttributeValueScenario1 extends AttributeValueCommons {
	AttributeValueDataSet dataSet = new AttributeValueDataSet();
	String offerCode = dataSet.offerCode1;
	String productCode = dataSet.productCode1;
	String usageChargeCode = dataSet.usageChargeCode1;

	String customerCode = "AttrCUST-" + Constant.customerCode;
	String subCode1 = "AttrSUB0_"+Constant.subscriptionCode;
	String AP1 = "AttrAP1_"+Constant.accessPointCode;
	String subCode2 = "AttrSUB1_"+Constant.subscriptionCode;
	String AP2 = "AttrAP2_"+Constant.accessPointCode;
	String subCode3 = "AttrSUB2_"+Constant.subscriptionCode;
	String AP3 = "AttrAP3_"+Constant.accessPointCode;
	String subCode4 = "AttrSUB3_"+Constant.subscriptionCode;
	String AP4 = "AttrAP4_"+Constant.accessPointCode;
	String subCode5 = "AttrSUB4_"+Constant.subscriptionCode;
	String AP5 = "AttrAP5_"+Constant.accessPointCode;
	String companyRegistration = getRandomNumberbetween1And200();
	String bcCode = "AttrBC-" + Constant.customerCode;

	@Test(priority = 0, enabled = true)
	public void _ATTRV_T01() {
		System.out.println("**************************      attribute value : scenario 1     ******************************");
	}

	/***   Create customer   ***/
	@Parameters({ "billingCountry", "billingCycle", "acrs" })
	@Test(priority = 3, enabled = true)
	public void _ATTRV_T01_createCustomer(String billingCountry, String billingCycle, String acrs) throws InterruptedException {
		// Create customer
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bcCode,bcCode,"BILLINGACCOUNT", "MONTHLY");
		payloadTests.createBillingCycleByAPI(baseURI, login, password,bcCode,bcCode,"BILLINGACCOUNT", "CAL_PERIOD_MONTHLY");
		payloadTests.createCustomer(baseURI, login, password, customerCode,customerCode , bcCode);
	}

	/***   Create and Activate a new Subscription   ***/
	@Parameters({ "seller", "version" })
	@Test(priority = 4, enabled = true)
	public void _ATTRV_T01_createAndActivateSubscription1(String seller, String version) throws AWTException, InterruptedException{
		// Go to customer details page
		homeTests.goToCustomTablesListPage();
		customersTests.searchForCustomerMethod(customerCode);

		// Create a new subscription from subscription tab in customer details page
		subscriptionsTests.create_subscription_FromCustomerPage(seller, subCode1, offerCode);

		// Create Access point
		subscriptionsTests.createAccessPointMethod(AP1);

		// Activate the product instance for the subscription
		subscriptionsTests.activateProductInstanceWithAttribute1("1", "1,11", "true", "A", version);

	}

	/***   Create and Activate a new Subscription   ***/
	@Parameters({ "seller", "version" })
	@Test(priority = 6, enabled = true)
	public void _ATTRV_T01_createAndActivateSubscription2(String seller, String version) throws AWTException, InterruptedException{
		// Go to customer details page
		//customersTests.searchForCustomerMethod(customerCode);

		// Create a new subscription from subscription tab in customer details page
		subscriptionsTests.create_subscription_FromCustomerPage(seller, subCode2, offerCode);

		// Create Access point
		subscriptionsTests.createAccessPointMethod(AP2);

		// Activate the product instance for the subscription
		subscriptionsTests.activateProductInstanceWithAttribute1("2", "2,22", "false", "", version);
	}

	/***   Create and Activate a new Subscription   ***/
	@Parameters({ "seller", "version" })
	@Test(priority = 8, enabled = true)
	public void _ATTRV_T01_createAndActivateSubscription3(String seller, String version) throws AWTException, InterruptedException{
		// Go to customer details page
		//		customersTests.searchForCustomerMethod(customerCode);

		// Create a new subscription from subscription tab in customer details page
		subscriptionsTests.create_subscription_FromCustomerPage(seller, subCode3, offerCode);

		// Create Access point
		subscriptionsTests.createAccessPointMethod(AP3);

		// Activate the product instance for the subscription
		subscriptionsTests.activateProductInstanceWithAttribute1("3", "", "true", "", version);
	}

	/***   Create and Activate a new Subscription   ***/
	@Parameters({ "seller", "version" })
	@Test(priority = 10, enabled = true)
	public void _ATTRV_T01_createAndActivateSubscription4(String seller, String version) throws AWTException, InterruptedException{
		// Go to customer details page
		//		customersTests.searchForCustomerMethod(customerCode);

		// Create a new subscription from subscription tab in customer details page
		subscriptionsTests.create_subscription_FromCustomerPage(seller, subCode4, offerCode);

		// Create Access point
		subscriptionsTests.createAccessPointMethod(AP4);

		// Activate the product instance for the subscription
		subscriptionsTests.activateProductInstanceWithAttribute1("", "", "false", "", version);
	}

	/***   Create and Activate a new Subscription   ***/
	@Parameters({ "seller", "version" })
	@Test(priority = 10, enabled = true)
	public void _ATTRV_T01_createAndActivateSubscription5(String seller, String version) throws AWTException, InterruptedException{
		// Go to customer details page
		//		customersTests.searchForCustomerMethod(customerCode);

		// Create a new subscription from subscription tab in customer details page
		subscriptionsTests.create_subscription_FromCustomerPage(seller, subCode5, offerCode);

		// Create Access point
		subscriptionsTests.createAccessPointMethod(AP5);

		// Activate the product instance for the subscription
		subscriptionsTests.activateProductInstanceWithAttribute1("", "", "true", "", version);
	}

	/***   Insert EDRs, first version   ***/ 
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 12)
	public void _ATTRV_T01_insertChargeCdr(String baseURI, String login, String password) throws InterruptedException {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.'00Z'", Locale.FRANCE);
		String date = format.format(calendar.getTime());

		// create API request body
		String body = "{\r\n"
				+ "    \"cdrs\": [\r\n"
				+ "    \"" + date + ";1;" + AP1 + ";param1;param2;param3;param4;param5;param6;param7;param8;param9;" + date + ";" + date + ";" + date + ";" + date + ";" + date + ";0.1;0.2;0.3;0.4;0.5;extraParam\",\r\n"
				+ "    \"" + date + ";1;" + AP2 + ";param1;param2;param3;param4;param5;param6;param7;param8;param9;" + date + ";" + date + ";" + date + ";" + date + ";" + date + ";0.1;0.2;0.3;0.4;0.5;extraParam\",\r\n"
				+ "    \"" + date + ";1;" + AP3 + ";param1;param2;param3;param4;param5;param6;param7;param8;param9;" + date + ";" + date + ";" + date + ";" + date + ";" + date + ";0.1;0.2;0.3;0.4;0.5;extraParam\",\r\n"
				+ "    \"" + date + ";1;" + AP4 + ";param1;param2;param3;param4;param5;param6;param7;param8;param9;" + date + ";" + date + ";" + date + ";" + date + ";" + date + ";0.1;0.2;0.3;0.4;0.5;extraParam\",\r\n"
				+ "    \"" + date + ";1;" + AP5 + ";param1;param2;param3;param4;param5;param6;param7;param8;param9;" + date + ";" + date + ";" + date + ";" + date + ";" + date + ";0.1;0.2;0.3;0.4;0.5;extraParam\"\r\n"
				+ "    ]\r\n"
				+ "}";
		System.out.println("Request body : " + body);
		int result = insertChargeCdr(baseURI, login, password,body);
		Assert.assertEquals(result, 5);
	}

	/***   Run Rated Transaction Job (RT_Job)   ***/
	@Parameters({ "baseURI", "login", "password" })
	@Test(priority = 14)
	public void _ATTRV_T01_runRTJob(String baseURI, String login, String password) throws InterruptedException {
		jobsTests.runRTJob(baseURI, login, password);
	}

	/***   Verify Rated items on costumer page (2 items open)   ***/
	@Test(priority = 16, enabled = true)
	public void _ATTRV_T01_checkRatedItemsOnCustomerPage() {
		//Search a customer
		//		customersTests.searchForCustomerMethod(customerCode);

		// Go to rated items tab
		customersTests.GoToRatedItemsTab("");
		waitPageLoaded();

		Assert.assertEquals(ratedItemsPage.elementsFoundSize("€0.99"), 2);
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("€1.99"), 2);
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("€2.99"), 2);
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("€3.99"), 2);
		Assert.assertEquals(ratedItemsPage.elementsFoundSize("€4.99"), 2);
	}
}
