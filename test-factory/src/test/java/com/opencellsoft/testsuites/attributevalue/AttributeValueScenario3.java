package com.opencellsoft.testsuites.attributevalue;

import static org.testng.Assert.assertEquals;

import java.awt.AWTException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.utility.Constant;

public class AttributeValueScenario3 extends AttributeValueCommons {
	AttributeValueDataSet2 dataSet = new AttributeValueDataSet2();
	String offerCode = dataSet.offerCode2;
	String productCode = dataSet.productCode2;
	String oneshotChargeCode = dataSet.oneshotChargeCode2;

	AttributeValueScenario2 AttributeValueScenario2 = new AttributeValueScenario2();
	String customerCode = AttributeValueScenario2.customerCode;
	String customerName = AttributeValueScenario2.consumerName;
	String subCode2 = "Attr2SUB2-001";

	//	String customerCode = "AttrCUST2-" + Constant.customerCode;
	//	String customerName = Constant.customerName;
	//	String subCode1 = "Attr2SUB1-"+Constant.customerCode;
	//	String subCode2 = "Attr2SUB2-"+Constant.customerCode;
	String dateValue = "10";
	String emailValue =  "azerty.1234@gmail.com";
	String infoValue = "Second Price";
	String integerValue = "2";
	String listOfNumericValueDot = "22.2";
	String listOfNumericValueComma = "22,2";
	String listOfTextValue = "BBB";
	String multipleNumericValueDot = "5.05";
	String multipleNumericValueComma = "5,05";
	String multipleTextValue = "NOIR";
	String numericValueDot = "2.22";
	String numericValueComma = "2,22";
	String phoneValue = "+3344556677";
	String textValue = "B";
	
	String customerEmail = Constant.customerEmail;
	String paypalUserId = Constant.paypalUserId;
	String consumerCode = "AttrCONS2-" + Constant.customerCode;
	String consumerName = Constant.customerCode;
	String companyRegistrationNumber = Constant.companyRegistrationNumber;
	String vatNumber = Constant.vatNumber;
	String AP2 = "Attr2AP2_"+Constant.customerCode;
	String orderCode1 = "Attr2Order1_"+Constant.customerCode;
	String companyRegistration = getRandomNumberbetween1And200();
	String order1Number = null;


	@Test(priority = 2, enabled = true)
	public void _ATTRV_T03()  {
		System.out.println("**************************      attribute value : scenario 3     ******************************");
	}

	/***   Create and Activate a new Subscription   ***/
	@Parameters({ "seller", "version" })
	@Test(priority = 8, enabled = true)
	public void _ATTRV_T03_createAndActivateSubscription(String seller, String version) throws AWTException, InterruptedException{
		// Go to customers list page
		homeTests.goToSubscriptionsListPage();

		// Create a new subscription from subscription tab in customer details page
		subscriptionsTests.create_subscription_FromSubscriptionsListPage(seller, customerCode, subCode2, offerCode);

		// Create Access point
		subscriptionsTests.createAccessPointMethod(AP2);

		// Activate the product instance for the subscription
		subscriptionsTests.activateProductInstanceWithAttribute2("false",dateValue,productCode,emailValue,infoValue,integerValue,listOfNumericValueDot, listOfTextValue,multipleNumericValueDot,multipleTextValue, numericValueDot,phoneValue, textValue, version);

	}

	/***   Add new oneShot line with two product lines   ***/
	@Test(priority = 20, enabled = true)
	public void _ATTRV_T02_AddNewOneShotLineWithTwoProductLines() {
		// Go to customers list page
		homeTests.goToOrdersListPage();

		// Create a new order from order tab in customer details page
		ordersTests.createOrderFromOrdersListPage(customerName, dataSet.version);

		// Create order line
		ordersTests.createOrderLineOneShot(subCode2, productCode,"1", "1");

		ordersTests.addProductToOrderLine("2", "2");
	}

	/***   Check order attribute   ***/

	@Test(priority = 22, enabled = true)
	public void _ATTRV_T02_CheckAttributeValuesOrder2() throws InterruptedException {

		// Check that 7 attributes are displayed
		int size = driver.findElements(By.xpath("//table//thead//th")).size() - 4;
		assertEquals(size, 7);

		// Check that the attribute values are the same as the subscription
		String tenthOfTheMonthDate = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), 10).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		for(int i = 3; i<10; i++) {
			String attributeName  = driver.findElement(By.xpath("//table//thead//th[" + i + "]")).getText();
			String attributeValue = null;
			try {
				attributeValue = driver.findElement(By.xpath("//table//tbody//tr[2]//td[" + i + "]")).getText();
			}catch (Exception e) {
				System.out.println("maybe the attribut is a boolean or expression ! ");
			}
			System.out.println("<<<<<< table => Attribute name : "+ attributeName + " , Attribute value : " + attributeValue );
			switch(attributeName) {
			//			case "dateattribut": 						Assert.assertEquals(attributeValue, tenthOfTheMonthDate); 							break;
				case "emailattribut":        				Assert.assertEquals(attributeValue, emailValue); 														break;
				case "informationattribut":     			Assert.assertEquals(attributeValue, infoValue);															break;
				case "integerattribut":         			Assert.assertEquals(attributeValue, integerValue); 														break;
				case "listofnumericvaluesattribut": 		Assert.assertTrue(attributeValue.equals(listOfNumericValueDot) || attributeValue.equals(listOfNumericValueComma));	break;
				case "listoftextvaluesattribut":    		Assert.assertEquals(attributeValue, listOfTextValue); 		 											break;
				case "multiplelistnumericvaluesattribut":	Assert.assertTrue(attributeValue.equals(multipleNumericValueDot) || attributeValue.equals(multipleNumericValueComma));	break;
				case "multiplelisttextvaluesattribut":   	Assert.assertEquals(attributeValue, multipleTextValue); 		 										break;
				case "numericattribut":                   	Assert.assertTrue(attributeValue.equals(numericValueDot) || attributeValue.equals(numericValueComma));	break;
				case "phoneattribut":                    	Assert.assertEquals(attributeValue, phoneValue); 														break;
				case "textattribut":                      	Assert.assertEquals(attributeValue, textValue); 			 											break;
			}
		}

		//Check that the other attributes are displayed in the pop-up window when we click the attributes button
		ordersTests.displayOtherAttributesPopup(1);
		// Check that 6 other attributes are displayed in the popup
		size = driver.findElements(By.xpath("//div[@role = 'dialog']//table//tbody//tr")).size();
		assertEquals(size, 6);
		// check the attributes values

		for(int i = 1 ; i<6 ; i++) {
			String xpath1 = "//div[@role = 'dialog']//table//tbody//tr[" + i + "]//td[1]//div";
			String xpath2 = "//div[@role = 'dialog']//table//tbody//tr[" + i + "]//td[2]//div";
			String attributeName = driver.findElement(By.xpath(xpath1)).getText();
			String attributeValue = null;
			try {
				attributeValue = driver.findElement(By.xpath(xpath2)).getText();
			}catch (Exception e) {
				System.out.println("maybe the attribut is a boolean or an expression ! ");
			}
			//			System.out.println("<<<<<< popup => Attribute name : "+ attributeName + " , Attribute value : " + attributeValue );

			switch(attributeName) {
			//			case "dateattribut": 						Assert.assertEquals(attributeValue, tenthOfTheMonthDate); 							break;
			case "emailattribut":        				Assert.assertEquals(attributeValue, emailValue); 														break;
			case "informationattribut":     			Assert.assertEquals(attributeValue, infoValue);															break;
			case "integerattribut":         			Assert.assertEquals(attributeValue, integerValue); 														break;
			case "listofnumericvaluesattribut": 		Assert.assertTrue(attributeValue.equals(listOfNumericValueDot) || attributeValue.equals(listOfNumericValueComma));	break;
			case "listoftextvaluesattribut":    		Assert.assertEquals(attributeValue, listOfTextValue); 		 											break;
			case "multiplelistnumericvaluesattribut":	Assert.assertTrue(attributeValue.equals(multipleNumericValueDot) || attributeValue.equals(multipleNumericValueComma));	break;
			case "multiplelisttextvaluesattribut":   	Assert.assertEquals(attributeValue, multipleTextValue); 		 										break;
			case "numericattribut":                   	Assert.assertTrue(attributeValue.equals(numericValueDot) || attributeValue.equals(numericValueComma));	break;
			case "phoneattribut":                    	Assert.assertEquals(attributeValue, phoneValue); 														break;
			case "textattribut":                      	Assert.assertEquals(attributeValue, textValue); 		 								break;
			}
		}

		ordersTests.closeOtherAttributesPopup();
	}

	/***   Save and activate the order   ***/
	@Test(priority = 24, enabled = true)
	public void _ATTRV_T02_SaveAndActivateTheOrder() {

		// save Order Line
		ordersTests.save();

		// go Back to order
		ordersTests.goBack();

		ordersTests.save();

		String orderNumber = ordersTests.validateOrder1();
		System.out.println("orderNumber : " + orderNumber);
	}

}
