package com.opencellsoft.testsuites.attributevalue;

import java.awt.AWTException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.utility.Constant;

public class AttributeValueDataSet extends AttributeValueCommons {

//	String code = Constant.offerCode;
	String code = "NQQ1263";
	String offerCode1 = "AttrOFF1-" + code;
	String productCode1 = "AttrPROD1-" + code;
	String usageChargeCode1 = "AttrCHAR1-" + code;
	String pricePlanVersionCode1 = "AttrPV1-" + code;

	String offerDescription   = "AttrOffDesc1_" + code ;
	String productDescription = "AttrProdDesc1_" + code ;
	String ChargeDescription  = "AttrChgDesc1_" + code;


	String booleanAttrCode1 = "booleanAttr1-" + code;
	String eLAttrCode1      = "elAttr1-" + code;
	String integerAttrCode1 = "integerAttr1-" + code;
	String numericAttrCode1 = "numericAttr1-" + code;
	String stringAttrCode1  = "textAttr1-" + code;


	String booleanAttrDescription="booleanattribut";
	String dateAttrDescription="dateattribut";
	String elAttrDescription="elattribut";
	String emailAttrDescription="emailattribut";
	String informationAttrDescription="informationattribut";
	String integerAttrDescription="integerattribut";
	String listOfNumericValuesAttrDescription="listofnumericvaluesattribut";
	String listOfTextValuesAttrDescription="listoftextvaluesattribut";
	String multipleListNumericValuesAttrDescription="multiplelistnumericvaluesattribut";
	String multipleListTextValuesAttrDescription="multiplelisttextvaluesattribut";
	String numericAttrDescription="numericattribut";
	String phoneAttrDescription="phoneattribut";
	String stringAttrDescription="textattribut";



	String elDefaultValue = "#{serviceInstance.code}";

	@Test(priority = 0, enabled = true)
	public void _ATTRV_DS() {
		System.out.println("**************************      attribute value : data set     ******************************");
		System.out.println("String code =\"" + code + "\";");
	}

	/***  create a New Offer   
	 * @throws AWTException 
	 * @throws InterruptedException ***/
	@Parameters({ "PriceVersionDate", "defaultQuantity", "minimalQuantity", "maximalQuantity", "sequence"})
	@Test(priority = 4, enabled = true)
	public void _ATTRV_DS_createOffer1(String PriceVersionDate,String defaultQuantity, String minimalQuantity, String maximalQuantity,
			String sequence) throws InterruptedException, AWTException {
		homeTests.goToOffersPage();
		offersTests.createOfferMethod(offerCode1, offerDescription);
		productsTests.createProductMethod(PriceVersionDate,productCode1,productDescription );

		productsTests.createProductAttribut(eLAttrCode1,elAttrDescription, "EL", null,null);
		productsTests.pickAttribute(eLAttrCode1);
		productsTests.setAttributeParameters(eLAttrCode1, null,null,elDefaultValue,null,null,null,null,true,null);

		productsTests.createProductAttribut(integerAttrCode1,integerAttrDescription, "Integer value", null,null);
		productsTests.pickAttribute(integerAttrCode1);
		productsTests.setAttributeParameters(integerAttrCode1, null,null,null,null,null,null,null,true,null);

		productsTests.createProductAttribut(numericAttrCode1, numericAttrDescription, "Numeric value", "2",null);
		productsTests.pickAttribute(numericAttrCode1);
		productsTests.setAttributeParameters(numericAttrCode1, null,null,null,null,null,null,null,true,null);

		productsTests.createProductAttribut(stringAttrCode1, stringAttrDescription, "Text value", null,null);
		productsTests.pickAttribute(stringAttrCode1);
		productsTests.setAttributeParameters(stringAttrCode1, null,null,null,null,null,null,null,true,null);

		productsTests.createProductAttribut(booleanAttrCode1, booleanAttrDescription, "Boolean", null,null);
		productsTests.pickAttribute(booleanAttrCode1);
		productsTests.setAttributeParameters(booleanAttrCode1, null,null,null,null,null,null,null,true,null);

		chargesTests.createUsageCharge(usageChargeCode1,ChargeDescription,"","", version);
		chargesTests.createNewPriceVersionPriceGrid(pricePlanVersionCode1);
		addAttributesToUsageCharge1();
		addUnitPriceToPriceVersion();
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(ChargeDescription, version);
		try {
			productsTests.publishProductVersion();
		}
		catch (Exception e) {
			chargesTests.goBack();
			productsTests.publishProductVersion();
		}

		productsTests.activateProductAndGoBack();
		offersTests.pickProductToOffer( productCode1,productDescription, version);
		offersTests.fillProductParametersMethod(defaultQuantity,minimalQuantity, maximalQuantity, sequence, false );
		offersTests.ActivateOfferMethod();

	}

	public void addAttributesToUsageCharge1() {	
		if(this.version.equalsIgnoreCase("14.1.X")) {
			chargesTests.addAttribute(eLAttrCode1,"1",version);
			chargesTests.addAttribute(integerAttrCode1,"3",version);
			chargesTests.addAttribute(numericAttrCode1,"7",version);
			chargesTests.addAttribute(stringAttrCode1,"11",version);
			chargesTests.addAttribute(booleanAttrCode1,"15",version);
		}
		else {
			chargesTests.addAttribute(eLAttrCode1,"1",version);
			chargesTests.addAttribute(integerAttrCode1,"1",version);
			chargesTests.addAttribute(numericAttrCode1,"1",version);
			chargesTests.addAttribute(stringAttrCode1,"1",version);
			chargesTests.addAttribute(booleanAttrCode1,"1",version);		
		}
	}

	public void addUnitPriceToPriceVersion() throws InterruptedException{
		chargesTests.addNewValueToPriceVersion(productCode1,"elattribut", "2");
		chargesTests.addNewValueToPriceVersion("1","integerattribut", "2");
		chargesTests.addNewValueToPriceVersion("1,11","numericattribut", "2");
		chargesTests.addNewValueToPriceVersion("A","textattribut", "2");
		chargesTests.addNewValueToPriceVersion("true","booleanattribut", "2");
		chargesTests.addNewValueToPriceVersion("0,99","priceOrDiscount", "2");

		chargesTests.addNewValueToPriceVersion(productCode1,"elattribut", "3");
		chargesTests.addNewValueToPriceVersion("2","integerattribut", "3");
		chargesTests.addNewValueToPriceVersion("2,22","numericattribut", "3");
		chargesTests.addNewValueToPriceVersion("false","booleanattribut", "3");
		chargesTests.addNewValueToPriceVersion("1,99","priceOrDiscount", "3");

		chargesTests.addNewValueToPriceVersion(productCode1,"elattribut", "4");
		chargesTests.addNewValueToPriceVersion("3","integerattribut", "4");
		chargesTests.addNewValueToPriceVersion("true","booleanattribut", "4");
		chargesTests.addNewValueToPriceVersion("2,99","priceOrDiscount", "4");

		chargesTests.addNewValueToPriceVersion(productCode1,"elattribut", "5");
		chargesTests.addNewValueToPriceVersion("false","booleanattribut", "5");
		chargesTests.addNewValueToPriceVersion("3,99","priceOrDiscount", "5");

		chargesTests.addNewValueToPriceVersion(productCode1,"elattribut", "6");
		chargesTests.addNewValueToPriceVersion("true","booleanattribut", "6");
		chargesTests.addNewValueToPriceVersion("4,99","priceOrDiscount", "6");

		chargesTests.addNewValueToPriceVersion("5,99","priceOrDiscount", "7");
		chargesTests.save();
	}

}
