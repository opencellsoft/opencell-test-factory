package com.opencellsoft.testsuites.attributevalue;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencellsoft.utility.Constant;

public class AttributeValueDataSet2 extends AttributeValueCommons {
	
	String code = Constant.offerCode;
//	String code = "VUJ5828";

	
	String offerCode2 = "AttrOFF2-" + code;
	String productCode2 = "AttrPROD2-" + code;
	String oneshotChargeCode2 = "AttrCHAR2-" + code;
	String pricePlanVersionCode2 = "AttrPV2-" + code;

	String offerDescription = "AttrOffDesc2_" + code;
	String productDescription = "AttrProdDesc2_" + code ;
	
	String ChargeDescription= "AttrChgDesc2_" + code ;
	String BooleanAttrCode2 = "booleanAttr2-" + code;
	String DateAttrCode2 = "dateAttr2-" + code;
	String ELAttrCode2 = "elAttr2-" + code;
	String EmailAttrCode2 = "emailAttr2-" + code;
	String InformationAttrCode2 = "informationAttr2-" + code;
	String IntegerAttrCode2 = "integerAttr2-" + code;
	String ListOfNumericValuesAttrCode2 = "listOfNumericValuesAttr2-" + code;
	String ListOfTextValuesAttrCode2 = "listOfTextValuesAttr2-" + code;
	String MultipleListNumericValuesAttrCode2 = "multipleListNumericValuesAttr2-" + code;
	String MultipleListTextValuesAttrCode2 = "MultipleListTextValuesAttr2-" + code;
	String NumericAttrCode2 = "numericAttr2-" + code;
	String PhoneAttrCode2 = "phoneAttr2-" + code;
	String StringAttrCode2 = "textAttr2-" + code;

	String booleanAttrDescription="booleanattribut";
	String dateAttrDescription="dateattribut";
	String elAttrDescription="elattribut";
	String emailAttrDescription="emailattribut";
	String informationAttrDescription="informationattribut";
	String integerAttrDescription="integerattribut";
	String listOfNumericValuesAttrDescription="listofnumericvaluesattribut";
	String listOfTextValuesAttrDescription="listoftextvaluesattribut";
	String multipleListNumericValuesAttrDescription="multiplelistnumericvaluesattribut";
	String multipleListTextValuesAttrDescription="multiplelisttextvaluesattribut";
	String numericAttrDescription="numericattribut";
	String phoneAttrDescription="phoneattribut";
	String stringAttrDescription="textattribut";

	String[] listOfNumericValues = {"11.1","22.2","33.3"};
	String[] listOfTextValues = {"AAA","BBB","CCC"};
	String[] multipleListNumericValues = {"4.04","5.05","6.06"};
	String[] multipleListTextValues = {"JAUNE","VERT","NOIR"};

	
	String elDefaultValue = "#{serviceInstance.code}";
	
	@Test(priority = 0, enabled = true)
	public void _ATTRV_DS() {
		System.out.println("**************************      attribute value : data set2     ******************************");
		System.out.println("String code =\"" + code + "\";");
	}

	/***  create a New Offer   ***/
	@Parameters({ "PriceVersionDate", "defaultQuantity", "minimalQuantity", "maximalQuantity", "sequence"})
	@Test(priority = 4, enabled = true)
	public void _ATTRV_DS_createOffer2(String PriceVersionDate,String defaultQuantity, String minimalQuantity, String maximalQuantity,
			String sequence) throws Exception {
		homeTests.goToOffersPage();
		offersTests.createOfferMethod(offerCode2, offerDescription);
		productsTests.createProductMethod(PriceVersionDate,productCode2,productDescription );

		productsTests.createProductAttribut(ELAttrCode2,elAttrDescription, "EL", null,null);
		productsTests.pickAttribute(ELAttrCode2);
		productsTests.setAttributeParameters(ELAttrCode2,null,null,elDefaultValue,null,null,null,null,null,null);
		
		productsTests.createProductAttribut(BooleanAttrCode2, booleanAttrDescription, "Boolean", null,null);
		productsTests.pickAttribute(BooleanAttrCode2);
//		productsTests.setAttributeParameters(BooleanAttrCode2, null,null,null,null,null,null,null,true,null);
		
		productsTests.createProductAttribut(DateAttrCode2, dateAttrDescription, "Date", null,null);
		productsTests.pickAttribute(DateAttrCode2);
//		productsTests.setAttributeParameters(DateAttrCode2, null,null,null,null,null,null,null,true,null);
		
		productsTests.createProductAttribut(EmailAttrCode2,emailAttrDescription, "Email", null,null);
		productsTests.pickAttribute(EmailAttrCode2);
//		productsTests.setAttributeParameters(EmailAttrCode2, null,null,null,null,null,null,null,true,null);
		
		productsTests.createProductAttribut(InformationAttrCode2,informationAttrDescription, "Information", null,null);
		productsTests.pickAttribute(InformationAttrCode2);
//		productsTests.setAttributeParameters(InformationAttrCode2, null,null,null,null,null,null,null,true,null);
		
		productsTests.createProductAttribut(IntegerAttrCode2,integerAttrDescription, "Integer value", null,null);
		productsTests.pickAttribute(IntegerAttrCode2);
//		productsTests.setAttributeParameters(IntegerAttrCode2, null,null,null,null,null,null,null,true,null);
		
		productsTests.createProductAttribut(ListOfNumericValuesAttrCode2, listOfNumericValuesAttrDescription, "List of numeric values", null,listOfNumericValues);
		productsTests.pickAttribute(ListOfNumericValuesAttrCode2);
//		productsTests.setAttributeParameters(ListOfNumericValuesAttrCode2, null,null,null,null,null,null,null,true,null);
		
		productsTests.createProductAttribut(ListOfTextValuesAttrCode2, listOfTextValuesAttrDescription, "List of text values", null,listOfTextValues);
		productsTests.pickAttribute(ListOfTextValuesAttrCode2);
//		productsTests.setAttributeParameters(ListOfTextValuesAttrCode2, null,null,null,null,null,null,null,true,null);
		
		productsTests.createProductAttribut(MultipleListNumericValuesAttrCode2, multipleListNumericValuesAttrDescription, "Multiple list numeric values", null,multipleListNumericValues);
		productsTests.pickAttribute(MultipleListNumericValuesAttrCode2);
//		productsTests.setAttributeParameters(MultipleListNumericValuesAttrCode2, null,null,null,null,null,null,null,true,null);
		
		productsTests.createProductAttribut(MultipleListTextValuesAttrCode2, multipleListTextValuesAttrDescription, "Multiple list text values", null,multipleListTextValues);
		productsTests.pickAttribute(MultipleListTextValuesAttrCode2);
//		productsTests.setAttributeParameters(MultipleListTextValuesAttrCode2, null,null,null,null,null,null,null,true,null);
		
		productsTests.createProductAttribut(NumericAttrCode2, numericAttrDescription, "Numeric value", "2",null);
		productsTests.pickAttribute(NumericAttrCode2);
//		productsTests.setAttributeParameters(NumericAttrCode2, null,null,null,null,null,null,null,true,null);
		
		productsTests.createProductAttribut(PhoneAttrCode2, phoneAttrDescription, "Phone", null,null);
		productsTests.pickAttribute(PhoneAttrCode2);
//		productsTests.setAttributeParameters(PhoneAttrCode2, null,null,null,null,null,null,null,true,null);
		
		productsTests.createProductAttribut(StringAttrCode2, stringAttrDescription, "Text value", null,null);
		productsTests.pickAttribute(StringAttrCode2);
//		productsTests.setAttributeParameters(StringAttrCode2, null,null,null,null,null,null,null,true,null);
		
		productsTests.displayAllAttributes(13);
		
		homeTests.logout();
		loginTest.login( login, password, version);
		homeTests.goToPortal();
		productsTests.searchForProductMethod(productCode2, productDescription);
		
		chargesTests.createOneshotCharge(oneshotChargeCode2, ChargeDescription, "Other", this.version);
		//		chargesTests.createUsageCharge(usageChargeCode2,usageChargeDescription,"","", this.version);
		chargesTests.createNewPriceVersionPriceGrid(pricePlanVersionCode2);
		addAttributesToUsageCharge2();
		fillInThePriceGrid2();
		chargesTests.publishPriceVersion();
		chargesTests.activateCharge();
		chargesTests.pickChargeToProduct(ChargeDescription, version);
		try {
			productsTests.publishProductVersion();
		}
		catch (Exception e) {
			chargesTests.goBack();
			productsTests.publishProductVersion();
		}
		productsTests.activateProductAndGoBack();
		homeTests.goToOffersPage();
		offersTests.searchForOfferMethod(offerCode2, offerDescription);
		offersTests.pickProductToOffer( productCode2,productDescription, version);
		offersTests.fillProductParametersMethod(defaultQuantity,minimalQuantity, maximalQuantity, sequence, false );
		offersTests.ActivateOfferMethod();
	}

	/***  add Attributes To Usage Charge2   ***/
	@Test(priority = 40, enabled = false)
	public void addAttributesToUsageCharge2() {	
		if(version.equalsIgnoreCase("14.1.X")) {
			chargesTests.addAttribute(BooleanAttrCode2,"1",version);
			chargesTests.addAttribute(DateAttrCode2,"3",version);
			chargesTests.addAttribute(ELAttrCode2,"7",version);
			chargesTests.addAttribute(EmailAttrCode2,"11",version);
			chargesTests.addAttribute(InformationAttrCode2,"15",version);
			chargesTests.addAttribute(IntegerAttrCode2,"19",version);
			chargesTests.addAttribute(ListOfNumericValuesAttrCode2,"23",version);
			chargesTests.addAttribute(ListOfTextValuesAttrCode2,"27",version);
			chargesTests.addAttribute(MultipleListNumericValuesAttrCode2,"31",version);
			chargesTests.addAttribute(MultipleListTextValuesAttrCode2,"35",version);
			chargesTests.addAttribute(NumericAttrCode2,"39",version);
			chargesTests.addAttribute(PhoneAttrCode2,"43",version);
			chargesTests.addAttribute(StringAttrCode2,"47",version);

		}
		else {
			chargesTests.addAttribute(BooleanAttrCode2,"1",version);
			chargesTests.addAttribute(DateAttrCode2,"1",version);
			chargesTests.addAttribute(ELAttrCode2,"1",version);
			chargesTests.addAttribute(EmailAttrCode2,"1",version);
			chargesTests.addAttribute(InformationAttrCode2,"1",version);
			chargesTests.addAttribute(IntegerAttrCode2,"1",version);
			chargesTests.addAttribute(ListOfNumericValuesAttrCode2,"1",version);
			chargesTests.addAttribute(ListOfTextValuesAttrCode2,"1",version);
			chargesTests.addAttribute(MultipleListNumericValuesAttrCode2,"1",version);
			chargesTests.addAttribute(MultipleListTextValuesAttrCode2,"1",version);
			chargesTests.addAttribute(NumericAttrCode2,"1",version);
			chargesTests.addAttribute(PhoneAttrCode2,"1",version);
			chargesTests.addAttribute(StringAttrCode2,"1",version);		
		}
	}

	public void fillInThePriceGrid2() throws InterruptedException{
		// Price 1
		chargesTests.addNewValueToPriceVersion("true","booleanattribut", "2");
		chargesTests.addNewValueToPriceVersion("10","dateattribut", "2");
		chargesTests.addNewValueToPriceVersion(productCode2,"elattribut", "2");
		chargesTests.addNewValueToPriceVersion("ABcd10@gmail.com","emailattribut", "2");
		chargesTests.addNewValueToPriceVersion("First Price","informationattribut", "2");
		chargesTests.addNewValueToPriceVersion("1","integerattribut", "2");
		chargesTests.addNewValueToPriceVersion("11.1","listofnumericvaluesattribut", "2");
		chargesTests.addNewValueToPriceVersion("AAA","listoftextvaluesattribut", "2");
		chargesTests.addNewValueToPriceVersion("4.04","multiplelistnumericvaluesattribut", "2");
		chargesTests.addNewValueToPriceVersion("JAUNE","multiplelisttextvaluesattribut", "2");
		chargesTests.addNewValueToPriceVersion("1.11","numericattribut", "2");
		chargesTests.addNewValueToPriceVersion("+33221100","phoneattribut", "2");
		chargesTests.addNewValueToPriceVersion("A","textattribut", "2");
		chargesTests.addNewValueToPriceVersion("0.99","priceOrDiscount", "2");
		// Price 2
		chargesTests.addNewValueToPriceVersion("false","booleanattribut", "3");
		chargesTests.addNewValueToPriceVersion("10","dateattribut", "3");
		chargesTests.addNewValueToPriceVersion(productCode2,"elattribut", "3");
		chargesTests.addNewValueToPriceVersion("azerty.1234@gmail.com","emailattribut", "3");
		chargesTests.addNewValueToPriceVersion("Second Price","informationattribut", "3");
		chargesTests.addNewValueToPriceVersion("2","integerattribut", "3");
		chargesTests.addNewValueToPriceVersion("22.2","listofnumericvaluesattribut", "3");
		chargesTests.addNewValueToPriceVersion("BBB","listoftextvaluesattribut", "3");
		chargesTests.addNewValueToPriceVersion("5.05","multiplelistnumericvaluesattribut", "3");
		chargesTests.addNewValueToPriceVersion("NOIR","multiplelisttextvaluesattribut", "3");
		chargesTests.addNewValueToPriceVersion("2.22","numericattribut", "3");
		chargesTests.addNewValueToPriceVersion("+3344556677","phoneattribut", "3");
		chargesTests.addNewValueToPriceVersion("B","textattribut", "3");
		chargesTests.addNewValueToPriceVersion("9.99","priceOrDiscount", "3");
		// Price 3
		chargesTests.addNewValueToPriceVersion("true","booleanattribut", "4");
		chargesTests.addNewValueToPriceVersion("5","dateattribut", "4");
		chargesTests.addNewValueToPriceVersion(productCode2,"elattribut", "4");
		chargesTests.addNewValueToPriceVersion("ABcd10@gmail.com","emailattribut", "4");
		chargesTests.addNewValueToPriceVersion("Third Price","informationattribut", "4");
		chargesTests.addNewValueToPriceVersion("1","integerattribut", "4");
		chargesTests.addNewValueToPriceVersion("33.3","listofnumericvaluesattribut", "4");
		chargesTests.addNewValueToPriceVersion("CCC","listoftextvaluesattribut", "4");
		chargesTests.addNewValueToPriceVersion("6.06","multiplelistnumericvaluesattribut", "4");
		chargesTests.addNewValueToPriceVersion("JAUNE","multiplelisttextvaluesattribut", "4");
		chargesTests.addNewValueToPriceVersion("1.11","numericattribut", "4");
		chargesTests.addNewValueToPriceVersion("+33221100","phoneattribut", "4");
		chargesTests.addNewValueToPriceVersion("","textattribut", "4");
		chargesTests.addNewValueToPriceVersion("8.99","priceOrDiscount", "4");

		chargesTests.save();
	}	

}
