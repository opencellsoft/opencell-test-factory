package com.opencellsoft.base;

import java.util.HashMap;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

public class OptionsManager {
    //Get Chrome Options
    public static ChromeOptions getChromeOptions() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-search-engine-choice-screen");
		// To start Chrome in Maximized browser window
		options.addArguments("--start-maximized");
		// To remove Chrome is being controlled by automated test software
        options.addArguments("--ignore-certificate-errors");
        options.addArguments("--disable-popup-blocking");
        
		options.addArguments("disable-infobars");
		options.addArguments("--headless", "--no-sandbox");
		//options.addArguments("window-size=1920,1080");
		//options.addArguments("--headless", "--window-size=1920,1200");
		options.addArguments("force-device-scale-factor=1");
		options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--incognito");
		options.addArguments("--remote-allow-origins=*");
		//options.setCapability("se:cdpVersion", "85.0");
        return options;
    }
    //Get Firefox Options
    public static FirefoxOptions getFirefoxOptions () {
        FirefoxOptions options = new FirefoxOptions();
        FirefoxProfile profile = new FirefoxProfile();
        //Accept Untrusted Certificates
        profile.setAcceptUntrustedCertificates(true);
        profile.setAssumeUntrustedCertificateIssuer(false);
        //Use No Proxy Settings
        profile.setPreference("network.proxy.type", 0);
        //Set Firefox profile to capabilities
        options.setCapability(FirefoxDriver.SystemProperty.BROWSER_PROFILE, profile);
        return options;
    }
}