package com.opencellsoft.base;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.Optional;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.HasDevTools;
import org.openqa.selenium.devtools.v85.network.Network;
import org.openqa.selenium.devtools.v85.network.model.Request;
import org.openqa.selenium.devtools.v85.network.model.Response;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;

//import utils.GlobalData;

public class TestBase {
	public static WebDriver driver = null;
	// Set the path for chromedriver
	public CapabilityFactory capabilityFactory = new CapabilityFactory();
	String driverPath = System.getProperty("user.dir");
	String zoomLevelReduced = "75%";
	private DevTools devTools;

	@BeforeSuite
	@Parameters(value = { "browser", "nodeURL", "baseURI" })
	public void initialize(String browser, String nodeURL, String baseURI) throws MalformedURLException {
        try {          
            // Log the capabilities to debug
            driver = new RemoteWebDriver(new URL(nodeURL), capabilityFactory.getCapabilities(browser));
            System.out.println("Capabilities: " + ((RemoteWebDriver) driver).getCapabilities());
            driver = new Augmenter().augment(driver);
            if (driver instanceof HasDevTools) {
                devTools = ((HasDevTools) driver).getDevTools();
                devTools.createSession();
                System.out.println("DevTools session created successfully.");
                devTools.send(Network.enable(Optional.empty(), Optional.empty(), Optional.empty()));

                devTools.addListener(Network.requestWillBeSent(), request -> {
                    Request req = request.getRequest();
                    System.out.println("Request URL: " + req.getUrl());
                    System.out.println("Request Method: " + req.getMethod());
                    System.out.println("Request Headers: " + req.getHeaders().toString());
                    System.out.println("Request Post Data: " + req.getPostData().orElse("N/A"));
                });

                devTools.addListener(Network.responseReceived(), response -> {
                    Response res = response.getResponse();
                    System.out.println("Response URL: " + res.getUrl());
                    System.out.println("Response Status: " + res.getStatus());
                    System.out.println("Response Headers: " + res.getHeaders().toString());
                });
            } else {
            	System.err.println("Error: WebDriver does not support DevTools.");
                throw new IllegalArgumentException("Only Chrome browser is supported for DevTools");
            }
            
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
		driver.get(baseURI + "frontend/DEMO/portal");
		// Implicit wait
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	}
	
	@AfterSuite
	// Test cleanup
	public void TeardownTest() {
		if (driver != null) {
			driver.quit();
		}
	}
	
	public static void waitPageLoaded() {
		//JavaScript Executor to check ready state
		JavascriptExecutor j = (JavascriptExecutor)driver;
		//iterate 7 times after every three second to verify if in ready state
		for (int i=0; i<10; i++){
			try {
				Thread.sleep(1000);
			}catch (InterruptedException ex) {
				System.out.println("Page has not loaded yet ");
			}
			//again check page state
			try {
				if (j.executeScript("return document.readyState").toString().equals("complete")){
					break;
				}
			}catch (Exception e) {
				
			}
		}
	}
	
	public static void clickOn(WebElement element) {
		try {
			element.click();
		}
		catch (Exception e) {
			try {
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", element);
			}catch (Exception e1) {
				Actions actions = new Actions(driver);
				actions.moveToElement(element).click().perform();
			}
		}
		waitPageLoaded();
	}
	
}