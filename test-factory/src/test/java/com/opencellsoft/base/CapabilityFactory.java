package com.opencellsoft.base;

import org.openqa.selenium.Capabilities;

public class CapabilityFactory {
    public Capabilities capabilities;
    public Capabilities getCapabilities (String browser) {     
        switch (browser.toLowerCase()) {
        case "chrome":
        	capabilities = OptionsManager.getChromeOptions();
            return capabilities;
        case "firefox":
        	capabilities = OptionsManager.getFirefoxOptions();
            return capabilities;
        default:
            throw new IllegalArgumentException("Browser not supported: " + browser);
    }
    }
}
