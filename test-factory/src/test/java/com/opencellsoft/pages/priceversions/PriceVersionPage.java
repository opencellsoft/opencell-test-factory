package com.opencellsoft.pages.priceversions;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class PriceVersionPage extends TestBase {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;
	Actions action;

	public PriceVersionPage(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		this.fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(2)).ignoring(NoSuchElementException.class);
	}

	@FindBy(how = How.XPATH, using = "(//DIV[@class='MuiDataGrid-cell MuiDataGrid-cell--withRenderer MuiDataGrid-cell--textCenter MuiDataGrid-withBorder MuiDataGrid-cell--editable'])[0]")
	WebElement dimension0;
	@FindBy(how = How.XPATH, using = "(//DIV[@class='MuiDataGrid-cell MuiDataGrid-cell--withRenderer MuiDataGrid-cell--textCenter MuiDataGrid-withBorder MuiDataGrid-cell--editable'])[1]")
	WebElement dimension1;
	@FindBy(how = How.XPATH, using = "(//DIV[@class='MuiDataGrid-cell MuiDataGrid-cell--withRenderer MuiDataGrid-cell--textCenter MuiDataGrid-withBorder MuiDataGrid-cell--editable'])[1]")
	WebElement description1;
	@FindBy(how = How.XPATH, using = "(//DIV[@class='MuiDataGrid-cell MuiDataGrid-cell--withRenderer MuiDataGrid-cell--textCenter MuiDataGrid-withBorder MuiDataGrid-cell--editable'])[3]")
	WebElement unitPrice1;
	@FindBy(how = How.XPATH, using = "(//DIV[@class='MuiDataGrid-cell MuiDataGrid-cell--withRenderer MuiDataGrid-cell--textCenter MuiDataGrid-withBorder MuiDataGrid-cell--editable'])[4]")
	WebElement unitPrice4;
	@FindBy(how = How.XPATH, using = "(//DIV[@class='MuiDataGrid-cell MuiDataGrid-cell--withRenderer MuiDataGrid-cell--textCenter MuiDataGrid-withBorder MuiDataGrid-cell--editable'])[3]")
	WebElement dimension2;
	@FindBy(how = How.XPATH, using = "(//DIV[@class='MuiDataGrid-cell MuiDataGrid-cell--withRenderer MuiDataGrid-cell--textCenter MuiDataGrid-withBorder MuiDataGrid-cell--editable'])[4]")
	WebElement description2;
	@FindBy(how = How.XPATH, using = "(//DIV[@class='MuiDataGrid-cell MuiDataGrid-cell--withRenderer MuiDataGrid-cell--textCenter MuiDataGrid-withBorder MuiDataGrid-cell--editable'])[4]")
	WebElement unitPrice2;
	@FindBy(how = How.XPATH, using = "(//DIV[@class='MuiDataGrid-cell MuiDataGrid-cell--withRenderer MuiDataGrid-cell--textCenter MuiDataGrid-withBorder MuiDataGrid-cell--editable'])[8]")
	WebElement unitPrice3;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiDataGrid-cell MuiDataGrid-cell--withRenderer MuiDataGrid-cell--textCenter MuiDataGrid-withBorder MuiDataGrid-cell--editable']")
	WebElement descriptionDiv;
	@FindBy(how = How.XPATH, using = "//input[contains(@id, 'description')]")
	WebElement descriptionInput1;
	@FindBy(how = How.XPATH, using = "//input[contains(@id, 'description')]")	
	WebElement descriptionInput2;
	@FindBy(how = How.XPATH, using = "//input[contains(@id, 'priceOrDiscount')]")
	WebElement unitPriceInput1;
	@FindBy(how = How.XPATH, using = "//input[contains(@id, 'priceOrDiscount')]")
	WebElement unitPriceInput2;
	@FindBy(how = How.XPATH, using = "(//DIV[contains(@class, 'MuiDataGrid-cell--withRenderer MuiDataGrid-cell--textCenter MuiDataGrid-withBorder MuiDataGrid-cell--editable')])[3]")
	WebElement unitPriceColumn;                      
	@FindBy(how = How.XPATH, using = "(//div[@data-field = 'priceOrDiscount'])[2]")
	WebElement unitPriceInput;
	@FindBy(how = How.XPATH, using = "//*[normalize-space()='Save']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;
	@FindBy(how = How.CSS, using = ".MuiSvgIcon-fontSizeInherit")
	WebElement addAttributeButton;
	@FindBy(how = How.XPATH, using = "(//button[@type='button'])[15]")
	WebElement addBuisnessAttributeButton;
	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/main[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/fieldset[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[6]/div[1]/div[2]/div[1]/div[1]/div[1]/button[1]")
	WebElement addGridAttributeButton;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiInputBase-root MuiFilledInput-root MuiFilledInput-underline MuiInputBase-formControl MuiInputBase-adornedEnd MuiFilledInput-adornedEnd MuiInputBase-marginDense MuiFilledInput-marginDense']")
	WebElement attributeDiv;
	@FindBy(how = How.NAME, using = "attribute")
	WebElement attributeInput;
	@FindBy(how = How.XPATH, using = "//input[@id='isBusiness']")
	WebElement isBuisnessButton;
	@FindBy(how = How.CSS, using = "body > div:nth-child(8) > div:nth-child(3) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(2)")
	WebElement attribute;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement attributeCodeInput;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedPrimary MuiButton-containedSizeSmall MuiButton-sizeSmall']")
	WebElement saveAttributeGridButton;
	@FindBy(how = How.XPATH, using = "//div[contains(@class, '-formContainer')]//*[text() = 'Save']")
	WebElement saveAttributeGridButton2;
	@FindBy(how = How.XPATH, using = "//*[normalize-space()='Save']")
	WebElement saveMatrixPriceVersionButton;
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'MuiAlert-message') or contains(@class ,'MuiSnackbarContent-message')]]")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//*[ @aria-label = 'Publish version' or text() ='Publish version']")
	WebElement publishPriceVersionButton;
	@FindBy(how = How.XPATH, using = "//*[contains(@class, ' css-yt9ioa-option')]")
	WebElement option2;
	@FindBy(how = How.XPATH, using = "//input[@id='label']")
	WebElement priceVersionLabel;
	@FindBy(how = How.XPATH, using = "//input[@id='isBusiness']")
	WebElement buisnessToggle;
	@FindBy(how = How.XPATH, using = "(//div[@role='cell'])[2]")
	WebElement subscriptionDateFirstDimension;
	@FindBy(how = How.XPATH, using = "(//div[@role='cell'])[8]")
	WebElement subscriptionDateSecondDimension;
	@FindBy(how = How.XPATH, using = "/html/body/div/div/div/div/main/div[2]/div/div[2]/div[2]/div/div/fieldset/div/form/div/div/div/div[5]/div/div[2]/div[2]/div/div/div/div[2]/div/div[2]/div[2]/div/div/div/div/div/div[1]/div[2]/div/div/div/input")						
	WebElement firstSubscriptionDateInput;
	@FindBy(how = How.XPATH, using = "/html/body/div/div/div/div/main/div[2]/div/div[2]/div[2]/div/div/fieldset/div/form/div/div/div/div[5]/div/div[2]/div[2]/div/div/div/div[2]/div/div[2]/div[2]/div/div/div/div/div/div[2]/div[2]/div/div/div/input")
	WebElement secondSubscriptionDateInput;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day MuiPickersDay-current MuiPickersDay-daySelected']")
	WebElement selectedDate;

	
	@FindBy(how = How.XPATH, using = "(//div[@data-field='priceOrDiscount'])[2]")
	WebElement unitPriceColumn2;
	@FindBy(how = How.XPATH, using = "(//div[@data-field='priceOrDiscount'])[3]")
	WebElement unitPriceColumn3;
	@FindBy(how = How.XPATH, using = "(//div[@data-field='priceOrDiscount'])[4]")
	WebElement unitPriceColumn4;
	@FindBy(how = How.XPATH, using = "(//div[@data-field='priceOrDiscount'])[5]")
	WebElement unitPriceColumn5;
	@FindBy(how = How.XPATH, using = "(//div[@data-field='edr text parameter 1'])[2]")
	WebElement parameter1Column2;
	@FindBy(how = How.XPATH, using = "(//div[@data-field='edr text parameter 1'])[3]")
	WebElement parameter1Column3;
	@FindBy(how = How.XPATH, using = "(//div[@data-field='edr text parameter 1'])[4]")
	WebElement parameter1Column4;
	@FindBy(how = How.XPATH, using = "(//div[@data-field='edr text parameter 1'])[5]")
	WebElement parameter1Column5;
	
	public String getPriceVersionLabelValue() {
		fWait.until(ExpectedConditions.visibilityOf(priceVersionLabel));
		return priceVersionLabel.getAttribute("value");
	}

	public String getMatrixPriceVersionLabelValue() {
		fWait.until(ExpectedConditions.visibilityOf(priceVersionLabel));
		return priceVersionLabel.getAttribute("value");
	}

	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

	// this method is to click on save button
	public void clickOnSavePriceGridButton() {
		fWait.until(ExpectedConditions.elementToBeClickable(saveButton));
		saveButton.submit();
	}
	public void clickOnSave() {
		clickOn(saveButton);
	}
	// this method is to click on go back button
	public void clickOnGoBackButton() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(goBackButton));
			goBackButton.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			goBackButton.click();
		}
	}

	public void clickOnAddBuisnessAttributeButton() {
		fWait.until(ExpectedConditions.elementToBeClickable(addBuisnessAttributeButton));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", addBuisnessAttributeButton);
//		addBuisnessAttributeButton.click();
	}
	public void clickOnAddAttributeButton() {
		fWait.until(ExpectedConditions.elementToBeClickable(addAttributeButton));
		addAttributeButton.click();
	}
	
	public void clickOnAddNewAttributeButton(String buttonNumber) {
		WebElement addButton = driver.findElement(By.xpath("(//*[contains(@class, 'MuiSvgIcon-fontSizeInherit')])[" + buttonNumber + "]"));
		clickOn(addButton);
	}
	
	public void scrollIntoViewAttributeInput() {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
	        js.executeScript("arguments[0].scrollIntoView(true);", attributeInput);
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void clickOnAttributeInput() {
		fWait.until(ExpectedConditions.elementToBeClickable(attributeInput));
		clickOn(attributeInput);
	}

	public void setAttributeCode(String attributeCode) {
		wait.until(ExpectedConditions.visibilityOf(attributeCodeInput));
		attributeCodeInput.sendKeys(attributeCode);
	}

	public void clickOnAttribute(String attributeCode) {
		// wait.until(ExpectedConditions.elementToBeClickable(attribute));
		// attribute.click();
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//mark[contains(text(),'" + attributeCode + "')]")))
		.click();
	}
	
	public void clickOnSelectedAttribute(String attributeCode) {
		try {
			clickOn(driver.findElement(By.xpath("//mark[contains(text(),'" + attributeCode + "')]")));
		}catch (Exception e) {
			clickOn(driver.findElement(By.xpath("(//tbody//tr//td)[2]")));
		}
	}

	public void activateAttributeisBuisnessButton() {
		isBuisnessButton.click();
	}

	public void clickOnSaveMatrixPriceVersionButton() {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", saveMatrixPriceVersionButton);
		wait.until(ExpectedConditions.elementToBeClickable(saveMatrixPriceVersionButton));
		saveMatrixPriceVersionButton.click();
	}

	public void clickOnSaveAttributeGridButton() {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", saveAttributeGridButton);
		wait.until(ExpectedConditions.elementToBeClickable(saveAttributeGridButton));
		clickOn(saveAttributeGridButton);
	}
	
	public void clickOnSaveAttributeGridButton2() {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", saveAttributeGridButton2);
		clickOn(saveAttributeGridButton2);
	}

	// this method is to set a dimension to a price version
	public void setFirstDescription(String description) {
		wait.until(ExpectedConditions.visibilityOf(description1));
		action = new Actions(driver);
		action.doubleClick(description1).perform();
		descriptionInput1.sendKeys(description);
		unitPrice1.click();
	}

	// this method is to set a unit price
	public void setFirstUnitPrice(String price) throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(unitPriceColumn));
		action = new Actions(driver);
		action.doubleClick(unitPriceColumn).perform();
		fWait.until(ExpectedConditions.visibilityOf(unitPriceInput1));
		unitPriceInput1.sendKeys(Keys.CONTROL + "a");
		unitPriceInput1.sendKeys(Keys.DELETE);
		unitPriceInput1.sendKeys(price);
		Thread.sleep(1000);
		unitPriceInput1.sendKeys(Keys.RETURN);
	}
	// this method is to set a unit price
	public void setUnitPrice(String price) throws InterruptedException {
		action = new Actions(driver);

		try {
			action.doubleClick(unitPriceInput).perform();
		}catch (Exception e) {
			action.doubleClick(unitPriceInput).perform();
		}
		waitPageLoaded();
		clickOn(unitPriceInput1);
		unitPriceInput1.sendKeys(Keys.CONTROL + "a");
		waitPageLoaded();
		unitPriceInput1.sendKeys(price);
		waitPageLoaded();
		unitPriceInput1.sendKeys(Keys.RETURN);
		waitPageLoaded();
	}
	
	public void setSecondDescription(String label) {
		wait.until(ExpectedConditions.visibilityOf(description2));
		action = new Actions(driver);
		action.doubleClick(description2).perform();
		// wait.until(ExpectedConditions.visibilityOf(descriptionInput2));
		descriptionInput2.sendKeys(label);
		unitPrice2.click();
	}
	public void setSubscriptionDateFirstDimension() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOf(subscriptionDateFirstDimension));
		subscriptionDateFirstDimension.click();
		action = new Actions(driver);
		Thread.sleep(100);
		action.doubleClick(subscriptionDateFirstDimension).perform();
		Thread.sleep(100);
		wait.until(ExpectedConditions.visibilityOf(firstSubscriptionDateInput));
		firstSubscriptionDateInput.click();	
		selectedDate.click();
	}
	public void setSubscriptionDateSecondDimension() {
		wait.until(ExpectedConditions.visibilityOf(subscriptionDateSecondDimension));
		action = new Actions(driver);
		action.doubleClick(subscriptionDateSecondDimension).perform();
		action.doubleClick(subscriptionDateSecondDimension).perform();
		secondSubscriptionDateInput.click();	
		selectedDate.click();
	}

	public void setSecondUnitPrice(String price) {
		fWait.until(ExpectedConditions.visibilityOf(unitPrice2));
		action = new Actions(driver);
		action.doubleClick(unitPrice2).perform();
		unitPriceInput2.sendKeys(price);
		//description2.click();
	}

	public void selectFirstAttributeProperty() throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", dimension1);
		wait.until(ExpectedConditions.elementToBeClickable(dimension1));
		action = new Actions(driver);
		action.doubleClick(dimension1).perform();
		Thread.sleep(1000);
		Actions keyDown = new Actions(driver);
		keyDown.sendKeys(Keys.chord(Keys.DOWN, Keys.ENTER)).perform();
		keyDown.sendKeys(Keys.ESCAPE).perform();
		Thread.sleep(1000);
	}

	public void selectSecondAttributeProperty() throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", dimension2);
		wait.until(ExpectedConditions.elementToBeClickable(dimension2));
		action = new Actions(driver);
		action.doubleClick(dimension2).perform();
		Thread.sleep(1000);
		Actions keyDown = new Actions(driver);
		keyDown.sendKeys(Keys.chord(Keys.DOWN, Keys.DOWN, Keys.ENTER)).perform();
		keyDown.sendKeys(Keys.ESCAPE).perform();
		Thread.sleep(1000);
	}

	public void clickOnPublishPriceVersionButton() {
		try {
			clickOn(publishPriceVersionButton);
		}catch (Exception e) {
			fWait.until(ExpectedConditions.elementToBeClickable(publishPriceVersionButton));
			clickOn(publishPriceVersionButton);
		}
		
	}

	public void clickOnBuisnessToggle() {
//		fWait.until(ExpectedConditions.visibilityOf(buisnessToggle));
		buisnessToggle.click();
	}
	
	public void setAttributValue(String id, String value) throws InterruptedException {

		action = new Actions(driver);
		action.doubleClick(parameter1Column2).perform();
		waitPageLoaded();
		WebElement input = null;
		try {
			input = driver.findElement(By.xpath("//input[contains(@id,'" + id +"')]"));
			input.sendKeys(value + Keys.ENTER);
		}catch (Exception e) {
			action.doubleClick(dimension0).perform();
			waitPageLoaded();
			input = driver.findElement(By.xpath("//input[contains(@id,'" + id +"')]"));
			input.sendKeys(value + Keys.ENTER);
		}
		getOutOfTheColumn();
	}
	
	public void setUnitPrice1(String value) throws InterruptedException {
		action = new Actions(driver);
		
		try {
			action.doubleClick(unitPriceColumn2).perform();
			waitPageLoaded();
			unitPriceInput1.sendKeys(value + Keys.ENTER);
		}catch (Exception e) {
			getOutOfTheColumn();
			
			try {
				action.doubleClick(unitPriceColumn2).perform();
				waitPageLoaded();		
				unitPriceInput1.sendKeys(value + Keys.ENTER);
			}catch (Exception ex) {
				getOutOfTheColumn();
				
				try {
					action.doubleClick(unitPriceColumn2).perform();
					waitPageLoaded();		
					unitPriceInput1.sendKeys(value + Keys.ENTER);
					}catch (Exception exp) {
						getOutOfTheColumn();
						
						action.doubleClick(unitPriceColumn2).perform();
						waitPageLoaded();		
						unitPriceInput1.sendKeys(value + Keys.ENTER);
					}
				}
		}
		
		getOutOfTheColumn();
	}
	
	public void setUnitPrice2(String value) throws InterruptedException {

		action = new Actions(driver);
		
		try {
			action.doubleClick(unitPriceColumn3).perform();
			waitPageLoaded();
			unitPriceInput2.sendKeys(value + Keys.ENTER);
		}catch (Exception e) {
			getOutOfTheColumn();
			try {
				action.doubleClick(unitPriceColumn3).perform();
				waitPageLoaded();		
				unitPriceInput2.sendKeys(value + Keys.ENTER);
			}
			
			catch (Exception ex) {
				getOutOfTheColumn();
				
				action.doubleClick(unitPriceColumn3).perform();
				waitPageLoaded();		
				unitPriceInput2.sendKeys(value + Keys.ENTER);
			}
		}
		getOutOfTheColumn();
	}
	
	public void getOutOfTheColumn() throws InterruptedException {
		clickOn(priceVersionLabel);
	}
	
	public void setAttributeValue(String Value, String attributeName, String versionLigne) {
		WebElement elem = null, input;
		action = new Actions(driver);
		List<String> listof = List.of("listofnumericvaluesattribut","listoftextvaluesattribut","multiplelistnumericvaluesattribut","multiplelisttextvaluesattribut");
		try {
			try {
				elem = driver.findElement(By.xpath("(//div[@data-field = '" + attributeName + "'])[" + versionLigne + "]"));
				action.doubleClick(elem).perform();
			}catch (Exception ex) {
				elem = driver.findElement(By.xpath("(//div[@data-field = '" + attributeName + "'])[" + versionLigne + "]"));
				action.doubleClick(elem).perform();
			}
			
		}catch (Exception e) {
			try {
				elem = driver.findElement(By.xpath("(//div[@data-field = '" + attributeName + "'])[" + versionLigne + "]//div"));
				action.doubleClick(elem).perform();
			}catch (Exception ex) {
				elem = driver.findElement(By.xpath("(//div[@data-field = '" + attributeName + "'])[" + versionLigne + "]//div"));
				action.doubleClick(elem).perform();
			}
			
		}
		waitPageLoaded();
		
		// Boolean attribute 
		if(attributeName.equals("booleanattribut")) {
			
			if(Value == "true") {
				try {
					clickOn(driver.findElement(By.xpath("//div[contains(text(), 'Yes')]")));
				}catch (Exception e) {
					try {
						elem = driver.findElement(By.xpath("(//div[@data-field = '" + attributeName + "'])[" + versionLigne + "]"));
						action.doubleClick(elem).perform();
					}catch (Exception ex) {
						elem = driver.findElement(By.xpath("(//div[@data-field = '" + attributeName + "'])[" + versionLigne + "]//div"));
						action.doubleClick(elem).perform();
					}
					waitPageLoaded();
					clickOn(driver.findElement(By.xpath("//div[contains(text(), 'Yes')]")));
				}
				
			}else if (Value == "false") {
				clickOn(driver.findElement(By.xpath("//div[contains(text(), 'No')]")));
			}
		}
		
		// Date attribute
		else if(attributeName.equals("dateattribut")) {
			try {
				input = driver.findElement(By.xpath("//input[contains(@id, '" + attributeName + "')]"));
			}catch (Exception e) {
				elem = driver.findElement(By.xpath("(//div[@data-field = '" + attributeName + "'])[" + versionLigne + "]"));
				action.doubleClick(elem).perform();
				input = driver.findElement(By.xpath("//input[contains(@id, '" + attributeName + "')]"));
			}
			
			clickOn(input);
			WebElement tenthOfTheMonth = driver.findElement(By.xpath("//p[text() = '10']"));
			clickOn(tenthOfTheMonth);
		}
		
		// lists attribute (listOfNumericValuesattribut, listOfTextValuesattribut, multipleListNumericValuesattribut, multipleListTextValuesattribut)
		
		else if( listof.contains(attributeName)) {
			clickOn(driver.findElement(By.xpath("//*[(text()= '" + Value + "')]")));
		}
		
		// input attribute (elattribut, emailattribut, Informationattribut, integerattribut, numericattribut, phoneattribut, textattribut)
		else {
			
			try {
				input = driver.findElement(By.xpath("//input[contains(@id, '" + attributeName + "')]"));
			}catch (Exception e) {
				elem = driver.findElement(By.xpath("(//div[@data-field = '" + attributeName + "'])[" + versionLigne + "]"));
				action.doubleClick(elem).perform();
				input = driver.findElement(By.xpath("//input[contains(@id, '" + attributeName + "')]"));
			}	
			input.sendKeys(Keys.CONTROL + "a");
			input.sendKeys(Keys.DELETE);
			input.sendKeys(Value);
			waitPageLoaded();
			input.sendKeys(Keys.RETURN);
			waitPageLoaded();
		}
	}
}
