package com.opencellsoft.pages.priceversions;

import java.time.Duration;
import java.util.NoSuchElementException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewPriceVersionPage {

	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public NewPriceVersionPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//span[@class='MuiButton-label']//span[2]")
	WebElement createPriceVersion;
	@FindBy(how = How.XPATH, using = "//input[@id='startDate']")
	WebElement startDate;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day MuiPickersDay-current MuiPickersDay-daySelected']")
	WebElement startDateSelected;
	@FindBy(how = How.XPATH, using = "(//input[@id='priceMatrix'])[1]")
	WebElement priceGrid;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Save')]")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "//input[@id='label']")
	WebElement priceVersionLabel;
	@FindBy(how = How.XPATH, using = "//input[@id='startDate']")
	WebElement startDateInput;
	@FindBy(how = How.XPATH, using = "//input[@id='endDate']")
	WebElement endDateInput;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day MuiPickersDay-current MuiPickersDay-daySelected']")
	WebElement startDate2;
	@FindBy(how = How.XPATH, using = "//p[normalize-space()='14']")
	WebElement endDate;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiFormGroup-root']")
	WebElement priceGridButton;
	@FindBy(how = How.XPATH, using = "//input[@id='price']")
	WebElement priceInput;

	// this method is to click on create price version button
	public void clickCreatePriceVersion() {
		createPriceVersion.click();
	}

//this method is to select a calendar
	public void selectCalendar() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOf(startDate));
		startDate.click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOf(startDateSelected));
		startDateSelected.click();
	}

// this method is to click on price grid
	public void clickOnPriceGrid() {
		priceGrid.click();
	}

//this method is to click on save button
	public void clickOnSaveMatrixPriceVersionButton() {
		wait.until(ExpectedConditions.elementToBeClickable(saveButton));
		saveButton.click();
	}

//this method is to click on go back button to return to previous page
	public void clickOnGoBackButton() {
		wait.until(ExpectedConditions.visibilityOf(goBackButton));
		goBackButton.click();
	}

//this method is to set price version label
	public void setPriceVersionLabel(String priceVersioLabel) {
		wait.until(ExpectedConditions.visibilityOf(priceVersionLabel));
		priceVersionLabel.sendKeys(priceVersioLabel);
	}

//this method is to set price version start date
	public void setStartDate() {

		startDateInput.click();
		startDate2.click();
	}

//this method is to set price version end date
	public void setEndDate() {
		endDateInput.click();
		endDate.click();
	}

//this method is to select a price version with a price grid 
	public void clickOnPriceGridButton() {
		wait.until(ExpectedConditions.elementToBeClickable(priceGridButton));
		priceGridButton.click();
	}

//this method is to set a price to a price version
	public void setPrice(String price) {
		priceInput.sendKeys(price);
	}

	public void createPricePlanVersion(String pvLabel, String price) {
		this.setPriceVersionLabel(pvLabel);
		this.setStartDate();
//		this.setEndDate();
		this.setPrice(price);
		this.clickOnSaveMatrixPriceVersionButton();
	}

}
