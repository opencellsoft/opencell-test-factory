package com.opencellsoft.pages.offers;

import java.time.Duration;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class ListOfOffersPage extends TestBase {

	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public ListOfOffersPage(WebDriver driver) {
		  this.driver = driver;
		  wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		  fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10)).pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
	}
	
	//Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//button[@title='Create' or contains(@class,'MuiButton-colorSecondary') or contains(@class,'MuiButton-textSecondary')]")
	WebElement createOfferButton;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement filterButton;
	@FindBy(how = How.CSS, using = "li:nth-child(1)")
	WebElement filterCodeButton;
	@FindBy(how = How.XPATH, using = "(//SPAN[text()='Code'])[2]")
	WebElement filterByOfferCodeButton;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement offerCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase name' or @id='searchBar']")
	WebElement offerName;
	@FindBy(how = How.XPATH, using = "//tbody//td[2]")
	WebElement firstLine;
	@FindBy(how = How.XPATH, using = "//span[contains(.,'Tags')]")
	WebElement tagsButton;
	//@FindBy(how = How.XPATH, using = "//span[contains(.,'Products')]")
	@FindBy(how = How.XPATH, using = "//a[contains(@href,'/opencell/frontend/DEMO/portal/CPQ/products')]")
	WebElement productsButton;
	@FindBy(how = How.XPATH, using = "//button[@aria-label,'open drawer']")
	WebElement openDrawer;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase name']")
	WebElement filterName;
	@FindBy(how = How.CSS, using = "li:nth-child(1)")
	WebElement filterStatusButton;
	@FindBy(how = How.XPATH, using = "//div[@id='lifeCycleStatus']")
	WebElement offerStatusList;
	@FindBy(how = How.CSS, using = "li:nth-child(1)")
	WebElement selectedOfferStatus;
	@FindBy(how = How.CSS, using = "li:nth-child(1)")
	WebElement filterDateButton;
	@FindBy(how = How.XPATH, using = "//input[@id='validity.from']")
	WebElement fromDateList;
	@FindBy(how = How.CSS, using = "div[class='MuiPaper-root MuiPopover-paper MuiPaper-elevation8 MuiPaper-rounded'] div:nth-child(3) div:nth-child(4) button:nth-child(1)")
	WebElement fromDateSelected;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Last']")
	WebElement lastOfferViewed;
	@FindBy(how = How.XPATH, using = "//div[@id='mui-88609']")
	WebElement listPerPageRows;
	@FindBy(how = How.XPATH, using = "//li[@class='MuiButtonBase-root MuiListItem-root MuiMenuItem-root MuiTablePagination-menuItem Mui-selected MuiMenuItem-gutters MuiListItem-gutters MuiListItem-button Mui-selected']")
	WebElement rowsPerPageSelected;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Cancel')]")
	WebElement cancelButton;	
	// Defining all the user actions ( Methods) that can be performed in the
	// OC home page

	// This method is to click on Create Offer Button	
	public void clickOnCreateOfferButton() {
		clickOn(createOfferButton);
	}
	
	// This method is to click on Filter Button
	public void clickOnFilterButton() {
		wait.until(ExpectedConditions.elementToBeClickable(filterButton));
		clickOn(filterButton);
	}
	
	// This method is to click on Filter Code Button
	public void clickOnFilterCodeButton() {
		wait.until(ExpectedConditions.elementToBeClickable(filterCodeButton));
		clickOn(filterCodeButton);
	}
	
	// This method is to click on Filter Code Button
	public void clickOnFilterByOfferCode() {
		clickOn(filterByOfferCodeButton);
	}
	
	// This method is to set Offer Code in the code text box
	public void setOfferCode(String strOfferCode) {
		offerCode.sendKeys(strOfferCode);
		waitPageLoaded();
	}

	public void setOfferDescription(String offerDescription) {
		offerName.sendKeys(offerDescription);
		waitPageLoaded();
		
	}
	
	// This method is to select offer
	public void clickOnOffer() {
		try {
			clickOn(firstLine);
		}catch (Exception e) {
			clickOn(driver.findElement(By.xpath("//tbody//a")));
		}
	}	
	
	// This method is to select offer
	public void clickOnOffer(String offerName) {
		try {
			clickOn(firstLine);
		}catch (Exception e) {
			clickOn(driver.findElement(By.xpath("//*[text() = '" + offerName +"']")));
		}
	}	
	
	// This method is to set Code Offer Found
	public void clickOnCodeOfferFound(String offerCode) {
		WebElement elem = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//mark[contains(text(),'" + offerCode + "')]")));
		elem.click();
	}
	
	// This method to click on tags Button
	public void clickOnTagsButton() {
		tagsButton.click();
	}
	
	// This method to click on products Button
	public void clickOnProductsButton() {
		try {
		    wait.until(ExpectedConditions.elementToBeClickable(productsButton));
			//this.openDrawer.click();
		    productsButton.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
		    wait.until(ExpectedConditions.elementToBeClickable(productsButton));
		    productsButton.click();
		}
	}
	
	// This method is to set name Offer
	public void setFilterName(String setNameOffer) {
		filterName.sendKeys(setNameOffer);
		waitPageLoaded();
	}
	
	// This method is to click on filter status button
	public void clickOnFilterStatusButton() {
		clickOn(filterStatusButton);
	}
	
	// This method is to click on type status
	public void clickOnOfferStatus() {
		Actions act = new Actions(driver);
		act.moveToElement(offerStatusList).click().perform();
		waitPageLoaded();
		act.moveToElement(selectedOfferStatus).click().perform();
		waitPageLoaded();
	}
	
	// This method is to click on last offer viewed button
	public void clickOnLastOfferViewed() {
		// click on the button as soon as the "products" button is visible     
	    wait.until(ExpectedConditions.visibilityOf(productsButton));
		lastOfferViewed.click() ;
	}
	
	// This method is to click on rows per page
	public void clickOnRowsPerPage() {
		Actions act = new Actions(driver);
		act.moveToElement(listPerPageRows).click().perform();
		act.moveToElement(rowsPerPageSelected).click().perform();
	
	}
	
	// This method is to click on cancel button
	public void clickOnCancelButton() {
		wait.until(ExpectedConditions.visibilityOf(cancelButton));
		clickOn(cancelButton);
	}
}