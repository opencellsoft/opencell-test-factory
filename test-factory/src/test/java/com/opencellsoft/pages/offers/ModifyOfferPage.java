package com.opencellsoft.pages.offers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ModifyOfferPage {

	WebDriver driver ;

	public ModifyOfferPage(WebDriver driver) {
		this.driver = driver;
	}
	
	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//input[@id='newValidFrom']")
	WebElement newValidFromList;
	@FindBy(how = How.CSS, using = "div[class='MuiPaper-root MuiPopover-paper MuiPaper-elevation8 MuiPaper-rounded'] div:nth-child(3) div:nth-child(5) button:nth-child(1)")
	WebElement newValidFromSelectedUp;
	@FindBy(how = How.XPATH, using = "//input[@id='updatedCode']")
	WebElement offerCode;
	@FindBy(how = How.XPATH, using = "//input[@id='name']")
	WebElement offerDescription;
	@FindBy(how = How.XPATH, using = "//div[@id='renewalRule.initialTermType']")
	WebElement initialTermTypeListUp;
	@FindBy(how = How.XPATH, using = "//li[@class='MuiButtonBase-root MuiListItem-root MuiMenuItem-root Mui-selected MuiMenuItem-gutters MuiListItem-gutters MuiListItem-button Mui-selected']")
	WebElement initialTermTypeSelectedUp;
	@FindBy(how = How.XPATH, using = "//input[@id='offerProductDialog.quantityDefault']")
	WebElement defaultQuantite;
	@FindBy(how = How.XPATH, using = "//input[@id='offerProductDialog.quantityMin']")
	WebElement minimalQuantite;
	@FindBy(how = How.XPATH, using = "//input[@id='offerProductDialog.quantityMax']")
	WebElement maximalQuantite;
	@FindBy(how = How.XPATH, using = "//input[@id='offerProductDialog.sequence']")
	WebElement sequence;

	// This method is to update calendar start date  
	public void setValidFromDate() {	
		Actions act = new Actions(driver);
		act.moveToElement(newValidFromList).click().perform();
		act.moveToElement(newValidFromSelectedUp).click().perform();
	}
	
	// This method is to update Offer Code in the code text box
	public void setOfferCodeUpdate(String strOfferCode) {
		offerCode.sendKeys(strOfferCode);
	}

	// This method is to update Offer Description in the Description text box
	public void setOfferDescriptionUpdate(String strOfferDescription) {
		offerDescription.sendKeys(strOfferDescription);
	}

	// This method is to update default quantity in the default quantity text box
	public void setDefaultQuantiteUp(String setDefaultQuantity) {
		defaultQuantite.sendKeys(setDefaultQuantity);
	}

	// This method is to update minimal quantity in the minimal quantity text box
	public void setMinimalQuantiteUp(String setMinimalQuantity) {
		minimalQuantite.sendKeys(setMinimalQuantity);
	}

	// This method is to update maximal quantity in the maximal quantity text box
	public void setMaximalQuantiteUp(String setMaximalQuantity) {
		maximalQuantite.sendKeys(setMaximalQuantity);
	}

	// This method is to update sequence in sequence text box
	public void setSequenceUp(String setSequence) {
		sequence.sendKeys(setSequence);
	}

	// This method is to update initial term type
	public void clickOnInitialTermType() {
		Actions act = new Actions(driver);
		act.moveToElement(initialTermTypeListUp).click().perform();
		act.moveToElement(initialTermTypeSelectedUp).click().perform();
	}
}
