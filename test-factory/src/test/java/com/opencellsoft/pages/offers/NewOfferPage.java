package com.opencellsoft.pages.offers;

import java.time.Duration;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class NewOfferPage extends TestBase {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public NewOfferPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(2)).pollingEvery(Duration.ofSeconds(10)).ignoring(NoSuchElementException.class);
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement offerCode;
	@FindBy(how = How.XPATH, using = "//input[@id='name']")
	WebElement offerDescription;
	@FindBy(how = How.XPATH, using = "//input[@id='newValidFrom']")
	WebElement validFromInput;
	//@FindBy(how = How.XPATH, using = "//body[@style='overflow: hidden;']/div[@class='MuiPopover-root']/div[@class='MuiPaper-root MuiPopover-paper MuiPaper-elevation8 MuiPaper-rounded']/div[@class='MuiPickersBasePicker-container']/div[@class='MuiPickersBasePicker-pickerView']/div[@class='MuiPickersSlideTransition-transitionContainer MuiPickersCalendar-transitionContainer']/div/div[2]/div[2]/button[1]/span[1]")
	//@FindBy(how = How.XPATH, using = "//P[@class='MuiTypography-root MuiTypography-body2 MuiTypography-colorInherit'][text()='1']")
	@FindBy(how = How.XPATH, using = "(//P[@class='MuiTypography-root MuiTypography-body2 MuiTypography-colorInherit'][text()='1'])[1]")
	WebElement validFromDate;	
	@FindBy(how = How.ID, using = "mui-component-select-seller")
	WebElement sellersList;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement submitButton;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiFormControl-root MuiTextField-root ReferenceListInputListPickerText MuiFormControl-marginDense']")
	WebElement discountPlanInput;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']")
	WebElement filterByCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement discountCode ;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]")
	WebElement discount;
	

	
	// click on add filter button
		public void clickOnAddFilterButton() {
			filterButton.click();
		}
		public void selectDiscountPlan() {
			discount.click();
		}
		public void setDiscountCode(String discountCode) {
			this.discountCode.sendKeys(discountCode);
		}
		public void clickOnAddFilterByCode() {
			filterByCode.click();
		}
	// This method is to set offer code in the code text box
	public void setOfferCode(String strOfferCode) {
		offerCode.sendKeys(strOfferCode);
	}

	// This method is to set offer description in the description text box
	public void setOfferDescription(String strOfferDescription) {
		wait.until(ExpectedConditions.elementToBeClickable(offerDescription));
		offerDescription.sendKeys(strOfferDescription);
	}
	
	// This method is to select calendar start date
	public void setValidFromDate() {
		// explicit wait - to wait for the button to be click-able
		fWait.until(ExpectedConditions.elementToBeClickable(validFromInput));
		validFromInput.click();
		fWait.until(ExpectedConditions.elementToBeClickable(validFromDate));
		validFromDate.click();
	}

	// This method is to select seller
	public void selectSeller(String sellerCode) {
		clickOn(sellersList);
		clickOn(sellersList.findElement(By.xpath("//li[contains(.,'"+sellerCode+"')]")));
	}
	
	// This method is to click on Submit Button
	public void clickOnSubmitButton() {
		// explicit wait - to wait for the button to be click-able
		wait.until(ExpectedConditions.visibilityOf(submitButton));
		clickOn(submitButton);
	}

	public void clickOnDiscountPlanInput() {
		// TODO Auto-generated method stub
		discountPlanInput.click();
	}
}