package com.opencellsoft.pages.offers;

import java.awt.AWTException;
import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class OfferPage extends TestBase{
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public OfferPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);

	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//input[@id='updatedCode']")
	WebElement offerCode;
	@FindBy(how = How.XPATH, using = "//input[@id='name']")
	WebElement offerDescription;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//div[@listid='OFFER_PRODUCT' or contains(@deletemessagetitle , 'catalog/offers')]//button[2]")
	WebElement createProductButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Medias')]")
	WebElement mediaTab;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Commercial rules')]")
	WebElement commercialRulesTab;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Products')]")
	WebElement productsTab;
	@FindBy(how = How.XPATH, using = "(//*[contains(text(),'Products')])[2]")
	WebElement productsTab2;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Attributes')]")
	WebElement attributesTab;
	@FindBy(how = How.CSS, using = ".MuiBox-root:nth-child(4) .ButtonGroupGutters:nth-child(1) .MuiButton-label:nth-child(1) > span:nth-child(2)")
	WebElement createAttributeButton;
	@FindBy(how = How.CSS, using = ".MuiGrid-root:nth-child(1) > .MuiPaper-root:nth-child(1) > .MuiBox-root:nth-child(7) .ButtonGroupGutters:nth-child(1) > .MuiButtonBase-root:nth-child(1) > .MuiButton-label:nth-child(1)")
	WebElement pickAttributeButton;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Activate']")
	WebElement activateOfferButton;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Activate')]")
	WebElement activateOfferButton2;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Save')]")
	WebElement saveOfferButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "(//span[@class='MuiSwitch-root MuiSwitch-sizeSmall'])[1]")
	WebElement restrictOffersGrid;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiGrid-root gridBlock fullWidthChild fullWidthChild MuiGrid-container MuiGrid-item MuiGrid-grid-xs-6']//div[@class='MuiGrid-root gridBlock fullWidthChild fullWidthChild']//div[@class='MuiFormControl-root MuiTextField-root ReferenceListInputListPickerText MuiFormControl-marginDense']")
	WebElement allowedOffersChange;
	//	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']//span[@class='MuiButton-label']//*[name()='svg']")
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//button[@aria-label='Add filter' or @aria-label='Filter' or @title = 'Filter']")
	WebElement addFilter;
	@FindBy(how = How.XPATH, using = "//li//span[contains(text(),'Product Code') or contains(text(),'Product code')]")
	WebElement productCodeOption;
	@FindBy(how = How.CSS, using = "li:nth-child(1)")
	WebElement addFilterCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement setCodeOffer;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]")
	WebElement offer;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Custom Fields')]")
	WebElement customFieldsTab;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiGrid-root gridBlock MuiGrid-container MuiGrid-spacing-xs-2 MuiGrid-align-items-xs-flex-start MuiGrid-justify-content-xs-flex-end MuiGrid-grid-xs-6 MuiGrid-grid-sm-4']//div[3]")
	WebElement closeOfferButton;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text ra-confirm jss1686']")
	WebElement confirmCloseOfferButton;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-contained jss53 ra-delete-button jss1685 MuiButton-containedPrimary MuiButton-containedSizeSmall MuiButton-sizeSmall']")
	WebElement deleteOffer;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text ra-confirm jss1686']")
	WebElement confirmDeleteOffer;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Parameters')]")
	WebElement parameters;
	@FindBy(how = How.XPATH, using = "//input[@id='offerProductDialog.quantityDefault']")
	WebElement defaultQuantite;
	@FindBy(how = How.XPATH, using = "//input[@id='offerProductDialog.quantityMin']")
	WebElement minimalQuantite;
	@FindBy(how = How.XPATH, using = "//input[@id='offerProductDialog.quantityMax']")
	WebElement maximalQuantite;
	@FindBy(how = How.XPATH, using = "//input[@id='offerProductDialog.sequence']")
	WebElement sequence;
	@FindBy(how = How.XPATH, using = "//span[text()='Mandatory']")
	WebElement Mandatory;
	@FindBy(how = How.XPATH, using = "//span[text()='Visible']")
	WebElement Visible;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Confirm')]")
	WebElement confirm;
	@FindBy(how = How.XPATH, using = "//span[@class='']//input[@type='checkbox']")
	WebElement checkBoxProduct;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Dissociate']")
	WebElement dissociate;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text ra-confirm jss2776']")
	WebElement confirmDissociation;
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Duplicate')]")
	WebElement duplicate;
	@FindBy(how = How.XPATH, using = "//input[@name='tags']")
	WebElement tags;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addFilterTag;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']")
	WebElement filterTagCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement tagCode;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]")
	WebElement tagSelected;
	@FindBy(how = How.XPATH, using = "(//*[name()='svg'][@class='MuiSvgIcon-root MuiChip-deleteIcon'])[1]")
	WebElement checkBoxTag;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Initial term')]")
	WebElement initialTermTab;
	@FindBy(how = How.XPATH, using = "//div[@id='renewalRule.initialTermType']")
	WebElement initialTermTypeList;
	@FindBy(how = How.XPATH, using = "//input[@id='renewalRule.initialyActiveFor']")
	WebElement subscriptionPeriod;
	@FindBy(how = How.XPATH, using = "//div[@id='renewalRule.initialyActiveForUnit']")
	WebElement initialyActiveUnitList;
	@FindBy(how = How.XPATH, using = "//input[@id='autoEndOfEngagement']")
	WebElement endEngagement;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Renewal conditions')]")
	WebElement renewalConditionsTab;
	@FindBy(how = How.XPATH, using = "//div[@id='renewalRule.renewalTermType']")
	WebElement renewalConditionsTypeList;
	@FindBy(how = How.XPATH, using = "//input[@id='renewalRule.renewFor']")
	WebElement subscriptionRenewalPeriod;
	@FindBy(how = How.XPATH, using = "//div[@id='renewalRule.renewForUnit']")
	WebElement renewUnitList;
	@FindBy(how = How.XPATH, using = "//li[@class='MuiButtonBase-root MuiListItem-root MuiMenuItem-root Mui-selected MuiMenuItem-gutters MuiListItem-gutters MuiListItem-button Mui-selected']")
	WebElement renewUnitSelected;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Create')]")
	WebElement createMedia;
	@FindBy(how = How.XPATH, using = "//body[1]/div[1]/div[1]/div[1]/div[1]/main[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/fieldset[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[5]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/span[1]/span[1]/input[1]")
	WebElement checkBox;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Delete']")
	WebElement deleteMedia;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Confirm']")
	WebElement confirmDeleteMedia;
	//	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Pick')]")
	@FindBy(how = How.XPATH, using = "(//span[@class='MuiButton-label'][normalize-space()='Pick'])[2]")
	WebElement pickProductButton;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement codeProduct;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[2]")
	WebElement productList;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter' or @title='Filter']")
	WebElement addFilterButton;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Product Code')]")
	WebElement filterProductCode;
	@FindBy(how = How.CSS, using = "//div[@class='MuiBox-root jss5522 TabPanel']//div[@class='MuiToolbar-root MuiToolbar-dense jss5516 MuiToolbar-gutters']//button[@type='button']")
	WebElement createCommercialRule;
	@FindBy(how = How.XPATH, using = "//span[@class='']//input[@type='checkbox']")
	WebElement checkBoxCommercialRule;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiToolbar-root MuiToolbar-dense jss5782 MuiToolbar-gutters']//button[@aria-label='Delete']")
	WebElement deleteCommercialRule;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Confirm']")
	WebElement confirmDeleteCommercialRule;
	@FindBy(how = How.XPATH, using = "//span[@class='']//input[@type='checkbox']")
	WebElement checkBoxAtt;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Delete']")
	WebElement deleteAttribute;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Confirm']")
	WebElement confirmDeleteAttribute;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiFormControl-root MuiTextField-root ReferenceListInputListPickerText MuiFormControl-marginDense']")
	WebElement discountPlanInput;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addFilterDP;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']")
	WebElement filterCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement codeDiscountPlan;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]")
	WebElement discountPlanSelected;
	@FindBy(how = How.XPATH, using = "(//*[name()='svg'][@class='MuiSvgIcon-root MuiChip-deleteIcon'])[1]")
	WebElement removeDiscountPlan;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Actions')]")
	WebElement actionsButton;
	@FindBy(how = How.XPATH, using = "//span[@title='Published']")
	WebElement statusSpan;
	@FindBy(how = How.XPATH, using = "//table[@filters='[object Object]']//span[contains(text(),'Product Code')]")
	WebElement sortByNameButton;
	@FindBy(how = How.XPATH, using = "(//button[normalize-space()='Pick'])[2]")
	WebElement productPickButton;
	@FindBy(how = How.XPATH, using = "(//span[normalize-space()='Pick'])[2]")
	WebElement productPickButton2;
	@FindBy(how = How.XPATH, using = "(//input[@id = 'wildcardOrIgnoreCase description'])")
	WebElement productDescriptionInput;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Cancel']")
	WebElement cancelButton;
	public void clickOnSortButton() {
		// TODO Auto-generated method stub
		wait.until(ExpectedConditions.elementToBeClickable(sortByNameButton));
		sortByNameButton.click();
	}
	public String getStatusText() {
		// TODO Auto-generated method stub
		return statusSpan.getAttribute("value");
	}
	// This method is to get text message
	public String getTextMessage() {
		fWait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

	public String getOfferCode() {
		return offerCode.getAttribute("value");
	}

	public String getOfferDescription() {
		return offerDescription.getAttribute("value");
	}

	// This method is to click on create product button
	public void clickOnCreateProductButton() throws InterruptedException, AWTException {

		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", createProductButton);
			createProductButton.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(createProductButton));
			createProductButton.click();
		}	
	}

	// This method is to click on media tab
	public void clickOnMediaTab() {
		mediaTab.click();
	}

	// This method is to click on attribute tab
	public void clickOnAttributesTab() {
		wait.until(ExpectedConditions.elementToBeClickable(attributesTab));
		attributesTab.click();
	}

	// This method is to click on product tab
	public void clickOnProductsTab() {
		wait.until(ExpectedConditions.elementToBeClickable(productsTab));
		productsTab.click();
	}
	// This method is to click on product tab
	public void clickOnProductsTab2() {
		wait.until(ExpectedConditions.elementToBeClickable(productsTab2));
		clickOn(productsTab2);
		clickOn(productsTab2);
	}
	// This method is to click on commercial rules tab
	public void clickOnCommercialRulesTab() {
		wait.until(ExpectedConditions.elementToBeClickable(commercialRulesTab));
		commercialRulesTab.click();
	}

	// This method is to click on create attribute button
	public void clickOnCreateAttributeButton() {
		wait.until(ExpectedConditions.visibilityOf(createAttributeButton));
		createAttributeButton.click();
	}

	// This method is to click on activate offer
	public void clickOnActivateOfferButton() throws InterruptedException {
		/*JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(250, 0)");
		Thread.sleep(1000);
		Actions actions = new Actions(driver);
		actions.moveToElement(activateOfferButton).click().perform();*/
		wait.until(ExpectedConditions.elementToBeClickable(activateOfferButton));
		activateOfferButton.click();
	}

	public void clickOnActivateOfferButton2() throws InterruptedException {
		clickOn(activateOfferButton2);
	}

	// This method is to click on pick attribute
	public void clickOnPickAttributeButton() {
		wait.until(ExpectedConditions.elementToBeClickable(pickAttributeButton));
		pickAttributeButton.click();
	}

	// This method is to click on go back button
	public void clickOnGoBackButton() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(goBackButton));
			goBackButton.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(goBackButton));
			goBackButton.click();
		}
	}

	// This method is to click on save button
	public void clickOnSaveButton() {
		try {
			wait.until(ExpectedConditions.visibilityOf(saveButton));
			saveButton.submit();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.visibilityOf(saveButton));
			saveButton.submit();
		}
	}

	public void clickOnSaveOfferButton() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(saveOfferButton));
			saveOfferButton.click();
		}
		catch(org.openqa.selenium.ElementClickInterceptedException ex) {
			wait.until(ExpectedConditions.elementToBeClickable(saveOfferButton));
			saveOfferButton.click();
		}
	}

	// This method is to click on restrict offers grid
	public void clickOnRestrictOffersGrid() {
		wait.until(ExpectedConditions.visibilityOf(restrictOffersGrid));
		restrictOffersGrid.click();
	}

	// This method is to click on allowed offers for subscription changes
	public void clickOnAllowedOffersChange() {
		wait.until(ExpectedConditions.visibilityOf(allowedOffersChange));
		allowedOffersChange.click();
	}

	// This method is to click on add filter
	public void clickOnAddFilter() {
		clickOn(addFilter);
	}

	// This method is to click on add filter code
	public void clickOnAddFilterCode() {
		addFilterCode.click();
	}

	// This method is to set code offer in the code text box
	public void setCodeOffer(String strOfferCode) {
		setCodeOffer.sendKeys(strOfferCode);
	}

	// This method is to select offer
	public void clickOffer() {
		offer.click();
	}

	// This method is to click on customer fields tab
	public void clickOnCustomFieldsTab() {
		customFieldsTab.click();
	}

	// This method is to click close offer button
	public void clickOnCloseOfferButton() {
		closeOfferButton.click();
	}

	// This method is to confirm close offer button
	public void clickOnConfirmCloseOfferButton() {
		confirmCloseOfferButton.click();
	}

	// This method is to delete offer button
	public void clickOnDeleteOfferButton() {
		deleteOffer.click();
	}

	// This method is to confirm delete offer button
	public void clickOnConfirmDeleteOfferButton() {
		confirmDeleteOffer.click();
	}

	// This method is to click on parameters product button
	public void clickOnParametersProductButton() {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", parameters);
		parameters.click();
	}

	// This method is to set default quantity
	public void setDefaultQuantity(String setDefaultQuantity) {
		defaultQuantite.sendKeys(Keys.CONTROL + "a");
		defaultQuantite.sendKeys(setDefaultQuantity);
	}

	// This method is to set minimal quantity
	public void setMinimalQuantity(String setMinimalQuantity) {
		minimalQuantite.sendKeys(Keys.CONTROL + "a");
		minimalQuantite.sendKeys(setMinimalQuantity);
	}

	// This method is to set maximal quantity
	public void setMaximalQuantity(String setMaximalQuantity) {
		maximalQuantite.sendKeys(Keys.CONTROL + "a");
		maximalQuantite.sendKeys(setMaximalQuantity);
	}

	// This method is to set sequence
	public void setSequence(String setSequence) {
		sequence.sendKeys(Keys.CONTROL + "a");
		sequence.sendKeys(setSequence);
	}
	// This method is to set Mandatory
	public void setMandatory() {
		Mandatory.click();
	}
	// This method is to set Visible
	public void setVisible() {
		Visible.click();
	}
	// This method is to click on confirm button
	public void clickOnConfirm() {
		confirm.click();
	}

	// This method is to click on check box product
	public void clickOnCheckBoxProduct() {
		checkBoxProduct.click();
	}

	// This method is to click on dissociate product button
	public void clickOnDissociateProduct() {
		dissociate.click();
	}

	// This method is to click on confirm dissociation button
	public void clickOnConfirmDissociation() {
		confirmDissociation.click();
	}

	// This method is to click on duplicate offer button
	public void clickOnDuplicateOffer() {
		wait.until(ExpectedConditions.visibilityOf(this.duplicate));
		duplicate.click();
	}

	public void clickOnActionsButton() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(actionsButton));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", actionsButton);
			//actionsButton.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(actionsButton));
			Actions actions = new Actions(driver);
			actions.moveToElement(actionsButton).click().perform();
		}
	}

	// This method is to click on tags text box
	public void clickOnTags() {
		tags.click();
	}

	// This method is to click on add filter tag
	public void clickOnAddFilterTag() {
		addFilterTag.click();
	}

	// This method is to click on filter tag code
	public void clickOnTagCodeFilter() {
		filterTagCode.click();
	}

	// This method is to set tag code in the code text box
	public void setTagCode(String setTagCode) {
		tagCode.sendKeys(setTagCode);
	}

	// This method is to select tag
	public void clickOnTagSelected() {
		tagSelected.click();
	}

	// This method is to click on check tag
	public void clickOnCheckBoxTag() {
		checkBoxTag.click();
	}

	// This method is to click on initial term tab
	public void clickOnInitialTermTab() {
		initialTermTab.click();
	}

	// This method is to select initial term type
	public void clickOnInitialTermType(String initialTermTypeSelected) {
		initialTermTypeList.click();
		new Select(initialTermTypeList).selectByVisibleText(initialTermTypeSelected);
	}

	// This method is to set subscription period
	public void setSubscriptionPeriod(String setSubscriptionPeriod) {
		subscriptionPeriod.sendKeys(setSubscriptionPeriod);
	}

	// This method is to click on initial active unit
	public void clickOnInitialyActiveUnit(String initialyActiveUnitSelected) {
		initialyActiveUnitList.click();
		new Select(initialyActiveUnitList).selectByVisibleText(initialyActiveUnitSelected);
	}

	// This method is to click radio button of end engagement
	public void clickOnEndEngagement() {
		endEngagement.click();
	}

	// This method is to click on renewal conditions tab
	public void clickOnRenewalConditionsTab() {
		renewalConditionsTab.click();
	}

	// This method is to click on renewal conditions type
	public void clickOnRenewalConditionsType(String renewalConditionsTypeSelected) {
		renewalConditionsTypeList.click();
		new Select(renewalConditionsTypeList).selectByVisibleText(renewalConditionsTypeSelected);
	}

	// This method is to set subscription renewal period
	public void setSubscriptionRenewalPeriod(String setSubscriptionRenewalPeriod) {
		subscriptionRenewalPeriod.sendKeys(setSubscriptionRenewalPeriod);
	}

	// This method is to select renew unit type
	public void clickRenewUnit(String renewUnitSelected) {
		renewUnitList.click();
		new Select(renewUnitList).selectByVisibleText(renewUnitSelected);
	}

	// This method is to click on create media button
	public void clickOnCreateMedia() {
		createMedia.click();
	}

	// This method is to select check box media
	public void clickOnCheckBoxMedia() {
		checkBox.click();
	}

	// This method is to click on delete media
	public void clickOnDeleteMedia() {
		deleteMedia.click();
	}

	// This method is to confirm delete media
	public void clickOnConfirmDeleteMedia() {
		confirmDeleteMedia.click();
	}

	// This method is to click on pick product button
	public void clickOnPickProductButton() {
		wait.until(ExpectedConditions.elementToBeClickable(pickProductButton));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", pickProductButton);
		pickProductButton.click();
	}

	// This method is to click on add filter button
	public void clickOnAddFilterButton() {
		//JavascriptExecutor js = (JavascriptExecutor) driver;
		//js.executeScript("arguments[0].click();", addFilterButton);
		wait.until(ExpectedConditions.elementToBeClickable(addFilterButton));
		addFilterButton.click();
	}

	// This method is to click on add filter button
	public void clickOnAddFilterButton2() {
		clickOn(addFilterButton);
	}
	// click on filter product by code
	public void clickOnFilterProductCode() {
		wait.until(ExpectedConditions.elementToBeClickable(filterProductCode));
		filterProductCode.click();
	}

	// This method is to set product code
	public void setCodeProduct(String strProductCode) {
		try {wait.until(ExpectedConditions.visibilityOf(codeProduct));}catch (Exception e) {}
		codeProduct.sendKeys(strProductCode);
		waitPageLoaded();

	}

	// This method is to select product
	public void clickOnProductList() {
		wait.until(ExpectedConditions.elementToBeClickable(productList));
		productList.click();
	}

	// This method is to set Code Product Found
	public void clickOnProductCodeFound(String strProductCodeFound) {
		//		Actions act = new Actions(driver);
		//act.sendKeys(Keys.chord(Keys.SPACE, Keys.BACK_SPACE)).perform();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//MARK[text()='" + strProductCodeFound + "']")));
		codeProduct.findElement(By.xpath("//MARK[text()='" + strProductCodeFound + "']")).click();
	}

	// This method is to click on create commercial rule
	public void clickOnCreateCommercialRule() {
		createCommercialRule.click();
	}

	// This method is to click on check box commercial rule
	public void clickOnCheckBoxCommercialRule() {
		checkBoxCommercialRule.click();
	}

	// This method is to click on delete commercial rule
	public void clickOnDeleteCommercialRule() {
		deleteCommercialRule.click();
	}

	// This method is to click on confirm delete commercial rule
	public void clickOnConfirmDeleteCommercialRule() {
		confirmDeleteCommercialRule.click();
	}

	// This method is to click on check box of an attribute
	public void clickOnCheckBoxAttribute() {
		checkBoxAtt.click();
	}

	// This method is to click on delete attribute
	public void clickOnDeleteAttribute() {
		deleteAttribute.click();
	}

	// This method is to confirm delete attribute
	public void clickOnConfirmDeleteAttribute() {
		confirmDeleteAttribute.click();
	}

	// This method is to click on discount plans
	public void clickOnDiscountPlanInput() {
		wait.until(ExpectedConditions.elementToBeClickable(discountPlanInput));
		discountPlanInput.click();
	}

	// This method is to click on add filter discount plans
	public void clickOnAddFilterDP() {
		addFilterDP.click();
	}

	// This method is to click on filter code of discount plans
	public void clickOnFilterCode() {
		filterCode.click();
	}

	// This method is to set code discount plan
	public void setCodeDiscountPlan(String setDiscountPlanCode) {
		codeDiscountPlan.sendKeys(setDiscountPlanCode);
	}

	// This method is to select discount plans
	public void clickOnDiscountPlanSelected() {
		discountPlanSelected.click();
	}

	// This method is to remote discount plans
	public void clickOnRemoveDiscountPlan() {
		removeDiscountPlan.click();
	}

	public void clickOnProductCode(String productCode) throws AWTException {
		WebElement elem = 
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'" + productCode + "')]")));
		elem.click();
	}


	public boolean isProductPicked(String ProductCode) {
		List<WebElement> Products = driver.findElements(By.xpath("//span[normalize-space()='" + ProductCode + "']"));
		if(Products.size()==0) { return false;}else {return true;}
	}

	public void clickOnPickButton(){
		clickOn(productPickButton);
	}
	public void clickOnPickButton2(){
		clickOn(productPickButton2);
	}

	public void clickOnCancelButton(){
		clickOn(cancelButton);
	}

	public void clickOnProductCodeOption(){
		clickOn(productCodeOption);
	}	

	public void setProductDescription(String Description) {
		productDescriptionInput.sendKeys(Description);
		waitPageLoaded();
	}

	public void selectProductToPick(String Description) {
		try {
			WebElement product = driver.findElement(By.xpath("//div[@role = 'dialog']//*[text() = '" + Description + "']"));
			clickOn(product);
		}
		catch (Exception e) {
			clickOn(driver.findElement(By.xpath("(//div[@role = 'dialog']//tbody//td)[2]")));
		}
	}
}