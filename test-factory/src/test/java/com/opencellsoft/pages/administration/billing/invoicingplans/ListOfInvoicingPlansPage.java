package com.opencellsoft.pages.administration.billing.invoicingplans;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;


public class ListOfInvoicingPlansPage{
	
	WebDriver driver;
	WebDriverWait wait;

	public ListOfInvoicingPlansPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
	}

	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-textSizeSmall MuiButton-sizeSmall']")
	WebElement createInvoicePlanButton;
	
	public void clickOnCreateInvoicePlanButton() {
		// TODO Auto-generated method stub
		createInvoicePlanButton.click();
	}

}
