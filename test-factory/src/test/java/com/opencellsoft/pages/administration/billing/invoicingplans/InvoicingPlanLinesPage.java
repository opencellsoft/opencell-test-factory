package com.opencellsoft.pages.administration.billing.invoicingplans;

import java.time.Duration;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class InvoicingPlanLinesPage {

	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public InvoicingPlanLinesPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Create')]")
	WebElement createInvoicePlanLinesButton;
	@FindBy(how = How.ID, using = "code")
	WebElement invoicePlanLinesButtonCode;
	@FindBy(how = How.ID, using = "label")
	WebElement invoicePlanLinesButtonDescription;
	//@FindBy(how = How.ID, using = "advancement")
	@FindBy(how = How.XPATH, using = "//input[@id='advancement']")
	WebElement invoicingAdvancementRate;
	@FindBy(how = How.ID, using = "rateToBill")
	WebElement invoiceDownPayementRate;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Save')]")
	WebElement saveInvoicePlanLinesButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackInvoicePlanLinesButton;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;

	// get message from textBox
	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

	// This method is to click On create Invoicing Plan lines Button
	public void clickOnCreateInvoicePlanLinesButton() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(createInvoicePlanLinesButton));
			createInvoicePlanLinesButton.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(createInvoicePlanLinesButton));
			createInvoicePlanLinesButton.click();
		}	
	}

	// This method is to set code Invoicing Plan lines
	public void setCodeInvoicePlanLines(String strCodeInvoicingPlanLines) {
		invoicePlanLinesButtonCode.sendKeys(strCodeInvoicingPlanLines);
	}

	// This method is to set description Invoicing Plan lines
	public void setDescriptionInvoicePlanLines(String strDescritptionInvoicingPlanLines) {
		invoicePlanLinesButtonDescription.sendKeys(strDescritptionInvoicingPlanLines);
	}

	// This method is to set advancement rate of Invoicing Plan lines
	public void setInvoicingAdvancementRate(String strInvoicingAdvancementRate) {
		wait.until(ExpectedConditions.visibilityOf(invoicingAdvancementRate));
		invoicingAdvancementRate.clear();
		invoicingAdvancementRate.sendKeys(strInvoicingAdvancementRate);
	}

	// This method is to set Down Payment Rate of Invoicing Plan lines
	public void setInvoicingDownPayementRate(String strInvoicingDownPayementRate) {
		invoiceDownPayementRate.sendKeys(strInvoicingDownPayementRate);
	}

	// This method is to click save Invoicing Plan lines
	public void clickOnSaveInvoicingPlanLinesButton() {
		wait.until(ExpectedConditions.elementToBeClickable(saveInvoicePlanLinesButton));
		saveInvoicePlanLinesButton.click();
	}

	// This method is to click goBack Invoicing Plan lines
	public void clickOnGoBackInvoicingPlanLinesButton() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(goBackInvoicePlanLinesButton));
		Thread.sleep(2000);
		goBackInvoicePlanLinesButton.click();
	}

	// Verifying created Invoicing plan lines code
	public String getInvoicingPlanLinesCodeInputText() {
		return invoicePlanLinesButtonCode.getAttribute("value");
	}

	// verifying created Invoicing plan lines description
	public String getInvoicingPlanLinesDescriptionInputText() {
		return invoicePlanLinesButtonDescription.getAttribute("value");
	}
}
