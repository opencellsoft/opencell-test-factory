package com.opencellsoft.pages.administration.billing.invoicingplans;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class NewInvoicingPlanPage extends TestBase {
	WebDriver driver;
	WebDriverWait wait;

	public NewInvoicingPlanPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
	}

	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement invoicingName;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement invoicingDescription;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Save']")
	WebElement saveInvoicingPlanButton;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;

	// get message from textBox
	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

	public void setInvoicingName(String invoicingName) {
		// TODO Auto-generated method stub
		this.invoicingName.sendKeys(invoicingName);
	}

	public void setInvoicingDescription(String invoicingDescription) {
		// TODO Auto-generated method stub
		this.invoicingDescription.sendKeys(invoicingDescription);
	}

	public void clickSaveInvoicingPlanButton() {
		// TODO Auto-generated method stub
		saveInvoicingPlanButton.click();
	}

	public String getInvoicingPlanCodeInputText() {
		return invoicingName.getAttribute("value");
	}

	public String getInvoicingPlanDescriptionInputText() {
		// TODO Auto-generated method stub
		return invoicingDescription.getAttribute("value");
	}
}
