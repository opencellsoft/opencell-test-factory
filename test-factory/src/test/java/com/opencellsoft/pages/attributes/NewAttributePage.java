package com.opencellsoft.pages.attributes;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;


public class NewAttributePage {
	WebDriver driver;

	public NewAttributePage(WebDriver driver) {
		this.driver = driver;
	}

	// Using FindBy for locating elements
	@FindBy(how = How.ID, using = "attributeType")
	WebElement attributeList;
/*	@FindBy(how = How.XPATH, using = "//li[contains(text(),'Integer value')]")
	WebElement integerValueType;*/
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement attributeCode;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement attributeDescription;		
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add']")
	WebElement addAttributeValueButton;	
	@FindBy(how = How.ID, using = "allowedValues[0].value")
	WebElement firstAttributeValueInput;	
	@FindBy(how = How.ID, using = "allowedValues[1].value")
	WebElement secondAttributeValueInput;	
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Save']")
	WebElement submitButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;	

	// This method is to set select attribute type
	public void selectAttributeType(String attributeType) {		
		Wait<WebDriver> fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(5)).pollingEvery(Duration.ofSeconds(3)).ignoring(NoSuchElementException.class);
		fWait.until(ExpectedConditions.elementToBeClickable(attributeList));
		attributeList.click();
		attributeList.findElement(By.xpath("//li[normalize-space()='"+attributeType+"']")).click();
	}
	
	
	// This method is to set attribute Code in the code text box
	public void setAttributeCode(String attributeCode) {
		this.attributeCode.sendKeys(attributeCode);
	}
	
	// This method is to set attribute description in the description text box
	public void setAttributeDescription(String attributeDescription) {
		this.attributeDescription.sendKeys(attributeDescription);	
	}

	// This method is to click add button
	public void clickOnAddAttributeValuesButton() {
		Wait<WebDriver> fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(5)).pollingEvery(Duration.ofSeconds(5)).ignoring(NoSuchElementException.class);
		fWait.until(ExpectedConditions.visibilityOf(addAttributeValueButton));
		addAttributeValueButton.click();	
	}

	// This method is to set the first value
	public void setFirstAttributeValue(String firstValue) {			
		WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		wait.until(ExpectedConditions.visibilityOf(firstAttributeValueInput));

		firstAttributeValueInput.sendKeys(firstValue);
	}

	// This method is to set the second value
	public void setSecondAttributeValue(String secondValue) {
		WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		wait.until(ExpectedConditions.visibilityOf(secondAttributeValueInput));

		secondAttributeValueInput.sendKeys(secondValue);
		
	}

	// This method is to click on save button
	public void clickOnSaveAttributeButton() {
		WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		wait.until(ExpectedConditions.visibilityOf(submitButton));
		submitButton.submit();

	}
	
	// This method is to click on go back button
	public void clickOnGoBackButton() {
		WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		wait.until(ExpectedConditions.visibilityOf(goBackButton));
		goBackButton.click();
	}
}
