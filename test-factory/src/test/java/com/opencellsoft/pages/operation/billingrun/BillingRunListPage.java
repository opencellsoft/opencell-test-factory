package com.opencellsoft.pages.operation.billingrun;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class BillingRunListPage extends TestBase {

	public BillingRunListPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//*[contains(@class, 'MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-textSizeSmall MuiButton-sizeSmall') or @data-testid='AddIcon']")
	WebElement createNewBillingRunButton;
	@FindBy(how = How.XPATH, using = "//span[text() = 'Cycle run']")
	WebElement selectCycleRun;
	@FindBy(how = How.XPATH, using = "//span[text() = 'Exceptional run']")
	WebElement selectExceptionalRun;
	@FindBy(how = How.XPATH, using = "//input[@id = 'id']")
	WebElement billingCycleId;
	@FindBy(how = How.XPATH, using = "//div[@id = 'billingCycle']")
	WebElement billingCycleInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'status']")
	WebElement statusInput;
	@FindBy(how = How.XPATH, using = "//div[@id = 'runType']")
	WebElement runTypeInput;
	@FindBy(how = How.XPATH, using = "//div[@id = 'billingCycle->type']")
	WebElement splitLevelInput;
	@FindBy(how = How.XPATH, using = "(//table//tbody//span)[3]")
	WebElement searchResult;
	@FindBy(how = How.XPATH, using = "//table//tbody//*[1]//td[2]//span//span")
	WebElement billingCycle;
	@FindBy(how = How.XPATH, using = "//table//tbody//*[1]//td[3]//span//span")
	WebElement runType;
	@FindBy(how = How.XPATH, using = "//table//tbody//*[1]//td[4]//span")
	WebElement splitLevel;
	@FindBy(how = How.XPATH, using = "//table//tbody//*[1]//td[6]//span//span")
	WebElement processType;
	@FindBy(how = How.XPATH, using = "//table//tbody//*[1]//td[7]//span")
	WebElement status;
	@FindBy(how = How.XPATH, using = "//table//tbody//*[1]//td[9]//span//span")
	WebElement accounts;
	@FindBy(how = How.XPATH, using = "//table//tbody//*[1]//td[10]//span//span")
	WebElement invoices;
	@FindBy(how = How.XPATH, using = "//table//tbody//*[1]//td[11]//span//span")
	WebElement amountWithoutTax;
	@FindBy(how = How.XPATH, using = "//p[text() = 'Rows per page:']")
	WebElement pagination;
	@FindBy(how = How.XPATH, using = "//table[@id = 'operation/billing-run']//tbody//tr[@pathname = '/operation/billing-run/list']")
	List<WebElement> thereIs10Row;
	@FindBy(how = How.XPATH, using = "//table[@id = 'operation/billing-run']//tbody//tr[@pathname = '/operation/billing-run/list']")
	List<WebElement> thereIs20Row;
	@FindBy(how = How.XPATH, using = "//table[@id = 'operation/billing-run']//tbody//a[@pathname = '/operation/billing-run/list']")
	List<WebElement> thereIs25Row;
	@FindBy(how = How.XPATH, using = "//table[@id = 'operation/billing-run']//tbody//a[@pathname = '/operation/billing-run/list']")
	List<WebElement> thereIs50Row;
	@FindBy(how = How.XPATH, using = "//p[contains(text(), '1-20 of ')]")
	WebElement displayfrom1to20Row;
	@FindBy(how = How.XPATH, using = "//p[contains(text(), '1-25 of ')]")
	WebElement displayfrom1to25Row;
	@FindBy(how = How.XPATH, using = "//p[contains(text(), '1-10 of ')]")
	WebElement displayfrom1to10Row;
	@FindBy(how = How.XPATH, using = "//p[contains(text(), '21-40 of ')]")
	WebElement displayfrom21to40Row;
	@FindBy(how = How.XPATH, using = "//p[contains(text(), '1-50 of ')]")
	WebElement displayfrom1to50Row;
	@FindBy(how = How.XPATH, using = "//p[contains(text(), '26-50 of ')]")
	WebElement displayfrom26to50Row;
	
	@FindBy(how = How.XPATH, using = "//*[normalize-space() = 'Next']")
	WebElement nextButton;
	@FindBy(how = How.XPATH, using = "//*[normalize-space() = 'Prev']")
	WebElement previousButton;
	@FindBy(how = How.XPATH, using = "//div[contains(@class , 'MuiTablePagination-input')]")
	WebElement paginationList;
	@FindBy(how = How.XPATH, using = "//li[contains(@data-value , '50')]")
	WebElement choose50row;
	@FindBy(how = How.XPATH, using = "//li[contains(@data-value , '10')]")
	WebElement choose10row;
	@FindBy(how = How.XPATH, using = "//div[contains(@id, 'billingCycle->type')]")
	WebElement FilterBySplitLevelList;
	@FindBy(how = How.XPATH, using = "//li[contains(text(), 'Billing account')  or contains(text(), 'BILLING ACCOUNT')]")
	WebElement splitByBillingAccount;
	@FindBy(how = How.XPATH, using = "//li[contains(text(), 'Subscription')  or contains(text(), 'SUBSCRIPTION')]")
	WebElement splitBySubscription;
	@FindBy(how = How.XPATH, using = "//li[contains(text(), 'Order')  or contains(text(), 'ORDER')]")
	WebElement splitByOrder;
	@FindBy(how = How.XPATH, using = "//div[contains(@id, 'runType')]")
	WebElement FilterByType;
	@FindBy(how = How.XPATH, using = "//li[contains(text(), 'Cycle')  or contains(text(), 'CYCLE')]")
	WebElement filterByCycle;
	@FindBy(how = How.XPATH, using = "//li[contains(text(), 'Exceptional')  or contains(text(), 'EXCEPTIONAL')]")
	WebElement filterByExceptional;
	@FindBy(how = How.XPATH, using = "//ul[@aria-labelledby = 'runType-label']//li[1]")
	WebElement allRunTypes;
	
	public void clickOnCreateNewBillingRunButton() {
		clickOn(createNewBillingRunButton);
	}
	
	public void selectCycleRun() {
		clickOn(selectCycleRun);
	}
	
	public void selectExceptionalRun() {
		clickOn(selectExceptionalRun);
	}
	
	public void searchByBillingCycle(String billingCyclecode) {
		try {
			clickOn(billingCycleInput);
			clickOn(driver.findElement(By.xpath("//li[text() = '" + billingCyclecode + "']")));
		}catch (Exception e) {
			clickOn(billingCycleInput);
			clickOn(driver.findElement(By.xpath("//li[text() = '" + billingCyclecode + "']")));
		}
		
	}
	
	public void searchById(String billingCycleId) {
		try {
			this.billingCycleId.sendKeys(billingCycleId);
		}catch (Exception e) {
			 WebElement bcId = driver.findElement(By.xpath("//input[@id = 'id']"));
			 bcId.sendKeys(billingCycleId);
		}
	}
	
	public void searchByBillingStatus(String status) {
		clickOn(statusInput);
		try {
			clickOn(driver.findElement(By.xpath("//li//div//*[text() = '" + status + "']")));
		}catch (Exception e) {
			waitPageLoaded();
			clickOn(driver.findElement(By.xpath("//li//div//*[text() = '" + status + "']")));
		}
		
	}
	
	public void searchByBillingRunType(String RunType) {
		clickOn(runTypeInput);
		clickOn(driver.findElement(By.xpath("//li[text() = '" + RunType + "']")));
	}
	
	public void searchByBillingRunSplitlevel(String splitLevel) {
		clickOn(splitLevelInput);
		clickOn(driver.findElement(By.xpath("//li[@data-value = '" + splitLevel + "']")));
	}
	
	
	public void goToBillingRunDetails() {
		clickOn(searchResult);
	}
	
	public List<String> getBillingRunDetails() {
		List<String> BillingRunDetails = new ArrayList<>();
		BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[1]//*//*")).getText());
		BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[2]//*//*")).getText());
		BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[3]//*//*")).getText());
		
//		BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[1]//span//span")).getText());
//		BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[2]//span//span")).getText());
//		BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[3]//span//span")).getText());
		try {
			BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[4]//*//*")).getText());
//			BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[4]//span")).getText());
		}catch (Exception e) {
			BillingRunDetails.add("");
		}
		BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[5]//*")).getText());
		BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[6]//*//*")).getText());
		BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[7]//*")).getText());
		BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[8]//*")).getText());
		
//		BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[5]//span")).getText());
//		BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[6]//span//span")).getText());
//		BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[7]//span")).getText());
//		BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[8]//span")).getText());
		try {
			BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[9]//*//*")).getText());
		}catch (Exception e) {
			BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[9]//*")).getText());
		}
		
		BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[10]//*//*")).getText());
		BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[11]//*//*")).getText());
		BillingRunDetails.add(driver.findElement(By.xpath("//table//tbody//*[1]//td[12]//*//*")).getText());
		return BillingRunDetails;
	}
	
	public boolean paginationIsDisplayed() {
		return pagination.isDisplayed();
	}
	public boolean displayfrom1to10RowInThePage() {
		return displayfrom1to10Row.isDisplayed();
	}
	public boolean displayfrom1to20RowInThePage() {
		return displayfrom1to20Row.isDisplayed();
	}
	public boolean displayfrom1to25RowInThePage() {
		return displayfrom1to25Row.isDisplayed();
	}
	public boolean displayfrom1to50RowInThePage() {
		return displayfrom1to50Row.isDisplayed();
	}
	public boolean displayfrom21to40RowInThePage() {
		return displayfrom21to40Row.isDisplayed();
	}
	public boolean displayfrom26to50RowInThePage() {
		return displayfrom26to50Row.isDisplayed();
	}
	
	public int thereIs10RowDisplayed() {
		return thereIs10Row.size();
	}
	public int thereIs20RowDisplayed() {
		return thereIs20Row.size();
	}
	public int thereIs25RowDisplayed() {
		return thereIs25Row.size();
	}
	public int thereIs50RowDisplayed() {
		return thereIs50Row.size();
	}
	public void clickOnNextbutton(){
		clickOn(nextButton);
	}

	public void clickOnPreviousbutton(){
		clickOn(previousButton);
	}
	
	public void clickOnPaginationList(){
		clickOn(paginationList);
	}
	
	public void select50row(){
		clickOn(choose50row);
	}
	
	public void select10row(){
		clickOn(choose10row);
	}
	
	public void clickOnFilterBySplitLevel() {
		clickOn(FilterBySplitLevelList);
	}
	
	public void selectSplitByBillingAccount() {
		clickOn(splitByBillingAccount);
	}
	
	public void selectSplitBySubscription() {
		clickOn(splitBySubscription);
	}
	
	public void selectSplitByOrder() {
		clickOn(splitByOrder);
	}
	
	public void clickOnFilterByType() {
		clickOn(FilterByType);
	}
	public void selectFilterByCycle() {
		clickOn(filterByCycle);
	}
	public void selectFilterByExceptional() {
		clickOn(filterByExceptional);
	}
	public void selectAllRunTypes() {
		clickOn(allRunTypes);
	}
}
