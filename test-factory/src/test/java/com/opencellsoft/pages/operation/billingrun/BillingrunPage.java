package com.opencellsoft.pages.operation.billingrun;

import java.time.Duration;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BillingrunPage {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public BillingrunPage(WebDriver driver) {

		this.driver= driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		this.fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(60))
				.pollingEvery(Duration.ofSeconds(10)).ignoring(NoSuchElementException.class);
	}
	
	@FindBy(how= How.XPATH, using= "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-textSizeSmall MuiButton-sizeSmall']")
	WebElement addBillingRunButton;
	@FindBy(how= How.XPATH, using= "//span[normalize-space()='Cycle run']")
	WebElement cycleRunButton;
	@FindBy(how= How.XPATH, using= "//span[normalize-space()='Exceptional run']")
	WebElement exeptionalRunButton;

	public void clickOnAddBillingrunButton() {
		wait.until(ExpectedConditions.elementToBeClickable(addBillingRunButton));
		addBillingRunButton.click();
	}
	
	public void clickOnCyclerunButton() {
		cycleRunButton.click();
	}

	public void clickOnExeptionalrunButton() {
		wait.until(ExpectedConditions.elementToBeClickable(exeptionalRunButton));
		exeptionalRunButton.click();
	}
}
