package com.opencellsoft.pages.operation.billingrun;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class BillingRunDetailsPage extends TestBase {

	public BillingRunDetailsPage() {
		PageFactory.initElements(driver, this);
	}
	Actions actions;
	@FindBy(how = How.XPATH, using = "//input[@id='billingCycleCode']")
	WebElement billingCycleCodeInput;
	@FindBy(how = How.XPATH, using = "//input[contains(@id, 'description')]")
	WebElement searchByDescriptionInput;
	@FindBy(how = How.XPATH, using = "//table//tr[1]//td[1]")
	WebElement firstChoice;
	@FindBy(how = How.XPATH, using = "//div[@id='billingRunTypeEnum']")
	WebElement processTypeInput;
	@FindBy(how = How.XPATH, using = "//input[@id='skipValidationScript']")
	WebElement SkipInvoiceValidation;
	@FindBy(how = How.XPATH, using = "//input[@id='computeDatesAtValidation']")
	WebElement computeDatesAtValidation;
	@FindBy(how = How.XPATH, using = "//input[@id = 'incrementalInvoiceLines']")
	WebElement incrementalInvoiceLines;
	@FindBy(how = How.XPATH, using = "//input[@id = 'preReportAutoOnCreate']")
	WebElement preReportAutoOnCreate;
	@FindBy(how = How.XPATH, using = "//input[@id = 'preReportAutoOnInvoiceLinesJob']")
	WebElement preReportAutoOnInvoiceLinesJob;
	@FindBy(how = How.XPATH, using = "//div[@id='rejectAutoAction']")
	WebElement actionOnRejectInput;
	@FindBy(how = How.XPATH, using = "//div[@id='suspectAutoAction']")
	WebElement actionOnSuspectInput;
	@FindBy(how = How.XPATH, using = "//div[@id='invoiceType']")
	WebElement invoiceTypeInput;
	@FindBy(how = How.XPATH, using = "//div[@data-rbd-draggable-id = 'billingAccount']")
	WebElement billingAccountDraggable;
	@FindBy(how = How.XPATH, using = "//div[@data-rbd-draggable-id = 'billingAccount.code']")
	WebElement billingAccountCodeDraggable;
	@FindBy(how = How.XPATH, using = "//div[@data-rbd-draggable-id = 'usageDate']")
	WebElement usageDateDraggable;
//	@FindBy(how = How.XPATH, using = "//p[@title = 'Operator']")
	@FindBy(how = How.XPATH, using = "//table[@class = 'data-grid']")
	WebElement filterFields;
	@FindBy(how = How.XPATH, using = "//input[@name='applicationEl']")
	WebElement applicationElInput;
	@FindBy(how = How.XPATH, using = "//*[text()='Create']")
	WebElement createButton;
	@FindBy(how = How.XPATH, using = "//*[text()='Cancel']")
	WebElement cancelButton;
	@FindBy(how = How.XPATH, using = "//label[@for = 'selectedAction_CANCEL']")
	WebElement cancelRadioButton;
	@FindBy(how = How.XPATH, using = "//div[contains(@class,'ButtonGroupGutters')]//*[text() = 'Process']")
	WebElement processButton;
	@FindBy(how = How.XPATH, using = "//*[text()='Yes']")
	WebElement yesButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Import new rated items and keep invoice lines open']")
	WebElement importNewRatedItemsAndKeepInvoiceLinesOpenButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Close invoice lines and generate invoices']")
	WebElement closeInvoiceLinesAndGenerateInvoicesButton;
	@FindBy(how = How.XPATH, using = "//button//span[text() = 'Actions']")
	WebElement actionsButton;
	@FindBy(how = How.XPATH, using = "//button//span[text() = 'Disable']")
	WebElement disableBillingRunButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Confirm']")
	WebElement confirmButton;
	@FindBy(how = How.XPATH, using = "//button//span[text() = 'Enable']")
	WebElement enableBillingRunButton;
	@FindBy(how = How.XPATH, using = "//span[text() = 'Update filter']")
	WebElement updateFilterButton;
	@FindBy(how = How.XPATH, using = "//button[@title = 'Refresh']")
	WebElement refreshButton;
	@FindBy(how = How.XPATH, using = "//p[text() = 'Statistics']/following-sibling::span")
	WebElement billingRunStatus;
	@FindBy(how = How.XPATH, using = "//table[@listid = 'operation/invoices']//tbody//td[2]//span//span")
	WebElement invoiceNumber;
	
	@FindBy(how = How.XPATH, using = "//table[@listid = 'operation/invoices']//tbody//td[4]//span//span")
	WebElement invoiceType;
	@FindBy(how = How.XPATH, using = "//table[@listid = 'operation/invoices']//tbody//td[7]//span")
	WebElement invoiceStatus ;
	@FindBy(how = How.XPATH, using = "//table[@listid = 'operation/invoices']//tbody//td[8]//span//span")
	WebElement invoiceAmountWoT;
	@FindBy(how = How.XPATH, using = "(//form[@class = 'EditBillingRun']//button[contains(@class, 'MuiIconButton-colorPrimary')])[3]")
	WebElement statisticsButton;
	@FindBy(how = How.XPATH, using = "//*[contains(text(), 'Generate report') or contains(text(), 'GENERATE REPORT')]")
	WebElement generateRepportButton;
	@FindBy(how = How.XPATH, using = "//*[@title= 'Regenerate report' or @aria-label='Regenerate report']")
	WebElement regenerateRepportButton;
	@FindBy(how = How.XPATH, using = "//input[@id = 'lastTransactionDate']")
	WebElement lastTransactionInput;
	@FindBy(how = How.XPATH, using = "(//button[@class = 'MuiButtonBase-root MuiIconButton-root MuiPickersCalendarHeader-iconButton' or contains(@aria-label , 'month')])[2]")
	WebElement nextMonth;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog' or contains(@class,'MuiPopover-paper')]//*[text() = '1']")
	WebElement firstOfTheMonth;
	@FindBy(how = How.XPATH, using = "//table//span[text() = 'Status']")
	WebElement sortByStatus;
	@FindBy(how = How.XPATH, using = "//table//span[text() = 'Customer']")
	WebElement sortByCustomer;
	@FindBy(how = How.XPATH, using = "//input[@id='invoiceNumber']")
	WebElement invoiceNumberInput;
	@FindBy(how = How.XPATH, using = "//*[contains(@id,'BACode') or contains(@name,'BACode')]")
	WebElement invoiceCustomerInput;
	@FindBy(how = How.XPATH, using = "//input[@id='status']")
	WebElement invoiceStatusInput;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement invoiceCustomerDescriptionInput;
	@FindBy(how = How.XPATH, using = "//input[@id='seller']")
	WebElement invoiceSellerInput;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement invoiceSellerCodeInput;
	@FindBy(how = How.XPATH, using = "//button[@aria-label = 'Add filter' or @aria-label = 'Filter' ]")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//li[@data-key='amountWithoutTaxBet']")
	WebElement amountWOTaxIsBetween;
	@FindBy(how = How.XPATH, using = "//input[@id='amountWithoutTaxBet-min']")
	WebElement amountWOTaxMin;
	@FindBy(how = How.XPATH, using = "//input[@id='amountWithoutTaxBet-max']")
	WebElement amountWOTaxMax;
	@FindBy(how = How.XPATH, using = "//button//span[text()= 'Validate invoices']")
	WebElement validateInvoicesButton;
	@FindBy(how = How.XPATH, using = "//div[@id = 'VALIDATE_INVOICES_FILTRED']")
	WebElement filteredOption;
	@FindBy(how = How.XPATH, using = "//div[@id = 'VALIDATE_INVOICES_SELECTED']")
	WebElement selectedOption;
	@FindBy(how = How.XPATH, using = "//span[text()= 'REJECTED']/following-sibling::*[local-name()='svg']")
	WebElement rejectedOption;
	@FindBy(how = How.XPATH, using = "//span[text()= 'SUSPECT']/following-sibling::*[local-name()='svg']")
	WebElement suspectOption;
	public void selectBillingCycle(String billingCycle) {
		clickOn(billingCycleCodeInput);
		searchByDescriptionInput.sendKeys(billingCycle);
		waitPageLoaded();
		try {
			clickOn(driver.findElement(By.xpath("//div[@role = 'dialog']//*[text() = '" + billingCycle + "']")));
		}catch (Exception e) {
			searchByDescriptionInput.sendKeys(billingCycle);
			waitPageLoaded();
			clickOn(firstChoice);
		}
		
	}
	
	public void selectProcessType(String processType) {
		clickOn(processTypeInput);
		clickOn(driver.findElement(By.xpath("//li[@role = 'option' and @data-value='" + processType + "']")));
	}
	
	public void setLastTransactionDate() {
		clickOn(lastTransactionInput);
		clickOn(nextMonth);
		clickOn(firstOfTheMonth);
	}
	
	public void setIncrementalInvoiceLines(Boolean value) {
		WebElement element = driver.findElement(By.xpath("(//span[contains(@class, 'MuiSwitch-switchBase MuiSwitch-colorPrimary')])[3]"));
		String class_name = element.getAttribute("class");
		
		if((value.equals(true) && !class_name.contains("Mui-checked")) || (value.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(incrementalInvoiceLines);
		}
	}
	
	public void setSkipInvoiceValidation(Boolean value) {
		WebElement element = driver.findElement(By.xpath("(//span[contains(@class, 'MuiSwitch-switchBase MuiSwitch-colorPrimary')])[1]"));
		String class_name = element.getAttribute("class");
		
		if((value.equals(true) && !class_name.contains("Mui-checked")) || (value.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(SkipInvoiceValidation);
		}
	}	

	public void setRecomputeDatesAtValidation(Boolean value) {
		WebElement element = driver.findElement(By.xpath("(//span[contains(@class, 'MuiSwitch-switchBase MuiSwitch-colorPrimary')])[2]"));
		String class_name = element.getAttribute("class");
		
		if((value.equals(true) && !class_name.contains("Mui-checked")) || (value.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(computeDatesAtValidation);
		}
	}
	
	public void selectActionOnReject(String actionOnReject) {
		clickOn(actionOnRejectInput);
		clickOn(driver.findElement(By.xpath("//li[@role = 'option' and normalize-space()='" + actionOnReject + "']")) );
	}

	public void selectActionOnSuspect(String actionOnSuspect) {
		clickOn(actionOnSuspectInput);
		clickOn(driver.findElement(By.xpath("//li[@role = 'option' and normalize-space()='" + actionOnSuspect + "']")) );
	}

	public void selectInvoiceType(String invoiceType) {
		clickOn(invoiceTypeInput);
		clickOn(driver.findElement(By.xpath("//li[@role = 'option' and normalize-space()='" + invoiceType + "']")) );
	}
	
	public void setPIROnBillingRunCreation(Boolean value) {
		WebElement element = driver.findElement(By.xpath("(//span[contains(@class, 'MuiSwitch-switchBase MuiSwitch-colorPrimary')])[5]"));
		String class_name = element.getAttribute("class");
		
		if((value.equals(true) && !class_name.contains("Mui-checked")) || (value.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(preReportAutoOnCreate);
		}
	}
	
	public void setPIRWhenLaunchingInvoiceLinesJob(Boolean value) {
		WebElement element = driver.findElement(By.xpath("(//span[contains(@class, 'MuiSwitch-switchBase MuiSwitch-colorPrimary')])[6]"));
		String class_name = element.getAttribute("class");
		
		if((value.equals(true) && !class_name.contains("Mui-checked")) || (value.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(preReportAutoOnInvoiceLinesJob);
		}
	}
	
	public void setApplicationEl(String applicationEl) {
		applicationElInput.sendKeys(applicationEl);
	}
	public void setInvoiceDate(String invoiceDate) {
	}
	
	public void clickOnBillingAccountDraggable() {
		clickOn(billingAccountDraggable);
	}
	
	public void dragAndDropBillingAccountCode() throws InterruptedException {
		
		actions = new Actions(driver);
		actions.moveToElement(updateFilterButton).perform();

///////////////////////////////////////////////////////////////////////////////////////
//		actions.moveToElement(billingAccountCodeDraggable)
//		.pause(Duration.ofSeconds(1))
//        .clickAndHold()
//        .pause(Duration.ofSeconds(1))
//		.moveToElement(filterFields)
//        .release(filterFields)
//        .build()
//        .perform();
///////////////////////////////////////////////////////////////////////////////////////
//		actions.dragAndDrop(billingAccountCodeDraggable, filterFields).build().perform();
	}
	
	public void dragAndDropUsageDate() {
		actions = new Actions(driver);
        actions.moveToElement(usageDateDraggable).build().perform();
		actions.dragAndDrop(usageDateDraggable, filterFields).build().perform();
	}
	
	public void setOperator(String operator, String column) {
		actions = new Actions(driver);
		
		WebElement operatorCulomn =  driver.findElement(By.xpath("(//table//tr[" + column + "]/td)[2]"));
		actions.doubleClick(operatorCulomn).build().perform();

		WebElement selectOperator =  driver.findElement(By.xpath("//*[text() = '" + operator + "']"));
		clickOn(selectOperator);
	}
	
	public void setValue(String value, String column) {
		actions = new Actions(driver);
		
		WebElement valueCulomn =  driver.findElement(By.xpath("(//table//tr[" + column + "]/td)[3]"));
		actions.doubleClick(valueCulomn).build().perform();
		
		WebElement valueInput =  driver.findElement(By.xpath("//input[@class = 'data-editor']"));
		valueInput.sendKeys(value);
		valueInput.sendKeys(Keys.ENTER);
	}
	
	
	public void clickOnCreateButton() {
		clickOn(createButton);
	}

	public void clickOnCancelButton() {
		clickOn(cancelButton);
	}
	
	public void CheckCancelRTs() {
		clickOn(cancelRadioButton);
	}
	
	public void clickOnProcessButton() {
		clickOn(processButton);
	}
	public void clickOnYesButton() {
		clickOn(yesButton);
	}
	
	public void clickOnImportNewRatedItemsAndKeepInvoiceLinesOpen() {
		clickOn(importNewRatedItemsAndKeepInvoiceLinesOpenButton);
	}
	
	public void clickOnCloseInvoiceLinesAndGenerateInvoices() {
		clickOn(closeInvoiceLinesAndGenerateInvoicesButton);
	}
	public void clickOnRefreshButton() {
		try {
			clickOn(refreshButton);
		}catch (Exception e) {
			driver.navigate().refresh();
			waitPageLoaded();
		}
		
	}
	public void clickOnActionsButton() {
		clickOn(actionsButton);
	}
	public void clickOnDisableBillingRunButton() {
		clickOn(disableBillingRunButton);
	}		
	public void clickOnConfirmButton() {
		try {
			clickOn(confirmButton);
		}catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public void clickOnEnableBillingRunButton() {
		clickOn(enableBillingRunButton);
	}	
	
	public void clickOnUpdateFilter() {
		clickOn(updateFilterButton);
	}
	
	public String billingRunStatus() {
		return billingRunStatus.getText();
	}
	
	public String invoiceType() {
		return invoiceType.getText();
	}
	
	public String invoiceStatus() {
		return invoiceStatus.getText();
	}
	
	public String invoiceAmountWoT() {
		return invoiceAmountWoT.getText();
	}

	public String invoiceNumber() {
		return invoiceNumber.getText();
	}
	
	public String invoiceDetail(String index) {
		return driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//td[" + index + "]")).getText();
	}
	
	public void clickOnInvoiceNumber() {
		clickOn(invoiceNumber);
	}
	
	public void clickOnCustomerLink(String index) {
		try {
			clickOn(driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//td[" + index + "]//a")));
		}
		catch (Exception e) {
			clickOn(driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//td[" + index + "]//a")));
		}
	}
	
	public void clickOnlink(String index) {
		clickOn(driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//td[" + index + "]//a")));
	}
	
	public void clickOnStatisticsButton() {
		clickOn(statisticsButton);
	}
	
	public void clickOnGenerateRepportButton() {
		clickOn(generateRepportButton);
	}	
	
	public void clickOnRegenerateRepportButton() {
		clickOn(regenerateRepportButton);
		waitPageLoaded();
	}
	
	
	public List<String> getInvoiceGeneratedDetails(int i, String version) {
		List<String> invoiceGeneratedDetails = new ArrayList<>();
		WebElement number, customer, invoiceType, invoiceDate, dueDate, status, AmountWithoutTax, AmountWithTax, rejectionReason;
		// id in V16 & V17 , number in V14.1.X, 15.X
		try { 
			number = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[2]//*//span"));
			invoiceGeneratedDetails.add(number.getText());
			}
		catch (Exception ex) { 
			number = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[2]//span//span"));
			invoiceGeneratedDetails.add(number.getText());
			}
		///////////////////////////////////////////////////::
		try {
			customer = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[3]//*//span"));
			invoiceGeneratedDetails.add(customer.getText());
			}
		catch (Exception ex) {
			customer = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[3]//span//span"));
			invoiceGeneratedDetails.add(customer.getText());
		}
		//////////////////////////////////////////////////////////
		try {
			invoiceType = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[4]//*//span"));
			invoiceGeneratedDetails.add(invoiceType.getText());
			}
		catch (Exception ex) {
			invoiceType = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[4]//span//span"));
			invoiceGeneratedDetails.add(invoiceType.getText());}
		////////////////////////////////////////////////////////////
		try {
				try{ 
					invoiceDate = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[5]//span"));
					invoiceGeneratedDetails.add(invoiceDate.getText());
				}
				catch (Exception e) { 
					invoiceDate = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[5]//*//span"));
					invoiceGeneratedDetails.add(invoiceDate.getText());	
				}
			}
		catch (Exception ex) {
			try{ 
				invoiceDate = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[5]//span"));
				invoiceGeneratedDetails.add(invoiceDate.getText());	
			}
			catch (Exception e) { 
				invoiceDate = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[5]//*//span"));
				invoiceGeneratedDetails.add(invoiceDate.getText());	
			}
		}
		////////////////////////////////////////////////////////////
		try {
			dueDate = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[6]//span"));
			invoiceGeneratedDetails.add(dueDate.getText());
			}
		catch (Exception ex) {
			dueDate = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[6]//span"));
			invoiceGeneratedDetails.add(dueDate.getText());
		}
		////////////////////////////////////////////////////////////
		try {
			status = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[7]//span"));
			invoiceGeneratedDetails.add(status.getText());
			}
		catch (Exception ex) {
			status = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[7]//span"));
			invoiceGeneratedDetails.add(status.getText());
			}
////////////////////////////////////////////////////////////
		try {
			try { 
				AmountWithoutTax = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[8]//*//span"));
				invoiceGeneratedDetails.add(AmountWithoutTax.getText());
				}
			catch (Exception e) { 
				AmountWithoutTax = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[8]//span"));
				invoiceGeneratedDetails.add(AmountWithoutTax.getText());
				}
			}
		catch (Exception ex) {
			try { 
				AmountWithoutTax = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[8]//span//span"));
				invoiceGeneratedDetails.add(AmountWithoutTax.getText());
				}
			catch (Exception e) { 
				AmountWithoutTax = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[8]//span"));
				invoiceGeneratedDetails.add(AmountWithoutTax.getText());
				}
			}
////////////////////////////////////////////////////////////
		try {
			try { 
				AmountWithTax = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[9]//*//span"));
				invoiceGeneratedDetails.add(AmountWithTax.getText());
				}
			catch (Exception e) { 
				AmountWithTax = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[9]//span"));
				invoiceGeneratedDetails.add(AmountWithTax.getText());
				}
			}
		catch (Exception ex) {
			try { 
				AmountWithTax = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[9]//*//span"));
				invoiceGeneratedDetails.add(AmountWithTax.getText());
				}
			catch (Exception e) { 
				AmountWithTax = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[9]//span"));
				invoiceGeneratedDetails.add(AmountWithTax.getText());
				}
			}
////////////////////////////////////////////////////////////		
		try {
			try { 
				rejectionReason = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[10]//*//span"));
				invoiceGeneratedDetails.add(rejectionReason.getText());
				}
			catch (Exception e) { 
				rejectionReason = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[10]//*"));
				invoiceGeneratedDetails.add(rejectionReason.getText());
				}
			}
		catch (Exception ex) {
			try { 
				rejectionReason = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[10]//*//span"));
				invoiceGeneratedDetails.add(rejectionReason.getText());
				}
			catch (Exception e) { 
				rejectionReason = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[10]//span"));
				invoiceGeneratedDetails.add(rejectionReason.getText());
				}
		}
		
		if(!version.equals("14.1.X")) {
			try {
				try { 
					AmountWithTax = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[12]//span//span"));
					invoiceGeneratedDetails.add(AmountWithTax.getText());
					}
				catch (Exception e) { 
					AmountWithTax = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[12]//span"));
					invoiceGeneratedDetails.add(AmountWithTax.getText());
					}
				}
			catch (Exception ex) {
				try { 
					AmountWithTax = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[12]//span//span"));
					invoiceGeneratedDetails.add(AmountWithTax.getText());
					}
				catch (Exception e) { 
					AmountWithTax = driver.findElement(By.xpath("//table[@listid = 'operation/invoices']//tbody//*[" + i + "]//td[12]//span"));
					invoiceGeneratedDetails.add(AmountWithTax.getText());
					}
				}
		}
		return invoiceGeneratedDetails;
	}
	
	public void clickOnSortByStatus() {
		clickOn(sortByStatus);
	}
	
	public void clickOnSortByCustomer() {
		clickOn(sortByCustomer);
	}
	
	public void searchByInvoiceNumber(String number) {
		try {
			this.invoiceNumberInput.sendKeys(Keys.chord(Keys.CONTROL, "a"),number);
		}catch (Exception e) {
			 WebElement invoiceNumber = driver.findElement(By.xpath("//input[@id='invoiceNumber']"));
			 invoiceNumber.sendKeys(Keys.chord(Keys.CONTROL, "a"),number);
		}
	}
	
	public void searchByInvoiceCustomer141X(String customer) {
		waitPageLoaded();
		clickOn(invoiceCustomerInput);
		try {
			clickOn(driver.findElement(By.xpath("//li[text() = '" + customer + "']")));
		}catch (Exception e) {
			clickOn(invoiceCustomerInput);
			clickOn(driver.findElement(By.xpath("//li[text() = '" + customer + "']")));
		}
		
	}
	
	public void searchByInvoiceCustomer(String customer) {
		clickOn(invoiceCustomerInput);
		invoiceCustomerDescriptionInput.sendKeys(customer);
		waitPageLoaded();
		try {
			clickOn(driver.findElement(By.xpath("//div[@role='dialog']//table//tr//td[2]")));
		}catch (Exception e) {
			clickOn(driver.findElement(By.xpath("//div[@role='dialog']//table//tr//td[2]")));
		}
		
	}
	public void searchByInvoiceSeller(String seller) {
		clickOn(invoiceSellerInput);
		invoiceSellerCodeInput.sendKeys(seller);
		waitPageLoaded();
		clickOn(driver.findElement(By.xpath("//table//tr//td[2]")));
	}
	public void searchByInvoiceStatus(String status) {
		clickOn(invoiceStatusInput);
		clickOn(driver.findElement(By.xpath("//li//div//strong[text() = '" + status + "']")));
	}
	public void addFilterByAmountWOTax(){
		clickOn(filterButton);
		clickOn(amountWOTaxIsBetween);
	}
	public void searchByInvoiceAmountMin(String amountMin) {
		amountWOTaxMin.sendKeys(amountMin);
		waitPageLoaded();
	}
	public void searchByInvoiceAmountMax(String amountMax) {
		amountWOTaxMax.sendKeys(amountMax);
		waitPageLoaded();
	}
	
	public void clickonValidateInvoicesButton(){
		clickOn(validateInvoicesButton);
	}
	
	public void clickOnFilteredOption(){
		clickOn(filteredOption);
	}
	
	public void clickOnSelectedOption(){
		clickOn(selectedOption);
	}
	
	public void clearStatus() {
		clickOn(rejectedOption);
		clickOn(suspectOption);
	}
}
