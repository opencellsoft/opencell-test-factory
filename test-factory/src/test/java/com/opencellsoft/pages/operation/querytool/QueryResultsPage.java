package com.opencellsoft.pages.operation.querytool;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class QueryResultsPage extends TestBase {

	public QueryResultsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//input[@title = 'Name' or @id = 'searchBar']")
	WebElement searchInput;
	@FindBy(how = How.XPATH, using = "//table[contains(@id, 'query-runs-results')]//tbody//*[@role = 'row' or contains(@class ,'MuiTableRow-root')]")
	List<WebElement> queriesList;
	@FindBy(how = How.XPATH, using = "(//table//tbody//input[@type = 'checkbox'])[1]")
	WebElement firstLine;
	@FindBy(how = How.XPATH, using = "//table//thead//input[@type = 'checkbox']")
	WebElement allList;
	@FindBy(how = How.XPATH, using = "//button[@title = 'Delete' or @aria-label = 'Delete']")
	WebElement deleteButton;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//*[text() = 'Confirm']")
	WebElement confirmButton;
	@FindBy(how = How.XPATH, using = "//div[@role = 'alert']")
	WebElement message;
	public void setSearchInput(String value) {
		searchInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		searchInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public void sortBy(String value) {
		try {
			clickOn(driver.findElement(By.xpath("//span[contains(@class, 'MuiButtonBase-root') and contains(@data-field, '" + value + "')]")));
		}catch (Exception e) {
			clickOn(driver.findElement(By.xpath("//th[@sortby= '" + value + "']")));
		}
	}
	
	public List<WebElement> queriesList() {
		return this.queriesList;
	}
	
	public void clickOnTheFirstLine() {
		clickOn(firstLine);
	}
	
	public void selectAllList() {
		clickOn(allList);
	}
	
	public void clickOnDeleteButton() {
		clickOn(deleteButton);
	}
	
	public void clickOnConfirmButton() {
		clickOn(confirmButton);
	}
	public void clickOnDownload(int i) {
		WebElement elem = null;
		try {
			elem = driver.findElement(By.xpath("(//table[contains(@id, 'query-runs-results')]//td//button)[" + i + "]"));
		}catch (Exception e) {
			elem = driver.findElement(By.xpath("(//table[contains(@id, 'query-runs-results')]//td//button)[" + i + "]"));
		}
		
		clickOn(elem);
	}
	
	public String messageText() {
		return message.getText();
	}

}
