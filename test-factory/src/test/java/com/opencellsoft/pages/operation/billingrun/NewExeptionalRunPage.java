package com.opencellsoft.pages.operation.billingrun;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewExeptionalRunPage {

	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public NewExeptionalRunPage(WebDriver driver) {

		this.driver= driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		this.fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(60))
				.pollingEvery(Duration.ofSeconds(10)).ignoring(NoSuchElementException.class);
	}
	
	@FindBy(how= How.XPATH, using= "//div[@id='billingRunTypeEnum']")
	WebElement processTypeInput;
	@FindBy(how= How.XPATH, using= "//li[normalize-space()='Automatic']")
	WebElement processTypeOption;
	@FindBy(how= How.XPATH, using= "//input[@id='invoiceDate']")
	WebElement invoiceDateInput;
	@FindBy(how= How.XPATH, using= "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day MuiPickersDay-current MuiPickersDay-daySelected']")
	WebElement invoiceDateButton;
	@FindBy(how= How.XPATH, using= "//div[@id='rejectAutoAction']")
	WebElement actionOnrejectInput;
	@FindBy(how= How.XPATH, using= "//li[normalize-space()='Move']")
	WebElement actionOnrejectOption;
	@FindBy(how= How.XPATH, using= "//div[@id='suspectAutoAction']")
	WebElement actionOnSuspectInput ;
	@FindBy(how= How.XPATH, using= "//li[normalize-space()='Move']")
	WebElement actionOnSuspectOption;
	@FindBy(how= How.XPATH, using= "//button[@type='submit']")
	WebElement createButton;
	@FindBy(how= How.XPATH, using= "//div[@class='MuiSnackbarContent-message']")
	WebElement messageBox;
	@FindBy(how= How.XPATH, using= "//span[contains(text(),'Cancel')]")
	WebElement cancelButton;
	
	public void setProcessTypeInput() {
		processTypeInput.click();		
	}

	public void selectProcessTypeOption() {
		processTypeOption.click();
	}

	public void setInvoiceDateInput() {
		invoiceDateInput.click();
	}
	
	public void clickOnInvoiceDateButton() {
		wait.until(ExpectedConditions.elementToBeClickable(invoiceDateButton));
		invoiceDateButton.click();
	}
	
	public void setActionOnrejectInput() {
		actionOnrejectInput.click();
	}
	
	public void selectActionOnrejectOption() {
		actionOnrejectOption.click();
	}
	
	public void setActionOnSuspectInput() {
		actionOnSuspectInput.click();
	}
	
	public void selectActionOnSuspectOption() {
		actionOnSuspectOption.click();
	}	
	
	public void setFilterFields() {
			
	}
		  
	public void clickOnCreateButton() {
		wait.until(ExpectedConditions.elementToBeClickable(createButton));
		createButton.click();
	}
	
	public String getMessageBox() {
		wait.until(ExpectedConditions.visibilityOf(messageBox));
		return messageBox.getText();
	}
	
	public void clickOnCancelButton() {
		wait.until(ExpectedConditions.elementToBeClickable(cancelButton));
		cancelButton.click();
	}
}
