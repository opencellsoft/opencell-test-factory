package com.opencellsoft.pages.operation.billingrun;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewCycleRunPage {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public NewCycleRunPage(WebDriver driver) {

		this.driver= driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		this.fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(60))
				.pollingEvery(Duration.ofSeconds(10)).ignoring(NoSuchElementException.class);
	}
	
	@FindBy(how= How.XPATH, using= "//input[@id='billingCycleCode']")
	WebElement billingCycleInput;
	@FindBy(how= How.XPATH, using= "//button[@aria-label='Add filter']")
	WebElement filterButton;
	@FindBy(how= How.XPATH, using= "//li[@role='menuitem']//span[contains(text(),'Code')]")
	WebElement codeButton;
	@FindBy(how= How.XPATH, using= "//input[@id='wildcardOrIgnoreCase code']")
	WebElement codeInput;
	@FindBy(how= How.XPATH, using= "//input[@id='wildcardOrIgnoreCase description']")
	WebElement descriptionInput;
	@FindBy(how= How.XPATH, using= "//div[@id='billingRunTypeEnum']")
	WebElement processTypeInput;
	@FindBy(how= How.XPATH, using= "//li[normalize-space()='Automatic']")
	WebElement processTypeOption;
	@FindBy(how= How.XPATH, using= "//input[@id='lastTransactionDate']")
	WebElement lastTransDateInput;
	@FindBy(how= How.XPATH, using= "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day MuiPickersDay-daySelected']")
	WebElement lastTransDateButton;
	@FindBy(how= How.XPATH, using= "//input[@id='invoiceDate']")
	WebElement invoiceDateInput;
	@FindBy(how= How.XPATH, using= "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day MuiPickersDay-current MuiPickersDay-daySelected']")
	WebElement invoiceDateButton;
	@FindBy(how= How.XPATH, using= "//div[@id='referenceDate']")
	WebElement referenceDateInput;
	@FindBy(how= How.XPATH, using= "//li[normalize-space()='Today']")
	WebElement referenceDateOption;
	@FindBy(how= How.XPATH, using= "//div[@id='rejectAutoAction']")
	WebElement actionOnrejectInput;
	@FindBy(how= How.XPATH, using= "//li[normalize-space()='Quarantine']")
	WebElement actionOnrejectOption;
	@FindBy(how= How.XPATH, using= "//div[@id='suspectAutoAction']")
	WebElement actionOnSuspectInput ;
	@FindBy(how= How.XPATH, using= "//li[normalize-space()='Cancel']")
	WebElement actionOnSuspectOption;
	@FindBy(how= How.XPATH, using= "//span[normalize-space()='Create']")
	WebElement createButton;
	@FindBy(how= How.XPATH, using= "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;
	@FindBy(how= How.XPATH, using= "//div[@class='MuiSnackbarContent-message']")
	WebElement messageBox;
	
	
	public void setBillingCycleInput() {
		billingCycleInput.click();
	}
	
	public void clickOnFilterButton() {
		filterButton.click();
	}
	
	public void clickOnCodeButton() {
		codeButton.click();
	}
	
	public void setCodeInput() {
		codeInput.click();
	}
	
	public void setDescriptionInput() {
		descriptionInput.click();
	}
	
	public void clickOnBillingCycleCode(String billingCycle) {
		WebElement billingCycleCode = 
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'"+billingCycle+"')]")));
		billingCycleCode.click();
	}
	
	public void setProcessTypeInput() {
		processTypeInput.click();
	}
	
	public void selectProcessTypeOption() {
		processTypeOption.click();
	}
	
	public void setLastTransDateInput() {
		lastTransDateInput.click();	
	}
	
	public void clickOnLastTransDateButton() {
		wait.until(ExpectedConditions.elementToBeClickable(lastTransDateButton));
		lastTransDateButton.click();	
	}
	
	public void setInvoiceDateInput() {
		invoiceDateInput.click();	
	}
	
	public void clickOnInvoiceDateButton() {
		wait.until(ExpectedConditions.elementToBeClickable(invoiceDateButton));
		invoiceDateButton.click();	
	}
	
	public void setReferenceDateInput() {
		referenceDateInput.click();
	}
	
	public void selectReferenceDateOption() {
		referenceDateOption.click();
	}
	
	public void setActionOnrejectInput() {
		actionOnrejectInput.click();
	}
	
	public void selectActionOnrejectOption() {
		actionOnrejectOption.click();
	}
	
	public void setActionOnSuspectInput() {
		actionOnSuspectInput.click();
	}
	
	public void selectActionOnSuspectOption() {
		actionOnSuspectOption.click();
	}
	
	public void clickOncreateButton() {
		createButton.click();
	}

	public String getMessageBox() {
		wait.until(ExpectedConditions.visibilityOfAllElements(messageBox));
		return messageBox.getText();
	}
	
	public void clickOnGoBackButton() {
		wait.until(ExpectedConditions.visibilityOf(goBackButton));
		goBackButton.click();
	}
}
