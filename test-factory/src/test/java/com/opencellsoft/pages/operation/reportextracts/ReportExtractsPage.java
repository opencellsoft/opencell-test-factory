package com.opencellsoft.pages.operation.reportextracts;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class ReportExtractsPage extends TestBase {
	
	public ReportExtractsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//input[@id = 'searchBar']" )
	WebElement searchInput;
	@FindBy(how = How.XPATH, using = "//table[contains(@id, 'report-extracts')]//tbody//*[@role = 'row' or contains(@class ,'MuiTableRow-root')]")
	List<WebElement> reportExtractsList;
	@FindBy(how = How.XPATH, using = "(//table[contains(@id, 'report-extracts')]//button)[1]" )
	WebElement runReportButton;
	@FindBy(how = How.XPATH, using = "//button[contains(@class , 'confirm') or text() = 'Run']" )
	WebElement confirmButton;
	@FindBy(how = How.XPATH, using = "(//table[contains(@id, 'report-extracts')]//button)[2]" )
	WebElement downloadReportButton;
	@FindBy(how = How.XPATH, using = "//*[contains(@class , 'message')]" )
	WebElement messageText;
	public void setSearchInput(String value) {
		searchInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		searchInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public List<WebElement> reportExtractsList() {
		return reportExtractsList;
	}
	
	public void sortBy(String value) {
		try {
			clickOn(driver.findElement(By.xpath("//span[contains(@class, 'MuiButtonBase-root') and contains(@data-field, '" + value + "')]")));
		}catch (Exception e) {
			clickOn(driver.findElement(By.xpath("//th[@sortby= '" + value + "']")));
		}
		
	}
	
	public void clickOnRunReportButton() {
		clickOn(runReportButton);
	}
	
	public void clickOnConfirmButton() {
		clickOn(confirmButton);
	}
	
	public void clickOnDownloadReportButton() {
		clickOn(downloadReportButton);
	}
	
	public String message() {
		return messageText.getText();
	}
}
