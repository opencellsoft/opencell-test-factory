package com.opencellsoft.pages.operation.querytool;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class QueryBuilderPage extends TestBase {

	public QueryBuilderPage() {
		PageFactory.initElements(driver, this);
	}

	Actions actions;
	JavascriptExecutor js;
	
	@FindBy(how = How.XPATH, using = "//div[@id='scopeEntity' or contains(@aria-labelledby, 'scopeEntity') ]")
	WebElement scopeEntityInput;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add to selected fields' or @title='Add to selected fields']")
	WebElement addToSelectedFieldsButton;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='entityFields.toolbar.action.addToGroupFilter' or @title='Add to current filter group']")
	WebElement addToFilterFieldsButton;
	@FindBy(how = How.XPATH, using = "//div[@data-rbd-droppable-id = 'filterFieldsTable']//table//tbody//tr[1]//td[2]")
	WebElement operatorColumn;
	@FindBy(how = How.XPATH, using = "//div[@data-rbd-droppable-id = 'filterFieldsTable']//table//tbody//tr[1]//td[3]")
	WebElement valueColumn;
	@FindBy(how = How.XPATH, using = "//div[@data-rbd-droppable-id = 'filterFieldsTable']//table//tbody//tr[1]//td[3]//input")
	WebElement valueInput;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Update filter']")
	WebElement updateFilterButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Save']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//input[@id = 'queryName' or @name = 'queryName']")
	WebElement queryNameInput;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//*[text() = 'Confirm']")
	WebElement confirmButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Execute']")
	WebElement executeButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Execute on background']")
	WebElement executeOnBackGroundButton;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//span[text() = 'No notification']")
	WebElement noNotificationButton;
	@FindBy(how = How.XPATH, using = "//*[@aria-label='Download' or text() = 'Download']")
	WebElement downloadButton;
	@FindBy(how = How.XPATH, using = "//*[text()='Schedule']")
	WebElement scheduleButton;
	@FindBy(how = How.XPATH, using = "//textarea[@id = 'usersEmails']")
	WebElement emailInput;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Delete']")
	WebElement deleteButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Save & Execute']")
	WebElement saveAndExecuteButton;
	@FindBy(how = How.XPATH, using = "//input[@id = 'querySelect' or @name = 'querySelect']")
	WebElement querySelectInput;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//input[contains(@id , 'code') or @title = 'Query name']")
	WebElement searchByNameInput;
	
	public void selectScopeEntity( String scopeEntity){
		clickOn(scopeEntityInput);
		try {
			clickOn(driver.findElement(By.xpath("//li[text() = '" + scopeEntity + "' or @data-value = '" + scopeEntity + "']")));
		}catch (Exception e) {
			clickOn(scopeEntityInput);
			waitPageLoaded();
			clickOn(driver.findElement(By.xpath("//li[text() = '" + scopeEntity + "' or @data-value = '" + scopeEntity + "']")));
		}
		
	}

	public void addSelectedField(String selected_field) {
		WebElement field = driver.findElement(By.xpath("//input[@type='checkbox' and contains(@name, '" + selected_field + "')]"));
		actions = new Actions(driver);
		actions.moveToElement(field).perform();
		clickOn(field);
		clickOn(addToSelectedFieldsButton);
	}

	public void addFilterField(String filter_field,String filter_operator,String filter_value) {
		WebElement field = driver.findElement(By.xpath("//input[@type='checkbox' and contains(@name, '" + filter_field + "')]"));
		actions = new Actions(driver);
		actions.moveToElement(field).perform();
		waitPageLoaded();
		clickOn(field);
		clickOn(addToFilterFieldsButton);
		try {
			actions.moveToElement(saveAndExecuteButton).perform();
		}catch (Exception e) {
			actions.moveToElement(updateFilterButton).perform();
			 actions.sendKeys(Keys.PAGE_DOWN).perform();
			 actions.sendKeys(Keys.PAGE_DOWN).perform();
			 actions.sendKeys(Keys.PAGE_DOWN).perform();
		}
		waitPageLoaded();
		actions.doubleClick(operatorColumn).perform();
		waitPageLoaded();
		clickOn(driver.findElement(By.xpath("//*[text()= '" + filter_operator + "']")));
		clickOn(valueColumn);
		actions.doubleClick(valueColumn).perform();
		waitPageLoaded();
		valueInput.sendKeys(filter_value);
		waitPageLoaded();
		valueInput.sendKeys(Keys.ENTER);
		waitPageLoaded();
	}

	public void clickOnUpdateFilterButton() {
		clickOn(updateFilterButton);
	}
	
	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
	
	public void setQueryNameInput( String value) {
		queryNameInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		queryNameInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public void clickOnConfirmButton() {
		clickOn(confirmButton);
	}
	
	public void clickOnExecuteButton() {
		clickOn(executeButton);
	}
	
	public void clickOnExecuteOnBackGroundButton() {
		clickOn(executeOnBackGroundButton);
		clickOn(noNotificationButton);
	}
	
	
	public void clickOnDownloadButton() {
		clickOn(downloadButton);
	}
	
	public void selectFileType(String value) {
		clickOn(driver.findElement(By.xpath("//li[text() = '" + value + "']")));
	}

	public void clickOnScheduleButton() {
		clickOn(scheduleButton);
	}
	
	public void setEmailInput(String value) {
		emailInput.sendKeys(value);
	}
	
	public void checkFileFormat(String value) {
		clickOn(driver.findElement(By.xpath("//span[text()= '"+ value +"']")));
	}
	
	public void checkfrequency(String value) {
		clickOn(driver.findElement(By.xpath("//span[text()= '"+ value +"']")));
	}
	
	public void clickOnDeleteButton() {
		clickOn(deleteButton);
	}
	
	public void selectQueryName (String value) {
		clickOn(querySelectInput);
		searchByNameInput.sendKeys(value);
		waitPageLoaded();
		clickOn(driver.findElement(By.xpath("//td//*[text() = '" + value + "']")));
	}
	
	public void clickOnRemoveSelectedField( String position) {
		clickOn(driver.findElement(By.xpath("(//div[contains(@class, 'selectedFieldsTable')]//tbody/tr["+ position +"]//button)[3]")));
		
	}
}
