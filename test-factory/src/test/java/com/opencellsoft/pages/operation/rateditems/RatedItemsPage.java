package com.opencellsoft.pages.operation.rateditems;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class RatedItemsPage extends TestBase {

	public RatedItemsPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.XPATH, using = "//button[@aria-label= 'Add filter' or @aria-label= 'Filter'  or @title = 'Filter']")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//li[@data-key = 'subscription']")
	WebElement subscriptionChoice;
	@FindBy(how = How.XPATH, using = "//li[@data-key = 'parameterExtra']")
	WebElement ParamExtraChoice;
	@FindBy(how = How.XPATH, using = "//input[@id = 'parameterExtra']")
	WebElement ParamExtraInput;
	@FindBy(how = How.XPATH, using = "//div[@data-source = 'subscription']//input")
	WebElement filterBySubscription;
	@FindBy(how = How.XPATH, using = "//input[@id = 'wildcardOrIgnoreCase code']")
	WebElement subscriptionCode;
	@FindBy(how = How.XPATH, using = "(//div[@role = 'dialog']//table[@filters = '[object Object]']//tbody//tr)[1]")
	WebElement firstResult;
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'20') or contains(text(),'25') ]")
	WebElement rowsPerPageButton;
	@FindBy(how = How.XPATH, using = "//li[@data-value = '50']")
	WebElement option50;
	
	public void clickOnFilterButton() {
		clickOn(filterButton);
	}
	
	public void selectFilterBySubscription() {
		clickOn(subscriptionChoice);
	}
	
	public void selectFilterByParamExtra() {
		clickOn(ParamExtraChoice);
	}
	
	public void clickOnFilterBySubscription() {
		clickOn(filterBySubscription);
	}
	public void setSubscriptionCodeInput(String value) {
		subscriptionCode.sendKeys(Keys.CONTROL + "a");
		subscriptionCode.sendKeys(Keys.DELETE);
		subscriptionCode.sendKeys(value);
		waitPageLoaded();
	}
	
	public void setParamExtraInput(String value) {
		ParamExtraInput.sendKeys(Keys.CONTROL + "a");
		ParamExtraInput.sendKeys(Keys.DELETE);
		ParamExtraInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public void clickOnFirstResult() {
		clickOn(firstResult);
	}
	
	public void clickOnResult( String value) {
		try {
			clickOn(driver.findElement(By.xpath("//div[@role = 'dialog']//*[text() = '" + value + "']")));
		}catch (Exception e) {
			waitPageLoaded();
			waitPageLoaded();
			try {
				setSubscriptionCodeInput(value);
				waitPageLoaded();
				clickOn(driver.findElement(By.xpath("//div[@role = 'dialog']//*[text() = '" + value + "']")));
			}catch (Exception ex) {
				clickOn(firstResult);
			}
		}
		
	}
	// return the number of lines found
	public int elementsFoundSize(String value) {
		List<WebElement> result = driver.findElements(By.xpath("//td[normalize-space()='"+ value + "']"));
		return result.size();
	}
	
	public void selectRowsPerPage(String value) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView();", rowsPerPageButton);
			clickOn(rowsPerPageButton);
			}
		catch (Exception e) {
			clickOn(rowsPerPageButton);
		}
		
		try {
			clickOn(option50);
			}
		catch (Exception e) {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView();", rowsPerPageButton);
			clickOn(rowsPerPageButton);
			clickOn(option50);
		}
		
	}
}
