package com.opencellsoft.pages.products;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ModifyProductPage {


	WebDriver driver;
	
	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement productCode;
	@FindBy(how = How.XPATH, using = "//input[@id='label']")
	WebElement productLabel;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement submitProductPage;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackProductPage;

	public ModifyProductPage(WebDriver driver) {
		this.driver = driver;
	}
	// set product's new code
	public void setCode(String strCode) {
		productCode.clear();
		productCode.sendKeys(strCode);
	}	
	// set product's new label
	public void setLabel(String strProductDescription) {
		productLabel.clear();
		productLabel.sendKeys(strProductDescription);
	}
	// click on save button
	public void clickOnSaveButton() {
		submitProductPage.submit();
	}
	// click on goBack button
	public void clickOnGoBackButton() {
		goBackProductPage.click();
	}
	
}
