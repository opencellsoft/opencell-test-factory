package com.opencellsoft.pages.products;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class ListOfProductsPage extends TestBase {

	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;
	public ListOfProductsPage(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10)).pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addFilterProductButton;
	@FindBy(how = How.XPATH, using = "//BUTTON[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-sizeSmall MuiButton-textSizeSmall MuiButton-colorSecondary MuiButton-disableElevation MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-sizeSmall MuiButton-textSizeSmall MuiButton-colorSecondary MuiButton-disableElevation css-1j44f4v']")
	WebElement createProductButton;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Last']")
	WebElement latestProductsViewed;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase description' or @id = 'searchBar']")
	WebElement productDescriptionInput;
	@FindBy(how = How.XPATH, using = "body > div:nth-child(8) > div:nth-child(3) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(2)")
	WebElement productSelected;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Product Code')]")
	WebElement productCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement productCodeInput;
	@FindBy(how = How.XPATH, using = "//span[@class='MuiButtonBase-root MuiTableSortLabel-root MuiTableSortLabel-active jss514']//*[name()='svg']")
	WebElement sortButton;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Product Code')]")
	WebElement productCodeFilter;


	// This method to click on Create product Button
	public void clickOnCreateProductButton() {
		wait.until(ExpectedConditions.elementToBeClickable(createProductButton));
		createProductButton.click();
	}
	// this method to get the appeared message 
	public String getTextMessage(){
		return	messageTextBox.getText();
	}

	//search for product
	public void searchForProduct(String description) {
		productDescriptionInput.sendKeys(description);
		fWait.until(ExpectedConditions.elementToBeClickable(productSelected));
		productSelected.click();
	}
	
	//search for product
	public void searchForProductByDescription(String code, String description) {
		productDescriptionInput.sendKeys(description);
		waitPageLoaded();
		WebElement result = driver.findElement(By.xpath("//span//*[contains(text(),'" + description + "')]"));
		clickOn(result);
	}
	
	//search for product with advanced filters
	public void advancedSearchForProduct(String code) {
		// TODO Auto-generated method stub
		addFilterProductButton.click();
		productCode.click();
		productCodeInput.sendKeys(code);	
	}
	// this method is to list the latest products viewed by the user
	public void listLatest() {
		// TODO Auto-generated method stub
		latestProductsViewed.click();
	}
	//this method is to sort the products list
	public void clickOnSortButton() {
		// TODO Auto-generated method stub
		// explicit wait - to wait for the button to be click-able
		WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		wait.until(ExpectedConditions.visibilityOf(sortButton));
		// click on the compose button as soon as the button is visible											
		sortButton.click();		
	}
	public void clickOnFilterButton() {
		//		wait.until(ExpectedConditions.elementToBeClickable(filterButton));
		//		filterButton.click();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", filterButton);
	}

	public void clickOnFilterByProductCodeFilter() {
		wait.until(ExpectedConditions.elementToBeClickable(productCodeFilter));
		productCodeFilter.click();
	}

	public void setProductCode(String productCode) {
		productCodeInput.sendKeys(productCode);
	}

	public void selectVisibleFoundProduct(String productCode) {
		//driver.findElement(By.xpath("//span[@title='"+productCode+"']")).click();
		driver.findElement(By.xpath("//MARK[text()='"+productCode+"']")).click();
	}
}