package com.opencellsoft.pages.products;
import java.time.Duration;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class NewProductPage extends TestBase {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public NewProductPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10)).pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement productCode;
	@FindBy(how = How.XPATH, using = "//input[@id='label']")
	WebElement productLabel;
	@FindBy(how = How.XPATH, using = "//input[@name='productLine']")
	WebElement productLineInput;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase description']")
	WebElement productName;
	@FindBy(how = How.CSS, using = "body > div:nth-child(8) > div:nth-child(3) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(2)")
	WebElement productLine;
	@FindBy(how = How.XPATH, using = "//div[@id='brand']")
	WebElement brandsList;
	@FindBy(how = How.XPATH, using = "//div[@id='menu-brand']/div[3]/ul/li[2]")
	WebElement brand;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement submitButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Custom Fields']")
	WebElement customFieldsButton;
	@FindBy(how = How.XPATH, using = "//input[@id='customFields.CF_REF']")
	WebElement customFieldReference;
	@FindBy(how = How.XPATH, using = "//div[@id='priceVersionDateSetting']")
	WebElement priceVersionDateSetting;
	@FindBy(how = How.XPATH, using = "(//*[normalize-space()='Pricing'])[1]")
	WebElement pricingButton;

	// This method is to select a priceVersionDateSetting
	public void selectPriceVersionDateSetting(String dateSetting) {
		wait.until(ExpectedConditions.elementToBeClickable(priceVersionDateSetting));
		clickOn(priceVersionDateSetting);
		try {
		clickOn(priceVersionDateSetting.findElement(By.xpath("//li[contains(text(),'"+dateSetting+"')]")));
		}catch (Exception e) {
			clickOn(priceVersionDateSetting.findElement(By.xpath("//li//*[contains(text(),'"+dateSetting+"')]")));
		}
	} 
	
	// click on add charge button
	public void clickOnPricingTab() {
		wait.until(ExpectedConditions.elementToBeClickable(pricingButton));
		clickOn(pricingButton);
	}
		
	// set a product's code
	public void setProductCode(String strCode) {
		wait.until(ExpectedConditions.visibilityOf(productCode));
		productCode.sendKeys(strCode);
	}

	// set a product's label
	public void setProductLabel(String strProductDescription) {
		wait.until(ExpectedConditions.visibilityOf(productLabel));
		productLabel.sendKeys(strProductDescription);
	}

	//select a product's brand
	public void selectProductBrand() {
		Actions act = new Actions(driver);
		act.moveToElement(brandsList).click().perform();
		brand.click();
	}

	// select product's line
	public void selectProductLine() {
		Actions act = new Actions(driver);
		act.moveToElement(productLineInput).click().perform();
		productLine.click();
	}

	//add custom field to a product
	public void addCustomFields(String customFieldReference) {
		customFieldsButton.click();
		this.customFieldReference.sendKeys(customFieldReference);
	}

	// click save button
	public void clickOnSaveProductButton() {
		wait.until(ExpectedConditions.elementToBeClickable(submitButton));
		submitButton.submit();
	}
	
	// click save button
	public void clickOnSaveButton() {
		clickOn(submitButton);
	}
	// click go back button
	public void clickOnGoBackButton() {
		goBackButton.click();
	}
}