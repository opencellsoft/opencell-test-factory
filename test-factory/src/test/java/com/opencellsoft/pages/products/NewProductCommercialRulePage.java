package com.opencellsoft.pages.products;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class NewProductCommercialRulePage {
	WebDriver driver;

	public NewProductCommercialRulePage(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement commercialRuleCodeInput;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement commercialRuleDescriptionInput;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement commercialRuleType;
	@FindBy(how = How.XPATH, using = "//li[normalize-space()='Replacement']")
	WebElement selectCommercialRule;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement submitProductCommercialRulePage;
	
	// set commercial rule's code
	public void setCode(String ruleCode) {
		commercialRuleCodeInput.sendKeys(ruleCode);
	}
// set commercial rule's description
	public void setDescription(String ruleDescription) {
		commercialRuleDescriptionInput.sendKeys(ruleDescription);
	}
// select a rule's type
	public void setRuleType() {
		commercialRuleType.click();
		selectCommercialRule.click();
	}
// this method is to add a product's commercial rule
	public void setcommercialRulesInformation(String ruleCode, String ruleDescription) {
		this.setCode(ruleCode);
		this.setDescription(ruleDescription);
		this.setRuleType();
		this.save();
	}
// click save button
	public void save() {
		submitProductCommercialRulePage.click();
	}
}
