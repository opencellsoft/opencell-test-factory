package com.opencellsoft.pages.products;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.locators.RelativeLocator;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class ProductPage extends TestBase {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public ProductPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);

	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addFilterProductButton;
	@FindBy(how = How.XPATH, using = "//*[@class='MuiTab-wrapper' or @role='tab'][text()='Attributes']")
	WebElement attributesTab;
	//@FindBy(how = How.XPATH, using = "//span[contains(text(),'Activate')]")
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Activate']")
	WebElement activateProductButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Cancel')]")
	WebElement cancel;
	@FindBy(how = How.CSS, using = ".MuiBox-root:nth-child(2) .ButtonGroupGutters:nth-child(1) .MuiButton-label:nth-child(1) > span:nth-child(2)")
	WebElement createProductButton;
	@FindBy(how = How.XPATH, using = "(//SPAN[text()='Create'])[4]")
	WebElement createAttributeButton;
	@FindBy(how = How.XPATH, using = "(//BUTTON[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-textSizeSmall MuiButton-sizeSmall'])[7]")
	WebElement addAttributeButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Charges')]")
	WebElement chargesTab;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiBox-root jss252 TabPanel']//span[contains(text(),'Create')]")
	WebElement createChargeButton;
	@FindBy(how = How.CSS, using = ".MuiTableRow-root:nth-child(3) > .column-code > .MuiTypography-root")
	WebElement chargeTemplate;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Pricing']")
	WebElement chargeButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Confirm']")
	WebElement confirmDeleteProductButton;
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Close')])[2]")
	WebElement closeProductVersionButton;
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Close')])[1]")
	WebElement closeProductButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Custom Fields']")
	WebElement customFieldsButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'New')]")
	WebElement createProductVersionButton;
	@FindBy(how = How.XPATH, using = "//input[@id='customFields.CF_REF']")
	WebElement customFieldsReference;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Commercial rules']")
	WebElement commercialRulesButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Create')]")
	WebElement createCommercialRuleButton;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement codeProduct;
	@FindBy(how = How.XPATH, using = "(//SPAN[@class='MuiButton-label'])[10]")
	WebElement createChargeBtn;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Delete']")
	WebElement deleteProductButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Duplicate')]")
	WebElement duplicate;
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Duplicate')])[2]")
	WebElement duplicateProductVersionButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Discounts']")
	WebElement discountButton;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiGrid-root gridBlock fullWidthChild fullWidthChild MuiGrid-container']//div[@class='MuiFormControl-root MuiTextField-root ReferenceListInputListPickerText MuiFormControl-marginDense']")
	WebElement discountInput;
	@FindBy(how = How.XPATH, using = "//textarea[@id='productVersions.1.shortDescription']")
	WebElement externalDescriptionInput;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Product Code')]")
	WebElement filterProductCode;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "(//div[contains(@class,'PageTitle')]//a)[2]")
	WebElement returnToOfferLink;
	@FindBy(how = How.XPATH, using = "//input[@id='label']")
	WebElement label;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Medias']")
	WebElement mediaButton;
	@FindBy(how = How.XPATH, using = "//div[contains(@class , 'MuiAlert-message') or contains(@class , 'MuiSnackbarContent-message')]")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//body[1]/div[1]/div[1]/div[1]/div[1]/main[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/fieldset[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/table[1]/tbody[1]/tr[1]/td[2]")
	WebElement createdCharge;
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement productCode;
	@FindBy(how = How.CSS, using = ".MuiGrid-root:nth-child(1) > .MuiPaper-root:nth-child(1) > .MuiBox-root:nth-child(7) .ButtonGroupGutters:nth-child(1) > .MuiButtonBase-root:nth-child(1) > .MuiButton-label:nth-child(1)")
	WebElement pickChargeButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Pick']")
	WebElement pickButton;
	@FindBy(how = How.XPATH, using = "(//*[normalize-space()='Pick'])[1]")
	WebElement pricingPickButton1;
	@FindBy(how = How.XPATH, using = "(//span[normalize-space()='Pick'])[2]")
	WebElement pricingPickButton2;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Publish')]")
	WebElement publishProductVersionButton;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiBox-root jss557 TabPanel']//span[@class='MuiButton-label'][normalize-space()='Pick']")
	WebElement pickProductButton;
	@FindBy(how = How.CSS, using = ".MuiTableCell-root.MuiTableCell-body.column-code.jss794.jss787")
	WebElement selectDiscount;
	@FindBy(how = How.CSS, using = ".MuiTableCell-root.MuiTableCell-body.column-code.jss794.jss787")
	WebElement selectMedia;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement submitButton;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//span[@class='MuiTab-wrapper'][normalize-space()='Tags']")
	WebElement tagButton;
	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/main[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/fieldset[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/button[2]/span[1]/span[2]")
	WebElement createMediaButton;
	@FindBy(how = How.CSS, using = ".MuiTableCell-root.MuiTableCell-body.column-code.jss794.jss787")
	WebElement tagOption;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiGrid-root gridBlock fullWidthChild fullWidthChild MuiGrid-container']//div[@class='MuiFormControl-root MuiTextField-root ReferenceListInputListPickerText MuiFormControl-marginDense']")
	WebElement tagInput;
	@FindBy(how = How.XPATH, using = "//BUTTON[@class='MuiButtonBase-root MuiButton-root MuiButton-outlined MuiButton-outlinedSecondary MuiButton-outlinedSizeSmall MuiButton-sizeSmall MuiButton-disableElevation']")
	WebElement createdAttributeParameters;
	@FindBy(how = How.XPATH, using = "//table[contains(@listid , 'TAB_ATTRIBUTES')]//td//button[@class = 'MuiButtonBase-root MuiButton-root MuiButton-outlined MuiButton-outlinedSecondary MuiButton-outlinedSizeSmall MuiButton-sizeSmall MuiButton-disableElevation' or @type = 'button']")
	List <WebElement> AttributeParametersButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Parameters']")
	WebElement textAttributeParametersButton;
	@FindBy(how = How.XPATH, using = "//input[@id='attributesDialog.mandatory']")
	WebElement attributesDialogMandatory;
	@FindBy(how = How.XPATH, using = "//input[@id='attributesDialog.readOnly']")
	WebElement attributesDialogReadOnly;
	@FindBy(how = How.XPATH, using = "//input[@id='attributesDialog.defaultValue']")
	WebElement attributeDefaultValueInput;
	@FindBy(how = How.XPATH, using = "//input[@id='attributesDialog.display']")
	WebElement attributeDisplayInput;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiGrid-root gridBlock fullWidthChild fullWidthChild MuiGrid-container MuiGrid-grid-xs-12']//div[@class='MuiFormControl-root MuiTextField-root ReferenceListInputListPickerText MuiFormControl-marginDense']")
	WebElement discountsList;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Discounts']")
	WebElement discountsButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Confirm')]")
	//	@FindBy(how = How.XPATH, using = "(//BUTTON[@class='MuiButtonBase-root MuiButton-root MuiButton-outlined MuiButtonGroup-grouped MuiButtonGroup-groupedHorizontal MuiButtonGroup-groupedOutlined MuiButtonGroup-groupedOutlinedHorizontal MuiButtonGroup-groupedOutlined'])[2]")
	WebElement confirmButton;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']")
	WebElement filterByCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement discountCode ;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]")
	WebElement discount;
	@FindBy(how = How.XPATH, using = "//input[@id='productVersions.0.endDate']")
	WebElement productVersionEndDateInput;
	@FindBy(how = How.XPATH, using = "(//span[@class='MuiTab-wrapper'][normalize-space()='General'])[2]")
	WebElement generalTab;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day']//p[@class='MuiTypography-root MuiTypography-body2 MuiTypography-colorInherit'][normalize-space()='30']")
	WebElement productVersionEndDate;
	@FindBy(how = How.XPATH, using = "//*[@class='MuiTab-wrapper' or contains(@class, 'MuiButtonBase-root')][text()='Pricing']")
	WebElement pricingButton;
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Create')])[1]")
	WebElement createPricingButton1;
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Create')])[2]")
	WebElement createPricingButton2;
	@FindBy(how = How.XPATH, using = "(//input[@id = 'wildcardOrIgnoreCase description'])")
	WebElement chargeDescriptionInput;
	@FindBy(how = How.XPATH, using = "(//span[contains(text(), 'Create')])[4]")
	WebElement createAttributeButton2;
	@FindBy(how = How.XPATH, using = "(//*[contains(text(), 'Pick')])[3]")
	WebElement PickAttributeButton;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//input[contains(@id, 'wildcardOrIgnoreCase code')]")
	WebElement filterByAttributeCodeInput;
	
	public void selectDiscountPlan() {
		discount.click();
	}
	public void setDiscountCode(String discountCode) {
		this.discountCode.sendKeys(discountCode);
	}
	public void clickOnAddFilterByCode() {
		filterByCode.click();
	}
	public void clickOnDiscountsButton() {
		discountsButton.click();
	}

	public void clickOnDiscountsList() {
		wait.until(ExpectedConditions.elementToBeClickable(discountsList));
		discountsList.click();
	}

	public String getProductCodeInputText() {
		return productCode.getAttribute("value");
	}

	public String getProductLabelInputText() {
		return label.getAttribute("value");
	}

	// close product version
	public void closeProductVersion() {
		closeProductVersionButton.click();
	}

	// delete a product
	public void deleteProduct() {
		deleteProductButton.click();
		confirmDeleteProductButton.submit();
	}

	// edit a product
	public void editProduct(String strProductCode, String strProductLabel) {
		productCode.clear();
		setProductCode(strProductCode);
		label.clear();
		setProductLabel(strProductLabel);
	}

	// add a custom field
	public void addCustomFields(String customFieldReference) {
		customFieldsButton.click();
		this.customFieldsButton.sendKeys(customFieldReference);
	}

	// click on duplicate button
	public void duplicate() {
		duplicate.click();
	}

	// method to get the appeared message after operation
	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOfAllElements(messageTextBox));
		return messageTextBox.getText();

	}

	// click on add filter button
	public void clickOnAddFilterButton() {
		addFilterProductButton.click();
	}

	// click on filter by code
	public void clickOnFilterProductCode() {
		filterProductCode.click();
	}

	// set product code
	public void setProductCode(String strProductCode) {
		productCode.clear();
		productCode.sendKeys(strProductCode);
	}

	// set product's label
	public void setProductLabel(String strProductLabel) {
		label.clear();
		label.sendKeys(strProductLabel);
	}

	// click on charges section
	public void clickOnChargesTab() {
		chargesTab.click();
	}

	// click on add charge button
	public void clickOnCreateChargeButton() {
		createChargeButton.click();
	}

	// click on pick charge button
	public void clickOnPickChargeButton() {
		pickChargeButton.click();
	}

	// click on charge template
	public void selectChargeTemplate() {
		chargeTemplate.click();
	}

	// click on attributes section
	public void clickOnAttributesTab() {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", attributesTab);
		clickOn(attributesTab);
	}

	// click on create attribute button
	public void clickOnCreateAttributeButton() throws AWTException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", createAttributeButton);
		wait.until(ExpectedConditions.elementToBeClickable(createAttributeButton));
		clickOn(createAttributeButton);
	}

	// click on add attribute button
	public void clickOnAddAttributeButton() throws AWTException {
		Robot robot = new Robot();
		// Scroll Down using Robot class
		robot.keyPress(KeyEvent.VK_PAGE_DOWN);
		robot.keyRelease(KeyEvent.VK_PAGE_DOWN);
		wait.until(ExpectedConditions.visibilityOf(this.addAttributeButton));
		addAttributeButton.click();
	}

	public void clickOnCreateAttributeButton2() {
		clickOn(createAttributeButton2);
	}

	// click on save button
	public void clickOnSaveButton() {
		wait.until(ExpectedConditions.elementToBeClickable(submitButton));
		submitButton.submit();
		waitPageLoaded();
	}

	// click on publish product version button
	public void clickOnPublishProductVersionButton() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(publishProductVersionButton));
			publishProductVersionButton.click();
		}catch (Exception e) {
			clickOn(publishProductVersionButton);
		}
	}

	// click on go back button
	public void clickOnGoBackButton() {
		try {
			goBackButton.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(goBackButton));
			goBackButton.click();
		}
	}

	public void clickOnReturnToOffer() {
		try {
			clickOn(returnToOfferLink);
		}catch (Exception e) {
			goBackButton.click();
		}
	}

	// click on close product button
	public void closeProduct() {
		closeProductButton.click();
	}

	// click on activate product button
	public void activateProduct() {
		wait.until(ExpectedConditions.elementToBeClickable(activateProductButton));
		activateProductButton.click();
	}
	
	public void activateProduct2() {
		clickOn(activateProductButton);
	}

	// click on activate product button
	public void clickOnActivateProductButton() {
		wait.until(ExpectedConditions.elementToBeClickable(activateProductButton));
		activateProductButton.click();
		Actions actions = new Actions(driver);
		actions.moveToElement(activateProductButton).click().build().perform();		
	}

	// this method is to create a product version
	public void clickOnCreateProductVersionButton() {
		wait.until(ExpectedConditions.visibilityOf(createProductVersionButton));
		createProductVersionButton.click();
	}

	public void setProductVersionExternalDescription(String productVersionExternalDescription) {
		externalDescriptionInput.sendKeys(productVersionExternalDescription);
	}

	// this method is to create a product version
	public void createProductVersion(String productExternalDescription) {
		// TODO Auto-generated method stub
		createProductVersionButton.click();
		externalDescriptionInput.sendKeys(productExternalDescription);
		submitButton.submit();

	}

	// click on duplicate a product version
	public void clickOnDuplicateProductVersionButton() {
		duplicateProductVersionButton.click();
	}

	// this method is to add a tag to a product
	public void clickOnTagTab() {

		tagButton.click();
	}

	public void clickOntagDropDown() {
		tagInput.click();

	}

	public void selectTag() {
		tagOption.click();
	}

	public void clickOnSaveTagButton() {
		submitButton.submit();
	}

	// this method is to add a discount plan to a product
	public void addDiscount() {
		discountButton.click();
		discountsList.click();
		selectDiscount.click();
		submitButton.submit();
	}

	// this method is to add a media to a product
	public void addMedia() {
		mediaButton.click();
		pickButton.click();
		selectMedia.click();
		submitButton.submit();
	}

	public void clickOnMediaButton() {
		mediaButton.click();
	}

	// this method is to create a rule for a product
	public void clickOnCommercialRulesButton() {
		commercialRulesButton.click();
	}

	public void clickOnCreateRuleButton() {
		createCommercialRuleButton.click();
	}

	// this method is to click on pick product button
	public void clickOnPickProductButton() {
		pickProductButton.click();
	}

	// this method is to set code of product
	public void setCodeProduct(String strProductCode) {
		codeProduct.sendKeys(strProductCode);
	}

	// this method is to select product
	/*
	 * public void clickOnProductList() { productList.click(); }
	 */

	public void clickOnChargeProduct() {
		clickOn(chargeButton);
	}

	public void clickOnCreateChargeProductButton() {
		fWait.until(ExpectedConditions.elementToBeClickable(createChargeBtn));
		createChargeBtn.click();
	}

	public void clickOnCharge() {
		createdCharge.click();
	}

	public void clickOnCreateMediaButton() {
		fWait.until(ExpectedConditions.elementToBeClickable(createMediaButton));
		createMediaButton.click();
	}

	public void clickOnCreatedAttributeParameters() {
		wait.until(ExpectedConditions.elementToBeClickable(createdAttributeParameters));
		createdAttributeParameters.click();
	}
	public void clickOnCreatedAttributeParameters2() {
		clickOn(AttributeParametersButton.get(0));
	}
	
	public void clickOnAttributeParametersButton(String value) {
		for(int i = 0; i < AttributeParametersButton.size(); i++) {
			String attributeCode = null;
			try {
				attributeCode = driver.findElement(By.xpath("//table[contains(@listId, 'TAB_ATTRIBUTES')]//tbody//a[" + (i+1) + "]//td[2]//span//span")).getText();
			}catch (Exception e) {
				attributeCode = driver.findElement(By.xpath("//table[contains(@listId, 'TAB_ATTRIBUTES')]//tbody//a[" + (i+1) + "]//td[2]//span//span")).getText();
			}
			
			if(attributeCode.equals(value)) {
				waitPageLoaded();
				clickOn(AttributeParametersButton.get(i));
				break;
			}
		}	
	}
	
	public void clickOnAttributeParameterButton( int i ) {
		clickOn(AttributeParametersButton.get(i));
	}
	
	public void clickOnTextAttributeParametersButton() throws AWTException {
		Robot robot = new Robot();
		// Scroll Down using Robot class
		robot.keyPress(KeyEvent.VK_PAGE_DOWN);
		robot.keyRelease(KeyEvent.VK_PAGE_DOWN);
		wait.until(ExpectedConditions.visibilityOf(textAttributeParametersButton));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].click();", textAttributeParametersButton);
	}

	public void toggleMandatoryAttributeOn() {
		this.attributesDialogMandatory.click();
	}

	public void toggleReadOnlyAttributeOn() {
		this.attributesDialogReadOnly.click();
	}
	
	public void setAttributeSequence(String value) {
//		attributeSequenceInput.clear();
//		attributeSequenceInput.sendKeys(Value);
//		waitPageLoaded();
	}
	public void setAttributeMandatoryWithEL(String value) {
//		attributeMandatoryWithELInput.clear();
//		attributeMandatoryWithELInput.sendKeys(Value);
//		waitPageLoaded();
	}
	public void setAttributeDefaultValue(String value) {
		attributeDefaultValueInput.clear();
		attributeDefaultValueInput.sendKeys(value);
		waitPageLoaded();
	}
	public void setAttributeValidationType(String value) {
//		attributeValidationTypeInput.clear();
//		attributeValidationTypeInput.sendKeys(Value);
//		waitPageLoaded();
	}
	public void setAttributeValidationPattern(String value) {
//		attributeValidationPatternInput.clear();
//		attributeValidationPatternInput.sendKeys(Value);
//		waitPageLoaded();
	}
	public void setAttributeValidationLabel(String value) {
//		attributeValidationLabelInput.clear();
//		attributeValidationLabelInput.sendKeys(Value);
//		waitPageLoaded();
	}
	public void setAttributeMandatory(Boolean value) {
//		attributeMandatoryInput.sendKeys(Value);
	}
	public void setAttributeDisplay(Boolean value) {
		WebElement element = driver.findElement(By.xpath("(//div[@role = 'dialog']//span[contains(@class, 'MuiSwitch-switchBase MuiSwitch-colorPrimary')])[3]"));
		String class_name = element.getAttribute("class");
		if((value.equals(true) && !class_name.contains("Mui-checked")) || (value.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(attributeDisplayInput);
		}
	}
	public void setAttributeReadOnly(Boolean value) {
//		attributeReadOnlyInput.sendKeys(Value);
	}

	public void clickOnConfirmButton() {
		clickOn(confirmButton);
	}

	public void clickOnGeneralTab() {
		clickOn(generalTab);
	}
	
	public void clickOnProductVersionEndDateInput() {
		clickOn(productVersionEndDateInput);
	}
	
	public void setProductVersionEndDate() {
		clickOn(productVersionEndDate);
	}

	public void clickOnPricingButton() {
		clickOn(pricingButton);
		clickOn(pricingButton);
	}

	public void clickOnCreatePricingButton() {
		clickOn(createPricingButton1);
	}

	public void clickOnCreatePricingButton1() {
		clickOn(createPricingButton1);
	}
	
	public void clickOnCreatePricingButton2() {
		clickOn(createPricingButton2);
	}

	public boolean isChargePicked(String chargeDescription) {
		List<WebElement> charge = driver.findElements(By.xpath("//span[normalize-space()='" + chargeDescription + "']"));
		if(charge.size()==0) { return false;}else {return true;}
	}

	public void clickOnPickButton2(){
		try {
			clickOn(pricingPickButton2);
			
		}catch (Exception e) {
			clickOn(driver.findElement(By.xpath("(//div[@class = 'ButtonGroupGutters'])[2]//button[1]")));
		}
	}
	
	public void clickOnPickButton1(){
		try {
			clickOn(pricingPickButton1);
			
		}catch (Exception e) {
			clickOn(driver.findElement(By.xpath("(//div[@class = 'ButtonGroupGutters'])[1]//button[1]")));
		}
	}

	public void setChargeDescription(String Description) {
		
		chargeDescriptionInput.sendKeys(Description);
		waitPageLoaded();
	}

	public void selectChargeToPick(String Description) {
		WebElement charge = driver.findElement(By.xpath("//div[@role = 'dialog']//span[normalize-space()='" + Description + "']"));
		try {
			charge.click();}
		catch (Exception e) {
			driver.findElement(By.xpath("(//div[@role = 'dialog']//tbody//td)[2]")).click();
		}
	}
	
	
	public void clickOnPickAttributeButton(){
		clickOn(PickAttributeButton);
	}
	
	public void setAttributeCode(String attributeCode) {
		filterByAttributeCodeInput.sendKeys(attributeCode);
		waitPageLoaded();
	}
	
	public void clickOnAttribute(String attributeCode){
		try {
			clickOn(driver.findElement(By.xpath("//div[contains(@class, 'MuiDialog-container')]//table//tr//td[2]")));
		}catch (Exception e) {
			clickOn(driver.findElement(By.xpath("//div[contains(@class, 'MuiDialog-container')]//table//tr//td[2]")));
		}
		
	}
}