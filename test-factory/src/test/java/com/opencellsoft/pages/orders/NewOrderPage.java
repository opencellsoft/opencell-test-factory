package com.opencellsoft.pages.orders;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class NewOrderPage extends TestBase {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public NewOrderPage(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(40))
				.pollingEvery(Duration.ofSeconds(2)).ignoring(NoSuchElementException.class);
	}

	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Save')]")
	WebElement saveOrderButton;
	@FindBy(how = How.XPATH, using = "//input[@id='client']")
	WebElement customerList;	
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addFilterButton;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Customer Code')]")
	WebElement codeFilterOption;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement customerCode;
	@FindBy(how = How.XPATH, using = "//input[@id='billingPlan']")
	WebElement invoicingPlansList;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement invoicingPlanCode;	
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//input[@id = 'description' or @name = 'description' or @aria-describedby = 'description-helper-text']")
	WebElement customerDescription;
	@FindBy(how = How.XPATH, using = "//table//tbody//td[1]")
	WebElement firstElement;
	// This method is to get text message
	public String getTextMessage() {
		fWait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}
	
	// This method is to click on invoicing plan list
	public void clickOnInvoicingPlansList() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(invoicingPlansList));
			invoicingPlansList.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(invoicingPlansList));
			invoicingPlansList.click();
		}
	}
	
	public void setInvoicingPlanCode(String invoicingPlanCode) {
		wait.until(ExpectedConditions.elementToBeClickable(this.invoicingPlanCode));
		this.invoicingPlanCode.sendKeys(invoicingPlanCode);	
	}
	
	// This method is to select invoicing plan
	public void selectInvoicingPlan(String invoicingPlanCode) {
		try {
			WebElement elem = wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("//mark[contains(text(),'" + invoicingPlanCode + "')]")));
			elem.click();
		}
		catch(org.openqa.selenium.TimeoutException ex)
		{
			WebElement elem = wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath("//mark[contains(text(),'" + invoicingPlanCode + "')]")));
			elem.click();
		}
	}
	
	// This method is to click customer list	
	public void clickOnCustomerList() {
		wait.until(ExpectedConditions.elementToBeClickable(customerList));
		customerList.click();
	}
	
	public void setCustomerDescription(String customerName){
		customerDescription.sendKeys(customerName);
		waitPageLoaded();
	}
	public void clickOnFirstResult() {
		clickOn(firstElement);
	}
	// This method is to click on add filter button
	public void clickOnAddFilterButton() {
		wait.until(ExpectedConditions.elementToBeClickable(addFilterButton));
		addFilterButton.click();
	}
	
	// This method is to click on customer code filter
	public void selectCodeFilterOption() {
		wait.until(ExpectedConditions.elementToBeClickable(codeFilterOption));
		codeFilterOption.click();
	}

	// This method is to set customer code in the code text box
	public void setCustomerCode(String CodeCustomer) {
		wait.until(ExpectedConditions.visibilityOf(customerCode));
		customerCode.sendKeys(CodeCustomer);
	}
	
	// This method is to select customer
	public void clickOnFoundCustomer(String strCustomerCodeFound) {
		WebElement elem = 
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//mark[contains(text(),'" + strCustomerCodeFound + "')]")));
		elem.click();
	}
	
	// this method is to click on the button save to save an order
	public void clickOnSaveOrderButton() {
		wait.until(ExpectedConditions.elementToBeClickable(saveOrderButton));
		saveOrderButton.click();
	}
	public void clickOnSaveButton() {
		clickOn(saveOrderButton);
	}	
}