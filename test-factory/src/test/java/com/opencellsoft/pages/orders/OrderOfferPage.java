package com.opencellsoft.pages.orders;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OrderOfferPage {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;
	JavascriptExecutor js;
	Actions act;

	public OrderOfferPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		js = (JavascriptExecutor) driver;
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(60))
				.pollingEvery(Duration.ofSeconds(10)).ignoring(NoSuchElementException.class);
		act = new Actions(this.driver);
	}

	@FindBy(how = How.XPATH, using = "//input[@name='offerTemplateCode']")
	WebElement offerInput;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addFilterButton;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Code')]")
	WebElement filterByCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement customerCode;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "//input[@id='offerTemplateCode']")
	WebElement offerTemplateCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement offerCode;
	@FindBy(how = How.XPATH, using = "//input[@type='checkbox']")
	WebElement productCheckbox;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[1]")
	WebElement quantityCell;
	@FindBy(how = How.XPATH, using = "//input[@id='r0.c0']")
	WebElement quantityInput;
	@FindBy(how = How.XPATH, using = "//th[@class='cell actions']//button[@type='button']")
	WebElement editProductAttribute;
	@FindBy(how = How.XPATH, using = ".//td[1]")
	WebElement selectedProduct;
	@FindBy(how = How.XPATH, using = "//table//tr[1]//td[2]")
	WebElement productCell;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/th[1]/button[1]")
	WebElement editAttributeButton;
	@FindBy(how = How.XPATH, using = "//table/tbody/tr[1]/td[4]")
	WebElement attributeValue;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiExpansionPanelSummary-content']")
	WebElement productTab;
	@FindBy(how = How.XPATH, using = "(//DIV[@class='MuiExpansionPanelSummary-content'])[2]")
	WebElement product2Tab;
	
	public void clickOnProductTab() {
		wait.until(ExpectedConditions.elementToBeClickable(productTab));
		productTab.click();
	}
	
	public void clickOnProduct2Tab() {
		wait.until(ExpectedConditions.elementToBeClickable(product2Tab));
		product2Tab.click();
	}
	
	// This method is to set attributes values
	public void clickOnEditProductAttributeButton() throws AWTException {
		Robot robot = new Robot();
		// Scroll Down using Robot class
		robot.keyPress(KeyEvent.VK_PAGE_DOWN);
		robot.keyRelease(KeyEvent.VK_PAGE_DOWN);
		wait.until(ExpectedConditions.visibilityOf(editProductAttribute));
		this.editProductAttribute.click();
	}

	public void selectOfferTemplate(String offerCode) {
		this.offerTemplateCode.click();
		this.addFilterButton.click();
		this.filterByCode.click();
		this.offerCode.sendKeys(offerCode);
		WebElement elem = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//mark[contains(text(),'" + offerCode + "')]")));
		elem.click();
	}

	public void clickOnOfferTemplateCode() {
		wait.until(ExpectedConditions.elementToBeClickable(offerTemplateCode));
		offerTemplateCode.click();
	}
	
	// This method is to click on add filter button
	public void clickOnAddFilterButton() {
		wait.until(ExpectedConditions.elementToBeClickable(addFilterButton));
		addFilterButton.click();
	}
	
	public void selectCodeFilterOption() {
		wait.until(ExpectedConditions.elementToBeClickable(filterByCode));
		filterByCode.click();
	}

	public void setOfferCode(String setCodeOffer) {
		wait.until(ExpectedConditions.visibilityOf(offerCode));
		offerCode.sendKeys(setCodeOffer);
	}
	
	public void clickOnFoundOffer(String offerCode) {		
		WebElement elem = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//mark[contains(text(),'" + offerCode + "')]")));
		elem.click();
	}
	
	public void selectProduct() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOf(selectedProduct));
		js.executeScript("arguments[0].click();", productCheckbox);
	}

	// This method is to set the first quantity of product
	public void setProductQuantity(String quantity) throws InterruptedException {		
		Actions act = new Actions(driver);
		js.executeScript("arguments[0].setAttribute('class','cell selected')", quantityCell);
		quantityCell.click();
		act.doubleClick(quantityCell).perform();
		act.doubleClick(quantityCell).perform();
		wait.until(ExpectedConditions.visibilityOf(quantityInput));
		act.doubleClick(quantityInput).perform();
		quantityInput.sendKeys(quantity);
		act.doubleClick(this.productCell).perform();		
	}

	// this method is to click on the offer Input
	public void clickOnOfferInput() {
		offerInput.click();
	}

	// this method is to click on choose filter button
	public void clickOnFilterButton() {
		addFilterButton.click();
	}

	// this method is to select filter by code from filters list
	public void clickOnFilterByCode() {
		filterByCode.click();
	}

	// this method is to set a customer's code to search for offer
	public void setCustomerCode(String code) {
		customerCode.sendKeys(code);
	}

	// this method is to click on save button
	public void clickSaveButton() {
		fWait.until(ExpectedConditions.visibilityOf(saveButton));
		saveButton.submit();
	}

	// this method is to select an offer using customer's code for search
	public void selectOffer(String code) {
		this.clickOnOfferInput();
		this.clickOnFilterButton();
		this.clickOnFilterByCode();
		this.setCustomerCode(code);
		this.clickSaveButton();
	}

	// this method is to click on goback button to go to previous page
	public void clickGoBack() {
		fWait.until(ExpectedConditions.visibilityOf(goBackButton));
		goBackButton.click();
	}

	// method to get the appeared message after operation
	public String getTextMessage() {
		fWait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}
	
	public String getClassAttributeValue() {
		wait.until(ExpectedConditions.visibilityOf(this.attributeValue));
		return this.attributeValue.getAttribute("class");
	}

	public void clickOnEditAttribute() throws AWTException {
		wait.until(ExpectedConditions.visibilityOf(editAttributeButton));
		editAttributeButton.click();
	}
}
