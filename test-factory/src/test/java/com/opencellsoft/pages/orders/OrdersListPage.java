package com.opencellsoft.pages.orders;

import java.time.Duration;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class OrdersListPage extends TestBase {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public OrdersListPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(60))
				.pollingEvery(Duration.ofSeconds(20)).ignoring(NoSuchElementException.class);
	}

	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addFilterButton;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Customer')]")
	WebElement filterByCustomer;
	@FindBy(how = How.XPATH, using = "//input[@id='billingAccount']")
	WebElement customerInput;
	@FindBy(how = How.XPATH, using = "//tbody/a/td[1]")
	WebElement foundCustomer;
	@FindBy(how = How.XPATH, using = "//tbody/a/td[1]")
	WebElement orderFound;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-textSizeSmall MuiButton-sizeSmall']")
	WebElement createOrderButton;
	@FindBy(how = How.XPATH, using = "//button[@aria-controls = 'customized-menu']")
	WebElement customizedMenu;
	@FindBy(how = How.XPATH, using = "//div[text() = 'Create order']")
	WebElement createOrderSubMenu;
	public void clickOnCreateOrderButton() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(createOrderButton));
			createOrderButton.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(createOrderButton));
			createOrderButton.click();
		}
		waitPageLoaded();
	}
	
	public void clickOnCreateOrder() {
		clickOn(customizedMenu);
		clickOn(createOrderSubMenu);
	}
	// this method is to click on filter button
	public void clickAddFilterCustomerButton() {
		wait.until(ExpectedConditions.visibilityOf(addFilterButton));
		addFilterButton.click();
	}

	// this method is to select a filter by customer
	public void selectCustomerFilter() {
		filterByCustomer.click();
	}

	// this method is to enter a customer code to filter
	public void enterCustomer(String customerCode) {
		customerInput.sendKeys(customerCode);
	}

	// this method is to select the customer we're looking for
	public void clickOnCustomer() {
		wait.until(ExpectedConditions.visibilityOf(foundCustomer));
		foundCustomer.click();
	}

	// this method is to find a quote
	public void findQuote(String customerCode) throws InterruptedException {
		this.clickAddFilterCustomerButton();
		this.selectCustomerFilter();
		this.enterCustomer(customerCode);
		Thread.sleep(4000);
		this.clickOnCustomer();
	}
	
	public void clickOnOrderFound() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOf(orderFound));
		this.orderFound.click();
	}
}
