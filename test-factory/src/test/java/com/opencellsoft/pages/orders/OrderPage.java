package com.opencellsoft.pages.orders;

import java.awt.AWTException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class OrderPage extends TestBase {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public OrderPage(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(2)).ignoring(NoSuchElementException.class);
	}

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Validate')]")
	WebElement validateOrderButton;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Create')]")
	WebElement addOfferButton;
	@FindBy(how = How.XPATH, using = "//tbody//tr[1]")
	WebElement customer;
	@FindBy(how = How.XPATH, using = "//tbody//tr[1]")
	WebElement consumer;
	@FindBy(how = How.XPATH, using = "//input[@id='userAccount']")
	WebElement consumerInput;
	@FindBy(how = How.XPATH, using = "//span[@class='MuiButtonBase-root MuiTableSortLabel-root']//span[contains(text(),'Subscription')]")
	WebElement subscriptionTab;
	@FindBy(how = How.XPATH, using = "//input[@id='billingPlan']")
	WebElement invoicingPlanList;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Finalize')]")
	WebElement finalizeButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Update progress to next level')]")
	WebElement UpdateProgressToNextLevel;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Go Back')]")
	WebElement goBack;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Save')]")
	WebElement saveOrderButton;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiGrid-root gridBlock fullWidthChild fullWidthChild MuiGrid-container MuiGrid-spacing-xs-2']//div[2]//div[1]//div[1]")
	WebElement defaultConsumerInput;
	@FindBy(how = How.XPATH, using = "//table[2]/tbody/tr[1]/td[4]")
	WebElement invoiceAmount1;
	@FindBy(how = How.XPATH, using = "//table[2]/tbody/tr[2]/td[4]")
	WebElement invoiceAmount2;
	@FindBy(how = How.XPATH, using = "//table[2]/tbody/tr[3]/td[4]")
	WebElement invoiceAmount3;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Duplicate')]")
	WebElement duplicateOrderButton;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Add line')]")
	WebElement addOrderLineButton;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Create')]")
	WebElement createOrderLineButton;
	@FindBy(how = How.XPATH, using = "//button//span[contains(text(), 'Apply one shot')]")
	WebElement applyOneShotButton;
	@FindBy(how = How.XPATH, using = "/html/body/div[1]/div/div/div/main/div[2]/div/div[2]/div[2]/div/div/fieldset/div/form/div/div[1]/div/div/div/div/div[2]/div[1]/div/div/a")
	WebElement customerLink;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement invoicingPlanCode;
	@FindBy(how = How.XPATH, using = "//label[contains(text(), 'Order N')]/following-sibling::div//span//span")
	WebElement orderNumber;
	@FindBy(how = How.XPATH, using = "(//label[contains(text(), 'Status')]/following-sibling::div//div//span)[1]")
	WebElement orderStatus;
	@FindBy(how = How.XPATH, using = "(//form[@class =  'Order']//table[@aria-labelledby = 'tableTitle'])[1]")
	WebElement taxDetailsTable;
	@FindBy(how = How.XPATH, using = "//table[@title = 'Order lines' or @listid = 'B2B-customer-care/order-offers' or @listid = 'B2B/order-offers']")
	WebElement orderLineTable;
	@FindBy(how = How.XPATH, using = "//div[contains(text(), 'Delete')]")
	WebElement deleteOrderLineButton;
	@FindBy(how = How.XPATH, using = "//*[contains(text() , 'Confirm')]")
	WebElement confirmButton;
	@FindBy(how = How.XPATH, using = "//div[@role= 'dialog']//span[text() = 'Apply']")
	WebElement applyButton;
	// this method is to click on the button validate to validate an order
	public void clickOnValidateOrderButton() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(validateOrderButton));
			clickOn(validateOrderButton);
		}catch (Exception e) {
			clickOn(validateOrderButton);
		}
	}

	// this method is to get text from text box
	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

	public String getInvoiceAmount1() throws AWTException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", invoiceAmount1);
		wait.until(ExpectedConditions.visibilityOf(invoiceAmount1));
		return invoiceAmount1.getText();
	}

	public String getInvoiceAmount2() throws AWTException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", invoiceAmount2);
		wait.until(ExpectedConditions.visibilityOf(invoiceAmount2));
		return invoiceAmount2.getText();
	}

	public String getInvoiceAmount3() throws AWTException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", invoiceAmount3);
		wait.until(ExpectedConditions.visibilityOf(invoiceAmount3));
		return invoiceAmount3.getText();
	}

	// this method is to click on add offer button
	public void clickOnAddOfferButton() {
		addOfferButton.click();
	}

	public void clickOnDefaultConsumer() {
		wait.until(ExpectedConditions.elementToBeClickable(consumerInput));
		consumerInput.click();
	}

	// this method is to select a customer
	public void clickOnCustomer() {
		wait.until(ExpectedConditions.elementToBeClickable(customer));
		customer.click();
	}

	public void clickOnConsumer(String consumerCode) {
		//wait.until(ExpectedConditions.elementToBeClickable(consumer));
		//consumer.click();
		WebElement elem = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//span[contains(text(),'" + consumerCode + "')]")));		
		elem.click();
	}

	// This method is to click On subscription Button
	public void clickOnSubscription() {
		subscriptionTab.click();
	}

	// This method is to click on invoicing plan list
	public void invoicingPlanList() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(invoicingPlanList));
			invoicingPlanList.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(invoicingPlanList));
			invoicingPlanList.click();
		}
	}

	// This method is to select invoicing plan
	public void invoicingPlanSelected(String strInvoiceLineSelected) {
		invoicingPlanList.findElement(By.xpath("//li[contains(text(),'" + strInvoiceLineSelected + "')]")).click();
	}

	// This method is to select invoicing plan
	public void selectInvoicingPlan(String invoicingPlanCode) {
		WebElement elem = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//mark[contains(text(),'" + invoicingPlanCode + "')]")));
		elem.click();
		/*JavascriptExecutor je = (JavascriptExecutor) driver;
		WebElement element = driver.findElement(By.xpath("//li[contains(text(),'" + invoicingPlanCode + "')]"));
		je.executeScript("arguments[0].scrollIntoView(true);", element);
		je.executeScript("arguments[0].click();", element);*/

	}

	public void setInvoicingPlanCode(String invoicingPlanCode) {
		wait.until(ExpectedConditions.elementToBeClickable(this.invoicingPlanCode));
		this.invoicingPlanCode.sendKeys(invoicingPlanCode);	
	}

	// This method is to click finalize button
	public void clickFinalize() {
		wait.until(ExpectedConditions.elementToBeClickable(finalizeButton));
		finalizeButton.click();
	}

	// This method is to click Update Progress To Next Level
	public void clickUpdateProgressToNextLevel1() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(UpdateProgressToNextLevel));
			UpdateProgressToNextLevel.click();
		}catch (Exception e) {
			clickOn(UpdateProgressToNextLevel);
		}

	}

	public void clickUpdateProgressToNextLevel2() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(UpdateProgressToNextLevel));
			UpdateProgressToNextLevel.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(UpdateProgressToNextLevel));
			UpdateProgressToNextLevel.click();
		}		
	}

	public void clickUpdateProgressToNextLevel3() {
		wait.until(ExpectedConditions.elementToBeClickable(UpdateProgressToNextLevel));
		UpdateProgressToNextLevel.click();
	}

	public void clickOnGoBackButton() {
		wait.until(ExpectedConditions.visibilityOf(UpdateProgressToNextLevel));
		goBack.click();
	}

	public void clickOnSaveOrderButton() {
		wait.until(ExpectedConditions.elementToBeClickable(saveOrderButton));
		clickOn(saveOrderButton);
	}

	// This method is to select Sub Category
	public void selectDefaultConsumer(String defaultConsumer) {
		defaultConsumerInput.click();
		defaultConsumerInput.findElement(By.xpath("//span[contains(text(),'" + defaultConsumer + "')]")).click();
	}

	public void clickOnCustomerLink(String customerCode) {
		driver.findElement(By.xpath("//a[normalize-space()='" + customerCode + "']")).click();
	}
	public void clickOnCustomerLink() {
		JavascriptExecutor je = (JavascriptExecutor) driver;

		je.executeScript("arguments[0].click();", customerLink);

		//		customerLink.click();
	}

	public void clickOnDuplicateOrderButton() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(duplicateOrderButton));
			clickOn(duplicateOrderButton);
		}catch (Exception e) {
			clickOn(duplicateOrderButton);
		}

	}

	public void clickOnAddOrderLineButton() {
		clickOn(addOrderLineButton);
	}

	public void clickOnCreateOrderLineButton() {
		clickOn(createOrderLineButton);
	}

	public void clickOnApplyOneShotButton() {
		clickOn(applyOneShotButton);
	}

	public void clickOnGoBackButton1() {
		clickOn(goBack);
	}

	public String getOrderNumber() {
		return orderNumber.getText();
	}

	public void checkOrderLine(String i){
		clickOn(driver.findElement(By.xpath("(//table[@title = 'Order lines' or @listid = 'B2B-customer-care/order-offers' or @listid = 'B2B/order-offers']//input[@type = 'checkbox'])[" + i + "]")));
	}
	
	public void clickOnDeleteOrderLineButton() {
		clickOn(deleteOrderLineButton);
	}
	
	public void clickOnConfirmButton(){
		clickOn(confirmButton);
	}
	
	public void goToOrderLineDetails(String i) {
		try {
			clickOn(driver.findElement(By.xpath("(//table[@title = 'Order lines' or @listid = 'B2B-customer-care/order-offers' or @listid = 'B2B/order-offers']//tbody//a)[" + i + "]//td[6]")));
		}catch (Exception e) {
			clickOn(driver.findElement(By.xpath("(//table[@title = 'Order lines' or @listid = 'B2B-customer-care/order-offers' or @listid = 'B2B/order-offers']//tbody//tr)[" + i + "]//td[6]")));
		}
	}
	
	public List<String> getOrderDetails() {
		List<String> orderDetails = new ArrayList<>();
		// get order number
		orderDetails.add(orderNumber.getText());
		// get order status
		orderDetails.add(orderStatus.getText());
		// get tax rate
		orderDetails.add(taxDetailsTable.findElement(By.xpath("//tbody//tr[1]//td[1]")).getText());
		// get tax 
		orderDetails.add(taxDetailsTable.findElement(By.xpath("//tbody//tr[1]//td[2]")).getText());
		// get amount with tax
		orderDetails.add(taxDetailsTable.findElement(By.xpath("//tbody//tr[1]//td[3]")).getText());
		// get amount without tax
		orderDetails.add(taxDetailsTable.findElement(By.xpath("//tbody//tr[1]//td[4]")).getText());

		return orderDetails;
	}

	public List<String> getOrderLineDetails(int i, String version) {
		List<String> getOrderLineDetails = new ArrayList<>();
		WebElement OpenMoreDetailsButton = orderLineTable.findElement(By.xpath("//a[" + i + "]//td[1]//span[1]"));
		try {
			clickOn(OpenMoreDetailsButton);
		}catch (Exception e) {
			OpenMoreDetailsButton = orderLineTable.findElement(By.xpath("//a[" + i + "]//td[1]//span[1]"));
			clickOn(OpenMoreDetailsButton);
		}

		if(version.equals("15.0.X")) {
			//Offer code
			getOrderLineDetails.add(orderLineTable.findElement(By.xpath("//a[" + i + "]//td[2]//span//span")).getText());
			//subscription code
			getOrderLineDetails.add(orderLineTable.findElement(By.xpath("//a[" + i + "]//td[3]//span//span")).getText());
			//consumer code
			getOrderLineDetails.add(orderLineTable.findElement(By.xpath("//a[" + i + "]//td[7]//span//span")).getText());
		}else {
			//Offer code
			getOrderLineDetails.add(orderLineTable.findElement(By.xpath("//a[" + i + "]//td[3]//span//span")).getText());
			//subscription code
			getOrderLineDetails.add(orderLineTable.findElement(By.xpath("//a[" + i + "]//td[4]//span//span")).getText());
			//consumer code
			getOrderLineDetails.add(orderLineTable.findElement(By.xpath("//a[" + i + "]//td[8]//span//span")).getText());
		}
		//Product description
		getOrderLineDetails.add(driver.findElement(By.xpath("//table[@title = 'Order lines' or @listid = 'B2B-customer-care/order-offers' or @listid = 'B2B/order-offers']//table//tr[1]//td[1]//*//*")).getText());
		//quantity
		getOrderLineDetails.add(driver.findElement(By.xpath("//table[@title = 'Order lines' or @listid = 'B2B-customer-care/order-offers' or @listid = 'B2B/order-offers']//table//tr[1]//td[2]//*//*")).getText());
		
		clickOn(OpenMoreDetailsButton);
		
		return getOrderLineDetails;
	}

	public void updateAttributValueInTheTable(String attributeName, String newAttributeValue) {
		WebElement elem = null, input, span;
		Actions action = new Actions(driver);
		List<String> listof = List.of("listofnumericvaluesattribut","listoftextvaluesattribut");
		List<String> multipleListof = List.of("multiplelistnumericvaluesattribut","multiplelisttextvaluesattribut");
		for(int i = 3; i<10; i++) {
			String tableAttributeName  = driver.findElement(By.xpath("//table//thead//th[" + i + "]")).getText();
			if(attributeName.equals(tableAttributeName)) {
				try {
					elem = driver.findElement(By.xpath("//table//tbody//tr[1]//td[" + i + "]"));
					action.doubleClick(elem).perform();
					waitPageLoaded();
				}catch (Exception ex) {
					elem = driver.findElement(By.xpath("//table//tbody//tr[1]//td[" + i + "]"));
					action.doubleClick(elem).perform();
					waitPageLoaded();
				}
				// Boolean attribute 
				if(attributeName.equals("booleanattribut")) {
					span = driver.findElement(By.xpath("//table//tbody//tr[1]//td[" + i + "]//span[contains(@class , 'MuiCheckbox-root')]"));
					String classValue = span.getAttribute("class");
					if((newAttributeValue.equals("true") && !classValue.contains("Mui-checked") ) || (newAttributeValue.equals("false") && classValue.contains("Mui-checked") )) {
						input = driver.findElement(By.xpath("//table//tbody//tr[1]//td[" + i + "]//input"));
						clickOn(input);
					}
				}
				else if(attributeName.equals("dateattribut")) {
					try {
						input = driver.findElement(By.xpath("//table//tbody//tr[1]//td[" + i + "]//input"));
					}catch (Exception e) {
						elem = driver.findElement(By.xpath("//table//tbody//tr[1]//td[" + i + "]"));
						action.doubleClick(elem).perform();
						waitPageLoaded();
						input = driver.findElement(By.xpath("//table//tbody//tr[1]//td[" + i + "]//input"));
					}
					
					clickOn(input);
					WebElement day = driver.findElement(By.xpath("//p[text() = '" + newAttributeValue + "']"));
					clickOn(day);
				}
				else if( listof.contains(attributeName)) {
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + newAttributeValue + "')]")));
				}
				else if( multipleListof.contains(attributeName)) {
//					clickOn(driver.findElement(By.xpath("//*[(text()= '" + newAttributeValue + "')]")));
				}
				else {
					input = driver.findElement(By.xpath("//table//tbody//td[" + i + "]//input"));
					input.sendKeys(Keys.CONTROL + "a");
					input.sendKeys(Keys.DELETE);
					input.sendKeys(newAttributeValue);
					waitPageLoaded();
					input.sendKeys(Keys.RETURN);
					waitPageLoaded();
				}
				break;
			}
		}
	}

	public void updateAttributValueInThePopup(String attributeName, String newAttributeValue) {
		WebElement elem = null, input, span;
		Actions action = new Actions(driver);
		List<String> listof = List.of("listofnumericvaluesattribut","listoftextvaluesattribut");
		List<String> multipleListof = List.of("multiplelistnumericvaluesattribut","multiplelisttextvaluesattribut");
		for(int i = 1; i<7; i++) {
			String xpath = "//div[@role = 'dialog']//table//tbody//tr[" + i + "]//td[1]";
			String tableAttributeName  = driver.findElement(By.xpath(xpath)).getText();
			if(attributeName.equals(tableAttributeName)) {
				try {
					elem = driver.findElement(By.xpath("//div[@role = 'dialog']//table//tbody//tr[" + i + "]//td[2]"));
					action.doubleClick(elem).perform();
					waitPageLoaded();
				}catch (Exception ex) {
					elem = driver.findElement(By.xpath("//div[@role = 'dialog']//table//tbody//tr[" + i + "]//td[2]"));
					action.doubleClick(elem).perform();
					waitPageLoaded();
				}
				// Boolean attribute 
				if(attributeName.equals("booleanattribut")) {
					span = driver.findElement(By.xpath("//div[@role = 'dialog']//table//tbody//tr[" + i + "]//td[2]//span[contains(@class , 'MuiCheckbox-root')]"));
					String classValue = span.getAttribute("class");
					if((newAttributeValue.equals("true") && !classValue.contains("Mui-checked") ) || (newAttributeValue.equals("false") && classValue.contains("Mui-checked") )) {
						input = driver.findElement(By.xpath("//table//tbody//tr[1]//td[" + i + "]//input"));
						clickOn(input);
					}
				}
				else if(attributeName.equals("dateattribut")) {
					try {
						input = driver.findElement(By.xpath("//div[@role = 'dialog']//table//tbody//tr[" + i + "]//td[2]//input"));
					}catch (Exception e) {
						elem = driver.findElement(By.xpath("//div[@role = 'dialog']//table//tbody//tr[" + i + "]//td[2]"));
						action.doubleClick(elem).perform();
						waitPageLoaded();
						input = driver.findElement(By.xpath("//div[@role = 'dialog']//table//tbody//tr[" + i + "]//td[2]//input"));
					}
					
					clickOn(input);
					WebElement day = driver.findElement(By.xpath("//p[text() = '" + newAttributeValue + "']"));
					clickOn(day);
				}
				else if( listof.contains(attributeName)) {
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + newAttributeValue + "')]")));
				}
				else if( multipleListof.contains(attributeName)) {
					clickOn(elem.findElement(By.tagName("svg")));
					action.doubleClick(elem).perform();
					waitPageLoaded();
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + newAttributeValue + "')]")));
				}
				else {
					input = driver.findElement(By.xpath("//div[@role = 'dialog']//table//tbody//tr[" + i + "]//td[2]//input"));
					input.sendKeys(Keys.CONTROL + "a");
					input.sendKeys(Keys.DELETE);
					input.sendKeys(newAttributeValue);
					waitPageLoaded();
					input.sendKeys(Keys.RETURN);
					waitPageLoaded();
				}
				break;
			}
			
		}
	}
	
	public void clickOnApplyButton() {
		clickOn(applyButton);
	}
	
}