package com.opencellsoft.pages.orders;

import java.awt.AWTException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.google.inject.Key;
import com.opencellsoft.base.TestBase;

public class NewOrderLinePage extends TestBase {
	
	public NewOrderLinePage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//input[@id = 'offerTemplateCode']")
	WebElement offerInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'subscription.code']")
	WebElement subscriptionInput;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter' or @aria-label='Filter'or @title = 'Filter']")
	WebElement addFilterButton;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Code')]")
	WebElement filterOptionCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement offerCodeInput;
	@FindBy(how = How.XPATH, using = "//button[@type = 'submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//span[text() = 'Go Back']")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "//table[contains(@class, 'data-grid')]//button[contains(@class, 'MuiButtonBase-root MuiButton-root MuiButton-text')]")
	List<WebElement> otherAttributesButton;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//span[text() = 'Cancel']")
	WebElement clickOnCloseOtherAttributesButton;
	
	public void clickOnOfferInput() {
		clickOn(offerInput);
	}
	
	public void clickOnSubscriptionInput() {
		clickOn(subscriptionInput);
	}
	
	public void clickOnAddFilterButton() {
		clickOn(addFilterButton);
	}
	public void selectFilterOptionCode() {
		clickOn(filterOptionCode);
	}
	public void setCodefilter(String Code) {
		offerCodeInput.sendKeys(Code);
		waitPageLoaded();
	}
	
	public void clickOnselectedResult(String Code) {
		try {
			clickOn(driver.findElement(By.xpath("//*[contains(text(),'" + Code + "')]")));
		}catch (Exception ex) {
			clickOn(driver.findElement(By.xpath("(//table//tbody//tr//td)[1]")));
		}
	}
	
	public void clickOnProductTab(String Code) {
		try {
			clickOn(driver.findElement(By.xpath("(//*[contains(text(),'" + Code + "')])[1]")));
		}catch (Exception e) {
			try {
				clickOn(driver.findElement(By.xpath("(//div[contains(@class, 'MuiButtonBase-root MuiIconButton-root')])[2]")));
			}catch (Exception ex) {
				clickOn(driver.findElement(By.xpath("(//div[contains(@class, 'MuiButtonBase-root MuiIconButton-root')])[1]")));
			}
		}
		
	}
	
	public void setQuantityInput(String line, String value) {
		WebElement quantity= null, deliveryDate = null;
		
		Actions act = new Actions(driver);
		try {
			quantity = driver.findElement(By.xpath("(//table[@class = 'data-grid'])[1]//tr[" + line + "]//td[1]"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
		    js.executeScript("arguments[0].scrollIntoView(true);", quantity);
		    waitPageLoaded();
			act.doubleClick(quantity).perform();
			waitPageLoaded();
			WebElement quantityInput = driver.findElement(By.xpath("//input[@type = 'number']"));
			quantityInput.sendKeys(Keys.CONTROL + "a");
			quantityInput.sendKeys(value);
			waitPageLoaded();
			deliveryDate = driver.findElement(By.xpath("(//table[@class = 'data-grid'])[1]//tr[1]//td[2]"));
			clickOn(deliveryDate);
		}catch (Exception e) {
			quantity = driver.findElement(By.xpath("(//table[@class = 'data-grid'])[1]//tr[" + line + "]//td[1]"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
		    js.executeScript("arguments[0].scrollIntoView(true);", quantity);
		    waitPageLoaded();
			act.doubleClick(quantity).perform();
			waitPageLoaded();
			WebElement quantityInput = driver.findElement(By.xpath("//input[@type = 'number']"));
			quantityInput.sendKeys(Keys.CONTROL + "a");
			quantityInput.sendKeys(value);
			waitPageLoaded();	
			deliveryDate = driver.findElement(By.xpath("(//table[@class = 'data-grid'])[1]//tr[1]//td[2]"));
			clickOn(deliveryDate);
		}
	}
	
	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
	
	public void clickOnGoBackButton() {
		clickOn(goBackButton);
	}
	
	public void clickOnOtherAttributesButton(int i) {
		clickOn(otherAttributesButton.get(i));
	}
	
	public void clickOnCloseOtherAttributesButton() {
		clickOn(clickOnCloseOtherAttributesButton);
	}
}
