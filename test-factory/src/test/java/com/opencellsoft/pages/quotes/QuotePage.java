package com.opencellsoft.pages.quotes;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class QuotePage {

	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public QuotePage(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-textSizeSmall MuiButton-sizeSmall']")
	WebElement createQuoteOffers;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Create')]")
	WebElement createQuoteOfferButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'New version')]")
	WebElement newQuoteVersionButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Publish version')]")
//	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Publish version')])[1]")
	WebElement publishQuoteVersionButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Send')]")
	WebElement sendQuoteButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Request approval')]")
	WebElement requestApprovalButton;	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'ACCEPTED')]")
	WebElement validateQuoteButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Download PDF')]")
	WebElement downloadQuotePDFButton;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Duplicate')]")
	WebElement duplicateQuoteButton;
	@FindBy(how = How.XPATH, using = "//input[@id='quoteVersions.0.discountPlanCode']")
	WebElement discountInput;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Code')]")
	WebElement filterCodeOption;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement codeInput;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]")
	WebElement foundDiscountPlan;
	@FindBy(how = How.XPATH, using = "//span[@class='MuiButton-label']//span[contains(text(),'Create')]")
	WebElement createButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Add']")
	WebElement addQuoteLineButton;
	@FindBy(how = How.XPATH, using = "//input[@id='quoteVersions.0.contractCode']")
	WebElement contractInput;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Actions']")
	WebElement actionsButton;
	
	//this method is to click On add quote line button in quote page
	public void clickOnAddQuoteLineButton() {
		wait.until(ExpectedConditions.elementToBeClickable(addQuoteLineButton));
		addQuoteLineButton.click();
	}
	//this  method is to click on the create quote button in quote list
	public void clickOnCreateQuoteButton() {
		wait.until(ExpectedConditions.elementToBeClickable(createButton));
		createButton.click();
	}

	// get message from textBox
	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

	// This method is to click on new version button
	public void clickOnNewQuoteVersionButton() {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,250)");
		jse.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		// Actions actions = new Actions(driver);
		// actions.moveToElement(newQuoteVersionButton).click().perform();
		wait.until(ExpectedConditions.elementToBeClickable(newQuoteVersionButton));
		newQuoteVersionButton.click();
	}

	// This method is to click on create quote offers button
	public void clickOnCreateQuoteOffers() {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0, 250);");
		// jse.executeScript("window.scrollBy(0,250)");
		// jse.executeScript("window.scrollBy(0,250)");
		// Actions actions = new Actions(driver);
		// actions.moveToElement(createQuoteOffers).click().perform();
		fWait.until(ExpectedConditions.visibilityOf(createQuoteOffers));
		wait.until(ExpectedConditions.elementToBeClickable(createQuoteOffers));
		createQuoteOffers.click();
	}

	// This method is to click on create quote offers button
	public void clickOnCreateQuoteOffer() {
		wait.until(ExpectedConditions.elementToBeClickable(createQuoteOffers));
		createQuoteOfferButton.click();
	}

	// This method is to click on publish version button
	public void clickOnPublishVersionQuoteButton() {
		//JavascriptExecutor jse = (JavascriptExecutor) driver;
		//jse.executeScript("scroll(0, 250);");
		//JavascriptExecutor js = (JavascriptExecutor) driver;
		//js.executeScript("arguments[0].click();", publishQuoteVersionButton);
		try {
			wait.until(ExpectedConditions.elementToBeClickable(publishQuoteVersionButton));
			publishQuoteVersionButton.click();
			//Actions actions = new Actions(driver);
			//actions.moveToElement(publishQuoteVersionButton).click().perform();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(publishQuoteVersionButton));
			Actions actions = new Actions(driver);
			actions.moveToElement(publishQuoteVersionButton).click().perform();
		}		
	}

	// This method is to click on send button
	public void clickOnSendQuoteButton() {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0, 250);");
		wait.until(ExpectedConditions.elementToBeClickable(sendQuoteButton));
		sendQuoteButton.click();
	}
	
	public void clickOnRequestApprovalButton() {
		wait.until(ExpectedConditions.elementToBeClickable(requestApprovalButton));
		requestApprovalButton.click();
	}	

	// This method is to click on validate button
	public void clickOnValidateQuoteButton() {
		wait.until(ExpectedConditions.elementToBeClickable(validateQuoteButton));
		validateQuoteButton.click();
	}

	// This method is to click on download PDF button
	public void clickOnDownloadQuotePDF() {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0, 250);");
		wait.until(ExpectedConditions.elementToBeClickable(downloadQuotePDFButton));
		downloadQuotePDFButton.click();
	}

	// This method is to click on save quote offer button
	public void clickOnSaveButton() {
		saveButton.submit();
	}

	// This method is to click duplicate quote
	public void clickOnDuplicateQuoteButton() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", duplicateQuoteButton);
	}	
	
	//this method is to click on the discount input to display discount list
	public void clickOnDiscountInput() {
		fWait.until(ExpectedConditions.elementToBeClickable(discountInput));
		discountInput.click();
	}
	
	//this method is to click on the filter button
	public void clickOnFilterButton() {
		wait.until(ExpectedConditions.elementToBeClickable(filterButton));
		filterButton.click();
	}
	
	// this method is to select the filter by code option
	public void selectFilterByCodeOption() {
		fWait.until(ExpectedConditions.elementToBeClickable(filterCodeOption));
		filterCodeOption.click();
	}
	
	// this method is to set the code text in code input
	public void setCodeFilter(String code) {
		wait.until(ExpectedConditions.visibilityOf(codeInput));
		codeInput.sendKeys(code);
	}
	
	// this method is to select the found discount plan
	public void selectFoundDiscountPlan() {
		wait.until(ExpectedConditions.elementToBeClickable(foundDiscountPlan));
		foundDiscountPlan.click();
	}
	
	// this method is to click On contract input to display the list of contract 
	public void clickOnContractInput() {
		contractInput.click();
	}
	
	// this method is to select a contract in the quote page
	public void selectContract(String contractCode) {
		driver.findElement(By.xpath("//span[@title='"+contractCode+"']")).click();
	}
	
	public void clickOnActionsButton() {	
		Actions actions = new Actions(driver);
		try {
			wait.until(ExpectedConditions.elementToBeClickable(actionsButton));
			actions.moveToElement(actionsButton).click().perform();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(actionsButton));
			actions.moveToElement(actionsButton).click().perform();
		}		
	}
}
