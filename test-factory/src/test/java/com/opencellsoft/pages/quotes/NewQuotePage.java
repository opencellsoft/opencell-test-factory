package com.opencellsoft.pages.quotes;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class NewQuotePage extends TestBase {

	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public NewQuotePage(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10)).pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement quoteCode;
	@FindBy(how = How.XPATH, using = "//input[@name='applicantAccount']")
	WebElement customerInput;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter' or @title='Filter']")
	WebElement addFilterButton;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Customer Code')]")
	WebElement codeFilterOption;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement customerCode;
	@FindBy(how = How.XPATH, using = "//tbody//tr[1]")
	WebElement foundCustomer;
	@FindBy(how = How.XPATH, using = "//div[@id='userAccount']")
	WebElement defaultConsumerList;
	@FindBy(how = How.XPATH, using = "//li[@class='MuiButtonBase-root MuiListItem-root MuiMenuItem-root MuiMenuItem-gutters MuiListItem-gutters MuiListItem-button']")
	WebElement defaultConsumerSelected;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackBtn;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//div[@id='sellerCode']")
	WebElement sellerList;
	@FindBy(how = How.XPATH, using = "//li[contains(text(),'SELLER_FR')]")
	WebElement sellerFR;

	// get message from textBox
	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

	// This method is to set quote code in the code text box
	public void setQuoteCode(String quoteCode) {
		fWait.until(ExpectedConditions.visibilityOf(this.quoteCode));
		this.quoteCode.sendKeys(quoteCode);
	}

	// This method is to click customer
	public void clickOnCustomerInput() {
//		fWait.until(ExpectedConditions.elementToBeClickable(customerInput));
//		customerInput.click();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", customerInput);

	}

	// This method is to click on add filter button
	public void clickOnAddFilterButton() {
		wait.until(ExpectedConditions.elementToBeClickable(addFilterButton));
		addFilterButton.click();
		//JavascriptExecutor js = (JavascriptExecutor) driver;
		//js.executeScript("arguments[0].click();", addFilterButton);
	}

	// This method is to click on customer code filter
	public void selectCodeFilterOption() {
		wait.until(ExpectedConditions.elementToBeClickable(codeFilterOption));
		codeFilterOption.click();
	}

	// This method is to set customer code in the code text box
	public void setCustomerCode(String setCodeCustomer) {
		wait.until(ExpectedConditions.visibilityOf(customerCode));
		customerCode.sendKeys(setCodeCustomer);
	}

	// This method is to select customer
	public void clickOnFoundCustomer(String strCustomerCodeFound) {
		WebElement elem = 
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//mark[contains(text(),'" + strCustomerCodeFound + "')]")));
		elem.click();
	}

	// This method is to select default consumer
	public void selectDefaultConsumer(String selectDefaultConsumer) {
		fWait.until(ExpectedConditions.visibilityOf(defaultConsumerList));
		defaultConsumerList.click();
		WebElement elem = 
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[contains(.,'" + selectDefaultConsumer + "')]")));
		elem.click();
	}

	// This method is to click on save button
	public void clickOnSaveQuoteButton() {
		wait.until(ExpectedConditions.visibilityOf(saveButton)); 
		saveButton.submit();

	}

	// This method is to go back button
	public void clickOnGoBackButton() {
		wait.until(ExpectedConditions.elementToBeClickable(goBackBtn));
		goBackBtn.click();
	}
	
	public void clickOnSellerList() {
		wait.until(ExpectedConditions.elementToBeClickable(sellerList));
		sellerList.click();
	}
	
	public void clickOnSellerFR() {
		try {
		wait.until(ExpectedConditions.elementToBeClickable(sellerFR));
		sellerFR.click();}
		catch (Exception e) {
			clickOn(driver.findElement(By.xpath("(//ul[@aria-labelledby = 'sellerCode-label']//li)[1]")));
		}
	}
}