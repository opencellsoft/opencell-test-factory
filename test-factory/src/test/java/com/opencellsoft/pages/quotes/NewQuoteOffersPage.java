package com.opencellsoft.pages.quotes;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.utility.Constant;

public class NewQuoteOffersPage extends TestBase {

	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;
	JavascriptExecutor js;

	public NewQuoteOffersPage(WebDriver driver) {
		this.driver = driver;
		js = (JavascriptExecutor) driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
	}

	// Using FindBy for locating elements
	@FindBy(how = How.NAME, using = "offerTemplate00")
	WebElement offerTemplate;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addOfferFilterButton;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Code')]")
	WebElement addOfferFilterCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement codeOffer;
	@FindBy(how = How.XPATH, using = "//tbody//tr[1]")
	WebElement codeOfferSelected;
	@FindBy(how = How.XPATH, using = "//div[@id='userAccount']")
	WebElement consumerList;
	@FindBy(how = How.XPATH, using = "//li[@class='MuiButtonBase-root MuiListItem-root MuiMenuItem-root MuiMenuItem-gutters MuiListItem-gutters MuiListItem-button']")
	WebElement selectedConsumer;
	@FindBy(how = How.XPATH, using = "//div[@id='quoteLine']/div/div[2]/div/div/div/div/div/div[2]/span/table/tbody/tr/td/span/span/input")
	WebElement productCheckboxInput;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[1]")
	WebElement productCell;
	@FindBy(how = How.XPATH, using = "//span[contains(@class,'MuiIconButton-label')]//child::input[@type='checkbox']")
	WebElement productCheckbox;
	@FindBy(how = How.XPATH, using = ".//td[6]")
	WebElement deliveryDateInput;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[1]")
	WebElement quantityCell;
	@FindBy(how = How.XPATH, using = "//input[@id='r0.c0']")
	WebElement quantiteInput;
	@FindBy(how = How.XPATH, using = "(//SPAN[@class='MuiButton-label'])[11]")
	WebElement editProductAttributeButton;
	@FindBy(how = How.XPATH, using = "//div[@class=\"MuiTypography-root MuiTypography-body1\"]")
	WebElement attributeListValues;
	@FindBy(how = How.XPATH, using = "//td[@class='cell selected editing']")
	WebElement attributeValuesToSelect;
	@FindBy(how = How.XPATH, using = "(//tbody/tr/td[2])[2]")
	WebElement attributeValue;
	@FindBy(how = How.XPATH, using = "//div[3]/div/div/div/div/div/span/table/tbody/tr/td[2]")
	WebElement selectedAttributeValue;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Activate')]")
	WebElement activateQuoteOfferButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Cancel')]")
	WebElement cancelAttributeValueButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Save']")
	WebElement saveQuoteOfferButton;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement submitButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "/html/body/div/div/div/div/main/div[2]/div/div[2]/div[2]/div/div/fieldset/div/form/div/div[2]/div/div[3]/div/div/div/div[2]/div[2]/div/div[2]/div/div/div/div[2]/div/div/div/div/div/div/div[2]/span/table/tbody/tr[1]/td[2]")
	WebElement deliveryDate;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiExpansionPanelSummary-content']")
	WebElement offerDetails;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase name']")
	WebElement offerName;

	// get message from textBox
	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

	// This method is to click offer quote
	public void clickOnOffer() {
		wait.until(ExpectedConditions.visibilityOf(offerTemplate));
		offerTemplate.click();
	}

	// This method is to click on add filter button
	public void clickOnAddOfferFilterButton() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", addOfferFilterButton);
	}

	// This method is to click on add filter code offer
	public void clickOnAddOfferFilterCode() {
		addOfferFilterCode.click();
	}

	// This method is to set code offer in the code text box
	public void setCodeOffer(String codeOffer) {
		this.codeOffer.sendKeys(codeOffer);
	}

	// This method is to select offer
	public void clickOnCodeOfferFound(String codeOfferFound) {
		WebElement elem = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//mark[contains(text(),'" + codeOfferFound + "')]")));
		elem.click();
	}

	// This method is to select consumer
	public void selectConsumer(String consumer) {
		wait.until(ExpectedConditions.visibilityOf(consumerList));
		consumerList.click();// assuming you have to click the "dropdown" to open it
		WebElement elem = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[contains(.,'" + consumer + "')]")));
		elem.click();
	}

	// This method is to select the first check box product
	public void selectProduct() throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", productCell);
		Actions actions = new Actions(driver);
		actions.moveToElement(productCell).click().perform();		
	}

	// This method is to set the first quantity of product
	public void setProductQuantity(String quantity) throws InterruptedException {		
		Actions act = new Actions(driver);
		wait.until(ExpectedConditions.visibilityOf(quantityCell));
		js.executeScript("arguments[0].setAttribute('class','cell selected')", quantityCell);
		//act.doubleClick(quantiteCell).perform();
		act.doubleClick(quantityCell).perform();
		wait.until(ExpectedConditions.visibilityOf(quantiteInput));
		act.doubleClick(quantiteInput).perform();
		quantiteInput.sendKeys(quantity);
		act.doubleClick(deliveryDate).perform();
		Thread.sleep(2000);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", saveQuoteOfferButton);
		wait.until(ExpectedConditions.visibilityOf(saveQuoteOfferButton));
		js.executeScript("arguments[0].click();", saveQuoteOfferButton);
//		saveQuoteOfferButton.click();
	}

	// This method is to set attributes values
	public void clickOnEditProductAttributeButton() throws AWTException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", editProductAttributeButton);
		wait.until(ExpectedConditions.elementToBeClickable(editProductAttributeButton));
		this.editProductAttributeButton.click();
	}
	
	//this method is to set values of the attributes list
	public void setListAttributeValues() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOf(attributeValue));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('class','cell selected')", attributeValue);
		wait.until(ExpectedConditions.elementToBeClickable(attributeValue));
		attributeValue.click();
		Actions act = new Actions(this.driver);
		act.doubleClick(selectedAttributeValue).perform();
		Thread.sleep(2000);
		Actions keyDown = new Actions(driver);
		keyDown.sendKeys(Keys.chord(Keys.DOWN, Keys.DOWN, Keys.ENTER)).perform();
	}

	// This method is to click on activate button
	public void clickOnActivateQuoteOffersButton() {
		wait.until(ExpectedConditions.elementToBeClickable(activateQuoteOfferButton));
		activateQuoteOfferButton.click();
	}

	// This method is to click on save button
	public void clickOnSaveQuoteOffersButton() throws InterruptedException, AWTException {
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", submitButton);		
		try {
			wait.until(ExpectedConditions.elementToBeClickable(this.submitButton));
			submitButton.submit();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(this.submitButton));
			submitButton.submit();
		}
	}

	// This method is to click on go back button
	public void clickOnGoBackQuoteOffersButton() {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", goBackButton);
		wait.until(ExpectedConditions.visibilityOf(goBackButton));
		goBackButton.click();
	}

	public void clickOnOfferDetails() {
		wait.until(ExpectedConditions.elementToBeClickable(this.offerDetails));
		clickOn(offerDetails);
	}
	
	public void setOffername(String offerName) {
		wait.until(ExpectedConditions.visibilityOf(this.offerName));
		this.offerName.sendKeys(offerName);
	}
	
	public void selectOfferByName(String offerName) throws AWTException {
		WebElement elem = 
			//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//mark[contains(text(),'" + offerName + "')]")));
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table/tbody/tr[1]/td[1]")));
	elem.click(); 	
	}
}