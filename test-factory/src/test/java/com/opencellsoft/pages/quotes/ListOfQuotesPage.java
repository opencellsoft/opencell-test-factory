package com.opencellsoft.pages.quotes;

import java.time.Duration;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ListOfQuotesPage {

	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public ListOfQuotesPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(3)).ignoring(NoSuchElementException.class);
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-textSizeSmall MuiButton-sizeSmall']")
	WebElement createQuoteButton;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addFilterQuote;
	@FindBy(how = How.XPATH, using = "//input[@id='searchBar']")
	WebElement searchBar;	
	@FindBy(how = How.XPATH, using = "//li[@class='MuiButtonBase-root MuiListItem-root MuiMenuItem-root new-filter-item MuiMenuItem-gutters MuiListItem-gutters MuiListItem-button']//span[contains(text(),'Quote code')]")
	WebElement addFilterCodeQuote;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement setCodeQuote;
	@FindBy(how = How.XPATH, using = "//table[1]/tbody[1]/a[1]/td[1]")
	WebElement quoteSelected;

	// This method is to create quote button
	public void clickOnCreateQuoteButton() {
		fWait.until(ExpectedConditions.visibilityOf(createQuoteButton));
		// click on the compose button as soon as the button is visible
		createQuoteButton.click();
	}

	public void clickOnAddFilterQuoteButton() {
		fWait.until(ExpectedConditions.visibilityOf(addFilterQuote));
		// click on the compose button as soon as the button is visible
		addFilterQuote.click();
	}

	public void clickOnAddFilterCodeQuoteButton() {
		fWait.until(ExpectedConditions.visibilityOf(addFilterCodeQuote));
		// click on the compose button as soon as the button is visible
		addFilterCodeQuote.click();
	}

	public void setCodeQuote(String strCodeQuote) {
		fWait.until(ExpectedConditions.visibilityOf(setCodeQuote));
		// click on the compose button as soon as the button is visible
		setCodeQuote.sendKeys(strCodeQuote);
	}

	// write the customer code in the search bar filter
	public void setSearchBarText(String text) {
		wait.until(ExpectedConditions.visibilityOf(searchBar));
		searchBar.sendKeys(text);
	}
	
	public void selectQuote() {
		try {
			wait.until(ExpectedConditions.visibilityOf(quoteSelected));
			// click on the compose button as soon as the button is visible
			quoteSelected.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.visibilityOf(quoteSelected));
			// click on the compose button as soon as the button is visible
			quoteSelected.click();
		}
	}

}
