package com.opencellsoft.pages.commercialrules;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class NewCommercialRules {

	WebDriver driver;

	public NewCommercialRules(WebDriver driver) {
		this.driver = driver;
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement commercialRulesCode;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement commercialRulesSummary;
	@FindBy(how = How.ID, using = "type")
	WebElement commercialTypeList;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement submitButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;
	
	// This method is to set commercial rules code in the code text box
	public void setCommercialRulesCode(String strCommercialRulesCode) {
		commercialRulesCode.sendKeys(strCommercialRulesCode);
	}
	
	// This method is to set commercial rules description in the description text box
	public void setCommercialRulesSummary(String strCommercialRulesSummary) {
		commercialRulesSummary.sendKeys(strCommercialRulesSummary);
	}
	
	// This method is to select commercial rules type
	public void commercialType(String selectCommercialType) {
		commercialTypeList.click();// assuming you have to click the "dropdown" to open it
		commercialTypeList.findElement(By.xpath("//li[contains(.,'"+selectCommercialType+"')]")).click();	
	}

	// This method is to click on submit button
	public void clickOnSubmitButton() {
		submitButton.submit();
	}
	
	// This method is to click on go back button
	public void clickOnGoButtonButton() {
		goBackButton.click();
	}
}
