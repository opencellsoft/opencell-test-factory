package com.opencellsoft.pages.articles;

import java.awt.AWTException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ArticlePage {
	WebDriver driver;
	Actions act;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public ArticlePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement articleCode;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement articleDescription;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbar-root MuiSnackbar-anchorOriginTopRight']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Label by Language']")
	WebElement labelsByLanguageTab;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Mapping']")
	WebElement mappingSection;
	@FindBy(how = How.XPATH, using = "//div[@class='ButtonGroupGutters']//button[@type='button']")
	WebElement createMappingLine;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Custom Fields']")
	WebElement customFieldsSection;
	@FindBy(how = How.XPATH, using = "//input[@id='CF_TEXT']")
	WebElement CFText;
	@FindBy(how = How.XPATH, using = "//input[@id='CF_NUMBER']")
	WebElement CFNumber;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Save']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSizeSmall MuiButton-sizeSmall']")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Delete']")
	WebElement deleteButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Confirm']")
	WebElement confirmButton;
	@FindBy(how = How.XPATH, using = "//div[@id='taxClass']")
	WebElement classTaxInput;
	@FindBy(how = How.CSS, using = "tbody tr td:nth-child(1)")
	WebElement languageCell;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[1]/div[1]/div[1]/div[1]")
	WebElement languageInput;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[2]/div[1]/span[1]")
	WebElement labelCell;
	@FindBy(how = How.XPATH, using = "//input[@class='data-editor']")
	WebElement labelInput;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/th[1]/button[1]")
	WebElement removeLabelsByLanguageButton;
	@FindBy(how = How.XPATH, using = "//body[1]/div[1]/div[1]/div[1]/div[1]/main[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/fieldset[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/table[1]/tbody[1]/tr[1]/td[1]")
	WebElement mappingList;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBack;
	@FindBy(how = How.XPATH, using = "//tbody/tr[2]/td[1]")
	WebElement emptyCell;
	
	// get the appeared msg after operation
	public String getTextMessage() {
		return messageTextBox.getText();
	}

	// This method is to click mapping section
	public void clickOnMappingSection() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(mappingSection));
			mappingSection.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(mappingSection));
			mappingSection.click();
		}		
	}

	// This method is to click on create mapping
	public void clickOnCreateMapping() throws AWTException {
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", createMappingLine);
		try {
			wait.until(ExpectedConditions.elementToBeClickable(createMappingLine));
			Actions act = new Actions(driver);
			act.moveToElement(createMappingLine).click().build().perform();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(createMappingLine));
			Actions act = new Actions(driver);
			act.moveToElement(createMappingLine).click().build().perform();
			//createMappingLine.click();
		}		
		
	}

	// save update
	public void saveUpdates() {
		saveButton.click();
		goBackButton.click();
	}

	// this method is to create an article's custom filed
	public void createCustomFields(String CfText, String CfNumber) {
		customFieldsSection.click();
		CFText.sendKeys(CfText);
		CFNumber.sendKeys(CfNumber);
		this.saveUpdates();
	}

	public void clickOnGoBackButton() {
		// TODO Auto-generated method stub
		wait.until(ExpectedConditions.elementToBeClickable(goBackButton));
		goBackButton.click();
	}

	public void clickOnDeleteButton() {
		// TODO Auto-generated method stub
		deleteButton.click();
	}

	public void clickOnConfirmButton() {
		// TODO Auto-generated method stub
		confirmButton.click();

	}

	public void clickOnTaxClassInput() {
		// TODO Auto-generated method stub
		classTaxInput.click();
	}

	public void clickOnNewTaxClass(String taxClass) {
		WebElement elem = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//li[normalize-space()='" + taxClass + "')]")));
		elem.click();
	}

	public void clickOnSaveButton(String classTax) {
		// TODO Auto-generated method stub
		saveButton.click();
	}
	public void selectLanguage() throws InterruptedException {
		Actions act = new Actions(driver);
		act.doubleClick(languageCell).perform();
		Thread.sleep(1000);
		Actions keyDown = new Actions(driver);
		keyDown.sendKeys(Keys.chord(Keys.UP, Keys.ENTER)).perform();
		keyDown.sendKeys(Keys.ESCAPE).perform();
	}
	// this method is to select language
	public void editLanguage() throws InterruptedException {
		Actions act = new Actions(driver);
		act.doubleClick(languageCell).perform();
		Thread.sleep(1000);
		Actions keyDown = new Actions(driver);
		keyDown.sendKeys(Keys.chord(Keys.DOWN, Keys.ENTER)).perform();
		keyDown.sendKeys(Keys.ESCAPE).perform();
	}

	// this method is to select language
	public void setLabel(String label) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('class','cell editing')", labelCell);
		Actions act = new Actions(this.driver);
		act.doubleClick(labelCell).perform();
		labelInput.sendKeys(label);

	}
	public void editLabel(String editedLabel) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('class','cell editing')", labelCell);
		Actions act = new Actions(this.driver);
		act.doubleClick(labelCell).perform();
		labelInput.clear();
		labelInput.sendKeys(editedLabel);

	}

	// this method is to click on save article button
	public void clickSaveArticleButton() {
		saveButton.submit();
	}

	// this method is to click on save article button
	public void selectMapping() {
		wait.until(ExpectedConditions.visibilityOf(mappingList));
		mappingList.click();
	}

	public void clickOnlabelsByLanguageTab() {
		// TODO Auto-generated method stub
		labelsByLanguageTab.click();
	}

	public void clickOnRemoveLabelsByLanguageButton() {
		// TODO Auto-generated method stub
		wait.until(ExpectedConditions.visibilityOf(removeLabelsByLanguageButton));
		removeLabelsByLanguageButton.click();
	}

	public void clickOnGoBack() {
		// TODO Auto-generated method stub
		goBack.click();
	}
	public String getArticleCodeInputText() {
		// TODO Auto-generated method stub
		return articleCode.getAttribute("value");

	}
	public String getArticleDescriptionInputText() {
		// TODO Auto-generated method stub
		return articleDescription.getAttribute("value");
	}

	public void selectEmptyCell() {
		// TODO Auto-generated method stub
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('class','cell selected')", emptyCell);
		emptyCell.click();
	}

}
