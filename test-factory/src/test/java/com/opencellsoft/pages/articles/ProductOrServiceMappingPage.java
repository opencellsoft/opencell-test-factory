package com.opencellsoft.pages.articles;

import java.time.Duration;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.Wait;

public class ProductOrServiceMappingPage {

	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public ProductOrServiceMappingPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10)).pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement mappingCode;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement mappingDescription;
	@FindBy(how = How.NAME, using = "offer")
	WebElement offer;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addFilterButton;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Code')]")
	WebElement filterByCodeOffer;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement codeOffer;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement codeProduct;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement nameCharge;
	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/div[2]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/table[1]/tbody[1]")
	WebElement offerToSelect;
	@FindBy(how = How.NAME, using = "product")
	WebElement product;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Product Code')]")
	WebElement filterByProductCode;
	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/div[2]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/table[1]/tbody[1]")
	WebElement productToSelect;
	@FindBy(how = How.NAME, using = "charge")
	WebElement charge;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Name')]")
	WebElement filterByChargeName;
	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/div[2]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/table[1]/tbody[1]/tr[1]")
	WebElement chargeToSelect;
//	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSizeSmall MuiButton-sizeSmall']")
	WebElement goBackMappingButton;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement saveMappingButton;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbar-root MuiSnackbar-anchorOriginTopRight']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Delete']")
	WebElement delete;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Confirm']")
	WebElement confirmDelete;
	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/main[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/fieldset[1]/div[1]/form[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/span[1]/table[1]/tbody[1]/tr[1]/td[3]")
	WebElement valueCell;
	@FindBy(how = How.XPATH, using = "//input[@class='data-editor']")
	WebElement valueInput;

	
	// Defining all the user actions (Methods) that can be performed in the
	// Create article product or service mapping page
	
	
	// get message from textBox
	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

	// This method is to set mapping code in the code text box
	public void setMappingCode(String strCode) {
		mappingCode.sendKeys(strCode);
	}

	// This method is to set mapping description in the code text box
	public void setMappingDescription(String strDescription) {
		wait.until(ExpectedConditions.visibilityOf(mappingDescription));
		mappingDescription.sendKeys(strDescription);
	}

	public void setMappingDescriptionUpdated(String strDescription) {
		mappingDescription.clear();
		wait.until(ExpectedConditions.visibilityOf(mappingDescription));
		mappingDescription.sendKeys(strDescription);
	}

	// This method is to click offer label
	public void clickOffer() {
		offer.click();
	}

	// This method is to click add filter button
	public void addFilterButton() {
		addFilterButton.click();
	}

	// This method is to click add filter code Offer button
	public void addFilterByCodeOfferButton() {
		filterByCodeOffer.click();
	}

	// This method is to set Code Offer 
	public void setCodeOffer(String strCodeOffer) {
		codeOffer.sendKeys(strCodeOffer);
	}
	
	// This method is to set Code Offer Found 
	public void clickOnCodeOfferFound(String strCodeOfferFound) {
		WebElement elem = 
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//mark[contains(text(),'" + strCodeOfferFound + "')]")));
		elem.click();		
	}
	
	// This method is to select Offer 	
	public void selectOffer() {
		offerToSelect.click();
	}	

	// This method is to click Product label
	public void clickProduct() {
		wait.until(ExpectedConditions.visibilityOf(product));
		product.click();
	}

	// This method is to click add filter Code Product button
	public void addFilterByCodeProductButton() {
		filterByProductCode.click();
	}

	// This method is to set Code Product
	public void setCodeProduct(String strCodeProduct) {
		codeProduct.sendKeys(strCodeProduct);
	}

	// This method is to set Code Product Found 
	public void clickOnCodeProductFound(String strCodeProductFound) {
		WebElement elem = 
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//mark[contains(text(),'" + strCodeProductFound + "')]")));
		elem.click();
	}

	// This method is to select Product 	
	public void selectProduct() {
		productToSelect.click();
	}

	// This method is to click Charge label
	public void clickCharge() {
		fWait.until(ExpectedConditions.elementToBeClickable(charge));
		charge.click();
	}

	// This method is to click add filter Name Charge button
	public void addFilterByNameChargeButton() {
		filterByChargeName.click();
	}

	// This method is to set Name Charge 
	public void setNameCharge(String strNameCharge) {
		nameCharge.sendKeys(strNameCharge);
	}
	
	// This method is to set Name Charge 
	public void clickOnNameChargeFound(String strNameChargeFound) {
		WebElement elem = 
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//mark[contains(text(),'" + strNameChargeFound + "')]")));
		elem.click();
	}

	// This method is to select Charge 	
	public void selectCharge() {
		chargeToSelect.click();
	}

	// This method is to click on Save mapping page
	public void clickOnSaveMappingButton() {
		fWait.until(ExpectedConditions.visibilityOf(saveMappingButton));
		saveMappingButton.submit();
	}

	// This method is to click on GoBack mapping page 
	public void clickOnGoBackMappingButton() {
		wait.until(ExpectedConditions.elementToBeClickable(goBackMappingButton));
		goBackMappingButton.click();
	}

	// This method is to click on delete mapping page 
	public void clickOnDeleteButton() {
		wait.until(ExpectedConditions.visibilityOf(delete));
		delete.click();
	}

	// This method is to click on confirm delete mapping page 
	public void clickOnConfirmDeleteButton() {
		wait.until(ExpectedConditions.visibilityOf(confirmDelete));
		confirmDelete.click();
	}

	public void setAttributeMappingValue(String attributeValue) throws InterruptedException {	
		wait.until(ExpectedConditions.visibilityOf(valueCell));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('class','cell editing')", valueCell);
		wait.until(ExpectedConditions.elementToBeClickable(valueCell));
		valueCell.click();
		Actions act = new Actions(this.driver);
		act.doubleClick(valueCell).perform();
		Actions keyDown = new Actions(driver); 
		if(attributeValue.equals("Yes")) {
			keyDown.sendKeys(Keys.chord(Keys.DOWN, Keys.DOWN, Keys.ENTER)).perform();
		}
		else 
			keyDown.sendKeys(Keys.chord(Keys.DOWN, Keys.ENTER)).perform();
	}
	
	public void editAttributeValue() throws InterruptedException {
		// TODO Auto-generated method stub
/*		Thread.sleep(1000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('class','cell editing')", valueCell);
		Actions act = new Actions(this.driver);
		act.doubleClick(valueCell).perform();
		valueInput.sendKeys(attributeMappingValue);		
	//	valueInput.click();
		Thread.sleep(3000);
		act.sendKeys(Keys.ENTER);
		//act.sendKeys(Keys.ENTER).perform();
		
	*/	
		wait.until(ExpectedConditions.visibilityOf(valueCell));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('class','cell editing')", valueCell);
		wait.until(ExpectedConditions.elementToBeClickable(valueCell));
		valueCell.click();
		Actions act = new Actions(this.driver);
		act.doubleClick(valueCell).perform();
		//Thread.sleep(1000);
		Actions keyDown = new Actions(driver); 
		keyDown.sendKeys(Keys.chord(Keys.DOWN, Keys.ENTER)).perform();
		//act.sendKeys(Keys.chord(Keys.DOWN, Keys.DOWN, Keys.ENTER)).perform();
		//keyDown.sendKeys(Keys.ESCAPE).perform();
	
		
	}

	public String getMappingCodeInputText() {
		// TODO Auto-generated method stub
		return mappingCode.getAttribute("value");
	}

	//Verifying created article mapping line code
	public String getArticleMappingLineCodeInputText() {
		return mappingCode.getAttribute("value");
	}

	//Verifying created article mapping line description
	public String getArticleMappingLineDescriptionInputText() {
		return mappingDescription.getAttribute("value");
	}

	//Verifying charge name input text
	public String getChargeNameInputText() {
		return nameCharge.getAttribute("value");
	}

	//Verifying offer code input text
	public String getOfferCodeInputText() {
		return codeOffer.getAttribute("value");
	}

		
	public String getMappingDescriptionInputText() {

		// TODO Auto-generated method stub
		return mappingDescription.getAttribute("value");
		
	}


}
