package com.opencellsoft.pages.articles;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ListArticlesPage {
	WebDriver driver;
	WebDriverWait wait;

	public ListArticlesPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
	}

	@FindBy(how = How.XPATH, using = "//BUTTON[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-textSizeSmall MuiButton-sizeSmall']")
	WebElement createArticleButton;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement articleCodeInput;
	@FindBy(how = How.XPATH, using = "//table/tbody/tr[1]/td[1]")
	WebElement article;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Description')]")
	WebElement filterByDescription;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase description']")
	WebElement descriptionInput;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbar-root MuiSnackbar-anchorOriginTopRight']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//th[@data-testid='datagrid-column-code']//span[@title='Sort']")
	WebElement sortByCodeButton;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Export']")
	WebElement exportArticlesListButton;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Last']")
	WebElement latestArticlesViewed;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Cancel')]")
	WebElement cancel;

	// click on create article button
	public void clickOnCreateArticleButton() {
		wait.until(ExpectedConditions.elementToBeClickable(createArticleButton));
		createArticleButton.click();
	}

	public void setArticleCode(String articlecode) {
		// TODO Auto-generated method stub
		articleCodeInput.sendKeys(articlecode);
	}

	public void clickOnArticle(String strCodeArticleFound) {
		WebElement elem = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//mark[contains(text(),'" + strCodeArticleFound + "')]")));
		elem.click();
	}

	public void clickOnFilterButton() {
		// TODO Auto-generated method stub
		filterButton.click();
	}

	public void clickOnFilterOption() {
		// TODO Auto-generated method stub
		filterByDescription.click();
	}

	public void setFilter(String articledescription) {
		// TODO Auto-generated method stub
		wait.until(ExpectedConditions.visibilityOf(descriptionInput));
		descriptionInput.sendKeys(articledescription);
	}

	public String getTextMessage() {
		return messageTextBox.getText();
	}

	public void clikOnSortByCodeButton() {
		// TODO Auto-generated method stub
		sortByCodeButton.click();
	}

	public void clickOnExportArticlesListButton() {
		// TODO Auto-generated method stub
		exportArticlesListButton.click();
	}
	//This method is to click on Latest Articles Viewed
	public void clickOnLatestArticlesViewed() {
		latestArticlesViewed.click();
	}

	//This method is to click on Cancel Latest Articles Viewed
	public void clickOnCancelLatestArticlesViewedButton() {
		wait.until(ExpectedConditions.elementToBeClickable(cancel));
		cancel.click();
	}
}
