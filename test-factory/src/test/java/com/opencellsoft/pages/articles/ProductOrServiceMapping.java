package com.opencellsoft.pages.articles;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ProductOrServiceMapping {
	WebDriver driver;

	public ProductOrServiceMapping(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement mappingCode;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement mappingDescription;
	@FindBy(how = How.XPATH, using = "//input[@name='offer']")
	WebElement offerSelect;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addFilterBtn;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Code')]")
	WebElement filterByCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement filterByCodeInput;
	@FindBy(how = How.XPATH, using = "//tr[@class='MuiTableRow-root jss1139 jss1141 jss1140 MuiTableRow-hover']")
	WebElement offerToSelect;
	@FindBy(how = How.XPATH, using = "//input[@name='product']")
	WebElement productSelect;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Product Code')]")
	WebElement filterByProductCode;
	@FindBy(how = How.XPATH, using = "tbody tr:nth-child(1)")
	WebElement productToSelect;
	@FindBy(how = How.XPATH, using = "//input[@name='charge']")
	WebElement chargeSelect;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Name')]")
	WebElement filterByChargeName;
	@FindBy(how = How.XPATH, using = "//tr[@class='MuiTableRow-root jss1004 jss1006 jss1005 MuiTableRow-hover']")
	WebElement chargeToSelect;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackBtn;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement saveBtn;

	// set mapping code
	public void setMappingCode(String code) {
		mappingCode.sendKeys(code);
	}

//set  mapping description
	public void setMappingDescription(String description) {
		mappingDescription.sendKeys(description);
	}

//this mapping is to select an offer in mapping creation
	public void selectOffer(String offerCode) {
		offerSelect.click();
		addFilterBtn.click();
		filterByCode.click();
		filterByCodeInput.sendKeys(offerCode);
		offerToSelect.click();
	}

//this mapping is to select a product in mapping creation
	public void selectProduct(String productCode) {
		productSelect.click();
		addFilterBtn.click();
		filterByProductCode.click();
		filterByCodeInput.sendKeys(productCode);
		productToSelect.click();
	}

//this mapping is to select a charge in mapping creation
	public void selectCharge(String chargeName) {
		chargeSelect.click();
		addFilterBtn.click();
		filterByChargeName.click();
		filterByCodeInput.sendKeys(chargeName);
		chargeToSelect.click();
	}

// click on save the created mapping
	public void saveMapping() {
		saveBtn.click();
		goBackBtn.click();
		goBackBtn.click();
	}

// this method is to create a mapping
	public void createMapping(String mappingCode, String mappingDescription, String offerCode) {
		this.setMappingCode(mappingCode);
		this.setMappingDescription(mappingDescription);
		this.selectOffer(offerCode);
		this.saveMapping();

	}

}
