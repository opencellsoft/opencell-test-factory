package com.opencellsoft.pages.articles;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewArticlePage {
	WebDriver driver;
	WebDriverWait wait;

	public NewArticlePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement articleCode;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement articleDescription;
	@FindBy(how = How.XPATH, using = "//div[@id='invoiceSubCategory']")
	WebElement subCategoryList;
	@FindBy(how = How.XPATH, using = "//div[@id='taxClass']")
	WebElement taxClassList;
	@FindBy(how = How.XPATH, using = "//INPUT[@id='accountingCode']")
	WebElement accountingCodeList;	
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement saveArticleButton;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbar-root MuiSnackbar-anchorOriginTopRight']")
	WebElement messageTextBox;

	// Defining all the user actions (Methods) that can be performed in the
	// Create article home page
	// This method is to set Article code in the code text box
	public void setArticleCode(String strArticleCode) {
		articleCode.sendKeys(strArticleCode);
	}

	// This method is to set Article description in the code text box
	public void setArticleDescription(String strArticleDescription) {
		articleDescription.sendKeys(strArticleDescription);
	}

	// This method is to update Article description in the code text box
	public void setArticleDescriptionUpdated(String strArticleDescription) {
		wait.until(ExpectedConditions.visibilityOf(articleDescription));
		articleDescription.sendKeys(strArticleDescription);
	}

	// This method is to select Sub Category
	public void selectSubCategory(String subCategorySelected) {
		subCategoryList.click();
		subCategoryList.findElement(By.xpath("//li[contains(.,'" + subCategorySelected + "')]")).click();
	}

	// This method is to select Tax Class
	public void selectTaxClass(String taxClassSelected) {
		taxClassList.click();
		taxClassList.findElement(By.xpath("//li[contains(.,'" + taxClassSelected + "')]")).click();
	}

	// This method is to select Accounting Code
	public void selectAccountingCode(String accountingCode) {
		accountingCodeList.click();
		taxClassList.findElement(By.xpath("//SPAN[text()='" + accountingCode + "']")).click();
	}

	// This method is to click on save article button
	public void clickSaveButton() {
		saveArticleButton.submit();
	}

	// get the appeared msg after operation
	public String getTextMessage() {
		return messageTextBox.getText();
	}

	public String getArticleCodeInputText() {
		// TODO Auto-generated method stub
		return articleCode.getAttribute("value");

	}

	public String getArticleDescriptionInputText() {
		// TODO Auto-generated method stub
		return articleDescription.getAttribute("value");
	}

}
