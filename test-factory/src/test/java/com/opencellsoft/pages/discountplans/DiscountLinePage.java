package com.opencellsoft.pages.discountplans;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class DiscountLinePage {
	WebDriver driver;

	public DiscountLinePage(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement DiscountLineCodeI;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Save']")
	WebElement saveBtn;
	@FindBy(how=How.XPATH,using="//input[@id='priority']")
	WebElement priorityI; 
	@FindBy(how=How.XPATH,using="//span[normalize-space()='Percentage']")
	WebElement pencentage; 
	@FindBy(how=How.XPATH,using="//input[@id='discountValue']")
	WebElement discountValue; 
	@FindBy(how=How.XPATH,using="//span[normalize-space()='Flat amount']")
	WebElement flatAmount; 
	
	//set discount line code
	public void setDLCode(String code) {
		DiscountLineCodeI.sendKeys(code);
	}
	//set discount line priority
	public void setDLPriority() {
		priorityI.sendKeys("10");
	}
//click save button
	public void saveDiscountLine() {
		saveBtn.click();
	}
//this method is to set main discount line informations
	public void createDiscountLine(String code) {
		this.setDLCode(code);
		this.setDLPriority();
	}
	//set persentage
	public void setPercentage() {
		pencentage.click();
		discountValue.sendKeys("10");
	}
	//set flat amount
	public void setFlatAmount() {
		flatAmount.click();
		discountValue.sendKeys("150");
	}
}
