package com.opencellsoft.pages.discountplans;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class DiscountPlansListPage {
	WebDriver driver;

	public DiscountPlansListPage(WebDriver driver) {
		this.driver = driver;
	}
	
	@FindBy(how=How.XPATH, using="//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-textSizeSmall MuiButton-sizeSmall']")
	WebElement addDiscountPlanBtn;
	
	//click on create dicount plan button
	public void clickCreateNewDiscountPlan() {
		addDiscountPlanBtn.click();
	}
}
