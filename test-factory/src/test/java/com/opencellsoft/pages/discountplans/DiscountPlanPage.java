package com.opencellsoft.pages.discountplans;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

public class DiscountPlanPage {
	WebDriver driver;

	public DiscountPlanPage(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(how = How.XPATH, using = "//input[@id='label']")
	WebElement discountPlanLabel;
	@FindBy(how = How.XPATH, using = "//div[@id='discountPlanType']")
	WebElement discountPlanType;
	@FindBy(how = How.XPATH, using = "//input[@id='startDate']")
	WebElement startDateInput;
	@FindBy(how = How.XPATH, using = "//input[@id='endDate']")
	WebElement endDateInput;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day MuiPickersDay-current MuiPickersDay-daySelected']")
	WebElement startDate;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement updateBtn;
	@FindBy(how = How.XPATH, using = "//input[@id='applicationFilterEL']")
	WebElement applicationFilter;
	@FindBy(how = How.XPATH, using = "//div[@class='ButtonGroupGutters']//button[@type='button']")
	WebElement createBtn;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement submitButton;
	
	
	public void setDiscountPlanLabel(String DPLabel) {
		discountPlanLabel.sendKeys(DPLabel);
	}

	public void setDiscountPlanType(String discountType) {
		discountPlanType.click();
		new Select(discountPlanType).selectByVisibleText(discountType);
	}

	public void setStartDate() {
		startDateInput.click();
		startDate.click();
	}

	public void setEndDate() {
		endDateInput.click();
		startDate.click();
	}

	public void clickSaveBtn() {
		updateBtn.click();
	}

	// this method is to edit an existing discount plan's informations
	public void editDiscountPlan(String DPLabel) {
		this.setDiscountPlanLabel(DPLabel);
		this.setStartDate();
		this.setEndDate();
		this.setDiscountPlanType("promo code");
		this.clickSaveBtn();
	}

	public void setApplicationFilter( String filter) {
		applicationFilter.sendKeys(filter);
	}
//click create discount line
	public void clickCreate() {
		createBtn.click();
	}

// click save button
	public void clickOnSubmitButton() {
		submitButton.click();
	}
}
