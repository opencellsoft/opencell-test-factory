package com.opencellsoft.pages.discountplans;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

public class CreateDiscountPlanPage {
	WebDriver driver;

	public CreateDiscountPlanPage(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement discountPlanCode;
	@FindBy(how = How.XPATH, using = "//input[@id='label']")
	WebElement discountPlanLabel;
	@FindBy(how = How.NAME, using = "discountPlanType")
	WebElement discountPlanType;
	@FindBy(how = How.XPATH, using = "//input[@id='startDate']")
	WebElement startDateInput;
	@FindBy(how = How.XPATH, using = "//input[@id='endDate']")
	WebElement endDateInput;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day MuiPickersDay-current MuiPickersDay-daySelected']")
	WebElement startDate;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Save')]")
	WebElement saveBtn;

// set discount plan's code
	public void setDiscountPlanCode(String DPCode) {
		discountPlanCode.sendKeys(DPCode);
	}

	// set discount plan's label
	public void setDiscountPlanLabel(String DPLabel) {
		discountPlanLabel.sendKeys(DPLabel);
	}

	// set discount plan's type
	public void setDiscountPlanType(String discountType) {
		discountPlanType.click();
		new Select(discountPlanType).selectByVisibleText(discountType);
	}

	// set discount plan's start date
	public void setStartDate() {
		startDateInput.click();
		startDate.click();
	}

	// set discount plan's end date
	public void setEndDate() {
		endDateInput.click();
		startDate.click();
	}

	// click save btn
	public void clickSaveDiscountPlanBtn() {
		saveBtn.click();
	}

// this method is to create a discount plan
	public void createDiscountPlan(String DPCode, String DPLabel, String discountType) {
		this.setDiscountPlanCode(DPCode);
		this.setDiscountPlanLabel(DPLabel);
		this.setStartDate();
//		this.setEndDate();
		this.setDiscountPlanType(discountType);

	}
}