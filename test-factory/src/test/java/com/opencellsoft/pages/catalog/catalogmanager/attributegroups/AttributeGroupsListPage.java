package com.opencellsoft.pages.catalog.catalogmanager.attributegroups;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class AttributeGroupsListPage extends TestBase  {

	public AttributeGroupsListPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//*[@title = 'Create' or @id = 'CREATE' or @data-testid='AddIcon']")
	WebElement createButton;
	@FindBy(how = How.XPATH, using = "//input[@id = 'searchBar']")
	WebElement searchInput;
	@FindBy(how = How.XPATH, using = "//table[contains(@id, 'group-attribute')]//tbody//*[@role = 'row' or contains(@class ,'MuiTableRow-root')]")
	List<WebElement> attributeGroupsList;
	@FindBy(how = How.XPATH, using = "//button[@title = 'Filter' or @aria-label = 'Filter']")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//input[@id = 'emptyGroup']")
	WebElement emptyToggle;
	@FindBy(how = How.XPATH, using = "//input[@id = 'description']")
	WebElement searchByDescriptionInput;
	public void clickOnCreateButton() {
		clickOn(createButton);
	}
	
	public void setSearchInput(String value) {
		searchInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		searchInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public List<WebElement> attributeGroupsList() {
		return attributeGroupsList;
	}
	
	public void goToAttributeGroupsDetails(String value) {
		clickOn(driver.findElement(By.xpath("//*[contains(text() , '" + value + "')]")));
	}
	
	public void clickOnFilterButton() {
		clickOn(filterButton);
	}
	
	public void activateFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//li//*[text()= '" + value + "']")));
	}
	
	public void setEmptyInput(Boolean value) {
		clickOn(emptyToggle);
	}
	
	public void setSearchByDescriptionInput(String value) {
		searchByDescriptionInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		searchByDescriptionInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public void clickOnRemoveFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//button[@data-key = '" + value + "']")));
	}
	
	public void sortBy(String value) {
		clickOn(driver.findElement(By.xpath("//span[contains(@class, 'MuiButtonBase-root') and contains(@data-field, '" + value + "')]")));
	}
}
