package com.opencellsoft.pages.catalog.catalogmanager.attributes;

import java.time.Duration;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AttributePage {

	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public AttributePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(2)).ignoring(NoSuchElementException.class);
	}

	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement attributeDescription;
	@FindBy(how = How.ID, using = "allowedValues[0].value")
	WebElement firstAttributeValueInput;
	@FindBy(how = How.ID, using = "allowedValues[1].value")
	WebElement secondAttributeValueInput;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSizeSmall MuiButton-sizeSmall']")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;

	public String getAttributeDescriptionInputText() {
		try {
			fWait.until(ExpectedConditions.visibilityOf(attributeDescription));
			return attributeDescription.getAttribute("value");
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			fWait.until(ExpectedConditions.visibilityOf(attributeDescription));
			return attributeDescription.getAttribute("value");
		}
	}

	public String getFirstAttributeInputValue() {
		wait.until(ExpectedConditions.visibilityOf(firstAttributeValueInput));
		return firstAttributeValueInput.getAttribute("value");
	}

	public String getSecondAttributeInputValue() {
		wait.until(ExpectedConditions.visibilityOf(secondAttributeValueInput));
		return secondAttributeValueInput.getAttribute("value");
	}

	public void clickOnGoBackButton() {
		// TODO Auto-generated method stub
		wait.until(ExpectedConditions.elementToBeClickable(goBackButton));
		goBackButton.click();
	}

	public String getTextMessage() {
		// TODO Auto-generated method stub
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

}
