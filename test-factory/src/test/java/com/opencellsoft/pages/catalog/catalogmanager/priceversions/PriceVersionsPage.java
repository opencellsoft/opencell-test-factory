package com.opencellsoft.pages.catalog.catalogmanager.priceversions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class PriceVersionsPage extends TestBase {

	public PriceVersionsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//input[contains(@id, 'status')]")
	WebElement statusSelectBox;
	@FindBy(how = How.XPATH, using = "(//table//thead//input[@type = 'checkbox'])[1]")
	WebElement selectAllCheckBox;
	@FindBy(how = How.XPATH, using = "//*[text()= 'Add']")
	WebElement addButton;
	@FindBy(how = How.XPATH, using = "//form[@class = 'ExportMassAction']//*[text() = 'Export']")
	WebElement exportButton;
	@FindBy(how = How.XPATH, using = "//div[@class = 'dropzone']//input[@type = 'file']")
	WebElement uploadFile;
	@FindBy(how = How.XPATH, using = "(//input[contains(@id , 'PV55_-_DRAFT.startDate') or contains(@id , 'mui')])[1]")
	WebElement startDateInput;
	@FindBy(how = How.XPATH, using = "(//input[@placeholder = 'YYYY-MM-DD'])[3]")
	WebElement startDateInput1;
	@FindBy(how = How.XPATH, using = "//button[@type = 'submit']")
	WebElement validateButton;
	@FindBy(how = How.XPATH, using = "//div[@role = 'presentation' or contains(@class, 'MuiSnackbar-root')]//div[contains(@class , 'MuiSnackbarContent-message') or contains(@class , 'MuiAlert-message')]")
	WebElement executionMessage;
	public void selectStatus(String value,String value2) {
		Actions actions = new Actions(driver);
		actions.moveToElement(statusSelectBox).perform();
		waitPageLoaded();
		clickOn(statusSelectBox);
		clickOn(driver.findElement(By.xpath("//div[@role = 'tooltip']//*[text() = '" + value + "' or text() = '" + value2 + "']")));
	}

	public void clickOnSelectAllCheckBox() {
		clickOn(selectAllCheckBox);
	}

	public void clickOnAddButton() {
		clickOn(addButton);
	}

	public void clickOnExportButton() {
		clickOn(exportButton);
	}

	public void clickOnUploadFileButton() {
		String projectPath = System.getProperty("user.dir");
		uploadFile.sendKeys(projectPath+"\\data\\priceversions.zip");
		waitPageLoaded();
		waitPageLoaded();
	}

	public void setStartDate(int day) {
		try {
			clickOn(startDateInput);
		}catch (Exception e) {
			waitPageLoaded();
			clickOn(startDateInput1);
		}
		
		try {
			clickOn(driver.findElement(By.xpath("//div[@role = 'dialog']//*[text() = '" + day + "']")));
		}catch (Exception e) {
			waitPageLoaded();
			clickOn(driver.findElement(By.xpath("//div[@role = 'dialog']//*[text() = '" + day + "']")));
		}
		
	}

	public void clickOnValidateButton() {
		clickOn(validateButton);
	}

	public String getExecutionMessage() {
		return executionMessage.getText();
	}
}
