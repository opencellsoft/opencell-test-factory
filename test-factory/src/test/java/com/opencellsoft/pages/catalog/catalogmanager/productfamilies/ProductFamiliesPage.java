package com.opencellsoft.pages.catalog.catalogmanager.productfamilies;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class ProductFamiliesPage extends TestBase {

	public ProductFamiliesPage () {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement productFamilyCode;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement productFamilyLabel;
	@FindBy(how = How.XPATH, using = "//textarea[@id='longDescription']")
	WebElement productFamilyDescription;
	@FindBy(how = How.ID, using = "seller.code")
	WebElement sellersList;
	@FindBy(how = How.XPATH, using = "//ul[@aria-labelledby = 'seller.code-label']//li[2]")
	WebElement firstChoice;
	@FindBy(how = How.XPATH, using = "//div[@id='parentLine.code']")
	WebElement parentProductFamilyList;
	@FindBy(how = How.CSS, using = "li:nth-child(6)")
	WebElement parentProductFamilySelected;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Save') or @type='submit']")
	WebElement submitButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;	
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Delete')]")
	WebElement delete;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//*[contains(text(),'Confirm')]")
	WebElement confirmDelete;
	
	public void setCode(String pf_code) {
		productFamilyCode.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		productFamilyCode.sendKeys(pf_code);
		waitPageLoaded();
	}
	
	// This method is to set product family Label
	public void setName(String pf_name) {
		productFamilyLabel.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		productFamilyLabel.sendKeys(pf_name);
		waitPageLoaded();
	}

	// This method is to set product family description
	public void setDescription(String pf_description) {
		productFamilyDescription.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		productFamilyDescription.sendKeys(pf_description);
		waitPageLoaded();
	}
	
	// This method is to select seller
	public void selectSeller(String selectSeller) {
		clickOn(sellersList);
		if(selectSeller != null) {
			sellersList.findElement(By.xpath("//li[contains(.,'" + selectSeller + "')]")).click();
		}else {
			clickOn(firstChoice);
		}
	}

	// This method is to select parent product family
	public void selectParent(String pf_parent) {
		if(pf_parent != null) {
			clickOn(parentProductFamilyList);
			clickOn(driver.findElement(By.xpath("//li[contains(@data-value ,'" + pf_parent + "')]")));
		}
	}

	// This method is to click on submit button
	public void clickOnSaveButton() {
		clickOn(submitButton);
	}

	// This method is to click on go back button
	public void clickOnGoBackButton() {
		clickOn(goBackButton);
	}
	// This method is to click on delete button
	public void clickOnDeleteButton() {
		clickOn(delete);
	}

	// This method is to click on confirm delete
	public void clickOnConfirmButton() {
		clickOn(confirmDelete);
	}
}
