package com.opencellsoft.pages.catalog.catalogmanager.media;

import java.time.Duration;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewMediaPage {

	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public NewMediaPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(90)).ignoring(NoSuchElementException.class);
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//div[@id='mimeType']")
	WebElement mediaTypeList;
	@FindBy(how = How.CSS, using = "li:nth-child(1)")
	WebElement mediaTypeImage;
	@FindBy(how = How.CSS, using = "li:nth-child(2)")
	WebElement mediaTypeVideo;
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement mediaCode;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement mediaDescription;
	@FindBy(how = How.XPATH, using = "//input[@id='mediaPath']")
	WebElement pathURL;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Save')]")
	WebElement submitButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;

	// This method is to select media type image
	public void selectMediaTypeImage() {
		Actions act = new Actions(driver);
		act.moveToElement(mediaTypeList).click().perform();
		act.moveToElement(mediaTypeImage).click().perform();
	}
	
	// This method is to select media type video
	public void selectMediaTypeVideo() {
		Actions act = new Actions(driver);
		act.moveToElement(mediaTypeList).click().perform();
		act.moveToElement(mediaTypeVideo).click().perform();
	}

	// This method is to set media code
	public void setMediaCode(String strMediaCode) {
		mediaCode.sendKeys(strMediaCode);
	}

	// This method is to set media description
	public void setMediaDescription(String strMediaDescription) {
		mediaDescription.sendKeys(strMediaDescription);
	}

	// This method is to set URL of image
	public void setURLPath(String strMediaURL) {
		pathURL.sendKeys(strMediaURL);
	}

	// This method is to click on submit button
	public void clickOnSubmitButton() {
		submitButton.submit();
	}

	// This method is to click on go back button
	public void clickOnGoBackButton() {
		fWait.until(ExpectedConditions.elementToBeClickable(goBackButton));
		goBackButton.click();
	}

	// This method is to set all media informations and click submit button
	public void setMediaInformations(String mediaCode, String mediaDescription, String mediaURL) {
		this.setMediaCode(mediaCode);
		this.setMediaDescription(mediaDescription);
		this.setURLPath(mediaURL);
		this.clickOnSubmitButton();
	}
}
