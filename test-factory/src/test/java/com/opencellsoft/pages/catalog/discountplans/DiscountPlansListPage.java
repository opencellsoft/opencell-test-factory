package com.opencellsoft.pages.catalog.discountplans;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class DiscountPlansListPage extends TestBase {

	WebDriverWait wait;
	public DiscountPlansListPage() {
		PageFactory.initElements(driver, this);
		this.wait = new WebDriverWait(driver,Duration.ofSeconds(10));
	}
	
	@FindBy(how=How.XPATH, using = "//*[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-textSizeSmall MuiButton-sizeSmall' or @data-testid='AddIcon']")
	WebElement createDiscountPlanButton;
	@FindBy(how = How.XPATH, using = "//*[@aria-label='Add filter']")
	WebElement addFilterButton;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Code')]")
	WebElement filterByDiscountPlanOption;
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement filterByCodeInput;
	@FindBy(how = How.XPATH, using = "//input[@id='searchBar']")
	WebElement searchInput;
	@FindBy(how = How.XPATH, using = "//tbody//tr[1]")
	WebElement foundDiscountPlan;
	@FindBy(how = How.XPATH, using = "//table[contains(@id, 'discount-plans')]//tbody//*[@role = 'row' or contains(@class ,'MuiTableRow-root')]")
	List<WebElement> discountPlansList;
	@FindBy(how = How.XPATH, using = "//*[@title = 'Filter' or @aria-label='Filter']")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//div[@id = 'discountPlanType']")
	WebElement typeInput;
	
	//click on create discount plan button
	public void clickOnCreateDiscountPlanButton() {
		clickOn(createDiscountPlanButton);
	}

	//this method is to add Discount Plans filter button
	public void clickOnAddDiscountPlanFilterButton() {
		addFilterButton.click();
	}

	// this method is to click on add Discount Plans Code filter button
	public void clickOnFilterByCodeOption() {
		filterByDiscountPlanOption.click();
	}
	
	//this method is set Discount Plans code input 
	public void setDiscountPlanCode(String discountPlansCode) {
		filterByCodeInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		filterByCodeInput.sendKeys(discountPlansCode);
		waitPageLoaded();
	}
	
	public void setSearchInput(String value) {
		searchInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		searchInput.sendKeys(value);
		waitPageLoaded();
	}
	
	//this method is to select customer
	public void selectdiscountPlans() {
		clickOn(foundDiscountPlan);
	}
	
	public void clickOnFoundDiscountPlan(String foundDiscountPlanCode) {
		// TODO Auto-generated method stub
		wait.until(ExpectedConditions.visibilityOf(searchInput));
		searchInput.findElement(By.xpath("//mark[normalize-space(),'"+ foundDiscountPlanCode + "')]")).click();		
	}
	
	public List<WebElement> discountPlansList() {
		return discountPlansList;
	}
	
	public void sortBy(String value) {
		try {
			clickOn(driver.findElement(By.xpath("//span[contains(@class, 'MuiButtonBase-root') and contains(@data-field, '" + value + "')]")));
		}catch (Exception e) { 
			clickOn(driver.findElement(By.xpath("//th[contains(@sortby, '" + value + "')]")));
		}
		
	}
	
	public void goToDiscountPlanDetails(String value) {
		clickOn(driver.findElement(By.xpath("//*[contains(text() , '" + value + "')]")));
	}

	public void clickOnFilterButton() {
		clickOn(filterButton);
	}
	
	public void activateFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//li//*[text()= '" + value + "']")));
	}
	
	public void clickOnTypeInput() {
		clickOn(typeInput);
	}

	public void filterBy(String value) {
		clickOn(driver.findElement(By.xpath("//li[text()= '" + value + "']")));
	}
	
	public void clickOnRemoveFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//button[@data-key = '" + value + "']")));
	}
}
