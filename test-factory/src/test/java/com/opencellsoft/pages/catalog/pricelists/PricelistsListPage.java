package com.opencellsoft.pages.catalog.pricelists;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class PricelistsListPage  extends TestBase  {

	public PricelistsListPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//*[contains(@title , 'create') or @id = 'CREATE' or @data-testid='AddIcon']")
	WebElement createButton;
	@FindBy(how = How.XPATH, using = "//input[contains(@id , 'searchBar')]")
	WebElement searchInput;
	@FindBy(how = How.XPATH, using = "//table[contains(@id, 'price-lists')]//tbody//*[@role = 'row' or contains(@class ,'MuiTableRow-root')]")
	List<WebElement> priceListsList;
	@FindBy(how = How.XPATH, using = "//button[@title = 'Filter' or @aria-label='Filter']")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//div[contains(@aria-labelledby , 'status')]")
	WebElement filterByStatusInput;
	
	public void clickOnCreateButton() {
		clickOn(createButton);
	}
	
	public void setSearchInput(String value) {
		searchInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		searchInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public List<WebElement> PriceListsList() {
		return priceListsList;
	}
	
	public List<WebElement> PriceListsInStatus(String value) {
		List<WebElement> elements = driver.findElements(By.xpath("//td[contains(@class, 'status')]//span[text() = '" + value + "']"));
		return elements;
	}
	
	public void goToPriceListDetails(String value) {
		clickOn(driver.findElement(By.xpath("//*[contains(text() , '" + value + "')]")));
	}
	
	public void clickOnFilterButton() {
		clickOn(filterButton);
	}
	
	public void activateFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//li//*[text()= '" + value + "']")));
	}
	
	public void filterByStatus(String value) {
		clickOn(filterByStatusInput);
		clickOn(driver.findElement(By.xpath("//li[text()= '" + value + "']")));
	}
	
	public void clickOnRemoveFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//button[@data-key = '" + value + "']")));
	}
	
	public void sortBy(String value) {
		clickOn(driver.findElement(By.xpath("//span[contains(@class, 'MuiButtonBase-root') and contains(@data-field, '" + value + "')]")));
	}

}
