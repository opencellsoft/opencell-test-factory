package com.opencellsoft.pages.catalog.catalogueManager.referenceTables;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class CustomTablesAddRows extends TestBase {
	
	public CustomTablesAddRows() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Add rows')]")
	WebElement addRowsButton;
	@FindBy(how = How.XPATH, using = "//input[contains(@id, 'annee')]")
	WebElement yearInput;
	@FindBy(how = How.XPATH, using = "//input[contains(@id, 'ville')]")
	WebElement cityInput;
	@FindBy(how = How.XPATH, using = "//input[contains(@id, 'pays')]")
	WebElement countryInput;
	@FindBy(how = How.XPATH, using = "//input[contains(@id, 'habitant')]")
	WebElement residentInput;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Save']")
	WebElement saveButton;
	
	Actions actions = new Actions(driver);
	
	public void clickOnAddRowsButton() {
		clickOn(addRowsButton);
	}
	
	public void doubleClickOnYear(){
		WebElement year = driver.findElement(By.xpath("(//*[@data-field = 'annee'])[2]"));
		actions.doubleClick(year).click().perform();
		waitPageLoaded();
	}
	public void setYearInput(String year) {
		yearInput.sendKeys(year);
		waitPageLoaded();
		yearInput.sendKeys(Keys.RETURN);
		waitPageLoaded();
	}
	public void doubleClickOnCity(){
		WebElement city = driver.findElement(By.xpath("(//*[@data-field = 'ville'])[2]"));
		actions.doubleClick(city).click().perform();
		waitPageLoaded();
	}
	public void setCityInput(String city) {
		cityInput.sendKeys(city);
		waitPageLoaded();
		cityInput.sendKeys(Keys.RETURN);
		waitPageLoaded();
	}	
	public void doubleClickOnCountry(){
		WebElement country = driver.findElement(By.xpath("(//*[@data-field = 'pays'])[2]"));
		actions.doubleClick(country).click().perform();
		waitPageLoaded();
	}
	public void setCountryInput(String country) {
		countryInput.sendKeys(country);
		waitPageLoaded();
		countryInput.sendKeys(Keys.RETURN);
		waitPageLoaded();
	}		
	public void doubleClickOnResident(){
		WebElement resident = driver.findElement(By.xpath("(//*[@data-field = 'habitant'])[2]"));
		actions.doubleClick(resident).click().perform();
		waitPageLoaded();
	}
	public void setResidentInput(String resident) {
		residentInput.sendKeys(resident);
		waitPageLoaded();
		residentInput.sendKeys(Keys.RETURN);
		waitPageLoaded();
	}
	
	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
}
