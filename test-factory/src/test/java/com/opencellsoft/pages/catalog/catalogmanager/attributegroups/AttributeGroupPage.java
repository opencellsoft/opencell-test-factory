package com.opencellsoft.pages.catalog.catalogmanager.attributegroups;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class AttributeGroupPage extends TestBase {

	public AttributeGroupPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//input[@id = 'code']")
	WebElement codeInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'description']")
	WebElement descriptionInput;
	@FindBy(how = How.XPATH, using = "//*[@id = 'attributes']")
	WebElement attributesInput;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//input[@id = 'searchBar']")
	WebElement searchByAttributeNameInput;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//table//tbody//tr[1]//td[2]")
	WebElement firstResult;
	@FindBy(how = How.XPATH, using = "//button[@type = 'submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//span[text() = 'Go Back']")
	WebElement goBackButton;
	
	public void setCode(String code) {
		codeInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		codeInput.sendKeys(code);
		waitPageLoaded();
	}
	
	public void setDescription(String description) {
		descriptionInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		descriptionInput.sendKeys(description);
		waitPageLoaded();
	}
	
	public void setAttributes(String attribute) {
		clickOn(attributesInput);
		searchByAttributeNameInput.sendKeys(attribute);
		waitPageLoaded();
		clickOn(firstResult);
	}
	
	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
	
	public void clickOnGoBackButton() {
		clickOn(goBackButton);
	}
}
