package com.opencellsoft.pages.catalog.discountplans;

import java.time.Duration;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewDiscountLinePage {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public NewDiscountLinePage(WebDriver driver) {
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10)).pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
		this.driver = driver;
	}

	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement DiscountLineCodeInput;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Save']")
	WebElement saveDiscountLineButton;
	@FindBy(how=How.XPATH,using="//input[@id='priority']")
	WebElement priorityInput; 
	@FindBy(how=How.XPATH,using="//span[normalize-space()='Percentage']")
	WebElement pencentage; 
	@FindBy(how=How.XPATH,using="//input[@id='discountValue']")
	WebElement discountValue; 
	@FindBy(how=How.XPATH,using="//span[normalize-space()='Flat amount']")
	WebElement flatAmountOption; 
	@FindBy(how=How.XPATH,using="//input[@id='discountPlanItemType_PERCENTAGE']")
	WebElement prcentageOption; 
	@FindBy(how=How.XPATH,using="/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/main[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/fieldset[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/input[1]")
	WebElement discountArticle; 
	@FindBy(how=How.XPATH,using="//input[@name='article']")
	WebElement discountArticleInput; 
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement filterdiscountArticle;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Code')]")
	WebElement discountArticleCodeFilter;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement discountArticleCodeInput;
	@FindBy(how = How.XPATH, using = "//tbody//tr[1]")
	WebElement selectdiscountArticle;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbar-root MuiSnackbar-anchorOriginTopRight']")
	WebElement messageTextBox;
	

	
	//set discount line code
	public void setDiscountLineCode(String code) {
		DiscountLineCodeInput.sendKeys(code);
	}
	//set discount line priority
	public void setDiscountPlanPriority(String priority) {
		priorityInput.sendKeys(priority);
	}
//click save button
	public void clickOnSaveDiscountLineButton() {
		wait.until(ExpectedConditions.elementToBeClickable(saveDiscountLineButton));
		saveDiscountLineButton.click();
	}
//this method is to set main discount line informations
	public void createDiscountLine(String code) {
		this.setDiscountLineCode(code);
	}
	//set discount percentage
	public void setPercentage(String percentage) {
		discountValue.sendKeys(percentage);
	}
	public void clickOnFlatAmountOption() {
		flatAmountOption.click();
	}
	//set flat amount
	public void setFlatAmountValue(String discountValue) throws InterruptedException {
		Thread.sleep(1000);
		this.discountValue.sendKeys(discountValue);
	}

	//This method is to click discount article
	public void clickDiscountArticle() {
		discountArticle.click();
		discountArticleInput.click();
	}

	//this method is to add Discount Article filter button
	public void clickOnAddDiscountArticleFilterButton() {
		filterdiscountArticle.click();
	}

	// this method is to click on add Discount Article Code filter button
	public void clickOnDiscountArticleCodeFilterButton() {
		discountArticleCodeFilter.click();
	}
	
	//this method is set Discount Article code input 
	public void searchForDiscountArticleCodeInput(String strdiscountArticleCode) {
		discountArticleCodeInput.sendKeys(strdiscountArticleCode);
	}
	
	//this method is to select Discount Article
	public void selectDiscountArticle() {
		wait.until(ExpectedConditions.visibilityOf(selectdiscountArticle));		
		selectdiscountArticle.click();
	}

	//Verifying discount line code input text
	public String getDiscountLineCodeInputText() {
		return DiscountLineCodeInput.getAttribute("value");
	}

	//Verifying article code input text
	public String getArticleCodeInputText() {
		return discountArticleCodeInput.getAttribute("value");
	}

	// get the appeared msg after operation
	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}
	public void clickOnPercentageOption() {
		// TODO Auto-generated method stub
		prcentageOption.click();
	}
	

}
