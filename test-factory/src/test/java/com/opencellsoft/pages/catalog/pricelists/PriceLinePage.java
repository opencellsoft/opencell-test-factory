package com.opencellsoft.pages.catalog.pricelists;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class PriceLinePage extends TestBase {

	public PriceLinePage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//input[@id = 'code']")
	WebElement codeInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'description']")
	WebElement descriptionInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'charge']")
	WebElement chargeInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'surcharge']")
	WebElement priceInput;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//input[contains(@id , 'searchBar')]")
	WebElement searchByNameInput;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//table//tbody//tr[1]//td[2]")
	WebElement firstResult;
	@FindBy(how = How.XPATH, using = "//button[@type = 'submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//span[text() = 'Go Back']")
	WebElement goBackButton;
	public void setPricelineCode(String code) {
		codeInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		codeInput.sendKeys(code);
		waitPageLoaded();
	}
	
	public void setPricelineDescription(String description) {
		descriptionInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		descriptionInput.sendKeys(description);
		waitPageLoaded();
	}
	
	public void setPricelineCharge(String chargeName) {
		clickOn(chargeInput);
		setSearchByChargeName(chargeName);
		waitPageLoaded();
		clickOn(firstResult);
	}
	
	public void setSearchByChargeName(String chargeName) {
		searchByNameInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		searchByNameInput.sendKeys(chargeName);
		waitPageLoaded();
	}
	
	public void setPriceLinePrice(String price) {
		priceInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		priceInput.sendKeys(price);
		waitPageLoaded();
	}
	
	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
}
