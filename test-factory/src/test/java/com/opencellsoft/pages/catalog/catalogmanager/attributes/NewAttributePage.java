package com.opencellsoft.pages.catalog.catalogmanager.attributes;

import java.awt.AWTException;
import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class NewAttributePage extends TestBase {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public NewAttributePage(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		this.fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);

	}

	// Using FindBy for locating elements
	@FindBy(how = How.ID, using = "attributeType")
	WebElement attributeTypesList;
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement attributeCode;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement attributeDescription;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add']")
	WebElement addAttributeValueButton;
	@FindBy(how = How.ID, using = "allowedValues[0].value")
	WebElement firstAttributeValueInput;
	@FindBy(how = How.ID, using = "allowedValues[1].value")
	WebElement secondAttributeValueInput;
	@FindBy(how = How.XPATH, using = "//*[normalize-space()='Save']")
	WebElement submitButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "//input[@id='decimalNumber']")
	WebElement decimalsNumberInput;
	@FindBy(how = How.XPATH, using = "//button[contains(@aria-label, 'Add') or contains(@title, 'Add')]")
	WebElement addValueButton;
	@FindBy(how = How.XPATH, using = "//input[contains(@aria-label, 'Title') or contains(@title, 'Title') or contains(@aria-labelledby,'allowedValues') or contains(@id,'allowedValues')]")
	List<WebElement> titleInput;
	// This method is to set select attribute type
	public void selectAttributeType(String attributeType) throws AWTException {
		wait.until(ExpectedConditions.elementToBeClickable(attributeTypesList));
		this.attributeTypesList.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		WebElement element;
		try {
			element = driver.findElement(By.xpath("//li[contains(text(),'" + attributeType + "')]"));

		}catch (Exception e) {
			element = driver.findElement(By.xpath("//span[contains(text(),'" + attributeType + "')]"));// TODO: handle exception
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.click();
	}
	
	public void selectAttributeType2(String attributeType) throws AWTException {
		wait.until(ExpectedConditions.elementToBeClickable(attributeTypesList));
		clickOn(attributeTypesList);
		WebElement element = driver.findElement(By.xpath("//ul//*[text() = '" + attributeType + "']"));
		wait.until(ExpectedConditions.elementToBeClickable(element));
		clickOn(element);
	}

	// This method is to set attribute Code in the code text box
	public void setAttributeCode(String attributeCode) {
		wait.until(ExpectedConditions.visibilityOf(this.attributeCode));
		this.attributeCode.sendKeys(attributeCode);
	}

	// This method is to set attribute description in the description text box
	public void setAttributeDescription(String attributeDescription) {
		this.attributeDescription.sendKeys(attributeDescription);
	}

	// This method is to click add button
	public void clickOnAddAttributeValuesButton() {
		wait.until(ExpectedConditions.elementToBeClickable(addAttributeValueButton));
		addAttributeValueButton.click();
	}

	// This method is to set the first value
	public void setFirstAttributeValue(String firstValue) {
		wait.until(ExpectedConditions.visibilityOf(firstAttributeValueInput));
		firstAttributeValueInput.sendKeys(firstValue);
	}

	// This method is to set the second value
	public void setSecondAttributeValue(String secondValue) throws AWTException {
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", secondAttributeValueInput);
		wait.until(ExpectedConditions.visibilityOf(secondAttributeValueInput));
		secondAttributeValueInput.sendKeys(secondValue);

	}

	// This method is to click on save button
	public void clickOnSaveAttributeButton() {
		wait.until(ExpectedConditions.visibilityOf(submitButton));
		submitButton.submit();
	}

	// This method is to click on go back button
	public void clickOnGoBackButton() {
		//JavascriptExecutor js = (JavascriptExecutor)driver;
		//js.executeScript("arguments[0].scrollIntoView();", goBackButton); 
		Actions act = new Actions(this.driver);
		act.sendKeys(Keys.chord(Keys.PAGE_UP)).perform();
		wait.until(ExpectedConditions.elementToBeClickable(goBackButton));
		clickOn(goBackButton);
	}

	public void setNumberOfDecimals(String numberOfDecimals) {
		wait.until(ExpectedConditions.visibilityOf(decimalsNumberInput));
		decimalsNumberInput.sendKeys(numberOfDecimals);
	}
	
	public void clickOnAddValue() {
		clickOn(addValueButton);
	}
	
	public void setValue(int index, String value) {
		titleInput.get(index).sendKeys(value);
		waitPageLoaded();
	}
	
	
}
