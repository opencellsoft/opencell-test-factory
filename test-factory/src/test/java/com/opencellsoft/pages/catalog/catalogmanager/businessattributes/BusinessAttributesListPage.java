package com.opencellsoft.pages.catalog.catalogmanager.businessattributes;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class BusinessAttributesListPage extends TestBase  {

	public BusinessAttributesListPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//input[@id = 'searchBar']")
	WebElement searchInput;
	@FindBy(how = How.XPATH, using = "//input[@title = 'Value' or contains(@name , 'Value')]")
	WebElement valueInput;
	@FindBy(how = How.XPATH, using = "//table[contains(@id, 'business-attributes')]//tbody//*[@role = 'row' or contains(@class ,'MuiTableRow-root')]")
	List<WebElement> businessAttributesList;
	@FindBy(how = How.XPATH, using = "//button[@title = 'Filter' or @aria-label = 'Filter']")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//table//th//input")
	WebElement checkAllCheckBox;
	@FindBy(how = How.XPATH, using = "//button[@title = 'Delete' or @aria-label = 'Delete']")
	WebElement deleteButton;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//*[text() = 'Confirm']")
	WebElement confirmButton;
	
	public void setSearchInput(String value) {
		searchInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		searchInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public List<WebElement> businessAttributesList() {
		return businessAttributesList;
	}
	
	public void clickOnFilterButton() {
		clickOn(filterButton);
	}
	
	public void activateFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//li//*[text()= '" + value + "']")));
	}
	
	public void clickOnCheckAllCheckBox() {
		clickOn(checkAllCheckBox);
	}
	
	public void clickOnDeleteButton() {
		clickOn(deleteButton);
	}
	
	public void clickOnConfirmButton() {
		clickOn(confirmButton);
	}
	
	public void setValueInput(String value) {
		valueInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		valueInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public void clickOnRemoveFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//button[@data-key = '" + value + "']")));
	}
	
	public void sortBy(String value) {
		clickOn(driver.findElement(By.xpath("//span[contains(@class, 'MuiButtonBase-root') and contains(@data-field, '" + value + "')]")));
	}
}
