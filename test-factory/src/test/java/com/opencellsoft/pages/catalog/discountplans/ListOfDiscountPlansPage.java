package com.opencellsoft.pages.catalog.discountplans;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ListOfDiscountPlansPage {
	WebDriver driver;
	WebDriverWait wait;

	public ListOfDiscountPlansPage(WebDriver driver) {
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		this.driver = driver;
	}
	
	@FindBy(how=How.XPATH, using="//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-textSizeSmall MuiButton-sizeSmall']")
	WebElement createDiscountPlanButton;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addFilterButton;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Code')]")
	WebElement filterByDiscountPlanOption;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement discountPlanCodeInput;
	@FindBy(how = How.XPATH, using = "//tbody//tr[1]")
	WebElement foundDiscountPlan;

	
	//click on create discount plan button
	public void clickOnCreateDiscountPlanButton() {
		createDiscountPlanButton.click();
	}

	//this method is to add Discount Plans filter button
	public void clickOnAddDiscountPlanFilterButton() {
		addFilterButton.click();
	}

	// this method is to click on add Discount Plans Code filter button
	public void clickOnFilterByCodeOption() {
		filterByDiscountPlanOption.click();
	}
	
	//this method is set Discount Plans code input 
	public void setDiscountPlanCode(String discountPlansCode) {
		discountPlanCodeInput.sendKeys(discountPlansCode);
	}
	
	//this method is to select customer
	public void selectdiscountPlans() {
		wait.until(ExpectedConditions.visibilityOf(foundDiscountPlan));		
		foundDiscountPlan.click();
	}
	
	public void clickOnFoundDiscountPlan(String foundDiscountPlanCode) {
		// TODO Auto-generated method stub
		wait.until(ExpectedConditions.visibilityOf(discountPlanCodeInput));
		discountPlanCodeInput.findElement(By.xpath("//mark[normalize-space(),'"+ foundDiscountPlanCode + "')]")).click();		
	}
	

}
