package com.opencellsoft.pages.catalog.discountplans;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import com.opencellsoft.base.TestBase;

public class DiscountLinePage extends TestBase{



	public DiscountLinePage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//input[@id = 'description']")
	WebElement labelInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'discountValue']")
	WebElement discountvalueInput;
	@FindBy(how = How.XPATH, using = "//button[@type = 'submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//span[text() = 'Go Back']")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "//span[text() = 'Delete']")
	WebElement deleteButton;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//*[text() = 'Confirm']")
	WebElement confirmButton;
	
	public void setLabel(String label) {
		labelInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		labelInput.sendKeys(label);
		waitPageLoaded();
	}
	
	public void setDiscountValue(String discountvalue) {
		discountvalueInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		discountvalueInput.sendKeys(discountvalue);
		waitPageLoaded();
	}
	
	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
	
	public void clickOnGoBackButton() {
		clickOn(goBackButton);
	}
	
	public void clickOnDeleteButton() {
		clickOn(deleteButton);
	}
	public void clickOnConfirmButton() {
		clickOn(confirmButton);
	}
}
