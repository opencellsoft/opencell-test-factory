package com.opencellsoft.pages.catalog.catalogmanager.media;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class MediaPage extends TestBase {

	public MediaPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//div[@id = 'mimeType']")
	WebElement typeInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'code']")
	WebElement codeInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'description']")
	WebElement descriptionInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'mediaPath']")
	WebElement urlPathInput;
	@FindBy(how = How.XPATH, using = "//button[@type = 'submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//span[text() = 'Go Back']")
	WebElement goBackButton;
	
	public void setType(String type) {
		clickOn(typeInput);
		clickOn(driver.findElement(By.xpath("//ul//*[text()= '" + type + "']")));
	}
	
	public void setCode(String code) {
		codeInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		codeInput.sendKeys(code);
		waitPageLoaded();
	}
	
	public void setDescription(String description) {
		descriptionInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		descriptionInput.sendKeys(description);
		waitPageLoaded();
	}
	
	public void setUrlPath(String urlPath) {
		urlPathInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		urlPathInput.sendKeys(urlPath);
		waitPageLoaded();
	}
	
	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
	
	public void clickOnGoBackButton() {
		clickOn(goBackButton);
	}
}
