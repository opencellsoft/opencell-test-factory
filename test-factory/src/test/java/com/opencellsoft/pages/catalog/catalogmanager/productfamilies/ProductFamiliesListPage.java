package com.opencellsoft.pages.catalog.catalogmanager.productfamilies;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class ProductFamiliesListPage extends TestBase {

	public ProductFamiliesListPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//*[@title = 'Create' or @id = 'CREATE' or @data-testid='AddIcon']")
	WebElement createButton;
	@FindBy(how = How.XPATH, using = "//input[@title = 'Name' or @id = 'searchBar']")
	WebElement searchInput;
	@FindBy(how = How.XPATH, using = "//table[contains(@id, 'products-line')]//tbody//*[@role = 'row' or contains(@class ,'MuiTableRow-root')]")
	List<WebElement> productFamiliesList;
	@FindBy(how = How.XPATH, using = "//button[@title = 'Filter' or @aria-label = 'Filter']")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//input[@title = 'Name' or @title = 'description' or @name = 'description']")
	WebElement FilterByProductFamiliesNameInput;
	
	public void clickOnCreateButton() {
		clickOn(createButton);
	}
	
	public void clickOnFilterButton() {
		clickOn(filterButton);
	}
	
	public void setSearchInput(String value) {
		searchInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		searchInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public List<WebElement> productFamiliesList() {
		return productFamiliesList;
	}
	
	public void goToProductFamiliesDetails(String value) {
		clickOn(driver.findElement(By.xpath("//*[contains(text() , '" + value + "')]")));
	}
	public void activateFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//li//*[text()= '" + value + "']")));
	}
	public void setFilterByProductFamiliesName(String value) {
		FilterByProductFamiliesNameInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		FilterByProductFamiliesNameInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public void clickOnRemoveFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//button[@data-key = '" + value + "']")));
	}
	
	public void sortBy(String value) {
		try {
			clickOn(driver.findElement(By.xpath("//span[contains(@class, 'MuiButtonBase-root') and contains(@data-field, '" + value + "')]")));
		}catch (Exception e) {
			clickOn(driver.findElement(By.xpath("//th[@sortby= '" + value + "']")));
		}
		
	}
}