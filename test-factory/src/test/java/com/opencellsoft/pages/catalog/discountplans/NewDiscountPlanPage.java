package com.opencellsoft.pages.catalog.discountplans;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewDiscountPlanPage {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public NewDiscountPlanPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(5)).ignoring(NoSuchElementException.class);
	}

	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement discountPlanCode;
	@FindBy(how = How.XPATH, using = "//input[@id='label']")
	WebElement discountPlanLabel;
	@FindBy(how = How.XPATH, using = "//div[@id='discountPlanType']")
	WebElement discountPlanType;
	@FindBy(how = How.XPATH, using = "//input[@id='startDate']")
	WebElement startDateInput;
	@FindBy(how = How.XPATH, using = "//input[@id='endDate']")
	WebElement endDateInput;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day MuiPickersDay-current MuiPickersDay-daySelected']")
	WebElement startDate;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Save')]")
	WebElement saveDiscounPlanButton;

// set discount plan's code
	public void setDiscountPlanCode(String discountPlanCode) {
		this.discountPlanCode.sendKeys(discountPlanCode);
	}

	// set discount plan's label
	public void setDiscountPlanLabel(String discountPlanLabel) {
		this.discountPlanLabel.sendKeys(discountPlanLabel);
	}

	// set discount plan's type
	public void setDiscountPlanType(String discountType) {
		wait.until(ExpectedConditions.visibilityOf(discountPlanType));
		discountPlanType.click();
		discountPlanType.findElement(By.xpath("//li[normalize-space()='" + discountType + "']")).click();
	}

	// set discount plan's start date
	public void setStartDate() {
		startDateInput.click();
		startDate.click();
	}

	// set discount plan's end date
	public void setEndDate() {
		endDateInput.click();
		startDate.click();
	}

	// click on save discount plan button
	public void clickOnSaveDiscountPlanButton() {
		saveDiscounPlanButton.click();
	}

}