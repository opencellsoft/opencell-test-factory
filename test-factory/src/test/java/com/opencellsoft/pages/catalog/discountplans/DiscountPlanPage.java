package com.opencellsoft.pages.catalog.discountplans;

import java.time.Duration;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;


public class DiscountPlanPage extends TestBase {

	public DiscountPlanPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement discountPlanCodeInput;
	@FindBy(how = How.XPATH, using = "//input[@id='label']")
	WebElement discountPlanLabelInput;
	@FindBy(how = How.XPATH, using = "//div[@id='discountPlanType']")
	WebElement discountPlanTypeInput;
	@FindBy(how = How.XPATH, using = "//input[@id='startDate']")
	WebElement startDateInput;
	@FindBy(how = How.XPATH, using = "//input[@id='endDate']")
	WebElement endDateInput;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day MuiPickersDay-current MuiPickersDay-daySelected']")
	WebElement startDate;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement updateBtn;
	@FindBy(how = How.XPATH, using = "//input[@id='applicationFilterEL']")
	WebElement applicationFilter;
	@FindBy(how = How.XPATH, using = "//div[@class='ButtonGroupGutters']//button[@type='button']")
	WebElement createDiscountLineButton;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement submitButton;
	@FindBy(how = How.XPATH, using = "//*[normalize-space()='Save' or @type = 'submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Go Back']")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Activate')]")
	WebElement activateDiscountPlanButton;
	
	
	
	public void setCode(String code) {
		discountPlanCodeInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		discountPlanCodeInput.sendKeys(code);
		waitPageLoaded();
	}
	
	public void setLabel(String discountPlanLabel) {
		discountPlanLabelInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		discountPlanLabelInput.sendKeys(discountPlanLabel);
		waitPageLoaded();
	}

	public void setType(String discountType) {
		clickOn(discountPlanTypeInput);
		clickOn(driver.findElement(By.xpath("//*[text() = '" + discountType + "']")));
	}

	public void setStartDate() {
		startDateInput.click();
		startDate.click();
	}

	public void setEndDate() {
		endDateInput.click();
		startDate.click();
	}

	public void clickSaveBtn() {
		updateBtn.click();
	}

	public void setApplicationFilter( String filter) {
		applicationFilter.sendKeys(filter);
	}
	//click create discount line
	public void clickOnCreateDiscountLineButton() {
		clickOn(createDiscountLineButton);
	}

	// click save button
	public void clickOnSubmitButton() {
		submitButton.click();
	}

	public void clickOnActivateButton() {
		clickOn(activateDiscountPlanButton);
	}

	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
	
	public void clickOnGoBackButton() {
		clickOn(goBackButton);
	}
	
}
