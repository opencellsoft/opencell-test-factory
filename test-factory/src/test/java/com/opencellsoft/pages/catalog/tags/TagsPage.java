package com.opencellsoft.pages.catalog.tags;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import com.opencellsoft.base.TestBase;

public class TagsPage extends TestBase {

	public TagsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//input[@id='code' or @name = 'code']")
	WebElement tagCode;
	@FindBy(how = How.XPATH, using = "//input[@id='name' or @name = 'name']")
	WebElement tagLabel;
	@FindBy(how = How.XPATH, using = "//div[@id='tagType.code']")
	WebElement tagCategoryInput;
	@FindBy(how = How.XPATH, using = "//div[@id = 'seller.code']")
	WebElement sellersList;
	@FindBy(how = How.XPATH, using = "//ul[@aria-labelledby = 'seller.code-label']//li[2]")
	WebElement firstChoice;
	@FindBy(how = How.XPATH, using = "//div[@id='parentTag.code']")
	WebElement parenttagList;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Save') or @type = 'submit']")
	WebElement submitButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Delete')]")
	WebElement delete;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//*[contains(text(),'Confirm')]")
	WebElement confirmDelete;
	
	public void setCode(String tag_code) {
		tagCode.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		tagCode.sendKeys(tag_code);
		waitPageLoaded();
	}
	
	// This method is to set product family Label
	public void setName(String tag_name) {
		tagLabel.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		tagLabel.sendKeys(tag_name);
		waitPageLoaded();
	}

	// This method is to set product family description
	public void selectCategory(String tag_category) {
		clickOn(tagCategoryInput);
		driver.findElement(By.xpath("//ul//*[contains(text(),'" + tag_category + "')]")).click();
		
	}
	
	// This method is to select seller
	public void selectSeller(String selectSeller) {
		clickOn(sellersList);
		if(selectSeller != null) {
			sellersList.findElement(By.xpath("//li[contains(text(),'" + selectSeller + "')]")).click();
		}else {
			clickOn(firstChoice);
		}
	}

	// This method is to select parent product family
	public void selectParent(String tag_parent) {
		if(tag_parent != null) {
			clickOn(parenttagList);
			clickOn(driver.findElement(By.xpath("//li[contains(@data-value ,'" + tag_parent + "')]")));
		}
	}

	// This method is to click on submit button
	public void clickOnSaveButton() {
		clickOn(submitButton);
	}

	// This method is to click on go back button
	public void clickOnGoBackButton() {
		clickOn(goBackButton);
	}
	// This method is to click on delete button
	public void clickOnDeleteButton() {
		clickOn(delete);
	}

	// This method is to click on confirm delete
	public void clickOnConfirmButton() {
		clickOn(confirmDelete);
	}
}
