package com.opencellsoft.pages.catalog.catalogueManager.referenceTables;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class CustomTablesListPage extends TestBase {

	public CustomTablesListPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//*[@id = 'search' or @id = 'searchBar']")
	WebElement searchInput;
	@FindBy(how = How.XPATH, using = "(//div[@class = 'MuiGrid-root gridBlock fullWidthChild fullWidthChild'])[2]")
	WebElement dataGridElt;
	
	public void setSearchInput(String code) {
		searchInput.sendKeys(code);
	}
	
	public void clickOnResult(String code) {
		try {
			clickOn(driver.findElement(By.xpath("//tbody")));
		}catch (Exception e) {
			clickOn(driver.findElement(By.xpath("//mark[contains(text(),'" + code + "')]")));
		}
	}
	
	public String getDataGridElt() {
		return dataGridElt.getText();
	}
}
