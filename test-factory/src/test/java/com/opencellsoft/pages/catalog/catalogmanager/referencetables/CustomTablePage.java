package com.opencellsoft.pages.catalog.catalogmanager.referencetables;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class CustomTablePage extends TestBase {
	
	public CustomTablePage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Add rows')]")
	WebElement addRowsButton;
	@FindBy(how = How.XPATH, using = "//button[@label = 'Edit']")
	WebElement editeButton;
	@FindBy(how = How.XPATH, using = "(//button[@class = 'MuiButtonBase-root MuiIconButton-root'])[1]")
	WebElement deleteButton;
	@FindBy(how = How.XPATH, using = "//button[@type = 'submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//button[@aria-label = 'Previous page']")
	WebElement previousPageButton;
	@FindBy(how = How.XPATH, using = "//button[@aria-label = 'Next page']")
	WebElement nextPageButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Actions')]")
	WebElement actionButton;
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Download XLSX')]")
	WebElement downlaodXLSXButton;
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Download CSV')]")
	WebElement downlaodCSVButton;
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'100')]")
	WebElement selectNumberOfRows;
	@FindBy(how = How.XPATH, using = "//li[@data-value = '25']")
	WebElement option25;
	
	public void clickOnAddRowsButton() {
		clickOn(addRowsButton);
	}
	
	public void clickOnEditeButton() {
		clickOn(editeButton);
	}
	
	public void clickOnDeleteButton() {
		clickOn(deleteButton);
	}
	
	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
	
	public void clickOnPreviousPageButton() {
		clickOn(previousPageButton);
	}
	
	public void clickOnNextPageButton() {
		clickOn(nextPageButton);
	}
	
	public void clickOnActionButton() {
		clickOn(actionButton);
	}
	
	public void clickOnDownlaodXLSXButton() {
		clickOn(downlaodXLSXButton);
	}
	
	public void clickOndownlaodCSVButton() {
		clickOn(downlaodCSVButton);
	}
	
	public boolean valueIsPresent(String value) {
		List<WebElement> elements = driver.findElements(By.xpath("//*[contains(text(), '" + value + "')]"));
		return !elements.isEmpty();
		
	}
	
	public boolean rowIsPresent(String value) {
		List<WebElement> elements = driver.findElements(By.xpath("//form[@class = 'CustomTables']//span[contains(text(), '" + value + "')]"));
		return elements.isEmpty();
		
	}

	public void select25RowsPerPage() {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].scrollIntoView();", selectNumberOfRows);
			clickOn(selectNumberOfRows);
			}
		catch (Exception e) {
			clickOn(selectNumberOfRows);
		}
		clickOn(option25);
	}
	
}
