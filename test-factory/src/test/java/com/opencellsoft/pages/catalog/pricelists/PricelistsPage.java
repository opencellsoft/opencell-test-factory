package com.opencellsoft.pages.catalog.pricelists;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class PricelistsPage extends TestBase {

	public PricelistsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//input[@id = 'code']")
	WebElement nameInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'description']")
	WebElement descriptionInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'validFrom']")
	WebElement validFromInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'validUntil']")
	WebElement validToInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'applicationStartDate']")
	WebElement applicationStartDateInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'applicationEndDate']")
	WebElement applicationEndDateInput;
	@FindBy(how = How.XPATH, using = "//*[@id = 'country' or @name = 'country' ]")
	WebElement countryInput;
	@FindBy(how = How.XPATH, using = "//button[@type = 'submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//span[text() = 'Go Back']")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Edit']")
	WebElement updateButton;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//input[contains(@id , 'code')]")
	WebElement searchByNameInput;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//table//tbody//tr[1]//td[2]")
	WebElement firstResult;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Delete']")
	WebElement deleteButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Confirm']")
	WebElement confirmButton;
	@FindBy(how = How.XPATH, using = "//form[contains(@class, 'PriceList')]//*[text() = 'Create']")
	WebElement createPriceLineButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Actions']")
	WebElement actionsButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Activate']")
	WebElement activateButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Duplicate']")
	WebElement duplicateButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Archive']")
	WebElement archiveButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Close']")
	WebElement closeButton;
	public void setName(String name) {
		nameInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		nameInput.sendKeys(name);
		waitPageLoaded();
	}
	
	public void setDescription(String description) {
		descriptionInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		descriptionInput.sendKeys(description);
		waitPageLoaded();
	}
	
	public void setValidFrom(String value) {
		clickOn(validFromInput);
		clickOn(driver.findElement(By.xpath("//div[@role = 'dialog' or @role = 'presentation']//*[text() = '" + value + "']")));
	}
	
	public void setValidTo(String value) {
		clickOn(validToInput);
		clickOn(driver.findElement(By.xpath("//div[@role = 'dialog' or @role = 'presentation']//*[text() = '" + value + "']")));
	}
	
	public void setApplicationStartDate(String value) {
		clickOn(applicationStartDateInput);
		clickOn(driver.findElement(By.xpath("//div[@role = 'dialog' or @role = 'presentation']//*[text() = '" + value + "']")));
	}
	
	public void setApplicationEndDate(String value) {
		clickOn(applicationEndDateInput);
		clickOn(driver.findElement(By.xpath("//div[@role = 'dialog' or @role = 'presentation']//*[text() = '" + value + "']")));
	}

	public void setCountry(String country) {
		clickOn(countryInput);
		if (country != null) {
			searchByNameInput.sendKeys(country);
			waitPageLoaded();
			clickOn(firstResult);
		}else {
			clickOn(firstResult);
		}
	}
	
	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
	
	public void clickOnGoBackButton() {
		clickOn(goBackButton);
	}
	
	public void clickOnUpdateButton() {
		clickOn(updateButton);
	}
	
	public void setSearchByLevelName(String name) {
		searchByNameInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		searchByNameInput.sendKeys(name);
		waitPageLoaded();
	}
	
	public void clickOnFirstResult() {
		clickOn(firstResult);
	}
	public void clickOnDeleteButton() {
		clickOn(deleteButton);
	}
	public void clickOnConfirmButton() {
		clickOn(confirmButton);
	}
	
	public void clickOnCreatePriceLineButton() {
		clickOn(createPriceLineButton);
	}
	
	public void clickOnActionsButton() {
		clickOn(actionsButton);
	}
	
	public void clickOnActivateButton() {
		clickOn(activateButton);
	}
	
	public void clickOnDuplicateButton() {
		clickOn(duplicateButton);
	}
	
	public void clickOnArchiveButton() {
		clickOn(archiveButton);
	}
	
	public void clickOnCloseButton() {
		clickOn(closeButton);
	}
}
