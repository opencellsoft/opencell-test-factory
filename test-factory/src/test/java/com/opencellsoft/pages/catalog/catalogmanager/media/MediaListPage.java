package com.opencellsoft.pages.catalog.catalogmanager.media;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;



public class MediaListPage extends TestBase {

	public MediaListPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//*[@title = 'Create' or @id = 'CREATE' or @data-testid='AddIcon']")
	WebElement createButton;
	@FindBy(how = How.XPATH, using = "//input[@id = 'searchBar']")
	WebElement searchInput;
	@FindBy(how = How.XPATH, using = "//input[@title = 'Media name' or @id = 'label']")
	WebElement searchByDescriptionInput;
	@FindBy(how = How.XPATH, using = "//table[contains(@id, 'digital-resources')]//tbody//*[@role = 'row' or contains(@class ,'MuiTableRow-root')]")
	List<WebElement> mediaList;
	@FindBy(how = How.XPATH, using = "//button[@title = 'Filter' or @aria-label = 'Filter']")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//table[contains(@id, 'group-attribute')]//tbody//*[@role = 'row' or contains(@class ,'MuiTableRow-root')]")
	List<WebElement> attributeGroupsList;
	
	
	public void clickOnCreateButton() {
		clickOn(createButton);
	}
	
	public void setSearchInput(String value) {
		searchInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		searchInput.sendKeys(value);
		waitPageLoaded();
	}

	public List<WebElement> MediaList() {
		return mediaList;
	}

	public void goToMediaDetails(String value) {
		clickOn(driver.findElement(By.xpath("//*[contains(text() , '" + value + "')]")));
	}
	
	public void clickOnFilterButton() {
		clickOn(filterButton);
	}
	
	public void activateFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//li//*[text()= '" + value + "']")));
	}
	
	public void setDescription(String value) {
		searchByDescriptionInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		searchByDescriptionInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public void clickOnRemoveFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//button[@data-key = '" + value + "']")));
	}
	
	public void sortBy(String value) {
		clickOn(driver.findElement(By.xpath("//span[contains(@class, 'MuiButtonBase-root') and contains(@data-field, '" + value + "')]")));
	}
}