package com.opencellsoft.pages.tags;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class NewTagPage {
	WebDriver driver;

	public NewTagPage(WebDriver driver) {
		this.driver = driver;
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement tagCode;
	@FindBy(how = How.XPATH, using = "//input[@id='name']")
	WebElement tagName;
	@FindBy(how = How.XPATH, using = "//div[@id='tagType.code']")
	WebElement tagTypeList;
	@FindBy(how = How.XPATH, using = "//div[@id='menu-tagType.code']/div[3]/ul/li[4]")
	WebElement tagTypeCode;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement submitButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;
	
	// Defining all the user actions (Methods) that can be performed in the
	// Tags page
//this method is to set a tag code
	public void setTagCode(String strTagCode) {
		tagCode.sendKeys(strTagCode);
	}	
	// this method is to set a tag's name
	public void setTagName(String strTagName) {
		tagName.sendKeys(strTagName);
	}
// this method is to select a tag type
	public void selectTagType() {
		tagTypeList.click();
		tagTypeCode.click();
	}
	
	// This method to click on save Button
	public void clickOnSaveButton() {
		submitButton.submit();
	}
//this method is to click on go back button
	public void clickOnGoBackButton() {
		goBackButton.click();
	}

}
