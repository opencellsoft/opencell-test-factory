package com.opencellsoft.pages.tags;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ListOfTagsPage {

	WebDriver driver;

	public ListOfTagsPage(WebDriver driver) {
		this.driver = driver;
	}

	// Using FindBy for locating elements
	@FindBy(how = How.CSS, using = ".MuiButton-root:nth-child(2)" )
	WebElement createTagButton;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase name']")
	WebElement searchInput;
	@FindBy(how = How.CSS, using = ".MuiTableRow-root.jss1022.jss1024.jss1023.MuiTableRow-hover")
	WebElement foundTag;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addFilterButton;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Tag code')]")
	WebElement searchByCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement codeInput;


	// Defining all the user actions (Methods) that can be performed in the

	// This method to click on Create tag Button
	public void clickOnCreateTagButton() {
		createTagButton.click();
	}
	//this method is to look for a tag 
	public void searchForTag(String tagName) {
		searchInput.sendKeys(tagName);
		foundTag.click();
	}
	//this method is to look for a tag using filters
	public void searchingWithFilter(String tagCode) {
		addFilterButton.click();
		searchByCode.click();
		codeInput.sendKeys(tagCode);
		foundTag.click();
	}

}
