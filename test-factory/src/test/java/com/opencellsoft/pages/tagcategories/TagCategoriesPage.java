package com.opencellsoft.pages.tagcategories;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class TagCategoriesPage {

	WebDriver driver;

	public TagCategoriesPage(WebDriver driver) {
		this.driver = driver;
	}
	
	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Delete']")
	WebElement tagCategoryDelete;
	
	// This method is to click on delete button
	public void clickOnDeleteTagCategory() {
		tagCategoryDelete.click() ;
	}

}
