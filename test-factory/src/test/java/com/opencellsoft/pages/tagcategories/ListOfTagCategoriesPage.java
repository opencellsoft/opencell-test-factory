package com.opencellsoft.pages.tagcategories;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ListOfTagCategoriesPage {

	WebDriver driver;

	public ListOfTagCategoriesPage(WebDriver driver) {
		this.driver = driver;
	}
	
	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase description']")
	WebElement tagCategoryName;
	@FindBy(how = How.CSS, using = "body > div:nth-child(8) > div:nth-child(3) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(2)")
	WebElement tagCategorySelected;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addFilter;
	@FindBy(how = How.CSS, using = "li:nth-child(1)")
	WebElement tagCategoryCodeBtn;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement tagCategoryCode;
	
	// This method is to set tag description in the description text box
	public void setTagCategoryName(String setTagCategoryDescription) {
		tagCategoryName.sendKeys(setTagCategoryDescription);
	}

	// This method is to select a tag category from list
	public void clickTagCategorySelected() {
		tagCategorySelected.click();
	}

	// This method is to click on add filter button
	public void clickOnAddFilter() {
		addFilter.click();
	}

	// This method is to click on tag category code filter button
	public void clickOnTagCategoryCodeBtn() {
		tagCategoryCodeBtn.click();
	}
	
	// This method is to set tag category code
	public void setTagCategoryCode(String setTagCategoryCode) {
		tagCategoryCode.sendKeys(setTagCategoryCode);
	}
}
