package com.opencellsoft.pages.tagcategories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class NewTagCategoriesPage {

	WebDriver driver;

	public NewTagCategoriesPage(WebDriver driver) {
		this.driver = driver;
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-textSizeSmall MuiButton-sizeSmall']")
	WebElement createTagCategoriesBtn;
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement tagCategoryCode;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement tagCategoryDescription ;
	//@FindBy(how = How.XPATH, using = "//div[@id='seller.code']")
	@FindBy(how = How.ID, using = "seller.code")
	WebElement sellersList;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Save')]")
	WebElement saveBtn;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackBtn;

	// This method is to click on create on tag category button
	public void clickOnCreateTagCategoriesBtn() {
		createTagCategoriesBtn.click();
	}
	
	// This method is to set tag category code in the code text box
	public void setTagCategoryCode(String setTagCategoryCode) {
		tagCategoryCode.sendKeys(setTagCategoryCode);
	}
	
	// This method is to set tag category description in the description text box
	public void setTagCategoryName(String setTagCategoryDescription) {
		tagCategoryDescription.sendKeys(setTagCategoryDescription);
	}
	
	// This method is to select a seller	
	public void selectSeller(String sellerCode) {
		sellersList.click();// assuming you have to click the "dropdown" to open it
		sellersList.findElement(By.xpath("//li[contains(.,'"+sellerCode+"')]")).click();
	}

	// This method is to click on save button
	public void clickSave() {
		saveBtn.submit();
	}

	// This method is to click on go back button
	public void clickOnGoBack() {
		goBackBtn.click();
	}

}
