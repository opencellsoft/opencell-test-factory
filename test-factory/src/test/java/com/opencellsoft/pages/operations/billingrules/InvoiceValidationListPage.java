package com.opencellsoft.pages.operations.billingrules;

import java.time.Duration;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class InvoiceValidationListPage extends TestBase {
	WebDriver driver;
	Actions act;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public InvoiceValidationListPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
	}
	
	@FindBy(how = How.XPATH, using = "//*[text() = '-1']")
	WebElement comInvoice;
	@FindBy(how = How.XPATH, using = "//input[@id='searchBar']")
	WebElement searchBar;
	@FindBy(how = How.XPATH, using = "//*[@data-testid='datagrid-column-code']")
	WebElement columnCode;	
	@FindBy(how = How.XPATH, using = "//*[@data-testid='datagrid-column-description']")
	WebElement columnDescription;	
	@FindBy(how = How.XPATH, using = "//*[@data-testid='ClearIcon']")
	WebElement clearIcon;	
	@FindBy(how = How.XPATH, using = "//*[@data-testid='EditIcon']")
	WebElement editIcon;	
	@FindBy(how = How.XPATH, using = "//*[@data-testid='HighlightOffIcon']")
	WebElement highlightOffIcon;	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Confirm')]")
	WebElement confirmButton;	
	@FindBy(how = How.XPATH, using = "//div[@data-source='hasValidationScript']")
	WebElement hasValidationScriptCheckbox;	
	@FindBy(how = How.XPATH, using = "//div[@data-source='hasRules']")
	WebElement hasRulesCheckbox;	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;	
	
	public void clickOnGoBackButton() {
		wait.until(ExpectedConditions.visibilityOf(goBackButton));
		goBackButton.click();
	}
	
	public void clickOnHasRulesCheckbox() {
		wait.until(ExpectedConditions.visibilityOf(hasRulesCheckbox));
		hasRulesCheckbox.click();
	}
	
	public void clickOnHasValidationScriptCheckbox() {
		wait.until(ExpectedConditions.visibilityOf(hasValidationScriptCheckbox));
		hasValidationScriptCheckbox.click();
	}
	
	public void goToComInvoice() {
		clickOn(comInvoice);
	}
	
	public void setText(String text) {
		wait.until(ExpectedConditions.visibilityOf(searchBar));
		searchBar.sendKeys(text);
	}
	
	public void clickOnConfirmButton() {
		wait.until(ExpectedConditions.visibilityOf(confirmButton));
		confirmButton.click();
	}
	
	public void sortByCode() {
		wait.until(ExpectedConditions.visibilityOf(columnCode));
		columnCode.click();
	}
	
	public void sortByDescription() {
		wait.until(ExpectedConditions.visibilityOf(columnDescription));
		columnDescription.click();
	}
	
	public void ClickOnClearIcon() {
		wait.until(ExpectedConditions.visibilityOf(clearIcon));
		clearIcon.click();
	}
	
	public void ClickOnEditIcon() {
		wait.until(ExpectedConditions.visibilityOf(editIcon));
		editIcon.click();
	}
	
	public void ClickOnDeleteIcon() {
		wait.until(ExpectedConditions.visibilityOf(highlightOffIcon));
		highlightOffIcon.click();
	}
	
	public void ClickOnComInvoice() {
		wait.until(ExpectedConditions.visibilityOf(comInvoice));
		comInvoice.click();
	}
	
	
}
