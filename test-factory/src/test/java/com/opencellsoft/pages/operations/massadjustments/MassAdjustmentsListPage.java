package com.opencellsoft.pages.operations.massadjustments;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.opencellsoft.base.TestBase;

public class MassAdjustmentsListPage extends TestBase {

	WebDriver driver;
	Actions act;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public MassAdjustmentsListPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
	}
	
	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'List and Filters')]")
	WebElement listAndFiltersButton;
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Mark whole result')]")
	WebElement markWholeResultButton;
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Un-mark whole result')]")
	WebElement unMarkWholeResultButton;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbar-root MuiSnackbar-anchorOriginTopRight']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiGrid-root MuiGrid-container MuiGrid-item MuiGrid-align-items-xs-center MuiGrid-grid-xs-3']//button[@type='button']")
	WebElement createCreditNotesButton;
	@FindBy(how = How.XPATH, using = "//body[@style='overflow: hidden;']/div[@class='MuiDialog-root DialogWithDividers']/div[@class='MuiDialog-container MuiDialog-scrollPaper']/div[@class='MuiPaper-root MuiDialog-paper MuiDialog-paperScrollPaper MuiDialog-paperWidthSm MuiPaper-elevation24 MuiPaper-rounded']/div[@class='MuiDialogActions-root MuiDialogActions-spacing']/div[@class='MuiButtonGroup-root']/button[2]")
	WebElement confirmButton;
	@FindBy(how = How.XPATH, using = "//SPAN[@class='MuiTab-wrapper'][text()='Job history']")
	WebElement jobHistoryTab;
	
	public void clickOnJobHistoryTab() {
		wait.until(ExpectedConditions.elementToBeClickable(jobHistoryTab));
		jobHistoryTab.click();
	}
	
	public void clickOnConfirmButton() {
		wait.until(ExpectedConditions.elementToBeClickable(confirmButton));
		confirmButton.click();
	}
	
	public void clickOnCreateCreditNotesButton() {
		wait.until(ExpectedConditions.elementToBeClickable(createCreditNotesButton));
		createCreditNotesButton.click();
	}
	
	public void clickOnListAndFiltersButton() {
		wait.until(ExpectedConditions.elementToBeClickable(listAndFiltersButton));
		listAndFiltersButton.click();
	}
	
	public void checkText(String text) {
		List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + text + "')]"));
		//Assert.assertTrue("Text not found!", list.size() > 0);
		Assert.assertTrue(list.size() > 0);
	}
	
	public void clickOnMarkWholeResultButton() {
		wait.until(ExpectedConditions.elementToBeClickable(markWholeResultButton));
		markWholeResultButton.click();
	}
	
	public void clickOnUnMarkWholeResultButton() {
		wait.until(ExpectedConditions.elementToBeClickable(unMarkWholeResultButton));
		unMarkWholeResultButton.click();
	}
	
	// get msg
	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}
	
}
