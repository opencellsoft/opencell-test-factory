package com.opencellsoft.pages.operations.walletoperations;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class WalletOperationsPage extends TestBase {

	public WalletOperationsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID, using = "searchBar")
	WebElement searchBarInput;
	@FindBy(how = How.ID, using = "subscription")
	WebElement subscriptionInput;
	@FindBy(how = How.XPATH, using = "(//div[contains(@class, 'PageTitle')]//button)[1]")
	WebElement refreshButton1;
	@FindBy(how = How.XPATH, using = "//button[contains(@title, 'Refresh') or contains(@aria-label, 'Refresh')]")
	WebElement refreshButton2;
	@FindBy(how = How.XPATH, using = "//button[@aria-label= 'Add filter'  or @title = 'Filter']")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//li[@data-key = 'subscription']")
	WebElement subscriptionChoice;
	@FindBy(how = How.XPATH, using = "(//ul[@role = 'menu']//li[@role = 'menuitem'])[1]")
	WebElement firstChoice;
	@FindBy(how = How.XPATH, using = "//div[@data-source = 'subscription']//input")
	WebElement filterBySubscription;
	@FindBy(how = How.XPATH, using = "//input[@id = 'wildcardOrIgnoreCase code']")
	WebElement subscriptionCode;
	@FindBy(how = How.XPATH, using = "(//div[@role = 'dialog']//table[@filters = '[object Object]']//tbody//tr)[1]")
	WebElement firstResult;
	
	
	public void setSearchBarInput(String value) {
		searchBarInput.sendKeys(Keys.CONTROL + "a");
		searchBarInput.sendKeys(Keys.DELETE);
		searchBarInput.sendKeys(value);
	}
	
	public void setSubscriptionInput(String value) {
		subscriptionInput.sendKeys(Keys.CONTROL + "a");
		subscriptionInput.sendKeys(Keys.DELETE);
		subscriptionInput.sendKeys(value);
	}

	// return the number of lines found
	public int elementsFoundSize(String value) {
		List<WebElement> result = driver.findElements(By.xpath("//td[normalize-space()='"+ value + "']"));
		if(result.size() == 0) {
			waitPageLoaded();
			result = driver.findElements(By.xpath("//td[normalize-space()='"+ value + "']"));
		}
		return result.size();
	}

	// return the number of lines found
	public String getAmount() {
		return  driver.findElement(By.xpath("//table//tbody//tr//td[13]")).getText();
	}

	public void clickOnRefreshButton() {
		try {
		clickOn(refreshButton1);
		waitPageLoaded();
		}catch (Exception e) {
			clickOn(refreshButton2);
		}
	}
	
	public void clickOnFilterButton() {
		clickOn(filterButton);
	}
	
	public void selectFilterBySubscription() {
		clickOn(subscriptionChoice);
	}
	
	public void clickOnFilterBySubscription() {
		clickOn(filterBySubscription);
	}
	public void selectFirstChoice() {
		clickOn(firstChoice);
	}
	public void setSubscriptionCodeInput(String value) {
		subscriptionCode.sendKeys(Keys.CONTROL + "a");
		subscriptionCode.sendKeys(Keys.DELETE);
		waitPageLoaded();
		subscriptionCode.sendKeys(value);
		waitPageLoaded();
	}
	
	public void selectSubscription() {
		clickOn(firstResult);
	}
	
}
