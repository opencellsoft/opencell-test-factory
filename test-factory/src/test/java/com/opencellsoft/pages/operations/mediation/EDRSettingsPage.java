package com.opencellsoft.pages.operations.mediation;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


import com.opencellsoft.base.TestBase;

public class EDRSettingsPage extends TestBase {

	Actions act;
	public EDRSettingsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//input[@type='checkbox']")
	WebElement EnableEdrVersionning;
	@FindBy(how = How.XPATH, using = "(//div[@data-field='criteriaEL'])[2]")
	WebElement CriteriaELColumn1;
	@FindBy(how = How.XPATH, using = "(//div[@data-field='keyEL'])[2]")
	WebElement EventKeyELColumn1;
	@FindBy(how = How.XPATH, using = "(//div[@data-field='isNewVersionEL'])[2]")
	WebElement IsNewVersionELColumn1;	
	@FindBy(how = How.XPATH, using = "(//div[@data-field='criteriaEL'])[3]")
	WebElement CriteriaELColumn2;
	@FindBy(how = How.XPATH, using = "(//div[@data-field='keyEL'])[3]")
	WebElement EventKeyELColumn2;
	@FindBy(how = How.XPATH, using = "(//div[@data-field='isNewVersionEL'])[3]")
	WebElement IsNewVersionELColumn2;
	@FindBy(how = How.XPATH, using = "//textarea[contains(@id,'criteriaEL')]")
	WebElement CriteriaELInput;
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'keyEL')]")
	WebElement EventKeyELInput;
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'isNewVersionEL')]")
	WebElement IsNewVersionELInput;


	@FindBy(how = How.XPATH, using = "(//button[@type='submit'])")
	WebElement SaveButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement GoBackButton;

	@FindBy(how = How.XPATH, using = "//div[@class='MuiDataGrid-main']")
	WebElement dataGridElt;


	public boolean checkEdrVersionningTable(String CriteriaEL1,String EventKeyEL1,String NewVersionEL1,String CriteriaEL2, String EventKeyEL2,String NewVersionEL2) {
		String dgValues = dataGridElt.getText();
		if (dgValues.contains(CriteriaEL1)&& dgValues.contains(EventKeyEL1) && dgValues.contains(NewVersionEL1) && dgValues.contains(CriteriaEL2) && dgValues.contains(EventKeyEL2) && dgValues.contains(NewVersionEL2))
			return true;
		else 
			return false;
	}
	
	public boolean EnebleEDRsCheckBox() {
		return EnableEdrVersionning.isSelected();
	}


	public void clickOnEnableEdrVersionning() {
		EnableEdrVersionning.click();	
	}

	public void SetCriteriaEL1(String CriteriaEL) {
		act = new Actions(driver);
		act.doubleClick(CriteriaELColumn1).perform();
		CriteriaELInput.sendKeys(CriteriaEL);
		CriteriaELInput.sendKeys(Keys.RETURN);
	}




	public void SetEventKeyEL1(String KeyEL) {
		act = new Actions(driver);
		act.doubleClick(EventKeyELColumn1).perform();
		EventKeyELInput.sendKeys(KeyEL);
		EventKeyELInput.sendKeys(Keys.RETURN);
	}

	public void SetIsNewVersionEL1(String NewVersionEL) {
		act = new Actions(driver);
		act.doubleClick(IsNewVersionELColumn1).perform();
		IsNewVersionELInput.sendKeys(NewVersionEL);
		IsNewVersionELInput.sendKeys(Keys.RETURN);
	}

	public void SetCriteriaEL2(String CriteriaEL) {
		act = new Actions(driver);
		act.doubleClick(CriteriaELColumn2).perform();
		CriteriaELInput.sendKeys(CriteriaEL);
		CriteriaELInput.sendKeys(Keys.RETURN);
	}

	public void SetEventKeyEL2(String KeyEL) {
		act = new Actions(driver);
		act.doubleClick(EventKeyELColumn2).perform();
		EventKeyELInput.sendKeys(KeyEL);
		EventKeyELInput.sendKeys(Keys.RETURN);
	}

	public void SetIsNewVersionEL2(String NewVersionEL) {
		act = new Actions(driver);
		act.doubleClick(IsNewVersionELColumn2).perform();
		IsNewVersionELInput.sendKeys(NewVersionEL);
		IsNewVersionELInput.sendKeys(Keys.RETURN);
	}

	public void clickOnSave() {
		clickOn(SaveButton);
	}
}
