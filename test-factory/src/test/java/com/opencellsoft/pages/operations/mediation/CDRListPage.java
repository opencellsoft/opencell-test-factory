package com.opencellsoft.pages.operations.mediation;

import java.time.Duration;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class CDRListPage extends TestBase {
	WebDriver driver;
	Actions act;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public CDRListPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
	}
	
	@FindBy(how = How.XPATH, using = "//input[@id='searchBar']")
	WebElement searchBar;
	@FindBy(how = How.XPATH, using = "//*[@data-testid='datagrid-column-status']")
	WebElement columnStatus;	
	@FindBy(how = How.XPATH, using = "//*[@data-testid='datagrid-column-eventDate']")
	WebElement eventDate;	
	@FindBy(how = How.XPATH, using = "//*[@data-testid='datagrid-column-description']")
	WebElement columnDescription;	
	@FindBy(how = How.XPATH, using = "//*[@data-testid='ClearIcon']")
	WebElement clearIcon;	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Confirm')]")
	WebElement confirmButton;	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;	
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//*[@data-testid='AddIcon']")
	WebElement addIcon;	
	@FindBy(how = How.XPATH, using = "//*[@data-testid='NoteAddIcon']")
	WebElement noteAddIcon;	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Close')]")
	WebElement closeButton;	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Import from server')]")
	WebElement importButton;
	@FindBy(how = How.XPATH, using = "//div[@id='EXPORT']")
	WebElement exportButton;
	

	public void clickOnCloseButton() {
		wait.until(ExpectedConditions.visibilityOf(closeButton));
		closeButton.click();
	}
	
	public void clickOnImportButton() {
		wait.until(ExpectedConditions.visibilityOf(importButton));
		importButton.click();
	}
	
	public void clickOnExportButton() {
		wait.until(ExpectedConditions.visibilityOf(exportButton));
		exportButton.click();
	}
		
	public void clickOnAddButton() {
		wait.until(ExpectedConditions.visibilityOf(addIcon));
		addIcon.click();
	}
	
	public void clickOnNoteAddIcon() {
		wait.until(ExpectedConditions.visibilityOf(noteAddIcon));
		noteAddIcon.click();
	}
	
	public void clickOnGoBackButton() {
		wait.until(ExpectedConditions.visibilityOf(goBackButton));
		goBackButton.click();
	}
	
	public void clickOnSaveButton() {
		wait.until(ExpectedConditions.visibilityOf(saveButton));
		saveButton.click();
	}
	public void setText(String text) {
		wait.until(ExpectedConditions.visibilityOf(searchBar));
		searchBar.sendKeys(text);
	}
	
	public void clickOnConfirmButton() {
		wait.until(ExpectedConditions.visibilityOf(confirmButton));
		confirmButton.click();
	}
	
	public void sortByEventDate() {
		wait.until(ExpectedConditions.visibilityOf(eventDate));
		eventDate.click();
	}
	
	public void sortByStatus() {
		wait.until(ExpectedConditions.visibilityOf(columnStatus));
		columnStatus.click();
	}
	
	public void ClickOnClearIcon() {
		wait.until(ExpectedConditions.visibilityOf(clearIcon));
		clearIcon.click();
	}
	

	
	
}
