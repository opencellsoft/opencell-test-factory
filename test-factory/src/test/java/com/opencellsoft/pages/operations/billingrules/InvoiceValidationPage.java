package com.opencellsoft.pages.operations.billingrules;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class InvoiceValidationPage extends TestBase {
	public InvoiceValidationPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//table[@class = 'data-grid']//td[2]")
	WebElement labelColum;
	@FindBy(how = How.XPATH, using = "//table[@class = 'data-grid']//td[2]/input")
	WebElement labelInput;
	@FindBy(how = How.XPATH, using = "//table[@class = 'data-grid']//td[5]")
	WebElement typeColum;
	@FindBy(how = How.XPATH, using = "//*[text() = 'SCRIPT']")
	WebElement scriptType;
	@FindBy(how = How.XPATH, using = "//table[@class = 'data-grid']//td[6]")
	WebElement failColum1;
	@FindBy(how = How.XPATH, using = "//table[@class = 'data-grid']//td[7]")
	WebElement failColum2;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Suspect']")
	WebElement failModeSuspect;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Rejected']")
	WebElement failModeRejected;
	@FindBy(how = How.XPATH, using = "//table[@class = 'data-grid']//td//button")
	WebElement actionButton;
	@FindBy(how = How.XPATH, using = "//input[@id = 'validationScript']")
	WebElement invoiceValidationList;
	@FindBy(how = How.XPATH, using = "(//span[contains(text() , 'CompareInvoiceAmount')])[2]")
	WebElement compareInvoiceAmountScript2;
	@FindBy(how = How.XPATH, using = "(//span[contains(text() , 'CompareInvoiceAmount')])[1]")
	WebElement compareInvoiceAmountScript1;
	@FindBy(how = How.XPATH, using = "//input[@id = 'value' or @type = 'number']")
	WebElement valueInput;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Confirm']")
	WebElement confirmButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Cancel']")
	WebElement cancelButton;
	@FindBy(how = How.XPATH, using = "(//*[text() = 'Cancel'])[2]")
	WebElement cancelButton2;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Save']")
	WebElement saveButton;

	Actions act;
	public void setLabel(String label) {
		act = new Actions(driver);
		act.doubleClick(labelColum).perform();
		waitPageLoaded();
		clickOn(labelInput);
		labelInput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		labelInput.sendKeys(label);
	}


	public void setType(String mode) {
		act = new Actions(driver);
		clickOn(typeColum);
		act.doubleClick(typeColum).perform();
		waitPageLoaded();
		if(mode.equals("script")) {
			clickOn(scriptType);
		}
	}

	public void setFailMode(String mode, String version) {
		act = new Actions(driver);
		if(version.equals("14.1.X")) {
			clickOn(failColum1);
			act.doubleClick(failColum1).perform();
		}else {
			clickOn(failColum2);
			act.doubleClick(failColum2).perform();
		}
		waitPageLoaded();
		if(mode.equals("suspect")) {
			clickOn(failModeSuspect);
		}else {
			clickOn(failModeRejected);
		}
	}

	public void clickOnActionButton() {
		clickOn(actionButton);
	}

	public void clickOnInvoiceValidationList() {
		clickOn(invoiceValidationList);
	}

	public void selectCompareInvoiceAmountScript() {
		try{
			clickOn(compareInvoiceAmountScript2);
		}catch (Exception e) {
			clickOn(compareInvoiceAmountScript1);
		}

	}

	public void setValue(String value) {
		valueInput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		valueInput.sendKeys(value);
		waitPageLoaded();
	}

	public void clickOnConfirm() {
		clickOn(confirmButton);
	}
	public void clickOnCancelButton() {
		clickOn(cancelButton);
	}

	public void clickOnCancelButton2() {
		clickOn(cancelButton2);
	}

	public void clickOnSave() {
		clickOn(saveButton);
	}

}
