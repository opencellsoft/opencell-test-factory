package com.opencellsoft.pages.operations.rating;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.opencellsoft.base.TestBase;

public class RatedItemsListPage extends TestBase {

	WebDriver driver;
	Actions act;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public RatedItemsListPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
	}
	
	// Using FindBy for locating elements

	@FindBy(how = How.XPATH, using = "//div[@role='alert']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//button[@type='TAB_SIMPLE_FILTERS']")
	WebElement simpleFiltersTab;
	@FindBy(how = How.XPATH, using = "//button[@type='TAB_ADVANCED_FILTER']")
	WebElement advancedFilterTab;
	@FindBy(how = How.XPATH, using = "//button[@type='TAB_LIST']")
	WebElement rtJobsReportTab;
	
	@FindBy(how = How.XPATH, using = "//div[@id='RUN_A_JOB']//div//button[@type='button']")
	WebElement runJobButton;
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'RT job')]")
	WebElement runRTJobButton;
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Article mapping job')]")
	WebElement articleMappingJobButton;	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Adjustment items (filter)')]")
	WebElement adjustmentItemsFilterButton;	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Confirm')]")
	WebElement confirmButton;

	public void clickOnAdjustmentItemsFilterButton() {
		wait.until(ExpectedConditions.elementToBeClickable(adjustmentItemsFilterButton));
		adjustmentItemsFilterButton.click();
	}

	public void clickOnSimpleFiltersTab() {
		wait.until(ExpectedConditions.elementToBeClickable(simpleFiltersTab));
		simpleFiltersTab.click();
	}
	
	public void clickOnAdvancedFilterTab() {
		wait.until(ExpectedConditions.elementToBeClickable(advancedFilterTab));
		advancedFilterTab.click();
	}
	
	public void clickOnRTJobsReportTab() {
		wait.until(ExpectedConditions.elementToBeClickable(rtJobsReportTab));
		rtJobsReportTab.click();
	}
	
	public void clickOnConfirmButton() {
		wait.until(ExpectedConditions.elementToBeClickable(confirmButton));
		confirmButton.click();
	}
	
	public void clickOnRunJobButton() {
		wait.until(ExpectedConditions.elementToBeClickable(runJobButton));
		runJobButton.click();
	}
	
	public void clickOnRunRTJobButton() {
		wait.until(ExpectedConditions.elementToBeClickable(runRTJobButton));
		runRTJobButton.click();
	}
	
	public void clickOnRunArticleMappingJobButton() {
		wait.until(ExpectedConditions.elementToBeClickable(this.articleMappingJobButton));
		articleMappingJobButton.click();
	}
	
	public void checkText(String text) {
		List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + text + "')]"));
		//Assert.assertTrue("Text not found!", list.size() > 0);
		Assert.assertTrue(list.size() > 0);
	}
		
	// get msg
	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}
	
}
