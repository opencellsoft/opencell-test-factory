package com.opencellsoft.pages.operations.rating;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.opencellsoft.base.TestBase;

public class WalletOperationsListPage extends TestBase {

	WebDriver driver;
	Actions act;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public WalletOperationsListPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
	}
	
	// Using FindBy for locating elements

	@FindBy(how = How.XPATH, using = "//div[@role='alert']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//button[@type='TAB_SIMPLE_FILTERS']")
	WebElement simpleFiltersTab;
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Run a job')]")
	WebElement runJobButton;
	@FindBy(how = How.XPATH, using = "//div[@class='css-95o2bu-CustomActionItem-labelWithIcon'][contains(text(),'Rerate')]")
	WebElement rerateButton;
	@FindBy(how = How.XPATH, using = "//div[@id='reratingTarget']/label[@for='reratingTarget_ALL']")
	WebElement reratingTargetALL;
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Mark to rerate')]")
	WebElement markToRerateButton;
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Confirm')]")
	WebElement confirmButton;
	@FindBy(how = How.XPATH, using = "//button[@id='2']")
	WebElement advancedFilterTab;
	@FindBy(how = How.XPATH, using = "//button[@type = 'TAB_LIST')]")
	WebElement jobsReportsTab;
	@FindBy(how = How.XPATH, using = "//button[@id='4']")
	WebElement reratingBatchesTab;
	@FindBy(how = How.XPATH, using = "//div[@style='display: flex; align-items: center;']//button[@type='button'][contains(text(),'Filter')]")
	WebElement filterButton;


	public void clickOnSimpleFiltersTab() {
		wait.until(ExpectedConditions.elementToBeClickable(simpleFiltersTab));
		simpleFiltersTab.click();
	}
	
	public void clickOnFilterButton() {
		wait.until(ExpectedConditions.elementToBeClickable(filterButton));
		filterButton.click();
	}
	
	public void clickOnAdvancedFilterTab() {
		wait.until(ExpectedConditions.elementToBeClickable(advancedFilterTab));
		advancedFilterTab.click();
	}
	
	public void clickOnReratingBatchesTab() {
		wait.until(ExpectedConditions.elementToBeClickable(reratingBatchesTab));
		reratingBatchesTab.click();
	}
	
	public void clickOnConfirmButton() {
		wait.until(ExpectedConditions.elementToBeClickable(confirmButton));
		confirmButton.click();
	}
	
	public void clickOnRunJobButton() {
		wait.until(ExpectedConditions.elementToBeClickable(runJobButton));
		runJobButton.click();
	}
	
	public void clickOnRerateButton() {
		wait.until(ExpectedConditions.elementToBeClickable(rerateButton));
		rerateButton.click();
	}
	
	public void reratingTargetALL() {
		wait.until(ExpectedConditions.elementToBeClickable(reratingTargetALL));
		reratingTargetALL.click();
	}
	
	
	public void clickOnMarkToRerateButton() {
		wait.until(ExpectedConditions.elementToBeClickable(markToRerateButton));
		markToRerateButton.click();
	}

	
	public void checkText(String text) {
		List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + text + "')]"));
		//Assert.assertTrue("Text not found!", list.size() > 0);
		Assert.assertTrue(list.size() > 0);
	}
		
	// get msg
	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}
	
}
