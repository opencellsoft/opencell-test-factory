package com.opencellsoft.pages.operations.billingrules;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class BillingCyclePage extends TestBase {

	public BillingCyclePage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.XPATH, using = "//input[@id = 'code']")
	WebElement codeInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'description']")
	WebElement descriptionInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'calendar']")
	WebElement calendarSelect;
	@FindBy(how = How.XPATH, using = "//input[@id = 'incrementalInvoiceLines']")
	WebElement incrementalInvoiceLines;
	@FindBy(how = How.XPATH, using = "//input[@id = 'applicationEl']")
	WebElement applicationElInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'enableAggregation']")
	WebElement enableAggregation;
	@FindBy(how = How.XPATH, using = "//div[@id = 'dateAggregation']")
	WebElement dateAggregationInput;
	@FindBy(how = How.XPATH, using = "//div[@id = 'discountAggregation']")
	WebElement discountAggregation;
	@FindBy(how = How.XPATH, using = "//input[@id = 'useAccountingArticleLabel']")
	WebElement useAccountingArticleLabel;
	@FindBy(how = How.XPATH, using = "//input[@id = 'aggregateUnitAmounts']")
	WebElement aggregateUnitPrice;
	@FindBy(how = How.XPATH, using = "//input[@id = 'ignoreSubscriptions']")
	WebElement ignoreSubscriptions;
	@FindBy(how = How.XPATH, using = "//input[@id = 'ignoreOrders']")
	WebElement ignoreOrders;
	@FindBy(how = How.XPATH, using = "//input[@id = 'ignoreUserAccounts']")
	WebElement ignoreConsumers;
	@FindBy(how = How.XPATH, using = "(//table//input[@type='checkbox'])[1]")
	WebElement businessKey;
	@FindBy(how = How.XPATH, using = "(//table//input[@type='checkbox'])[2]")
	WebElement parameter1;
	@FindBy(how = How.XPATH, using = "(//table//input[@type='checkbox'])[3]")
	WebElement parameter2;
	@FindBy(how = How.XPATH, using = "(//table//input[@type='checkbox'])[4]")
	WebElement parameter3;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Save')]")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Aggregation rules')]")
	WebElement aggregationRulesTab;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Split rules')]")
	WebElement splitRulesTab;
	@FindBy(how = How.XPATH, using = "//div[@id = 'type']")
	WebElement splitLevelInput;
	public void setCode(String code) {
		codeInput.sendKeys(code);
	}
	public void setDescription(String description) {
		descriptionInput.sendKeys(description);
	}
	public void selectBillingCalendar(String calendar) {
		clickOn(calendarSelect);
		clickOn(driver.findElement(By.xpath("//table//tr//td//span[contains(text(),'" + calendar + "')]")));
	}
	public void setIncrementalInvoiceLines(Boolean iil) {
		if(iil.equals(true)) {
			clickOn(incrementalInvoiceLines);
		}
	}
	public void setApplicationEl(String applicationEL) {
		applicationElInput.sendKeys(applicationEL);
	}	
	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
	
	public void goToSplitRulesTab() {
		clickOn(splitRulesTab);
	}
	
	public void setSplitLevel(String value) {
		try {
			clickOn(splitLevelInput);
			clickOn(driver.findElement(By.xpath("//li[@data-value = '" + value + "']")));
		}catch (Exception e) {
			clickOn(splitRulesTab);
			clickOn(splitLevelInput);
			clickOn(driver.findElement(By.xpath("//li[@data-value = '" + value + "']")));
		}
		
		
	}
	
	public void goToAggregationRulesTab() {
		clickOn(aggregationRulesTab);
	}

	public void setEnableAggregation(Boolean value) {
		WebElement element = driver.findElement(By.xpath("(//div[@role = 'tabpanel'][5]//span[contains(@class, 'MuiSwitch-switchBase MuiSwitch-colorPrimary')])[1]"));
		String class_name = element.getAttribute("class");
		
		if((value.equals(true) && !class_name.contains("Mui-checked")) || (value.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(enableAggregation);
		}
	}
	public void setDateAggregation(String value) {
		clickOn(dateAggregationInput);
		clickOn(driver.findElement(By.xpath("//li[normalize-space() = '" + value + "']")));
	}
	
	
	public void setDiscountAggregation(String value) {

	}
	
	public void setUseAccountingArticleLabel(Boolean value) {
		WebElement element = driver.findElement(By.xpath("(//div[@role = 'tabpanel'][5]//span[contains(@class, 'MuiSwitch-switchBase MuiSwitch-colorPrimary')])[2]"));
		String class_name = element.getAttribute("class");
		
		if((value.equals(true) && !class_name.contains("Mui-checked")) || (value.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(useAccountingArticleLabel);
		}
	}
	public void setAggregateUnitPrice(Boolean value) {
		WebElement element = driver.findElement(By.xpath("(//div[@role = 'tabpanel'][5]//span[contains(@class, 'MuiSwitch-switchBase MuiSwitch-colorPrimary')])[4]"));
		String class_name = element.getAttribute("class");
		
		if((value.equals(true) && !class_name.contains("Mui-checked")) || (value.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(aggregateUnitPrice);
		}
	}
	public void setIgnoreSubscriptions(Boolean value) {
		WebElement element = driver.findElement(By.xpath("(//div[@role = 'tabpanel'][5]//span[contains(@class, 'MuiSwitch-switchBase MuiSwitch-colorPrimary')])[3]"));
		String class_name = element.getAttribute("class");
		
		if((value.equals(true) && !class_name.contains("Mui-checked")) || (value.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(ignoreSubscriptions);
		}
	}
	public void setIgnoreOrders(Boolean value) {
		WebElement element = driver.findElement(By.xpath("(//div[@role = 'tabpanel'][5]//span[contains(@class, 'MuiSwitch-switchBase MuiSwitch-colorPrimary')])[5]"));
		String class_name = element.getAttribute("class");
		
		if((value.equals(true) && !class_name.contains("Mui-checked")) || (value.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(ignoreOrders);
		}
	}
	public void setIgnoreConsumers(Boolean value) {
		WebElement element = driver.findElement(By.xpath("(//div[@role = 'tabpanel'][5]//span[contains(@class, 'MuiSwitch-switchBase MuiSwitch-colorPrimary')])[6]"));
		String class_name = element.getAttribute("class");
		
		if((value.equals(true) && !class_name.contains("Mui-checked")) || (value.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(ignoreConsumers);
		}
	}
	public void setBusinessKey(Boolean value) {
		WebElement element = driver.findElement(By.xpath("(//div[@role = 'tabpanel'][5]//span[contains(@class, 'MuiIconButton-colorPrimary')])[1]"));
		String class_name = element.getAttribute("class");
		
		if((value.equals(true) && !class_name.contains("Mui-checked")) || (value.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(businessKey);
		}
	}
	public void setParameter1(Boolean value) {
		WebElement element = driver.findElement(By.xpath("(//div[@role = 'tabpanel'][5]//span[contains(@class, 'MuiIconButton-colorPrimary')])[2]"));
		String class_name = element.getAttribute("class");
		
		if((value.equals(true) && !class_name.contains("Mui-checked")) || (value.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(parameter1);
		}
	}
	public void setParameter2(Boolean value) {
		WebElement element = driver.findElement(By.xpath("(//div[@role = 'tabpanel'][5]//span[contains(@class, 'MuiIconButton-colorPrimary')])[3]"));
		String class_name = element.getAttribute("class");
		
		if((value.equals(true) && !class_name.contains("Mui-checked")) || (value.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(parameter2);
		}
	}
	public void setParameter3(Boolean value) {
		WebElement element = driver.findElement(By.xpath("(//div[@role = 'tabpanel'][5]//span[contains(@class, 'MuiIconButton-colorPrimary')])[4]"));
		String class_name = element.getAttribute("class");
		
		if((value.equals(true) && !class_name.contains("Mui-checked")) || (value.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(parameter3);
		}
	}
	
}
