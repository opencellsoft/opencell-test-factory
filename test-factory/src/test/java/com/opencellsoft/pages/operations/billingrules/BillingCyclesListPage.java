package com.opencellsoft.pages.operations.billingrules;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class BillingCyclesListPage extends TestBase {
	
	public BillingCyclesListPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//*[@data-testid = 'AddIcon' or @class  ='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-textSizeSmall MuiButton-sizeSmall']")
	WebElement createNewBillingCycleButton;
	
	public void clickOnCreateNewBillingCycleButton() {
		clickOn(createNewBillingCycleButton);
	}
	
}
