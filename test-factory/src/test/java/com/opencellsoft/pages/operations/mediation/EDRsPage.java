package com.opencellsoft.pages.operations.mediation;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class EDRsPage extends TestBase {

	public EDRsPage() {
		PageFactory.initElements(driver, this);
	}
		
	@FindBy(how = How.XPATH, using = "//*[contains(@class,'MuiSvgIcon-root MuiChip-deleteIcon')]")
	WebElement eventKeyDeleteButton;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter' or @title = 'Filter']")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//li[@data-key='subscription']//span[normalize-space()='Subscription']")
	WebElement subscriptionOption;	
	@FindBy(how = How.ID, using = "subscription")
	WebElement subscriptionInput;	
	
	@FindBy(how = How.XPATH, using = "//span[@type='number' and normalize-space()='1']")
	List<WebElement> eventVersion1;	
	@FindBy(how = How.XPATH, using = "//span[@type='number' and normalize-space()='2']")
	List<WebElement> eventVersion2;	
	@FindBy(how = How.XPATH, using = "//span[@type='number' and normalize-space()='3']")
	List<WebElement> eventVersion3;	
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Rated']")
	List<WebElement> ratedStatus;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Cancelled']")
	List<WebElement> cancelledStatus;
	@FindBy(how = How.XPATH, using = "//button[@title='Refresh']")
	WebElement refreshButton;	

	
	public void clickOnEventKeyDeleteButton() {
		clickOn(eventKeyDeleteButton);	
	}
	public void clickOnRefreshButton() {
		refreshButton.click();	
	}
	
	public void clickOnFilterButton() {
		clickOn(filterButton);
	}
	public void clickOnSubscriptionOption() {
		clickOn(subscriptionOption);
	}	
	public void setSubscriptionCode(String subscriptionCode) {
		subscriptionInput.sendKeys(subscriptionCode);
	}
	// return number of elements with eventVersion= 1
	public int eventVersion1Size() {
		return eventVersion1.size();
	}
	// return number of elements with eventVersion= 2
	public int eventVersion2Size() {
		return eventVersion2.size();
	}
	// return number of elements with eventVersion= 3
		public int eventVersion3Size() {
			return eventVersion3.size();
		}
	public int getEventKeyAndParameter3FirstLineSize(String param3) {
		List<WebElement> EventKeyAndParam3 = driver.findElements(By.xpath("//td[normalize-space()='"+ param3 + "']"));
		return EventKeyAndParam3.size();
	}

	public int getEventKeyAndParameter3ELSecondLineSize(String param3EL) {
		List<WebElement> EventKeyAndParam3 = driver.findElements(By.xpath("//td[normalize-space()='"+ param3EL + "']"));
		return EventKeyAndParam3.size();
	}
	// return number of elements with value= RATED
	public int ratedStatusSize() {
		return ratedStatus.size();
	}
	// return number of elements with value= CANCELLED
	public int cancelledStatusSize() {
		return cancelledStatus.size();
	}	
	
	public int checkAccessPoint(String accessPoint) {
		List<WebElement> AccessPoint = driver.findElements(By.xpath("//td[normalize-space()='"+ accessPoint + "']"));
		return AccessPoint.size();
	}	
	public int checkParameter2FirstLine(String param2) {
		List<WebElement> parameter2FirstLine = driver.findElements(By.xpath("//td[normalize-space()='"+ param2 + "']"));
		return parameter2FirstLine.size();
	}	
	public int checkParameter2ELSecondLine(String param2EL) {
		List<WebElement> parameter2SecondLine = driver.findElements(By.xpath("//td[normalize-space()='"+ param2EL + "']"));
		return parameter2SecondLine.size();
	}
}
