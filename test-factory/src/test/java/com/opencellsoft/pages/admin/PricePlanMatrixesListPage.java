package com.opencellsoft.pages.admin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class PricePlanMatrixesListPage extends TestBase {

	public PricePlanMatrixesListPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//input[@id='searchForm:code_txt']")
	WebElement codeInput;
	@FindBy(how = How.XPATH, using = "//input[@id='searchForm:eventCode_txt' or @id='searchForm:code_txt']")
	WebElement chargeCodeInput;
	@FindBy(how = How.XPATH, using = "//button[@id='searchForm:buttonSearch']")
	WebElement searchButton;
	
	public void setCodeInput(String code) {
		codeInput.sendKeys(code);
	}
	
	public void setChargeCodeInput(String code) {
		chargeCodeInput.sendKeys("*"+code);
	}
	
	public void clickOnPricePlanLine(String code) {
		driver.findElement(By.xpath("//a[contains(text(),'" + code +"')]")).click();	
	}
	
	public void clickOnSearchButton() {
		searchButton.click();	
	}
}
