package com.opencellsoft.pages.admin;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.opencellsoft.base.TestBase;

public class NewCustomFieldPage extends TestBase {

	public NewCustomFieldPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//input[@id = 'cftForm:code_txt']")
	WebElement codeInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'cftForm:description']")
	WebElement labelInput;
	@FindBy(how = How.XPATH, using = "//select[@id = 'cftForm:fieldType_enum_input']")
	WebElement typeSelect;
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Save')])[3]")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//div[@id = 'cftForm:fieldType_enum']")
	WebElement fieldType_enum;
	@FindBy(how = How.XPATH, using = "//li[@id = 'cftForm:fieldType_enum_6']")
	WebElement fieldType_enum_6;

	public void setCode(String code) {
		codeInput.sendKeys(code);
	}

	public void setLabel(String label) {
		labelInput.click();
		labelInput.sendKeys(label);
	}

	public void selectType(String type) {
		fieldType_enum.click();
		waitPageLoaded();
		driver.findElement(By.xpath("//li[contains(text(), '" + type + "')]")).click();
		//fieldType_enum_6.click();

	}

	public void clickOnSaveButton() {
		waitPageLoaded();
		//driver.findElement(By.xpath("//div[@id = 'cftDialog_modal']")).click();
		
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", saveButton);
//		clickOn(saveButton);
//		saveButton.click();	
	}
}
