package com.opencellsoft.pages.admin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class AdminHomePage extends TestBase {
	
	public AdminHomePage() {
		PageFactory.initElements(driver, this);
	}
	
//	Actions actions = new Actions(driver);
	
	@FindBy(how = How.XPATH, using = "//form[@id='menu']//span[text()='Catalog']")
	WebElement catalogMenu;
	@FindBy(how = How.XPATH, using = "//form[@id='menu']//span[text()='Service Management']")
	WebElement serviceManagementMenu;
	@FindBy(how = How.XPATH, using = "//form[@id='menu']//span[text()='Charges']")
	WebElement chargesMenu;
	@FindBy(how = How.XPATH, using = "//form[@id='menu']//span[text()='Usages']")
	WebElement usageMenu;
	@FindBy(how = How.XPATH, using = "//form[@id='menu']//span[text()='Triggered EDR']")
	WebElement triggerEdrMenu;
	@FindBy(how = How.XPATH, using = "//form[@id='menu']//span[text()='Price plans']")
	WebElement pricePlans1Menu;
	@FindBy(how = How.XPATH, using = "//form[@id='menu']//a[@id='menu:pricePlanMatrixes']")
	WebElement pricePlans2Menu;
	@FindBy(how = How.XPATH, using = "//form[@id='menu']//span[text()='Administration']")
	WebElement administrationMenu;
	@FindBy(how = How.XPATH, using = "//form[@id='menu']//span[text()='Jobs']")
	WebElement jobsMenu;
	@FindBy(how = How.XPATH, using = "//form[@id='menu']//span[text()='Job instances']")
	WebElement jobInstancesMenu;
	@FindBy(how = How.XPATH, using = "//form[@id='menu']//span[text()='Script']")
	WebElement scriptMenu;
	@FindBy(how = How.XPATH, using = "//form[@id='menu']//a[@id='menu:scriptInstances']")
	WebElement scriptsMenu;
	@FindBy(how = How.XPATH, using = "//form[@id='menu']//a[@id='menu:customizedEntities']")
	WebElement entityCustomizationMenu;
	
	public void MoveToCatalogMenu() {
		Actions actions = new Actions(driver);
		actions.moveToElement(catalogMenu).perform();
	}
	public void MoveToServiceManagementMenu() {
		Actions actions = new Actions(driver);
		actions.moveToElement(serviceManagementMenu).perform();
	}	
	public void MoveToChargesMenu() {
		Actions actions = new Actions(driver);
		actions.moveToElement(chargesMenu).perform();
	}
	
	public void MoveToPricePlans() {
		Actions actions = new Actions(driver);
		try {
			actions.moveToElement(pricePlans1Menu).perform();
		}catch (Exception e) {
			driver.navigate().refresh();
			waitPageLoaded();
			actions.moveToElement(pricePlans1Menu).perform();
		}
	}
	
	public void clickOnUsageMenu() {
		clickOn(usageMenu);
	}	

	public void clickOnPricePlans() {
		Actions actions = new Actions(driver);
		try {
			actions.moveToElement(pricePlans2Menu).click().perform();
		}catch (Exception e) {
			clickOn(pricePlans2Menu);
		}
	}
	
	public void clickOnTriggerEdrMenu() {
		Actions actions = new Actions(driver);
		actions.moveToElement(triggerEdrMenu).click().perform();
	}
	public void clickOnJobInstancesMenu() {
		Actions actions = new Actions(driver);
		actions.moveToElement(jobInstancesMenu).click().perform();
	}
	
	public void MoveToAdministrationMenu() {
		Actions actions = new Actions(driver);
		actions.moveToElement(administrationMenu).perform();
	}
	
	public void MoveToJobsMenu() {
		Actions actions = new Actions(driver);
		actions.moveToElement(jobsMenu).perform();
	}
	
	
	public void MoveToScriptMenu() {
		Actions actions = new Actions(driver);
		actions.moveToElement(scriptMenu).perform();
	}
	
	public void clickOnScriptsMenu() {
		Actions actions = new Actions(driver);
		actions.moveToElement(scriptsMenu).click().perform();
	}
	
	public void clickOnEntityCustomizationMenu() {
		clickOn(entityCustomizationMenu);
	}
}
