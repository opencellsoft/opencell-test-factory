package com.opencellsoft.pages.admin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class UsageChargesListPage extends TestBase {
	public UsageChargesListPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "searchForm:code_txt")
	WebElement searchCodetxt;
	
	@FindBy(how = How.ID, using = "searchForm:buttonSearch")
	WebElement searchButton;
	
	public void SetChargeCode(String chargeCode) {
		searchCodetxt.sendKeys(chargeCode);
	}
	
	public void clickOnsearchButton() {
		clickOn(searchButton);
	}

	public void clickOnChargeCodeLink(String chargeCode) {
		try {
			WebElement  ChagreLink = driver.findElement(By.xpath("//tbody//a[text()='" + chargeCode + "']"));
			clickOn(ChagreLink);
		}catch (Exception e) {
			WebElement  ChagreLink = driver.findElement(By.xpath("//tbody//a[text()='" + chargeCode + "']"));
			clickOn(ChagreLink);
		}
		
	}

}
