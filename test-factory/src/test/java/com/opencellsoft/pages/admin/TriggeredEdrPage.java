package com.opencellsoft.pages.admin;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class TriggeredEdrPage extends TestBase {
	public TriggeredEdrPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//button//span[text()='New']")
	WebElement newButton;
	@FindBy(how = How.XPATH, using = "//input[@id='searchForm:code_txt']")
	WebElement searchingCodeInput;
	@FindBy(how = How.XPATH, using = "//span[text()='Search']")
	WebElement searchButton;
	
	public void clickOnNewButton() {
		newButton.click();	
	}
	
	public void setSearchingCode(String TriggeredEDRCode) {
		searchingCodeInput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
		searchingCodeInput.sendKeys(TriggeredEDRCode);	
	}
	
	public void clickOnSearchButton() {
		searchButton.click();	
	}
	
	public void clickOnSearchResult(String code) {
		WebElement searchResult = driver.findElement(By.xpath("//a[text()='" + code +"']"));
		searchResult.click();	
	}
	
}
