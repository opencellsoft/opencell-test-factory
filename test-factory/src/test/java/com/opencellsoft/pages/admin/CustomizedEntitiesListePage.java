package com.opencellsoft.pages.admin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class CustomizedEntitiesListePage extends TestBase  {
	public CustomizedEntitiesListePage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//span[text()='New']")
	WebElement newButton;
	
	public void clickOnNewButton() {
		newButton.click();	
	}
}
