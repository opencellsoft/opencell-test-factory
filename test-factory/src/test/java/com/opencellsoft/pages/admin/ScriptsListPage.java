package com.opencellsoft.pages.admin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class ScriptsListPage extends TestBase {
	
	public ScriptsListPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//button//span[text()='New']")
	WebElement newButton;
	
	public void clickOnNewButton() {
		newButton.click();	
	}
}
