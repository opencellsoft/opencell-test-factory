package com.opencellsoft.pages.admin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class PricePlanMatrixDetailPage extends TestBase {

	public PricePlanMatrixDetailPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//input[@id='formPricePlan:tabView:amountWithoutTax_number']")
	WebElement amountWithoutTaxInput;
	@FindBy(how = How.XPATH, using = "//button[@id='formPricePlan:tabView:scriptSelectId_selectLink']")
	WebElement scriptSelectIdButton;
	@FindBy(how = How.XPATH, using = "//input[@id='pricePlanMatScriptPopuppopupForm:searchField1']")
	WebElement searchPricePlanMatScriptInput;
	@FindBy(how = How.XPATH, using = "//button[@id='pricePlanMatScriptPopuppopupForm:buttonSearch']")
	WebElement searchPricePlanMatScriptButton;
	@FindBy(how = How.XPATH, using = "//tbody//tr")
	WebElement firstResult;
	@FindBy(how = How.XPATH, using = "//span[text() = 'Save']")
	WebElement saveButton;
	public void setAmountWithoutTax(String amount) {
		amountWithoutTaxInput.sendKeys(amount);
	}
	public void clickOnScriptSelectIdButton() {
		scriptSelectIdButton.click();	
	}
	public void setsearchPricePlanMatScript(String script) {
		searchPricePlanMatScriptInput.sendKeys(script);
	}
	public void clickOnSearchPricePlanMatScriptButton() {
		searchPricePlanMatScriptButton.click();	
	}
	
	public void selectPricePlanMatScriptLine(String script) {
		driver.findElement(By.xpath("//tr[text() = '"+ script +"']")).click();	
	}
	
	public void selectFirstResult() {
		firstResult.click();	
	}
	public void clickOnSaveButton() {
		saveButton.click();	
	}
}
