package com.opencellsoft.pages.admin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class UsageChargeDetailPage extends TestBase {

	public UsageChargeDetailPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//button[@title='Add']")
	WebElement addButton;
	@FindBy(how = How.XPATH, using = "//button//span[text()='Save']")
	WebElement saveButton;
	@FindBy(how = How.ID, using = "usageChargeId:tabView:invoiceSubCategorySelectedId_label")
	WebElement SubCategorySelect;
	@FindBy(how = How.XPATH, using = "//ul//li[text()='ISCAT_DEFAULT']")
	WebElement SubCategory_ISCAT_DEFAULT;
	@FindBy(how = How.ID, using = "usageChargeId:tabView:triggerNextCharge_bool")
	WebElement triggerNextCharge;
	@FindBy(how = How.XPATH, using = "(//button//span[text()='Yes'])[2]")
	WebElement yesButton;
	
	public void clickOnTriggeredEdr(String TriggeredEdrCode) {
		WebElement  TriggeredEdr = driver.findElement(By.xpath("//ul//li[@data-item-label='" + TriggeredEdrCode + "']"));
		Actions action = new Actions(driver); 
		action.moveToElement(TriggeredEdr).click().perform();
		waitPageLoaded();
	}
	
	public void clickOnAddButton() {
		clickOn(addButton);

	}

	public void selectSubCategory() {
		clickOn(SubCategorySelect);
		clickOn(SubCategory_ISCAT_DEFAULT);
	}
	
	public void checkTriggerNextCharge() {
		clickOn(triggerNextCharge);
	}	
	
	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
	public void clickOnYesButton() {
		clickOn(yesButton);
	}	
	
}
