package com.opencellsoft.pages.admin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class TriggeredEdrUpdatePage extends TestBase {
	
	public TriggeredEdrUpdatePage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "triggeredTemplateId:conditionEl_txt")
	WebElement conditionEL;	
	@FindBy(how = How.ID, using = "triggeredTemplateId:param3El_txt")
	WebElement param3EL;
	@FindBy(how = How.ID, using = "triggeredTemplateId:quantityEl_txt")
	WebElement quantityInput;
	@FindBy(how = How.XPATH, using = "//button//span[text()='Save']")
	WebElement saveButton;
	
	public void Setparam3EL(String param3) {
		param3EL.clear();
		param3EL.sendKeys(param3);
	}
	public void setQuantity(String quantity) {
		quantityInput.clear();
		quantityInput.sendKeys(quantity);
	}
	
	public void SetConditionEL(String condition) {
		conditionEL.clear();
		conditionEL.sendKeys(condition);
	}
	
	public void clickOnSaveButton() {
		saveButton.click();	
	}
}
