package com.opencellsoft.pages.admin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class NewCustomizedEntityPage extends TestBase   {
	public NewCustomizedEntityPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(how = How.ID, using = "cetForm:code_txt")
	WebElement codeInput;
	@FindBy(how = How.ID, using = "cetForm:name_txt")
	WebElement nameInput;
	@FindBy(how = How.ID, using = "cetForm:description_txt")
	WebElement descriptionInput;
	@FindBy(how = How.XPATH, using = "(//span[contains(@class,'ui-chkbox-icon ui-icon')])[1]")
	WebElement separateTableCheckbox;
	@FindBy(how = How.ID, using = "fieldsForm:fields:0:addFieldInTabBtn")
	WebElement addFieldButton;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Save']")
	WebElement saveButton;
	
	public void SetCode(String code) {
		codeInput.sendKeys(code);
	}

	public void SetName(String name) {
		nameInput.sendKeys(name);
	}
	
	public void SetDescription(String description) {
		descriptionInput.sendKeys(description);
	}
	
	public void checkSeparateTableCheckbox() {
		separateTableCheckbox.click();	
	}
	
	public void clickOnSaveButton() {
		saveButton.click();	
	}
	
	public void clickOnAddFieldButton() {
		clickOn(addFieldButton);
		//addFieldButton.click();	
	}
	
}
