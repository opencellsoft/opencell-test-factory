package com.opencellsoft.pages.admin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class NewTriggeredEDRPage extends TestBase{
	public NewTriggeredEDRPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(how = How.ID, using = "triggeredTemplateId:code_txt")
	WebElement code;
	@FindBy(how = How.ID, using = "triggeredTemplateId:description")
	WebElement description;
	@FindBy(how = How.ID, using = "triggeredTemplateId:conditionEl_txt")
	WebElement conditionEL;	
	@FindBy(how = How.ID, using = "triggeredTemplateId:quantityEl_txt")
	WebElement quantityEL;
	@FindBy(how = How.ID, using = "triggeredTemplateId:param1El_txt")
	WebElement param1EL;
	@FindBy(how = How.ID, using = "triggeredTemplateId:param2El_txt")
	WebElement param2EL;
	@FindBy(how = How.ID, using = "triggeredTemplateId:param3El_txt")
	WebElement param3EL;
	@FindBy(how = How.XPATH, using = "//button//span[text()='Save']")
	WebElement saveButton;
	
	public void SetCode(String triggerEdrCode) {
		code.sendKeys(triggerEdrCode);
	}
	
	public void SetDescription(String triggerEdrDescription) {
		description.sendKeys(triggerEdrDescription);
	}
	
	public void SetConditionEL(String condition) {
		conditionEL.sendKeys(condition);
	}
	
	public void SetQuantityEL(String quantity) {
		quantityEL.sendKeys(quantity);
	}
	
	public void Setparam1EL(String param1) {
		param1EL.sendKeys(param1);
	}
	
	public void Setparam2EL(String param2) {
		param2EL.sendKeys(param2);
	}
	
	public void Setparam3EL(String param3) {
		param3EL.sendKeys(param3);
	}
	
	public void clickOnSaveButton() {
		saveButton.click();	
	}
}
