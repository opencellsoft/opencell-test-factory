package com.opencellsoft.pages.admin;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class NewScriptPage extends TestBase {

	public NewScriptPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID, using = "scriptInstanceForm:codeMirror")
	WebElement scriptSource;

	@FindBy(how = How.XPATH, using = "//span[text() = 'Validate / Compile']")
	WebElement validateCompileButton;
	@FindBy(how = How.XPATH, using = "//span[text() = 'Save']")
	WebElement saveButton;

	String ValoCapScript = " package org.meveo.service.script; "
			+ "import org.apache.commons.lang3.StringUtils; "
			+ "import org.json.simple.parser.JSONParser; "
			+ "import org.json.simple.parser.ParseException; "
			+ "import org.meveo.model.billing.WalletOperation; "
			+ "import org.meveo.model.rating.EDR; "
			+ "import javax.persistence.Query; "
			+ "import java.math.BigDecimal; "
			+ "import java.math.MathContext; "
			+ "import java.time.ZoneOffset; "
			+ "import java.util.List; "
			+ "import java.util.Map; "
			+ "import org.json.simple.JSONObject; "
			+ "import org.meveo.service.custom.CustomTableService; "
			+ "import org.meveo.service.billing.impl.UsageRatingService; "
			+ "public class ValoCap extends Script { "
			+ "    private static final long serialVersionUID = 1L; "
			+ "    private UsageRatingService usageRatingService = getServiceInterface(\"UsageRatingService\"); "
			+ "    @Override public void execute(Map < String, Object > context) { "
			+ "        log.info(\"Start execution MappingEdrToWallet\"); "
			+ "        Object entity = context.get(Script.CONTEXT_ENTITY); "
			+ "        if (entity instanceof WalletOperation) { "
			+ "            WalletOperation wo = (WalletOperation) entity; "
			+ "            log.debug(\"Wallet Operation Code ={}\", wo.getCode()); "
			+ "            EDR edr = wo.getEdr(); "
			+ "            log.debug(\"Created EDR : {}\", edr); "
			+ "            BigDecimal UnitAmountWithoutTax = new BigDecimal(5); "
			+ "            wo.setUnitAmountWithoutTax(UnitAmountWithoutTax); "
			+ "            log.info(\"usageRatingService => : \" + usageRatingService); "
			+ "            usageRatingService.calculateAmounts(wo, UnitAmountWithoutTax, UnitAmountWithoutTax); "
			+ "            log.info(\"End execution\"); "
			+ "        } "
			+ "    } "
			+ "} "
			+ "";

	String eligibilityUtilsScript = "package org.meveo.service.script;import org.meveo.model.billing.WalletOperation;import org.meveo.model.catalog.PricePlanMatrix;import org.meveo.model.catalog.PricePlanMatrixLine;import org.meveo.model.catalog.PricePlanMatrixVersion;import org.meveo.model.cpq.contract.Contract;import org.meveo.model.cpq.contract.ContractItem;import org.meveo.model.cpq.contract.ContractRateTypeEnum;import org.meveo.service.billing.impl.PricePlanSelectionService;import java.math.BigDecimal;import java.util.List;import java.util.Map;import java.util.stream.Collectors; "
			+ "public class EligibilityUtils extends Script { "
			+ "    private static final long serialVersionUID = 1L; "
			+ "    PricePlanSelectionService pricePlanSelectionService = (PricePlanSelectionService) getServiceInterface(PricePlanSelectionService.class.getSimpleName()); "
			+ "    @Override "
			+ "    public void execute(Map<String, Object> context) { "
			+ "        log.info(\"Start execution Eligibility Utils\");Object entity = context.get(Script.CONTEXT_ENTITY);context.put(Script.RESULT_VALUE, false); "
			+ "        if (entity instanceof WalletOperation) { WalletOperation walletOperation = (WalletOperation) entity;log.trace(\"Wallet operation = {}\", walletOperation);context.put(Script.RESULT_VALUE, isADiscountedWOOrHasNoDiscountedWO(walletOperation));} "
			+ "        log.info(\"End execution Eligibility Utils, result found : {}\", context.get(Script.RESULT_VALUE)); "
			+ "    } "
			+ "    public boolean isChargeDiscounted(WalletOperation walletOperation) { "
			+ "        boolean hasDiscount = false; "
			+ "        if (walletOperation.getContract() != null) {Contract contract = walletOperation.getContract();log.debug(\"Found contract for wallet {} with operation date {} : {}\",walletOperation.getCode(), walletOperation.getOperationDate(), contract); "
			+ "            List<ContractItem> contractItems = contract.getContractItems().stream().filter(contractItem ->walletOperation.getChargeInstance().getCode().equals(contractItem.getChargeTemplate().getCode())&& contractItem.getContractRateType().equals(ContractRateTypeEnum.PERCENTAGE)).collect(Collectors.toList()); "
			+ "            if (!contractItems.isEmpty()) { "
			+ "                ContractItem contractItem = contractItems.get(0); "
			+ "                log.debug(\"Found contract item for wallet {} with operation date {} : {}\", "
			+ "                        walletOperation.getCode(), walletOperation.getOperationDate(), contractItem); "
			+ "                if (contractItem.getPricePlan() == null) { "
			+ "                    hasDiscount = true; "
			+ "                } else { "
			+ "                    PricePlanMatrix discountPricePlan = contractItem.getPricePlan(); "
			+ "                    PricePlanMatrixLine matchedLine = pricePlanSelectionService.determinePricePlanLine(discountPricePlan, walletOperation); "
			+ "                    hasDiscount = matchedLine != null; "
			+ "                } "
			+ "            } "
			+ "        } else { "
			+ "            log.debug(\"Found no contract for wallet {} with operation date {}\", walletOperation.getId(), walletOperation.getOperationDate()); "
			+ "        } "
			+ "        log.trace(\"isChargeDiscounted : result={}\", hasDiscount); "
			+ "        return hasDiscount; "
			+ "    } "
			+ " "
			+ "    public boolean isADiscountedWOOrHasNoDiscountedWO(WalletOperation walletOperation) { "
			+ "        return (walletOperation.getAmountWithoutTax().compareTo(BigDecimal.ZERO) <= 0 || walletOperation.getDiscountedWalletOperation() != null || !isChargeDiscounted(walletOperation)); "
			+ "    } "
			+ "} "
			+ "";

	
	public void setValoCapScriptSource() {
		System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<     ValoCapScript     >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		System.out.println(ValoCapScript);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].innerText = '" + ValoCapScript +"'", scriptSource);
	}

	public void setEligibilityUtilsScriptSource() {
		System.out.println(eligibilityUtilsScript);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].innerText = '" + eligibilityUtilsScript +"'", scriptSource);
		
	}
	
	public void clickOnValidateCompileButton() {
		validateCompileButton.click();	
	}

	public void clickOnSaveButton() {
		saveButton.click();	
	}
}
