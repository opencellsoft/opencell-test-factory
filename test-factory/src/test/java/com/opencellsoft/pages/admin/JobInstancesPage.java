package com.opencellsoft.pages.admin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class JobInstancesPage extends TestBase {
	public JobInstancesPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//button//span[text()='New']")
	WebElement newButton;
	@FindBy(how = How.XPATH, using = "//input[@id='searchForm:code_txt']")
	WebElement searchingCodeInput;
	@FindBy(how = How.XPATH, using = "//span[text()='Search']")
	WebElement searchButton;
	@FindBy(how = How.XPATH, using = "//*[@id='formId:tabView:timerFollowingJob_label']")
	WebElement nextJobSelect;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Save']")
	WebElement saveButton;
	public void clickOnNewButton() {
		clickOn(newButton);
	}
	
	public void setSearchingCode(String value) {
		searchingCodeInput.sendKeys(value);	
	}
	
	public void clickOnSearchButton() {
		clickOn(searchButton);
	}
	
	public void clickOnSearchResult(String code) {
		WebElement searchResult = driver.findElement(By.xpath("//a[text()='" + code +"']"));
		clickOn(searchResult);
	}
	
	public void SetNextJob(String value) {
		clickOn(nextJobSelect);
		WebElement searchResult = driver.findElement(By.xpath("//li[text()='" + value +"']"));
		clickOn(searchResult);
	}
	
	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
}
