package com.opencellsoft.pages.invoices;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.opencellsoft.base.TestBase;
import com.opencellsoft.utility.Constant;

public class NewInvoiceLinePage extends TestBase {
	WebDriver driver;
	Wait<WebDriver> fWait;
	WebDriverWait wait;
	public NewInvoiceLinePage(WebDriver driver) {
		this.driver = driver;
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(100)).pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
	}

//	@FindBy(how = How.XPATH, using = "//div[@class='MuiGrid-root gridBlock fullWidthChild fullWidthChild MuiGrid-container MuiGrid-grid-xs-6 MuiGrid-grid-sm-6']//div[@class='MuiInputBase-root MuiFilledInput-root MuiFilledInput-underline MuiInputBase-formControl MuiInputBase-adornedEnd MuiFilledInput-adornedEnd MuiInputBase-marginDense MuiFilledInput-marginDense']")
	@FindBy(how = How.CSS, using = ".MuiInputBase-root.MuiFilledInput-root.MuiFilledInput-underline.Mui-error.Mui-error.MuiInputBase-formControl.MuiInputBase-adornedEnd.MuiFilledInput-adornedEnd.jss216.MuiInputBase-marginDense.MuiFilledInput-marginDense']")
	WebElement articleDiv;
	@FindBy(how = How.XPATH, using = "//input[@name='article']")
	WebElement articleInput;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement codeInput;
	@FindBy(how = How.CSS, using = "tbody tr:nth-child(1)")
	WebElement article;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement descriptionInput;
	@FindBy(how = How.XPATH, using = "//input[@id='quantity']")
	WebElement quantityInput;
	@FindBy(how = How.XPATH, using = "//input[@id='unitPrice' or @id='functionalCurrency']")
	WebElement priceInput;
	@FindBy(how = How.XPATH, using = "//*[normalize-space()='Save']")
	WebElement saveInvoiceLineButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackInvoiceLineButton;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiGrid-root gridBlock fullWidthChild fullWidthChild MuiGrid-container MuiGrid-grid-xs-6']//div[@class='MuiInputBase-root MuiFilledInput-root MuiFilledInput-underline MuiInputBase-formControl MuiInputBase-adornedEnd MuiFilledInput-adornedEnd jss216 MuiInputBase-marginDense MuiFilledInput-marginDense']")
	WebElement discountPlanDiv;
	@FindBy(how = How.XPATH, using = "//input[@id='discountPlan']")
	WebElement discountPlanInput;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Code')]")
	WebElement filterByCodeOption;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement discountPlanCodeInput;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]")
	WebElement discountPlanFound;
	@FindBy(how = How.XPATH, using = "//label[@for = 'taxMode_TAX']//input[@id ='taxMode_TAX']")
	WebElement selecteTax;
	@FindBy(how = How.XPATH, using = "//input[@id ='tax']")
	WebElement tax;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'TAX_00')]")
	WebElement tax00;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'TAX_20')]")
	WebElement tax20;
	@FindBy(how = How.NAME, using = "article")
	WebElement articleLabel;
	
	//get message from textBox
	public String getTextMessage() {
		fWait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

	public void selectArticle(String code) {
		WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		articleLabel.click();
		wait.until(ExpectedConditions.visibilityOf(articleInput));
		articleInput.click();
		codeInput.sendKeys(code);
		wait.until(ExpectedConditions.visibilityOf(article));
		article.click();
	}
	
	//set  invoice line price
	public void setInvoiceLinePrice(String price) {
		priceInput.sendKeys(price);
	}

	// set invoice line quantity
	public void setInvoiceLineQuantity(String quantity) {
		quantityInput.sendKeys(quantity);
	}

	// set Description
	public void setInvoiceLineDescription(String description) {
		wait.until(ExpectedConditions.elementToBeClickable(descriptionInput));
		descriptionInput.sendKeys(description);

	}
	
	// this method is to select an article for an invoice line
	public void setArticle(String code) {
		wait.until(ExpectedConditions.elementToBeClickable(articleInput));
		articleInput.click();
		waitPageLoaded();
		codeInput.sendKeys(code);
		waitPageLoaded();
		try {
			clickOn(article);
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(article));
			clickOn(article);
		}	
	}
	
	// click on save button
	public void clickOnSaveInvoiceLineButton() {
		wait.until(ExpectedConditions.elementToBeClickable(saveInvoiceLineButton));
		clickOn(saveInvoiceLineButton);
	}
	
	//	click on go back button 
	public void clickGoBack() {
		try {
			clickOn(goBackInvoiceLineButton);
		}
		catch (Exception e) {
			wait.until(ExpectedConditions.elementToBeClickable(goBackInvoiceLineButton));
			clickOn(goBackInvoiceLineButton);
		}
				
	}

	//Verifying article code input text
	public String getArticleCodeInputText() {
		return codeInput.getAttribute("value");
	}

	//Verifying invoice description input text
	public String getInvoiceDescriptionInputText() {
		return descriptionInput.getAttribute("value");
	}

	public void clickOnDiscountPlanInput() {
		// TODO Auto-generated method stub
//		discountPlanDiv.click();
		wait.until(ExpectedConditions.visibilityOf(discountPlanInput));
		discountPlanInput.click();
	}
	public void clickOnFilterButton() {
		filterButton.click();
	}
	
	public void selectFilterByCodeOption() {
		filterByCodeOption.click();
	}
	
	public void setDiscountPlanCodeFilter(String discountPlanCode) {
		discountPlanCodeInput.sendKeys(discountPlanCode);
	}
	public void selectFoundDiscountPlan() {
		discountPlanFound.click();		
	}
	
	public void selectTax00() {
		selecteTax.click();
		waitPageLoaded();
		tax.click();
		waitPageLoaded();
		tax00.click();
		waitPageLoaded();
	}
	
	public void selectTax20() {
		selecteTax.click();
		waitPageLoaded();
		tax.click();
		waitPageLoaded();
		tax20.click();
		waitPageLoaded();
	}
	public void selectTax(String taxCode) {
		clickOn(selecteTax);
		clickOn(tax);
		clickOn(driver.findElement(By.xpath("//span[contains(text(),'TAX_" + taxCode + "')]")));
	}

	public void createNewInvoiceLineMethod(String articleCode, String invoiceQuantity, String invoicePrice, String taxCode) {
		this.setArticle(articleCode);
		this.setInvoiceLineDescription(Constant.invoiceLineDescription);
		this.setInvoiceLineQuantity(invoiceQuantity);
		this.setInvoiceLinePrice(invoicePrice);
		this.selectTax(taxCode);
		this.clickOnSaveInvoiceLineButton();
		waitPageLoaded();
		this.clickGoBack();
	}
}
