package com.opencellsoft.pages.invoices;

import java.time.Duration;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class InvoiceLinePage {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public InvoiceLinePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10)).pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);

	}

	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//div[@class='ButtonGroupGutters saveButtonRow']//button[@type='button']//span[@class='MuiButton-label']")
	WebElement goBackInvoiceLineButton;

	//	click on go back button 
	public void clickOnGoBackButton() {
		wait.until(ExpectedConditions.elementToBeClickable(goBackInvoiceLineButton));
		//goBackInvoiceLineButton.click();	
		//driver.navigate().back();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		//js.executeScript("history.go(-1);");
		//goBackInvoiceLineButton.sendKeys(Keys.ENTER);
		Actions act = new Actions(driver);
		act.moveToElement(goBackInvoiceLineButton).click(goBackInvoiceLineButton).build().perform();
	}
	
	//get appeared message in textBox after operation
	public String getTextMessage() {
		fWait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

}
