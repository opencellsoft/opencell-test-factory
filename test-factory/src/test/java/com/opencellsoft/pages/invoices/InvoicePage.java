package com.opencellsoft.pages.invoices;

import java.awt.AWTException;
import java.time.Duration;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class InvoicePage extends TestBase {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public InvoicePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(3)).ignoring(NoSuchElementException.class);
	}

	//	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-textSizeSmall MuiButton-sizeSmall']")
	//	@FindBy(how = How.CSS, using = "div[class='ButtonGroupGutters'] button[type='button']")
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Create')]")
	WebElement createInvoiceLineButton;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Validate')]")
	WebElement validateInvoiceButton;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'View pdf')]")
	WebElement viewPDFInvoiceButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Actions')]")
	WebElement actionsButton;
	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/main[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/fieldset[1]/div[1]/form[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/table[1]/tbody[1]/tr[1]/td[5]")
	WebElement discountAmountCell;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiGrid-root gridBlock fullWidthChild fullWidthChild MuiGrid-container MuiGrid-spacing-xs-2 MuiGrid-item MuiGrid-align-items-xs-center MuiGrid-grid-xs-11 MuiGrid-grid-sm-11']//div[1]//div[1]//div[1]")
	WebElement amountWithoutTax;
	@FindBy(how = How.XPATH, using = "//button[contains(@class ,'MuiButtonBase-root MuiButton-root MuiButton-text')][normalize-space() = 'Go Back']")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "//h2[contains(text(),'Validating an invoice')]")
	List<WebElement> ValidatingInvoicePopup;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Update and validate')]")
	WebElement updateAndValidateButton;
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Payment status')]/following-sibling::*//span[contains(text(),'PENDING') or contains(text(),'Pending')]")
	List<WebElement> paymentStatusPending;
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Payment status')]/following-sibling::*//span[contains(text(),'PART-PAID') or contains(text(),'Part-paid')]")
	List<WebElement> paymentStatusPartPaid;
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Payment status')]/following-sibling::*//span[contains(text(),'PAID') or contains(text(),'Paid')]")
	List<WebElement> paymentStatusPaid;
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Payment status')]/following-sibling::*//span[contains(text(),'REFUNDED') or contains(text(),'Refunded')]")
	List<WebElement> paymentStatusRefunded;
	@FindBy(how = How.XPATH, using = "//button//span[contains(text(),'Payment')]")
	WebElement paymentTab;
	@FindBy(how = How.XPATH, using = "//table[@id = 'simplePaymentHistory']//*//span[contains(text(),'MATCHED') or contains(text(),'Matched')]")
	List<WebElement> matchedStatus;
	@FindBy(how = How.XPATH, using = "//input[@id = 'invoiceDate']")
	WebElement invoiceDateInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'dueDate']")
	WebElement invoiceDueInput;
	@FindBy(how = How.XPATH, using = "(//button[@class = 'MuiButtonBase-root MuiIconButton-root MuiPickersCalendarHeader-iconButton' or contains(@aria-label , 'month')])[1]")
	WebElement montheBefore;
	@FindBy(how = How.XPATH, using = "(//button[@class = 'MuiButtonBase-root MuiIconButton-root MuiPickersCalendarHeader-iconButton' or contains(@aria-label , 'month')])[2]")
	WebElement montheAfter;
	@FindBy(how = How.XPATH, using = "(//div[@role = 'presentation']//*[text()='1'])[1]")
	WebElement monthFirst;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Save']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//div[@class= 'ButtonGroupGutters saveButtonRow']")
	WebElement saveButton1;
	@FindBy(how = How.XPATH, using = "(//label[contains(text(),'Status')]/following-sibling::div//div//span)[1]")
	WebElement invoiceStatus;
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Invoice Type') or contains(@id, 'type')]/following-sibling::div//span//span")
	WebElement invoiceType;
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Rejected by rule')]/following-sibling::div//span//span")
	WebElement rejectedByRule;

	public Boolean compareAmountWithoutTaxWithPriceVersionUnitPrice(String unitPrice) {
		return amountWithoutTax.getAttribute("value").equals(unitPrice);
	}

	public String getDiscountAmountValue() {
		return discountAmountCell.getText();
	}

	// click on create line button
	public void clickOnCreateInvoiceLine() throws AWTException {
		try {
			clickOn(createInvoiceLineButton);
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(createInvoiceLineButton));
			createInvoiceLineButton.click();
		}
	}

	// click on validate Invoice button
	public void validateInvoice() {
		try {
			clickOn(validateInvoiceButton);
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(validateInvoiceButton));
			clickOn(validateInvoiceButton);
		}
	}

	// get message from textBox
	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

	// click on view pdf invoice button
	public void viewPDFInvoice() {
		wait.until(ExpectedConditions.visibilityOf(viewPDFInvoiceButton));
		viewPDFInvoiceButton.click();
	}

	public void clickOnActionsButton() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(actionsButton));
			Actions actions = new Actions(driver);
			actions.moveToElement(actionsButton).click().perform();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(actionsButton));
			Actions actions = new Actions(driver);
			actions.moveToElement(actionsButton).click().perform();
		}	
	}

	// click on view pdf invoice button
	public void clickOnViewPDFButton() {
		wait.until(ExpectedConditions.elementToBeClickable(viewPDFInvoiceButton));
		viewPDFInvoiceButton.click();
	}

	// this method is to select customer
	public void clickOnCustomer(String customerCode) {
		driver.findElement(By.xpath("//a[normalize-space()='" + customerCode + "']")).click();
	}

	public void clickOnSaveButton() throws AWTException {
		clickOn(saveButton);
	}

	public void clickOnGoBackButton() {
		try {
			clickOn(goBackButton);
		}catch (Exception e) {
			wait.until(ExpectedConditions.elementToBeClickable(goBackButton));
			goBackButton.click();
		}
	}

	public boolean validatingInvoiceIsDisplayed() {
		if(ValidatingInvoicePopup.size()>0) { return true; } else { return false; }
	}

	public void clickOnUpdateAndValidateButton() {
		try {
			clickOn(updateAndValidateButton);
		}catch (Exception e) {
			wait.until(ExpectedConditions.elementToBeClickable(updateAndValidateButton));
			clickOn(updateAndValidateButton);
		}
	}

	public void clickOnPaymentTab() {
		this.paymentTab.click();
	}

	public String getInvoiceStatus() {
		return invoiceStatus.getText();
	}

	public String getInvoiceType() {
		String type;
		try {
			type = invoiceType.getText();
		}catch (Exception e) {
			WebElement elem = driver.findElement(By.xpath("//label[contains(text(),'Invoice Type') or contains(@id, 'type')]/following-sibling::div//div"));
			type = elem.getText();
		}
		return type;
	}

	public String getRejectedByRule() {
		return rejectedByRule.getText();
	}

	public boolean paymentStatusIsPending() {
		if (paymentStatusPending.size() == 0) {return false;}else {return true;}
	}

	public boolean paymentStatusIsPartPaid() {
		if (paymentStatusPartPaid.size() == 0) {return false;}else {return true;}
	}

	public boolean paymentStatusIsPaid() {
		if (paymentStatusPaid.size() == 0) {return false;}else {return true;}
	}

	public boolean paymentStatusIsRefunded() {
		if (paymentStatusRefunded.size() == 0) {return false;}else {return true;}
	}

	public boolean paymentStatusIsMatched() {
		if (matchedStatus.size() == 0) {return false;}else {return true;}
	}

	public String getInvoiceNumber() {
		try {
			return driver.findElement(By.xpath("//label[text()='Number' or text()='Invoice number']/following-sibling::div//span//span")).getText();
		}catch (Exception e) {
			return driver.findElement(By.xpath("(//form[@class = 'Invoice']//span//span)[1]")).getText();
		}
	}

	public void setInvoiceDateInPast(int m) {
		clickOn(invoiceDateInput);
		try {
			for (int i= 0; i< m; i++){clickOn(montheBefore);}
		}catch (Exception e) {
			clickOn(invoiceDateInput);
			for (int i= 0; i< m; i++){clickOn(montheBefore);}
		}
		
		clickOn(monthFirst);
	}

	public void setDueDateInPast(int m) {
		clickOn(invoiceDueInput);
		for (int i= 0; i< m; i++){
			clickOn(montheBefore);
		}
		clickOn(monthFirst);
	}

	public void setInvoiceDateInFutur(int m) {
		clickOn(invoiceDateInput);
		try {
			for (int i= 0; i< m; i++){
				clickOn(montheAfter);
			}
		}catch (Exception e) {
			clickOn(invoiceDateInput);
			for (int i= 0; i< m; i++){
				clickOn(montheAfter);
			}
		}
		
		clickOn(monthFirst);
	}

	public void setDueDateInFutur(int m) {
		clickOn(invoiceDueInput);
		for (int i= 0; i< m; i++){
			clickOn(montheAfter);
		}
		clickOn(monthFirst);
	}

}