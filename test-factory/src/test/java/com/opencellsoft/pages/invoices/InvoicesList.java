package com.opencellsoft.pages.invoices;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class InvoicesList {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public InvoicesList(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10)).pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
	}

	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-textSizeSmall MuiButton-sizeSmall']//span[@class='MuiButton-label']")
	WebElement addInvoiceButton;
	//@FindBy(how = How.XPATH, using = "//body[@style='overflow: hidden;']/div[@id='customized-menu']/div[@class='MuiPaper-root MuiMenu-paper MuiPopover-paper MuiPaper-elevation0 MuiPaper-rounded']/ul[@class='MuiList-root MuiMenu-list MuiList-padding']/div[1]")
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Create invoice')]")
	WebElement createInvoiceOption;
	@FindBy(how = How.XPATH, using = "//div[@class='list-page jss2504']//button[@aria-label='Add filter']")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addFilterButton;
	//@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Customer')]")
	@FindBy(how = How.XPATH, using = "//li[@data-key='billingAccount']")
	WebElement customerFilter;
	@FindBy(how = How.XPATH, using = "//input[@id='billingAccount']")
	WebElement customerInput;
	@FindBy(how = How.XPATH, using = "//tbody//tr[1]")
	WebElement selectCustomer;
	@FindBy(how = How.XPATH, using = "(//div[contains(text(),'Generate invoice')])[1]")
	WebElement generateInvoiceOption;
	@FindBy(how = How.XPATH, using = "//input[@id='billingAccount.code']")
	WebElement billingAccountInput;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Customer Code')]")
	WebElement customerCodeFilterOption;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement customerCodefilterInput;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Confirm']")
	WebElement confirmButton;
	@FindBy(how = How.XPATH, using = "//input[@id='searchBar']")
	WebElement searchBar;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Next')]")
	WebElement nextButton;	
	
	public void setCustomerCodeFilter(String customerCode) {
//		fWait.until(ExpectedConditions.visibilityOf(customerCodefilterInput));
		customerCodefilterInput.sendKeys(customerCode);
	}
	// click on create invoice button
	public void clickOnCreateInvoiceButton() {
		fWait.until(ExpectedConditions.elementToBeClickable(addInvoiceButton));
		addInvoiceButton.click();
	}
	
	// this method is to click customer filter
	public void clickOnCustomerFilter() {
		customerFilter.click();
	}
	
	public void selectCustomerCodeFilterOption() {
		wait.until(ExpectedConditions.elementToBeClickable(customerCodeFilterOption));
		customerCodeFilterOption.click();
	}
	
	// select create invoice  
	public void selectCreateInvoiceOption() {
//		wait.until(ExpectedConditions.elementToBeClickable(createInvoiceOption));
		wait.until(ExpectedConditions.visibilityOf(createInvoiceOption));
		createInvoiceOption.click();
	}
	
	//this method is to add filter button
	public void clickOnAddFilterButton() {
		wait.until(ExpectedConditions.elementToBeClickable(addFilterButton));		
		addFilterButton.click();
	}
	
	//this method is set customer code 
	public void searchForCustomer(String customerCode) {
		wait.until(ExpectedConditions.visibilityOf(customerInput));		
		customerInput.sendKeys(customerCode);
	}
	
	//this method is to select customer
	public void selectCustomer() {
		wait.until(ExpectedConditions.visibilityOf(selectCustomer));		
		selectCustomer.click();
	}
	
	public void selectCustomerByVisibleCode(String customerCode) {
		// TODO Auto-generated method stub
//		wait.until(ExpectedConditions.visibilityOf(customerInput));
		driver.findElement(By.xpath("//span[contains(text(),'"+customerCode+"')]")).click();		
	}
	public void clickOnCustomer(String strCodeCustomerFound) {
		// TODO Auto-generated method stub
		wait.until(ExpectedConditions.visibilityOf(customerInput));
		customerInput.findElement(By.xpath("//a[contains(text(),'"+ strCodeCustomerFound +"')]")).click();		
	}
//this method is to select generate invoice option in invoices list
	public void selectGenerateInvoiceOption() {
		// TODO Auto-generated method stub
//		wait.until(ExpectedConditions.visibilityOf(generateInvoiceOption));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", generateInvoiceOption);
//		generateInvoiceOption.click();
	}

	public void clickOnTargetCustomersInput() {
		// TODO Auto-generated method stub
		wait.until(ExpectedConditions.visibilityOf(billingAccountInput));
		billingAccountInput.click();
	}
	//this method is to confirm generation of invoice for a selected customer
	public void clickOnConfirmButton() {
		// TODO Auto-generated method stub
		confirmButton.click();
	}

	public void checkPagination () {
		wait.until(ExpectedConditions.elementToBeClickable(nextButton));
		this.nextButton.click();		
	}

	public void checkBreadCrumb (String text) {
		List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + text + "')]"));
		Assert.assertTrue(list.size() > 0);

		//Assert.assertTrue("Text not found!", list.size() > 0);		
	}
}