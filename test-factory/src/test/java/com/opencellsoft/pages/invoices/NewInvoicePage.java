package com.opencellsoft.pages.invoices;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class NewInvoicePage extends TestBase {

	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public NewInvoicePage(WebDriver driver) {
		this.driver = driver;
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
	}

	@FindBy(how = How.XPATH, using = "//input[@name='billingAccount.code']")
	WebElement customer;
	@FindBy(how = How.XPATH, using = "//div[@id='type.code']")
	WebElement invoiceType;
	@FindBy(how = How.XPATH, using = "//div[@id='seller']")
	WebElement seller;
	@FindBy(how = How.XPATH, using = "//ul[@aria-labelledby = 'seller-label']//li[1]")
	WebElement firstSeller;
	@FindBy(how = How.XPATH, using = "//li[contains(text(),'MAIN_SELLER') or @data-value = 'MAIN_SELLER']")
	WebElement mainSeller;
	@FindBy(how = How.XPATH, using = "//*[text()='Commercial invoice' or text()='Invoice']")
	WebElement commercialInvoice;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addfilter;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Customer Code')]")
	WebElement customerCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement codeInput;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;

	// get message from textBox
	public String getTextMessage() {
		WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

	//search for costumer method
	public void searchForCustomer(String customerCode) {
		codeInput.sendKeys(customerCode);
	}

	// add a filter
	public void clickOnCustomerDropdown() {
		Wait<WebDriver> fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(20))
				.pollingEvery(Duration.ofMillis(10)).ignoring(NoSuchElementException.class);
		fWait.until(ExpectedConditions.visibilityOf(customer));
		customer.click();
	}

	public void clickAddFilterBtn() {
		wait.until(ExpectedConditions.elementToBeClickable(addfilter));
		//addfilter.click();
		Actions act = new Actions(driver);
		act.moveToElement(addfilter).click(addfilter);
		act.perform();
	}

	// select code for filter
	public void selectCodeFilter() {
		wait.until(ExpectedConditions.elementToBeClickable(customerCode));
		customerCode.click();
	}

	// click on customer
	public void clickOnCustomer(String customerCode) {
		WebElement elem = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//mark[contains(text(),'" + customerCode + "')]")));
		elem.click();
	}

	// select invoice type commercial
	public void clickOnInvoiceTypeDropdown() {
		try {
			invoiceType.click();
		}
		catch (Exception e) {
			fWait.until(ExpectedConditions.visibilityOf(invoiceType));
			invoiceType.click();
		}
		
	}

	public void selectCommercialInvoiceType() {
		try {
			commercialInvoice.click();
		}
		catch (Exception e) {
			wait.until(ExpectedConditions.elementToBeClickable(commercialInvoice));
			commercialInvoice.click();
		}
		
	}
	
	public void setCommercialInvoiceType() {
		clickOn(invoiceType);
		clickOn(commercialInvoice);
	}

	public void selectInvoiceType(String invoiceType) {
		this.invoiceType.findElement(By.xpath("//li[contains(.,'" + invoiceType + "')]")).click();
	}

	public void selectMainSeller() {
		this.seller.click();
		waitPageLoaded();
		this.mainSeller.click();
	}
	public void setSeller() {
		clickOn(seller);
		clickOn(firstSeller);
	}
	
	// click on save button
	public void clickOnSaveCommercialInvoiceButton() {
		fWait.until(ExpectedConditions.visibilityOf(saveButton));
		saveButton.submit();
		waitPageLoaded();
	}
	
	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
	
}
