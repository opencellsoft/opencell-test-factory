package com.opencellsoft.pages.subscriptions;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.opencellsoft.base.TestBase;

public class AccessPointsPage extends TestBase{

	public AccessPointsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "code")
	WebElement accessPointCode;
	@FindBy(how = How.XPATH, using = "//input[@id='startDate']")
	WebElement startDateInput;
	@FindBy(how = How.XPATH, using = "(//P[@class='MuiTypography-root MuiTypography-body2 MuiTypography-colorInherit'][text()='1'])[1]")
	WebElement startDate;
	@FindBy(how = How.XPATH, using = "//button//span[text()='Save']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//*[contains(text() , 'Go Back')]")
	WebElement goBackLink;
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Go Back')]")
	WebElement goBackButton;
	
	public void setAccessPointCode(String APCode) {
		accessPointCode.sendKeys(APCode);
		waitPageLoaded();
	}
	
	public void setAccessStartDate() {
		startDateInput.click();
		waitPageLoaded();
		startDate.click();
	}

	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
	
	public void clickOnGoBackLink() {
		clickOn(goBackLink);
	}
	public void clickOnGoBackButton() {
		clickOn(goBackButton);
	}
	

}
