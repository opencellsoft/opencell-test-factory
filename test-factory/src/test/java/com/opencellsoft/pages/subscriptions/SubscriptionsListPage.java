package com.opencellsoft.pages.subscriptions;

import java.time.Duration;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.opencellsoft.base.TestBase;

public class SubscriptionsListPage extends TestBase {

	public SubscriptionsListPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addfilterBtn;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Code')]")
	WebElement code;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement codeInput;
	@FindBy (how= How.CSS, using =".MuiTableCell-root.MuiTableCell-body.column-description.jss1975.jss1968")
	WebElement foundSubscription;

	@FindBy(how = How.XPATH, using = "//*[@id='CREATE' or @title = 'Create']")
	WebElement createNewSubscriptionButton;

	//this method is to click on the filter button
	public void clickFilterBtn() {
		addfilterBtn.click();
	}
	// this method is to select  the filter by code
	public void clickCodeFilter() {
		code.click();
	}
	//this method is to set subscription code in the input of filter code
	public void enterSubscriptionCode(String code) {
		codeInput.sendKeys(code);
	}
	//this method is to search by code for a subscription in the subscriptions list
	public void searchForSubscription(String code) {
		this.clickFilterBtn();
		this.clickCodeFilter();
		this.enterSubscriptionCode(code);
	}
	//this method is to select a subscription we  search for is by code
	public void clicOnSubscription() {
		Wait<WebDriver> fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(100)).pollingEvery(Duration.ofSeconds(5)).ignoring(NoSuchElementException.class);
		fWait.until(ExpectedConditions.visibilityOf(foundSubscription));
		// click on the compose button as soon as the button is visible				
		foundSubscription.click();
	}

	// this method is to click on create button
	public void clickAddNewSubscription() {
		clickOn(createNewSubscriptionButton);
	}
	
}
