package com.opencellsoft.pages.charges;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.opencellsoft.base.TestBase;

public class ListOfChargesPage extends TestBase {

	WebDriver driver;
	Wait<WebDriver> fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(5)).pollingEvery(Duration.ofSeconds(1)).ignoring(NoSuchElementException.class);


	public ListOfChargesPage(WebDriver driver) {
		this.driver = driver;
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-textSizeSmall MuiButton-sizeSmall']")
	WebElement createChargeButton;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase name']")
	WebElement SearchBarInput;
	
	// This method is to click on create charge button	
	public void clickOnCreateChargeButton() {
		fWait.until(ExpectedConditions.elementToBeClickable(createChargeButton));
		createChargeButton.click();
	}
	
	public void setSearchBarText(String chargeDescription) {
		SearchBarInput.sendKeys(chargeDescription);
	}
	
	public void clickOnChargeFound(String chargeDescription) {
		WebElement result = driver.findElement(By.xpath("//input/*[contains(text(), '" + chargeDescription + "')]"));
		result.click();
	}
}
