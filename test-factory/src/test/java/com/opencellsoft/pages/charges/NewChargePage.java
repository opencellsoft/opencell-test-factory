package com.opencellsoft.pages.charges;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class NewChargePage extends TestBase {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public NewChargePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
	}

	// Using FindBy for locating elements
//	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	@FindBy(how = How.XPATH, using = "//INPUT[@id='code']")
	WebElement chargeCode;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement chargeDescription;
	@FindBy(how = How.XPATH, using = "//div[@id='chargeType']")
	WebElement chargeTypesList;
	@FindBy(how = How.CSS, using = "li:nth-child(1)")
	WebElement selectedchargeType;
	@FindBy(how = How.ID, using = "chargeTypeOneShot")
	WebElement oneShotChargeTarifTypeList;
	@FindBy(how = How.XPATH, using = "//div[@id='chargeType']")
	WebElement recurringchargeTypeList;
	@FindBy(how = How.CSS, using = "li:nth-child(2)")
	WebElement selectedRecurringChargeType;
	@FindBy(how = How.XPATH, using = "//li[@data-value='U']")
	WebElement usagechargeType;
	@FindBy(how = How.ID, using = "recurrenceCalendar")
	WebElement repetitionCalendarList;
	@FindBy(how = How.XPATH, using = "//*[text()='Activate']")
	WebElement activateChargeButton;
	@FindBy(how = How.XPATH, using = "//*[text()='Save']")
	WebElement submitChargeButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "//div[contains(@class , 'MuiAlert-message') or contains(@class , 'MuiSnackbarContent-message')]")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//input[@id='subscriptionProrata']")
	WebElement subscriptionProrataToggle;
	@FindBy(how = How.XPATH, using = "//input[@id='terminationProrata']")
	WebElement terminationProrataToggle;
	@FindBy(how = How.XPATH, using = "//input[@id='applyInAdvance']")
	WebElement applyInAdvanceToggle;
	@FindBy(how = How.XPATH, using = "//input[@id='usageParam1']")
	WebElement param1;
	@FindBy(how = How.XPATH, using = "//input[@id='usageParam2']")
	WebElement param2;
	

	// Defining all the user actions (Methods) that can be performed in the
	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

	// This method is to set Charge code
	public void setChargeCode(String strChargeCode) {
		try {
			wait.until(ExpectedConditions.visibilityOf(chargeCode));
			chargeCode.sendKeys(strChargeCode);
		}catch (Exception e) {
			chargeCode.sendKeys(strChargeCode);
		}
	}
	
	// This method is to set Charge code
	public void clearAndSetChargeCode(String strChargeCode) {
		try {
			chargeCode.sendKeys(Keys.CONTROL + "a");
			chargeCode.sendKeys(strChargeCode);
		}catch (Exception e) {
			driver.navigate().refresh();
			waitPageLoaded();
			chargeCode.sendKeys(Keys.CONTROL + "a");
			chargeCode.sendKeys(strChargeCode);
		}
	}
	// This method is to set offer Description
	public void setChargeDescription(String strChargeDescription) {
		chargeDescription.sendKeys(strChargeDescription);
	}

	// This method is to select one charge type
	public void selectOneShotChargeType() {
		wait.until(ExpectedConditions.visibilityOf(chargeTypesList));
		chargeTypesList.click();
		selectedchargeType.click();		
	}

	// This method is to select charge type
	public void selectChargeType(String chargeType) {
		wait.until(ExpectedConditions.visibilityOf(chargeTypesList));
		chargeTypesList.click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ul//*[contains(text(),'" + chargeType + "')]")));
		chargeTypesList.findElement(By.xpath("//ul//*[contains(text(),'" + chargeType + "')]")).click();
	}
	
	// This method is to set param1
	public void setParam1(String param) {
		param1.sendKeys(Keys.CONTROL + "a");
		param1.sendKeys(param);
		waitPageLoaded();
	}	
	
	// This method is to set param1
	public void setParam2(String param) {
		param2.sendKeys(Keys.CONTROL + "a");
		param2.sendKeys(param);
		waitPageLoaded();
	}	
	// This method is to select tariff type
	public void selectOneShotTarifType(String oneShotChargeTarifType) {
		wait.until(ExpectedConditions.visibilityOf(oneShotChargeTarifTypeList));
		oneShotChargeTarifTypeList.click();// assuming you have to click the "dropdown" to open it
		oneShotChargeTarifTypeList.findElement(By.xpath("//li[contains(.,'" + oneShotChargeTarifType + "')]")).click();
		waitPageLoaded();
	}

	// This method is to select recurring charge type
	public void selectRecurringChargeType() {
		wait.until(ExpectedConditions.visibilityOf(recurringchargeTypeList));
		recurringchargeTypeList.click();
		selectedRecurringChargeType.click();
	}

	// This method is to select charge repetition calendar

	public void selectRepetitionCalendar(String repetitionCalendar) {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(repetitionCalendarList));
			clickOn(repetitionCalendarList);
			WebElement elem = 
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[normalize-space()='" + repetitionCalendar + "']")));
			clickOn(elem);
		}catch (Exception e) {
			try {
				clickOn(driver.findElement(By.xpath("//ul//*[normalize-space()='CAL_PERIOD_MONTHLY']")));
			}catch (Exception ex) {
				clickOn(driver.findElement(By.xpath("//ul//li[1]")));
			}
		}
	
	}

	// This method is to click on Submit Button
	public void clickOnSaveChargeButton() {
			Actions act = new Actions(this.driver);
			act.sendKeys(Keys.chord(Keys.PAGE_UP)).perform();
			wait.until(ExpectedConditions.elementToBeClickable(submitChargeButton));
			clickOn(submitChargeButton);
	}

	// This method is to click on go back button
	public void clickOnGoBackButton() {
		wait.until(ExpectedConditions.visibilityOf(goBackButton));
		goBackButton.click();
	}

	// This method is to click on activate button
	public void clickOnActivateButton() {
		wait.until(ExpectedConditions.elementToBeClickable(activateChargeButton));
		activateChargeButton.click();
	}

	public void activateSubscriptionProrata() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", subscriptionProrataToggle);
	}

	public void activateTerminationProrata() {
		terminationProrataToggle.click();
	}

	public void activateApplyInAdvance() {
		clickOn(applyInAdvanceToggle);
	}
	
	// This method is to select Usage charge type
	public void selectUsageChargeType() {
		wait.until(ExpectedConditions.visibilityOf(chargeTypesList));
		chargeTypesList.click();
		wait.until(ExpectedConditions.elementToBeClickable(usagechargeType));
		usagechargeType.click();
	}
}
