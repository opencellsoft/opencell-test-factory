package com.opencellsoft.pages.charges;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class NewPriceVersionpage extends TestBase {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public NewPriceVersionpage(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		this.fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10)).pollingEvery(Duration.ofSeconds(2)).ignoring(NoSuchElementException.class);
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//input[@id='label']")
	WebElement priceVersionLabel;
	@FindBy(how = How.XPATH, using = "//input[@id='startDate']")
	WebElement startDateInput;
	@FindBy(how = How.XPATH, using = "//input[@id='endDate']")
	WebElement endDateInput;
	//@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day MuiPickersDay-current MuiPickersDay-daySelected']")
	@FindBy(how = How.XPATH, using = "(//*[text()='1'])[1]")
	WebElement startDate;
	@FindBy(how = How.XPATH, using = "(//*[text()='14'])[1]")
	WebElement endDate;
	@FindBy(how = How.XPATH, using = "//input[@id='priceMatrix']")
	WebElement priceGridButton;
	@FindBy(how = How.XPATH, using = "//input[@id='price']")
	WebElement unitPrice;
	@FindBy(how = How.XPATH, using = "//*[text()='Save']")
	WebElement savePriceVersionButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;	
	@FindBy(how = How.XPATH, using = "(//button[@class = 'MuiButtonBase-root MuiIconButton-root MuiPickersCalendarHeader-iconButton' or contains(@aria-label , 'month')])[1]")
	WebElement previousMonth;
	@FindBy(how = How.XPATH, using = "(//button[@class = 'MuiButtonBase-root MuiIconButton-root MuiPickersCalendarHeader-iconButton' or contains(@aria-label , 'month')])[2]")
	WebElement nexMonth;
	// This method is to set price version code in the code text box
	public void setPriceVersionCode(String priceVersionLabel) {
		this.priceVersionLabel.sendKeys(priceVersionLabel);
	}

	// This method is to select calendar start date
	public void setPriceVersionStartDate() {
		wait.until(ExpectedConditions.elementToBeClickable(startDateInput));
		startDateInput.click();
		wait.until(ExpectedConditions.elementToBeClickable(startDate));
		startDate.click();
	}
	
	// This method is to select calendar end date
	public void setEndDate() {
		wait.until(ExpectedConditions.visibilityOf(endDateInput));
		endDateInput.click();
		wait.until(ExpectedConditions.visibilityOf(endDate));
		endDate.click();		
	}
	
	// This method is to click price grid button
	public void clickOnPriceGridButton() {
		//wait.until(ExpectedConditions.visibilityOf(priceGridButton));
		clickOn(priceGridButton);
	}
	
	// This method is to set price in the price text box
	public void setPriceVersionUnitPrice(String unitPrice) {
		wait.until(ExpectedConditions.visibilityOf(this.unitPrice));
		this.unitPrice.sendKeys(unitPrice);
	}
	
	public void clickOnSavePriceVersionButton() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(savePriceVersionButton));
			clickOn(savePriceVersionButton);
			//savePriceVersionButton.click();	
		}
		catch (Exception e) {
			clickOn(savePriceVersionButton);
		}
	}
	
	public void clickOnGoBackButton() {
		try {
			wait.until(ExpectedConditions.visibilityOf(goBackButton));
			goBackButton.click();
		}
		catch (Exception e) {
			goBackButton.click();
		}
				
	}
	
	// This method is to select calendar start date & End date in the present
	public void setPriceVersionDateInPresent() {
		startDateInput.click();
		waitPageLoaded();
		startDate.click();
		waitPageLoaded();
		endDateInput.click();
		waitPageLoaded();
		nexMonth.click();
		waitPageLoaded();
		startDate.click();
		waitPageLoaded();
	}
	
	// This method is to select calendar start date & End date in the present
	public void setPriceVersionDateInFutur() {
		startDateInput.click();
		waitPageLoaded();
		nexMonth.click();
		waitPageLoaded();
		startDate.click();
		waitPageLoaded();
		endDateInput.click();
		waitPageLoaded();
		nexMonth.click();
		waitPageLoaded();
		nexMonth.click();
		waitPageLoaded();
		startDate.click();
		waitPageLoaded();
	}
	
	public boolean errorMessageIsDsplayed() {
		List<WebElement> errorMessage = driver.findElements(By.xpath("//div[contains(text(), 'The following parameters are required or contain invalid values: pricePlanMatrixCode.')]"));
		return !errorMessage.isEmpty();
	}
}