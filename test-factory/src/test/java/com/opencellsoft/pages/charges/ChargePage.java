package com.opencellsoft.pages.charges;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class ChargePage extends TestBase {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;
	Actions actions;
	public ChargePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(3)).ignoring(NoSuchElementException.class);
		actions = new Actions(driver);
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Activate')]")
	WebElement activateChargeButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Create')]")
	WebElement createPriceVersionButton;
	@FindBy(how = How.XPATH, using = "//div[contains(@class , 'MuiAlert-message') or contains(@class , 'MuiSnackbarContent-message')]")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Save']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement chargeCode;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement chargeDescription;
	@FindBy(how = How.XPATH, using = "//div[@id='chargeType']")
	WebElement chargeTypeList;

	public String getChargeCodeInputValue() {
		return chargeCode.getAttribute("value");
	}

	public String getChargeDescriptionInputValue() {
		return chargeDescription.getAttribute("value");
	}

	public String getChargeTypeInputValue() {
		return chargeTypeList.getAttribute("value");
	}

	// This method is to click on create price version
	public void clickOnCreatePriceVersionButton() {
		try {
			createPriceVersionButton.click();
		}
		catch (Exception e) {
//			driver.navigate().refresh();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", createPriceVersionButton);
			clickOn(createPriceVersionButton);
		}
	}

	public void clickOnActivateChargeButton() {
		try {
		wait.until(ExpectedConditions.elementToBeClickable(activateChargeButton));
		activateChargeButton.click();
		}
		catch (Exception e) {
			clickOn(activateChargeButton);
		}
	}

	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

	// this method is to click on go back button
	public void clickOnGoBackButton() {
		wait.until(ExpectedConditions.visibilityOf(goBackButton));
		goBackButton.click();
	}
	
	// this method is to click on go back button
	public void clickOnGoBacklink() {
		try {
		goBackButton.click();
		}
		catch (Exception e) {
			clickOn(driver.findElement(By.xpath("(//div[contains(@class , 'PageTitle ')]//a)[2]")));
		}
	}
	public void clickOnSaveButton() {
		wait.until(ExpectedConditions.visibilityOf(saveButton));
		saveButton.click();
	}
}
