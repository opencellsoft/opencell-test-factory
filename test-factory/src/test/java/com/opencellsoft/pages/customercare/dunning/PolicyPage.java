package com.opencellsoft.pages.customercare.dunning;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class PolicyPage extends TestBase {


	public PolicyPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//input[@id = 'name']")
	WebElement nameInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'description']")
	WebElement descriptionInput;
	@FindBy(how = How.XPATH, using = "(//form[@class = 'B2B_Dunning_policy']//span[contains(@class, 'MuiSwitch-colorPrimary')])[4]")
	WebElement activeInput1;
	@FindBy(how = How.XPATH, using = "(//form[@class = 'B2B_Dunning_policy']//span[contains(@class, 'MuiSwitch-colorPrimary')])[3]")
	WebElement activeInput2;
	@FindBy(how = How.XPATH, using = "//button[@type = 'submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//span[text() = 'Go Back']")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "//*[contains(text() , 'Pick')]")
	WebElement pickButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Edit']")
	WebElement updateButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Add line']")
	WebElement addLineButton;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//input[contains(@id , 'code')]")
	WebElement searchByLevelNameInput;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//table//tbody//tr[1]//td[2]")
	WebElement firstResult;
	@FindBy(how = How.XPATH, using = "//div[contains(@id, 'inEditRules')]")
	WebElement inEditRuleInput;
	@FindBy(how = How.XPATH, using = "//div[contains(@id, 'operators')]")
	WebElement operatorInput;
	@FindBy(how = How.XPATH, using = "//div[contains(@id, 'editValues')]")
	WebElement editValueInput;
	
	public void setName(String name) {
		nameInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		nameInput.sendKeys(name);
		waitPageLoaded();
	}
	
	public void setDescription(String description) {
		descriptionInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		descriptionInput.sendKeys(description);
		waitPageLoaded();
	}
	
	public void setActive(Boolean active) {
		String class_name;
		try {
			class_name = activeInput1.getAttribute("class");
		}
		catch (Exception e) {
			class_name = activeInput2.getAttribute("class");
		}
		if((active.equals(true) && !class_name.contains("Mui-checked")) || (active.equals(false) && class_name.contains("Mui-checked")) ) {
			try {
				clickOn(activeInput1);
			}catch (Exception e) {
				clickOn(activeInput2);
			}
			
		}
	}
	
	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
	
	public void clickOnGoBackButton() {
		clickOn(goBackButton);
	}
	
	public void clickOnPickButton() {
		clickOn(pickButton);
	}
	
	public void clickOnUpdateButton() {
		clickOn(updateButton);
	}
	
	public void clickOnAddLineButton() {
		clickOn(addLineButton);
	}
	
	public void setInEditRule(String inEditRule) {
		clickOn(inEditRuleInput);
		clickOn(driver.findElement(By.xpath("//*[text() = '" + inEditRule + "']")));
	}
	
	public void setOperator(String operator) {
		clickOn(operatorInput);
		clickOn(driver.findElement(By.xpath("//*[text() = '" + operator + "']")));
	}
	
	public void setEditValue(String editValue) {
		clickOn(editValueInput);
		clickOn(driver.findElement(By.xpath("//*[text() = '" + editValue + "']")));
	}
	
	public void setSearchByLevelName(String name) {
		searchByLevelNameInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		searchByLevelNameInput.sendKeys(name);
		waitPageLoaded();
	}
	
	public void clickOnFirstResult() {
		clickOn(firstResult);
	}
	
	public void pickLevelToPolicy(String level_name) {
		clickOnPickButton();
		setSearchByLevelName(level_name);
		clickOnFirstResult();
	}
}
