package com.opencellsoft.pages.customercare.dunning;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class LevelPage extends TestBase {

	public LevelPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//input[@id = 'code']")
	WebElement nameInput;
	@FindBy(how = How.XPATH, using = "//div[contains(@class , 'ql-editor')]")
	WebElement descriptionInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'daysOverdue']")
	WebElement daysOverdueInput;
	@FindBy(how = How.XPATH, using = "(//form[@class='DunningLevels']//span[contains(@class, 'MuiIconButton-root') or contains(@class,'MuiSwitch-switchBase')])[2]")
	WebElement endOfDunningInput;
	@FindBy(how = How.XPATH, using = "(//form[@class='DunningLevels']//span[contains(@class, 'MuiIconButton-root') or contains(@class,'MuiSwitch-switchBase')])[3]")
	WebElement activeInput;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Edit']")
	WebElement updateButton;
	@FindBy(how = How.XPATH, using = "//button[@type = 'submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//span[text() = 'Go Back']")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Pick']")
	WebElement pickButton;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//input[contains(@id , 'code')]")
	WebElement searchByNameInput;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//table//tbody//tr[1]//td[2]")
	WebElement firstResult;
	
	public void setName(String name) {
		nameInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		nameInput.sendKeys(name);
		waitPageLoaded();
	}
	
	public void setDescription(String description) {
		descriptionInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		descriptionInput.sendKeys(description);
		waitPageLoaded();
	}
	
	public void setDaysOverdue(String daysOverdue) {
		daysOverdueInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		daysOverdueInput.sendKeys(daysOverdue);
		waitPageLoaded();
	}
	
	
	public void setEndOfDunning(Boolean level_endOfDunning) {
		String class_name = endOfDunningInput.getAttribute("class");
		if((level_endOfDunning.equals(true) && !class_name.contains("Mui-checked")) || (level_endOfDunning.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(endOfDunningInput);
		}
	}
	public void setActive(Boolean active) {
		String class_name = activeInput.getAttribute("class");
		if((active.equals(true) && !class_name.contains("Mui-checked")) || (active.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(activeInput);
		}
	}
	
	public void clickOnUpdateButton() {
		clickOn(updateButton);
	}
	
	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
	
	public void clickOnGoBackButton() {
		clickOn(goBackButton);
	}
	
	public void clickOnPickButton() {
		clickOn(pickButton);
	}
	
	public void setSearchByActionName(String name) {
		searchByNameInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		searchByNameInput.sendKeys(name);
		waitPageLoaded();
	}
	
	public void clickOnFirstResult() {
		clickOn(firstResult);
	}
	
}
