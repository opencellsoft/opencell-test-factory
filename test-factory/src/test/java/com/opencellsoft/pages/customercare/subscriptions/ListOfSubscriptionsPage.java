package com.opencellsoft.pages.customercare.subscriptions;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class ListOfSubscriptionsPage {
	WebDriver driver;
	Wait<WebDriver> fWait;
	Wait<WebDriver> wait;
	
	public ListOfSubscriptionsPage(WebDriver driver) {
		this.driver = driver;
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(30))
				.pollingEvery(Duration.ofSeconds(2)).ignoring(NoSuchElementException.class);
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
	}

	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addfilterBtn;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Code')]")
	WebElement code;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement codeInput;
	//@FindBy (how= How.CSS, using =".MuiTableCell-root.MuiTableCell-body.column-description.jss1975.jss1968")
	@FindBy (how= How.XPATH, using ="//table/tbody/tr[1]")
	WebElement foundSubscription;
	@FindBy(how = How.XPATH, using = "//input[@id='searchBar']")
	WebElement searchBar;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Next')]")
	WebElement nextButton;	
	
	//this method is to click on the filter button
	public void clickFilterBtn() {
		addfilterBtn.click();
	}
	// this method is to select  the filter by code
	public void clickCodeFilter() {
		code.click();
	}
	//this method is to set subscription code in the input of filter code
	public void enterSubscriptionCode(String code) {
		codeInput.sendKeys(code);
	}
	//this method is to search by code for a subscription in the subscriptions list
	public void searchForSubscription(String code) {
		this.clickFilterBtn();
		this.clickCodeFilter();
		this.enterSubscriptionCode(code);
	}
	//this method is to select a subscription we  search for is by code
	public void clicOnSubscription() {
		wait.until(ExpectedConditions.visibilityOf(foundSubscription));
		// click on the element as soon as the button is visible				
		//foundSubscription.click();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", foundSubscription);
	}

	public void checkPagination () {
		wait.until(ExpectedConditions.elementToBeClickable(nextButton));
		this.nextButton.click();		
	}

	public void checkBreadCrumb (String text) {
		List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + text + "')]"));
		Assert.assertTrue(list.size() > 0);
	}	

	// write the customer code in the search bar filter
	public void setSearchBarText(String text) {
		wait.until(ExpectedConditions.visibilityOf(searchBar));
		searchBar.sendKeys(text);
	}

}
