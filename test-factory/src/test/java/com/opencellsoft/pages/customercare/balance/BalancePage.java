package com.opencellsoft.pages.customercare.balance;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import com.opencellsoft.base.TestBase;

public class BalancePage extends TestBase {

	public BalancePage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Pay with security deposit')]")
	WebElement payWithSecurityDepositButton;
	@FindBy(how = How.ID, using = "securityDeposits")
	WebElement securityDepositsList;
	@FindBy(how = How.XPATH, using = "//button//span[contains(text(),'Confirm')]")
	WebElement confirmButton;
	
	public void selectLastInvoice() {
		List<WebElement> result = driver.findElements(By.xpath("//table[@class = 'MuiTable-root']//tbody[@class = 'MuiTableBody-root']//input[@type = 'checkbox']"));
		WebElement lastrow = result.get(result.size()-1);
		lastrow.click();
	}
	
	public void clickOnPayWithSecurityDepositButton() {
		payWithSecurityDepositButton.click();
	}
	
	public void clickOnSecurityDepositsList() {
		securityDepositsList.click();
	}
	
	public void selectSecurityDeposit(String sd) {
		driver.findElement(By.xpath("//table[@filters = '[object Object]']//td//span[ text() = '" + sd + "']")).click();
	}
	public void clickOnConfirmButton() {
		confirmButton.click();
	}
}
