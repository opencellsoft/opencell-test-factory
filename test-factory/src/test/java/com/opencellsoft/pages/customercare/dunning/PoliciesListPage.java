package com.opencellsoft.pages.customercare.dunning;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class PoliciesListPage extends TestBase  {

	public PoliciesListPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//*[@title = 'Create' or @id = 'CREATE']")
	WebElement createButton;
	@FindBy(how = How.XPATH, using = "//input[contains(@title , 'Name') or @name = 'wildcardOrIgnoreCase policyName']")
	WebElement searchInput;
	@FindBy(how = How.XPATH, using = "//table[contains(@id, 'policies')]//tbody//*[@role = 'row' or contains(@class ,'MuiTableRow-root')]")
	List<WebElement> policiesList;
	@FindBy(how = How.XPATH, using = "//button[@title = 'Filter']")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//input[contains(@id , 'policyDescription')]")
	WebElement descriptionInput;
	@FindBy(how = How.XPATH, using = "//div[@data-source = 'isActivePolicy']//span[contains(@class , 'MuiIconButton-root')]")
	WebElement activeInput;
	
	public void clickOnCreateButton() {
		clickOn(createButton);
	}
	
	public void setSearchInput(String value) {
		searchInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		searchInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public List<WebElement> policiesList() {
		return policiesList;
	}
	
	public void goToPolicyDetails(String value) {
		clickOn(driver.findElement(By.xpath("//*[contains(text() , '" + value + "')]")));
	}
	
	public void clickOnFilterButton() {
		clickOn(filterButton);
	}
	
	public void activateFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//li//*[text()= '" + value + "']")));
	}
	
	public void setDescriptionInput(String value) {
		descriptionInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		descriptionInput.sendKeys(value);
		waitPageLoaded();
	}

	public void setActive(Boolean active) {
		String class_name = activeInput.getAttribute("class");
		if((active.equals(true) && !class_name.contains("Mui-checked")) || (active.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(activeInput);
		}
	}
	
	public void filterBy(String value) {
		clickOn(driver.findElement(By.xpath("//li[text()= '" + value + "']")));
	}
	
	public void clickOnRemoveFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//button[@data-key = '" + value + "']")));
	}
	
	public void sortBy(String value) {
		clickOn(driver.findElement(By.xpath("//span[contains(@class, 'MuiButtonBase-root') and contains(@data-field, '" + value + "')]")));
	}
	
}
