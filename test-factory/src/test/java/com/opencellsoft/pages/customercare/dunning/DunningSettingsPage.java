package com.opencellsoft.pages.customercare.dunning;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class DunningSettingsPage extends TestBase {
	
	public DunningSettingsPage() {
		PageFactory.initElements(driver, this);
	}
	
	Actions actions ;
	@FindBy(how = How.XPATH, using = "//input[@id = 'maxDunningLevels']")
	WebElement maxDunningLevelsInput;
	@FindBy(how = How.XPATH, using = "//input[@class = 'data-editor']")
	WebElement dataEditorInput;
	@FindBy(how = How.XPATH, using = "(//button[@title = 'Save' or @aria-label = 'Save'])[1]")
	WebElement save1Button;
	@FindBy(how = How.XPATH, using = "(//button[@title = 'Save' or @aria-label = 'Save'])[2]")
	WebElement save2Button;
	@FindBy(how = How.XPATH, using = "//button//span[contains(text(), 'Pause reasons')]")
	WebElement pauseReasonButton;
	@FindBy(how = How.XPATH, using = "//*[contains(text(), 'Stop reasons')]")
	WebElement stopReasonButton;
	
	public void setMaxDunningLevelsInput(String value) {
		maxDunningLevelsInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		maxDunningLevelsInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public String getSetMaxDunningLevelsInput(){
		return maxDunningLevelsInput.getAttribute("value");
	}
	
	public void clickOnSave1Button() {
		clickOn(save1Button);
	}
	
	public void clickOnPauseReasonButton() {
		clickOn(pauseReasonButton);
	}
	
	public void setPausReasonInput(String pausReason, String line) {
		actions = new Actions(driver);
		actions.doubleClick(driver.findElement(By.xpath("(//table)[1]//tbody//tr[" + line + "]//td[1]"))).perform();
		waitPageLoaded();
		dataEditorInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		dataEditorInput.sendKeys(pausReason);
		waitPageLoaded();
		dataEditorInput.sendKeys(Keys.RETURN);
	}
	
	public void setPauseDescriptionInput(String pauseDescription, String line) {
		actions = new Actions(driver);
		actions.doubleClick(driver.findElement(By.xpath("(//table)[1]//tbody//tr[" + line + "]//td[2]"))).perform();
		waitPageLoaded();
		dataEditorInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		dataEditorInput.sendKeys(pauseDescription);
		waitPageLoaded();
		dataEditorInput.sendKeys(Keys.RETURN);
	}
	
	public void clickOnStopReasonButton() {
		clickOn(stopReasonButton);
	}
	
	public void setStopReasonInput(String pausReason, String line) {
		actions = new Actions(driver);
		actions.doubleClick(driver.findElement(By.xpath("(//table)[2]//tbody//tr[" + line + "]//td[1]"))).perform();
		waitPageLoaded();
		dataEditorInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		dataEditorInput.sendKeys(pausReason);
		waitPageLoaded();
		dataEditorInput.sendKeys(Keys.RETURN);
	}
	
	public void setStopDescriptionInput(String pauseDescription, String line) {
		actions = new Actions(driver);
		actions.doubleClick(driver.findElement(By.xpath("(//table)[2]//tbody//tr[" + line + "]//td[2]"))).perform();
		waitPageLoaded();
		dataEditorInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		dataEditorInput.sendKeys(pauseDescription);
		waitPageLoaded();
		dataEditorInput.sendKeys(Keys.RETURN);
	}
	
	public void clickOnSave2Button() {
		clickOn(save2Button);
	}
	
	public Boolean searchInPauseReasonTable(String pauseReason) {
		WebElement table = driver.findElement(By.xpath("(//table)[1]"));
		return table.getText().contains(pauseReason);
	}
	
	public Boolean searchInStopReasonTable(String stopReason) {
		WebElement table = driver.findElement(By.xpath("(//table)[2]"));
		return table.getText().contains(stopReason);
	}
}
