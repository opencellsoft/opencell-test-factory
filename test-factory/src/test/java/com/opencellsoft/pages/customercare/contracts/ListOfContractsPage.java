package com.opencellsoft.pages.customercare.contracts;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;


public class ListOfContractsPage extends TestBase {
	
WebDriver driver;
WebDriverWait wait;

	public ListOfContractsPage(WebDriver driver) {

		this.driver= driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));	
	}
		
	
		// Using FindBy for locating elements
		@FindBy(how= How.XPATH, using= "//span[normalize-space()='Contracts']")
		WebElement contractsButton;
		
		@FindBy(how= How.XPATH, using= "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-textSizeSmall MuiButton-sizeSmall']//span[@class='MuiButton-label']//*[name()='svg']")
		WebElement createContractButton;
		
		@FindBy(how= How.XPATH, using= "//*[name()='path' and contains(@d,'M10 18h4v-')]")
		WebElement filterButton;
		
		@FindBy(how= How.XPATH, using= "//li[@role='menuitem']//span[contains(text(),'Code')]")
		WebElement codeButton;
		
		@FindBy(how= How.XPATH, using= "//input[@id='wildcardOrIgnoreCase code' or @id='searchBar']")
		WebElement codeLabel;

		@FindBy(how= How.XPATH, using= "//table//tbody")
		WebElement result;
		
		@FindBy(how= How.XPATH, using= "//tbody/tr[3]/td[1]")
		WebElement contractLine;
			
		//This method is to click on Contracts in menu
		public void clickOnContractsButton() {
			clickOn(contractsButton);
		}
		
		//This method is to click on CreatecustomerCare to create new Contract
		public void clickOnCreateNewContractButton() {
			clickOn(createContractButton);
		}
		
		//This method is to click on Filter button
		public void clickOnFilterButton() {
			clickOn(filterButton);
		}
		
		//This method is to click on code button 
		public void clickOnFilterByCodeButton(){
			clickOn(codeButton);
		}
		
		//This method is to set code in input box
		public void setCode(String code){
			codeLabel.sendKeys(code);
		}
		
		//This method is to click on line
		public void clickOnCodeLine(){
			wait.until(ExpectedConditions.elementToBeClickable(contractLine));
			clickOn(contractLine);
		}
		
		public void goToContractDetails() {
			clickOn(result);
		}
}
