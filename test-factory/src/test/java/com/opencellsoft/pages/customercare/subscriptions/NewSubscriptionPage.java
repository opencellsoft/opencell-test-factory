package com.opencellsoft.pages.customercare.subscriptions;

import java.awt.AWTException;
import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class NewSubscriptionPage extends TestBase {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;
	JavascriptExecutor js;
	Actions act;

	public NewSubscriptionPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		js = (JavascriptExecutor) driver;
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);
		act = new Actions(this.driver);

	}
	
	@FindBy(how = How.XPATH, using = "//input[@id='billingAccount']")
	WebElement customerInput;
	@FindBy(how = How.XPATH, using = "((//table//tbody//tr//td)[1]//span)[2]")
	WebElement consumer;
	@FindBy(how = How.XPATH, using = "(//table)[3]//tbody//tr//td[1]")
	WebElement consumer2;
	@FindBy(how = How.XPATH, using = "//input[@id='userAccount']")
	WebElement consumerInput;
	@FindBy(how = How.XPATH, using = "//div[@id='billingCycle']")
	WebElement billingCycleInput;
	@FindBy(how = How.XPATH, using = "//input[@id='subscriptionCode']")
	WebElement subscriptionCodeInput;
	@FindBy(how = How.XPATH, using = "//input[@id='offerTemplate']")
	WebElement offerInput;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter' or @title = 'Filter']")
	WebElement addFilterButton;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Code')]")
	WebElement filterOptionCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement offerCodeInput;
	@FindBy(how = How.CSS, using = "tbody tr:nth-child(1)")
	WebElement offerFound;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement saveSubscriptionButton;
	@FindBy(how = How.XPATH, using = ".//td[1]")
	WebElement selectedProduct;
//	@FindBy(how = How.XPATH, using = "//INPUT[@id='r0.c0']")
	@FindBy(how = How.XPATH, using = "//span[@class='MuiTypography-root MuiTypography-body1']")
	WebElement productNameSpan;
//	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[4]")
//	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/main[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/fieldset[1]/div[1]/form[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/span[1]/table[1]/tbody[1]/tr[1]/td[1]")
//	WebElement quantityCell;
	@FindBy(how = How.XPATH, using = "(//td)[6]")
	WebElement attributeCell;
	@FindBy(how = How.XPATH, using = "//table[1]/tbody[1]/tr[1]/td[1]")
	WebElement quantityCell;
	@FindBy(how = How.XPATH, using = "//td[@class='cell selected error']")
	WebElement selectedQuantityCell;
//	@FindBy(how = How.XPATH, using = "//input[@id='r0.c3']")
	@FindBy(how = How.XPATH, using = "//input[@id='r0.c0']")
	WebElement quantityInput;
	@FindBy(how = How.XPATH, using = "//table/tbody/tr[1]/td[2]")
	WebElement dateCell;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[5]")
	WebElement deliveryDate;
	@FindBy(how = How.XPATH, using = "//input[@id='subscriptionDate']")
	WebElement subscriptionDateInput;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiExpansionPanelSummary-content']")
	WebElement productDiv;
	@FindBy(how = How.XPATH, using = "//span[@title='Sort']//span[contains(text(),'Name')]")
	WebElement sortByNameButton;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day']//p[@class='MuiTypography-root MuiTypography-body2 MuiTypography-colorInherit'][normalize-space()='1']")
	WebElement subscriptionDate;
	@FindBy(how = How.ID, using = "seller")
	WebElement seller;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Renewal conditions')]")
	WebElement renewalConditionsTab;
	@FindBy(how = How.XPATH, using = "//input[@name='renewalRule.autoRenew']")
	WebElement renewAfterInitialTerm;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase name']")
	WebElement offerName;
	@FindBy(how = How.XPATH, using = "//div[@id='salesPersonName.code']")
	WebElement salesPerson;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Got it')]")
	WebElement gotItButton;	
	@FindBy(how = How.XPATH, using = "//input[contains(@id , 'salesPerson') or contains(@name , 'salesPerson')]")
	WebElement salesPersonInput;
	@FindBy(how = How.XPATH, using = "//div[contains(@id , 'salesPerson') or contains(@name , 'salesPerson')]")
	WebElement salesPersonDiv;
	@FindBy(how = How.XPATH, using = "//ul[@aria-labelledby = 'salesPersonName.code-label']//li[1]")
	WebElement salesPersonFirstChoice;
	@FindBy(how = How.XPATH, using = "//table//tr[1]//td[1]")
	WebElement salesPersonFirstChoice2;
	@FindBy(how = How.XPATH, using = "//li[@data-key = 'code' or text() = 'Code' or text() = 'Customer code']")
	WebElement filterByCustomerCode;
	@FindBy(how = How.XPATH, using = "//li[@data-key = 'code' or text() = 'Code' or text() = 'Offer code']")
	WebElement filterByOfferCode;
	@FindBy(how = How.XPATH, using = "//input[contains(@id , 'code') or title = 'Customer code']")
	WebElement customerCodeInput;
	@FindBy(how = How.XPATH, using = "//table//tbody//td[2]")
	WebElement firstResult;
	
	public void setCustomer(String customerCode) {
		clickOn(customerInput);
		clickOn(addFilterButton);
		clickOn(filterByCustomerCode);
		customerCodeInput.sendKeys(customerCode);
		waitPageLoaded();
		clickOn(firstResult);
	}
	public void setOffer(String offerCode) {
		clickOn(offerInput);
		clickOn(addFilterButton);
		clickOn(filterByOfferCode);
		offerCodeInput.sendKeys(offerCode);
		waitPageLoaded();
		clickOn(firstResult);
	}
// this method to click on customer input
	public void clickOnConsumerInput() {
		wait.until(ExpectedConditions.elementToBeClickable(consumerInput));
		clickOn(consumerInput);
	}
	
	public void clickOnGotItButton() {
		wait.until(ExpectedConditions.elementToBeClickable(gotItButton));
		clickOn(gotItButton);
	}

//this method is to select an existing consumer for the customer
	public void selectConsumer() {
		try {
			clickOn(consumer);
		}catch (Exception e) {
			clickOn(consumer2);
		}
	}

//this method is to click on billing cycle div
	public void clickOnBillingCycleInput() {
		// TODO Auto-generated method stub
		wait.until(ExpectedConditions.visibilityOf(billingCycleInput));
		clickOn(billingCycleInput);
	}

//this method is to select a billing cycle Option for the subscription
	public void selectBillingCycleOption(String billingCycleOption) {
		try {
			// TODO Auto-generated method stub
			driver.findElement(By.xpath("//li[normalize-space()='" + billingCycleOption + "']")).click();
		}catch (Exception e) {
			// TODO Auto-generated method stub
			driver.findElement(By.xpath("(//ul[@aria-labelledby = 'billingCycle-label']//li)[2]")).click();
		}
	}

//this method is to set the subscription code
	public void setSubscriptionCode(String subscriptionCode) {
		// TODO Auto-generated method stub
		wait.until(ExpectedConditions.visibilityOf(subscriptionCodeInput));
		subscriptionCodeInput.sendKeys(subscriptionCode);
		waitPageLoaded();
	}

	public void clickOnOfferInput() {
		// TODO Auto-generated method stub
		wait.until(ExpectedConditions.visibilityOf(offerInput));
		offerInput.click();
	}

	public void clickOnAddFilterButton() {
		// TODO Auto-generated method stub
		
		try {
			
			wait.until(ExpectedConditions.visibilityOf(addFilterButton));
			addFilterButton.click();
			}catch(Exception e) {		
				Actions actions = new Actions(driver);
				actions.moveToElement(addFilterButton).click().perform();
			}
	}

	public void selectFilterOption() {
		try {
			wait.until(ExpectedConditions.visibilityOf(filterOptionCode));
			filterOptionCode.click();
		}catch(Exception e) {		
			Actions actions = new Actions(driver);
			actions.moveToElement(filterOptionCode).click().perform();
		}
	}

	public void setOfferCodefilter(String offerCode) {
		// TODO Auto-generated method stub
		offerCodeInput.sendKeys(offerCode);
	}

// this method is to select an offer
	public void selectOffer(String strCodeOfferFound) throws AWTException {
		try{
			WebElement elem = 
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//mark[contains(text(),'" + strCodeOfferFound + "')]")));
		elem.click(); 	
		}catch (Exception e) {
			try {
				clickOn(driver.findElement(By.xpath("(//table//tbody//tr//td)[1]")));
			}catch (Exception ex) {
				clickOn(driver.findElement(By.xpath("//table//tbody")));
			}
		}
	}

	//this method is to select a seller for the subscription
	public void selectSeller(String sellerValue) {	
		seller.click();
		driver.findElement(By.xpath("//li[normalize-space()='" + sellerValue + "']")).click();
		waitPageLoaded();
	}
	
	public void selectSalesPerson(String salesPersonName) {	
		wait.until(ExpectedConditions.elementToBeClickable(this.salesPerson));
		this.salesPerson.click();
		driver.findElement(By.xpath("//li[contains(text(),'" + salesPersonName + "')]")).click();
	}
	
	public void selectProduct() throws InterruptedException, AWTException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", productNameSpan);
		Actions actions = new Actions(driver);
		actions.moveToElement(productNameSpan).click().perform();			
	}

	public void setProductQuantity(String quantity) throws InterruptedException {
		js.executeScript("arguments[0].setAttribute('class','cell selected')", quantityCell);
		act.doubleClick(quantityCell).perform();
		wait.until(ExpectedConditions.visibilityOf(quantityInput));
		act.doubleClick(quantityInput).perform();
		quantityInput.sendKeys(quantity);
		act.doubleClick(deliveryDate).perform();
	}

// this method is to Click on save subscription button
	public void clickOnSaveSubscriptionButton() {
		// TODO Auto-generated method stub
		wait.until(ExpectedConditions.visibilityOf(saveSubscriptionButton));
		saveSubscriptionButton.submit();
	}
	
	public void clickOnSaveButton() {
		clickOn(saveSubscriptionButton);
	}

	public void setSubscriptionDate() {
		// TODO Auto-generated method stub
		subscriptionDate.click();
	}

	public void clickOnSubscriptionDateInput() {
		// TODO Auto-generated method stub
		subscriptionDateInput.click();
	}

	public void clickOnProductDiv() throws AWTException {
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", productDiv);
			wait.until(ExpectedConditions.elementToBeClickable(productDiv));
			productDiv.click();
		}catch (Exception e) {
			clickOn(productDiv);
		}
	}
	
	public void setQuantity(String quantity) throws InterruptedException, AWTException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(quantityCell));
		}
		catch(org.openqa.selenium.TimeoutException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(quantityCell));
		}
		js.executeScript("arguments[0].setAttribute('class','cell selected')", quantityCell);	
		quantityCell.click();
		act.doubleClick(quantityCell).perform();
		wait.until(ExpectedConditions.visibilityOf(quantityInput));
		quantityInput.sendKeys(quantity);
	}
	
	public void setAttribute() {
		wait.until(ExpectedConditions.visibilityOf(attributeCell));
		js.executeScript("arguments[0].setAttribute('class','cell selected')", attributeCell);
		act.doubleClick(attributeCell).perform();
		Actions keyDown = new Actions(driver);
		keyDown.sendKeys(Keys.chord(Keys.DOWN, Keys.ENTER)).perform();
		keyDown.sendKeys(Keys.ESCAPE).perform();
	}

	public void clickOnSortButton() {
		// TODO Auto-generated method stub
		wait.until(ExpectedConditions.elementToBeClickable(sortByNameButton));
		sortByNameButton.click();
	}

	public void clickOnRenewalConditionsTab() {
		clickOn(renewalConditionsTab);
	}
	public void clickOnRenewAfterInitialTerm() {
		// TODO Auto-generated method stub
		clickOn(renewAfterInitialTerm);
	}
	
	public void setOffername(String offerName) {
		wait.until(ExpectedConditions.visibilityOf(this.offerName));
		this.offerName.sendKeys(offerName);
	}
	
	public void selectOfferByName(String offerName) throws AWTException {
		try{
			WebElement elem = 
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//mark[contains(text(),'" + offerName + "')]")));
		elem.click(); 	
		}catch (Exception e) {
			try {
				clickOn(driver.findElement(By.xpath("(//table//tbody//tr//td)[1]")));
			}catch (Exception ex) {
				clickOn(driver.findElement(By.xpath("//table//tbody")));
			}
		}
	}
	
	public void setSalesPerson() {
		try {
			clickOn(salesPersonDiv);
			clickOn(salesPersonFirstChoice);
		}catch (Exception e) {
			clickOn(salesPersonInput);
			clickOn(salesPersonFirstChoice2);
		}
		
	}
}
