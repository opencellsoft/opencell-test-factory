package com.opencellsoft.pages.customercare.securitydeposits;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class ListOfSecurityDepositPage extends TestBase {

	public ListOfSecurityDepositPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'New security deposit')]")
	WebElement newSecurityDepositButton;
	
	public void clickOnNewSecurityDepositButton() {
		newSecurityDepositButton.click();
	}
	
	public void selectSecurityDeposit(String sd) {
		driver.findElement(By.xpath("//table[@customrowclick='[object Object]']//span[text() = '" + sd + "']")).click();
	}
}
