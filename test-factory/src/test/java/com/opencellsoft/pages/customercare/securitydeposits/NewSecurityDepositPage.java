package com.opencellsoft.pages.customercare.securitydeposits;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class NewSecurityDepositPage extends TestBase {

	public NewSecurityDepositPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "template.id")
	WebElement templateName;
	@FindBy(how = How.XPATH, using = "//td//span[contains(text(), 'DEFAULT_SD_TEMPLATE')]")
	WebElement defaultSDTemplate;
	@FindBy(how = How.ID, using = "code")
	WebElement securityDepositeName;
	@FindBy(how = How.ID, using = "description")
	WebElement description;
	@FindBy(how = How.ID, using = "amount")
	WebElement expectedBalance;
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'saveButtonRow')]//button[@type = 'submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//button//span[contains(text(), 'Instantiate')]")
	WebElement InstantiateButton;
	
	public void selectTemplateName() {
		this.templateName.click();
		waitPageLoaded();
		this.defaultSDTemplate.click();
		waitPageLoaded();
	}
	
	public void setSecurityDepositName(String Name) {
		this.securityDepositeName.sendKeys(Name);
	}
	
	public void setSecurityDepositDescription(String description) {
		this.description.sendKeys(description);
	}	
	
	public void setExpectedBalance(String amount) {
		this.expectedBalance.sendKeys(amount);
	}
	
	public void clickOnSaveButton() {
		clickOn(this.saveButton);
	}
	
	public void clickOnInstantiateButton() {
		this.InstantiateButton.click();
	}
}
