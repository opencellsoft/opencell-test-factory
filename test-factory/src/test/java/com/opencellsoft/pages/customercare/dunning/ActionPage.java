package com.opencellsoft.pages.customercare.dunning;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class ActionPage extends TestBase {

	public ActionPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//input[@id = 'name']")
	WebElement nameInput;
	@FindBy(how = How.XPATH, using = "//textarea[@id = 'description']")
	WebElement descriptionInput;
	@FindBy(how = How.XPATH, using = "//*[@aria-label = 'Action Type' or @id = 'actionType']")
	WebElement actionTypeInput;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Edit']")
	WebElement updateButton;
	@FindBy(how = How.XPATH, using = "//button[@type = 'submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//span[text() = 'Go Back']")
	WebElement goBackButton;
	
	public void setName(String name) {
		nameInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		nameInput.sendKeys(name);
		waitPageLoaded();
	}
	
	public void setDescription(String description) {
		descriptionInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		descriptionInput.sendKeys(description);
		waitPageLoaded();
	}
	
	public void setType(String type) {
		clickOn(actionTypeInput);
		clickOn(driver.findElement(By.xpath("//*[text() = '" + type + "']")));
	}
	
	public void clickOnUpdateButton() {
		clickOn(updateButton);
	}
	
	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
	
	public void clickOnGoBackButton() {
		clickOn(goBackButton);
	}
}
