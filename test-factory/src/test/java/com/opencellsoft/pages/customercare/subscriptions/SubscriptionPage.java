package com.opencellsoft.pages.customercare.subscriptions;

import java.awt.AWTException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class SubscriptionPage extends TestBase {
	WebDriver driver;
	Wait<WebDriver> fWait;
	Wait<WebDriver> wait;
	JavascriptExecutor js;
	Actions act;

	public SubscriptionPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		js = (JavascriptExecutor) driver;
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(2)).ignoring(NoSuchElementException.class);
		act = new Actions(this.driver);
	}

	@FindBy(how = How.XPATH, using = "//*[@id=\"main-content\"]/div/div[2]/div[2]/div/div/fieldset/div/form/div/div[2]/div/div/div/div/div/div/div/span/table/tbody/tr/td[4]/th/button/span[1]")
	WebElement editAttributeButton;
	@FindBy(how = How.XPATH, using = "/html/body/div[2]/div[3]/div/div[1]/div/div/div/span/table/tbody/tr/td[2]")
	WebElement attributeValueCell;
	@FindBy(how = How.XPATH, using = "(//BUTTON[@class='MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedPrimary MuiButton-containedSizeSmall MuiButton-sizeSmall'])[2]")
	WebElement saveSubscriptionButton;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement saveSubscriptionButtonEdit;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedPrimary']")
	WebElement activateButton;
	@FindBy(how = How.XPATH, using = "//table[1]/tbody[1]/tr[1]/th[2]/button[1]")
	WebElement activateAttributeInstancesButton;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/th[2]/button[1]")
	WebElement activateProductButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Activate')]")
	WebElement activateProductInstanceButton;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedPrimary']//span[@class='MuiButton-label'][normalize-space()='Activate']")
	//span[@class='MuiButton-label'][contains(text(),'Activate')]
	WebElement confirmActivationButton;
	@FindBy(how = How.XPATH, using = "//tbody[1]/tr[1]/td[3]")
	WebElement dateCell;
	@FindBy(how = How.XPATH, using = "//tbody[1]/tr[1]/td[4]")
	WebElement attributeCell;
	@FindBy(how = How.XPATH, using = "//table[1]/tbody[1]/tr[1]/td[2]")
	WebElement quantityCell;
	@FindBy(how = How.XPATH, using = "//input[@id='r0.c1']")
	WebElement quantityInput;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSizeSmall MuiButton-sizeSmall']")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "//*[contains(text() , 'Go Back')]")
	WebElement goBack;
	@FindBy(how = How.XPATH, using = "(//div[contains(@class,'PageTitle')]//a)[2]")
	WebElement goToCustomer;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiExpansionPanelSummary-content']")
	WebElement selectedProductDiv;
	@FindBy(how = How.XPATH, using = "//button[@role='tab']//span[text()='Acces points']")
	WebElement accessPointTab;
	@FindBy(how = How.XPATH, using = "//div[@class='ButtonGroupGutters']//button[contains(@class,'MuiButtonBase-root MuiButton-root')]")
	WebElement createAPButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Got it']")
	List<WebElement> gotItButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Add product')]")
	WebElement addProductButton;	
	@FindBy(how = How.XPATH, using = "//table[1]/tbody[1]/tr[1]/td[3]")
	WebElement statusColumn;	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Attributes')]")
	WebElement attributesTab;		


	public void clickOnGotItButton() {
		if (!gotItButton.isEmpty()) {
			this.gotItButton.get(0).click();
			waitPageLoaded();
		}
	}

	public void clickOnAccessPointTab() {
		clickOn(accessPointTab);
	}
	public void goToCustomer() {
		clickOn(goToCustomer);
	}

	public void clickOnCreateAPButton() {
		try {
			clickOn(createAPButton);
		}catch (Exception e) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", createAPButton);
			act.click(createAPButton).perform();}
	}

	public void clickOnEditAttribute() throws AWTException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", editAttributeButton);
		wait.until(ExpectedConditions.visibilityOf(editAttributeButton));
		this.editAttributeButton.click();
	}

	public void setListAttributeValues() throws InterruptedException {
		wait.until(ExpectedConditions.visibilityOf(attributeValueCell));
		js.executeScript("arguments[0].setAttribute('class','cell selected')", attributeValueCell);
		attributeValueCell.click();
		Actions act = new Actions(this.driver);
		Thread.sleep(1000);
		act.doubleClick(attributeValueCell).perform();
		Actions keyDown = new Actions(driver);
		keyDown.sendKeys(Keys.chord(Keys.DOWN, Keys.ENTER)).perform();
	}

	// method to get the appeared message after operation
	public String getTextMessage() throws AWTException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", messageTextBox);
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

	public void clickOnActivateAttributeInstancesButton() {
		wait.until(ExpectedConditions.elementToBeClickable(activateAttributeInstancesButton));
		activateAttributeInstancesButton.click();
	}

	public void clickOnActivateProductButton() {
		// TODO Auto-generated method stub
		try {
			wait.until(ExpectedConditions.elementToBeClickable(activateProductButton));
			clickOn(activateProductButton);
		}
		catch (Exception e) {
			clickOn(activateProductButton);
		}
	}

	public void clickOnActivateProductInstanceButton() {
		wait.until(ExpectedConditions.elementToBeClickable(activateProductInstanceButton));
		activateProductInstanceButton.click();
	}

	public void clickOnConfirmActivationProduct() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(confirmActivationButton));
			clickOn(confirmActivationButton);
		}
		catch (Exception e) {
			clickOn(confirmActivationButton);
		}
	}

	public void clickOnSaveSubscriptionButton() {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", saveSubscriptionButton);
		wait.until(ExpectedConditions.visibilityOf(saveSubscriptionButton));
		saveSubscriptionButton.submit();
	}

	public void setQuantity(String quantity) {
		wait.until(ExpectedConditions.visibilityOf(quantityCell));
		js.executeScript("arguments[0].setAttribute('class','cell selected')", quantityCell);
		act.doubleClick(quantityCell).perform();
		quantityInput.sendKeys(quantity);
	}

	public void setAttribute() {
		wait.until(ExpectedConditions.visibilityOf(attributeCell));
		js.executeScript("arguments[0].setAttribute('class','cell selected')", attributeCell);
		act.doubleClick(attributeCell).perform();
		Actions keyDown = new Actions(driver);
		keyDown.sendKeys(Keys.chord(Keys.UP, Keys.ENTER)).perform();
		keyDown.sendKeys(Keys.ESCAPE).perform();
	}

	public void clickOnGoBackButton() {
		// TODO Auto-generated method stub
		try {
			wait.until(ExpectedConditions.visibilityOf(goBackButton));
			goBackButton.click();
		} catch (org.openqa.selenium.StaleElementReferenceException ex) {
			fWait.until(ExpectedConditions.visibilityOf(goBackButton));
			goBackButton.click();
		}
	}
	
	public void clickOnGoBack() {
		clickOn(goBack);
	}

	public void clickOnAddProductButton() {
		wait.until(ExpectedConditions.visibilityOf(addProductButton));
		//addProductButton.click();
		clickOn(addProductButton);
	}

	public void clickOnProduct(String productCode) {
		try {
			WebElement elem = 
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'" + productCode + "')]")));
			elem.click();	
		}catch (Exception e) {
			clickOn(driver.findElement(By.xpath("//table//tbody//tr")));
		}
	}

	public void clickOnProduct() {
		clickOn(driver.findElement(By.xpath("(//table//tbody//tr)[2]//td[1]")));
	}

	public void clickOnSelectedProductDiv() {
		try {
			wait.until(ExpectedConditions.visibilityOf(selectedProductDiv));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", selectedProductDiv);
			clickOn(selectedProductDiv);
		}catch (Exception e) {
			clickOn(selectedProductDiv);
		}	
	}

	public void clickOnProductCode(String productCode) {
		WebElement elem = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//span[contains(text(),'" + productCode + "')]")));
		elem.click();
	}

	public void clickOnProductName(String productName) {
		WebElement elem = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//span[@class='value-viewer'][contains(text(),'" + productName + "')]")));
		elem.click();
	}

	public void clickOnAttributesTab() {
		wait.until(ExpectedConditions.visibilityOf(this.attributesTab));
		attributesTab.click();
	}


	public void setAttribute(String value, String columnNumber) {
		WebElement attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + columnNumber + "]")));
		WebElement attrColumn2 = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[2]")));
		act = new Actions(driver);
		act.doubleClick(attrColumn).perform();
		waitPageLoaded();
		WebElement attrInput = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + columnNumber + "]//input")));
		if(value == "true") {
			clickOn(attrInput);
		}
		else if (value == "false") {
			attrColumn2.click();
		}
		else {
			attrInput.sendKeys(value);
			waitPageLoaded();
			attrColumn2.click();
		}
		waitPageLoaded();
	}

	public void setAttributes141X(String integerAttribut, String numericAttribut, String booleanAttribut, String textAttribut, String version) {
		act = new Actions(driver);
		WebElement attrInput = null;
		WebElement attrColumn = null;
		String attrType = null;
		for( int i = 6; i< 11; i++) {
			/*  get the attribute name */
			attrType = driver.findElement((By.xpath("(//table)[1]//thead//tr[1]//th[" + i + "]//div"))).getText();

			/* enter the attribute column and find the attribute input */
			try {
				attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//div")));
				act.doubleClick(attrColumn).perform();
				waitPageLoaded();
				attrInput = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//input")));
				waitPageLoaded();
			}catch (Exception e) {
				attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//div")));
				act.doubleClick(attrColumn).perform();
				waitPageLoaded();
				attrInput = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//input")));
				waitPageLoaded();
			}
			/* Fill the field with the corresponding value */
			if(attrType.equalsIgnoreCase("integerAttribut") ) {
				attrInput.sendKeys(integerAttribut);
				waitPageLoaded();
			}
			else if (attrType.equalsIgnoreCase("numericAttribut")) {
				attrInput.sendKeys(numericAttribut);
				waitPageLoaded();
			}
			else if (attrType.equalsIgnoreCase("textAttribut")) {
				attrInput.sendKeys(textAttribut);
				waitPageLoaded();
			}
			else if(attrType.equalsIgnoreCase("booleanAttribut")) {
				clickOn(attrInput);
				waitPageLoaded();

				if(booleanAttribut == "false"){
					try {
						attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//div")));
						act.doubleClick(attrColumn).perform();
					}catch (Exception e) {
						attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]")));
						act.doubleClick(attrColumn).perform();
					}
					waitPageLoaded();
					attrInput = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//input")));
					clickOn(attrInput);	
				}
			}
			/* exit the column */
			try {
				attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + (i-2) + "]")));
				act.click(attrColumn).perform();
			}catch (Exception e) {
				attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + (i-2) + "]//div")));
				act.click(attrColumn).perform();
			}
			waitPageLoaded();
		}
	}

	public void setAttributes141X2(String booleanattr, String dateattr, String elattr, String emailattr, String informationattr, String integerattr, String listofnumericvaluesattr, String listoftextvaluesattr, String ml_numericvaluesattr, String ml_textvaluesattr, String numericattr, String phoneattribut, String textattr, String version) {
		act = new Actions(driver);
		WebElement attrInput = null;
		WebElement attrColumn = null;
		String attrType = null;
		for( int i = 6; i< 11; i++) {
			/*  get the attribute name */
			attrType = driver.findElement((By.xpath("(//table)[1]//thead//tr[1]//th[" + i + "]//div"))).getText();

			/* enter the attribute column and find the attribute input */
			try {
				attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//div")));
				act.doubleClick(attrColumn).perform();
				waitPageLoaded();
				attrInput = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//input")));
				waitPageLoaded();
			}catch (Exception e) {
				attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//div")));
				act.doubleClick(attrColumn).perform();
				waitPageLoaded();
				attrInput = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//input")));
				waitPageLoaded();
			}
			
			// Fill the field with the corresponding value
			switch(attrType.toLowerCase()){

			case "booleanattribut": 
				clickOn(attrInput);
				waitPageLoaded();

				if(booleanattr == "false"){
					try {
						attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//div")));
						act.doubleClick(attrColumn).perform();
					}catch (Exception e) {
						attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]")));
						act.doubleClick(attrColumn).perform();
					}
					waitPageLoaded();
					attrInput = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//input")));
					clickOn(attrInput);	
				}

				break;
			case "dateattribut": 
				clickOn(attrInput);
				WebElement tenthOfTheMonth = driver.findElement(By.xpath("//p[text() = '10']"));
				clickOn(tenthOfTheMonth);
				break;
			case "emailattribut":
				attrInput.sendKeys(emailattr);
				waitPageLoaded();
				break;
			case "informationattribut":
				attrInput.sendKeys(informationattr);
				waitPageLoaded();
				break;
			case "integerattribut": 
				attrInput.sendKeys(integerattr);
				waitPageLoaded();
				break;
			case "listofnumericvaluesattribut": 
				try {
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + listofnumericvaluesattr + "')]")));
				}catch (Exception e) {
					clickOn(attrInput);
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + listofnumericvaluesattr + "')]")));
				}
				break;
			case "listoftextvaluesattribut": 
				try {
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + listoftextvaluesattr + "')]")));
				}catch (Exception e) {
					clickOn(attrInput);
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + listoftextvaluesattr + "')]")));
				}
				break;
			case "multiplelistnumericvaluesattribut": 
				try {
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + ml_numericvaluesattr + "')]")));
				}catch (Exception e) {
					clickOn(attrInput);
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + ml_numericvaluesattr + "')]")));
				}
				break;
			case "multiplelisttextvaluesattribut": 
				try {
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + ml_textvaluesattr + "')]")));
				}catch (Exception e) {
					clickOn(attrInput);
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + ml_textvaluesattr + "')]")));
				}
				break;		
			case "numericattribut":
				attrInput.sendKeys(numericattr);
				waitPageLoaded();
				break;
			case "phoneattribut":
				attrInput.sendKeys(phoneattribut);
				waitPageLoaded();
				break;
			case "textattribut":
				attrInput.sendKeys(textattr);
				waitPageLoaded();
				break;
			default:
				System.out.println("No choice");
				break;
			}
			
			/* exit the column */
			try {
				attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + (i-2) + "]")));
				act.click(attrColumn).perform();
			}catch (Exception e) {
				attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + (i-2) + "]//div")));
				act.click(attrColumn).perform();
			}
			waitPageLoaded();
		}
	}
	
	public void setAttributes150X(String integerAttribut, String numericAttribut, String booleanAttribut, String textAttribut, String version) {
		act = new Actions(driver);
		WebElement attrInput = null;
		WebElement attrColumn = null;
		String attrType = null;
		for( int i = 6; i< 11; i++) {
			/*  get the attribute name */
			attrType = driver.findElement((By.xpath("(//table)[1]//thead//tr[1]//th[" + i + "]//div"))).getText();
			//			System.out.println(attrType);
			/* enter the attribute column and find the attribute input */
			if(!attrType.equalsIgnoreCase("booleanAttribut") && !attrType.equalsIgnoreCase("elattribut") ) {
				try {
					attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//div")));
					act.doubleClick(attrColumn).perform();
					waitPageLoaded();
					attrInput = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//input")));
					waitPageLoaded();
				}catch (Exception e) {
					attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//div")));
					act.doubleClick(attrColumn).perform();
					waitPageLoaded();
					attrInput = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//input")));
					waitPageLoaded();
				}
			}
			/* Fill the field with the corresponding value */
			if(attrType.equalsIgnoreCase("integerAttribut") ) {
				attrInput.sendKeys(integerAttribut);
				waitPageLoaded();
			}
			else if (attrType.equalsIgnoreCase("numericAttribut")) {
				attrInput.sendKeys(numericAttribut);
				waitPageLoaded();
			}
			else if (attrType.equalsIgnoreCase("textAttribut")) {
				attrInput.sendKeys(textAttribut);
				waitPageLoaded();
			}
			else if(attrType.equalsIgnoreCase("booleanAttribut")) {

				attrColumn = driver.findElement((By.xpath("//table[contains(@class, 'data-grid')]//span[contains(@class, 'MuiCheckbox-root')]")));
				String class_name = attrColumn.getAttribute("class");
				if((!class_name.contains("Mui-checked") && booleanAttribut == "true") || (class_name.contains("Mui-checked") && booleanAttribut == "false")) {
					attrInput = driver.findElement((By.xpath("//table[contains(@class, 'data-grid')]//input[@type = 'checkbox']")));
					clickOn(attrInput);
				}
			}
			/* exit the column */
			try {
				attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[2]")));
				act.click(attrColumn).perform();
			}catch (Exception e) {
				attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[3]//div")));
				act.click(attrColumn).perform();
			}
			waitPageLoaded();
		}

	}
	
	public void setAttributes150X2(String booleanattr, String dateattr, String elattr, String emailattr, String informationattr, String integerattr, String listofnumericvaluesattr, String listoftextvaluesattr, String ml_numericvaluesattr, String ml_textvaluesattr, String numericattr, String phoneattribut, String textattr, String version) {
		act = new Actions(driver);
		WebElement attrInput = null;
		WebElement attrColumn = null;
		String attrType = null;
		for( int i = 6; i< 19; i++) {
			//  get the attribute name from the table head
			attrType = driver.findElement((By.xpath("(//table)[1]//thead//tr[1]//th[" + i + "]//div"))).getText();
			//			System.out.println(attrType);
			// enter in the attribute column and find the attribute input
			if(!attrType.equalsIgnoreCase("booleanAttribut") && !attrType.equalsIgnoreCase("elattribut") ) {
				try {
					attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]")));
					act.doubleClick(attrColumn).perform();
					waitPageLoaded();
					attrInput = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//input")));
					waitPageLoaded();
				}catch (Exception e) {
					attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//div")));
					act.doubleClick(attrColumn).perform();
					waitPageLoaded();
					attrInput = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//input")));
					waitPageLoaded();
				}
			}
			// Fill the field with the corresponding value
			switch(attrType.toLowerCase()){

			case "booleanattribut": 
				attrColumn = driver.findElement((By.xpath("//table[contains(@class, 'data-grid')]//span[contains(@class, 'MuiCheckbox-root')]")));
				String class_name;
				try {
					class_name = attrColumn.getAttribute("class");
				}catch (Exception e) {
					attrColumn = driver.findElement((By.xpath("//table[contains(@class, 'data-grid')]//span[contains(@class, 'MuiCheckbox-root')]")));
					class_name = attrColumn.getAttribute("class");
				}
				
				if((!class_name.contains("Mui-checked") && booleanattr == "true") || (class_name.contains("Mui-checked") && booleanattr == "false")) {
					attrInput = driver.findElement((By.xpath("//table[contains(@class, 'data-grid')]//input[@type = 'checkbox']")));
					clickOn(attrInput);
				}

				break;
			case "dateattribut": 
				clickOn(attrInput);
				WebElement tenthOfTheMonth = driver.findElement(By.xpath("//p[text() = '10']"));
				clickOn(tenthOfTheMonth);
				break;
			case "emailattribut":
				attrInput.sendKeys(emailattr);
				waitPageLoaded();
				break;
			case "informationattribut":
				attrInput.sendKeys(informationattr);
				waitPageLoaded();
				break;
			case "integerattribut": 
				attrInput.sendKeys(integerattr);
				waitPageLoaded();
				break;
			case "listofnumericvaluesattribut": 
				try {
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + listofnumericvaluesattr + "')]")));
				}catch (Exception e) {
					clickOn(attrInput);
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + listofnumericvaluesattr + "')]")));
				}
				break;
			case "listoftextvaluesattribut": 
				try {
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + listoftextvaluesattr + "')]")));
				}catch (Exception e) {
					clickOn(attrInput);
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + listoftextvaluesattr + "')]")));
				}
				break;
			case "multiplelistnumericvaluesattribut": 
				try {
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + ml_numericvaluesattr + "')]")));
				}catch (Exception e) {
					clickOn(attrInput);
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + ml_numericvaluesattr + "')]")));
				}
				break;
			case "multiplelisttextvaluesattribut": 
				try {
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + ml_textvaluesattr + "')]")));
				}catch (Exception e) {
					clickOn(attrInput);
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + ml_textvaluesattr + "')]")));
				}
				break;		
			case "numericattribut":
				attrInput.sendKeys(numericattr);
				waitPageLoaded();
				break;
			case "phoneattribut":
				attrInput.sendKeys(phoneattribut);
				waitPageLoaded();
				break;
			case "textattribut":
				attrInput.sendKeys(textattr);
				waitPageLoaded();
				break;
			default:
				System.out.println("No choice");
				break;
			}
			
			
			/* exit the column */
			try {
				attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[2]")));
				act.click(attrColumn).perform();
			}catch (Exception e) {
				attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[3]//div")));
				act.click(attrColumn).perform();
			}
			waitPageLoaded();
		}

	}
	
	public void setAttributes16X(String integerAttribut, String numericAttribut, String booleanAttribut, String textAttribut, String version) {
		act = new Actions(driver);
		WebElement attrInput = null;
		WebElement attrColumn = null;
		String attrType = null;
		for( int j = 7; j< 12; j++) {
			/*  get the attribute name */
			int i = j;
			try {
				attrType = driver.findElement((By.xpath("(//table)[1]//thead//tr[1]//th[" + i + "]//div"))).getText();
			}catch (Exception e) {
				i = 6;
				attrType = driver.findElement((By.xpath("(//table)[1]//thead//tr[1]//th[" + i + "]//div"))).getText();
			}

			//			System.out.println(attrType);
			/* enter the attribute column and find the attribute input */
			if(!attrType.equalsIgnoreCase("booleanAttribut") && !attrType.equalsIgnoreCase("elattribut") ) {
				try {
					attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//div")));
					act.doubleClick(attrColumn).perform();
					waitPageLoaded();
					attrInput = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//input")));
					waitPageLoaded();
				}catch (Exception e) {
					attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//div")));
					act.doubleClick(attrColumn).perform();
					waitPageLoaded();
					attrInput = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//input")));
					waitPageLoaded();
				}
			}
			/* Fill the field with the corresponding value */
			if(attrType.equalsIgnoreCase("integerAttribut") ) {
				attrInput.sendKeys(integerAttribut);
				waitPageLoaded();
			}
			else if (attrType.equalsIgnoreCase("numericAttribut")) {
				attrInput.sendKeys(numericAttribut);
				waitPageLoaded();
			}
			else if (attrType.equalsIgnoreCase("textAttribut")) {
				attrInput.sendKeys(textAttribut);
				waitPageLoaded();
			}
			else if(attrType.equalsIgnoreCase("booleanAttribut")) {

				attrColumn = driver.findElement((By.xpath("//table[contains(@class, 'data-grid')]//span[contains(@class, 'MuiCheckbox-root')]")));
				String class_name = attrColumn.getAttribute("class");
				if((!class_name.contains("Mui-checked") && booleanAttribut == "true") || (class_name.contains("Mui-checked") && booleanAttribut == "false")) {
					attrInput = driver.findElement((By.xpath("//table[contains(@class, 'data-grid')]//input[@type = 'checkbox']")));
					clickOn(attrInput);
				}
			}
			/* exit the column */
			try {
				attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[2]")));
				act.click(attrColumn).perform();
			}catch (Exception e) {
				attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[3]//div")));
				act.click(attrColumn).perform();
			}
			waitPageLoaded();
		}

	}

	public void setAttributes16X2(String booleanattr, String dateattr, String elattr, String emailattr, String informationattr, String integerattr, String listofnumericvaluesattr, String listoftextvaluesattr, String ml_numericvaluesattr, String ml_textvaluesattr, String numericattr, String phoneattribut, String textattr, String version) {
		act = new Actions(driver);
		WebElement attrInput = null;
		WebElement attrColumn = null;
		String attrType = null;
		for( int j = 7; j< 20; j++) {
			/*  get the attribute name */
			int i = j;
			try {
				attrType = driver.findElement((By.xpath("(//table)[1]//thead//tr[1]//th[" + i + "]//div"))).getText();
			}catch (Exception e) {
				i = 6;
				attrType = driver.findElement((By.xpath("(//table)[1]//thead//tr[1]//th[" + i + "]//div"))).getText();
			}

			System.out.println(attrType);
			/* enter the attribute column and find the attribute input */
			if(!attrType.equalsIgnoreCase("booleanAttribut") && !attrType.equalsIgnoreCase("elattribut") ) {
				try {
					attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//div")));
					act.doubleClick(attrColumn).perform();
					waitPageLoaded();
					attrInput = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//input")));
					waitPageLoaded();
				}catch (Exception e) {
					attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//span")));
					act.doubleClick(attrColumn).perform();
					waitPageLoaded();
					attrInput = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[" + i + "]//input")));
					waitPageLoaded();
				}
			}
			/* Fill the field with the corresponding value */
			switch(attrType.toLowerCase()){

			case "booleanattribut": 
				attrColumn = driver.findElement((By.xpath("//table[contains(@class, 'data-grid')]//span[contains(@class, 'MuiCheckbox-root')]")));
				String class_name;
				try {
					class_name = attrColumn.getAttribute("class");
				}catch (Exception e) {
					attrColumn = driver.findElement((By.xpath("//table[contains(@class, 'data-grid')]//span[contains(@class, 'MuiCheckbox-root')]")));
					class_name = attrColumn.getAttribute("class");
				}
				
				if((!class_name.contains("Mui-checked") && booleanattr == "true") || (class_name.contains("Mui-checked") && booleanattr == "false")) {
					attrInput = driver.findElement((By.xpath("//table[contains(@class, 'data-grid')]//input[@type = 'checkbox']")));
					clickOn(attrInput);
				}
				break;
			case "dateattribut": 
				clickOn(attrInput);
				WebElement tenthOfTheMonth = driver.findElement(By.xpath("//p[text() = '10']"));
				clickOn(tenthOfTheMonth);
				break;
			case "emailattribut":
				attrInput.sendKeys(emailattr);
				waitPageLoaded();
				break;
			case "informationattribut":
				attrInput.sendKeys(informationattr);
				waitPageLoaded();
				break;
			case "integerattribut": 
				attrInput.sendKeys(integerattr);
				waitPageLoaded();
				break;
			case "listofnumericvaluesattribut": 
				try {
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + listofnumericvaluesattr + "')]")));
				}catch (Exception e) {
					clickOn(attrInput);
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + listofnumericvaluesattr + "')]")));
				}
				break;
			case "listoftextvaluesattribut": 
				try {
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + listoftextvaluesattr + "')]")));
				}catch (Exception e) {
					clickOn(attrInput);
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + listoftextvaluesattr + "')]")));
				}
				break;
			case "multiplelistnumericvaluesattribut": 
				try {
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + ml_numericvaluesattr + "')]")));
				}catch (Exception e) {
					clickOn(attrInput);
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + ml_numericvaluesattr + "')]")));
				}
				break;
			case "multiplelisttextvaluesattribut": 
				try {
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + ml_textvaluesattr + "')]")));
				}catch (Exception e) {
					clickOn(attrInput);
					clickOn(driver.findElement(By.xpath("//*[(text()= '" + ml_textvaluesattr + "')]")));
				}
				break;		
			case "numericattribut":
				attrInput.sendKeys(numericattr);
				waitPageLoaded();
				break;
			case "phoneattribut":
				attrInput.sendKeys(phoneattribut);
				waitPageLoaded();
				break;
			case "textattribut":
				attrInput.sendKeys(textattr);
				waitPageLoaded();
				break;
			default:
				System.out.println("No choice");
				break;
			}

			/* exit the column */
			try {
				attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[2]")));
				act.click(attrColumn).perform();
			}catch (Exception e) {
				attrColumn = driver.findElement((By.xpath("(//table)[1]//tbody//tr[1]//td[3]//div")));
				act.click(attrColumn).perform();
			}
			waitPageLoaded();
		}

	}
}
