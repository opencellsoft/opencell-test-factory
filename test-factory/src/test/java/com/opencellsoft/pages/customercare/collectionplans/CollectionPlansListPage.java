package com.opencellsoft.pages.customercare.collectionplans;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class CollectionPlansListPage extends TestBase  {

	public CollectionPlansListPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//div[contains(@id, 'status')]")
	WebElement statusSelectBox;
	@FindBy(how = How.XPATH, using = "//div[contains(@id, 'EXPORT')]")
	WebElement downloadCollection;
	
	public void selectStatus(String value) {
		clickOn(statusSelectBox);
		clickOn(driver.findElement(By.xpath("//li[text() = '" + value + "']")));
	}
	
	public void exportFormat(String value) {
		clickOn(downloadCollection);
		clickOn(driver.findElement(By.xpath("//span[text() = '" + value + "']")));
	}
	
}
