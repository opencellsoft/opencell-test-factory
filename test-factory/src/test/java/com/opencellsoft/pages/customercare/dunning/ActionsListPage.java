package com.opencellsoft.pages.customercare.dunning;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class ActionsListPage  extends TestBase  {

	public ActionsListPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//*[@title = 'Create' or @id = 'CREATE']")
	WebElement createButton;
	@FindBy(how = How.XPATH, using = "//input[@title = 'Action Name' or @name = 'wildcardOrIgnoreCase code']")
	WebElement searchInput;
	@FindBy(how = How.XPATH, using = "//table[contains(@id, 'actions')]//tbody//*[@role = 'row' or contains(@class ,'MuiTableRow-root')]")
	List<WebElement> actionsList;

	
	public void clickOnCreateButton() {
		clickOn(createButton);
	}
	
	public void setSearchInput(String value) {
		searchInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		searchInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public List<WebElement> actionsList() {
		return actionsList;
	}
	
	public void goToActionDetails(String value) {
		clickOn(driver.findElement(By.xpath("//*[contains(text() , '" + value + "')]")));
	}
	
	public void activateFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//li//*[text()= '" + value + "']")));
	}
	
	
	public void filterBy(String value) {
		clickOn(driver.findElement(By.xpath("//li[text()= '" + value + "']")));
	}
	
	public void clickOnRemoveFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//button[@data-key = '" + value + "']")));
	}
	
	public void sortBy(String value) {
		clickOn(driver.findElement(By.xpath("//span[contains(@class, 'MuiButtonBase-root') and contains(@data-field, '" + value + "')]")));
	}
}
