package com.opencellsoft.pages.customercare.dunning;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class TemplatePage extends TestBase {

	public TemplatePage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//input[@id = 'code']")
	WebElement nameInput;
	@FindBy(how = How.XPATH, using = "//*[@aria-label = 'Channel'or @id = 'channel']")
	WebElement channelInput;
	@FindBy(how = How.XPATH, using = "//*[@aria-label = 'Language' or @id = 'language.id']")
	WebElement languageInput;
	@FindBy(how = How.XPATH, using = "//form[@class='DunningTemplates']//span[contains(@class, 'MuiIconButton-root') or contains(@class, 'MuiSwitch-switchBase')]")
	WebElement activeInput;
	@FindBy(how = How.XPATH, using = "//*[@name = 'subject']")
	WebElement objectInput;
	@FindBy(how = How.XPATH, using = "//*[contains(@class , 'ql-editor')]")
	WebElement bodyInput;
	@FindBy(how = How.XPATH, using = "//button[@type = 'submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//span[text() = 'Go Back']")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "//span[text() = 'Delete']")
	WebElement deleteButton;
	@FindBy(how = How.XPATH, using = "//div[@role ='dialog']//*[text() = 'Confirm']")
	WebElement confirmButton;
	
	public void setName(String name) {
		nameInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		nameInput.sendKeys(name);
		waitPageLoaded();
	}
	
	public void setChannel(String channel) {
		clickOn(channelInput);
		clickOn(driver.findElement(By.xpath("//*[text() = '" + channel + "']")));
	}
	
	public void setLanguage(String language) {
		clickOn(languageInput);
		clickOn(driver.findElement(By.xpath("//*[text() = '" + language + "']")));
	}
	
	public void setActive(Boolean active) {
		String class_name = activeInput.getAttribute("class");
		if((active.equals(true) && !class_name.contains("Mui-checked")) || (active.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(activeInput);
		}
		
	}
	
	public void setObject(String object) {
		objectInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		objectInput.sendKeys(object);
		waitPageLoaded();
	}
	
	public void setBody(String body) {
		bodyInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		bodyInput.sendKeys(body);
		waitPageLoaded();
	}
	
	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
	
	public void clickOnGoBackButton() {
		clickOn(goBackButton);
	}
	
	public void clickOnDeleteButton() {
		clickOn(deleteButton);
	}
	public void clickOnConfirmButton() {
		clickOn(confirmButton);
	}
	
}
