package com.opencellsoft.pages.customercare.dunning;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class TemplatesListPage extends TestBase {

	public TemplatesListPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//*[@title = 'Create' or @id='CREATE']")
	WebElement createButton;
	@FindBy(how = How.XPATH, using = "//input[@title = 'Name' or contains(@id , 'code')]")
	WebElement searchInput;
	@FindBy(how = How.XPATH, using = "//table[contains(@id, 'templates')]//tbody//*[@role = 'row' or contains(@class ,'MuiTableRow-root')]")
	List<WebElement> templatesList;
	@FindBy(how = How.XPATH, using = "//button[@title = 'Filter' or @aria-label = 'Filter']")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//div[@id = 'language']")
	WebElement languageInput;
	@FindBy(how = How.XPATH, using = "//div[@id = 'channel']")
	WebElement channelInput;
	
	public void clickOnCreateButton() {
		clickOn(createButton);
	}
	
	public void setSearchInput(String value) {
		searchInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		searchInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public List<WebElement> templatesList() {
		return templatesList;
	}
	
	public void goToTemplateDetails(String value) {
		clickOn(driver.findElement(By.xpath("//*[contains(text() , '" + value + "')]")));
	}
	
	public void clickOnFilterButton() {
		clickOn(filterButton);
	}
	
	public void activateFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//li//*[text()= '" + value + "']")));
	}
	
	public void clickOnLanguageInput() {
		clickOn(languageInput);
	}
	
	public void clickOnChannelInput() {
		clickOn(channelInput);
	}
	
	public void filterBy(String value) {
		clickOn(driver.findElement(By.xpath("//li[text()= '" + value + "']")));
	}
	
	public void clickOnRemoveFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//button[@data-key = '" + value + "']")));
	}
	
	public void sortBy(String value) {
		clickOn(driver.findElement(By.xpath("//span[contains(@class, 'MuiButtonBase-root') and contains(@data-field, '" + value + "')]")));
	}
	
	
}
