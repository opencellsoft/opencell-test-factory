package com.opencellsoft.pages.customercare.contracts;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.opencellsoft.base.TestBase;

public class ContractPage extends TestBase {
	
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public ContractPage(WebDriver driver) {
		this.driver= driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
	}

	// Using FindBy for locating elements
	@FindBy(how= How.XPATH, using= "//input[@id='description']")
	WebElement contractDescriptionInput;
	
	@FindBy(how= How.XPATH, using= "//input[@id='beginDate']")
	WebElement startDateInput;
	
	@FindBy(how= How.XPATH, using= "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day MuiPickersDay-current MuiPickersDay-daySelected']")
	WebElement startDateButton;
	
	@FindBy(how= How.XPATH, using= "//input[@id='endDate']")
	WebElement endDateInput;
	
	@FindBy(how= How.XPATH, using= "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day MuiPickersDay-current MuiPickersDay-daySelected']")
	WebElement endDateButton;

	@FindBy(how= How.XPATH, using= "//div[@id='contractAccountLevel']")
	WebElement contractAccountLevelInput;
	
	@FindBy(how= How.XPATH, using= "//li[normalize-space()='BILLING_ACCOUNT']")
	WebElement contractAccountLevelButtton;
	
	@FindBy(how= How.XPATH, using= "//input[@id='accountCode']")
	WebElement contractAttachedEntryInput;
	
	@FindBy(how= How.XPATH, using= "//button[@aria-label='Add filter']")
	WebElement filterButton;
	
	@FindBy(how= How.XPATH, using= "//li[@role='menuitem']//span[contains(text(),'Code')]")
	WebElement codeButton;
	
	@FindBy(how= How.XPATH, using= "//input[@id='wildcardOrIgnoreCase code']")
	WebElement codeLabel;
	
	@FindBy(how= How.XPATH, using= "//tbody/tr[1]/td[1]")
	WebElement contractAttachedEntryButtton;
	
	@FindBy(how= How.XPATH, using= "//input[@id='contractDate']")
	WebElement contractDateInput;
	
	@FindBy(how= How.XPATH, using= "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day MuiPickersDay-current MuiPickersDay-daySelected']")
	WebElement contractDateButton;
	
	@FindBy(how= How.XPATH, using= "//span[contains(text(),'Create')]")
	WebElement createContractLine;
	
	@FindBy(how= How.XPATH, using= "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;
	
//	@FindBy(how= How.XPATH, using= "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSizeSmall MuiButton-sizeSmall']")
	@FindBy(how= How.XPATH, using= "//*[contains(text(),'Activate')]")
	WebElement activateButton;
	@FindBy(how= How.XPATH, using= "//span[contains(text(), 'Actions')]")
	WebElement actionButton;
	@FindBy(how= How.XPATH, using= "//button//span[contains(text(),'Close')]")
	WebElement closeButton;
	@FindBy(how= How.XPATH, using= "//*[contains(text(),'Close')]")
	WebElement closeButton2;
	
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;
	
	@FindBy(how= How.XPATH, using= "//span[normalize-space()='Save']")
	WebElement saveButton;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Status')]/following-sibling::*//span[contains(text(),'Activated') or contains(text(),'ACTIVATED')]")
	List<WebElement> activatedStatus;
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Status')]/following-sibling::*//span[contains(text(),'Closed') or contains(text(),'CLOSED')]")
	WebElement closeedStatus;
	
	// This method to clear Contract Description Box Input Field
		public void clearContractDescriptionText(){
			contractDescriptionInput.clear();
			wait.until(ExpectedConditions.visibilityOf(contractDescriptionInput));
		}
		
		// This method to set text in Contract Description Box Input Field
		public void setContractDescriptionTextUpdate(String contractDescription){
			wait.until(ExpectedConditions.visibilityOf(contractDescriptionInput));
			contractDescriptionInput.sendKeys(contractDescription);
			waitPageLoaded();
		}
		
		// This method to click on contract start date box input
		public void clickOnContractStartDateUpdateInput(){
			try {wait.until(ExpectedConditions.visibilityOfAllElements(startDateInput));}catch (Exception e) {}
			clickOn(startDateInput);
		}
		
		// This method to click on contract start date 
		public void clickOnContractStartDateUpdateButton(){
			try {wait.until(ExpectedConditions.visibilityOfAllElements(startDateButton));}catch (Exception e) {}
			clickOn(startDateButton);
		}
		
		// This method to click on contract end date box input
		public void clickOnContractEndDateUnputeInput(){
			try {wait.until(ExpectedConditions.elementToBeClickable(endDateInput));}catch (Exception e) {}
			clickOn(endDateInput);
		}
		// This method to click on Contract End Date 
		public void clickOnContractEndDateUpdateButton() throws InterruptedException{
			try {wait.until(ExpectedConditions.visibilityOfAllElements(endDateButton));}catch (Exception e) {}
			clickOn(endDateButton);
		}
		
		// This method to click on Contract Account Level Label
		public void clickOnContractAccountLevelLabelUpdate() {
			try {wait.until(ExpectedConditions.visibilityOf(contractAccountLevelInput));}catch (Exception e) {}
			clickOn(contractAccountLevelInput);
		}

		// This method to click on Contract Account Level Button
		public void setContractAccountLevelButtonUpdate(){
			try {wait.until(ExpectedConditions.elementToBeClickable(contractAccountLevelButtton));}catch (Exception e) {}
			clickOn(contractAccountLevelButtton);
		}
	
		// This method to click on Contract Attached Entity Input
		public void clickOnContractAttachedEntityLabelUpdate() {
			try {wait.until(ExpectedConditions.elementToBeClickable(contractAttachedEntryInput));}catch (Exception e) {}
			clickOn(contractAttachedEntryInput);
		}
		// This method to click on filter button
		public void clickOnFilterButton() {
			try {wait.until(ExpectedConditions.elementToBeClickable(filterButton));}catch (Exception e) {}
			clickOn(filterButton);
		}
		
		//This method is to click on code button 
		public void clickOnFilterByCodeButton() throws InterruptedException{
			try {wait.until(ExpectedConditions.elementToBeClickable(codeButton));}catch (Exception e) {}
			clickOn(codeButton);
		}
		
		//This method is to set code in input box
		public void setCode(String customerCode) throws InterruptedException{
			clickOn(driver.findElement(By.xpath("//span[@title='" + customerCode + "']")));
		}
		
		
		// This method to click on Contract Date box input
		public void clickOnContractDateUpdateInput(){
			try {wait.until(ExpectedConditions.elementToBeClickable(contractDateInput));}catch (Exception e) {}
			clickOn(contractDateInput);
		}
		
		// This method to click on Contract Date button
		public void clickOnContractDateUpdateButton() throws InterruptedException{
			try {wait.until(ExpectedConditions.visibilityOfAllElements(contractDateButton));}catch (Exception e) {}
			clickOn(contractDateButton);
		}
		// This method to click on save button
		public void clickOnSaveContractButtonUpdate() {
			try {wait.until(ExpectedConditions.elementToBeClickable(saveButton));}catch (Exception e) {}
			clickOn(saveButton);
		}
		
		// This method to click on Go Back Contract
		public void clickOnGoBackContractButton() {
			try {wait.until(ExpectedConditions.visibilityOf(goBackButton));}catch (Exception e) {}
			clickOn(goBackButton);
		}
		
		// This method to click on create new line contract
		public void clickOnCreateContractLineButton() {
			try {wait.until(ExpectedConditions.elementToBeClickable(createContractLine));}catch (Exception e) {}
			clickOn(createContractLine);
		}
		
		// This method to click on activate contract
		public void clickOnAcivateContract() {
			wait.until(ExpectedConditions.elementToBeClickable(activateButton));
			clickOn(activateButton);
		}
		
		// This method to get text of element is created
		public String getTextMessage() {
			wait.until(ExpectedConditions.visibilityOfAllElements(messageTextBox));
			return messageTextBox.getText();
		}
		
		public int contractStatusIsActivated() {
			return activatedStatus.size();
		}
		
		public void clickOnActionButton() {
			clickOn(actionButton);
		}
		public void clickOnCloseContract() {
			clickOn(closeButton2);
		}
		
		public boolean contractStatusIsClosed() {
			return closeedStatus.isDisplayed();
		}
		
}

