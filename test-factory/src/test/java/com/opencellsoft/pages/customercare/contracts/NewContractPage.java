package com.opencellsoft.pages.customercare.contracts;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;
import com.opencellsoft.utility.Constant;

public class NewContractPage extends TestBase {

	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public NewContractPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		this.fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(20))
				.pollingEvery(Duration.ofSeconds(2)).ignoring(NoSuchElementException.class);
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement contractCodeInput;

	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement contractDescriptionInput;

	@FindBy(how = How.XPATH, using = "//input[@id='beginDate']")
	WebElement startDateInput;

	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day MuiPickersDay-current MuiPickersDay-daySelected']")
	WebElement startDateButton;

	@FindBy(how = How.XPATH, using = "//input[@id='endDate']")
	WebElement endDateInput;

	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day MuiPickersDay-current MuiPickersDay-daySelected']")
	WebElement endDateButton;

	@FindBy(how = How.XPATH, using = "//div[@id='contractAccountLevel']")
	WebElement contractAccountLevelInput;

	@FindBy(how = How.XPATH, using = "//li[normalize-space()='BILLING_ACCOUNT']")
	WebElement contractAccountLevelOption;

	@FindBy(how = How.XPATH, using = "//input[@id='accountCode']")
	WebElement contractAttachedEntityInput;

	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter' or @title= 'Filter']")
	WebElement filterButton;

	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Code') or contains(text(),'code')]")
	WebElement customerCodeButton;

	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement customerCodeInput;

	@FindBy(how = How.XPATH, using = "//input[@id='contractDate']")
	WebElement contractDateInput;

	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day MuiPickersDay-current MuiPickersDay-daySelected']")
	WebElement contractDateButton;

	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Save']")
	WebElement contractSaveButton;

	@FindBy(how = How.XPATH, using = "//div[@role='alert']")
	WebElement textMessage;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement contractGoBackButton;

	@FindBy(how = How.XPATH, using = "(//button//p[text()='1'])[1]")
	WebElement monthFirst;
	
	@FindBy(how = How.XPATH, using = "(//button//p[text()='2'])[1]")
	WebElement monthSecond;
	
	@FindBy(how = How.XPATH, using = "(//button[@class = 'MuiButtonBase-root MuiIconButton-root MuiPickersCalendarHeader-iconButton'])[1]")
	WebElement montheBefore;
	
	@FindBy(how = How.XPATH, using = "(//button[@class = 'MuiButtonBase-root MuiIconButton-root MuiPickersCalendarHeader-iconButton'])[2]")
	WebElement nexMonth;
	
	// This method to set Contract Code
	public void setContractCode(String codeContract) {
		try {wait.until(ExpectedConditions.visibilityOf(contractCodeInput));}catch (Exception e) {}
		contractCodeInput.sendKeys(codeContract);
	}

	// This method to set Contract Description
	public void setContractDescription(String descriptionContract) {
		try {wait.until(ExpectedConditions.visibilityOf(contractDescriptionInput));}catch (Exception e) {}
		contractDescriptionInput.sendKeys(descriptionContract);
	}

	// This method to click on contract start date box input
	public void clickOnContractStartDateInput() {
		try {wait.until(ExpectedConditions.visibilityOfAllElements(startDateInput));}catch (Exception e) {}
		startDateInput.click();
	}

	// This method to click on contract start date
	public void clickOnContractStartDateButton() throws InterruptedException {
		try {wait.until(ExpectedConditions.visibilityOfAllElements(startDateButton));}catch (Exception e) {}
		try {fWait.until(ExpectedConditions.elementToBeClickable(startDateButton));}catch (Exception e) {}
		clickOn(startDateButton);
	}

	// This method to click on contract end date box input
	public void clickOnContractEndDateInput() {
		try {wait.until(ExpectedConditions.visibilityOfAllElements(endDateInput));}catch (Exception e) {}
		clickOn(endDateInput);
	}

	// This method to click on Contract End Date
	public void clickOnContractEndDateButton() throws InterruptedException {
		try {wait.until(ExpectedConditions.visibilityOfAllElements(endDateButton));}catch (Exception e) {}
		clickOn(endDateButton);
	}

	// This method to click on Contract Account Level Box Input
	public void clickOnContractAccountLevelInput() {
		try { wait.until(ExpectedConditions.visibilityOfAllElements(contractAccountLevelInput));}catch (Exception e) {}
		clickOn(contractAccountLevelInput);
	}

	// This method to click on Contract Account Level
	public void selectContractAccountLevelOption() {
		try {
			clickOn(contractAccountLevelOption);
		}
		catch (Exception e) {
			wait.until(ExpectedConditions.elementToBeClickable(contractAccountLevelOption));
			contractAccountLevelOption.click();
		}
		
	}

	// This method to click on Contract Attached Entity Box Input
	public void clickOnContractAttachedEntityInput() {
		try {
			clickOn(contractAttachedEntityInput);
		}catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOfAllElements(contractAttachedEntityInput));
			contractAttachedEntityInput.click();
		}
		waitPageLoaded();
	}

	// This method to click on filter button
	public void clickOnFilterButton() {
		try {
			clickOn(filterButton);
		}catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOf(filterButton));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", filterButton);
			waitPageLoaded();
		}
	}

	// This method is to click on code button
	public void clickOnFilterByCodeButton() {
		try {
			clickOn(customerCodeButton);
		}catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOf(customerCodeButton));
			customerCodeButton.click();
		}
	}

	// This method is to set code in Box Input Field of filter
	public void setCodeFilter(String customerCode) {
		try {
			customerCodeInput.sendKeys(customerCode);
		}catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOfAllElements(customerCodeInput));
			customerCodeInput.sendKeys(customerCode);
		}
		waitPageLoaded();
	}

	// This method to click on contract attached Entity Code selected
	public void selectAttachedEntity(String customerCode) {
		try {
			clickOn(driver.findElement(By.xpath("//div[@role='dialog']//*[normalize-space()='" + customerCode + "']")));
		}catch (Exception e) {
			clickOn(driver.findElement(By.xpath("(//div[@role='dialog']//tbody//td)[1]")));
		}
	}

	// This method to click on Contract Date box input
	public void clickOnContractDateInput() {
		try {
			contractDateInput.click();
		}catch (Exception e) {
			fWait.until(ExpectedConditions.elementToBeClickable(contractDateInput));
			contractDateInput.click();
		}
	}

	// This method to click on Contract Date button
	public void clickOnContractDateButton() {
		try {
			contractDateButton.click();
		}catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOfAllElements(contractDateButton));
			contractDateButton.click();
			
		}
		waitPageLoaded();
	}

	// This method to click on Save Contract
	public void clickOnSaveContractButton() {
		try {
			contractSaveButton.click();
		}catch (Exception e) {
			wait.until(ExpectedConditions.elementToBeClickable(contractSaveButton));
			contractSaveButton.click();
		}
		
	}

	// This method to get text of element is created
	public String getTextCreateContractMessage() {
		wait.until(ExpectedConditions.visibilityOfAllElements(textMessage));
		return textMessage.getText();
	}

	public void clickOnGoBackButton() {
		// TODO Auto-generated method stub
		wait.until(ExpectedConditions.elementToBeClickable(contractGoBackButton));
		contractGoBackButton.click();
	}
	
	public void selectStartDateInPast() {
		startDateInput.click();
		waitPageLoaded();
		montheBefore.click();
		waitPageLoaded();
		montheBefore.click();
		waitPageLoaded();
		monthFirst.click();
		waitPageLoaded();
	}
	public void selectEndDateInPast() {
		endDateInput.click();
		waitPageLoaded();
		montheBefore.click();
		waitPageLoaded();
		monthFirst.click();
		waitPageLoaded();
	}
	public void selectStartDateInPresent() {
		startDateInput.click();
		waitPageLoaded();
		monthFirst.click();
		waitPageLoaded();
	}
	
	public void selectEndDateInPresent() {
		endDateInput.click();
		waitPageLoaded();
		nexMonth.click();
		waitPageLoaded();
		monthSecond.click();
		waitPageLoaded();
	}
	
	public void selectStartDateInFutur() {
		startDateInput.click();
		waitPageLoaded();
		nexMonth.click();
		waitPageLoaded();
		monthFirst.click();
		waitPageLoaded();
	}

	
	public void selectEndDateInFutur() {
		endDateInput.click();
		waitPageLoaded();
		nexMonth.click();
		waitPageLoaded();
		nexMonth.click();
		waitPageLoaded();
		monthFirst.click();
		waitPageLoaded();
	}
	
	public void setAttachedEntity (String entity) {
		clickOnContractAttachedEntityInput();
		clickOnFilterButton();
		clickOnFilterByCodeButton();
		setCodeFilter(entity);
		selectAttachedEntity(entity);
		waitPageLoaded();
	}
	
	public void selectContractDate() {
		clickOnContractDateInput();
		waitPageLoaded();
		clickOnContractDateButton();
		waitPageLoaded();
	}
	public void selectContractAccountLevel() {
		clickOnContractAccountLevelInput();
		waitPageLoaded();
		selectContractAccountLevelOption();
		waitPageLoaded();
	}
}
