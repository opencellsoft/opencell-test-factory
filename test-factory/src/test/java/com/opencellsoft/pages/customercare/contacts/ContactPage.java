package com.opencellsoft.pages.customercare.contacts;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.opencellsoft.base.TestBase;

public class ContactPage extends TestBase {

	Actions actions ;
	public ContactPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(how = How.XPATH, using = "//*[@id = 'title']")
	WebElement titleInput;
	@FindBy(how = How.XPATH, using = "//li[@data-value = 'MR']")
	WebElement mr;
	@FindBy(how = How.XPATH, using = "//input[@id = 'name']")
	WebElement nameInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'surName']")
	WebElement surnameInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'phone' or contains(@type, 'phone')]")
	WebElement phoneInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'email']")
	WebElement emailInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'country']")
	WebElement countryInput;
	@FindBy(how = How.XPATH, using = "//div[@role='dialog']//input[@id = 'countryCode']")
	WebElement countryCodeInput;
	@FindBy(how = How.XPATH, using = "//div[@role='dialog']//table//tr//td[1]")
	WebElement firstChoice;
	@FindBy(how = How.XPATH, using = "//input[@id = 'city']")
	WebElement cityInput;
	@FindBy(how = How.XPATH, using = "//input[contains(@id, 'company')]")
	WebElement companyInput;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//input[@title = 'Name' or contains(@id , 'description')]")
	WebElement searchForCompanyNameInput;
	@FindBy(how = How.XPATH, using = "//input[contains(@id, 'position')]")
	WebElement positionInput;
	@FindBy(how = How.XPATH, using = "//div[@data-field='mainContact']//span[contains(@class, 'MuiIconButton-root') or contains(@class , 'MuiButtonBase-root')]")
	WebElement mainContactInput;
	@FindBy(how = How.XPATH, using = "//button[@id = 'EDIT_CONTACT']")
	WebElement updateButton;
	@FindBy(how = How.XPATH, using = "//button[@type = 'submit']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//span[text() = 'Go Back']")
	WebElement goBackButton;
	public void setTitle(String title) {
		clickOn(titleInput);
		clickOn(mr);
	}
	
	public void setName(String name) {
		nameInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		nameInput.sendKeys(name);
		waitPageLoaded();
	}
	
	public void setSurname(String surname) {
		surnameInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		surnameInput.sendKeys(surname);
		waitPageLoaded();
	}
	
	public void setPhone(String phone) {
		phoneInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		phoneInput.sendKeys(phone);
		waitPageLoaded();
	}
	
	public void setEmail(String email) {
		emailInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		emailInput.sendKeys(email);
		waitPageLoaded();
	}
	
	public void setCountry(String country) {
		clickOn(countryInput);
		countryCodeInput.sendKeys(country);
		waitPageLoaded();
		clickOn(firstChoice);
	}
	
	public void setCity(String city) {
		cityInput.sendKeys(city);
		waitPageLoaded();
	}
	
	public void setCompany(String company, String line) {
		actions = new Actions(driver);
		actions.doubleClick(driver.findElement(By.xpath("(//*[@data-field = 'company'])[" + line + "]"))).perform();
		waitPageLoaded();
		clickOn(companyInput);
		searchForCompanyNameInput.sendKeys(company);
		waitPageLoaded();
		clickOn(firstChoice);
	}
	
	public void setPosition(String position, String line) {
		actions = new Actions(driver);
		actions.doubleClick(driver.findElement(By.xpath("(//*[@data-field = 'position'])[" + line + "]"))).perform();
		waitPageLoaded();
		positionInput.sendKeys(position);
		waitPageLoaded();
		positionInput.sendKeys(Keys.RETURN);
	}
	
	public void setMainContact(Boolean mainContact, String line) {
		actions = new Actions(driver);
		actions.doubleClick(driver.findElement(By.xpath("(//*[@data-field = 'mainContact'])[" + line + "]"))).perform();
		waitPageLoaded();
		String class_name = mainContactInput.getAttribute("class");
		if((mainContact.equals(true) && !class_name.contains("Mui-checked")) || (mainContact.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(mainContactInput);
		}
	}
	
	public void clickOnUpdateButton() {
		clickOn(updateButton);
	}
	
	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
	
	public void clickOnGoBackButton() {
		clickOn(goBackButton);
	}
	
}
