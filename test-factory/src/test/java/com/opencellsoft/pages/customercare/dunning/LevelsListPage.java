package com.opencellsoft.pages.customercare.dunning;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class LevelsListPage extends TestBase {


	public LevelsListPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//*[@title = 'Create' or @id = 'CREATE']")
	WebElement createButton;
	@FindBy(how = How.XPATH, using = "//input[@title = 'Name' or @name = 'wildcardOrIgnoreCase code']")
	WebElement searchInput;
	@FindBy(how = How.XPATH, using = "//table[contains(@id, 'levels')]//tbody//*[@role = 'row' or contains(@class ,'MuiTableRow-root')]")
	List<WebElement> levelsList;
	@FindBy(how = How.XPATH, using = "//*[text() = 'No results found']")
	List<WebElement> noResultsFound;
	
	public void clickOnCreateButton() {
		clickOn(createButton);
	}
	
	public void setSearchInput(String value) {
		searchInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		searchInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public List<WebElement> levelsList() {
		return levelsList;
	}
	
	public List<WebElement> noResultsFound() {
		return noResultsFound;
	}
	
	public void goToLevelDetails(String value) {
		clickOn(driver.findElement(By.xpath("//*[contains(text() , '" + value + "')]")));
	}
	
	public void sortBy(String value) {
		clickOn(driver.findElement(By.xpath("//span[contains(@class, 'MuiButtonBase-root') and contains(@data-field, '" + value + "')]")));
	}
}
