package com.opencellsoft.pages.customercare.securitydeposits;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.opencellsoft.base.TestBase;

public class SecurityDepositPage extends TestBase {

	public SecurityDepositPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Status')]/following-sibling::*//span[contains(text(),'VALIDATED')]")
	List<WebElement> validatedStatus;
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Status')]/following-sibling::*//span[contains(text(),'LOCKED')]")
	List<WebElement> lockedStatus;
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Status')]/following-sibling::*//span[contains(text(),'REFUNDED')]")
	List<WebElement> refundedStatus;
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Status')]/following-sibling::*//span[contains(text(),'CANCELLED')]")
	List<WebElement> canceledStatus;
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Status')]/following-sibling::*//span[contains(text(),'UNLOCKED')]")
	List<WebElement> unlockedStatus;
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Status')]/following-sibling::*//span[contains(text(),'HOLD')]")
	List<WebElement> holdStatus;
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Link to this invoice')]/following-sibling::*//a//*//span")
	List<WebElement> invoiceLink;
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Current balance')]/following-sibling::div//span//span")
	WebElement currentBalance;
	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Refund')]/following-sibling::*//a//*//span")
	List<WebElement> adjustementLink;
	@FindBy(how = How.XPATH, using = "//form[@class = 'SecurityDeposit']//div[contains(text(),'Credit')]")
	WebElement creditButton;
	@FindBy(how = How.XPATH, using = "//button//span[contains(text(), 'Instantiate')]")
	WebElement InstantiateButton;
	@FindBy(how = How.XPATH, using = "//form[@class = 'SecurityDeposit']//div[contains(text(),'Refund')]")
	WebElement refundButton;
	@FindBy(how = How.XPATH, using = "//form[@class = 'SecurityDeposit']//div[contains(text(),'Cancel')]")
	WebElement cancelButton;
	@FindBy(how = How.ID, using = "check_amount")
	WebElement checkAmount;
	@FindBy(how = How.ID, using = "check_reference")
	WebElement checkReference;
	@FindBy(how = How.ID, using = "refundReason")
	WebElement refundReason;
	@FindBy(how = How.ID, using = "cancelReason")
	WebElement cancelReason;
	@FindBy(how = How.XPATH, using = "//button//span[contains(text(),'Confirm')]")
	WebElement confirmButton;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;
	
	public boolean SecurityDepositStatusValidated() {
		if(validatedStatus.size()>0) { return true; } else { return false; }
	}
	
	public boolean SecurityDepositStatusLocked() {
		if(lockedStatus.size()>0) { return true; } else { return false; }
	}
	
	public boolean SecurityDepositStatusRefunded() {
		if(refundedStatus.size()>0) { return true; } else { return false; }
	}
	
	public boolean SecurityDepositStatusCanceled() {
		if(canceledStatus.size()>0) { return true; } else { return false; }
	}
	
	public boolean SecurityDepositStatusUnlocked() {
		if(unlockedStatus.size()>0) { return true; } else { return false; }
	}
	
	public boolean SecurityDepositStatusHold() {
		if(holdStatus.size()>0) { return true; } else { return false; }
	}
	
	public boolean invoiceTypeSDIsGenerated() {
		if(invoiceLink.size()>0) { return true; } else { return false; }
	}
	
	public boolean invoiceTypeAdjustementIsGenerated() {
		if(adjustementLink.size()>0) { return true; } else { return false; }
	}
	
	public void clickOnInvoiceLink() {
		this.invoiceLink.get(0).click();
	}
	
	public void clickOnAdjustementLink() {
		this.adjustementLink.get(0).click();
	}
	
	public String getCurrentBalance() {
		return currentBalance.getText();
	}
	
	public void clickOnCreditButton() {
		this.creditButton.click();
	}
	public void clickOnInstantiateButton() {
		this.InstantiateButton.click();
	}
	public void clickOnRefundButton() {
		this.refundButton.click();
	}

	public void clickOnCancelButton() {
		this.cancelButton.click();
	}
	
	public void setCheckAmount(String checkAmount) {
		this.checkAmount.sendKeys(checkAmount);
	}
	
	public void setCheckReference(String checkReference) {
		this.checkReference.sendKeys(checkReference);
	}
	
	public void setRefundReason(String refundReason) {
		this.refundReason.sendKeys(refundReason);
	}
	
	public void setCancelReason(String cancelReason) {
		this.cancelReason.sendKeys(cancelReason);
	}
	
	public void clickOnConfirmButton() {
		this.confirmButton.click();
	}
	
	public String getSDName () {
		return driver.findElement(By.xpath("//label[@id='code-label']/following-sibling::div//input[@id = 'code']")).getAttribute("value");
	}
	// method to get the appeared message after operation
	public String getTextMessage() {
		return messageTextBox.getText();

	}
}
