package com.opencellsoft.pages.customercare.contracts;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class NewContractLinePage extends TestBase {
	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public NewContractLinePage(WebDriver driver) {

		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		this.fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(20))
				.pollingEvery(Duration.ofSeconds(2)).ignoring(NoSuchElementException.class);
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement contractLineCodeInput;

	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement contractLineDescriptionInput;

	@FindBy(how = How.XPATH, using = "//input[@id='offer']")
	WebElement offerInput;

	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter' or @title = 'Filter']")
	WebElement offerFilterButton;

	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Code')]")
	WebElement offerCodeButton;

	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement offerCodeInput;

	@FindBy(how = How.XPATH, using = "//input[@id='product']")
	WebElement productInput;

	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter' or @title = 'Filter']")
	WebElement productFilterButton;

	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Code') or contains(text(),'code')]")
	WebElement productCodeButton;

	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement productCodeLabel;

	@FindBy(how = How.XPATH, using = "//input[@id='charge']")
	WebElement chargeInput;

	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter' or @title = 'Filter']")
	WebElement chargeFilterButton;

	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Name')]")
	WebElement chargeNameButton;

	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement chargeNameInput;

	@FindBy(how = How.XPATH, using = "//input[@id='discountType_discount']")
	WebElement discountRateRadio;

	@FindBy(how = How.XPATH, using = "//input[@id='discountType_surcharge']")
	WebElement customContractPriceRadio;

	@FindBy(how = How.XPATH, using = "//input[@id='priceGrid']")
	WebElement priceGridToggle;

	@FindBy(how = How.XPATH, using = "//input[@id='discount' or @id = 'DISCOUNT']")
	WebElement discountRateInput;		

	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Save']")
	WebElement saveButton;

	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;	

	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageUpdateTextBox;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;		

	@FindBy(how = How.XPATH, using = "//input[@id='ppDuplicateCharge']")
	WebElement duplicateChargeInput;		

	@FindBy(how = How.XPATH, using = "//input[@id='ppDuplicatePricePlan']")
	WebElement duplicatePricePlanInput;		

	@FindBy(how = How.XPATH, using = "(//input[@type='checkbox'])[3]")
	WebElement checkBox;

	@FindBy(how = How.XPATH, using = "//input[@id='surcharge' or @id = 'SURCHARGE']")
	WebElement priceInput;		

	@FindBy(how = How.XPATH, using = "//div[@class='MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedPrimary MuiButton-containedSizeSmall MuiButton-sizeSmall']")
	WebElement duplicateButton;		

	@FindBy(how = How.XPATH, using = "//div[@id='contractLineType']")
	WebElement contractLineTypeInput;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Create price version')]")
	WebElement createPriceVersionButton;

	@FindBy(how = How.XPATH, using = "(//tbody//td)[2]")
	WebElement priceVersionLine;

	@FindBy(how = How.XPATH, using = "//nav[@aria-label = 'breadcrumb']//ol//li//a")
	WebElement contractCode;

	@FindBy(how = How.ID, using = "priceVersionToDuplicate")
	WebElement priceVersionToDuplicateInput;

	@FindBy(how = How.ID, using = "seperateDiscountLine")
	WebElement separateDiscountLine;

	// This method to set Contract Code
	public void setContractLineCode(String codeContract) {
		wait.until(ExpectedConditions.visibilityOf(contractLineCodeInput));
		contractLineCodeInput.sendKeys(codeContract);
	}

	// This method to set Contract Description
	public void setContractLineDescription(String descriptionContract) {
		wait.until(ExpectedConditions.visibilityOf(contractLineDescriptionInput));
		contractLineDescriptionInput.sendKeys(descriptionContract);
	}

	// This method to click on Offer Box Input
	public void clickOnOfferInput() {
		wait.until(ExpectedConditions.visibilityOfAllElements(offerInput));
		offerInput.click();
	}

	// This method to click on filter button
	public void clickOnofferFilterButton() {
		try {
			clickOn(offerFilterButton);
		}catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOf(offerFilterButton));
			offerFilterButton.click();
		}

	}

	// This method is to click on code button
	public void clickOnFilterBy0fferCodeButton() {
		try {
			wait.until(ExpectedConditions.visibilityOf(offerCodeButton));
			offerCodeButton.click();
		}catch (Exception e) {
			clickOn(offerFilterButton);
			clickOn(offerCodeButton);			
		}
	}

	// This method is to set code in Box Input Field of filter
	public void setOfferCodeFilter(String offerCode) {
		try {
			wait.until(ExpectedConditions.visibilityOfAllElements(offerCodeInput));
			offerCodeInput.sendKeys(Keys.CONTROL + "a");
			offerCodeInput.sendKeys(offerCode);
		}catch (Exception e) {
			offerCodeInput.sendKeys(Keys.CONTROL + "a");
			offerCodeInput.sendKeys(offerCode);
		}
	}

	// This method to click on offer code
	public void selectOffer(String offerCode) {
		try {
		driver.findElement(By.xpath("//span//mark[contains(text(),'" + offerCode + "')]")).click();
		}catch (Exception e) {
			try {
				driver.findElement(By.xpath("(//tbody//td)[1]")).click();
			}catch (Exception ex) {
				setOfferCodeFilter(offerCode);
				driver.findElement(By.xpath("(//tbody//td)[1]")).click();
			}
			
		}
	}

	// This method to click on product Box Input
	public void clickOnProductInput() {
		try {
			wait.until(ExpectedConditions.visibilityOfAllElements(productInput));
			productInput.click();
		}catch (Exception e) {
			clickOn(productInput);
		}
		
	}

	// This method to click on filter button
	public void clickOnProductFilterButton() {
		wait.until(ExpectedConditions.visibilityOf(productFilterButton));
		productFilterButton.click();
	}

	// This method is to click on code button
	public void clickOnFilterByProductCodeButton() {
		wait.until(ExpectedConditions.visibilityOf(productCodeButton));
		productCodeButton.click();
	}

	// This method is to set code in Box Input Field of filter
	public void setProductCodeFilter(String productCode) {
		wait.until(ExpectedConditions.visibilityOfAllElements(productCodeLabel));
		productCodeLabel.sendKeys(productCode);
	}

	// This method to click on product code
	public void selectProduct(String productCode) {
		try {
			driver.findElement(By.xpath("//span//mark[contains(text(),'" + productCode + "')]")).click();
		}catch (Exception e) {
			clickOn( driver.findElement(By.xpath("//tbody")));
		}
	}

	// This method to click on charge Box Input
	public void clickOnChargeInput() {
		clickOn(chargeInput);
	}

	// This method to click on filter button
	public void clickOnChargeFilterButton() {
		clickOn(chargeFilterButton);
	}

	// This method is to click on code button
	public void clickOnFilterByChargeNameButton() {
		clickOn(chargeNameButton);
	}

	// This method is to set code in Box Input Field of filter
	public void setChargeNameFilter(String chargeName) {
		wait.until(ExpectedConditions.visibilityOfAllElements(chargeNameInput));
		chargeNameInput.sendKeys(Keys.CONTROL + "a");
		chargeNameInput.sendKeys(chargeName);
	}

	// This method to click on offer code
	public void selectCharge(String chargeName) {
		try {
			clickOn(driver.findElement(By.xpath("//span//mark[contains(text(),'" + chargeName + "')]")));
		}catch (Exception e) {
			setChargeNameFilter(chargeName);
			waitPageLoaded();
			clickOn( driver.findElement(By.xpath("//tbody")));
		}
	}

	// This method is to click on discount rate
	public void clickOnDiscout() {
		wait.until(ExpectedConditions.elementToBeClickable(discountRateRadio));
		discountRateRadio.click();
	}

	public void setDiscoutRate(String discountRate) {
		try {
			wait.until(ExpectedConditions.visibilityOf(discountRateInput));
			discountRateInput.sendKeys(discountRate);
		}catch (Exception e) {
			discountRateInput.sendKeys(discountRate);
		}
		
	}
	public void setSeparateDiscountLine(boolean separate) {
		if(separate == true) {
			separateDiscountLine.click();}
	}

	// This method is to click on Custom contract price
	public void selectCustomContractPrice() {
		customContractPriceRadio.click();
	}

	// This method is to click on Price Grid button
	public void clickOnPriceGridtoggle() {
		priceGridToggle.click();
	}
	public void setContractPrice(String contractPrice) {
		priceInput.sendKeys(contractPrice);
	}

	// This method is to click on save button
	public void clickOnSaveButton() {
		try {
		wait.until(ExpectedConditions.visibilityOfAllElements(saveButton));
		saveButton.click();
		}
		catch (Exception e) {
			clickOn(saveButton);
		}
	}

	// This method to get text of element is created
	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

	// This method to get text of element is created
	public String getTextUpdateMessage() {
		wait.until(ExpectedConditions.visibilityOf(messageUpdateTextBox));
		return messageUpdateTextBox.getText();
	}

	// This method is to click on go back button 
	public void clickOnGoBackButton() {
		//			wait.until(ExpectedConditions.elementToBeClickable(goBackButton));
		//			goBackButton.click();
		//			JavascriptExecutor js = (JavascriptExecutor) driver;
		//			js.executeScript("arguments[0].click();", goBackButton);
		WebElement link = driver.findElement(By.xpath("(//div[contains(@class,'PageTitle')]//a)[2]"));
		link.click();
	}

	public void clickOnDuplicateCharge() {
		// TODO Auto-generated method stub
		duplicateChargeInput.click();	
	}

	public void clickOnDuplicatePriePlan() {
		// TODO Auto-generated method stub
		duplicatePricePlanInput.click();
	}

	public void selectVisiblePricePlan(String priceplancode) {
		// TODO Auto-generated method stub
		driver.findElement(By.xpath("//span[contains(text(),'PPM_"+priceplancode+"')]")).click();
	}

	public void selectPriceVersionToDuplicate() {
		// TODO Auto-generated method stub
		checkBox.click();
	}

	public void clickOnDuplicateButton() {
		// TODO Auto-generated method stub
		duplicateButton.click();
	}

	public void selectContractLineType(String type) {
		try {
			clickOn(contractLineTypeInput);
		}catch (Exception e) {
			clickOn( driver.findElement(By.xpath("//tbody")));
			clickOn(contractLineTypeInput);
		}
		
		waitPageLoaded();
		driver.findElement(By.xpath("//li[text() = '"+ type +"']")).click();
		waitPageLoaded();
	}

	public void setOffer(String offer) {
		clickOnOfferInput();
		waitPageLoaded();
		clickOnofferFilterButton();
		waitPageLoaded();
		clickOnFilterBy0fferCodeButton();
		waitPageLoaded();
		setOfferCodeFilter(offer);
		waitPageLoaded();
		selectOffer(offer);
		waitPageLoaded();
	}
	public void setProduct(String product) {
		clickOnProductInput();
		waitPageLoaded();
		clickOnProductFilterButton();
		waitPageLoaded();
		clickOnFilterByProductCodeButton();
		waitPageLoaded();
		setProductCodeFilter(product);
		waitPageLoaded();
		selectProduct(product);
		waitPageLoaded();
	}
	public void setCharge(String charge) {
		clickOnChargeInput();
		clickOnChargeFilterButton();
		clickOnFilterByChargeNameButton();
		waitPageLoaded();
		setChargeNameFilter(charge);
		waitPageLoaded();
		selectCharge(charge);
		waitPageLoaded();
	}

	public void clickOnCreatePriceVersionButton() {
		clickOn(createPriceVersionButton);
	}

	public void clickOnpriceVersionToDuplicateInput() {
		clickOn(priceVersionToDuplicateInput);
	}

	public void clickOnPriceVersionLine() {
		clickOn(priceVersionLine);
	}
	public void goToContract() {
		clickOn(contractCode);
	}

}
