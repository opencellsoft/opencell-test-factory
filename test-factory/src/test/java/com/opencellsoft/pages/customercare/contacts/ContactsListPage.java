package com.opencellsoft.pages.customercare.contacts;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class ContactsListPage extends TestBase {

	public ContactsListPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//*[@title = 'Create' or @id ='CREATE']")
	WebElement createButton;
	@FindBy(how = How.XPATH, using = "//input[@id = 'searchBar']")
	WebElement searchInput;
	@FindBy(how = How.XPATH, using = "//table[contains(@id, 'contacts')]//tbody//*[@role = 'row' or contains(@class ,'MuiTableRow-root')]")
	List<WebElement> contactList;
	
	public void clickOnCreateButton() {
		clickOn(createButton);
	}
	
	public void setSearchInput(String value) {
		searchInput.sendKeys(Keys.CONTROL + "a", Keys.BACK_SPACE);
		searchInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public List<WebElement> contactList() {
		return contactList;
	}
	
	public void sortBy(String value) {
		clickOn(driver.findElement(By.xpath("//span[contains(@class, 'MuiButtonBase-root') and contains(@data-field, '" + value + "')]")));
	}
	
	public void goToContactDetails(String value) {
		clickOn(driver.findElement(By.xpath("//*[contains(text() , '" + value + "')]")));
	}
	
}
