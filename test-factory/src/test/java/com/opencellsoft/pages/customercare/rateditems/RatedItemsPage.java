package com.opencellsoft.pages.customercare.rateditems;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class RatedItemsPage extends TestBase {

	public RatedItemsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//div[@resource = 'B2B-customer-care/rated-transactions' or @listid = 'rated-transactions_CLIENT_LIST']//input[@id = 'searchBar']")
	WebElement searchBarInput;
	
	public void setSearchValue(String value) {
		searchBarInput.sendKeys(Keys.chord(Keys.CONTROL, "a"),value);
	}
	
	// return the number of lines found
	public int elementsFoundSize(String value) {
		List<WebElement> result = driver.findElements(By.xpath("//td[normalize-space()='"+ value + "']"));
		return result.size();
	}
	
}
