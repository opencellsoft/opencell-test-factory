package com.opencellsoft.pages.productfamilies;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

public class NewProductFamiliesPage {

	WebDriver driver;

	public NewProductFamiliesPage(WebDriver driver) {
		this.driver = driver;
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-textSizeSmall MuiButton-sizeSmall']")
	WebElement createProductFamilies;
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement productFamilyCode;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement productFamilyLabel;
	@FindBy(how = How.XPATH, using = "//textarea[@id='longDescription']")
	WebElement productFamilyDescription;
	@FindBy(how = How.ID, using = "seller.code")
	WebElement sellersList;
	@FindBy(how = How.XPATH, using = "//div[@id='parentLine.code']")
	WebElement parentProductFamilyList;
	@FindBy(how = How.CSS, using = "li:nth-child(6)")
	WebElement parentProductFamilySelected;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Save')]")
	WebElement submitButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;	
	
	// This method is to click on create product families
	public void clickOnCreateProductFamilies() {
		createProductFamilies.click();
	}
	
	// This method is to set product family code
	public void setProductFamilyCode(String setProductFamiliesCode) {
		productFamilyCode.sendKeys(setProductFamiliesCode);
	}
	
	// This method is to set product family Label
	public void setProductFamilyLabel(String setProductFamiliesLabel) {
		productFamilyLabel.sendKeys(setProductFamiliesLabel);
	}

	// This method is to set product family description
	public void setProductFamilyDescription(String setProductFamiliesDescription) {
		productFamilyDescription.sendKeys(setProductFamiliesDescription);
	}
	
	// This method is to select seller
	public void selectSeller(String selectSeller) {
		sellersList.click();// assuming you have to click the "dropdown" to open it
		sellersList.findElement(By.xpath("//li[contains(.,'"+selectSeller+"')]")).click();	
	}

	// This method is to select parent product family
	public void selectParentProductFamily(String selectParentProductFamily) {
		parentProductFamilyList.click();// assuming you have to click the "dropdown" to open it
		parentProductFamilyList.findElement(By.xpath("//li[contains(.,'"+selectParentProductFamily+"')]")).click();	
	}

	// This method is to click on submit button
	public void clickOnSubmitButton() {
		submitButton.click() ;
	}

	// This method is to click on go back button
	public void clickOnGoBackButton() {
		goBackButton.click();
	}
}
