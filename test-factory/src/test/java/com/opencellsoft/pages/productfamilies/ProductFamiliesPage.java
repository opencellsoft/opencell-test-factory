package com.opencellsoft.pages.productfamilies;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ProductFamiliesPage {

	WebDriver driver;

	public ProductFamiliesPage (WebDriver driver) {
		this.driver = driver ;
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Delete')]")
	WebElement delete;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Confirm')]")
	WebElement confirmDelete;

	// This method is to click on delete button
	public void clickOnDelete() {
		delete.click();
	}

	// This method is to click on confirm delete
	public void clickOnConfirmDelete() {
		confirmDelete.click();
	}
}
