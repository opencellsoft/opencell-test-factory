package com.opencellsoft.pages.productfamilies;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ListOfProductFamiliesPage {

	WebDriver driver;

	public ListOfProductFamiliesPage(WebDriver driver) {
		this.driver = driver;
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addFilter;
	@FindBy(how = How.CSS, using = "li:nth-child(1) span:nth-child(1)")
	WebElement addFilterCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement productFamilyCode;
	@FindBy(how = How.XPATH, using = "//input[@id='longDescription']")
	WebElement productFamilyDescription;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase description']")
	WebElement productFamilyName;
	@FindBy(how = How.CSS, using = "li:nth-child(1) span:nth-child(1)")
	WebElement addFilterDescription;
	@FindBy(how = How.CSS, using = "body > div:nth-child(8) > div:nth-child(3) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(2)")
	WebElement productFamilySelected;
	@FindBy(how = How.CSS, using = "li:nth-child(1) span:nth-child(1)")
	WebElement addFilterParentProduct;
	@FindBy(how = How.XPATH, using = "//div[@id='parentLine.id']")
	WebElement parentProductFamilyList;
	@FindBy(how = How.XPATH, using = "li:nth-child(14)")
	WebElement parentProductFamilySelected;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Last']")
	WebElement listLatestProduct;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Cancel')]")
	WebElement cancel;
	
	// This method is to click on add filter
	public void clickOnAddFilter() {
		addFilter.click();
	}
	
	// This method is to click on add code filter
	public void clickOnAddCodeFilter() {
		addFilterCode.click();
	}

	// This method is to click on description filter
	public void clickOnAddDescriptionFilter() {
		addFilterDescription.click();
	}

	// This method is to click on parent product filter
	public void clickOnAddParentProductFilter() {
		addFilterParentProduct.click();
	}

	// This method is to set product family code
	public void setProductFamilyCode(String setProductFamilyCode) {
		productFamilyCode.sendKeys(setProductFamilyCode);
	}

	// This method is to set product family description
	public void setProductFamilyDescription(String setProductFamilyDescription) {
		productFamilyDescription.sendKeys(setProductFamilyDescription);
	}

	// This method is to set product family name
	public void setProductFamilyName(String setProductFamilyName) {
		productFamilyName.sendKeys(setProductFamilyName);
	}

	// This method is to select parent product family
	public void selectParentProductFamily(String selectParentProductFamily) {
		parentProductFamilyList.click();// assuming you have to click the "dropdown" to open it
		parentProductFamilyList.findElement(By.xpath("//li[contains(.,'"+selectParentProductFamily+"')]")).click();
	}

	// This method is to select product family
	public void clickOnProductFamilySelected() {
		productFamilySelected.click();
	}

	// This method is to click on list of latest product
	public void clickOnListOfLatestProduct() {
		listLatestProduct.click();
	}

	// This method is to click on cancel button
	public void clickOnCancel() {
		cancel.click();
	}
}