package com.opencellsoft.pages.commons;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
	WebDriver driver;
	WebDriverWait wait;
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//input[@id='username']")
	WebElement loginTextBox;
	@FindBy(how = How.XPATH, using = "//input[@id='password']")
	WebElement passwordTextBox;
	@FindBy(how = How.XPATH, using = "//input[@id='kc-login']")
	WebElement signinButton;
	@FindBy(how = How.XPATH, using = "//*[@title = 'FR']")
	List<WebElement> frButtons;
	@FindBy(how = How.XPATH, using = "//*[@title = 'FR']")
	WebElement frButton;
	@FindBy(how = How.XPATH, using = "//*[@title = 'EN']")
	WebElement enButton;
	
	// Defining all the user actions (Methods) that can be performed in the
	// Login home page

	// This method is to set Email in the email text box
	public void setLogin(String strLogin) {
		// explicit wait - to wait for the element to be visible
		wait.until(ExpectedConditions.visibilityOf(loginTextBox));
		// sendKeys as soon as the element is visible		
		loginTextBox.sendKeys(strLogin);
	}

	// This method is to set Password in the password text box
	public void setPassword(String strPassword) {
		// explicit wait - to wait for the element to be visible
		wait.until(ExpectedConditions.visibilityOf(passwordTextBox));
		// sendKeys as soon as the element is visible			
		passwordTextBox.sendKeys(strPassword);
	}

	// This method is to click on Login Button
	public void clickOnLoginButton() {
		// explicit wait - to wait for the button to be click-able
		wait.until(ExpectedConditions.elementToBeClickable(signinButton));
		// click on the compose button as soon as the "passwordTextBox" button is visible			
		signinButton.click();
	}
	
	public void login(String login, String password) {
		driver.navigate().refresh();
		this.setLogin(login);
		this.setPassword(password);
		this.clickOnLoginButton();
	}
	
	public void clickOnFRButton() {
		frButton.click();
	}

	public List<WebElement> getFrButton() {
		return frButtons;
	}
	public void clickOnENButton() {
		enButton.click();
	}
	
}
