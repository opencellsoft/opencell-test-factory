package com.opencellsoft.pages.commons;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class HomePage extends TestBase {

	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;
	Actions actions;

	public HomePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(20))
				.pollingEvery(Duration.ofSeconds(2)).ignoring(NoSuchElementException.class);
		actions = new Actions(driver);
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//div[@class='jss27']//div//button[@type='button']")
	WebElement profileDropdown;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Logout')]")
	WebElement logoutLink;
	@FindBy(how = How.XPATH, using = "//p[@class='MuiTypography-root jss43 jss44 MuiTypography-body1']")
	WebElement loggedInUserNameText;
	@FindBy(how = How.XPATH, using = "//*[contains(@title, 'Catalog') or contains(@href, 'catalog') or contains(@href, 'CPQ')]")
	WebElement catalogButtonMenu;
	@FindBy(how = How.XPATH, using = "(//*[contains(@title, 'Catalog') or @aria-label = 'Catalog'])[1]")
	WebElement catalogButtonMenu2;
	@FindBy(how = How.XPATH, using = "(//*[contains(@title, 'Products') or @aria-label = 'Products' or text() = 'Products'])[1]")
	WebElement productsButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Catalog manager')]")
	WebElement catalogManagerButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Price versions')]")
	WebElement priceVersionsButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Export')]")
	WebElement exportButton;
	@FindBy(how = How.XPATH, using = "//a//span[text()= 'Import']")
	WebElement importButton;
	@FindBy(how = How.XPATH, using = "//a//span[text()= 'Business attributes']")
	WebElement businessAttributesButton;
	@FindBy(how = How.XPATH, using = "//a//span[text()= 'Attribute groups']")
	WebElement attributeGroupsButton;
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Catalog')]")
	WebElement catalogButton;
	@FindBy(how = How.XPATH, using = "//a[contains(@href,'offers')]")
	WebElement offerButton;
	@FindBy(how = How.XPATH, using = "//a[@title='General settings' or @aria-label='General settings' or contains(@href, 'settings')]")
	WebElement generalSettingsButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Discount plans']")
	WebElement discountPlanButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Media']")
	WebElement mediaButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Product families']")
	WebElement productFamiliesButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Tags']")
	WebElement tagsButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Commercial offers']")
	WebElement commercialOffersButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Tag categories')]")
	WebElement tagCategoriesButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Report extracts')]")
	WebElement reportExtractsButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Report extract history')]")
	WebElement reportExtractsHistoryButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Query tool')]")
	WebElement queryToolsButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Query builder')]")
	WebElement queryBuilderButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Query results')]")
	WebElement queryResultsButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Charges')]")
	WebElement chargesTab;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Product families')]")
	WebElement productFamilies;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Quotes')]")
	WebElement quotesTab;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Invoices')]")
	WebElement invoicesButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Orders']")
	WebElement ordersTab;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Orders')]")
	WebElement ordersButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Subscriptions']")
	WebElement subscriptionTab;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbar-root MuiSnackbar-anchorOriginTopRight']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//a[@title='Customer Care' or @aria-label = 'Customer Care' or contains(@href , 'B2B')]")
	WebElement customerCareButton;
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Customer Care')]//a")
	WebElement customerCareButton2;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Discount plans')]")
	WebElement discountPlansButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Articles']")
	WebElement articleButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Billing']")
	WebElement billingButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Invoicing plans']")
	WebElement invoicingPlansButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Attributes']")
	WebElement attributesMenu;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Customers']")
	WebElement customersButton;
	@FindBy(how= How.XPATH, using= "//span[normalize-space()='Contracts']")
	WebElement contractsButton;
	@FindBy(how= How.XPATH, using= "//*[@title='Operations' or @title = 'Opérations' or @aria-label='Operations']")
	WebElement operationButton;
	@FindBy(how= How.XPATH, using = "//a[@href='/opencell/frontend/DEMO/portal/operation']")
	WebElement operationButtonMenu;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()= 'Wallet operations']")
	WebElement walletOperationsbutton;
	@FindBy(how= How.XPATH, using= "//span[normalize-space()='Billing run']")
	WebElement billingRunButton;
	@FindBy(how= How.XPATH, using= "//a[@title='Marketing Manager']")
	WebElement marketingManagerButton;
	@FindBy(how= How.XPATH, using = "//button[@aria-label='open drawer']")
	WebElement goBackButton;
	@FindBy(how= How.XPATH, using = "//span[normalize-space()='Finance settings']")
	WebElement financeSettings;
	@FindBy(how= How.XPATH, using = "//*[@title='Finance' or @aria-label = 'Finance']")
	WebElement financeButton;
	@FindBy(how= How.XPATH, using = "//div[contains(@class, 'MenuItemsContainer')]//*[contains(text(), 'Manual matching')]")
	WebElement manualMatchingButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Reports']")
	WebElement reportsButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Trial balance']")
	WebElement trialBalanceButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='General ledger']")
	WebElement generalLedgerButton;
	@FindBy(how= How.XPATH, using = "//span[normalize-space()='Security deposit']")
	WebElement securityDepositButton;
	@FindBy(how= How.XPATH, using = "//span[normalize-space()='General settings']")
	WebElement generalSettings;
	@FindBy(how= How.XPATH, using = "//a[@href='/opencell/frontend/DEMO/portal/settings']")
	WebElement generalSettingsMenu;
	@FindBy(how= How.XPATH, using = "//span[normalize-space()='Application settings']")
	WebElement applicationSettings;
	@FindBy(how= How.XPATH, using = "//span[normalize-space()='EDR']")
	WebElement EDR;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Mediation')]")
	WebElement mediationButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'CDRs')]")
	WebElement CDRButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'EDR settings')]")
	WebElement EdrSettingsButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='EDRs']")
	WebElement edrsButton;
	@FindBy(how = How.XPATH, using = "//button[@title='Refresh']//span[@class = 'MuiIconButton-label']")
	WebElement refreshButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Products')]")
	WebElement productButton;
	@FindBy(how = How.XPATH, using = "//a[@href = '/opencell/frontend/DEMO/portal/']")
	WebElement homeButton;
	@FindBy(how = How.XPATH, using = "//button[contains(@aria-label, 'open drawer')]")
	WebElement OpenDrawerButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Framework agreements')]")
	WebElement frameworkAgreementButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Catalog manager')]")
	WebElement catalogueManagerButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Reference tables')]")
	WebElement referenceTablesButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Custom tables')]")
	WebElement customTablesButton;
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'MenuItemsContainer')]//span[contains(text(), 'Accounting period')]")
	WebElement accountingPeriodButton;
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'MenuItemsContainer')]//span[contains(text(), 'Accounting period manager')]")
	WebElement accountingPeriodManagerButton;
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'MenuItemsContainer')]//span[contains(text(), 'Accounting operations report')]")
	WebElement accountingPeriodReportButton;
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'MenuItemsContainer')]//span[contains(text(), 'Accounting entries')]")
	WebElement accountingEntriesButton;
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'MenuItemsContainer')]//span[contains(text(), 'Aged trial balance')]")
	WebElement agedTrialBalanceButton;
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'MenuItemsContainer')]//span[contains(text(), 'Billing rules')]")
	WebElement billingRulesButton;
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'MenuItemsContainer')]//span[contains(text(), 'Billing cycles')]")
	WebElement billingCyclesButton;
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'MenuItemsContainer')]//span[contains(text(), 'Rated items')]")
	WebElement ratedItemsButton;
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'MenuItemsContainer')]//span[contains(text(), 'Rating')]")
	WebElement ratingButton;
	@FindBy(how = How.XPATH, using = "//SPAN[@class='menuLabel'][text()='Mass adjustments']")
	WebElement massAdjustmentsButton;
	@FindBy(how = How.XPATH, using = "//SPAN[@class='menuLabel'][text()='Rating']")
	WebElement ratingMenuButton;
	@FindBy(how = How.XPATH, using = "//SPAN[@class='menuLabel'][text()='Wallet operations']")
	WebElement walletOperationsMenuButton;
	@FindBy(how = How.XPATH, using = "//SPAN[@class='menuLabel'][text()='Rated items']")
	WebElement ratedItemsMenuButton;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Invoice validation')]")
	WebElement invoiceValidationButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Contacts')]")
	WebElement contactsButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Collection plans')]")
	WebElement collectionPlansButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Dunning')]")
	WebElement dunningsButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Templates')]")
	WebElement templatesButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Actions')]")
	WebElement actionsButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Levels')]")
	WebElement levelsButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Policies')]")
	WebElement policiesButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Settings')]")
	WebElement dunningSettingButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(), 'Price lists')]")
	WebElement priceListsButtonButton;
	@FindBy(how = How.XPATH, using = "//button[@aria-label = 'Profile']")
	WebElement profileButton;
	@FindBy(how = How.XPATH, using = "//ul//*[@title = 'Logout' or @aria-label = 'Logout']")
	WebElement logoutButton;
	// this method is to get the appeared message after operation
	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

	// This method to click on Catalog Button
	public void clickOnCatalogButton() {
		wait.until(ExpectedConditions.elementToBeClickable(catalogButton));
		catalogButton.click();			
	}

	public void clickOnCatalogButtonMenu() {
		wait.until(ExpectedConditions.elementToBeClickable(catalogButtonMenu));
		catalogButtonMenu.click();
	}

	public void clickOnCatalogButtonMenu2() {
		try {
			clickOn(catalogButtonMenu);
		}
		catch (Exception e) {
			clickOn(catalogButtonMenu2);
		}
	}
	
	public void clickOnCatalogButtonMenu22() {
		clickOn(catalogButtonMenu2);
	}

	public void clickOnProductsButton() {
		clickOn(productsButton);
	}
	
	// This Method is to click on discount plan Button
	public void clickOnDiscountPlanButton() {
		discountPlanButton.click();
	}
	public void clickOnCatalogManagerButton(){
		clickOn(catalogManagerButton);
	}
	
	public void clickOnPriceVersionsButton(){
		clickOn(priceVersionsButton);
	}
	
	public void clickOnExportButton(){
		clickOn(exportButton);
	}
	
	public void clickOnImportButton(){
		clickOn(importButton);
	}
	
	public void clickOnBusinessAttributesButton(){
		clickOn(businessAttributesButton);
	}

	public void clickOnAttributeGroupsButton(){
		clickOn(attributeGroupsButton);
	}
	
	public void clickOnMediaButton(){
		clickOn(mediaButton);
	}
	
	public void clickOnProductFamiliesButton(){
		clickOn(productFamiliesButton);
	}

	public void clickOnTagsButton(){
		clickOn(tagsButton);
	}
	

	public void clickOnTagCategoriesButton(){
		clickOn(tagCategoriesButton);
	}
	
	public void clickOnReportExtractsButton(){
		clickOn(reportExtractsButton);
	}
	
	public void clickOnReportExtractsHistoryButton(){
		clickOn(reportExtractsHistoryButton);
	}
	
	public void clickOnQueryToolsButton(){
		clickOn(queryToolsButton);
	}
	
	public void clickOnQueryBuilderButton(){
		clickOn(queryBuilderButton);
	}
	public void clickOnQueryResultsButton(){
		clickOn(queryResultsButton);
	}
	// This Method is to click on media Button
	public void clickMediaBtn() {
		mediaButton.click();
	}

	// This Method is to click on administration Button
	public void clickOnAdministrationButton() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(generalSettingsButton));
			generalSettingsButton.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			clickOn(generalSettingsButton);
		}	
	}
	
	public void clickOnGeneralSettingsMainMenu() {
		clickOn(generalSettingsButton);	
	}

	// This method to click on profile dropdown
	public void clickOnProfileDropdown() {
		profileDropdown.click();
	}

	// This method to click on Logout link
	public void clickOnLogoutLink() {
		logoutLink.click();
	}

	// This method to verify LoggedIn Username Text
	public String verifyLoggedInUserNameText() {
		String userName = loggedInUserNameText.getText();
		return userName;
	}

	// this method is to click on tags button in menu
	public void clickOnTagsBtn() {
		tagsButton.click();
	}

	// this method is to click on commercial offers button in menu
	public void clickOnCommercialOffersButton() {
		fWait.until(ExpectedConditions.elementToBeClickable(commercialOffersButton));
		commercialOffersButton.click();
	}

	// this method is to click on tags categories button in menu
	public void clickOnTagCategoriesBtn() {
		tagCategoriesButton.click();
	}

	// this method is to click on charges button in menu
	public void clickOnChargesTab() {
		fWait.until(ExpectedConditions.elementToBeClickable(chargesTab));
		chargesTab.click();
	}

	//	this method is to click on products families button in menu

	public void clickOnProductFamilies() {
		productFamilies.click();
	}

	// this method is to click on invoices button in menu
	public void clickOnInvoicesButton() {
		fWait.until(ExpectedConditions.elementToBeClickable(invoicesButton));
		// click on the compose button as soon as the button is visible
		invoicesButton.click();
	}

	public void clickOnOrdersButton() {
		fWait.until(ExpectedConditions.elementToBeClickable(ordersButton));
		ordersButton.click();
	}

	// this method is to click on quotes button in menu
	public void clickOnQuotesTab() {
		wait.until(ExpectedConditions.visibilityOf(quotesTab));
		//fWait.until(ExpectedConditions.elementToBeClickable(quotesTab));
		quotesTab.click();
	}

	// this method is o click on orders button in menu
	public void clickOnOrdersTab() {
		wait.until(ExpectedConditions.elementToBeClickable(ordersTab));
		ordersTab.click();

	}

	// this method is to click on subscriptions in menu
	public void clickOnSubscriptionsButton() {
		wait.until(ExpectedConditions.elementToBeClickable(subscriptionTab));
		subscriptionTab.click();
	}

	// this method is to click on Customer Care Button
	public void clickOnCustomerCareButton() {
		try {
			clickOn(customerCareButton);
		}
		catch (Exception e) {
			wait.until(ExpectedConditions.elementToBeClickable(customerCareButton));
			//customerCareButton.click();
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", customerCareButton);
		}

	}
	public void clickOnCustomerCareButton0() {
		try {
			clickOn(customerCareButton);
		}
		catch (Exception e) {
			driver.navigate().refresh();
			waitPageLoaded();
			clickOn(customerCareButton);
		}
	}
	// this method is to click on Customer Care Button 2
	public void clickOnCustomerCareButton2() {
		clickOn(customerCareButton2);
	}
	
	public void clickOnContactsButton() {
		clickOn(contactsButton);
	}
	
	public void clickOnCollectionPlansButton() {
		clickOn(collectionPlansButton);
	}
	
	public void clickOnDunningsButton() {
		clickOn(dunningsButton);
	}
	
	public void clickOnTemplatesButton() {
		clickOn(templatesButton);
	}
	
	public void clickOnActionsButton() {
		clickOn(actionsButton);
	}
	
	public void clickOnLevelsButton() {
		clickOn(levelsButton);
	}
	
	public void clickOnPoliciesButton() {
		clickOn(policiesButton);
	}
	
	public void clickOnDunningSettingButton() {
		clickOn(dunningSettingButton);
	}
	
	public void clickOnPriceListsButton() {
		clickOn(priceListsButtonButton);
	}
	
	// this method is to click on Customer Care Button
	public void clickOnDiscountPlansButton() {
		wait.until(ExpectedConditions.visibilityOf(discountPlansButton));
		discountPlansButton.click();
	}

	// this method is to click on Article Button
	public void clickOnArticlesButton() {
		wait.until(ExpectedConditions.elementToBeClickable(articleButton));
		articleButton.click();
	}

	public void clickOnBillingSection() {
		// TODO Auto-generated method stub
		fWait.until(ExpectedConditions.visibilityOf(billingButton));
		billingButton.click();
	}

	public void clickOnInvoicingPlansButton() {
		try {
			wait.until(ExpectedConditions.visibilityOf(invoicingPlansButton));
			invoicingPlansButton.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.visibilityOf(invoicingPlansButton));
			invoicingPlansButton.click();
		}	
	}

	public void clickOnAttributeButton() {
		// TODO Auto-generated method stub
		attributesMenu.click();
	}
	public void clickOnCustomers() {
		// TODO Auto-generated method stub
		clickOn(customersButton);
		//		customersButton.click();
	}

	public void clickOnContractsButton() {
		// TODO Auto-generated method stub
		wait.until(ExpectedConditions.visibilityOf(contractsButton));
		clickOn(contractsButton);
	}

	public void clickOnOperationButton() {
		try{
			wait.until(ExpectedConditions.elementToBeClickable(operationButton));
			clickOn(operationButton);
		}
		catch (Exception e) {
			clickOn(operationButton);
		}
	}
	
	public void clickOnOperationMenuButton() {
		wait.until(ExpectedConditions.elementToBeClickable(this.operationButtonMenu));
		operationButtonMenu.click();
	}

	public void clickOnCDRMenuButton() {
		wait.until(ExpectedConditions.elementToBeClickable(this.CDRButton));
		CDRButton.click();
	}
	
	public void clickOnFinanceButton() {
		try{
			wait.until(ExpectedConditions.elementToBeClickable(financeButton));
			financeButton.click();
			waitPageLoaded();
		}
		catch (Exception e) {
			driver.navigate().refresh();
			waitPageLoaded();
			clickOn(financeButton);
		}

	}

	
	public void clickOnManualMatchingButton() {
		clickOn(manualMatchingButton);
	}	
	public void clickOnReportsButton() {
		clickOn(reportsButton);
	}
	
	public void clickOnTrialBalanceButton() {
		clickOn(trialBalanceButton);
	}	
	
	public void clickOnGeneralLedgerButton() {
		clickOn(generalLedgerButton);
	}
	
	public void clickOnSecurityDepositButton() {
		clickOn(securityDepositButton);
	}

	public void clickOnMediationButton() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(mediationButton));
			actions.moveToElement(mediationButton).perform();
			actions.click().build().perform();
			//mediationButton.click();
		}catch (Exception e) {
			clickOn(mediationButton);
		}

	}

	public void clickOnEdrSettings() {
		try {
			clickOn(EdrSettingsButton);
		}catch (Exception e) {
			actions.moveToElement(EdrSettingsButton).perform();
			actions.click().build().perform();
		}

	}

	public void clickOnEdrsButton() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(edrsButton));
			actions.moveToElement(edrsButton).perform();
			//actions.click().build().perform();
			edrsButton.click();
		}catch (Exception e) {
			clickOn(edrsButton);
		}
	}

	public void clickOnBillibRunButton() {
		wait.until(ExpectedConditions.visibilityOf(billingRunButton));
		billingRunButton.click();
	}

	public void clickOnMarketingManagerButton() {
		wait.until(ExpectedConditions.visibilityOf(marketingManagerButton));
		marketingManagerButton.click();
	}

	public void clickOnGoBack() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(goBackButton));
			goBackButton.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			clickOn(goBackButton);
		}	

	}

	public void clickOnRefreshButton() {
		clickOn(refreshButton);
	}

	public void clickOnFinanceSettings() {
		try {
			clickOn(financeSettings);
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(financeSettings));
			financeSettings.click();
		}	
	}

	public void clickOnGeneralSettings() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(generalSettings));
			generalSettings.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			clickOn(generalSettings);
		}	
	}

	public void clickonApplicationSettings() {
		clickOn(applicationSettings);	
	}

	public void clickOnEDR() {
		clickOn(EDR);	
	}

	public void clickOnWalletOperations() {
		clickOn(walletOperationsbutton);
	}

	public void clickOnProductButton () {
		clickOn(productButton);
	}
	// This Method is to click On Home Button
	public void clickOnHomeButton() {
		clickOn(homeButton);
	}

	public void clickOnOpenDrawerButton() {
		Actions a = new Actions(driver);
		a.moveToElement(OpenDrawerButton).perform();
		clickOn(OpenDrawerButton);
	}	
	public void clickOnFrameworkAgreementButton() {
		clickOn(frameworkAgreementButton);
	}

	public void clickOnCatalogueManagerButton() {
		clickOn(catalogueManagerButton);
	}

	public void clockOnReferenceTablesButton() {
		clickOn(referenceTablesButton);
	}

	public void clockOnCustomTablesButton() {
		clickOn(customTablesButton);
	}

	public void clickOnAccountingPeriodButton() {
		clickOn(accountingPeriodButton);
	}

	public void clickOnAccountingPeriodManagerButton() {
		clickOn(accountingPeriodManagerButton);
	}

	public void clickOnAccountingPeriodReportButton() {
		clickOn(accountingPeriodReportButton);
	}

	public void clickOnAccountingEntriesButton() {
		clickOn(accountingEntriesButton);
	}

	public void clickOnAgedTrialBalanceButton() {
		clickOn(agedTrialBalanceButton);
	}

	public void clickOnBillingRulesButton() {
		clickOn(billingRulesButton);
	}

	public void clickOnBillingCyclesButton() {
		clickOn(billingCyclesButton);
	}	

	public void clickOnRatedItemsButton() {
		clickOn(ratedItemsButton);
	}	

	public void clickOnRatingButton() {
		try {clickOn(ratingButton);}catch (Exception e) {}
	}	
	
	public void clickOnMassAdjustmentsButton() {
		wait.until(ExpectedConditions.elementToBeClickable(massAdjustmentsButton));
		massAdjustmentsButton.click();
		//try {clickOn(this.massAdjustmentsButton);}catch (Exception e) {}
	}	
	
	public void clickOnRatingMenuButton() {
		wait.until(ExpectedConditions.elementToBeClickable(ratingMenuButton));
		ratingMenuButton.click();
	}

	public void clickOnWalletOperationsMenuButton() {
		wait.until(ExpectedConditions.elementToBeClickable(walletOperationsMenuButton));
		this.walletOperationsMenuButton.click();
	}
	
	public void clickOnRatedItemsMenuButton() {
		wait.until(ExpectedConditions.elementToBeClickable(ratedItemsMenuButton));
		this.ratedItemsMenuButton.click();
	}
	
	public void clickOnInvoiceValidationButton() {
		clickOn(invoiceValidationButton);
	}

	public void clickOnProfileButton() {
		clickOn(profileButton);
	}	
	
	public void clickOnLogoutButton() {
		clickOn(logoutButton);
	}
	
}