package com.opencellsoft.pages.medias;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ListMediasPage {
	WebDriver driver;

	public ListMediasPage(WebDriver driver) {
		this.driver = driver;
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-textSizeSmall MuiButton-sizeSmall']")
	WebElement addBtn;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase label']")
	WebElement searchInput;
	@FindBy(how = How.XPATH, using = "//tr[@class='MuiTableRow-root jss1643 jss1645 jss1644 MuiTableRow-hover']")
	WebElement foundMedia;
	@FindBy(how = How.XPATH, using = "//tr[@class='MuiTableRow-root jss829 jss831 jss830 MuiTableRow-hover']")
	WebElement advancilyfoundMedia;
	@FindBy(how = How.XPATH, using = "//button[@aria-label='Add filter']")
	WebElement addFilterBtn;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Media code')]")
	WebElement serachByCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement codeInput;

	// This method is to click on media button
	public void clickAddMediaBtn() {
		addBtn.click();
	}
	
	// This method is to searching of media by name
	public void serachForMedia(String mediaName) {
		searchInput.sendKeys(mediaName);
		foundMedia.click();
	}

	// This method is to click on add filter button
	public void clickAddFIlterBtn() {
		addFilterBtn.click();
	}

	// This method is to searching of media by code
	public void SearchByCode(String mediaCode) {
		serachByCode.click();
		codeInput.sendKeys(mediaCode);
		advancilyfoundMedia.click();
		
	}	
}