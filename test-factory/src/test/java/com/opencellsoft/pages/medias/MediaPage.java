package com.opencellsoft.pages.medias;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class MediaPage {
	WebDriver driver;

	public MediaPage(WebDriver driver) {
		this.driver = driver;
	}
	
	// Using FindBy for locating elements
	@FindBy(how = How.CSS, using = "div.layout.jss1169 div.jss1170.jss1 main.jss1171 div.jss1172.jss2 div.jss1679 div.jss1680:nth-child(2) div.jss1681:nth-child(2) div.jss1810.jss1809 div.MuiBox-root.jss1814 div.SetErrorsOutsideForm form.OfferCommercial div.MuiGrid-root.gridBlock.MuiGrid-container.MuiGrid-spacing-xs-2.MuiGrid-grid-xs-12 div.MuiGrid-root.gridBlock.fullWidthChild.fullWidthChild.MuiGrid-container.MuiGrid-item:nth-child(1) div.MuiPaper-root.MuiCard-root.MuiPaper-elevation1.MuiPaper-rounded div.MuiBox-root.jss1886.TabPanel:nth-child(5) div.MuiGrid-root.gridBlock.fullWidthChild.fullWidthChild div.MuiGrid-root.gridBlock.fullWidthChild.fullWidthChild.MuiGrid-container div.MultiListPickerInput div.jss1927 div.MuiToolbar-root.MuiToolbar-dense.jss1950.MuiToolbar-gutters:nth-child(2) div.MuiToolbar-root.MuiToolbar-dense.jss1945.jss1952.jss1951.MuiToolbar-gutters:nth-child(2) div.ButtonGroupGutters > button.MuiButtonBase-root.MuiButton-root.MuiButton-text.MuiButton-textSecondary.MuiButton-textSizeSmall.MuiButton-sizeSmall:nth-child(2)")
	WebElement createMedia;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement mediaNameInput;
	@FindBy(how = How.XPATH, using = "//span[@class='']//input[@type='checkbox']")
	WebElement checkBox ;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiToolbar-root MuiToolbar-dense jss1939 MuiToolbar-gutters']//button[@aria-label='Delete']")
	WebElement deleteMedia;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Confirm']")
	WebElement confirmDeleteMedia;
	@FindBy(how = How.XPATH, using = "//input[@id='mediaPath']")
	WebElement urlPath;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement btnSave;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackBtn;
	
	// This method is to click on create media button
	public void clickOnCreateMedia() {
		createMedia.click();
	}	
	
	// This method is to select check box media
	public void clickOnCheckBoxMedia() {
		checkBox.click();
	}
	
	// This method is to click on delete media
	public void clickOnDeleteMedia() {
		deleteMedia.click();
	}
	
	// This method is to confirm delete media
	public void clickOnConfirmDeleteMedia() {
		confirmDeleteMedia.click();
	}
	
	// This method is to edit media type image
	public void editMediaTypeImage(String name, String url) {
		mediaNameInput.sendKeys(name);
		urlPath.sendKeys(url);
		btnSave.submit();
		goBackBtn.click();
	}
}
