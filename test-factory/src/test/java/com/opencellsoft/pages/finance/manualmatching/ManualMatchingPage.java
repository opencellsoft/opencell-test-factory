package com.opencellsoft.pages.finance.manualmatching;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class ManualMatchingPage extends TestBase {

	public ManualMatchingPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//button[@aria-controls='customized-menu' or text() = 'Actions']")
	WebElement actionButton;
	@FindBy(how = How.XPATH, using = "//*[text() ='Create AO']")
	WebElement createAOButton;
	@FindBy(how = How.XPATH, using = "//*[@id='payment_method']")
	WebElement paymentMethodInput;
	@FindBy(how = How.XPATH, using = "//*[@id = 'customerAccountCode']")
	WebElement customerInput;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//input[@id='wildcardOrIgnoreCase description']")
	WebElement searchBycustomerDescriptionInput;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//*[@id='currenciesForBA' or @id='currencies']")
	WebElement currencyInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'amount']")
	WebElement amountInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'reference']")
	WebElement referenceInput;
	@FindBy(how = How.XPATH, using = "//button[ normalize-space() = 'Validate']")
	WebElement validateButton;
	@FindBy(how = How.XPATH, using = "(//input[@id='searchBar'])[1]")
	WebElement debitAccountOperationsSearchInput;
	@FindBy(how = How.XPATH, using = "(//input[@id='searchBar'])[2]")
	WebElement creditAccountOperationsSearchInput;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Match']")
	WebElement matchButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Confirm']")
	WebElement confirmButton;
	@FindBy(how = How.XPATH, using = "//table[@source = 'debitAOs']//tbody//input[@type = 'checkbox']")
	WebElement firstDebitAOCheckBox;
	@FindBy(how = How.XPATH, using = "//table[@source = 'creditAOs']//tbody//input[@type = 'checkbox']")
	WebElement firstCreditAOCheckBox;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Match partially']")
	WebElement matchPartiallyButton;
	@FindBy(how = How.XPATH, using = "//*[contains(text() , 'Transfer AO mode')]")
	WebElement tranferAOModeButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Transfer AO']")
	WebElement tranferAOButton;
	@FindBy(how = How.XPATH, using = "//*[text() = 'Transfer']")
	WebElement tranferButton;
	@FindBy(how = How.XPATH, using = "//input[@id='customerAccount']")
	WebElement tranferToInput;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase description']")
	WebElement customerNameInput;
	@FindBy(how = How.XPATH, using = "(//*[@title= 'Filter' or @aria-label = 'Filter'])[1]")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//*[@id='matchingStatus' or @aria-labelledby='matchingStatus-label matchingStatus']")
	WebElement statusSearchInput;
	
	public void clickOnActionButton() {
		clickOn(actionButton);
	}

	public void clickOnCreateAOButton() {
		clickOn(createAOButton);
	}

	public void setPaymentMethod(String value) {
		clickOn(paymentMethodInput);
		clickOn(driver.findElement(By.xpath("//*[@data-value='" + value + "']")));

	}

	public void setCustomer(String value) {
		clickOn(customerInput);
		searchBycustomerDescriptionInput.sendKeys(Keys.chord(Keys.CONTROL, "a"),value);
		waitPageLoaded();
		clickOn(driver.findElement(By.xpath("//div[@role = 'dialog']//table//*[text() ='" + value + "']")));
	}

	public void setCurrency(String value) {
		clickOn(currencyInput);
		clickOn(driver.findElement(By.xpath("//*[@data-value='" + value + "']")));
	}

	public void setAmount(String value) {
		amountInput.sendKeys(Keys.chord(Keys.CONTROL, "a"),value);
	}

	public void setReference(String value) {
		referenceInput.sendKeys(Keys.chord(Keys.CONTROL, "a"),value);
	}

	public void setAccountOperationDate(String value) {
	}

	public void clickOnValidateButton() {
		clickOn(validateButton);
	}

	public void setCreditAccountOperationsSearchInput(String value) {
		creditAccountOperationsSearchInput.sendKeys(Keys.chord(Keys.CONTROL, "a"),value);
		waitPageLoaded();
	}

	public void setDebitAccountOperationsSearchInput(String value) {
		debitAccountOperationsSearchInput.sendKeys(Keys.chord(Keys.CONTROL, "a"),value);
		waitPageLoaded();
	}

	public int elementsFoundSizeDebitAOs( String value) {
		List<WebElement> result = driver.findElements(By.xpath("//table[@source='debitAOs']//td[ normalize-space() = '" + value + "']"));
		return result.size();
	}
	
	public int elementsFoundSizeCreditAOs( String value) {
		List<WebElement> result = driver.findElements(By.xpath("//table[@source='creditAOs']//td[ normalize-space() = '" + value + "']"));
		return result.size();
	}
	
	public void clickOnMatchButton() {
		clickOn(matchButton);
	}
	
	public void selectFirstDebitAO() {
		clickOn(firstDebitAOCheckBox);
	}
	
	public void selectFirstCreditAO() {
		firstCreditAOCheckBox.click();
//		clickOn(firstCreditAOCheckBox);
	}
	
	public void clickOnMatchPartiallyButton() {
		clickOn(matchPartiallyButton);
	}
	
	public void clickOnConfirmButton() {
		clickOn(confirmButton);
	}
	
	public void clickOnTranferAOMode() {
		clickOn(tranferAOModeButton);
	}
	
	public void clickOnTranferAOButton() {
		clickOn(tranferAOButton);
	}
	
	public void clickOnTranferButton() {
		clickOn(tranferButton);
	}
	
	public void setCustomerName(String value) {
		clickOn(tranferToInput);
		customerNameInput.sendKeys(Keys.chord(Keys.CONTROL, "a"),value);
		try {
			clickOn(driver.findElement(By.xpath("//table//td[ normalize-space() = '" + value + "']")));
		}catch (Exception e) {
			clickOn(driver.findElement(By.xpath("//table//td[ normalize-space() = '" + value + "']")));
		}
		
	}

	public void clickOnFilterButton() {
		clickOn(filterButton);
	}
	
	public void clickOnFilterChoise(String value) {
		clickOn(driver.findElement(By.xpath("//ul//*[text() = '" + value + "']")));
	}
	
	public void clickOnStatusSearchInput() {
		clickOn(statusSearchInput);
	}
	
	public void clickOnStatusChoise(String value) {
		clickOn(driver.findElement(By.xpath("//ul//*[contains(text() , '" + value + "')]")));
	}
	
}
