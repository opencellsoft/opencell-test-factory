package com.opencellsoft.pages.finance.reports;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class GeneralLedgerPage extends TestBase {

	public GeneralLedgerPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//*[@title = 'Filter' or @aria-label = 'Filter' or normalize-space() = 'Filter']")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//*[@id = 'wildcardOrIgnoreCase accountingCode->description']")
	WebElement accountingNameInput;
	@FindBy(how = How.XPATH, using = "//*[@id = 'functionalCurrency']")
	WebElement functionalCurrencyButton;
	@FindBy(how = How.XPATH, using = "//*[@aria-label='Export table' or @aria-label='Download selection']")
	WebElement downloadSelectionButton;
	@FindBy(how = How.XPATH, using = "//*[contains(@class , 'MuiAlert-message')]")
	List <WebElement> alertMsg;
	
	public void clickOnFilterButton() {
		clickOn(filterButton);
	}
	
	public void activateFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//li//*[text()= '" + value + "']")));
	}
	
	public void setFilterByAccountingNameInput(String value) {
		accountingNameInput.sendKeys(Keys.CONTROL + "a");
		accountingNameInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public void setFunctionalCurrency(Boolean value) {
		WebElement element = driver.findElement(By.xpath("(//span[contains(@class, 'MuiSwitch-switchBase MuiSwitch-colorPrimary')])[1]"));
		String class_name = element.getAttribute("class");
		
		if((value.equals(true) && !class_name.contains("Mui-checked")) || (value.equals(false) && class_name.contains("Mui-checked")) ) {
			clickOn(functionalCurrencyButton);
		}
	}
	
	
	public void clickOnDownloadSelectionButton() {
		clickOn(downloadSelectionButton);
	}
	
	public void clickOnExportFormat(String value) {
		clickOn(driver.findElement(By.xpath("//ul//*[ normalize-space() ='" + value + "']")));
	}
	
	public boolean isAlertMsgDisplayed() {
		return alertMsg.size() == 0;
	}
}
