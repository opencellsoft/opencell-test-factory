package com.opencellsoft.pages.finance.accountingperiod;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class AccountingPeriodManagerPage extends TestBase {

	public AccountingPeriodManagerPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//div[@id='main-content']//table")
	WebElement accoutingPeriodtable;
	@FindBy(how = How.XPATH, using = "//div[@id='main-content']//table//tbody//tr")
	List<WebElement> accoutingPeriodList;
	@FindBy(how = How.XPATH, using = "//div[@id='main-content']//table//tbody//tr//div[@role = 'expand']")
	List<WebElement> accoutingPeriodListExpandButton;
	@FindBy(how = How.XPATH, using = "//button[@title = 'Close sub period' or @aria-label = 'Close sub period']//*[contains(@style , 'green')]")
	List<WebElement> subPeriodsOpenList;
	@FindBy(how = How.XPATH, using = "//button[@title = 'Re-open sub period' or @aria-label = 'Re-open sub period']//*[contains(@style , 'red')]")
	List<WebElement> subPeriodsCloseList;
	@FindBy(how = How.XPATH, using = "//button[@title = 'Close fiscal year' or @aria-label = 'Close fiscal year']")
	List<WebElement> closeFiscalYearButtons;
	@FindBy(how = How.XPATH, using = "//button[@title = 'Re-open fiscal year' or @aria-label = 'Re-open fiscal year']")
	List<WebElement> reopenFiscalYearButtons;
	@FindBy(how = How.XPATH, using = "//button[@title = 'Refresh' or @aria-label = 'Refresh']")
	WebElement refreshButton;
	@FindBy(how = How.XPATH, using = "//*[contains(text(), 'Confirm')]")
	WebElement confirmButton;
	@FindBy(how = How.XPATH, using = "//*[contains(text(), 'YES')]")
	WebElement yesButton;
	@FindBy(how = How.XPATH, using = "//textarea[@id = 'reason']")
	WebElement reasonInput;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//*[contains(text(), 'Validate')]")
	WebElement validateButton;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//*[contains(text(), 'Cancel')]")
	WebElement cancelButton;
	@FindBy(how = How.XPATH, using = "//div[contains(text(), 'Generate accounting period')]")
	WebElement generateAccountingPeriodButton;
	
	
	// return the number of lines found
	public int elementsFoundSize(String value) {
		List<WebElement> result = driver.findElements(By.xpath("//td[translate(normalize-space(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='" + value + "']"));
		return result.size();
	}
	
	public int subPeriodOpenSize() {
		return subPeriodsOpenList.size();
	}

	public int subPeriodCloseSize() {
		return subPeriodsCloseList.size();
	}
	
	public boolean checkValueInAccoutingPeriodList(String value) {
		return accoutingPeriodtable.getText().contains(value);
	}
	
	public void clickOnAccountingPeriod(int i) {
		clickOn(accoutingPeriodListExpandButton.get(i-1));
		waitPageLoaded();
	}
	
	public void clickOnCloseFiscalYearButton(int i) {
		clickOn(closeFiscalYearButtons.get(i-1));
		waitPageLoaded();
	}
	
	public void clickOnReopenFiscalYearButton(int i) {
		clickOn(reopenFiscalYearButtons.get(i-1));
		waitPageLoaded();
	}
	
	public void clickOnConfirmButton() {
		clickOn(confirmButton);
		waitPageLoaded();
	}
	
	public void clickOnYesButton() {
		clickOn(yesButton);
		waitPageLoaded();
	}
	
	public void clickOnRefreshButton() {
		try {
			clickOn(refreshButton);
			waitPageLoaded();
		}catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public void clickOnCloseSubperiodForRegularUser(int i, int j) {
		WebElement button=null;
		try {
			button= driver.findElement(By.xpath("(//div[@id='main-content']//table)[\" + i + \"]//tbody/tr[\" + j + \"]//td[4]//button[@title = 'Close sub period' or @aria-label = 'Close sub period']"));

		}catch (Exception e) {
			button= driver.findElement(By.xpath("(//div[@id='main-content']//table)[\" + i + \"]//tbody/tr[\" + j + \"]//td[4]"));
		}
		clickOn(button);
		waitPageLoaded();
	}
	
	public void clickOnOpenSubperiodForRegularUser(int i, int j) {
		String element = "(//div[@id='main-content']//table)[" + i + "]//tbody/tr[" + j + "]//td[4]//button[@title = 'Re-open sub period' or @aria-label = 'Re-open sub period']";
		WebElement button= driver.findElement(By.xpath(element));
		clickOn(button);
		waitPageLoaded();
	}
	public void setReason (String reason) {
		reasonInput.sendKeys(reason);
		waitPageLoaded();
	}
	
	public void clickOnValidateButton() {
		clickOn(validateButton);
		waitPageLoaded();
	}
	
	public void clickOnCancelButton() {
		clickOn(cancelButton);
		waitPageLoaded();
	}
	
	public void clickOnCloseSubperiodForFinanceUser(int i, int j) {
		String element = "(//div[@id='main-content']//table)[" + i + "]//tbody/tr[" + j + "]//td[7]//button[@title = 'Close sub period' or @aria-label = 'Close sub period']";
		WebElement button= driver.findElement(By.xpath(element));
		clickOn(button);
		waitPageLoaded();
	}
	
	public void clickOnOpenSubperiodForFinanceUser(int i, int j) {
		String element = "(//div[@id='main-content']//table)[" + i + "]//tbody/tr[" + j + "]//td[7]//button[@title = 'Re-open sub period' or @aria-label = 'Re-open sub period']";
		WebElement button= driver.findElement(By.xpath(element));
		clickOn(button);
		waitPageLoaded();
	}
	
	public boolean isMessageDisplayed(String msg) {
		WebElement message = driver.findElement(By.xpath("//div[contains(text(), '" + msg + "')]"));
		return message.isDisplayed();
	}
	
	public void clickOnGenerateAccountingPeriodButton() {
		clickOn(generateAccountingPeriodButton);
		waitPageLoaded();
	}
}
