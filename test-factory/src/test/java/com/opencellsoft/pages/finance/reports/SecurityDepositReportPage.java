package com.opencellsoft.pages.finance.reports;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.opencellsoft.base.TestBase;

public class SecurityDepositReportPage extends TestBase {
	public SecurityDepositReportPage() {
		PageFactory.initElements(driver, this);
	}
	private String customer,status,totalBalance;

	@FindBy(how = How.ID, using = "customerAccount")
	WebElement customerAccountInput;
	@FindBy(how = How.ID, using = "wildcardOrIgnoreCase code")
	WebElement securityDepositNameInput;
	@FindBy(how = How.XPATH, using = "//table[@class='MuiTable-root']")
	WebElement reportTable;
	@FindBy(how = How.ID, using = "currency->currencyCode")
	WebElement filterByCurrencyInput;
	@FindBy(how = How.XPATH, using = "//*[@title = 'Filter' or @aria-label = 'Filter' or normalize-space() = 'Filter']")
	WebElement filterButton;
	
	public void setCustomerAccountInput(String value) {
		customerAccountInput.sendKeys(Keys.CONTROL + "a");
		customerAccountInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public void setSecurityDepositNameInput(String value) {
		securityDepositNameInput.sendKeys(Keys.CONTROL + "a" + Keys.DELETE);
		securityDepositNameInput.sendKeys(value);
		waitPageLoaded();
	}
	
	public void clickOnFilterButton() {
		clickOn(filterButton);
	}
	
	public void activateFilterBy(String value) {
		clickOn(driver.findElement(By.xpath("//li//*[text()= '" + value + "']")));
	}
	
	public void clickOnFilterByCurrencyInput() {
		clickOn(filterByCurrencyInput);
	}
	
	public void clickOnCurrency(String value) {
		clickOn(driver.findElement(By.xpath("//*[ normalize-space() ='" + value + "']")));
	}
	
	public void clickOnDeleteFilterByCurrency(String value) {
		clickOn(driver.findElement(By.xpath("//span[ normalize-space() ='" + value + "']/following-sibling::*[name()='svg']")));
	}
	
	// return the number of lines found
	public int elementsFoundSize(String value) {
		List<WebElement> result = driver.findElements(By.xpath("//td[normalize-space() = '" + value + "']"));
		return result.size();
	}
	
	public void checkSecurityDepositTable(List<SecurityDepositReportPage> expectedSD) {
		String tableValues=  reportTable.getText();
		/*check expected customer in security table*/
		int countMatch = StringUtils.countMatches(tableValues, expectedSD.get(0).getCustomer());
		Assert.assertEquals(countMatch, 5);
		
		/*check total balance*/
		Assert.assertTrue(tableValues.contains(expectedSD.get(0).getTotalBalance()));
		
		/*check expected status in security table*/
		for (SecurityDepositReportPage eSD:expectedSD) {
			countMatch = StringUtils.countMatches(tableValues, eSD.getStatus());
			if (eSD.getStatus().equals("LOCKED"))
				Assert.assertEquals(countMatch, 2);
			else
				Assert.assertEquals(countMatch, 1);
		}
	}
	
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public SecurityDepositReportPage( String customer, String status, String totalBalance) {
		super();
		
		this.customer = customer;
		this.status = status;
		this.totalBalance = totalBalance;
	}
	public String getTotalBalance() {
		return totalBalance;
	}
	public void setTotalBalance(String totalBalance) {
		this.totalBalance = totalBalance;
	}
	



}
