package com.opencellsoft.pages.finance.reports;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class TrialBalancePage extends TestBase {
	
	public TrialBalancePage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//*[@id = 'period']")
	WebElement filterByPeriodInput;
	@FindBy(how = How.XPATH, using = "//*[@aria-label='Export table' or @aria-label='Download selection']")
	WebElement downloadSelectionButton;
	@FindBy(how = How.XPATH, using = "//*[contains(@class , 'MuiAlert-message')]")
	List <WebElement> alertMsg;
	
	public void clickOnFilterByPeriodInput() {
		clickOn(filterByPeriodInput);
	}
	
	public void clickOnPeriod(String value) {
		clickOn(driver.findElement(By.xpath("//ul//*[ normalize-space() ='" + value + "']")));
	}
	
	public void clickOnDownloadSelectionButton() {
		clickOn(downloadSelectionButton);
	}
	
	public void clickOnExportFormat(String value) {
		clickOn(driver.findElement(By.xpath("//ul//*[ normalize-space() ='" + value + "']")));
	}
	
	public boolean isAlertMsgDisplayed() {
		return alertMsg.size() == 0;
	}
}
