/**
 * 
 */
package com.opencellsoft.pages.finance.accountingperiod;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class AccountingEntriesPage extends TestBase {
	
	public AccountingEntriesPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.XPATH, using = "//div[@aria-labelledby = 'fiscalYear-label fiscalYear']")
	WebElement fiscalYearLabel;
	@FindBy(how = How.XPATH, using = "//button[@aria-controls = 'customized-menu' or @title = 'Download selection']")
	WebElement actionButton;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'CSV')]")
	WebElement downlaodCSVButton;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'XLSX')]")
	WebElement downlaodXLSXButton;
	@FindBy(how = How.XPATH, using = "//ul[@role = 'menu']//input[@type = 'checkbox']")
	WebElement exportCurrentFiltreCheckbox;
	@FindBy(how = How.XPATH, using = "//span[text() = 'Confirm']")
	WebElement confirmButton;
	@FindBy(how = How.XPATH, using = "//input[@id = 'stepInDays']")
	WebElement StepInDaysInput;
	@FindBy(how = How.XPATH, using = "//div[@aria-labelledby = 'date-label date']")
	WebElement dateSelect;
	@FindBy(how = How.XPATH, using = "//li[@data-value = 'customStartDate']")
	WebElement customStartDate;
	@FindBy(how = How.XPATH, using = "//input[@id = 'customStartDate' or @value = 'YYYY-MM-DD' or @placeholder = 'YYYY-MM-DD']")
	WebElement customDateInput;
	@FindBy(how = How.XPATH, using = "(//button[@class = 'MuiButtonBase-root MuiIconButton-root MuiPickersCalendarHeader-iconButton' or @aria-label = 'Previous month'])[1]")
	WebElement montheBefore;
	@FindBy(how = How.XPATH, using = "(//button[@class = 'MuiButtonBase-root MuiIconButton-root MuiPickersCalendarHeader-iconButton' or contains(@aria-label , 'month')])[2]")
	WebElement montheAfter;
	@FindBy(how = How.XPATH, using = "(//div[@role = 'dialog' or contains(@class, 'MuiPickersBasePicke')]//*[text()='1'])[1]")
	WebElement monthFirst;
	@FindBy(how = How.XPATH, using = "//ul[@role='menu']//span[contains(@class, 'MuiCheckbox-root')]")
	WebElement exportInput;
	public void clickOnreference(String value) {
		clickOn(driver.findElement(By.xpath("//a//span[contains(text(), '" + value + "')]")));
	}
	
	public void clickOnActionButton() {
		try {
			clickOn(actionButton);
		}catch (Exception e) {
			clickOn(driver.findElement(By.xpath("//*[@id = 'EXPORT']//span")));
		}
		
	}
	
	public void clickOndownlaodCSVButton() {
		clickOn(downlaodCSVButton);
	}
	
	public void clickOndownlaodXLSXButton() {
		clickOn(downlaodXLSXButton);
	}
	
	public void hitEnter() {
		Actions actions = new Actions(driver);
		actions.sendKeys(Keys.ENTER).perform();
		waitPageLoaded();
	}
	
	public void selectExportCurrentFiltre() {
		String class_name = exportInput.getAttribute("class");
		if( !class_name.contains("Mui-checked")) {
			clickOn(exportCurrentFiltreCheckbox);
		}
	}
	
	public void clickOnConfirmButton() {
		clickOn(confirmButton);
	}
	
	public String getFiscalYearlabelText() {
		return fiscalYearLabel.getText();
	}
	
	public void setStepInDays(String i) {
		StepInDaysInput.sendKeys(Keys.CONTROL + "a");
		StepInDaysInput.sendKeys(i);
		waitPageLoaded();
	}
	
	public void selectCustomDate() {
		clickOn(dateSelect);
		waitPageLoaded();
		clickOn(customStartDate);
		waitPageLoaded();
	}
	
	public void selectPastDate(int m) {
		clickOn(customDateInput);
		waitPageLoaded();
		for (int i= 0; i< m; i++){
			clickOn(montheBefore);
		}
		clickOn(monthFirst);
	}
	
	public void selectFuturtDate(int m) {
		clickOn(customDateInput);
		for (int i= 0; i< m; i++){
			clickOn(montheAfter);
		}
		clickOn(monthFirst);
	}
	
	
}
