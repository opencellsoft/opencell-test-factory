package com.opencellsoft.pages.finance.accountingperiod;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class AccountingPeriodReportPage extends TestBase {

	public AccountingPeriodReportPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//input[@name='currentMonth']")
	WebElement currentMonthInput;
	@FindBy(how = How.XPATH, using = "//button[contains(@aria-label , 'Export') or @aria-label ='Download selection']")
	WebElement actionButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'CSV')]")
	WebElement downlaodCSVButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'XLSX')]")
	WebElement downlaodXLSXButton;
	@FindBy(how = How.XPATH, using = "//table//thead//input[@type = 'checkbox']")
	WebElement AllOperationsCheckbox;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//*[text() = 'Confirm']")
	WebElement confirmButton;
	
	public void clickOnCurrentMonthInput() {
		WebElement element = driver.findElement(By.xpath("(//span[contains(@class, 'MuiSwitch-switchBase MuiSwitch-colorPrimary')])[1]"));
		String class_name = element.getAttribute("class");
		if(class_name.contains("Mui-checked")) {
			clickOn(currentMonthInput);
		}
	}
	
	public void clickOnreference(String value) {
		clickOn(driver.findElement(By.xpath("//a//span[contains(text(), '" + value + "')]")));
	}
	
	public String getReferenceNumberOf(int row) {
		return driver.findElement(By.xpath("(//table//tr)[" + row + "]//td[7]")).getText();
	}
	
	public boolean IsForcedPosting(int row) {
		String element = driver.findElement(By.xpath("(//table//tr)[" + row + "]//td[12]")).getAttribute("value");
		if(element.equals("true")) {
			return true;
		}else {
			return false;
		}
	}
	
	public void clickOnActionButton() {
		clickOn(actionButton);
	}
	
	public void clickOndownlaodCSVButton() {
		clickOn(downlaodCSVButton);
	}
	
	public void clickOndownlaodXLSXButton() {
		clickOn(downlaodXLSXButton);
	}
	
	public void selectAllOperations() {
		WebElement element = driver.findElement(By.xpath("//table[contains(@class , 'MuiTable-root')]//thead//span[contains(@class, 'MuiCheckbox-root ')]"));
		String class_name = element.getAttribute("class");
		if(!class_name.contains("Mui-checked")) {
			clickOn(AllOperationsCheckbox);
		}
	}
	
	public void clickOnConfirmButton() {
		clickOn(confirmButton);
	}
	
	
	
}
