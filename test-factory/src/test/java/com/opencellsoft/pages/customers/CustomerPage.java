package com.opencellsoft.pages.customers;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.opencellsoft.base.TestBase;


public class CustomerPage extends TestBase {

	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public CustomerPage(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		this.fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(2)).ignoring(NoSuchElementException.class);
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//button[@title='Refresh']")
	WebElement refreshButton;
	@FindBy(how = How.XPATH, using = "//button//span[normalize-space()='Subscriptions']")
	WebElement subscriptionsTab2;	
	@FindBy(how = How.XPATH, using = "//button[@id='TAB_SUBSCRIPTIONS']")
	WebElement subscriptionsTab;	  
	@FindBy(how = How.XPATH, using = "//*[normalize-space()='Consumers']")
	WebElement consumersTab;
	@FindBy(how = How.XPATH, using = "//*[normalize-space()='Security deposit']")
	WebElement securityDepositTab;
	@FindBy(how = How.XPATH, using = " (//td)[11]")
	WebElement subscription;
	@FindBy(how = How.XPATH, using = " (//td)[11]")
	WebElement subscriptionWithInvoicingPlan;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'New subscription')]")
	WebElement newSubscriptionButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'New order')]")
	WebElement newOrderButton;
	@FindBy(how = How.XPATH, using = "//*[text()='Rated Items' or @id='TAB_CONSUMPTIONS']")
	WebElement ratedItemsButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Generate invoice')]")
	WebElement generateInvoiceButton;
	@FindBy(how = How.XPATH, using = "//*[@class='MuiTab-wrapper' or @id = 'TAB_INVOICES'][contains(text(),'Invoices')]")
	WebElement invoicesButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'New invoice')]")
	WebElement newInvoiceButton;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Paying account']")
	WebElement payingAccount;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Balance')]")
	WebElement balanceTab;
	@FindBy(how = How.XPATH, using = "//input[@value='0']")
	WebElement invoiceCheckBox;
	@FindBy(how = How.XPATH, using = "(//td)[18]")
	WebElement checkBox;
	@FindBy(how = How.XPATH, using = "//input[@value='1']")
	WebElement paymentToRefundCheckBox;
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Pay my invoice')]")
	WebElement payMyInvoiceButton;
	@FindBy(how = How.XPATH, using = "//input[@id='check_amount']")
	WebElement amountToPayInput;
	@FindBy(how = How.XPATH, using = "//input[@id='reference']")
	WebElement checkReferenceInput;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Confirm']")
	WebElement confirmButton;
	@FindBy(how = How.XPATH, using = "//div[@class='ButtonGroupGutters']//button[@type='button']")
	WebElement createPaymentButton;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement customerCode;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[12]")
	WebElement statusSpan;
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[9]")
	WebElement machingStatusSpan;
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Refund')]")
	WebElement refundButton;
	@FindBy(how = How.XPATH, using = "//input[@id='iban_refund']")
	WebElement ibanInput;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'New consumer')]")
	WebElement addNewConsumerButton;
	@FindBy(how = How.XPATH, using = "//table[1]/thead/tr/th[2]")
	WebElement subscriptionColumnLabel;		
	@FindBy(how = How.XPATH, using = "//span[@class='MuiTab-wrapper'][contains(text(),'Invoices')]")
	WebElement invoicesTab;	
	@FindBy(how = How.XPATH, using = "//table[1]/thead[1]/tr[1]/th[1]")
	WebElement invoiceColumnLabel;	
	@FindBy(how = How.XPATH, using = "//button[@role = 'tab']//span[text() = 'Billing']")
	WebElement billingTab;
	@FindBy(how = How.XPATH, using = "//input[@id = 'billingCycle']")
	WebElement billingCycleInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'wildcardOrIgnoreCase code']")
	WebElement billingCycleCodeInput;
	@FindBy(how = How.XPATH, using = "//*[@role='tablist']//*[@id ='TAB_ORDERS' or text() = 'Orders']")
	WebElement orderTab;
	@FindBy(how = How.XPATH, using = "//button[@type = 'submit' ]")
	WebElement saveButton;
	// This method is to click on subscription button
	public void clickOnCreateConsumerButton() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(addNewConsumerButton));
			addNewConsumerButton.click();
		} catch (org.openqa.selenium.StaleElementReferenceException ex) {
			clickOn(addNewConsumerButton);
		}
	}
	
	public void clickOnConsumersTab() {
		try {
			clickOn(consumersTab);
		}
		catch (Exception e) {
			fWait.until(ExpectedConditions.visibilityOf(consumersTab));
			consumersTab.click();
		}
	}

	public void clickOnInvoicesTab() {
		try {
			invoicesTab.click();
		}
		catch (Exception e) {
			wait.until(ExpectedConditions.elementToBeClickable(invoicesTab));
			invoicesTab.click();
		}
	}
	
	public void clickOnSubscriptionTab() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(subscriptionsTab));
			subscriptionsTab.click();

		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(subscriptionsTab));
			subscriptionsTab.click();
		}	
	}
	public void clickOnSubscriptionTab2() {
		clickOn(subscriptionsTab2);	
	}
	public void clickOnSecurityDepositTab() {
		try {
			securityDepositTab.click();
		} 
		catch (Exception e) {
			fWait.until(ExpectedConditions.elementToBeClickable(securityDepositTab));
			securityDepositTab.click();
		}
	}
	
	public void clickOnRefreshButton() {
		fWait.until(ExpectedConditions.visibilityOf(this.refreshButton));
		refreshButton.click();
	}

	public void clickOnSubscriptionWithInvoicingPlan() {
//			 fWait.until(ExpectedConditions.visibilityOf(subscription));
			 JavascriptExecutor jse = (JavascriptExecutor) driver;
			 jse.executeScript("arguments[0].click();", subscriptionWithInvoicingPlan);
		}
	public void clickOnGeneratedSubscription() {
//		 fWait.until(ExpectedConditions.visibilityOf(subscription));
		 JavascriptExecutor jse = (JavascriptExecutor) driver;
		 jse.executeScript("arguments[0].click();", subscription);
	}

	public void clickOnCreatedSubscription(String subscriptionCode) {
		WebElement elem = fWait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//span[contains(text(),'" + subscriptionCode + "')]")));
		elem.click();
	}

	public void clickOnCreateSubscriptionButton() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(newSubscriptionButton));
			newSubscriptionButton.click();
		} catch (org.openqa.selenium.StaleElementReferenceException ex) {
			fWait.until(ExpectedConditions.elementToBeClickable(newSubscriptionButton));
			newSubscriptionButton.click();
		}
	}
	public void clickOnCreateSubscription() {
		clickOn(newSubscriptionButton);
	}
	
	public void clickOnRatedItemsButton() {
		try {
			ratedItemsButton.click();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			wait.until(ExpectedConditions.elementToBeClickable(ratedItemsButton));
			ratedItemsButton.click();
		}
	}

	public void clickOnRatedItemsTab() {
		try {
			clickOn(ratedItemsButton);
		}
		catch (Exception e) {
			wait.until(ExpectedConditions.elementToBeClickable(ratedItemsButton));
			ratedItemsButton.click();
		}
	}
	
	public void clickOnGenerateInvoiceButton() {
		// TODO Auto-generated method stub
		generateInvoiceButton.click();
	}

	public void clickOnInvoicesButton() {
		try {
			clickOn(invoicesButton);
		}catch (Exception e) {
			wait.until(ExpectedConditions.elementToBeClickable(invoicesButton));
			invoicesButton.click();
		}
		
	}	

	public void clickOnNewInvoiceButton() {
		try {
			newInvoiceButton.click();
		}catch (Exception e) {
			wait.until(ExpectedConditions.elementToBeClickable(newInvoiceButton));
			newInvoiceButton.click();
		}
		
	}

	public void clickOnAddInvoiceButton() {
		newInvoiceButton.click();
	}
	
	// click on paying account section
	public void clickOnPayingAccount() {
		//wait.until(ExpectedConditions.visibilityOfAllElements(payingAccount));
		Actions actions = new Actions(driver);
		//actions.moveToElement(payingAccount).click().build().perform();
		wait.until(ExpectedConditions.elementToBeClickable(payingAccount));
		payingAccount.click();
	}

	public void clickOnCreatePaymentMethodButton() throws InterruptedException {
		try {
			clickOn(createPaymentButton);
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex) {
			wait.until(ExpectedConditions.elementToBeClickable(createPaymentButton));
			createPaymentButton.click();
		}		
	}

	public void clickOnBalanceTab() {
		try {
			clickOn(balanceTab);
		}catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOf(balanceTab));
			balanceTab.click();
		}
		
	}

	public void selectInvoice() throws InterruptedException {
//		wait.until(ExpectedConditions.visibilityOf(checkBox));
//		fWait.until(ExpectedConditions.elementToBeSelected(invoiceCheckBox));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(250, 0)");
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		js.executeScript("arguments[0].scrollIntoView();", invoiceCheckBox);
		while (!invoiceCheckBox.isSelected()) {
			Thread.sleep(1000);
			Actions actions = new Actions(driver);
			actions.moveToElement(invoiceCheckBox).click().perform();
		}
	}
	public void selectInvoiceToPay() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Thread.sleep(1000);
		js.executeScript("window.scrollTo(250, 0)");
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		js.executeScript("arguments[0].scrollIntoView();", invoiceCheckBox);
		while (!invoiceCheckBox.isSelected()) {
			Thread.sleep(1000);
			Actions actions = new Actions(driver);
			actions.moveToElement(invoiceCheckBox).click().perform();
		}
		
	}

	public void selectPaymentToRefund() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(250, 0)");
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		js.executeScript("arguments[0].scrollIntoView();", paymentToRefundCheckBox);
		while (!paymentToRefundCheckBox.isSelected()) {
			Thread.sleep(1000);
			Actions actions = new Actions(driver);
			actions.moveToElement(paymentToRefundCheckBox).click().perform();
			
		}
		}

	public void clickOnRefundButton() {
		wait.until(ExpectedConditions.elementToBeClickable(refundButton));
		refundButton.click();
	}

	public void clickOnPayMyInvoiceButton() {
		wait.until(ExpectedConditions.elementToBeClickable(payMyInvoiceButton));
		payMyInvoiceButton.click();
	}

	public void setAmountToPay(String invoiceAmount) {
		amountToPayInput.sendKeys(invoiceAmount);
	}

	public void setCheckReference(String checkReference) {
		fWait.until(ExpectedConditions.visibilityOf(checkReferenceInput));
		checkReferenceInput.sendKeys(checkReference);
	}

	public void clickOnConfirmButton() {
		wait.until(ExpectedConditions.elementToBeClickable(confirmButton));
		confirmButton.click();
	}

	// this method is to get text from text box
	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOf(messageTextBox));
		return messageTextBox.getText();
	}

	public String getCustomerCode() {
		return customerCode.getAttribute("value");
	}

	public String getInvoiceStatusSpanValue() throws InterruptedException {
//		wait.until(ExpectedConditions.visibilityOfAllElements(statusSpan));
//		wait.until(ExpectedConditions.elementToBeSelected(statusSpan));
		return statusSpan.getAttribute("value");
	}

	public String getMachingStatusSpanValue() {
//		wait.until(ExpectedConditions.visibilityOfAllElements(machingStatusSpan));
//		wait.until(ExpectedConditions.elementToBeSelected(machingStatusSpan));
		return machingStatusSpan.getAttribute("value");
	}

	public void setIBAN(String iban) throws InterruptedException {
		// TODO Auto-generated method stub
		ibanInput.sendKeys(iban);
	}

	public void clickOnGoBackButton() {
		clickOn(goBackButton);
	}
	
	public String getSubscriptionColumnLabel() {
		wait.until(ExpectedConditions.visibilityOf(subscriptionColumnLabel));
		return subscriptionColumnLabel.getAttribute("sortby");
	}
	
	public String getInvoiceColumnLabel() {
		wait.until(ExpectedConditions.visibilityOf(invoiceColumnLabel));
		return invoiceColumnLabel.getAttribute("sortby");
	}
	
	public void checkPresenceOfTextInPage (String text) {
		List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + text + "')]"));
		Assert.assertTrue(list.size() > 0);
		//Assert.assertTrue("Text not found!", list.size() > 0);		
	}
	
	public void clickOnBillingTab() {
		clickOn(billingTab);
	}
	
	public void clickOnBillingCycleInput() {
		clickOn(billingCycleInput);
	}
	
	public void setBillingCycle(String value) {
		billingCycleCodeInput.sendKeys(value);
		waitPageLoaded();
		clickOn(driver.findElement(By.xpath("(//table)[2]//tbody//tr[1]//td[1]")));
	}
	
	public void clickOnSaveButton() {
		clickOn(saveButton);
	}
	
	public void clickOnOrderTab() {
		clickOn(orderTab);
	}	
	
	public void clickOnCreateOrder() {
		clickOn(newOrderButton);
	}
}
