package com.opencellsoft.pages.customers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.opencellsoft.base.TestBase;

public class CustomerInvoiceTabPage extends TestBase {
	public CustomerInvoiceTabPage() {
		PageFactory.initElements(driver, this);
	}
		
	@FindBy(how = How.XPATH, using = "//button//span[contains(text(),'New invoice')]")
	WebElement newInvoiceButton;
	
	public void clickOnNewInvoiceButton() {
		clickOn(newInvoiceButton);
	}
}
