package com.opencellsoft.pages.customers;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class NewConsumerPage extends TestBase {

	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public NewConsumerPage(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		this.fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(30)).pollingEvery(Duration.ofSeconds(2)).ignoring(NoSuchElementException.class);
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//input[@id='registrationNo']")
	WebElement companyRegistrationNumber;
	@FindBy(how = How.XPATH, using = "//input[@id='isCompany_true']")
	WebElement consumerCompany;
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement consumerCode;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement consumerName;	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "(//div[contains(@class, 'PageTitle')]//a)[2]")
	WebElement returnToCustomer;
	@FindBy(how = How.ID, using = "name.title")
	WebElement legalFormList;
	@FindBy(how = How.XPATH, using = "//*[@id='vatNo']")
	WebElement numberVAT;
	@FindBy(how = How.XPATH, using = "//*[@id='icdNo']")
	List<WebElement> selectACRS;
	@FindBy(how = How.XPATH, using = "//*[@id = 'name.title']")
	List<WebElement> legalFrom;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Save')]")
	WebElement submitButton;
	@FindBy(how = How.XPATH, using = "//*[@name='registrationNumbers']")
	WebElement companyRegistrationInput;
	@FindBy(how = How.XPATH, using = "//table[1]//tr[1]//td[1]//input")
	WebElement companyRegistrationChechbox;
	@FindBy(how = How.XPATH, using = "//button//span[text() = 'Add']")
	WebElement companyRegistrationAddButton;
	@FindBy(how = How.XPATH, using = "//input[@id = 'wildcardOrIgnoreCase code']")
	WebElement crSearchCodeInput;
	@FindBy(how = How.XPATH, using = "(//div[contains(@class , 'MuiDialog-paper')]//input[@type = 'text'])[2]")
	WebElement registrationNumberInput;
	@FindBy(how = How.XPATH, using = "//button//span[text() = 'Confirm']")
	WebElement confirmButton;
	// This method is to select consumer company 
	public void clickOnConsumerCompany() {
		consumerCompany.click();
	}

	// This method is to set consumer code in the code text box
	public void setConsumerCode(String consumerCode) {
		this.consumerCode.sendKeys(consumerCode);
		waitPageLoaded();
	}

	// This method is to set consumer name in the name text box
	public void setConsumerName(String consumerName) {
		this.consumerName.sendKeys(consumerName);
		waitPageLoaded();
	}

	// This method is to select a legal form
	public void selectLegalForm(String selectLegalForm) {
		clickOn(legalFormList);
		clickOn(legalFormList.findElement(By.xpath("//li[contains(.,'"+selectLegalForm+"')]")));
	} 

	// This method is to set company registration
	public void setCompanyRegistration(String companyRegistrationNumber) {
		this.companyRegistrationNumber.sendKeys(companyRegistrationNumber);
		waitPageLoaded();
	}

	public void selectCompanyRegistration(String companyRegistration) {
		clickOn(companyRegistrationInput);
		crSearchCodeInput.sendKeys(companyRegistration);
		waitPageLoaded();
		clickOn(companyRegistrationChechbox);
		registrationNumberInput.sendKeys(companyRegistration);
		waitPageLoaded();
		clickOn(confirmButton);

	}
	// This method is to set number VAT of a company
	public void setNumberVAT(String numberVAT) {
		this.numberVAT.sendKeys(numberVAT);
		waitPageLoaded();
	}
	// This method is to click on submit button
	public void clickOnSubmitButton() {
		try {
			wait.until(ExpectedConditions.visibilityOf(submitButton));
			submitButton.submit();
		}
		catch(org.openqa.selenium.StaleElementReferenceException ex)
		{
			clickOn(submitButton);
		}		
	}
	public void openApplicableCompanyRegistrationSystemList() {
		try {clickOn(this.selectACRS.get(0));}catch (Exception e) {}
	}

	public void setApplicableCompanyRegistrationSystem(String acrs) {
		try {clickOn(driver.findElement(By.xpath("(//table//tbody//tr/td)[1]")));}catch (Exception e) {}
	}
	
	public void setLegalFrom() {
		try {
			clickOn(this.legalFrom.get(0));
			clickOn(driver.findElement(By.xpath("//ul[contains(@aria-labelledby, 'title')]//li[1]")));
		}catch (Exception e) {}
	}
	// This method is to click on go back button
	public void clickOnGoBackButton() {
		try {
			wait.until(ExpectedConditions.visibilityOf(goBackButton));
			goBackButton.click();
			waitPageLoaded();
		}catch (Exception e) {
			clickOn(goBackButton);
		}
	}

	public void clickOnReturnToCustomer() {
		clickOn(returnToCustomer);
	}

}
