package com.opencellsoft.pages.customers;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.opencellsoft.base.TestBase;

public class NewCustomerPage extends TestBase {

	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;
	JavascriptExecutor js;
	public NewCustomerPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(2)).ignoring(NoSuchElementException.class);
		js = (JavascriptExecutor) driver;
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//input[@id='address.country.code']")
	WebElement addressCountry;
	@FindBy(how = How.XPATH, using = "(//*[normalize-space()='Assoc.'])[1]")
	WebElement assocOptionType;
	@FindBy(how = How.XPATH, using = "//span[@class='MuiTypography-root MuiTypography-body1']//span[contains(text(),'LTD')]")
	WebElement cieOptionType;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//table/tbody/tr[1]")
	WebElement legalEntityType;
	@FindBy(how = How.XPATH, using = "//input[@id='icdNo']")
	WebElement ApplicableCompanyRegistrationSystem;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']//table/tbody/tr[1]")
	WebElement firstChoice;
	@FindBy(how = How.XPATH, using = "//div[@id='billingCycle']")
	WebElement billingCycleList;
	@FindBy(how = How.XPATH, using = "//div[@id='tradingCountry']")
	WebElement billingCountryList;
	@FindBy(how = How.XPATH, using = "//div[@id='tradingLanguage']")
	WebElement billingLanguageList;
	@FindBy(how = How.XPATH, using = "//input[@id='customFields.billin_cf']")
	WebElement billingCF;
	@FindBy(how = How.XPATH, using = "//*[normalize-space()='Billing']")
	WebElement billingInfoSection;
	@FindBy(how = How.XPATH, using = "//*[@id='billingCycle']")
	WebElement billingCycle;
	@FindBy(how = How.XPATH, using = "//li[normalize-space()='Monthly 1st']")
	WebElement billingMonthlyCycle1;
	@FindBy(how = How.XPATH, using = "//input[@id='isCompany_false']")
	WebElement createIndividualCustomer;
	@FindBy(how = How.XPATH, using = "//input[@id='code']")
	WebElement customerCode;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Customer Info')]")
	WebElement customerInfo;
	@FindBy(how = How.XPATH, using = "//input[@id='name.firstName']")
	WebElement customerFirstName;
	@FindBy(how = How.XPATH, using = "//input[@id='name.lastName']")
	WebElement customerLastName;
	@FindBy(how = How.XPATH, using = "//input[@id='contactInformation.email']")
	WebElement customerEmail;
	@FindBy(how = How.XPATH, using = "//input[@id='address.country.code']")
	WebElement customerCountry;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Billing')]")
	WebElement customerBilling;
	@FindBy(how = How.XPATH, using = "//input[@id='customerAccount.preferredPaymentMethod.cardNumber']")
	WebElement cardNum;
	@FindBy(how = How.XPATH, using = "//input[@id='customerAccount.preferredPaymentMethod.yearExpiration']")
	WebElement cardExpirationYear;
	@FindBy(how = How.XPATH, using = "//input[@id='customerAccount.preferredPaymentMethod.monthExpiration']")
	WebElement cardExpirationMonth;
	@FindBy(how = How.XPATH, using = "//*[@id='customer.customerCategory']")
	WebElement clientCategoryInput;
	@FindBy(how = How.XPATH, using = "//*[@id='customer.seller']")
	WebElement sellerList;
	@FindBy(how = How.XPATH, using = "//*[contains(text(), 'MAIN_SELLER')]")
	WebElement sellerOption;	
	@FindBy(how = How.XPATH, using = "//li[normalize-space()='Client']")
	WebElement clientCategoryOption;	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Custom Fields')]")
	WebElement customFieldButton;
	@FindBy(how = How.XPATH, using = "//div[@id='mui-component-select-customFields.TEST_CODE']")
	WebElement codeTestList;
	//	@FindBy(how = How.XPATH, using = "(//button[@type='button'])[39]")
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'New consumer')]")
	WebElement addNewConsumerButton;
	@FindBy(how = How.XPATH, using = "//input[@id='tradingCountry']")
	WebElement countryDropDown;
	@FindBy(how = How.XPATH, using = "//*[normalize-space()='Contact']")
	WebElement contactInfosSection;
	//	@FindBy(how = How.XPATH, using = "//div[@id='customerAccount.preferredPaymentMethod.paymentType']")
	@FindBy(how = How.XPATH, using = "//input[@id='tradingCurrency']")
	WebElement currencyInput;
	//	@FindBy(how = How.XPATH, using = "//span[@title='EUR']")
	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'EUR')])[1]")
	WebElement currencyOption;
	@FindBy(how = How.XPATH, using = "//input[@id='description']")
	WebElement customerName;
	@FindBy(how = How.XPATH, using = "//li[normalize-space()='Paypal']")
	WebElement paypalMethod;
	@FindBy(how = How.XPATH, using = "//li[normalize-space()='Check']")
	WebElement checkMethod;
	@FindBy(how = How.XPATH, using = "//li[normalize-space()='Card']")
	WebElement cardMethod;
	@FindBy(how = How.XPATH, using = "//li[normalize-space()='Direct debit']")
	WebElement directDebitMethod;
	@FindBy(how = How.XPATH, using = "//input[@id='customerAccount.preferredPaymentMethod.bankCoordinates.accountOwner']")
	WebElement accountOwner;
	@FindBy(how = How.XPATH, using = "//input[@id='customerAccount.preferredPaymentMethod.mandateIdentification']")
	WebElement mandateIdentification;
	@FindBy(how = How.XPATH, using = "//input[@id='customerAccount.preferredPaymentMethod.bankCoordinates.iban']")
	WebElement iban;
	@FindBy(how = How.XPATH, using = "//input[@id='customerAccount.preferredPaymentMethod.bankCoordinates.bic']")
	WebElement bic;
	@FindBy(how = How.XPATH, using = "//input[@id='customerAccount.preferredPaymentMethod.mandateDate']")
	WebElement mandateDate;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day MuiPickersDay-current MuiPickersDay-daySelected']")
	WebElement daySelected;
	@FindBy(how = How.XPATH, using = "//div[@id='customerAccount.preferredPaymentMethod.cardType']")
	WebElement cardType;
	@FindBy(how = How.XPATH, using = "//input[@id='contactInformation.email']")
	WebElement emailInput;
	@FindBy(how = How.XPATH, using = "//label[@id='legalEntityType-label']/following-sibling::div")
	//@FindBy(how = How.XPATH, using = "//input[@id='legalEntityType']")
	WebElement entityType;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;
	//@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiTab-root MuiTab-textColorInherit Mui-selected']")
	@FindBy(how = How.XPATH, using = "//*[normalize-space()='General']")
	WebElement generalInfoSection;
	@FindBy(how = How.XPATH, using = "//*[normalize-space()='Group']")
	WebElement groupSection;
	@FindBy(how = How.XPATH, using = "//input[@id='isCompany_true']")
	WebElement isCompanyButton;
	@FindBy(how = How.XPATH, using = "//div[@id='tradingLanguage']")
	WebElement languageDropDown;
	@FindBy(how = How.XPATH, using = "//li[normalize-space()='French' or @data-value='FRA']")
	WebElement languageOption;
	@FindBy(how = How.XPATH, using = "//div[@class='MuiSnackbarContent-message']")
	WebElement messageTextBox;
	@FindBy(how = How.XPATH, using = "//input[@id='customerAccount.preferredPaymentMethod.owner']")
	WebElement ownerCard;
	@FindBy(how = How.XPATH, using = "//*[contains(text(),'Paying account')]")
	WebElement payingAccount;
	@FindBy(how = How.XPATH, using = "//div[@id='customerAccount.tradingCurrency']")
	WebElement payingCurrencyList;
	@FindBy(how = How.XPATH, using = "//div[@id='customer.customerCategory']")
	WebElement payingClientCategoryList;
	@FindBy(how = How.XPATH, using = "//div[@id='customerAccount.preferredPaymentMethod.paymentType']")
	WebElement payingPaymentMethodList;
	@FindBy(how = How.XPATH, using = "//*[normalize-space()='Paying account']")
	WebElement payingAccountSection;
	@FindBy(how = How.XPATH, using = "//div[@id='customerAccount.preferredPaymentMethod.paymentType']")
	WebElement paymentMethodyList;
	@FindBy(how = How.XPATH, using = "//button[@type='submit']")
	WebElement submitButton;
	@FindBy(how = How.XPATH, using = "//input[@id='customerAccount.preferredPaymentMethod.userId']")
	WebElement userID;
	@FindBy(how = How.XPATH, using = "//li[normalize-space()='Visa']")
	WebElement visaCard;
	@FindBy(how = How.XPATH, using = "//input[@name='customerAccount.code']")
	WebElement userPayingAccount;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text jss55 add-filter MuiButton-textPrimary MuiButton-textSizeSmall MuiButton-sizeSmall']")
	WebElement filterPayingAccountCode;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']")
	WebElement payingAccountCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement userPayingAccountCodeInput;
	@FindBy(how = How.XPATH, using = "//tbody//tr[1]")
	WebElement userPayingAccountCodeSelected;
	@FindBy(how = How.XPATH, using = "//input[@name='customer.code']")
	WebElement groupAccount;
	@FindBy(how = How.XPATH, using = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text jss55 add-filter MuiButton-textPrimary MuiButton-textSizeSmall MuiButton-sizeSmall']")
	WebElement filterGroupAccountCode;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']")
	WebElement groupAccountCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement groupAccountCodeInput;
	@FindBy(how = How.XPATH, using = "//tbody//tr[1]")
	WebElement groupAccountCodeSelected;
	@FindBy(how = How.XPATH, using = "//DIV[@id='taxCategory']")
	WebElement taxCategoryList;
	//@FindBy(how = How.XPATH, using = "//ul[@aria-labelledby = 'billingCycle-label']//li[1]")
	@FindBy(how = How.XPATH, using = "(//*[contains(text(), 'Monthly')])[1]")
	WebElement firstBillingCycleOption;
	@FindBy(how = How.XPATH, using = "//input[@id='customerAccount.tradingCurrency']")
	WebElement currencySecondInput;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'EUR')]")
	WebElement currencySecondOption;
	@FindBy(how = How.XPATH, using = "//table[1]//tr[1]//td[1]//input")
	WebElement CompanyRegistrationCheckbox;
	@FindBy(how = How.XPATH, using = "//*[contains(@name, 'registrationNumbers')]")
	WebElement selectCompanyRegistrationInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'wildcardOrIgnoreCase code']")
	WebElement crSearchCodeInput;
	@FindBy(how = How.XPATH, using = "//*[contains(@name, 'CAregistrationNumbers')]")
	WebElement selectCACompanyRegistrationInput;
	@FindBy(how = How.XPATH, using = "//*[contains(@name, 'customer.registrationNumbers')]")
	WebElement selectCustomerCompanyRegistrationInput;
	@FindBy(how = How.XPATH, using = "(//div[contains(@class , 'MuiDialog-paper')]//input[@type = 'text'])[2]")
	WebElement registrationNumberInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'registrationNo']")
	WebElement companyRegistrationInput;
	@FindBy(how = How.XPATH, using = "//button//span[text() = 'Add']")
	WebElement addButton;
	@FindBy(how = How.XPATH, using = "//button//span[text() = 'Confirm']")
	WebElement confirmButton;
	@FindBy(how = How.XPATH, using = "//button//span[text() = 'Cancel']")
	WebElement cancelButton;
	// select type Individual for the Customer to create
	public void clickOnCreateIndividualCustomer() {
		createIndividualCustomer.click();
	}

	// set customer Code
	public void setIndividualCustomerCode(String strCustomerCode) {
		customerCode.sendKeys(strCustomerCode);
	}

	// click on individual costumer infos
	public void clickOnCustomerInfo() {
		customerInfo.click();
	}

	// set First Name
	public void setCustomerFirstName(String strCustomerFirstName) {
		customerFirstName.sendKeys(strCustomerFirstName);
	}

	// set last name
	public void setCustomerLastName(String strCustomerLastName) {
		customerLastName.sendKeys(strCustomerLastName);
	}

	// set Email
	public void setCustomerEmail(String strCustomerEmail) {
		customerEmail.sendKeys(strCustomerEmail);
	}

	// select a country in individual customer Infos
	public void clickOnCustomerCountry(String country) {
		customerCountry.click();
		// new Select(customerCountry).selectByVisibleText(country);
		customerCountry.findElement(By.xpath("//li[contains(.,'" + country + "')]")).click();

	}

	// click on customer billing section
	public void clickOnCustomerBilling() {
		customerBilling.click();
	}

	// click on customer billing cycle
	public void selectBillingCycle(String billingCycleSelected) {
		billingCycleList.click();
		new Select(billingCycleList).selectByVisibleText(billingCycleSelected);
	}

	// select the billing country of the customer
	public void selectBillingCountry(String billingCountrySelected) {
		billingCountryList.click();
		new Select(billingCountryList).selectByVisibleText(billingCountrySelected);
	}

	// select the billing language of the customer
	public void selectBillingLanguage(String billingLanguageSelected) {
		billingLanguageList.click();
		new Select(billingLanguageList).selectByVisibleText(billingLanguageSelected);
	}

	// click on paying account section
	public void clickOnPayingAccount() {
		payingAccount.click();
	}

	// select the currency of the client's paying account
	public void selectPayingAccountCurrency(String payingCurrencySelected) {
		payingCurrencyList.click();
		new Select(payingCurrencyList).selectByVisibleText(payingCurrencySelected);
	}

	// select the category of the client's paying account
	public void selectPayingAccountClientCategory(String payingClientCategorySelected) {
		payingClientCategoryList.click();
		new Select(payingClientCategoryList).selectByVisibleText(payingClientCategorySelected);
	}

	// select the method of the paying account
	public void selectPayingAccountPaymentMethod(String payingPaymentMethodSelected) {
		payingPaymentMethodList.click();
		new Select(payingPaymentMethodList).selectByVisibleText(payingPaymentMethodSelected);
	}

	// set ID
	public void setUserID(String setUserID) {
		wait.until(ExpectedConditions.visibilityOf(userID));
		userID.sendKeys(setUserID);
	}

	// click on custom fields section
	public void clickOnCustomFieldBtn() {
		customFieldButton.click();
	}

	// set billing custom fields
	public void setBillingCF(String setBillingCF) {
		billingCF.click();
		billingCF.sendKeys(setBillingCF);
	}

	// click on save button
	public void clickOnSubmitButton() {
		try {
			submitButton.submit();
		}catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOf(submitButton));
			submitButton.submit();
		}

	}

	// click on go back btn
	public void clickOnGoBackButton() {
		goBackButton.click();
	}

	// create consumer
	public void clickOnCreateConsumerButton() {
		fWait.until(ExpectedConditions.elementToBeClickable(addNewConsumerButton));
		//			addNewConsumerButton.click();
		js.executeScript("arguments[0].click();", addNewConsumerButton);

	}

	// select type company of customer: first step to create a company type customer
	public void selectCustomerCompany() {
		isCompanyButton.click();
	}

	// set Code
	public void setCustomerCode(String code) {
		wait.until(ExpectedConditions.visibilityOf(customerCode));
		customerCode.sendKeys(code);
	}

	// set name
	public void setCustomerName(String name) {
		wait.until(ExpectedConditions.visibilityOf(customerName));
		customerName.sendKeys(name);
	}

	// this method is to fill the general informations of a company customer
	public void clickOnGeneralInfosSection() {
		clickOn(generalInfoSection);
	}

	public void clickOnLegalEntityTypeDropDown() {
		try {
			entityType.click();
		}catch (Exception e) {
			//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", entityType);
			wait.until(ExpectedConditions.elementToBeClickable(entityType));
			entityType.click();		}
		
	}

	public void selectEntityTypeOption() {
		wait.until(ExpectedConditions.elementToBeClickable(cieOptionType));
		cieOptionType.click();
	}

	public void selectLegalEntityType() {
		try {
			clickOn(legalEntityType);
		}catch (Exception e) {
			clickOnLegalEntityTypeDropDown();
			waitPageLoaded();
			clickOn(legalEntityType);
		}
	}
	public void setApplicableCompanyRegistrationSystem() {
			clickOn(ApplicableCompanyRegistrationSystem);
			waitPageLoaded();
			clickOn(firstChoice);
	}
	
	public void clickOnBillingInfoSection() {
		wait.until(ExpectedConditions.elementToBeClickable(billingInfoSection));
		clickOn(billingInfoSection);
	}

	public void clickOnBillingCycleDropdown() {
		wait.until(ExpectedConditions.elementToBeClickable(billingCycle));
		billingCycle.click();
	}

	public void selectBillingCycleOption(String billingCycle) {
		WebElement elem = 
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[normalize-space()='" + billingCycle + "']")));

		elem.click();
	}

	public void selectFirstBillingCycleOption() {
		firstBillingCycleOption.click();
	}
	
	public void selectBillingCycle1(String billingCycle) {
		if(billingCycle != null) {
			driver.findElement(By.xpath("//input[@name = 'wildcardOrIgnoreCase code']")).sendKeys(billingCycle);
			waitPageLoaded();
			try {
				clickOn(driver.findElement(By.xpath("(//*[contains(text(), '" + billingCycle + "')])[1]")));

			}catch (Exception e) {
				driver.findElement(By.xpath("//input[@name = 'wildcardOrIgnoreCase code']")).sendKeys(Keys.CONTROL, "a", billingCycle);
				waitPageLoaded();
				clickOn(driver.findElement(By.xpath("(//*[contains(text(), '" + billingCycle + "')])[1]")));

			}
		}
		else {
			clickOn(driver.findElement(By.xpath("//div[@role = 'dialog']//table//td[2]")));
		}
		
	}
	
	public void clickOnCountryDropDown() {
		wait.until(ExpectedConditions.elementToBeClickable(countryDropDown));
		countryDropDown.click();
	}

	public void clickOnLanguageDropDown() {
		wait.until(ExpectedConditions.elementToBeClickable(languageDropDown));
		languageDropDown.click();
	}

	public void selectLanguageOption() {
		wait.until(ExpectedConditions.elementToBeClickable(languageOption));
		languageOption.click();
	}

	public void selectTaxCategory(String taxCategory) {
		taxCategoryList.click();
		taxCategoryList.findElement(By.xpath("//li[contains(text(),'" + taxCategory + "')]")).click();
	}

	public void selectCustomerCountryOption(String countryOption) {
		countryDropDown.findElement(By.xpath("//span[contains(text(),'"+countryOption+"')]")).click();
	}
	
	public void selectCustomerCountry(String countryOption) {
		try {
			clickOn(countryDropDown.findElement(By.xpath("//span[contains(text(),'"+countryOption+"')]")));
		}
		catch (Exception e) {
			clickOn(driver.findElement(By.xpath("(//table//td)[1]")));
		}
	}

	// this method is to fill contact informations of a company customer
	public void clickOnContactSection() {
		clickOn(contactInfosSection);
	}

	public void fillCustomerEmailInput(String email) {
		emailInput.sendKeys(email);
	}

	// this method is to fill paying informations of a company customer
	public void clickOnPayingAccountSection() {
		try {
			payingAccountSection.click();
		}catch (Exception e) {
			js.executeScript("arguments[0].click();", payingAccountSection);		
			}
		
	}

	public void clickOnCurrencyDropDown() {
		try {
			currencyInput.click();
		}
		catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOf(currencyInput));
			currencyInput.click();
		}

	}

	public void selectCurrencyOption() {
		currencyOption.click();
	}

	public void clickOnPaymentMethodList() {
		try {
			paymentMethodyList.click();
		}
		catch (Exception e) {
			wait.until(ExpectedConditions.elementToBeClickable(paymentMethodyList));
			paymentMethodyList.click();
		}

	}

	// select payment method type card
	public void selectCardPaymentMethod(String ownerName, String cardNumber, String expirationYear,
			String expirationMonth) {
		wait.until(ExpectedConditions.visibilityOf(cardMethod));
		cardMethod.click();
		ownerCard.sendKeys(ownerName);
		cardType.click();
		visaCard.click();
		cardNum.sendKeys(cardNumber);
		cardExpirationYear.sendKeys(expirationYear);
		cardExpirationMonth.sendKeys(expirationMonth);
	}

	// select payment method type DirectDebit
	public void selectDirectDebitPaymentMethod(String accountOwner, String mandateIdentification,String iban, String bic) {
		directDebitMethod.click();
		this.accountOwner.sendKeys(accountOwner);
		this.mandateIdentification.sendKeys(mandateIdentification);
		this.iban.sendKeys(iban);
		this.bic.sendKeys(bic);
		this.mandateDate.click();
		waitPageLoaded();
		this.daySelected.click();
	}

	// select payment method type check
	public void selectCheckPaymentMethod() {
		checkMethod.click();
	}

	// select payment method type paypal
	public void selectPaypalPaymentMethod() {
		wait.until(ExpectedConditions.elementToBeClickable(paypalMethod));
		paypalMethod.click();
	}

	// this method is to fill group informations of a company customer
	public void clickOnGroupSection() {
		groupSection.click();
	}

	public void clickOnSellerDropDown() {
		wait.until(ExpectedConditions.elementToBeClickable(sellerList));
		sellerList.click();
	}

	public void clickOnClientCategoryDropDown() {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", clientCategoryInput);
		wait.until(ExpectedConditions.elementToBeClickable(clientCategoryInput));
		clientCategoryInput.click();
	}

	public void selectClientCategory() {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(clientCategoryOption));
			clientCategoryOption.click();
			waitPageLoaded();
		}catch (Exception e) {
			clickOn(clientCategoryOption);
		}
	}

	public void selectSeller() {
		wait.until(ExpectedConditions.visibilityOf(sellerOption));
		clickOn(sellerOption);
	}

	public String getTextMessage() {
		wait.until(ExpectedConditions.visibilityOfAllElements(messageTextBox));
		return messageTextBox.getText();
	}

	public void groupAccount(String setGroupAccountCode) {
		groupAccount.click();
		filterGroupAccountCode.click();
		groupAccountCode.click();
		groupAccountCodeInput.sendKeys(setGroupAccountCode);
		groupAccountCodeSelected.click();
	}

	public void userPayingAccount(String setUserAccountCode) {
		userPayingAccount.click();
		filterPayingAccountCode.click();
		payingAccountCode.click();
		userPayingAccountCodeInput.sendKeys(setUserAccountCode);
		userPayingAccountCodeSelected.click();
	}

	public void clickOnSecondCurrencyDropDown() {
		currencySecondInput.click();
	}

	public void selectSecondCurrencyOption() {
		currencySecondOption.click();
	}

	public void setCompanyRegistration(String companyRegistration) {
		companyRegistrationInput.sendKeys(companyRegistration);
		waitPageLoaded();
	}
	
	public void selectCompanyRegistration(String companyRegistration) {
		clickOn(selectCompanyRegistrationInput);
		crSearchCodeInput.sendKeys(companyRegistration);
		waitPageLoaded();
		clickOn(CompanyRegistrationCheckbox);
		registrationNumberInput.sendKeys(companyRegistration);
		waitPageLoaded();
//		clickOn(addButton);
		clickOn(confirmButton);
	}
	
	public void selectCACompanyRegistration(String companyRegistration) {
		clickOn(selectCACompanyRegistrationInput);
		crSearchCodeInput.sendKeys(companyRegistration);
		waitPageLoaded();
		registrationNumberInput.sendKeys(companyRegistration);
		waitPageLoaded();
//		clickOn(addButton);
		clickOn(confirmButton);
	}	
	public void selectCustomerCompanyRegistration(String companyRegistration) {
		clickOn(selectCustomerCompanyRegistrationInput);
		crSearchCodeInput.sendKeys(companyRegistration);
		waitPageLoaded();
		registrationNumberInput.sendKeys(companyRegistration);
		waitPageLoaded();
//		clickOn(addButton);
		clickOn(confirmButton);
//		clickOn(cancelButton);
	}
}
