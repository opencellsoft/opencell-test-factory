package com.opencellsoft.pages.customers;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.opencellsoft.base.TestBase;

public class ListOfCustomersPage extends TestBase {

	WebDriver driver;
	Wait<WebDriver> fWait;
	Wait<WebDriver> wait;

	public ListOfCustomersPage(WebDriver driver) {
		this.driver = driver;
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(30))
				.pollingEvery(Duration.ofSeconds(2)).ignoring(NoSuchElementException.class);
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
	}

	// Using FindBy for locating elements
	@FindBy(how = How.XPATH, using = "//*[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-textSizeSmall MuiButton-sizeSmall'  or @id = 'CREATE']")
	WebElement createCustomerButton;
	@FindBy(how = How.XPATH, using = "(//button[@aria-label='Add filter'])[1]")
	WebElement filterButton;
	@FindBy(how = How.XPATH, using = "//li[@role='menuitem']//span[contains(text(),'Customer Code')]")
	WebElement filterByCustomerCode;
	@FindBy(how = How.XPATH, using = "//input[@id='wildcardOrIgnoreCase code']")
	WebElement customerCode;
	@FindBy(how = How.XPATH, using = "(//table//tbody//td)[1]")
	WebElement customer;
	@FindBy(how = How.XPATH, using = "//input[@id='searchBar']")
	WebElement searchBar;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Next')]")
	WebElement nextButton;

	// This method to click on Create Customer Button
	public void clickOnCreateCustomerButton() {
		try {
			clickOn(createCustomerButton);
		}catch (Exception e) {
			wait.until(ExpectedConditions.elementToBeClickable(createCustomerButton));
			createCustomerButton.click();
		}
	}

	// click on the filter button to add a filter
	public void clickOnFilterButton() {
		wait.until(ExpectedConditions.elementToBeClickable(filterButton));
		filterButton.click();
	}

	// select a filter by customer code
	public void clickOnFilterByCustomerCode() {
		filterByCustomerCode.click();
	}

	// select the searched for customer
	public void clickCustomer(String customerCode) {
		try {
			clickOn(customer);	
		}catch (Exception e) {
			clickOnCustomer(customerCode);
		}
	}

	public boolean customerIsDisplayed(String Code) {
		List<WebElement> elem = driver.findElements(By.xpath("//mark[contains(text(),'" + Code + "')]"));
		if( elem.isEmpty()) {
			return false;
		}else {return true;}
	}
	
	// write the customer code in the search input filter
	public void setCustomerCode(String strCustomerCode) {
		wait.until(ExpectedConditions.visibilityOf(customerCode));
		customerCode.sendKeys(strCustomerCode);
	}
	
	// write the customer code in the search bar filter
	public void setSearchBarText(String text) {
		try {
			searchBar.sendKeys(Keys.chord(Keys.CONTROL, "a"),text);
		}catch (Exception e) {
			wait.until(ExpectedConditions.visibilityOf(searchBar));
			searchBar.sendKeys(Keys.chord(Keys.CONTROL, "a"),text);
		}
	}
	
	// select the searched for customer
	public void clickOnCustomer(String customerCode) {
		WebElement elem = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//mark[contains(text(),'" + customerCode + "')]")));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", elem);	
		waitPageLoaded();
	}

	public void checkPagination () {
		wait.until(ExpectedConditions.elementToBeClickable(nextButton));
		this.nextButton.click();		
	}

	public void checkBreadCrumb (String text) {
		List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + text + "')]"));
		Assert.assertTrue(list.size() > 0);
	}
	
	public void checkPresenceTextPage (String text) {
		List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + text + "')]"));
		Assert.assertTrue(list.size() > 0);
	}
}