package com.opencellsoft.pages.customers;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewPaymentMethodPage {

	WebDriver driver;
	WebDriverWait wait;
	Wait<WebDriver> fWait;

	public NewPaymentMethodPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver,Duration.ofSeconds(10));
		fWait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(10))
				.pollingEvery(Duration.ofSeconds(30)).ignoring(NoSuchElementException.class);

	}

//	@FindBy(how = How.XPATH, using = "//input[@name='paymentType']")
	@FindBy(how = How.XPATH, using = "//div[@id='paymentType']")
	WebElement paymentTypeInput;
	@FindBy(how = How.XPATH, using = "//div[@id='paymentType']")
	WebElement paymentTypeDiv;
	@FindBy(how = How.XPATH, using = "//div[@id='cardType']")
	WebElement cardTypeInput;
	@FindBy(how = How.XPATH, using = "//input[@id='cardNumber']")
	WebElement cardNumberInput;
	@FindBy(how = How.XPATH, using = "//input[@id='owner']")
	WebElement cardOwnerInput;
	@FindBy(how = How.XPATH, using = "//input[@id='validThru']")
	WebElement validThruInput;
	@FindBy(how = How.XPATH, using = "//span[normalize-space()='Save']")
	WebElement saveButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;
	@FindBy(how = How.XPATH, using = "//input[@id='bankCoordinates.iban']")
	WebElement ibanInput;
	@FindBy(how = How.XPATH, using = "//input[@id='bankCoordinates.accountOwner']")
	WebElement accountOwner;
	@FindBy(how = How.XPATH, using = "//input[@id='mandateIdentification']")
	WebElement referenceMandate;
	@FindBy(how = How.XPATH, using = "//input[@id='mandateDate']")
	WebElement startDate;
	@FindBy(how = How.XPATH, using = "//input[@id='bankCoordinates.bic']")
	WebElement bicInput;
	@FindBy(how = How.XPATH, using = "//input[@id='preferred']")
	WebElement preferred;
	@FindBy(how = How.XPATH, using = "(//BUTTON[@class='MuiButtonBase-root MuiIconButton-root MuiPickersDay-day'])[1]")
	WebElement mandateDate;

	// this method is to click on payment type input
	public void clickOnPayementTypeInput() {
		fWait.until(ExpectedConditions.elementToBeClickable(paymentTypeInput));
		Actions builder = new Actions(driver);
		builder.moveToElement(paymentTypeInput).click(paymentTypeInput);
		builder.perform();
//		fWait.until(ExpectedConditions.elementToBeClickable(paymentTypeDiv));
//		paymentTypeDiv.click();
	}

	// this method is to select a payment method
	public void selectPaymentType(String paymentType) throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(paymentTypeDiv));
		Actions actions = new Actions(driver);
		//actions.moveToElement(paymentTypeDiv).click().build().perform();
		try {
			paymentTypeDiv.click();
			paymentTypeDiv.findElement(By.xpath("//li[contains(text(),'" + paymentType + "')]")).click();
		} catch (org.openqa.selenium.StaleElementReferenceException ex) {
			paymentTypeDiv.click();
			paymentTypeDiv.findElement(By.xpath("//li[contains(text(),'" + paymentType + "')]")).click();
		}		
	}

	// this method is to set a card type
	public void setCardType(String cardType) {
		cardTypeInput.click();
		cardTypeInput.findElement(By.xpath("//li[normalize-space()='" + cardType + "']")).click();
	}

	public void setCardNumber(String cardNumber) {
		// TODO Auto-generated method stub
		fWait.until(ExpectedConditions.visibilityOf(cardNumberInput));
		cardNumberInput.sendKeys(cardNumber);
	}

	public void setCardOwner(String cardOwner) {
		// TODO Auto-generated method stub
		cardOwnerInput.sendKeys(cardOwner);
	}

	// this method is to set the card validity month and year
	public void setValidThru(String validityDate) {
		fWait.until(ExpectedConditions.visibilityOf(validThruInput));
		validThruInput.sendKeys(validityDate);
	}

	// this method is to click on the button save
	public void clickOnSaveButton() {
		// TODO Auto-generated method stub
		wait.until(ExpectedConditions.elementToBeClickable(saveButton));
		saveButton.click();
	}

	// this method is to click on the Go Back Button
	public void clickOnGoBackButton() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(goBackButton));
		goBackButton.click();
		Actions actions = new Actions(driver);
		//actions.moveToElement(goBackButton).click().build().perform();
		//JavascriptExecutor js = (JavascriptExecutor)driver;
		//js.executeScript("arguments[0].click();", goBackButton);
	}

	public void setIBAN(String iban) {
		this.ibanInput.sendKeys(iban);
	}

	public void setAccountOwner(String accountOwner) {
		this.accountOwner.sendKeys(accountOwner);
	}

	public void setMandateDate() {
		wait.until(ExpectedConditions.elementToBeClickable(startDate));
		startDate.click();
		wait.until(ExpectedConditions.elementToBeClickable(mandateDate));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", mandateDate);
	}

	public void setReferenceMandate(String referenceMandate) {
		this.referenceMandate.sendKeys(referenceMandate);
	}

	public void setBic(String bic) {
		bicInput.sendKeys(bic);
	}

	public void setPreferred() {
		// TODO Auto-generated method stub
		preferred.click();
	}

}
