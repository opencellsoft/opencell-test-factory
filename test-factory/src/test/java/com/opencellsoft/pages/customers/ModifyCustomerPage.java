package com.opencellsoft.pages.customers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

public class ModifyCustomerPage {

	WebDriver driver;

	public ModifyCustomerPage(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Customer Info')]")
	WebElement customerInfo;
	@FindBy(how = How.XPATH, using = "//input[@id='name.firstName']")
	WebElement customerFirstName;
	@FindBy(how = How.XPATH, using = "//input[@id='name.lastName']")
	WebElement customerLastName;
	@FindBy(how = How.XPATH, using = "//input[@id='address.country.code']")
	WebElement customerCountry;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Billing')]")
	WebElement customerBilling;
	@FindBy(how = How.XPATH, using = "//div[@id='billingCycle']")
	WebElement billingCycleList;
	@FindBy(how = How.XPATH, using = "//div[@id='tradingCountry']")
	WebElement billingCountryList;
	@FindBy(how = How.XPATH, using = "//div[@id='tradingLanguage']")
	WebElement billingLanguageList;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Paying account')]")
	WebElement payingAccount;
	@FindBy(how = How.XPATH, using = "//div[@id='customerAccount.tradingCurrency']")
	WebElement payingCurrencyList;
	@FindBy(how = How.XPATH, using = "//div[@id='customer.customerCategory']")
	WebElement payingClientCategoryList;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Create')]")
	WebElement createPaymentMethods;
	@FindBy(how = How.XPATH, using = "//div[@id='paymentType']")
	WebElement paymentTypeList;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Save')]")
	WebElement submitButton;
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Go Back')]")
	WebElement goBackButton;

	// click on customer infos section
	public void clickOnCustomerInfo() {
		customerInfo.click();
	}

	// write customer firstName
	public void setCustomerFirstName(String strCustomerFirstName) {
		customerFirstName.clear();
		customerFirstName.sendKeys(strCustomerFirstName);
	}

	// writecustomer last name
	public void setCustomerLastName(String strCustomerLastName) {
		customerLastName.clear();
		customerLastName.sendKeys(strCustomerLastName);
	}
	// select the customer country

	public void clickOnCustomerCountry(String country) {
		customerCountry.click();
		customerCountry.click();
		new Select(customerCountry).selectByVisibleText(country);

	}

	// click on customer billing section
	public void clickOnCustomerBilling() {
		customerBilling.click();
	}

	// select billing Cycle
	public void selectBillingCycle(String billingCycleSelected) {
		billingCycleList.click();
		new Select(billingCycleList).selectByVisibleText(billingCycleSelected);	
	}

	// select billing country
	public void selectBillingCountry(String billingCountrySelected) {
		billingCountryList.click();
		new Select(billingCountryList).selectByVisibleText(billingCountrySelected);		
	}

	// select billing language
	public void selectBillingLanguage(String billingLanguageSelected) {
		billingLanguageList.click();
		new Select(billingLanguageList).selectByVisibleText(billingLanguageSelected);		
	}

	// click on customer paying account section
	public void clickOnPayingAccount() {
		payingAccount.click();
	}

	//select paying currency
	public void selectPayingAccountCurrency(String payingCurrencySelected) {
		payingCurrencyList.click();
		new Select(payingCurrencyList).selectByVisibleText(payingCurrencySelected);		
	}

	// select paying client category
	public void selectPayingAccountClientCategory(String payingClientCategorySelected) {
		payingClientCategoryList.click();
		new Select(payingClientCategoryList).selectByVisibleText(payingClientCategorySelected);		
	}

	// select payment method
	public void clickOnCreatePaymentMethods() {
		createPaymentMethods.click();
	}

	// select payment type
	public void clickOnPaymentType(String paymentTypeSelected) {
		paymentTypeList.click();
		new Select(paymentTypeList).selectByVisibleText(paymentTypeSelected);		
	}
}