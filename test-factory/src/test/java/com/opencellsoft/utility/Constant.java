package com.opencellsoft.utility;

import java.util.Locale;

import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;

public class Constant {
	static FakeValuesService fakeValuesService = new FakeValuesService(new Locale("en-GB"), new RandomService());
	static Faker faker = new Faker(new Locale("en-US"));
	
	
	//Product data sets
	//public static final String productCode = fakeValuesService.regexify("PD[0-9]{5}");
	public static final String productCode = faker.bothify("???#####", true);
	public static final String productDescription = faker.commerce().productName();
	public static final String productExternalDescription = faker.commerce().productName();
	public static final String productCode2 = faker.bothify("???####", true);
	public static final String productDescription2 = faker.commerce().productName();
	
	//Offer data sets
	//public static final String offerCode = fakeValuesService.regexify("OFR[0-9]{5}");
	public static final String offerCode = faker.bothify("???####", true);
	public static final String offerDescription = faker.commerce().productName();
	public static final String newOfferDescription = faker.commerce().productName();
	public static final String newOfferCode = fakeValuesService.regexify("OFR[0-9]{5}");
	
	//Customer data sets
	public static final String customerCode = faker.bothify("???####", true);
	public static final String customerName = faker.company().name();
	public static final String individualCustomerCode = faker.bothify("???####", true);
	public static final String customerEmail = faker.internet().emailAddress();
	public static final String customerFirstName = faker.name().firstName();
	public static final String customerLastName = faker.name().lastName();
	public static final String paypalUserId = faker.code().isbn10();
	public static final String consumerCode = faker.bothify("???####", true);
	public static final String consumerName = faker.company().name();
	public static final String companyRegistrationNumber = faker.code().isbn10();
	public static final String vatNumber = faker.code().isbn10();	
	public static final String cardOwner = faker.name().fullName();
	public static final String cardNumber = faker.finance().creditCard();
	public static final String IBAN = faker.finance().iban();
	public static final String BIC  = faker.finance().bic();
	public static final String accountOwner = faker.name().fullName();
	public static final String referenceMandate = faker.code().isbnRegistrant();
	
	//Charges data sets
	public static final String oneShotChargeCode = faker.bothify("???#####", true);
	public static final String oneShotChargeDescription = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String recurringChargeCode = faker.bothify("???#####", true);
	public static final String chargeDescriptionRecurring = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String usageChargeCode = faker.bothify("???#####", true);
	public static final String chargeDescriptionUsage = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String pricePlanVersionCode = faker.bothify("???#####", true);
	public static final String pricePlanMatrixVersionCode = faker.bothify("???#####", true);
	public static final String gridLineDescription1 = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String gridLineDescription2 = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");

	//Attributes data sets
	public static final String textAttributeCode = fakeValuesService.regexify("TXT[0-9]{5}");
	public static final String textAttributeDescription = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String ListOfTextValuesAttributeCode = fakeValuesService.regexify("LT[0-9]{5}");
	public static final String ListOfTextValuesAttributeDescription = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String numericAttributeCode = faker.bothify("???####", true);
	public static final String numericAttributeDescription = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String integerAttributeCode = faker.bothify("???####", true);
	public static final String integerAttributeDescription = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String dateAttributeCode = faker.bothify("???####", true);;
	public static final String dateAttributeDescription = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String emailAttributeCode = faker.bothify("???####", true);
	public static final String emailAttributeDescription = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String phoneAttributeCode = faker.bothify("???####", true);
	public static final String phoneAttributeDescription = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String booleanAttributeCode = faker.bothify("???####", true);
	public static final String booleanAttributeDescription = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String informationAttributeCode = faker.bothify("???####", true);
	public static final String informationAttributeDescription = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String eLAttributeCode = faker.bothify("???####", true);
	public static final String eLAttributeDescription = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	
	//Invoicing plans data sets
	public static final String invoicingPlanDescription = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String invoicingPlanName = faker.bothify("???####", true);
	public static final String invoicingPlanLine1Code = faker.bothify("???####", true);
	public static final String invoicingPlanLine2Code = faker.bothify("???####", true);
	public static final String invoicingPlanLine3Code = faker.bothify("???####", true);
	
	//Subscription data sets
	public static final String subscriptionCode = faker.bothify("???####", true);
	
	//Access Points data sets
	public static final String accessPointCode = faker.bothify("???####", true);

	//Media data sets
	public static final String mediaCode = faker.bothify("???####", true);
	public static final String mediaDescription = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String urlPathMedia = faker.letterify(
			"https://cdn.futura-sciences.com/buildsv6/images/mediumoriginal/6/5/2/652a7adb1b_98148_01-intro-773.jpg");
	//https://www.capitalgrandest.eu/wp-content/uploads/2020/01/opencell_log-1.jpg
	//Tag data sets
	public static final String tagCode = faker.app().name();
	public static final String tagName = faker.company().buzzword();
	public static final String tagCategoryCode = faker.bothify("???####", true);
	public static final String tagCategoryName = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");

	//Commercial rules data sets
	public static final String commercialRuleCode = faker.bothify("???####", true);
	public static final String commercialRuleDescription = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String productFamilyCode = fakeValuesService.regexify("PF[0-9]{5}");
	public static final String productFamilyDescription = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String productFamilyName = faker.commerce().productName();
	
	//Quote data sets	
	public static final String quoteCode = faker.bothify("???####", true);
	
	//Invoice data sets
	public static final String invoiceLineDescription = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");

	//Article data sets
	public static final String articleCode = faker.bothify("???####", true);
	public static final String articleDescription = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String articleLabelByLanguage = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String articleMappingLineCode = faker.bothify("???####", true);
	public static final String articleMappingLineDescription = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String articleMappingLineCode1 = faker.bothify("???####", true);
	public static final String articleMappingLineDescription1 = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String articleMappingLineCode2 = faker.bothify("???####", true);
	public static final String articleMappingLineDescription2 = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String newArticleLabelByLanguage = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");

	//Discount data sets
	public static final String discountLineCode = faker.bothify("???####", true);
	public static final String discountPlanCode = faker.bothify("???####", true);
	public static final String newDiscountLineCode = faker.bothify("???####", true);
	public static final String discountPlanLabel = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	
	//Payment data sets
	public static final String checkReference = faker.code().isbn13();
	
	//Contract data sets
	
	public static final String contractCode=faker.code().isbn13();
	public static final String contractDescription=faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String contractLineCode=faker.code().isbn13();
	public static final String contractLineDescription=faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String DiscountContractLineCode = faker.code().isbn13();
	public static final String ContractLinePriceGridCode = faker.code().isbn13();
	
	//Trigger EDR data sets
	public static final String triggerEdrCode =faker.bothify("TRG_EDR_???####", true);
	public static final String triggerEdrDescription =faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
	public static final String param3EL =faker.bothify("BB_####", true);
	public static final String param3 =faker.bothify("AA_####", true);	
	
	//  Security Deposit data sets
	public static final String securityDepositName = faker.bothify("SD_???####", true); ;
	public static final String securityDepositDescription = faker.lorem().fixedString(18).replaceAll("[^a-zA-Z0-9\\s]|\\s+$", "");
}
