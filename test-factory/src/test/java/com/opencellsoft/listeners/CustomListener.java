package com.opencellsoft.listeners;
import java.util.List;

import org.testng.IAlterSuiteListener;
import org.testng.xml.XmlSuite;

// Custom Listener implementing IAlterSuiteListener
public class CustomListener implements IAlterSuiteListener {    
	@Override    
	public void alter(List<XmlSuite> suites) {        
		// Modify the TestNG suite at runtime for dynamic parallel execution        
		for (XmlSuite suite: suites) {            
			// Modify parallel execution settings            
			suite.setParallel(XmlSuite.ParallelMode.TESTS);            
			suite.setThreadCount(3); // Specify the desired number of threads

        }

    }

}