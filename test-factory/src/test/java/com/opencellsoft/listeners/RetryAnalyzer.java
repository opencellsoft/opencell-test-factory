package com.opencellsoft.listeners;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryAnalyzer implements IRetryAnalyzer {
	private int retryCount = 1;
	private int maxRetryCount = 1;

	@Override
	public boolean retry(ITestResult result) {

		boolean iFlag = false;
		String resultString = result.getThrowable().toString();

		// Checking for specific reason of failure
		if (resultString.contains("NoSuchElementException") || resultString.contains("TimeoutException")) {

			if (retryCount < maxRetryCount) {
				System.out.println("Retrying " + result.getName() + " test for the " + (retryCount + 1) + " time(s).");
				retryCount++;
				iFlag = true;
			}

		} else {
			// making retryCount and maxRetryCount equal
			retryCount = 0;
			maxRetryCount = 0;
			iFlag = false;
		}

		return iFlag;
	}

	public boolean isOnFirstRun() {
		return retryCount == 0;
	}
}