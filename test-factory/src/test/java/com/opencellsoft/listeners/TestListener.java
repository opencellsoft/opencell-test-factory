package com.opencellsoft.listeners;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import org.testng.IRetryAnalyzer;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.Status;
import com.opencellsoft.report.ExtentManager;
import com.opencellsoft.report.ExtentTestManager;
import com.opencellsoft.utility.TestUtil;
import org.testng.IAnnotationTransformer;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.ITestAnnotation;
import org.testng.asserts.Assertion;

public class TestListener implements IAnnotationTransformer, ITestListener {

	private static final Logger log = LogManager.getLogger(TestListener.class);
	WebDriver driver;
    // ANSI color codes
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    // ANSI background color codes
    public static final String ANSI_BG_BLUE = "\u001B[44m";

	@Override
	// @SuppressWarnings({ "rawtypes" })
	public void transform(ITestAnnotation annotation, Class testClass, Constructor constructor, Method method) {
		Class<? extends IRetryAnalyzer> retry = annotation.getRetryAnalyzerClass();

		if (retry == null) {
			annotation.setRetryAnalyzer(RetryFailed.class);
		}
	}

	public void onStart(ITestContext context) {
		System.out.println("*** Test Suite " + context.getName() + " started ***" );
	}

	public void onFinish(ITestContext context) {
		System.out.println(("*** Test Suite " + context.getName() + " ending ***"));
		ExtentTestManager.endTest();
		ExtentManager.getInstance().flush();
	}

	public void onTestStart(ITestResult result) {
		System.out.println(("*** Running test method " + result.getMethod().getMethodName() + "..."));
		ExtentTestManager.startTest(result.getMethod().getMethodName());
	}

	public void onTestSuccess(ITestResult result) {
		System.out.println(ANSI_GREEN  + ANSI_BG_BLUE  +"*** Executed " + result.getMethod().getMethodName() + " test successfully..."+ANSI_RESET);
		ExtentTestManager.getTest().log(Status.PASS, "Test passed");
	}

	public void onTestFailure(ITestResult result) {
		System.out.println(ANSI_RED  + ANSI_BG_BLUE + "*** Test execution " + result.getMethod().getMethodName() + " failed..." + ANSI_RESET);
		System.out.println((result.getMethod().getMethodName() + " failed!"));

		try {
			ExtentTestManager.getTest().log(Status.FAIL, "Failed Case is: " + result.getName());
			ExtentTestManager.getTest().addScreenCaptureFromPath(TestUtil.captureScreenshot(result.getName()));
			ExtentTestManager.getTest().log(Status.FAIL,
					result.getName() + " FAIL with error " + result.getThrowable());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ExtentManager.createInstance().flush();
		
		// stop execution
//        new Assertion().fail("Aborting");
	}

	public void onTestSkipped(ITestResult result) {
		System.out.println("*** Test " + result.getMethod().getMethodName() + " skipped...");
		ExtentTestManager.getTest().log(Status.SKIP, "Test Skipped");
		// stop execution
//        new Assertion().fail("Aborting");
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		System.out.println("*** Test failed but within percentage % " + result.getMethod().getMethodName());
	}

}