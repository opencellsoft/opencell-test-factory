package com.alpiq.listeners;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import org.testng.IRetryAnalyzer;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.alpiq.utility.AlpiqTestUtil;
import com.aventstack.extentreports.Status;
import com.opencellsoft.report.ExtentManager;
import com.opencellsoft.report.ExtentTestManager;
import org.testng.IAnnotationTransformer;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.ITestAnnotation;

public class AlpiqTestListener implements IAnnotationTransformer, ITestListener {

	private static final Logger log = LogManager.getLogger(AlpiqTestListener.class);
	WebDriver driver;

	@Override
	// @SuppressWarnings({ "rawtypes" })
	public void transform(ITestAnnotation annotation, Class testClass, Constructor constructor, Method method) {
		Class<? extends IRetryAnalyzer> retry = annotation.getRetryAnalyzerClass();

		if (retry == null) {
			annotation.setRetryAnalyzer(RetryFailed.class);
		}
	}

	public void onStart(ITestContext context) {
		System.out.println("*** Test Suite " + context.getName() + " started ***");
	}

	public void onFinish(ITestContext context) {
		System.out.println(("*** Test Suite " + context.getName() + " ending ***"));
		ExtentTestManager.endTest();
		ExtentManager.getInstance().flush();
	}

	public void onTestStart(ITestResult result) {
		System.out.println(("*** Running test method " + result.getMethod().getMethodName() + "..."));
		ExtentTestManager.startTest(result.getMethod().getMethodName());
	}

	public void onTestSuccess(ITestResult result) {
		System.out.println("*** Executed " + result.getMethod().getMethodName() + " test successfully...");
		ExtentTestManager.getTest().log(Status.PASS, "Test passed");
	}

	public void onTestFailure(ITestResult result) {
		System.out.println("*** Test execution " + result.getMethod().getMethodName() + " failed...");
		System.out.println((result.getMethod().getMethodName() + " failed!"));

		try {
			ExtentTestManager.getTest().log(Status.FAIL, "Failed Case is: " + result.getName());
			ExtentTestManager.getTest().addScreenCaptureFromPath(AlpiqTestUtil.captureScreenshot(result.getName()));
			ExtentTestManager.getTest().log(Status.FAIL,
					result.getName() + " FAIL with error " + result.getThrowable());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ExtentManager.createInstance().flush();
	}

	public void onTestSkipped(ITestResult result) {
		System.out.println("*** Test " + result.getMethod().getMethodName() + " skipped...");
		ExtentTestManager.getTest().log(Status.SKIP, "Test Skipped");
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		System.out.println("*** Test failed but within percentage % " + result.getMethod().getMethodName());
	}

}