package com.alpiq.tests;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import com.alpiq.base.TestBase;
import com.alpiq.pages.ClientPage;
import com.alpiq.utility.AlpiqTestUtility;

public class ClientTests extends TestBase {
	
	public void creerCompteClient() throws InterruptedException, IOException {
		
		ClientPage clientPage = new ClientPage();
		AlpiqTestUtility utility = new AlpiqTestUtility();
		String password = utility.generatePassword();
		clientPage.setMotDePasse(password);
		waitPageLoaded();
		clientPage.setRepeterMotDePasse(password);
		waitPageLoaded();
		clientPage.clickOnValider();
		writeReport();
		Thread.sleep(6000);
		Boolean result;
		result = clientPage.verifierEmailMsg();	
		assertTrue(result);
	}
	
}
