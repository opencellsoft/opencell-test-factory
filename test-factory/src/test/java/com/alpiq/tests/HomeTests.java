package com.alpiq.tests;

import java.io.IOException;

import com.alpiq.base.TestBase;
import com.alpiq.pages.HomePage;

public class HomeTests extends TestBase {
	
	public void changerFournisseur() throws InterruptedException, IOException {
		
		HomePage homePage = new HomePage();
		Thread.sleep(5000);
		homePage.clickOnSwitchingSupplierButton();
		writeReport();
		Thread.sleep(5000);
		homePage.clickOnAccepteButton();
	}
}
