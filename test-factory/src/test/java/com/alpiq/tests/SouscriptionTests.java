package com.alpiq.tests;

import java.io.IOException;

import com.alpiq.base.TestBase;
import com.alpiq.pages.SouscriptionPage;

public class SouscriptionTests extends TestBase {

	public void souscriptionEtape1(String nom,String pdl ) throws InterruptedException, IOException {
		SouscriptionPage souscriptionPage = new SouscriptionPage();
		souscriptionPage.clickOnPDLLink();
		// Je connais mon numéro de PDL
		souscriptionPage.setNomTitulaire2(nom);
		souscriptionPage.setPdl(pdl);
		souscriptionPage.setConsentement();
		souscriptionPage.clickOnContinuerButton2();
		writeReport();
		Thread.sleep(5000);
		
	}

	public Boolean verifSouscriptionExisteDeja() {
		SouscriptionPage souscriptionPage = new SouscriptionPage();
		if(souscriptionPage.alertMSG() == 0) {
			return false;
		}else {
			return true;
		}

	}
	
	public Boolean verifEmailExisteDeja() {
		SouscriptionPage souscriptionPage = new SouscriptionPage();
		if(souscriptionPage.alertEmailExisteDeja() == 0) {
			return false;
		}else {
			return true;
		}

	}
	
	public void souscriptionEtape2(String electriciteRenouvelable, String origine, String sourceEnergie) throws InterruptedException, IOException {
		SouscriptionPage souscriptionPage = new SouscriptionPage();

		// Je définis mon offre
//		souscriptionPage.setElectriciteRenouvelable(electriciteRenouvelable);
//		souscriptionPage.setOrigine(origine);
//		souscriptionPage.sourceEnergie(sourceEnergie);
		souscriptionPage.clickOnContinuerButton3();
		writeReport();
		Thread.sleep(5000);
	}

	public void souscriptionEtape3() throws InterruptedException, IOException {
		SouscriptionPage souscriptionPage = new SouscriptionPage();

		// J'optimise mon offre
		souscriptionPage.clickOnContinuerButton1();
		writeReport();
		Thread.sleep(5000);
	}

	public void souscriptionEtape4(String modeFacturation) throws InterruptedException, IOException {
		SouscriptionPage souscriptionPage = new SouscriptionPage();

		// Je choisis mon mode de facturation
//		try {
//			souscriptionPage.modeFacturation(modeFacturation);
//		}catch (Exception e) {
//			souscriptionPage.clickOnContinuerButton1();
//			souscriptionPage.modeFacturation(modeFacturation);
//		}
		
		souscriptionPage.clickOnContinuerButton1();
		writeReport();
		Thread.sleep(5000);
	}

	public void souscriptionEtape5(String identite,String prenom, String email, String phone, String accepterRecevoirEmail, String utilisationPhone ) throws InterruptedException, IOException {
		SouscriptionPage souscriptionPage = new SouscriptionPage();

		// Je saisis mes coordonnées
		try {
			souscriptionPage.setIdentite(identite);
		}catch (Exception e) {
			souscriptionPage.clickOnContinuerButton1();
			souscriptionPage.setIdentite(identite);
		}
		
		souscriptionPage.setPrenom(prenom);
		souscriptionPage.setEmail(email);
		souscriptionPage.setPhone(phone);
		souscriptionPage.setAcceptationRecevoirEmail(accepterRecevoirEmail);
		souscriptionPage.SetUtilisationPhone(utilisationPhone);
		souscriptionPage.clickOnContinuerButton1();
		writeReport();
		Thread.sleep(5000);
	}

	public void souscriptionEtape6(String iban, String pa, String cgv) throws InterruptedException, IOException {
		SouscriptionPage souscriptionPage = new SouscriptionPage();
		// Je finalise ma souscription
		try {
			souscriptionPage.setDateDebut();
		}catch (Exception e) {
			souscriptionPage.clickOnContinuerButton1();
			souscriptionPage.setDateDebut();
		}
		
		souscriptionPage.setIban(iban);
		souscriptionPage.setPrelevementsAutomatiques(pa);
		souscriptionPage.setCGV(cgv);
		souscriptionPage.clickOnSouscrirButton();
		writeReport();
		Thread.sleep(5000);

	}

	public void souscriptionEtape7() throws InterruptedException, IOException {
		SouscriptionPage souscriptionPage = new SouscriptionPage();
		// Créez dès à présent votre compte personnel Alpiq
		souscriptionPage.clickOnCreerEspaceClientButton();
		writeReport();
		Thread.sleep(5000);
	}

}
