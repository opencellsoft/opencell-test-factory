package com.alpiq.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.alpiq.base.TestBase;

public class AlpiqTestUtility extends TestBase {

	public String generatePassword() {
		
		String LOWERCASE = "abcdefghijklmnopqrstuvwxyz";
		String DIGITS = "0123456789";
		String SPECIAL_CHARACTERS = "&@";
		SecureRandom random = new SecureRandom();

		// StringBuilder to build the password
		StringBuilder password = new StringBuilder();

		// Ensure at least one character from each type
		password.append(getRandomChar(LOWERCASE, random));
		password.append(getRandomChar(LOWERCASE, random));
		password.append(getRandomChar(LOWERCASE, random));
		password.append(getRandomChar(LOWERCASE, random));
		password.append(getRandomChar(SPECIAL_CHARACTERS, random));
		password.append(getRandomChar(DIGITS, random));
		password.append(getRandomChar(DIGITS, random));
		password.append(getRandomChar(DIGITS, random));
		password.append(getRandomChar(DIGITS, random));


		// Fill the rest of the password with random characters
		// Concatenate all character types
//		String allCharacters = LOWERCASE + DIGITS + SPECIAL_CHARACTERS;
//		for (int i = 4; i < 8; i++) {
//			password.append(allCharacters.charAt(random.nextInt(allCharacters.length())));
//		}

//		return password.toString();
		return "1234@abcd";
	}

	private static char getRandomChar(String characters, SecureRandom random) {
		return characters.charAt(random.nextInt(characters.length()));
	}

	public Object[][] readExcelData(String filePath, String sheetName) throws IOException{
		// Load the Excel file
		FileInputStream inputStream = new FileInputStream(new File(filePath));
		//        Workbook workbook = WorkbookFactory.create(inputStream);
		// Select the sheet
		try (XSSFWorkbook workbook = new XSSFWorkbook(inputStream)) {
			Sheet sheet = workbook.getSheet(sheetName);

			int rowCount = sheet.getLastRowNum();
			int colCount = sheet.getRow(0).getLastCellNum();
			Object[][] Data = new Object[rowCount][colCount];
			// Iterate through rows and columns and store data in the 2D array
			for (int row = 1; row < rowCount+1; row++) {
				for (int col = 0; col < colCount; col++) {
					Data[row-1][col] = sheet.getRow(row).getCell(col).toString();
				}
			}

			// Close the Excel input stream
			inputStream.close();
			return Data;
		}
	}

	public Object[][]  readCSVData(String filePath) throws IOException {
		List<String[]> records = new ArrayList<>();
		filePath = "src/test/resources/exceldata/alpiqData.csv";
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		
		String line = "";
		line = reader.readLine();
		while((line = reader.readLine()) != null){
			String[] values = line.split(";");
			records.add(values);
		}

		int numRows = records.size();
		int numCols = records.get(0).length;

		// Create a 2D array to store the CSV data
		String[][] table = new String[numRows][numCols];

		// Copy data from the list to the 2D array
		for (int i = 0; i < numRows; i++) {
			table[i] = records.get(i);
		}

		return table;

	}
}
