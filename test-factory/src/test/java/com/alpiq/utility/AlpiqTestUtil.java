package com.alpiq.utility;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.io.FileHandler;
import com.alpiq.base.TestBase;


public class AlpiqTestUtil extends TestBase {

	
    public static String screenshotpath;
    private static String fileSeperator = System.getProperty("file.separator");
    private static String screenShotsDirectory = System.getProperty("user.dir") +fileSeperator+ "screenshots";
    ArrayList<String> tabs;
    public static String captureScreenshot(String methodName) throws IOException, WebDriverException {

        //Using GregorianCalendar to fetch the time details
        Calendar cal = new GregorianCalendar();
        //Month value is always 1 less than actual. For February, MONTH would return 1 
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);
        int sec = cal.get(Calendar.SECOND);
        int min = cal.get(Calendar.MINUTE);
        int date = cal.get(Calendar.DATE);
        int day = cal.get(Calendar.HOUR_OF_DAY);

        //Take screen shot and store it in volatile memory with reference name scrFile
        File scrFile = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE);
        try {

            //Create unique file name and store in under screenshot folder of root directory
            //screenshotpath = System.getProperty("user.dir") + "/screenshots/" + methodName + "_" + year + "_" + date+ "_" + (month + 1) + "_" + day + "_" + min + "_" + sec + ".jpeg";
        	screenshotpath = getScreenshotPath (screenShotsDirectory) + "/" +  methodName + "_" + year + "_" + date+ "_" + (month + 1) + "_" + day + "_" + min + "_" + sec + ".jpeg";
            //Copy the file to destination
            FileHandler.copy(scrFile, new File(screenshotpath));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
        return screenshotpath;

    }
    
    //Create the report path
    private static String getScreenshotPath (String path) {
    	File testDirectory = new File(path);
        if (!testDirectory.exists()) {
        	if (testDirectory.mkdir()) {
                System.out.println("Directory: " + path + " is created!" );
                return screenShotsDirectory;
            } else {
                System.out.println("Failed to create directory: " + path);
                return System.getProperty("user.dir");
            }
        } else {
            System.out.println("Directory already exists: " + path);
        }
		return screenShotsDirectory;
    }
    
	public void openAdmin(String baseURI) {
		// Open new Tab (for admin app)
		((JavascriptExecutor)getDriver()).executeScript("window.open()");
		tabs = new ArrayList<String>(getDriver().getWindowHandles());
		getDriver().switchTo().window(tabs.get(1));
		getDriver().get(baseURI);
		waitPageLoaded();
	}
	
	public void closeAdmin() {

		tabs = new ArrayList<String>(getDriver().getWindowHandles());
		// clos eAdmin
		getDriver().close();
		// Go back to the portal tab
		getDriver().switchTo().window(tabs.get(0));
	}
}
