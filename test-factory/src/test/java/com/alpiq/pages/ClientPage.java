package com.alpiq.pages;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import com.alpiq.base.TestBase;

public class ClientPage extends TestBase {
	
	public ClientPage() {
		PageFactory.initElements(getDriver(), this);
		wait = new FluentWait<>(getDriver())
				 .withTimeout(Duration.ofSeconds(240))			 
	             .pollingEvery(Duration.ofSeconds(3))
	             .ignoring(org.openqa.selenium.NoSuchElementException.class);
	}

	@FindBy(how = How.XPATH, using = "//input[@id = 'password-new']")
	WebElement motDePasseInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'password-confirm']")
	WebElement repetermotDePasseInput;
	@FindBy(how = How.XPATH, using = "//input[@id = 'submitBtn' and @value = 'VALIDER']")
	WebElement validerButton;
	@FindBy(how = How.XPATH, using = "//label[contains(text(), 'ter le mot de passe')]")
	WebElement repetermotDePasseLabel;
	@FindBy(how = How.XPATH, using = "//p[contains(text(),'Rendez-vous dans votre boite')]")
	List<WebElement> msgVerifierEmail;
	
	public void setMotDePasse(String value) {
		try {
//			wait.until(ExpectedConditions.elementToBeClickable(motDePasseInput));
			this.motDePasseInput.sendKeys(value);
		}catch (Exception e) {
			this.motDePasseInput.sendKeys(value);
		}
		
	}
	
	public void setRepeterMotDePasse(String value) {
		try {
//			wait.until(ExpectedConditions.elementToBeClickable(repetermotDePasseInput));
			this.repetermotDePasseInput.sendKeys(value);
		}catch (Exception e) {
			this.repetermotDePasseInput.sendKeys(value);
		}
		
	}
	
	public void clickOnValider() {
		clickOn(validerButton);
	}
	
	public Boolean verifierEmailMsg() throws InterruptedException {
		try {
//			wait.until(ExpectedConditions.visibilityOf(msgVerifierEmail.get(0)));
		}catch (Exception e) {
			Thread.sleep(5000);
		}
		try {
			if(msgVerifierEmail.size() == 0) { return false; } else { return true; }
		}catch (Exception e) {
			 return false;
		}
	}
	
}
