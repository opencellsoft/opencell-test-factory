package com.alpiq.pages;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import com.alpiq.base.TestBase;

public class SouscriptionPage extends TestBase {

	public SouscriptionPage() {
		PageFactory.initElements(getDriver(), this);
		wait = new FluentWait<>(getDriver())
				.withTimeout(Duration.ofSeconds(240))			 
	            .pollingEvery(Duration.ofSeconds(3))
				.ignoring(org.openqa.selenium.NoSuchElementException.class);
	}

	@FindBy(how = How.XPATH, using = "(//form//fieldset)[1]//input")
	WebElement nomTitulaireInput1;
	@FindBy(how = How.XPATH, using = "((//form)[2]//fieldset)[1]//input")
	WebElement nomTitulaireInput2;
	@FindBy(how = How.XPATH, using = "((//form)[2]//fieldset)[2]//input")
	WebElement pdlInput;
	@FindBy(how = How.XPATH, using = "(//form//fieldset)[2]//input[@aria-label = 'Votre adresse']")
	WebElement adresseInput;
	@FindBy(how = How.XPATH, using = "//input[@type = 'checkbox' and @value = 'PRESENCE_DE_CONSENTEMENT']")
	WebElement consentementCheckbox;
	@FindBy(how = How.XPATH, using = "//button[@type = 'submit' and contains(text(), 'Continuer')]")
	WebElement ContinuerButton1;
	@FindBy(how = How.XPATH, using = "(//button[@type = 'submit' and contains(text(), 'Continuer')])[2]")
	WebElement ContinuerButton2;
	@FindBy(how = How.XPATH, using = "//form//button[contains(text(), 'CONTINUER')]")
	WebElement ContinuerButton3;
	@FindBy(how = How.XPATH, using = "//a[contains(@class, 'text-underline')]//b[contains(text(), 'Je connais mon num')]")
	WebElement pdlLink;
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'range--text')]//span[text() = '0%']")
	WebElement zeroPerCent;
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'range--text')]//span[text() = '25%']")
	WebElement twentyFivePerCent;
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'range--text')]//span[text() = '50%']")
	WebElement fiftyPerCent;
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'range--text')]//span[text() = '75%']")
	WebElement seventyFivePerCent;
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'range--text')]//span[text() = '100%']")
	WebElement oneHundredPerCent;
	@FindBy(how = How.XPATH, using = "((//div[@class = 'energy-options'])[1]//span[@class = 'custom-control-indicator radio'])[1]")
	WebElement franceNord;
	@FindBy(how = How.XPATH, using = "((//div[@class = 'energy-options'])[1]//span[@class = 'custom-control-indicator radio'])[2]")
	WebElement franceSudOuest;
	@FindBy(how = How.XPATH, using = "((//div[@class = 'energy-options'])[1]//span[@class = 'custom-control-indicator radio'])[3]")
	WebElement franceSudEst;
	@FindBy(how = How.XPATH, using = "((//div[@class = 'energy-options'])[2]//span[@class = 'custom-control-indicator radio'])[1]")
	WebElement sourceSolaire;
	@FindBy(how = How.XPATH, using = "((//div[@class = 'energy-options'])[2]//span[@class = 'custom-control-indicator radio'])[2]")
	WebElement sourceHydraulique;
	@FindBy(how = How.XPATH, using = "//input[@value = 'MENS']/following-sibling::label/span")
	WebElement modeMensuel;
	@FindBy(how = How.XPATH, using = "//input[@value = 'REEL']/following-sibling::label/span")
	WebElement modeReel;
	@FindBy(how = How.XPATH, using = "//div[contains(@class , 'toggler--selector')]")
	WebElement identiteInput;
	@FindBy(how = How.XPATH, using = "//legend[contains(text(),'Pr')]/following-sibling::div//input[@type = 'text']")
	WebElement prenomInput;
	@FindBy(how = How.XPATH, using = "//input[@aria-label = 'Email']")
	WebElement emailInput;
	@FindBy(how = How.XPATH, using = "//input[contains(@id, 'phone_number')]")
	WebElement phoneInput;
	@FindBy(how = How.XPATH, using = "(//fieldset[@class = 'form-group'])[3]//input[@type = 'checkbox']")
	WebElement AcceptationRecevoirEmail;
	@FindBy(how = How.XPATH, using = "(//fieldset[@class = 'form-group'])[3]//input[@type = 'checkbox']")
	WebElement utilisationPhone;
	@FindBy(how = How.XPATH, using = "//div[@role = 'dialog']/following-sibling::label[contains(text(), 'lectionner une date')]")
	WebElement dateDebutInput;
	@FindBy(how = How.XPATH, using = "//button[@title = 'Next month']")
	WebElement moisSuivantBtn;
	@FindBy(how = How.XPATH, using = "//div[@role = 'button']//span[text() = '1']")
	WebElement premierDuMoisSuivantBtn;
	@FindBy(how = How.XPATH, using = "//legend[contains(text(),'IBAN')]/following-sibling::div//input[@type = 'text']")
	WebElement ibanInput;
	@FindBy(how = How.XPATH, using = "(//span[contains(@class, 'custom-control-indicator checkbox')])[1]")
	WebElement prelevementsAutomatiques;
	@FindBy(how = How.XPATH, using = "(//span[contains(@class, 'custom-control-indicator checkbox')])[2]")
	WebElement CGV;
	@FindBy(how = How.XPATH, using = "//button[@type = 'submit' and contains(text(), 'souscris')]")
	WebElement souscrirButton;
	@FindBy(how = How.XPATH, using = "//button[contains(@class ,'btn-main') and contains(text(), 'espace client')]")
	WebElement CreerEspaceClientButton;
	@FindBy(how = How.XPATH, using = "//div[@role = 'alert' and contains(text(), 'Veuillez-vous rapprocher du service client pour finaliser votre souscription')]")
	List<WebElement> alertMSG;
	@FindBy(how = How.XPATH, using = "//div[contains(@class, 'invalid-feedback') and contains(text(), 'veuillez vous connecter')]")
	List<WebElement> alertEmailExisteDeja;
	
	public void setNomTitulaire1(String value) {
		this.nomTitulaireInput1.sendKeys(Keys.CONTROL + "a");
		this.nomTitulaireInput1.sendKeys(value);
	}

	public void setNomTitulaire2(String value) {
		this.nomTitulaireInput2.sendKeys(Keys.CONTROL + "a");
		this.nomTitulaireInput2.sendKeys(value);
	}

	public void setadresse(String value) {
		this.adresseInput.sendKeys(Keys.CONTROL + "a");
		this.adresseInput.sendKeys(value);
	}

	public void setPdl(String value) {
		this.pdlInput.sendKeys(Keys.CONTROL + "a");
		this.pdlInput.sendKeys(value);
	}

	public void setConsentement() {
		clickOn(consentementCheckbox);;
	}

	public void clickOnContinuerButton1() throws InterruptedException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(ContinuerButton1));
		} catch (Exception e) {
			Thread.sleep(5000);
		}
		
		clickOn(ContinuerButton1);
	}

	public void clickOnContinuerButton2() throws InterruptedException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(ContinuerButton2));
		} catch (Exception e) {
			Thread.sleep(5000);
		}
		
		clickOn(ContinuerButton2);
	}

	public void clickOnContinuerButton3() throws InterruptedException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(ContinuerButton3));
		} catch (Exception e) {
			Thread.sleep(5000);
		}
		
		clickOn(ContinuerButton3);
	}

	public void clickOnPDLLink() throws InterruptedException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(pdlLink));
		} catch (Exception e) {
			Thread.sleep(5000);
		}
		
		clickOn(pdlLink);
	}
	
	public int alertMSG() {
		return alertMSG.size();
	}
	
	public int alertEmailExisteDeja() {
		return alertEmailExisteDeja.size();
	}
	
	public void setElectriciteRenouvelable(String value) throws InterruptedException {

		try {
			wait.until(ExpectedConditions.visibilityOf(fiftyPerCent));
		} catch (Exception e) {
			Thread.sleep(5000);
		}


		try {
			switch(value){
			case "0":clickOn(zeroPerCent);break;
			case "25":clickOn(twentyFivePerCent);break;
			case "50":clickOn(fiftyPerCent);break;
			case "70":clickOn(seventyFivePerCent);break;
			case "100":clickOn(oneHundredPerCent);break;
			}
		}catch (Exception e) {
			Thread.sleep(5000);
		}

	}

	public void setOrigine(String value) throws InterruptedException {
		try {
			switch(value){
			case "FRANCE_NO":clickOn(franceNord);break;
			case "FRANCE_SO":clickOn(franceSudOuest);break;
			case "FRANCE_SE":clickOn(franceSudEst);break;
			}
		}catch (Exception e) {
			Thread.sleep(5000);
		}
		
	}

	public void sourceEnergie(String value) throws InterruptedException {
		try {
			switch(value){
			case "SOLAIRE":clickOn(sourceSolaire);break;
			case "HYDRAULIQUE":clickOn(sourceHydraulique);break;
			}
		}catch (Exception e) {
			Thread.sleep(5000);
		}
		
		
	}

	public void modeFacturation(String value) throws InterruptedException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(modeMensuel));
		} catch (Exception e) {
			Thread.sleep(5000);
		}
		try {
			switch(value){
			case "MENS":clickOn(modeMensuel);break;
			case "REEL":clickOn(modeReel);break;
			}
		}catch (Exception e) {
			Thread.sleep(5000);
		}
	}

	public void setIdentite(String value) throws InterruptedException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(identiteInput));
		} catch (Exception e) {
			Thread.sleep(5000);
		}
		try {
			if(value.equals("Madame")) {clickOn(identiteInput);}
		}catch (Exception e) {
			Thread.sleep(5000);
		}
		
	}

	public void setPrenom(String value) throws InterruptedException {
		this.prenomInput.sendKeys(Keys.CONTROL + "a");
		this.prenomInput.sendKeys(value);
		Thread.sleep(3000);
	}

	public void setEmail(String value) throws InterruptedException {
		this.emailInput.sendKeys(Keys.CONTROL + "a");
		this.emailInput.sendKeys(value);
		Thread.sleep(3000);
	}

	public void setPhone(String value) throws InterruptedException {
		this.phoneInput.sendKeys(Keys.CONTROL + "a");
		this.phoneInput.sendKeys(value);
		Thread.sleep(3000);
	}

	public void setAcceptationRecevoirEmail(String value) {
		if(value.equals("oui")) {clickOn(AcceptationRecevoirEmail);}
	}

	public void SetUtilisationPhone(String value) {
		if(value.equals("oui")) {clickOn(utilisationPhone);}
	}

	public void setDateDebut() throws InterruptedException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(dateDebutInput));
		} catch (Exception e) {
			Thread.sleep(5000);
		}
		
		clickOn(dateDebutInput);
		try {
			wait.until(ExpectedConditions.elementToBeClickable(moisSuivantBtn));
		} catch (Exception e) {
			Thread.sleep(5000);
		}
		clickOn(moisSuivantBtn);
		try {
			wait.until(ExpectedConditions.elementToBeClickable(premierDuMoisSuivantBtn));
		} catch (Exception e) {
			Thread.sleep(5000);
		}
		clickOn(premierDuMoisSuivantBtn);
	}

	public void setIban(String value) {
		this.ibanInput.sendKeys(Keys.CONTROL + "a");
		this.ibanInput.sendKeys(value);
	}

	public void setPrelevementsAutomatiques(String value) throws InterruptedException {
		if(value.equals("oui")) {clickOn(prelevementsAutomatiques);}
		Thread.sleep(2000);
	}

	public void setCGV(String value) throws InterruptedException {
		if(value.equals("oui")) {clickOn(CGV);}
		Thread.sleep(2000);
	}

	public void clickOnSouscrirButton() throws InterruptedException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(souscrirButton));
		} catch (Exception e) {
			Thread.sleep(5000);
		}
		clickOn(souscrirButton);
	}

	public void clickOnCreerEspaceClientButton() throws InterruptedException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(CreerEspaceClientButton));
		} catch (Exception e) {
			Thread.sleep(5000);
		}
		clickOn(CreerEspaceClientButton);
	}

}
