package com.alpiq.pages;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import com.alpiq.base.TestBase;

public class HomePage extends TestBase {

	public HomePage() {
		PageFactory.initElements(getDriver(), this);
		wait = new FluentWait<>(getDriver())
				.withTimeout(Duration.ofSeconds(240))			 
	             .pollingEvery(Duration.ofSeconds(3))
	             .ignoring(org.openqa.selenium.NoSuchElementException.class);
	}

	@FindBy(how = How.XPATH, using = "//button[@id = 'onetrust-accept-btn-handler']")
	List<WebElement> accepteButton;
	@FindBy(how = How.XPATH, using = "//button[@id = 'onetrust-accept-btn-handler']")
	WebElement accepteButton2;
	@FindBy(how = How.XPATH, using = "//a[contains(text(), 'JE CHANGE DE FOURNISSEUR')]")
	WebElement switchingSupplierButton;

	public void clickOnAccepteButton() throws InterruptedException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(accepteButton2));
			clickOn(accepteButton2);
		}
		catch (Exception e) {
			if(accepteButton.size() !=0) {clickOn(accepteButton.get(0));}
		}
	}

	public void clickOnSwitchingSupplierButton() throws InterruptedException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(switchingSupplierButton));
			clickOn(switchingSupplierButton);
		}catch (Exception e) {
			clickOn(switchingSupplierButton);
		}
	}
}




