package com.alpiq.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.opencellsoft.base.CapabilityFactory;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Random;
import java.util.StringJoiner;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;
import org.openqa.selenium.remote.RemoteWebDriver;

public class TestBase {
	
	public CapabilityFactory capabilityFactory = new CapabilityFactory(); 
	String driverPath = System.getProperty("user.dir");
	protected static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<>();
	public static JavascriptExecutor executor;
	public static Wait<WebDriver> wait;
	
	
	public static Path changeLogPath ;
	public static String CSV_SPLITOR = ";" ;
	public static BufferedWriter writer;
	
	@BeforeSuite
	public static void setup() throws IOException {
		changeLogPath = Paths.get("exceldata", "alpiqperf", "report.csv");
		
		try (BufferedWriter writer = Files.newBufferedWriter(changeLogPath)){
			
            StringJoiner sjh = new StringJoiner(CSV_SPLITOR);
            sjh.add(cleanCSVContent("threadId"));
            sjh.add(cleanCSVContent("title"));
            sjh.add(cleanCSVContent("CurrentUrl"));
            sjh.add(cleanCSVContent("navigateStart"));
            sjh.add(cleanCSVContent("requestStart"));
            sjh.add(cleanCSVContent("responseStart"));
            sjh.add(cleanCSVContent("responseEnd"));
            sjh.add(cleanCSVContent("domComplete"));
            sjh.add(cleanCSVContent("loadEventStart"));
            sjh.add(cleanCSVContent("loadEventEnd"));
            //sjh.add(cleanCSVContent("backendPerformance_calc"));
            //sjh.add(cleanCSVContent("proccessingCompletPerformance_calc"));
            //sjh.add(cleanCSVContent("pageLoadTime"));
            sjh.add(cleanCSVContent("pagePerformance"));
            sjh.add(cleanCSVContent("page"));
            writer.write(sjh.toString() + "\n");
		}catch (Exception e) {
			System.err.format("IOException: %s%n", e);
		}
	}
	
	@AfterMethod
	public void quitDriver() {
		if (TestBase.getDriver() != null) {
			TestBase.getDriver().quit();
		}
	}
	
	public static  String cleanCSVContent(String content) {
        content = content.replace("\n", " ");
        content = content.replace(CSV_SPLITOR, "");
        return content;
    }

	public static void createDirectory(String directoryName) throws URISyntaxException, IOException {
        Path directoryPath = Paths.get("exceldata", directoryName);
        if (Files.notExists(directoryPath)) {
            Files.createDirectories(directoryPath);
        }
    }
	
	//	createDirectory("alpiqperf");
	public static WebDriver getDriver() {
        //Get driver from ThreadLocalMap
        return driver.get();
    }

	public static void waitPageLoaded() {
		//JavaScript Executor to check ready state
		executor = (JavascriptExecutor)driver.get();
		//iterate 7 times after every three second to verify if in ready state
		for (int i=0; i<10; i++){
			try {
				Thread.sleep(5000);
			}catch (InterruptedException ex) {
				System.out.println("Page has not loaded yet ");
			}
			//again check page state
			if (executor.executeScript("return document.readyState").toString().equals("complete")){
				break;
			}
		}
	}

	public static void clickOn(WebElement element) {
		try {
			element.click();
		}
		catch (Exception e) {
			executor = (JavascriptExecutor)driver.get();
			executor.executeScript("arguments[0].click();", element);
		}
		waitPageLoaded();
	}
	
	public static void writeReport() throws IOException {
		executor = (JavascriptExecutor)driver.get();
		long requestStart =(long) executor.executeScript("return window.performance.timing.requestStart");
		long responseStart = (long) executor.executeScript("return window.performance.timing.responseStart");
		long responseEnd = (long) executor.executeScript("return window.performance.timing.responseEnd");
		long navigationStart =(long) executor.executeScript("return window.performance.timing.navigationStart");
		long domComplete = (long) executor.executeScript("return window.performance.timing.domComplete");
		long loadEventStart = (long) executor.executeScript("return window.performance.timing.loadEventStart");
		long loadEventEnd = (long) executor.executeScript("return window.performance.timing.loadEventEnd");
		long threadId = Thread.currentThread().getId();
		Object pageLoadTimeMs = executor.executeScript("return performance.timing.loadEventEnd - performance.timing.navigationStart;");
		long pageLoadTime = new BigDecimal(String.valueOf(pageLoadTimeMs)).longValue();
		Object pagePerformanceMs = executor.executeScript("return performance.getEntriesByType(\"navigation\")[0][\"duration\"];");
		long pagePerformance = new BigDecimal(String.valueOf(pagePerformanceMs)).longValue();
		String page = String.valueOf(executor.executeScript("return performance.getEntriesByType(\"navigation\")[0][\"name\"];"));
		
		//System.out.println("pageLoadTime: " + pageLoadTime);
		System.out.println("pagePerformance: " + pagePerformance);
		System.out.println("page: " + page);
		
		// Calculate the performance
		long backendPerformance_calc = responseEnd - requestStart;
		long proccessingCompletPerformance_calc = domComplete - navigationStart;

		System.out.println("page title: " + driver.get().getTitle());
		System.out.println("currentUrl: " + driver.get().getCurrentUrl());
		System.out.println("Back End: ms " + backendPerformance_calc);
		System.out.println("Proccessing: ms " + proccessingCompletPerformance_calc);
		
		try (Writer writer = Files.newBufferedWriter(changeLogPath,StandardOpenOption.APPEND)){
            StringJoiner sjh = new StringJoiner(CSV_SPLITOR);
            sjh.add(cleanCSVContent(""+(threadId)));
            sjh.add(cleanCSVContent(driver.get().getTitle()));
            sjh.add(cleanCSVContent(driver.get().getCurrentUrl()));
            sjh.add(cleanCSVContent(""+(navigationStart)));
            sjh.add(cleanCSVContent(""+(requestStart)));
            sjh.add(cleanCSVContent(""+(responseStart)));
            sjh.add(cleanCSVContent(""+(responseEnd)));
            sjh.add(cleanCSVContent(""+(domComplete)));
            sjh.add(cleanCSVContent(""+(loadEventStart)));
            sjh.add(cleanCSVContent(""+(loadEventEnd)));
            //sjh.add(cleanCSVContent(""+(backendPerformance_calc)));
            //sjh.add(cleanCSVContent(""+(proccessingCompletPerformance_calc)));
            //sjh.add(cleanCSVContent(""+(pageLoadTime)));
            sjh.add(cleanCSVContent(""+(pagePerformance)));
            sjh.add(cleanCSVContent(""+(page)));
            
            writer.write(sjh.toString() + "\n");
		}
	}
	
	public static void randomWait() throws InterruptedException {
		Random r = new Random();
		int n = r.nextInt(600000) + 1000;
		Thread.sleep(n);
	}
}