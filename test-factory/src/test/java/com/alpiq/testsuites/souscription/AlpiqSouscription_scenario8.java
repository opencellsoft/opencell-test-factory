package com.alpiq.testsuites.souscription;

import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.alpiq.base.TestBase;
import com.alpiq.tests.ClientTests;
import com.alpiq.tests.HomeTests;
import com.alpiq.tests.SouscriptionTests;
import com.alpiq.utility.AlpiqTestUtility;
import com.opencellsoft.base.CapabilityFactory;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
public class AlpiqSouscription_scenario8 extends TestBase{

	HomeTests homeTests;
	SouscriptionTests souscriptionTests;
	ClientTests clientTests;
	AlpiqTestUtility utility;
	Boolean alert = null;
	Boolean alert2 = null;
	HashMap<String, String> data;
	
	public CapabilityFactory capabilityFactory = new CapabilityFactory(); 

	@Factory(dataProvider = "souscriptionDataCSV")
	public AlpiqSouscription_scenario8(String civilite, String nom,String prenom,String pdl, String email, String phone, String iban) {

		this.data = new HashMap<String, String>();
		this.data.put("civilite", civilite);
		this.data.put("nom", nom);
		this.data.put("prenom", prenom);
		this.data.put("pdl", pdl);
		this.data.put("email", email);
		this.data.put("phone", phone);
		this.data.put("iban", iban);

		homeTests =  new HomeTests();
		souscriptionTests = new SouscriptionTests();
		clientTests = new ClientTests();
		utility = new AlpiqTestUtility();
	}

	/***   souscription   ***/
	@Parameters({"browser","nodeURL","baseURI"})
	@Test(priority = 1)
	public void souscription(String browser, String nodeURL, String baseURI) throws Exception {
		
		// random wait
		randomWait();
		
		//Open webSite
		driver.set(new RemoteWebDriver(new URL(nodeURL), capabilityFactory.getCapabilities(browser)));
		driver.get().navigate().to(baseURI);
		writeReport();
		waitPageLoaded();
		
		//  changer le fournisseur
		homeTests.changerFournisseur();

		//  souscription Etape 1
		souscriptionTests.souscriptionEtape1(data.get("nom"), data.get("pdl") );
		alert = souscriptionTests.verifSouscriptionExisteDeja();

		if(alert.equals(false)) {
			//  souscription Etape 2
			souscriptionTests.souscriptionEtape2("50", "FRANCE_SO", "SOLAIRE");
			//  souscription Etape 3
			souscriptionTests.souscriptionEtape3();
			//  souscription Etape 4
			souscriptionTests.souscriptionEtape4("MENS");
			//  souscription Etape 5
			souscriptionTests.souscriptionEtape5(data.get("civilite"),data.get("prenom"), data.get("email"), data.get("phone"), "oui" , "non" );
			//  souscription Etape 6
			alert2 = souscriptionTests.verifEmailExisteDeja();
			if(alert2.equals(false)) {
				//  souscription Etape 6
				souscriptionTests.souscriptionEtape6(data.get("iban"),"oui","oui");
				//  souscription Etape 7
				souscriptionTests.souscriptionEtape7();
				//  Créer le compte client
				clientTests.creerCompteClient();
			}
		}
		
		if (TestBase.getDriver() != null) {
			TestBase.getDriver().quit();
		}
	}
	
	@DataProvider(name = "souscriptionDataCSV")
	public static Iterator<Object[]> souscriptionDataCSV() throws IOException, CsvValidationException{
		List<Object[]> list = new ArrayList<Object[]>();
		String csvFile = "src/test/resources/csvData/alpiqData8.csv";
		CSVReader csvReader = new CSVReader(new FileReader(csvFile));
		String[] data;
		// fetch entity without add it to data table.
		data = csvReader.readNext();
		
		// fetch data and add it to data table.
		while ((data = csvReader.readNext()) != null) {
			list.add(data);
		}
		csvReader.close();
		return list.iterator();
	}
}